<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Diagnostics.aspx.cs" Inherits="Diagnostics" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Admin Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
		<div style="background:silver;padding:5px">
			<asp:Literal runat="server" ID="litUserResultStatus"></asp:Literal>
		</div>
		<p>	
			Search User: <asp:TextBox ID="txtUsername" runat="server"></asp:TextBox> 
			<asp:ImageButton ID="Button1" OnClick="SearchUser" Text="Search" runat="server" ImageUrl="Themes/PartnerNet/images/Forum/search.gif" />		
			 (Search by Username or Email)						
		</p>		
		<asp:ListBox runat="server" ID="listUsers" AutoPostBack="true" EnableViewState="true" OnSelectedIndexChanged="GetSelectedUserDetails" Height="100" Visible="false"></asp:ListBox>	
		<br /><br />

		Update Email:<asp:TextBox ID="txtNewEmail" runat="server"></asp:TextBox>
		<asp:Button ID="Button4" OnClick="UpdateEmailAddress" Text="Save" runat="server"	/>		
		<br /><br />
		
		<b>Role Management</b>
		<div style="background:silver;padding:5px">
			<asp:Literal runat="server" ID="litRoles"></asp:Literal>
		</div>
		RoleName: <asp:TextBox ID="txtRoleName" runat="server"></asp:TextBox>		
		<asp:Button ID="Button2" OnClick="AddToRole" Text="Add Selected User to this Role" runat="server"	/>		
		<asp:Button ID="Button3" OnClick="RemoveFromRole" Text="Remove Selected User From this Role" runat="server"	/>		
		<br /><br />

		
		<div style="border:1px solid grey;color:Gray">			
			<asp:Literal runat="server" ID="litAllRoles"></asp:Literal>
		</div>
    </div>
    </form>
</body>
</html>

<%@ Application Codebehind="Global.asax.cs" Inherits="CommunityServer.Global" %>

<%@ Import Namespace="PartnerNetErrorHandler" %>
<%@ Import Namespace="PartnernetGlobalFunctions" %>
<%@ Import Namespace="AWS.Email" %>

<script Language="C#" RunAt="server">
    protected void Application_Error(Object sender, EventArgs e)
    {
        // At this point we have information about the error
        HttpContext ctx = HttpContext.Current;

        Exception exception = ctx.Server.GetLastError ();

        string errorInfo = 
         "<br>Offending URL: " + ctx.Request.Url.ToString () +
         "<br>Source: " + exception.Source + 
         "<br>Message: " + exception.Message +
         "<br>Stack trace: " + exception.StackTrace;
         
        //ctx.Response.Write (errorInfo);

	
	if (exception.Message.ToUpper() != "SERVER CANNOT MODIFY COOKIES AFTER HTTP HEADERS HAVE BEEN SENT.")
	{
        string ErrorUrl = ctx.Request.Url.ToString();
        string strAdminEmail = GlobalFunctions.GetPartnerNetConfig("ErrorHandler.ToAddress"); //System.Configuration.ConfigurationSettings.AppSettings["ErrorHandler.ToAddress"].ToString();

        EmailRequest EmailBuilder = new EmailRequest();
        EmailBuilder.TemplateFile = "Error.html";
        EmailBuilder.Subject = GlobalFunctions.GetPartnerNetConfig("ErrorHandler.Subject"); //System.Configuration.ConfigurationSettings.AppSettings["ErrorHandler.Subject"].ToString() + " - " + ErrorUrl;
        EmailBuilder.FromAddress = GlobalFunctions.GetPartnerNetConfig("ErrorHandler.FromAddress"); //System.Configuration.ConfigurationSettings.AppSettings["ErrorHandler.FromAddress"].ToString();
        EmailBuilder.FromName = GlobalFunctions.GetPartnerNetConfig("ErrorHandler.FromName"); //System.Configuration.ConfigurationSettings.AppSettings["ErrorHandler.FromName"].ToString();

        EmailRecipient chub = new EmailRecipient(strAdminEmail);                

        //--- Code to set the tokens in the Email Template to dynamic values --------
        Tokens tokes = chub.Tokens;
        tokes.Add("User", System.Web.HttpContext.Current.User.Identity.Name.ToString());
        tokes.Add("Url", ErrorUrl);
        tokes.Add("EmailBody", exception.Message);
        chub.Tokens = tokes;

        EmailBuilder.Recipients.Add(chub);
        //EmailBuilder.SubmitRequest();

        // --------------------------------------------------
        // To let the page finish running we clear the error
        // --------------------------------------------------
        ctx.Server.ClearError ();
      
      	Response.Redirect("http://my.habitat.org/error", false);
	}
    }

</script>
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using CommunityServer.Components;
using PartnerNetEncryptDecrypt;
using PartnerNetErrorHandler;
using PartnernetGlobalFunctions;

public partial class CSUpdateUser : CommunityServer.Components.CSPage
{
	protected void Page_Load(object sender, EventArgs e)
	{
		if (Request.QueryString.Count > 0)
		{
			string symmetricKey = GlobalFunctions.GetPartnerNetConfig("SymmetricKey");
			string userName = EncryptDecrypt.DecryptString(Request.QueryString["UserName"].ToString(), symmetricKey);
			string email = EncryptDecrypt.DecryptString(Request.QueryString["Email"].ToString(), symmetricKey);

			MembershipUser membershipUser = Membership.GetUser(userName);

			if (membershipUser != null)
			{
				User user = CommunityServer.Users.GetUser(userName);
				user.Email = email;
				CommunityServer.Users.UpdateUser(user);
			}
		}
	}
}
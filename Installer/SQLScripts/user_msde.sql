if not exists (select * from master.dbo.syslogins where loginname = N'NT AUTHORITY\NETWORK SERVICE')
exec sp_grantlogin N'NT AUTHORITY\NETWORK SERVICE'
GO
exec sp_grantdbaccess N'NT AUTHORITY\NETWORK SERVICE', N'NT Authority\Network Service'
GO
exec sp_addrolemember N'db_owner', N'NT Authority\Network Service'
GO

declare @HOSTNAME nvarchar(50)
set @HOSTNAME = CONVERT(nvarchar(50), SERVERPROPERTY('machinename'))

declare @AspNetUser nvarchar(50)
set @AspNetUser = @HOSTNAME + N'\ASPNET'

if not exists (select * from master.dbo.syslogins where loginname = @AspNetUser)
exec sp_grantlogin @AspNetUser
GO

declare @HOSTNAME nvarchar(50)
set @HOSTNAME = CONVERT(nvarchar(50), SERVERPROPERTY('machinename'))

declare @AspNetUser nvarchar(50)
set @AspNetUser = @HOSTNAME + N'\ASPNET'

exec sp_grantdbaccess @AspNetUser
GO

declare @HOSTNAME nvarchar(50)
set @HOSTNAME = CONVERT(nvarchar(50), SERVERPROPERTY('machinename'))

declare @AspNetUser nvarchar(50)
set @AspNetUser = @HOSTNAME + N'\ASPNET'

exec sp_addrolemember N'db_owner', @AspNetUser
GO
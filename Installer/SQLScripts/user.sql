if not exists (select * from master.dbo.syslogins where loginname = N'NT AUTHORITY\NETWORK SERVICE')
exec sp_grantlogin N'NT AUTHORITY\NETWORK SERVICE'
GO
exec sp_grantdbaccess N'NT AUTHORITY\NETWORK SERVICE', N'NT Authority\Network Service'
GO
exec sp_addrolemember N'db_owner', N'NT Authority\Network Service'
GO

declare @AspNetUser nvarchar(50)
set @AspNetUser = @@SERVERNAME + N'\ASPNET'

if not exists (select * from master.dbo.syslogins where loginname = @AspNetUser)
exec sp_grantlogin @AspNetUser
GO

declare @AspNetUser nvarchar(50)
set @AspNetUser = @@SERVERNAME + N'\ASPNET'

exec sp_grantdbaccess @AspNetUser
GO

declare @AspNetUser nvarchar(50)
set @AspNetUser = @@SERVERNAME + N'\ASPNET'

exec sp_addrolemember N'db_owner', @AspNetUser
GO
-- use master
-- restore database cs12test from disk='cs12test.bak' with replace
-- use cs12test

/*
Script created by SQL Compare from Red Gate Software Ltd at 1/22/2005 2:59:17 PM
*/
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_Messages]
(
[MessageID] [int] NOT NULL,
[Language] [nvarchar] (8) NULL CONSTRAINT [DF_cs_Messages_Language] DEFAULT ('en-US'),
[Title] [nvarchar] (1024) NULL,
[Body] [ntext] NULL,
[SettingsID] [int] NULL
)

GO

Create Table [dbo].[cs_EventLog]
(
	EventLogID [int] IDENTITY (1, 1) NOT NULL ,
	Message ntext NOT Null,
	Category nvarchar(255) NOT NULL,
	SettingsID [int] NOT NULL,
	EventID [int] NOT NULL,
	EventType [int] NOT NULL,
	EventDate DateTime NOT NULL Constraint cs_EventLog_EventDate Default(GetDate()),
	MachineName nvarchar(256) null,
	CONSTRAINT [PK_cs_EventLog] PRIMARY KEY  CLUSTERED 
	(
		[EventLogID]
	)  ON [PRIMARY] ,
)

Go

Create Table [dbo].[cs_es_Search_RemoveQueue]
(
	PostID int not null,
	SettingsID int not null
)

Go

Create Table [dbo].[cs_EventDescriptions]
(
	EventID [int] NOT NULL,
	[Description] nvarchar(512) NOT NULL,
	CONSTRAINT [PK_cs_EventDescriptions] PRIMARY KEY  CLUSTERED 
	(
		[EventID]
	)  ON [PRIMARY] ,

)

Go

Create Table [dbo].[cs_SectionTokens]
(
	TokenID [int] IDENTITY (1, 1) NOT NULL ,
	Token nvarchar(50) NOT Null,
	Link nvarchar(255) NULL,
	[Text] nvarchar(500) NOT NULL,
	SettingsID [int] NOT NULL,
	SectionID [int] NOT NULL,
	CONSTRAINT [PK_cs_Tokens] PRIMARY KEY  CLUSTERED 
	(
		[TokenID]
	)  ON [PRIMARY] ,
)

Go

CREATE Table [dbo].[cs_InkData]
(
	InkID [int] IDENTITY (1, 1) NOT NULL ,
	SettingsID [int] NOT NULL,
	SectionID [int] NOT NULL,
	UserID [int] NOT NULL,
	Ink NTEXT NOT NULL,
	DateUpdated [datetime] NOT NULL,
	ApplicationType [smallint] NOT NULL,
	CONSTRAINT [PK_cs_InkData] PRIMARY KEY  CLUSTERED 
	(
		[InkID]
	)  ON [PRIMARY] ,
)

Go

CREATE TABLE [dbo].[cs_Content] (
	[SettingsID] [int] NOT NULL ,
	[ContentID] [int] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (256) NOT NULL ,
	[Title] [nvarchar] (256) NOT NULL ,
	[Body] [ntext] NOT NULL ,
	[FormattedBody] [ntext] NOT NULL ,	
	[LastModified] [datetime] NOT NULL CONSTRAINT [DF_cs_Content_LastModifed] DEFAULT (getdate()),
	[SortOrder] [int] NOT NULL CONSTRAINT [DF_cs_Content_SortOrder] DEFAULT (0),
	[Hidden] [bit] NOT NULL CONSTRAINT [DF_cs_Content_Hidden] DEFAULT (1),
	CONSTRAINT [PK_cs_Content] PRIMARY KEY  CLUSTERED 
	(
		[ContentID]
	)  ON [PRIMARY] 
) 
GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_ApplicationConfigurationSettings]
(
	[SettingsID] [int] NOT NULL,
	[ApplicationType] [smallint] NOT NULL,
	[Settings] NText NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_ApplicationType]
(
[ApplicationType] [smallint] NOT NULL,
[ApplicationName] [varchar] (50) NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_Urls]
(
[UrlID] [int] NOT NULL,
[Url] [nvarchar] (512) NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_Referrals]
(
[ReferralID] [int] NOT NULL IDENTITY(1000, 1),
[SettingsID] [int] NOT NULL,
[SectionID] [int] NOT NULL,
[PostID] [int] NOT NULL,
[UrlID] [int] NOT NULL,
[Hits] [int] NOT NULL,
[LastDate] [datetime] NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_CodeScheduleType]
(
[ScheduleTypeCode] [int] NOT NULL IDENTITY(1, 1),
[ScheduleDescription] [varchar] (30) NULL,
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_Smilies]
(
[SmileyID] [int] NOT NULL IDENTITY(1, 1),
[SmileyCode] [nvarchar] (10) NULL,
[SmileyUrl] [nvarchar] (256) NULL,
[SmileyText] [nvarchar] (256) NULL,
[BracketSafe] [bit] NOT NULL CONSTRAINT [DF_cs_forums_BracketSafe] DEFAULT (0),
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_CodeServiceType]
(
[ServiceTypeCode] [int] NOT NULL IDENTITY(1, 1),
[ServiceTypeDescription] [nvarchar] (128) NULL,
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_ModerationAction]
(
[ModerationAction] [int] NOT NULL,
[Description] [nvarchar] (128) NULL,
[TotalActions] [int] NOT NULL CONSTRAINT [DF_cs_ModerationAction_TotalActions] DEFAULT (0),
[SettingsID] [int] NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_ModerationAudit]
(
[ID] [int] IDENTITY (1, 1) NOT NULL,
[ModerationAction] [int] NOT NULL,
[PostID] [int] NULL,
[UserID] [int] NULL,
[SectionID] [int] NULL,
[ModeratorID] [int] NOT NULL,
[ModeratedOn] [datetime] NOT NULL CONSTRAINT [DF_cs_ModerationAudit_ModeratedOn] DEFAULT (getdate()),
[Notes] [nvarchar] (1024) NULL,
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_LinkCategories]
(
[LinkCategoryID] [int] NOT NULL IDENTITY(1, 1),
[SectionID] [int] NOT NULL,
[Name] [nvarchar] (256) NULL,
[Description] [nvarchar] (2000) NULL,
[IsEnabled] [bit] NOT NULL,
[SortOrder] [int] NOT NULL CONSTRAINT [DF_cs_LinkCategories_SortOrder] DEFAULT (0),
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_Moderators]
(
[UserID] [int] NOT NULL,
[SectionID] [int] NOT NULL,
[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_cs_Moderators_DateCreated] DEFAULT (getdate()),
[EmailNotification] [bit] NOT NULL CONSTRAINT [DF_cs_Moderators_EmailNotification] DEFAULT (0),
[PostsModerated] [int] NOT NULL CONSTRAINT [DF_cs_Moderators_PostsModerated] DEFAULT (0),
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_Threads]
(
[ThreadID] [int] NOT NULL IDENTITY(1, 1),
[SectionID] [int] NOT NULL,
[UserID] [int] NOT NULL,
[PostAuthor] [nvarchar] (64) NULL CONSTRAINT [DF_cs_Threads_PostAuthor] DEFAULT (''),
[PostDate] [datetime] NOT NULL,
[ThreadDate] [datetime] NOT NULL,
[LastViewedDate] [datetime] NOT NULL CONSTRAINT [DF_cs_Threads_LastViewedDate] DEFAULT (getdate()),
[StickyDate] [datetime] NOT NULL,
[TotalViews] [int] NOT NULL CONSTRAINT [DF_cs_Threads_TotalViews] DEFAULT (0),
[TotalReplies] [int] NOT NULL CONSTRAINT [DF_cs_Threads_TotalReplies] DEFAULT (0),
[MostRecentPostAuthorID] [int] NOT NULL,
[MostRecentPostAuthor] [nvarchar] (64) NULL CONSTRAINT [DF_cs_Threads_MostRecentPostAuthor] DEFAULT (''),
[MostRecentPostID] [int] NOT NULL,
[IsLocked] [bit] NOT NULL,
[IsSticky] [bit] NOT NULL,
[IsApproved] [bit] NOT NULL CONSTRAINT [DF_cs_Threads_IsApproved] DEFAULT (1),
[RatingSum] [int] NOT NULL CONSTRAINT [DF_cs_Threads_RatingSum] DEFAULT (0),
[TotalRatings] [int] NOT NULL CONSTRAINT [DF_cs_Threads_TotalRating] DEFAULT (0),
[ThreadEmoticonID] [int] NOT NULL CONSTRAINT [DF_cs_Threads_ThreadEmoticon] DEFAULT (0),
[ThreadStatus] [int] NOT NULL CONSTRAINT [DF_cs_Threads_ThreadStatus] DEFAULT (0),
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_TrackedSections]
(
[SectionID] [int] NOT NULL,
[UserID] [int] NOT NULL,
[SubscriptionType] [int] NOT NULL CONSTRAINT [DF_cs_TrackedSections_SubscriptionType] DEFAULT (0),
[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_cs_TrackedSections_DateCreated] DEFAULT (getdate()),
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_UserProfile]
(
[UserID] [int] NOT NULL,
[TimeZone] [float] NOT NULL CONSTRAINT [DF_cs_UserProfile_TimeZone] DEFAULT ((-5)),
[TotalPosts] [int] NOT NULL CONSTRAINT [DF_cs_UserProfile_TotalPosts] DEFAULT (0),
[PostSortOrder] [int] NOT NULL CONSTRAINT [DF_cs_UserProfile_PostSortOrder] DEFAULT (0),
[PostRank] [binary] (1) NOT NULL CONSTRAINT [DF_cs_UserProfile_Attributes] DEFAULT (0),
[IsAvatarApproved] [smallint] NOT NULL CONSTRAINT [DF_cs_UserProfile_IsAvatarApproved] DEFAULT (1),
[ModerationLevel] [smallint] NOT NULL CONSTRAINT [DF_cs_UserProfile_IsTrusted] DEFAULT (0),
[EnableThreadTracking] [smallint] NOT NULL CONSTRAINT [DF_cs_UserProfile_TrackYourPosts] DEFAULT (0),
[EnableDisplayUnreadThreadsOnly] [smallint] NOT NULL CONSTRAINT [DF_cs_UserProfile_ShowUnreadTopicsOnly] DEFAULT (0),
[EnableAvatar] [smallint] NOT NULL CONSTRAINT [DF_cs_UserProfile_EnableAvatar] DEFAULT (0),
[EnableDisplayInMemberList] [smallint] NOT NULL CONSTRAINT [DF_cs_UserProfile_EnableDisplayInMemberList] DEFAULT (1),
[EnablePrivateMessages] [smallint] NOT NULL CONSTRAINT [DF_cs_UserProfile_EnablePrivateMessages] DEFAULT (1),
[EnableOnlineStatus] [smallint] NOT NULL CONSTRAINT [DF_cs_UserProfile_EnableOnlineStatus] DEFAULT (1),
[EnableEmail] [smallint] NOT NULL CONSTRAINT [DF_cs_UserProfile_EnableEmail] DEFAULT (1),
[EnableHtmlEmail] [smallint] NOT NULL CONSTRAINT [DF_cs_UserProfile_EnableHtmlEmail] DEFAULT (1),
[IsIgnored] [bit] NOT NULL CONSTRAINT [DF_cs_User_IsIgnored] DEFAULT (0),
[MembershipID] [uniqueidentifier] NOT NULL,
[SettingsID] [int] NOT NULL,
[PropertyNames] [ntext] NULL,
[PropertyValues] [ntext] NULL,
[Points] [int] NOT NULL CONSTRAINT [DF_cs_UserProfile_Points] DEFAULT (0),
[PublicToken] [UNIQUEIDENTIFIER] NOT NULL Constraint [CS_UserProfile_PublicTokenDefault] default(newid()),
[PointsUpdated] [datetime] NOT NULL CONSTRAINT [DF_cs_UserProfile_PointsUpdated] DEFAULT (getdate()),
[FavoritesShared] [int] NOT NULL CONSTRAINT [DF_cs_UserProfile_FavoritesShared] DEFAULT (0)
)

GO
Create TABLE [dbo].[cs_Users] (
	[MembershipID] UNIQUEIDENTIFIER  NOT NULL FOREIGN KEY REFERENCES dbo.aspnet_Users(UserId) ,
	[UserID] [int] IDENTITY (2100, 1) NOT NULL PRIMARY KEY CLUSTERED ,
	[ForceLogin] [bit] NOT NULL CONSTRAINT [DF_cs_User_ForceLogin] DEFAULT (0),
	[UserAccountStatus] [smallint] NOT NULL CONSTRAINT [DF_cs_User_Approved] DEFAULT (1),
	[AppUserToken] [varchar] (128) NULL ,
	[LastActivity] [datetime] NOT NULL CONSTRAINT [DF_cs_User_LastActivity] DEFAULT (getdate()),
	[LastAction] [nvarchar] (1024)  NULL CONSTRAINT [DF_cs_User_LastAction] DEFAULT ('')
	
)

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_Links]
(
[LinkID] [int] NOT NULL IDENTITY(1, 1),
[LinkCategoryID] [int] NOT NULL,
[Title] [nvarchar] (100) NULL,
[Url] [nvarchar] (255) NULL,
[IsEnabled] [bit] NOT NULL,
[SortOrder] [int] NOT NULL CONSTRAINT [DF_cs_Links_SortOrder] DEFAULT (0),
[SettingsID] [int], 
[Rel] [nvarchar] (100) NULL, 
[Description] [nvarchar] (2000) NULL,
[DateCreated] [datetime] NOT NULL CONSTRAINT [cs_Links_DateCreatedDefault] DEFAULT (getdate())
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_statistics_Site]
(
[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_cs_Statistics_DateCreated] DEFAULT (getdate()),
[TotalUsers] [int] NOT NULL,
[TotalPosts] [int] NOT NULL,
[TotalModerators] [int] NOT NULL,
[TotalModeratedPosts] [int] NOT NULL,
[TotalAnonymousUsers] [int] NOT NULL CONSTRAINT [DF_cs_Statistics_TotalAnonymousUsers] DEFAULT (0),
[TotalTopics] [int] NOT NULL,
[DaysPosts] [int] NOT NULL,
[DaysTopics] [int] NOT NULL,
[NewPostsInPast24Hours] [int] NOT NULL,
[NewThreadsInPast24Hours] [int] NOT NULL,
[NewUsersInPast24Hours] [int] NOT NULL,
[MostViewsPostID] [int] NOT NULL,
[MostActivePostID] [int] NOT NULL,
[MostActiveUserID] [int] NOT NULL,
[MostReadPostID] [int] NOT NULL,
[NewestUserID] [int] NOT NULL CONSTRAINT [DF_cs_statistics_Site_NewestUserID] DEFAULT (0),
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_ThreadRating]
(
[UserID] [int] NOT NULL,
[ThreadID] [int] NOT NULL,
[Rating] [int] NOT NULL,
[SettingsID] [int] NULL,
[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_cs_ThreadRating_DateCreated] DEFAULT (getdate())
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_EmailQueue]
(
[EmailID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_cs_EmailQueue_EmailID] DEFAULT (newid()),
[emailPriority] [int] NOT NULL,
[emailBodyFormat] [int] NOT NULL,
[emailTo] [nvarchar] (2000) NULL,
[emailCc] [ntext] NULL,
[emailBcc] [ntext] NULL,
[EmailFrom] [nvarchar] (256) NULL,
[EmailSubject] [nvarchar] (1024) NULL,
[EmailBody] [ntext] NULL,
[NextTryTime] [datetime] NOT NULL CONSTRAINT [DF_cs_EmailQueue_createdTimestamp] DEFAULT ('1/1/1979 12:00:00'),
[NumberOfTries] [int] NOT NULL CONSTRAINT [DF_cs_EmailQueue_NumberOfTries] DEFAULT (0),
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_weblog_Weblogs]
(
[SectionID] [int] NOT NULL,
[MostRecentPostID] [int] NOT NULL CONSTRAINT [DF_cs_weblog_Weblogs_MostRecentPostID] DEFAULT (0),
[MostRecentPostDate] [datetime] NOT NULL CONSTRAINT [DF_cs_weblog_Weblogs_MostRecentPostDate] DEFAULT (getdate()),
[MostRecentPostName] [nvarchar] (256) NULL,
[MostRecentPostAuthorID] [int] NOT NULL CONSTRAINT [DF_cs_weblog_Weblogs_MostRecentPostAuthorID] DEFAULT (0),
[MostRecentPostAuthor] [nvarchar] (256) NULL CONSTRAINT [DF_cs_weblog_Weblogs_MostRecentPostAuthor] DEFAULT (''),
[MostRecentPostSubject] [nvarchar] (256) NULL CONSTRAINT [DF_cs_weblog_Weblogs_MostRecentPostSubject] DEFAULT (''),
[MostRecentArticleID] [int] NOT NULL CONSTRAINT [DF_cs_weblog_Weblogs_MostRecentArticleID] DEFAULT (0),
[MostRecentArticleDate] [datetime] NOT NULL CONSTRAINT [DF_cs_weblog_Weblogs_MostRecentArticleDate] DEFAULT (getdate()),
[MostRecentArticleName] [nvarchar] (256) NULL,
[MostRecentArticleAuthorID] [int] NOT NULL CONSTRAINT [DF_cs_weblog_Weblogs_MostRecentArticleAuthorID] DEFAULT (0),
[MostRecentArticleAuthor] [nvarchar] (256) NULL CONSTRAINT [DF_cs_weblog_Weblogs_MostRecentArticleAuthor] DEFAULT (''),
[MostRecentArticleSubject] [nvarchar] (256) NULL CONSTRAINT [DF_cs_weblog_Weblogs_MostRecentArticleSubject] DEFAULT (''),
[PostCount] [int] NOT NULL CONSTRAINT [cs_weblog_Weblogs_PC] DEFAULT (0),
[ArticleCount] [int] NOT NULL CONSTRAINT [DF_cs_weblog_Weblogs_AC] DEFAULT (0),
[CommentCount] [int] NOT NULL CONSTRAINT [cs_weblog_Weblogs_CC] DEFAULT (0),
[TrackbackCount] [int] NOT NULL CONSTRAINT [cs_weblog_Weblogs_TC] DEFAULT (0),
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_Images]
(
[ImageID] [int] NOT NULL IDENTITY(1, 1),
[Length] [int] NOT NULL,
[ContentType] [nvarchar] (64) NULL,
[Content] [image] NOT NULL,
[DateLastUpdated] [datetime] NOT NULL CONSTRAINT [DF_cs_Images_DateLastUpdated] DEFAULT (getdate()),
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_UserAvatar]
(
[UserID] [int] NOT NULL,
[ImageID] [int] NOT NULL,
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_SectionsRead]
(
[GroupID] [int] NOT NULL CONSTRAINT [DF_cs_SectionsRead_GroupID] DEFAULT (0),
[SectionID] [int] NOT NULL,
[UserID] [int] NOT NULL,
[MarkReadAfter] [int] NOT NULL CONSTRAINT [DF_ForumsReadByDate_MarkReadAfter] DEFAULT (0),
[NewPosts] [bit] NOT NULL CONSTRAINT [DF_cs_SectionsRead_NewPosts] DEFAULT (1),
[LastActivity] [datetime] NOT NULL CONSTRAINT [DF_ForumsRead_LastActivity] DEFAULT (getdate()),
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_ForumPingback]
(
[SectionID] [int] NOT NULL,
[Pingback] [nvarchar] (512) NULL,
[Count] [int] NOT NULL CONSTRAINT [DF_cs_ForumPingback_Count] DEFAULT (0),
[LastUpdated] [datetime] NOT NULL CONSTRAINT [DF_cs_ForumPingback_LastUpdated] DEFAULT (getdate()),
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_AnonymousUsers]
(
[UserID] [char] (36) NULL,
[LastLogin] [datetime] NOT NULL CONSTRAINT [DF_AnonymousUsers_LastLogin] DEFAULT (getdate()),
[LastAction] [nvarchar] (1024) NULL CONSTRAINT [DF_cs_AnonymousUsers_LastAction] DEFAULT (''),
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_TrackedThreads]
(
[ThreadID] [int] NOT NULL,
[UserID] [int] NULL,
[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_ThreadTrackings_DateCreated] DEFAULT (getdate()),
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_PostAttachments]
(
[PostID] [int] NOT NULL,
[SectionID] [int] NOT NULL,
[UserID] [int] NOT NULL CONSTRAINT [DF_cs_PostAttachments_UserID] DEFAULT (0),
[Created] [datetime] NOT NULL CONSTRAINT [DF_cs_PostAttachments_Created] DEFAULT (getdate()),
[FileName] [nvarchar] (1024) NULL,
[Content] [image] NOT NULL,
[ContentType] [nvarchar] (50) NULL,
[ContentSize] [int] NOT NULL,
[SettingsID] [int] NULL,
[FriendlyFileName] [nvarchar] (256) NULL,
[Height] [int] NOT NULL DEFAULT (0),
[Width] [int] NOT NULL DEFAULT (0),
[IsRemote] bit NOT NULL Constraint cs_PostAttachments_IsRemoteDefault default(0)
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_PostAttachments_TEMP]
(
[AttachmentID] [uniqueidentifier] NOT NULL ,
[SectionID] [int] NOT NULL,
[UserID] [int] NOT NULL,
[Created] [datetime] NOT NULL,
[FileName] [nvarchar] (1024) NULL,
[Content] [image] NOT NULL,
[ContentType] [nvarchar] (50) NULL,
[ContentSize] [int] NOT NULL,
[SettingsID] [int] NULL,
[IsRemote] bit NOT NULL,
[Height] [int] NOT NULL DEFAULT (0),
[Width] [int] NOT NULL DEFAULT (0),
[FriendlyFileName] nvarchar(256) NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_Post_Categories]
(
[CategoryID] [int] NOT NULL IDENTITY(1000, 1),
[SectionID] [int] NOT NULL,
[Name] [nvarchar] (256) NULL,
[IsEnabled] [bit] NOT NULL,
[ParentID] [int] NOT NULL CONSTRAINT [DF_forums_Categories_ParentID] DEFAULT (0),
[Description] [nvarchar] (2000) NULL,
[SettingsID] [int] NULL,
[Path] [nvarchar] (255) NOT NULL CONSTRAINT [DF_cs_Post_Categories_Path] DEFAULT ('/'),
[TotalThreads] [int] NOT NULL CONSTRAINT [DF_cs_Post_Categories_TotalThreads] DEFAULT(0),
[MostRecentPostDate] [datetime] NULL,
[TotalSubThreads] [int] NOT NULL CONSTRAINT [DF_cs_Post_Categories_TotalSubThreads] DEFAULT(0),
[MostRecentSubPostDate] [datetime] NULL,
[FeaturedPostID] [int] NULL,
[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_cs_Post_Categories_DateCreated] DEFAULT( getdate() )
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[cs_vw_EveryOne_Role]
as

SELECT jA.[LoweredApplicationName], jA.[ApplicationId], [RoleId], [RoleName], [LoweredRoleName], aspnet_Roles.[Description] 

FROM [dbo].[aspnet_Roles]
JOIN [dbo].[aspnet_Applications] jA on [dbo].aspnet_Roles.ApplicationId = jA.ApplicationId

Where LoweredRoleName = 'everyone'

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_SiteSettings]
(
[SettingsID] [int] NOT NULL IDENTITY(1000, 1),
[Disabled] [bit] NOT NULL CONSTRAINT [DF_cs_SiteSettings_Enabled] DEFAULT (0),
[Version] [nvarchar] (64) NULL,
[SettingsXML] [ntext] NULL,
[SettingsKey] [uniqueidentifier] NULL,
[ApplicationName] [nvarchar] (256) NULL
	, CONSTRAINT PK_cs_SiteSettings PRIMARY KEY CLUSTERED (
		SettingsID
	)
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_Exceptions]
(
[ExceptionID] [int] NOT NULL IDENTITY(1, 1),
[ExceptionHash] [varchar] (128) NULL,
[SettingsID] [int] NOT NULL,
[Category] [int] NOT NULL,
[Exception] [nvarchar] (2000) NULL,
[ExceptionMessage] [nvarchar] (500) NULL,
[IPAddress] [varchar] (15) NULL,
[UserAgent] [nvarchar] (64) NULL,
[HttpReferrer] [nvarchar] (256) NULL,
[HttpVerb] [nvarchar] (24) NULL,
[PathAndQuery] [nvarchar] (512) NULL,
[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_cs_Exceptions_DateCreated] DEFAULT (getdate()),
[DateLastOccurred] [datetime] NOT NULL,
[Frequency] [int] NOT NULL CONSTRAINT [DF_cs_Exceptions_Frequency] DEFAULT (0)
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_Services]
(
[ServiceID] [int] NOT NULL IDENTITY(1, 1),
[ServiceName] [nvarchar] (30) NULL,
[ServiceTypeCode] [int] NULL,
[ServiceAssemblyPath] [nvarchar] (256) NULL,
[ServiceFullClassName] [nvarchar] (256) NULL,
[ServiceWorkingDirectory] [nvarchar] (256) NULL,
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_statistics_User]
(
[UserID] [int] NOT NULL CONSTRAINT [DF_cs_statistics_User_UserID] DEFAULT (0),
[TotalPosts] [int] NOT NULL CONSTRAINT [DF_forums_MostActiveUsers_TotalPosts] DEFAULT (0),
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_ThreadsRead]
(
[UserID] [int] NOT NULL,
[GroupID] [int] NOT NULL CONSTRAINT [DF_cs_ThreadsRead_GroupID] DEFAULT (0),
[SectionID] [int] NOT NULL CONSTRAINT [DF_cs_ThreadsRead_SectionID] DEFAULT (0),
[ThreadID] [int] NOT NULL,
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_Posts]
(
[PostID] [int] NOT NULL IDENTITY(1, 1),
[ThreadID] [int] NOT NULL,
[ParentID] [int] NOT NULL,
[PostAuthor] [nvarchar] (64) NULL CONSTRAINT [DF_cs_Posts_Username] DEFAULT (''),
[UserID] [int] NOT NULL,
[SectionID] [int] NOT NULL CONSTRAINT [DF_Posts_SectionID] DEFAULT (1),
[PostLevel] [int] NOT NULL,
[SortOrder] [int] NOT NULL,
[Subject] [nvarchar] (256) NULL,
[PostDate] [datetime] NOT NULL CONSTRAINT [DF_Posts_PostDate] DEFAULT (getdate()),
[IsApproved] [bit] NOT NULL CONSTRAINT [DF_Posts_Approved] DEFAULT (1),
[IsLocked] [bit] NOT NULL CONSTRAINT [DF_cs_Posts_IsLocked] DEFAULT (0),
[IsIndexed] [bit] NOT NULL CONSTRAINT [DF_cs_Posts_IsIndexed] DEFAULT (0),
[TotalViews] [int] NOT NULL CONSTRAINT [DF_Posts_Views] DEFAULT (0),
[Body] [ntext] NULL CONSTRAINT [DF__Posts__Body2__0B27A5C0] DEFAULT (''),
[FormattedBody] [ntext] NULL,
[IPAddress] [nvarchar] (32) NULL CONSTRAINT [DF_cs_Posts_IPAddress] DEFAULT (N'000.000.000.000'),
[PostType] [int] NOT NULL CONSTRAINT [DF__posts__PostType__290D0E62] DEFAULT (0),
[EmoticonID] [int] NOT NULL CONSTRAINT [DF_cs_Posts_EmoticonID] DEFAULT (0),
[PropertyNames] [ntext] NULL,
[PropertyValues] [ntext] NULL,
[SettingsID] [int] NULL,
[AggViews] [int] NOT NULL CONSTRAINT [DF__forums_Po__AggVi__78D3EB5B] DEFAULT (0),
[PostConfiguration] [int] NOT NULL DEFAULT (0),
[PostName] NVARCHAR(256),
[UserTime] DATETIME CONSTRAINT cs_Post_UserTimeDefaultValue DEFAULT(GetDate()),
[ApplicationPostType] INT NOT NULL CONSTRAINT cs_Post_ApplicationTypeDefaultValue DEFAULT(1),
[Points] [int] NOT NULL CONSTRAINT [DF_cs_Posts_Points] DEFAULT (0),
[RatingSum] [int] NOT NULL CONSTRAINT [DF_cs_Posts_RatingSum] DEFAULT (0),
[TotalRatings] [int] NOT NULL CONSTRAINT [DF_cs_Posts_TotalRatings] DEFAULT (0),
[PointsUpdated] [datetime] NOT NULL CONSTRAINT [DF_cs_Posts_PointsUpdated] DEFAULT (getdate())
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_SearchBarrel]
(
[WordHash] [int] NOT NULL,
[Word] [nvarchar] (64) NULL CONSTRAINT [DF_cs_SearchBarrel_word] DEFAULT (''),
[PostID] [int] NOT NULL,
[ThreadID] [int] NOT NULL CONSTRAINT [DF_cs_SearchBarrel_threadId_1] DEFAULT (0),
[SectionID] [int] NOT NULL CONSTRAINT [DF_cs_SearchBarrel_SectionID] DEFAULT (0),
[Weight] [float] NOT NULL CONSTRAINT [DF_cs_SearchBarrel_weight] DEFAULT (0),
[SettingsID] [int] NULL
)

 CREATE  CLUSTERED  INDEX [WordHash_SectionID_CLX_IDX] ON [dbo].[cs_SearchBarrel]([WordHash], [SectionID]) ON [PRIMARY]
GO
 
 CREATE  INDEX [Wordhash_PostID_Weight_IDX] ON [dbo].[cs_SearchBarrel]([WordHash], [PostID], [Weight]) ON [PRIMARY]
GO
 
 CREATE  INDEX [SectionID_IDX] ON [dbo].[cs_SearchBarrel]([SectionID]) ON [PRIMARY]
GO
 
 CREATE  INDEX [ThreadID_SettingsID_IDX] ON [dbo].[cs_SearchBarrel]([ThreadID], [SettingsID]) ON [PRIMARY]
GO
 
 CREATE  INDEX [PostID_SettingsID_IDX] ON [dbo].[cs_SearchBarrel]([PostID], [SettingsID]) ON [PRIMARY]
GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_ProductPermissions]
(
[SettingsID] [int] NOT NULL,
[ApplicationType] [smallint] NOT NULL,
[RoleID] [uniqueidentifier] NOT NULL,
[AllowMask] [binary] (8) NOT NULL,
[DenyMask] [binary] (8) NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_nntp_Posts]
(
[PostID] [int] NOT NULL CONSTRAINT [DF_cs_nntp_Posts_PostID] DEFAULT (0),
[SectionID] [int] NOT NULL CONSTRAINT [DF_cs_nntp_Posts_SectionID] DEFAULT (0),
[NntpPostID] [int] NOT NULL,
[NntpUniqueID] [nvarchar] (256) NULL,
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_ServiceSchedule]
(
[ServiceID] [int] NOT NULL,
[ScheduleName] [varchar] (30) NULL,
[MachineName] [varchar] (128) NULL,
[StartDate] [datetime] NOT NULL,
[EndDate] [datetime] NULL,
[ServiceParameters] [varchar] (256) NULL,
[ScheduleTypeCode] [int] NOT NULL,
[RunTimeHour] [int] NOT NULL,
[RunTimeMinute] [int] NOT NULL,
[DelayHour] [int] NULL,
[DelayMinute] [int] NULL,
[RunDaily] [binary] (8) NULL,
[RunWeekly] [binary] (8) NULL,
[RunMonthly] [binary] (12) NULL,
[RunYearly] [tinyint] NULL,
[RunOnce] [tinyint] NULL,
[LastRunTime] [datetime] NULL,
[NextRunTime] [datetime] NULL,
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_PostEditNotes]
(
[PostID] [int] NOT NULL,
[EditNotes] [nvarchar] (4000) NULL,
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_PostMetadata]
(
[PostID] [int] NOT NULL,
[MetaKey] [nvarchar] (50) NOT NULL,
[MetaType] [nvarchar] (50) NOT NULL,
[MetaValue] [nvarchar] (50) NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







Create function [dbo].[GetAnonymousUserID]
(
	@SettingsID int
)
RETURNS int
as
BEGIN
	Declare @UserID int
	Set @UserID = (Select cs_UserID FROM cs_vw_Users_FullUser where SettingsID = @SettingsID and IsAnonymous = 1)
	RETURN @UserID
END








GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_Censorship]
(
[SettingsID] [int] NOT NULL,
[WordID] [int] NOT NULL IDENTITY(1, 1),
[Word] [nvarchar] (20) NULL,
[Replacement] [nvarchar] (20) NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_DisallowedNames]
(
[DisallowedName] [nvarchar] (64) NOT NULL,
[SettingsID] [int] NOT NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_Groups]
(
[SettingsID] [int] NOT NULL CONSTRAINT [DF_cs_Groups_SettingsID] DEFAULT (0),
[GroupID] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (256) NULL,
[NewsgroupName] [nvarchar] (256) NULL CONSTRAINT [DF_cs_Groups_NewsgroupFriendlyName] DEFAULT (''),
[SortOrder] [int] NOT NULL CONSTRAINT [DF__ForumGrou__SortO__25518C17] DEFAULT (0),
[ApplicationType] [smallint] NOT NULL CONSTRAINT [DF_cs_Groups_ApplicationType] DEFAULT (0),
[Description] [nvarchar](1000) NOT NULL CONSTRAINT [DF_cs_Groups_Description] DEFAULT ('')
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_nntp_Newsgroups]
(
[SectionID] [int] NOT NULL,
[NntpGroup] [nvarchar] (256) NULL,
[NntpServer] [nvarchar] (100) NULL,
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_Posts_InCategories]
(
[CategoryID] [int] NOT NULL,
[PostID] [int] NOT NULL,
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_PostsArchive]
(
[PostID] [int] NOT NULL IDENTITY(1, 1),
[ThreadID] [int] NOT NULL,
[ParentID] [int] NOT NULL,
[PostLevel] [int] NOT NULL,
[SortOrder] [int] NOT NULL,
[Subject] [nvarchar] (256) NULL,
[PostDate] [datetime] NOT NULL,
[Approved] [bit] NOT NULL,
[SectionID] [int] NOT NULL,
[UserName] [nvarchar] (64) NULL,
[ThreadDate] [datetime] NOT NULL,
[TotalViews] [int] NOT NULL,
[IsLocked] [bit] NOT NULL,
[IsPinned] [bit] NOT NULL,
[PinnedDate] [datetime] NOT NULL,
[Body] [ntext] NULL,
[SettingsID] [int] NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_PrivateMessages]
(
[UserID] [int] NOT NULL,
[ThreadID] [int] NOT NULL,
[SettingsID] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_Ranks]
(
[RankID] [int] NOT NULL IDENTITY(1, 1),
[RankName] [nvarchar] (30) NULL,
[PostingCountMin] [int] NULL,
[PostingCountMax] [int] NULL,
[RankIconUrl] [nvarchar] (256) NULL,
[SettingsID] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_Reports]
(
[ReportID] [int] NOT NULL IDENTITY(1, 1),
[ReportName] [varchar] (20) NULL,
[Active] [bit] NOT NULL,
[ReportCommand] [varchar] (6500) NULL,
[ReportScript] [text] NULL,
[SettingsID] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_SchemaVersion]
(
[Major] [int] NOT NULL,
[Minor] [int] NOT NULL,
[Patch] [int] NOT NULL,
[InstallDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_SearchIgnoreWords]
(
[WordHash] [int] NOT NULL,
[Word] [nvarchar] (256) NULL,
[SettingsID] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_SectionPermissions]
(
[SettingsID] [int] NOT NULL,
[SectionID] [int] NOT NULL,
[RoleID] [uniqueidentifier] NOT NULL,
[AllowMask] [binary] (8) NOT NULL,
[DenyMask] [binary] (8) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_Sections]
(
[SectionID] [int] NOT NULL IDENTITY(1, 1),
[SettingsID] [int] NOT NULL CONSTRAINT [DF_cs_Sections_SettingsID] DEFAULT (0),
[IsActive] [smallint] NOT NULL CONSTRAINT [DF_Forums_Active] DEFAULT (1),
[ParentID] [int] NOT NULL CONSTRAINT [DF__Forums__ParentID__01342732] DEFAULT (0),
[GroupID] [int] NOT NULL,
[Name] [nvarchar] (256) NULL,
[NewsgroupName] [nvarchar] (256) NULL CONSTRAINT [DF_cs_Sections_NewsgroupName] DEFAULT (''),
[Description] [nvarchar] (1000) NULL,
[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_Forums_DateCreated] DEFAULT (getdate()),
[Url] [nvarchar] (512) NULL CONSTRAINT [DF_cs_Sections_Url] DEFAULT (''),
[IsModerated] [smallint] NOT NULL CONSTRAINT [DF_Forums_Moderated] DEFAULT (0),
[DaysToView] [int] NOT NULL CONSTRAINT [DF_Forums_DaysToView] DEFAULT (7),
[SortOrder] [int] NOT NULL CONSTRAINT [DF_Forums_SortOrder] DEFAULT (0),
[TotalPosts] [int] NOT NULL CONSTRAINT [DF_Forums_TotalPosts] DEFAULT (0),
[TotalThreads] [int] NOT NULL CONSTRAINT [DF_Forums_TotalThreads] DEFAULT (0),
[DisplayMask] [binary] (512) NOT NULL CONSTRAINT [DF__forums__DisplayM__004002F9] DEFAULT (0),
[EnablePostStatistics] [smallint] NOT NULL CONSTRAINT [DF_cs_Sections_EnablePostStatistics] DEFAULT (1),
[EnableAutoDelete] [smallint] NOT NULL CONSTRAINT [DF_cs_Sections_EnableAutoDelete] DEFAULT (0),
[EnableAnonymousPosting] [smallint] NOT NULL CONSTRAINT [DF_cs_Sections_EnableAnonymousPosting] DEFAULT (0),
[AutoDeleteThreshold] [int] NOT NULL CONSTRAINT [DF_cs_Sections_AutoDeleteThreshold] DEFAULT (90),
[MostRecentPostID] [int] NOT NULL CONSTRAINT [DF_Forums_MostRecentPostID] DEFAULT (0),
[MostRecentThreadID] [int] NOT NULL CONSTRAINT [DF_cs_Sections_MostRecentThreadID] DEFAULT (0),
[MostRecentThreadReplies] [int] NOT NULL CONSTRAINT [DF_cs_Sections_MostRecentThreadReplies] DEFAULT (0),
[MostRecentPostSubject] [nvarchar] (64) NULL CONSTRAINT [DF_cs_Sections_MostRecentPostSubject] DEFAULT (''),
[MostRecentPostAuthor] [nvarchar] (64) NULL CONSTRAINT [DF_cs_Sections_MostRecentPostAuthor] DEFAULT (''),
[MostRecentPostAuthorID] [int] NOT NULL CONSTRAINT [DF_cs_Sections_MostRecentPostAuthorID] DEFAULT (0),
[MostRecentPostDate] [datetime] NOT NULL CONSTRAINT [DF_cs_Sections_MostRecentPostDate] DEFAULT ('1/1/1797'),
[PostsToModerate] [int] NOT NULL CONSTRAINT [DF_forums_cs_PostsToModerate] DEFAULT (0),
[ForumType] [int] NOT NULL CONSTRAINT [DF_cs_Sections_ForumType] DEFAULT (0),
[IsSearchable] [smallint] NOT NULL CONSTRAINT [DF_cs_Sections_IsSearchable] DEFAULT (1),
[ApplicationType] [smallint] NOT NULL CONSTRAINT [DF_cs_Sections_ApplicationType] DEFAULT (0),
[ApplicationKey] [nvarchar] (256) NULL,
[PropertyNames] [ntext] NULL,
[PropertyValues] [ntext] NULL,
[Path] [varchar] (255) NOT NULL CONSTRAINT [DF__forums_For__Path__75F77EB0] DEFAULT (''),
[EnablePostPoints] [smallint] NOT NULL CONSTRAINT [DF_cs_Sections_EnablePostPoints] DEFAULT (1),
[DiskUsage] [bigint] NOT NULL CONSTRAINT [DF_cs_Sections_DiskUsage] DEFAULT (0)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_SiteMappings]
(
[SettingsID] [int] NOT NULL,
[SiteID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_Sites]
(
[SiteID] [int] NOT NULL IDENTITY(1000, 1),
[SiteUrl] [nvarchar] (512) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_Styles]
(
[StyleID] [int] NOT NULL IDENTITY(1, 1),
[StyleName] [varchar] (30) NULL,
[StyleSheetTemplate] [varchar] (30) NULL,
[BodyBackgroundColor] [varchar] (6) NULL,
[BodyTextColor] [varchar] (6) NULL,
[LinkVisited] [varchar] (6) NULL,
[LinkHover] [varchar] (6) NULL,
[LinkActive] [varchar] (6) NULL,
[RowColorPrimary] [varchar] (6) NULL,
[RowColorSecondary] [varchar] (6) NULL,
[RowColorTertiary] [varchar] (6) NULL,
[RowClassPrimary] [varchar] (30) NULL,
[RowClassSecondary] [varchar] (30) NULL,
[RowClassTertiary] [varchar] (30) NULL,
[HeaderColorPrimary] [varchar] (6) NULL,
[HeaderColorSecondary] [varchar] (6) NULL,
[HeaderColorTertiary] [varchar] (6) NULL,
[HeaderStylePrimary] [varchar] (30) NULL,
[HeaderStyleSecondary] [varchar] (30) NULL,
[HeaderStyleTertiary] [varchar] (30) NULL,
[CellColorPrimary] [varchar] (6) NULL,
[CellColorSecondary] [varchar] (6) NULL,
[CellColorTertiary] [varchar] (6) NULL,
[CellClassPrimary] [varchar] (30) NULL,
[CellClassSecondary] [varchar] (30) NULL,
[CellClassTertiary] [varchar] (30) NULL,
[FontFacePrimary] [varchar] (30) NULL,
[FontFaceSecondary] [varchar] (30) NULL,
[FontFaceTertiary] [varchar] (30) NULL,
[FontSizePrimary] [smallint] NULL,
[FontSizeSecondary] [smallint] NULL,
[FontSizeTertiary] [smallint] NULL,
[FontColorPrimary] [varchar] (6) NULL,
[FontColorSecondary] [varchar] (6) NULL,
[FontColorTertiary] [varchar] (6) NULL,
[SpanClassPrimary] [varchar] (30) NULL,
[SpanClassSecondary] [varchar] (30) NULL,
[SpanClassTertiary] [varchar] (30) NULL,
[SettingsID] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_Version]
(
[VERSION_MAJOR] [int] NOT NULL,
[VERSION_MINOR] [int] NOT NULL,
[VERSION_REVISION] [int] NOT NULL,
[VERSION_BUILD] [int] NOT NULL,
[SettingsID] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_Votes]
(
[PostID] [int] NOT NULL,
[UserID] [int] NOT NULL,
[Vote] [nvarchar] (2) NOT NULL,
[SettingsID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_VoteSummary]
(
[PostID] [int] NOT NULL,
[Vote] [nvarchar] (2) NULL,
[VoteCount] [int] NOT NULL,
[SettingsID] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_Post_Categories_Parents](
	[CategoryID] [int] NOT NULL,
	[UpLevelID] [int] NOT NULL,
	[SectionID] [int] NOT NULL,
CONSTRAINT [PK_cs_Post_Category_Parent] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC,
	[UpLevelID] ASC
) ON [PRIMARY]
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_PostRating]
(
	[UserID] [int] NOT NULL,
	[PostID] [int] NOT NULL,
	[Rating] [int] NOT NULL,
	[SettingsID] [int] NULL,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_cs_PostRating_DateCreated] DEFAULT (getdate())
)  ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_FavoritePosts]
(
	[FavoriteID] [int] IDENTITY (1, 1) NOT NULL ,
	[OwnerID] [int] NOT NULL ,
	[SettingsID] [int] NOT NULL ,
	[PostID] [int] NOT NULL ,
	[ApplicationType] [smallint] NOT NULL CONSTRAINT [DF_cs_FavoritePosts_ApplicationType] DEFAULT (9999)
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_FavoriteSections]
(
	[FavoriteID] [int] IDENTITY (1, 1) NOT NULL ,
	[OwnerID] [int] NOT NULL ,
	[SettingsID] [int] NOT NULL ,
	[SectionID] [int] NOT NULL ,
	[ApplicationType] [smallint] NOT NULL CONSTRAINT [DF_cs_FavoriteSections_ApplicationType] DEFAULT (9999)
) ON [PRIMARY]	
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_FavoriteUsers]
(
	[FavoriteID] [int] IDENTITY (1, 1) NOT NULL ,
	[OwnerID] [int] NOT NULL ,
	[SettingsID] [int] NOT NULL ,
	[UserID] [int] NOT NULL ,
	[ApplicationType] [smallint] NOT NULL CONSTRAINT [DF_cs_FavoriteUsers_ApplicationType] DEFAULT (9999)
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[files_Downloads] (
            [UserID] [int] NOT NULL ,
            [PostID] [int] NOT NULL ,
            [DownloadDate] [datetime] NOT NULL 
) ON [PRIMARY]
GO
CREATE  INDEX [IX_files_Downloads_UserID] ON [dbo].[files_Downloads]([UserID]) ON [PRIMARY]
GO
CREATE  INDEX [IX_files_Downloads_PostID] ON [dbo].[files_Downloads]([PostID]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_UrlRedirects] (
            [urlID] [int] IDENTITY (1, 1) NOT NULL ,
            [url] [nvarchar] (4000) NOT NULL ,
            [impressions] [int] NOT NULL ,
            [description] [nvarchar] (2000) NULL
) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_BannedAddresses] (
	[SettingsID] [int] NOT NULL ,
	[NetworkAddress] [nvarchar] (50) NOT NULL ,
	[BannedDate] [datetime] NOT NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[cs_BannedAddresses] WITH NOCHECK ADD 
	CONSTRAINT [PK_cs_BannedAddresses] PRIMARY KEY  CLUSTERED 
	(
		[SettingsID],
		[NetworkAddress]
	)  ON [PRIMARY] 
GO
ALTER TABLE [dbo].[cs_BannedAddresses] WITH NOCHECK ADD 
	CONSTRAINT [UK_cs_BannedAddresses] UNIQUE  NONCLUSTERED 
	(
		[NetworkAddress]
	)  ON [PRIMARY] 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cs_BannedNetworks] (
	[SettingsID] [int] NOT NULL ,
	[BannedNetworkID] [int] IDENTITY (1, 1) NOT NULL ,
	[StartingAddress] [nvarchar] (50) NOT NULL ,
	[EndingAddress] [nvarchar] (50) NOT NULL ,
	[BannedDate] [datetime] NULL 
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[cs_BannedNetworks] WITH NOCHECK ADD 
	CONSTRAINT [PK_cs_BannedNetworks] PRIMARY KEY  CLUSTERED 
	(
		[SettingsID],
		[BannedNetworkID]
	)  ON [PRIMARY] 
GO
ALTER TABLE [dbo].[cs_BannedNetworks] WITH NOCHECK ADD 
	CONSTRAINT [UK_cs_BannedNetworks] UNIQUE  NONCLUSTERED 
	(
		[BannedNetworkID]
	)  ON [PRIMARY] 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

CREATE TABLE [dbo].[cs_Folder]
(
	FolderId INT IDENTITY(1, 1) NOT NULL
		PRIMARY KEY NONCLUSTERED,
	UserId INT NOT NULL
		REFERENCES cs_Users(UserId),
	FolderName NVARCHAR(50) NOT NULL,
	ParentFolderId INT NULL
		REFERENCES cs_Folder(FolderId),
	SettingsID INT NOT NULL
		REFERENCES cs_SiteSettings(SettingsID)
)
GO


CREATE TABLE [dbo].[cs_FeedState]
(
	FeedStateId INT IDENTITY(0, 1) NOT NULL
		PRIMARY KEY CLUSTERED,
	StateName NVARCHAR(255) NULL,
	Description NTEXT NULL
)
GO


CREATE TABLE [dbo].[cs_Feed]
(
	FeedId INT IDENTITY(1, 1) NOT NULL
		PRIMARY KEY CLUSTERED,
	Url NVARCHAR(255) NOT NULL,
	Title NVARCHAR(255) NULL,
	Link NVARCHAR(255) NULL,
	Language NVARCHAR(10) NULL,
	Generator NVARCHAR(255) NULL,
	SubscribeDate DATETIME NOT NULL
		DEFAULT GetDate(),
	LastUpdateDate DATETIME NOT NULL
		DEFAULT GetDate(),
	LastModified DATETIME NOT NULL Default('1/1/2000'),
	ETag nvarchar(256) NULL,
	FeedStateId INT NOT NULL
		REFERENCES cs_FeedState(FeedStateId) DEFAULT 1,
	SettingsID INT NOT NULL
		REFERENCES cs_SiteSettings(SettingsID)
)
GO


CREATE TABLE [dbo].[cs_FolderFeed]
(
	FolderFeedId INT IDENTITY(1, 1) NOT NULL
		PRIMARY KEY NONCLUSTERED,
	[SettingsID] INT,
	UserID INT NOT NULL
		REFERENCES cs_Users(UserID),
	FolderId INT NULL
		REFERENCES cs_Folder(FolderId),
	FeedId INT NOT NULL
		REFERENCES cs_Feed(FeedId)
)
GO


CREATE TABLE [dbo].[cs_FeedPost]
(
	FeedPostId INT IDENTITY(1, 1) NOT NULL
		PRIMARY KEY NONCLUSTERED,
	FeedId INT NOT NULL
		REFERENCES cs_Feed(FeedId),
	Author NVARCHAR(255) NULL,
	Title NVARCHAR(255) NULL,
	Description NTEXT NULL,
	Source NVARCHAR(255) NULL,
	GuidName NVARCHAR(255) NULL,
	GuidIsPermaLink BIT NOT NULL,
	Link NVARCHAR(255) NULL,
	PubDate DATETIME NULL,
	CommentsUrl NVARCHAR(255) NULL,
	EnclosureUrl VARCHAR(255) NULL,
	EnclosureLength BIGINT NULL,
	EnclosureType VARCHAR(100) NULL,
	Creator NVARCHAR(255) NULL,
	CommentApiUrl NVARCHAR(255) NULL,
	CommentRssUrl NVARCHAR(255) NULL,
	CommentCount INT NULL	
)
GO

CREATE TABLE [dbo].[cs_UserReadPost]
(
	UserId INT NOT NULL
		PRIMARY KEY NONCLUSTERED (UserId, FeedPostId)
		FOREIGN KEY REFERENCES cs_Users(UserId) ,
	FeedPostId INT NOT NULL
		REFERENCES cs_FeedPost(FeedPostId)
	
)
GO

GO
CREATE TRIGGER [dbo].Tr_FeedPost_UserReadPost
  ON cs_FeedPost FOR DELETE
  AS
	SET NOCOUNT ON

	DELETE urp FROM cs_UserReadPost urp
	INNER JOIN deleted d ON urp.FeedPostId = d.FeedPostId
GO

CREATE  Table [dbo].cs_posts_deleted_archive
(
	DPID [int] IDENTITY (1, 1) NOT NULL , 
	PostID [int],
	SectionID [int],
	UserID [int],
	ParentID [int],
	ThreadID [int],
	PostType [int],
	ApplicationType [int],
	PostLevel [int],
	SettingsID [int],
	DeletedDate [datetime],
	Body [ntext],
	Subject nvarchar(500),
	IPAddress nvarchar(50)
)
GO

CREATE TABLE [dbo].[cs_RoleQuotas] (
	[SettingsID] [int] NOT NULL ,
	[RoleID] [uniqueidentifier] NOT NULL ,
	[ImageQuota] [int] NOT NULL CONSTRAINT [DF_cs_GalleryRoleQuotas_ImageQuota] DEFAULT (-1),
	[DiskQuota] [bigint] NOT NULL CONSTRAINT [DF_cs_GalleryRoleQuotas_DiskQuota] DEFAULT (-1)
) ON [PRIMARY]
GO

CREATE CLUSTERED INDEX Idx_UserReadPost_UserIdFeedId ON [dbo].cs_UserReadPost(UserId, FeedPostId)
GO
CREATE CLUSTERED INDEX Idx_Folder_FolderId ON [dbo].cs_Folder(FolderId)
GO
CREATE CLUSTERED INDEX Idx_FolderFeed_FolderId_FeedId	ON [dbo].cs_FolderFeed(FolderId, FeedId)
GO
CREATE CLUSTERED INDEX Idx_FeedPost_FeedId	ON [dbo].cs_FeedPost(FeedId)
GO
CREATE NONCLUSTERED INDEX Idx_Feed_Url	ON [dbo].cs_Feed(Url)
GO
CREATE NONCLUSTERED INDEX Idx_UserReadPost_FeedPostId	ON [dbo].cs_UserReadPost(FeedPostId)
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ARITHABORT ON
GO
ALTER TABLE [dbo].[cs_SchemaVersion] ADD CONSTRAINT [PK_cs_SchemaVersion] PRIMARY KEY CLUSTERED  ([Major], [Minor], [Patch])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Images] ADD CONSTRAINT [PK_cs_Images] PRIMARY KEY CLUSTERED  ([ImageID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Posts] ADD CONSTRAINT [PK_Posts] PRIMARY KEY CLUSTERED  ([PostID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_LinkCategories] ADD CONSTRAINT [PK_cs_LinkCategories] PRIMARY KEY CLUSTERED  ([LinkCategoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Links] ADD CONSTRAINT [PK_cs_Links] PRIMARY KEY CLUSTERED  ([LinkID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Exceptions] ADD CONSTRAINT [IX_cs_Exceptions] UNIQUE NONCLUSTERED  ([ExceptionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Exceptions] ADD CONSTRAINT [IX_cs_Exceptions_1] UNIQUE NONCLUSTERED  ([ExceptionHash])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_statistics_Site] ADD CONSTRAINT [PK_forums_Statistics] PRIMARY KEY CLUSTERED  ([DateCreated] DESC)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_SearchBarrel] ADD CONSTRAINT [IX_cs_SearchBarrel] UNIQUE NONCLUSTERED  ([WordHash], [PostID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_weblog_Weblogs] ADD CONSTRAINT [PK_cs_weblog_Weblogs] PRIMARY KEY CLUSTERED  ([SectionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Threads] ADD CONSTRAINT [PK_cs_Threads] PRIMARY KEY CLUSTERED  ([ThreadID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Posts_InCategories] ADD CONSTRAINT [PK_cs_PostsInCategories] PRIMARY KEY CLUSTERED  ([CategoryID], [PostID]) with fillfactor = 90
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_ThreadRating] ADD CONSTRAINT [IX_cs_ThreadRating] UNIQUE NONCLUSTERED  ([UserID], [ThreadID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_SiteMappings] ADD CONSTRAINT [PK_cs_SiteMappings] PRIMARY KEY CLUSTERED  ([SettingsID], [SiteID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_PostMetadata] ADD CONSTRAINT [PK_cs_PostMetadata] PRIMARY KEY CLUSTERED  ([PostID], [MetaKey])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Urls] ADD CONSTRAINT [PK_cs_Urls] PRIMARY KEY CLUSTERED  ([UrlID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_CodeServiceType] ADD CONSTRAINT [PK_SERVICE_TYPE_CODE] PRIMARY KEY CLUSTERED  ([ServiceTypeCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_SectionPermissions] ADD CONSTRAINT [PK_cs_SectionPermissions] PRIMARY KEY CLUSTERED  ([SettingsID], [SectionID], [RoleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Moderators] ADD CONSTRAINT [PK_Moderators] PRIMARY KEY CLUSTERED  ([UserID], [SectionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_SectionsRead] ADD CONSTRAINT [IX_ForumsReadByDate] UNIQUE NONCLUSTERED  ([SectionID], [UserID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_SectionsRead] ADD CONSTRAINT [PK_cs_SectionsRead] PRIMARY KEY CLUSTERED  ([UserID], [SectionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_PostEditNotes] ADD CONSTRAINT [IX_cs_PostEditNotes] UNIQUE CLUSTERED  ([PostID] DESC)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_PostEditNotes] ADD CONSTRAINT [PK_cs_PostEditNotes] PRIMARY KEY NONCLUSTERED  ([PostID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Referrals] ADD CONSTRAINT [PK_cs_Referrals] PRIMARY KEY CLUSTERED  ([SettingsID], [SectionID], [PostID], [UrlID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Post_Categories] ADD CONSTRAINT [PK_forums_Categories] PRIMARY KEY CLUSTERED  ([CategoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Post_Categories] ADD CONSTRAINT [IX_forums_Post_CategoryName] UNIQUE NONCLUSTERED  ([Name], [SectionID], [SettingsID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_ApplicationType] ADD CONSTRAINT [PK_cs_ApplicationType] PRIMARY KEY CLUSTERED  ([ApplicationType])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Reports] ADD CONSTRAINT [PK_REPORTS] PRIMARY KEY CLUSTERED  ([ReportID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Services] ADD CONSTRAINT [PK_SERVICE_ID] PRIMARY KEY CLUSTERED  ([ServiceID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Services] ADD CONSTRAINT [UK_SERVICE_NAME] UNIQUE NONCLUSTERED  ([ServiceName])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_UserProfile] ADD CONSTRAINT [PK_cs_UserProfile] PRIMARY KEY CLUSTERED  ([UserID], [SettingsID])
GO
 CREATE  INDEX [IX_cs_UserProfile_MembershipID] ON [dbo].[cs_UserProfile]([MembershipID]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_TrackedSections] ADD CONSTRAINT [IX_cs_TrackedSections] UNIQUE CLUSTERED  ([SectionID], [UserID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Ranks] ADD CONSTRAINT [PK_RANK_ID] PRIMARY KEY CLUSTERED  ([RankID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Ranks] ADD CONSTRAINT [UK_RANK_NAME] UNIQUE NONCLUSTERED  ([RankName])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_ServiceSchedule] ADD CONSTRAINT [PK_SERVICE_SCHEDULE] PRIMARY KEY CLUSTERED  ([ServiceID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_ModerationAction] ADD CONSTRAINT [IX_cs_ModerationAction] UNIQUE CLUSTERED  ([ModerationAction], [SettingsID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_ModerationAction] ADD CONSTRAINT [PK_cs_ModerationAction] PRIMARY KEY NONCLUSTERED  ([ModerationAction], [SettingsID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_ProductPermissions] ADD CONSTRAINT [PK_cs_ProductPermissions] PRIMARY KEY CLUSTERED  ([SettingsID], [ApplicationType], [RoleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Styles] ADD CONSTRAINT [PK_STYLE] PRIMARY KEY CLUSTERED  ([StyleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Groups] ADD CONSTRAINT [IX_cs_Groups_Validation] UNIQUE NONCLUSTERED  ([SettingsID], [Name], [ApplicationType])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Groups] ADD CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED  ([SettingsID], [GroupID], [ApplicationType])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Censorship] ADD CONSTRAINT [PK_CENSORSHIP] PRIMARY KEY CLUSTERED  ([WordID], [SettingsID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Censorship] ADD CONSTRAINT [UK_CENSORSHIP] UNIQUE NONCLUSTERED  ([SettingsID], [Word])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Sites] ADD CONSTRAINT [PK_cs_Sites] PRIMARY KEY CLUSTERED  ([SiteID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_CodeScheduleType] ADD CONSTRAINT [PK_CODE_SCHEDULE_TYPE] PRIMARY KEY CLUSTERED  ([ScheduleTypeCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_DisallowedNames] ADD CONSTRAINT [PK_DISALLOWED_NAME] PRIMARY KEY CLUSTERED  ([DisallowedName], [SettingsID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Smilies] ADD CONSTRAINT [PK_SMILIES_ID] PRIMARY KEY CLUSTERED  ([SmileyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Smilies] ADD CONSTRAINT [IX_cs_Smilies] UNIQUE  NONCLUSTERED ( [SmileyCode],[SettingsID] )  ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Sections] ADD CONSTRAINT [PK_cs_Sections] PRIMARY KEY CLUSTERED  ([SettingsID], [SectionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_Sections] ADD CONSTRAINT [IX_cs_Sections_Validation] UNIQUE NONCLUSTERED  ([SettingsID], [ApplicationKey], [ApplicationType])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_PostRating] ADD CONSTRAINT [IX_cs_PostRating] UNIQUE NONCLUSTERED ([UserID], [PostID]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_FavoritePosts] ADD CONSTRAINT [PK_cs_FavoritePosts] PRIMARY KEY CLUSTERED ([FavoriteID]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_FavoriteSections] ADD CONSTRAINT [PK_cs_FavoriteSections] PRIMARY KEY CLUSTERED ([FavoriteID]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[cs_FavoriteUsers] ADD CONSTRAINT [PK_cs_FavoriteUsers] PRIMARY KEY CLUSTERED ([FavoriteID]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
ALTER TABLE [dbo].[cs_RoleQuotas] WITH NOCHECK ADD CONSTRAINT [PK_cs_GalleryRoleQuotas] PRIMARY KEY  CLUSTERED 	([SettingsID], [RoleID]) ON [PRIMARY]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END

GO
ALTER TABLE [dbo].[cs_SearchBarrel] WITH NOCHECK ADD
CONSTRAINT [FK_cs_SearchBarrel_cs_Posts] FOREIGN KEY ([PostID]) REFERENCES [dbo].[cs_Posts] ([PostID]) ON DELETE CASCADE NOT FOR REPLICATION,
CONSTRAINT [FK_cs_SearchBarrel_cs_Threads] FOREIGN KEY ([ThreadID]) REFERENCES [dbo].[cs_Threads] ([ThreadID]) ON DELETE CASCADE NOT FOR REPLICATION
ALTER TABLE [dbo].[cs_ThreadsRead] WITH NOCHECK ADD
CONSTRAINT [FK_cs_ThreadsRead_cs_Threads] FOREIGN KEY ([ThreadID]) REFERENCES [dbo].[cs_Threads] ([ThreadID]) ON DELETE CASCADE NOT FOR REPLICATION
ALTER TABLE [dbo].[cs_TrackedThreads] WITH NOCHECK ADD
CONSTRAINT [FK_cs_TrackedThreads_cs_Threads] FOREIGN KEY ([ThreadID]) REFERENCES [dbo].[cs_Threads] ([ThreadID]) ON DELETE CASCADE NOT FOR REPLICATION
ALTER TABLE [dbo].[cs_ThreadRating] ADD
CONSTRAINT [FK_cs_ThreadRating_cs_UserProfile] FOREIGN KEY ([UserID], [SettingsID]) REFERENCES [dbo].[cs_UserProfile] ([UserID], [SettingsID]) ON DELETE CASCADE,
CONSTRAINT [FK_cs_ThreadRating_cs_Threads] FOREIGN KEY ([ThreadID]) REFERENCES [dbo].[cs_Threads] ([ThreadID]) ON DELETE CASCADE
ALTER TABLE [dbo].[cs_SectionPermissions] ADD
CONSTRAINT [FK_cs_SectionPermissions_SectionID] FOREIGN KEY ([SettingsID], [SectionID]) REFERENCES [dbo].[cs_Sections] ([SettingsID], [SectionID]),
CONSTRAINT [FK_cs_SectionPermissions_RoleID] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[aspnet_Roles] ([RoleId])
ALTER TABLE [dbo].[cs_ProductPermissions] ADD
CONSTRAINT [FK_cs_ProductPermissions_ApplicationType] FOREIGN KEY ([ApplicationType]) REFERENCES [dbo].[cs_ApplicationType] ([ApplicationType]),
CONSTRAINT [FK_cs_ProductPermission_SettingsID] FOREIGN KEY ([SettingsID]) REFERENCES [dbo].[cs_SiteSettings] ([SettingsID]),
CONSTRAINT [FK_cs_ProductPermissions_RoleID] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[aspnet_Roles] ([RoleId])
ALTER TABLE [dbo].[cs_Services] ADD
CONSTRAINT [FK_SERVICE_TYPE_CODE] FOREIGN KEY ([ServiceTypeCode]) REFERENCES [dbo].[cs_CodeServiceType] ([ServiceTypeCode])
ALTER TABLE [dbo].[cs_Referrals] ADD
CONSTRAINT [FK_cs_Referrals_cs_Urls] FOREIGN KEY ([UrlID]) REFERENCES [dbo].[cs_Urls] ([UrlID])
ALTER TABLE [dbo].[cs_UserAvatar] ADD
CONSTRAINT [FK_cs_UserAvatar_cs_Images] FOREIGN KEY ([ImageID]) REFERENCES [dbo].[cs_Images] ([ImageID]) ON DELETE CASCADE,
CONSTRAINT [FK_cs_UserAvatar_cs_UserProfile] FOREIGN KEY ([UserID], [SettingsID]) REFERENCES [dbo].[cs_UserProfile] ([UserID], [SettingsID]) ON DELETE CASCADE
ALTER TABLE [dbo].[cs_nntp_Posts] ADD
CONSTRAINT [FK_cs_nntp_Posts_cs_Posts] FOREIGN KEY ([PostID]) REFERENCES [dbo].[cs_Posts] ([PostID]) ON DELETE CASCADE
ALTER TABLE [dbo].[cs_PostEditNotes] ADD
CONSTRAINT [FK_cs_PostEditNotes_cs_Posts] FOREIGN KEY ([PostID]) REFERENCES [dbo].[cs_Posts] ([PostID]) ON DELETE CASCADE
ALTER TABLE [dbo].[cs_PostMetadata] ADD
CONSTRAINT [FK_cs_PostMetadata_cs_Posts] FOREIGN KEY ([PostID]) REFERENCES [dbo].[cs_Posts] ([PostID]) ON DELETE CASCADE
ALTER TABLE [dbo].[cs_ServiceSchedule] ADD
CONSTRAINT [FK_SCHEDULE_TYPE_CD] FOREIGN KEY ([ScheduleTypeCode]) REFERENCES [dbo].[cs_CodeScheduleType] ([ScheduleTypeCode]),
CONSTRAINT [FK_SERVICE_ID] FOREIGN KEY ([ServiceID]) REFERENCES [dbo].[cs_Services] ([ServiceID])
ALTER TABLE [dbo].[cs_Links] ADD
CONSTRAINT [FK_cs_Links_cs_LinkCategories] FOREIGN KEY ([LinkCategoryID]) REFERENCES [dbo].[cs_LinkCategories] ([LinkCategoryID])
ALTER TABLE [dbo].[cs_Posts] ADD
CONSTRAINT [FK_cs_Posts_cs_Threads] FOREIGN KEY ([ThreadID]) REFERENCES [dbo].[cs_Threads] ([ThreadID])
ALTER TABLE [dbo].[cs_ModerationAudit] ADD
CONSTRAINT [ID] PRIMARY KEY CLUSTERED ([ID])
ALTER TABLE cs_UserProfile ADD Constraint [FK_cs_Users] FOREIGN KEY  ([UserID]) REFERENCES [cs_Users] ([UserID])
ALTER TABLE [dbo].[cs_Post_Categories_Parents]  WITH NOCHECK ADD  
CONSTRAINT [FK_cs_Post_Categories_Parents_cs_Post_Categories] FOREIGN KEY([CategoryID]) REFERENCES [dbo].[cs_Post_Categories] ([CategoryID])
ALTER TABLE [dbo].[cs_PostRating]
ADD CONSTRAINT [FK_cs_PostRating_cs_Posts] FOREIGN KEY ([PostID]) REFERENCES [dbo].[cs_Posts] ([PostID]) ON DELETE CASCADE
ALTER TABLE [dbo].[cs_PostRating]
ADD CONSTRAINT [FK_cs_PostRating_cs_UserProfile] FOREIGN KEY ([UserID], [SettingsID]) REFERENCES [dbo].[cs_UserProfile] ([UserID], [SettingsID]) ON DELETE CASCADE
ALTER TABLE [dbo].[cs_FavoritePosts] ADD
CONSTRAINT [FK_cs_FavoritePosts_cs_UserProfile] FOREIGN KEY ([OwnerID],	[SettingsID]) REFERENCES [dbo].[cs_UserProfile] ([UserID], [SettingsID]) ON DELETE CASCADE,
CONSTRAINT [FK_cs_FavoritePosts_ApplicationType] FOREIGN KEY ([ApplicationType]) REFERENCES [dbo].[cs_ApplicationType] ([ApplicationType])
ALTER TABLE [dbo].[cs_FavoriteSections] ADD
CONSTRAINT [FK_cs_FavoriteSections_cs_UserProfile] FOREIGN KEY ([OwnerID], [SettingsID]) REFERENCES [dbo].[cs_UserProfile] ([UserID], [SettingsID]) ON DELETE CASCADE,
CONSTRAINT [FK_cs_FavoriteSections_ApplicationType] FOREIGN KEY ([ApplicationType]) REFERENCES [dbo].[cs_ApplicationType] ([ApplicationType])
ALTER TABLE [dbo].[cs_FavoriteUsers] ADD
CONSTRAINT [FK_cs_FavoriteUsers_cs_UserProfile] FOREIGN KEY ([OwnerID], [SettingsID]) REFERENCES [dbo].[cs_UserProfile] ([UserID], [SettingsID]) ON DELETE CASCADE,
CONSTRAINT [FK_cs_FavoriteUsers_ApplicationType] FOREIGN KEY ([ApplicationType]) REFERENCES [dbo].[cs_ApplicationType] ([ApplicationType])
ALTER TABLE [dbo].[cs_RoleQuotas] ADD 
CONSTRAINT [FK_cs_GalleryRoleQuotas_aspnet_Roles] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[aspnet_Roles] ([RoleId]),
CONSTRAINT [FK_cs_GalleryRoleQuotas_cs_SiteSettings] FOREIGN KEY ([SettingsID]) REFERENCES [dbo].[cs_SiteSettings] ([SettingsID])
GO
ALTER TABLE [dbo].[cs_UrlRedirects] WITH NOCHECK ADD 
CONSTRAINT [DF_cs_UrlRedirects_views] DEFAULT (0) FOR [impressions],
CONSTRAINT [IX_cs_UrlRedirects] UNIQUE  NONCLUSTERED ([urlID]) ON [PRIMARY] 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE CLUSTERED INDEX [IX_cs_ForumPingback] ON [dbo].[cs_ForumPingback] ([SectionID])
CREATE CLUSTERED INDEX [IX_cs_PrivateMessages] ON [dbo].[cs_PrivateMessages] ([UserID])
CREATE CLUSTERED INDEX [IX_PostsRead] ON [dbo].[cs_ThreadsRead] ([UserID])
CREATE CLUSTERED INDEX [IX_cs_UserAvatar_1] ON [dbo].[cs_UserAvatar] ([UserID])
CREATE CLUSTERED INDEX [IX_cs_Votes] ON [dbo].[cs_Votes] ([PostID])
CREATE CLUSTERED INDEX [IX_cs_VoteSummary] ON [dbo].[cs_VoteSummary] ([PostID])
CREATE NONCLUSTERED INDEX [IX_AnonymousUsers] ON [dbo].[cs_AnonymousUsers] ([LastLogin])
CREATE NONCLUSTERED INDEX [IX_cs_EmailQueue] ON [dbo].[cs_EmailQueue] ([EmailID])
CREATE NONCLUSTERED INDEX [IX_cs_ModerationAudit] ON [dbo].[cs_ModerationAudit] ([ModerationAction])
CREATE NONCLUSTERED INDEX [IX_cs_ModerationAudit_ModeratedOn] ON [dbo].[cs_ModerationAudit]([ModeratedOn])
CREATE NONCLUSTERED INDEX [IX_cs_PostAttachments] ON [dbo].[cs_PostAttachments] ([PostID])
CREATE NONCLUSTERED INDEX [IX_Posts_ThreadID] ON [dbo].[cs_Posts] ([ThreadID])
CREATE NONCLUSTERED INDEX [IX_Posts_ParentID] ON [dbo].[cs_Posts] ([ParentID])
CREATE NONCLUSTERED INDEX [IX_Posts_PostDate] ON [dbo].[cs_Posts] ([UserID], [PostDate])
CREATE NONCLUSTERED INDEX [IX_Posts_SectionID] ON [dbo].[cs_Posts] ([SectionID])
CREATE NONCLUSTERED INDEX [SectionID_Approved] ON [dbo].[cs_Posts] ([SectionID], [IsApproved])
CREATE NONCLUSTERED INDEX [IX_Posts_PostLevel] ON [dbo].[cs_Posts] ([PostLevel])
CREATE NONCLUSTERED INDEX [IX_Posts_SortOrder] ON [dbo].[cs_Posts] ([SortOrder])
CREATE NONCLUSTERED INDEX [IX_cs_Posts_SectionID_SettingsID] ON [dbo].[cs_Posts] ([SectionID], [SettingsID])
CREATE NONCLUSTERED INDEX [IX_Posts_Approved] ON [dbo].[cs_Posts] ([IsApproved])
CREATE NONCLUSTERED INDEX [IX_cs_PrivateMessages_1] ON [dbo].[cs_PrivateMessages] ([ThreadID])
CREATE NONCLUSTERED INDEX [IX_forums_SearchWordDictionary] ON [dbo].[cs_SearchIgnoreWords] ([WordHash])
CREATE NONCLUSTERED INDEX [IX_Section_Active] ON [dbo].[cs_Sections] ([SettingsID], [IsActive])
CREATE NONCLUSTERED INDEX [IX_ForumsRead] ON [dbo].[cs_SectionsRead] ([SectionID])
CREATE NONCLUSTERED INDEX [IX_forums_MostActiveUsers] ON [dbo].[cs_statistics_User] ([TotalPosts] DESC)
CREATE NONCLUSTERED INDEX [IX_cs_Threads] ON [dbo].[cs_Threads] ([SectionID], [ThreadID] DESC)
CREATE NONCLUSTERED INDEX [IX_cs_Threads_1] ON [dbo].[cs_Threads] ([SectionID], [StickyDate] DESC)
CREATE NONCLUSTERED INDEX [IX_cs_Threads_StickyDate] ON [dbo].[cs_Threads] ([SectionID], [StickyDate], [IsApproved])
CREATE NONCLUSTERED INDEX [IX_cs_Threads_ThreadID_SectionID_UserID] ON [dbo].[cs_Threads] ([SettingsID], [SectionID], [StickyDate])
CREATE NONCLUSTERED INDEX [IX_PostsRead_1] ON [dbo].[cs_ThreadsRead] ([ThreadID])
CREATE NONCLUSTERED INDEX [IX_cs_UserAvatar] ON [dbo].[cs_UserAvatar] ([UserID])
CREATE NONCLUSTERED INDEX [IX_cs_UserProfile] ON [dbo].[cs_UserProfile] ([TotalPosts] DESC)
CREATE CLUSTERED INDEX [IX_cs_TrackedThreads_ThreadID_UserID] ON [dbo].[cs_TrackedThreads] ([ThreadID], [UserID])
CREATE  INDEX [IX_cs_Posts_Thread_Sort] ON [dbo].[cs_Posts]([ThreadID], [SortOrder]) ON [PRIMARY]
CREATE  INDEX [IX_cs_Posts_InCategories_PostID] ON [dbo].[cs_Posts_InCategories]([PostID]) ON [PRIMARY]
CREATE INDEX IX_cs_Posts_Post_Parent ON cs_Posts(ParentID,PostDate,PostID)
CREATE UNIQUE NONCLUSTERED INDEX [IX_cs_cs_ApplicationConfigurationSettings] ON [dbo].[cs_ApplicationConfigurationSettings]([SettingsID],[ApplicationType])  ON [PRIMARY]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER cs_Group_Delete ON cs_Groups 
FOR DELETE 
AS
BEGIN
	DELETE cs_Sections WHERE GroupID IN (SELECT GroupID FROM DELETED)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER cs_nntp_Post_Delete ON [cs_nntp_Posts] 
FOR DELETE 
AS
BEGIN
	DELETE cs_Posts WHERE PostID IN (SELECT PostID FROM DELETED)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER cs_Section_Delete ON cs_Sections 
FOR DELETE 
AS
BEGIN
	DELETE cs_SectionPermissions WHERE SectionID IN (SELECT SectionID FROM DELETED)
	DELETE cs_Threads WHERE SectionID IN (SELECT SectionID FROM DELETED)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TABLE [dbo].[cs_BlogActivityReport] (
	[SectionID] [int] NOT NULL ,
	[ThreadID] [int] NOT NULL ,
	[CoverageDate] [datetime] NOT NULL ,
	[TotalViews] [int] NOT NULL ,
	[DayViews] [int] NOT NULL ,
	[DayComments] [int] NOT NULL ,
	[DayTrackBacks] [int] NOT NULL ,
	[IsPost] [bit] NOT NULL ,
	[IsArticle] [bit] NOT NULL ,
	[JobExecutionTimeStamp] [datetime] NOT NULL ,
	[DayPosts] [int] NOT NULL CONSTRAINT [DF_cs_BlogActivityReport_DayPosts] DEFAULT (0),
	[DayArticles] [int] NOT NULL CONSTRAINT [DF_cs_BlogActivityReport_DayArticles] DEFAULT (0)
	
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[cs_VisitsDaily] (
	[IP] [nvarchar] (20) NOT NULL ,
	[VisitCount] [int] NOT NULL ,
	[VisitDate] [datetime] NOT NULL ,
	[JobDate] [datetime] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[cs_Visits] (
	[IP] [nvarchar] (20) NOT NULL ,
	[VisitCount] [int] NOT NULL ,
	[DateTimeStamp] [datetime] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[cs_Licenses] (
	[LicenseID] [uniqueidentifier] NOT NULL ,
	[LicenseType] [nvarchar] (50) NOT NULL ,
	[LicenseValue] [ntext] NOT NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[cs_Licenses] WITH NOCHECK ADD 
	CONSTRAINT [PK_cs_Licenses] PRIMARY KEY  CLUSTERED 
	(
		[LicenseID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[cs_Licenses] ADD 
	CONSTRAINT [IX_cs_Licenses] UNIQUE  NONCLUSTERED 
	(
		[LicenseType]
	)  ON [PRIMARY] 
GO

--Bring the cs_SchemaVersion up to the current level
DECLARE @Major int, @Minor int, @Patch int, @Installed DateTime

Set @Major = 2;
Set @Minor = 0;
Set @Patch = 1;

WHILE( @Patch <= 57) --## Dont forget to update the patch counter ;) ##
BEGIN
	IF NOT EXISTS (Select * from cs_SchemaVersion where Major=@Major and Minor=@Minor and Patch=@Patch)
		Insert into cs_SchemaVersion(Major, Minor, Patch, InstallDate) values (@Major, @Minor, @Patch, GetDate())
	Set @Patch = @Patch + 1
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO


SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database updated succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors

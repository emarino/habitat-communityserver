if not exists( select * from [dbo].[aspnet_SchemaVersions] where Feature = N'common' and CompatibleSchemaVersion = '1' )
	INSERT INTO [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'common', N'1', 1)

if not exists( select * from [dbo].[aspnet_SchemaVersions] where Feature = N'membership' and CompatibleSchemaVersion = '1' )
	INSERT INTO [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'membership', N'1', 1)

if not exists( select * from [dbo].[aspnet_SchemaVersions] where Feature = N'profile' and CompatibleSchemaVersion = '1' )
	INSERT INTO [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'profile', N'1', 1)

if not exists( select * from [dbo].[aspnet_SchemaVersions] where Feature = N'role manager' and CompatibleSchemaVersion = '1' )
	INSERT INTO [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'role manager', N'1', 1)

if not exists( select * from cs_SchemaVersion where Major = 1 and Minor = 0 and Patch = 0 )
	insert into cs_SchemaVersion values(1,0,0, getdate())

if not exists( select * from cs_ApplicationType where ApplicationType = 0 )
	insert into cs_ApplicationType values( 0, 'Forum')

if not exists( select * from cs_ApplicationType where ApplicationType = 1 )
	insert into cs_ApplicationType values( 1, 'Weblog')

if not exists( select * from cs_ApplicationType where ApplicationType = 2 )
	insert into cs_ApplicationType values( 2, 'Gallery')

if not exists( select * from cs_ApplicationType where ApplicationType = 3 )
	insert into cs_ApplicationType values( 3, 'GuestBook')

if not exists( select * from cs_ApplicationType where ApplicationType = 4 )
	insert into cs_ApplicationType values( 4, 'Calendar')

if not exists( select * from cs_ApplicationType where ApplicationType = 5 )
	insert into cs_ApplicationType values( 5, 'ContentManagment')

if not exists( select * from cs_ApplicationType where ApplicationType = 6 )
	insert into cs_ApplicationType values( 6, 'FileGallery')

if not exists( select * from cs_ApplicationType where ApplicationType = 7 )
	insert into cs_ApplicationType values( 7, 'FeedReader')

if not exists( select * from cs_ApplicationType where ApplicationType = 8 )
	insert into cs_ApplicationType values( 8, 'BlogRoller')

if not exists( select * from cs_ApplicationType where ApplicationType = 9 )
	insert into cs_ApplicationType values( 9, 'Ads')

if not exists( select * from cs_ApplicationType where ApplicationType = 10 )
	insert into cs_ApplicationType values( 10, 'Spam')

if not exists( select * from cs_ApplicationType where ApplicationType = 52 )
	insert into cs_ApplicationType values( 52, 'MyKB')

if not exists( select * from cs_ApplicationType where ApplicationType = 9999 )
	insert into cs_ApplicationType values( 9999, 'Unknown')
	
	
	INSERT INTO cs_FeedState(
	StateName,
	Description
)
VALUES 
(
	'Updated successfully.',
	'This feed has been updated successfully.'
)
GO
INSERT INTO cs_FeedState
(
	StateName,
	Description
)
VALUES 
(
	'Feed not found.',
	'The feed you supplied is no longer available. Please double-check the feed URL or wait to see if the site comes back online later.'
)
GO

INSERT INTO cs_FeedState
(
	StateName,
	Description
)
VALUES 
(
	'Unknown error.',
	'An error occurred while attempting to update this feed.'
)
GO

INSERT INTO cs_FeedState
(
	StateName,
	Description
)
VALUES 
(
	'Invalid format.',
	'The format of the feed is in an invalid format.'
)
GO

INSERT INTO cs_FeedState
(
	StateName,
	Description
)
VALUES
(
	'Feed cached.',
	'This feed is currently using an older, cached version due to problems reaching the site, currently.'
)
GO

INSERT INTO cs_FeedState
(
	StateName,
	Description
)
VALUES
(
	'Ambiguous redirect.',
	'This site returned multiple redirect choices for the main feed location requested.'
)
GO

Insert 	[dbo].[cs_EventDescriptions] Values(1000,'Search was updated')
Insert 	[dbo].[cs_EventDescriptions] Values(1001,'Email queue was cleared')
Insert 	[dbo].[cs_EventDescriptions] Values(200,'CS application started')
Insert 	[dbo].[cs_EventDescriptions] Values(201,'CS application was closed')
Insert 	[dbo].[cs_EventDescriptions] Values(202,'CS jobs windows service started')
Insert 	[dbo].[cs_EventDescriptions] Values(203,'CS jobs windows service stopped')
Insert 	[dbo].[cs_EventDescriptions] Values(204,'Section Created')
Insert 	[dbo].[cs_EventDescriptions] Values(300,'Trackback Received')
Insert 	[dbo].[cs_EventDescriptions] Values(301,'Trackback Sent')
Insert 	[dbo].[cs_EventDescriptions] Values(302,'Pingback Received')
Insert 	[dbo].[cs_EventDescriptions] Values(303,'Pingback Sent')
Insert 	[dbo].[cs_EventDescriptions] Values(304,'Weblog Update Ping Sent')
Insert 	[dbo].[cs_EventDescriptions] Values(305,'Cross-Post Attempt')
Insert 	[dbo].[cs_EventDescriptions] Values(601,'Email Queue')

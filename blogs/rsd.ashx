<%@ WebHandler Language="C#" Class="CommunityServer.Blogs.Components.RSD" %>

using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Xml;
using CommunityServer.Components;
using CommunityServer.Blogs.Components;

namespace CommunityServer.Blogs.Components
{
	public class RSD : IHttpHandler 
	{
	    
		public void ProcessRequest (HttpContext context) 
		{

			Weblog weblog = Weblogs.GetWeblog();
	        
			context.Response.ContentType = "text/xml";
			XmlTextWriter xml = new XmlTextWriter(context.Response.OutputStream, Encoding.UTF8);

			xml.WriteStartElement("rsd");
			xml.WriteAttributeString("version", "1.0");
	        
			//service
			xml.WriteStartElement("service");
			xml.WriteElementString("engineName", "Community Server");
			xml.WriteElementString("engineLink", "http://communityserver.org");
			xml.WriteElementString("homePageLink", Globals.FullPath(weblog.Url));

			xml.WriteStartElement("apis");
			xml.WriteStartElement("api");
				xml.WriteAttributeString("name", "MetaWeblog");
				xml.WriteAttributeString("blogID", weblog.ApplicationKey);
				xml.WriteAttributeString("preferred", "true");
				xml.WriteAttributeString("apiLink", Globals.FullPath(BlogUrls.Instance().MetaBlog(weblog.ApplicationKey)));
			xml.WriteEndElement();
	    
			xml.WriteEndElement();
	        
			//End service
			xml.WriteEndElement();

			xml.Close();
	        
		}
	 
		public bool IsReusable {
			get {
				return false;
			}
		}

	}
}
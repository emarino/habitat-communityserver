<%@ Page language="c#" Codebehind="Default.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Membership.MembershipHomePage" %>
<%@ Import Namespace = "CommunityServer.Components" %>
<cp:controlpanelselectednavigation selectednavitem="SearchMembers" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol runat="server" resourcename="CP_Membership_Default_Title" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonFormArea">

		<div class="CommonGroupedContentArea">
			<cp:usersearch id="UserSearchControl" destinationurl="MemberSearch.aspx" runat="Server">
			    <SkinTemplate>
                <script type="text/javascript">
                // <![CDATA[
                joinedDateValue = new Date();
                lastPostDateValue = new Date();
                
                function LastPostDateComparerChange()
                {
                    var dateComparerIndex = document.getElementById('<%=CommunityServer.Controls.CSControlUtility.Instance().FindControl(this, "lastPostDateComparer").ClientID%>').selectedIndex;
                    if (dateComparerIndex == 0)
                        document.getElementById('lastPostDateContainer').style.display = 'none';
                    else
                        document.getElementById('lastPostDateContainer').style.display = 'inline';
                }

                function JoinedDateComparerChange()
                {
                    var dateComparerIndex = document.getElementById('<%=CommunityServer.Controls.CSControlUtility.Instance().FindControl(this, "joinedDateComparer").ClientID%>').selectedIndex;
                    if (dateComparerIndex == 0)
                        document.getElementById('joinedDateContainer').style.display = 'none';
                    else
                        document.getElementById('joinedDateContainer').style.display = 'inline';
                }

                function disableJoinedDate(DropDownList) 
                {
	                joinedDateValue = <%= CommunityServer.Controls.CSControlUtility.Instance().FindControl(this, "joinedDate").ClientID %>.DateTime;
                }
                function disableLastPostDate(DropDownList) 
                {
		            lastPostDateValue = <%= CommunityServer.Controls.CSControlUtility.Instance().FindControl(this, "lastPostDate").ClientID %>.DateTime;
                }
                // ]]>
                </script>


                <table border="0" cellpadding="2" cellspacing="0">
                <tr>
	                <td align="right" class="CommonFormFieldName">
		                <cp:helpicon runat="Server" resourcename="UserSearchControl_Search_Desc" />
	                </td>
	                <td class="CommonFormField">
		                <asp:textbox id="searchText" runat="server" Columns=75 lass="shorttxt" maxlength="255"></asp:textbox>
	                </td>
	                <td align="right" class="CommonFormFieldName">
		                <asp:LinkButton id="searchButton" runat="server" cssclass="CommonTextButton" />
                    </td>
                </tr>
                </table>

                <p />

                <fieldset>
                    <legend><cp:resourcecontrol runat="server" resourcename="UserSearchControl_SortBy" id="Resourcecontrol14" /></legend>
                    <table border="0" cellpadding="2" cellspacing="0">
                        <tr>
	                        <td colspan="3" class="CommonFormField">
		                        <cp:membersortdropdownlist id="memberSortDropDown" runat="server" />
		                        <cp:sortorderdropdownlist id="sortOrderDropDown" runat="server" />
	                        </td>
                        </tr>
                    </table>
                </fieldset>

                <p />
                <fieldset>
                    <legend><cp:resourcecontrol ID="Resourcecontrol5" runat="server" resourcename="UserSearchControl_FilterByDate" /></legend>
                    <table border="0" cellpadding="2" cellspacing="0">
                        <tr>
	                        <td align="right" class="CommonFormFieldName"><cp:resourcecontrol ID="Resourcecontrol2" runat="server" resourcename="UserSearchControl_JoinedDate" /></td>
	                        <td class="CommonFormField" nowrap="nowrap">
		                        <asp:dropdownlist id="joinedDateComparer" runat="server" />
		                        <span id="joinedDateContainer">
		                            <TWC:DateTimeSelector runat="server" ID="joinedDate" DateTimeFormat="MMMM d yyyy" ShowCalendarPopup="true" />
		                        </span>
	                        </td>
	                      </tr>
	                      <tr>
	                        <td align="right" class="CommonFormFieldName"><cp:resourcecontrol ID="Resourcecontrol3" runat="server" resourcename="UserSearchControl_LastPostDate" /></td>
	                        <td class="CommonFormField" nowrap="nowrap">
		                        <asp:dropdownlist id="lastPostDateComparer" runat="server" />
		                        <span id="lastPostDateContainer">
		                            <TWC:DateTimeSelector runat="server" ID="lastPostDate" DateTimeFormat="MMMM d yyyy" ShowCalendarPopup="true" />
		                        </span>
	                        </td>
                        </tr>
                    </table>
                </fieldset>

                <p />
                <fieldset>
                    <legend><cp:resourcecontrol ID="Resourcecontrol4" runat="server" resourcename="UserSearchControl_FilterBy" /></legend>
                    <table border="0" cellpadding="2" cellspacing="0">
	                    <tr>
	                        <td align="right" class="CommonFormFieldName">
                                <cp:resourcecontrol ID="Resourcecontrol1" runat="server" resourcename="UserSearchControl_Role" />
                            </td>
	                        <td class="CommonFormField">
	                            <asp:dropdownlist id="searchRole" runat="server" />
	                        </td>
	                    </tr>
                        <tr id="AdminRow1" runat="server">
	                        <td align="right" class="CommonFormFieldName">
		                        <cp:helpicon runat="Server" resourcename="UserSearchControl_SearchFor_Desc" />
		                        <cp:resourcecontrol runat="server" resourcename="UserSearchControl_SearchFor" />
	                        </td>
	                        <td class="CommonFormField">
        		                <asp:dropdownlist id="searchType" runat="server" align="absmiddle" />
	                        </td>
	                    </tr>
	                    <tr>
	                        <td align="right" class="CommonFormFieldName">
		                        <cp:helpicon runat="Server" resourcename="UserSearchControl_Status_Desc" />
		                        <cp:resourcecontrol runat="server" resourcename="UserSearchControl_Status" />
	                        </td>
        	                <td class="CommonFormField"><cp:accountstatusdropdownlist enableshowall = "true" id="currentAccountStatus" runat="server" /></td>
                        </tr>
                    </table>
                </fieldset>
	        </SkinTemplate>
			</cp:usersearch>
		</div>

		<asp:panel id="UserAddedMessagePanel" runat="Server" visible="false">
			<div class="CommonGroupedContentArea">
			<cp:statusmessage runat="server" id="AddUserStatus" />
			<ul>
				<li><cp:hyperlink id="EditUserLink" runat="Server"></cp:hyperlink></li>
				<li><cp:hyperlink id="AddUserLink" runat="Server"></cp:hyperlink></li>
			</ul>
			</div>
		</asp:panel>

		<div class="CommonGroupedContentArea">
		    <fieldset>
		        <legend><cp:resourcecontrol runat="server" resourcename="CP_Membership_Default_QuickLists" id="Resourcecontrol2"/></legend>
			    <ul class="CommonSidebarList">
				    <li><CP:hyperlink id="linkTopPosters" runat="server" navigateurl="#" resourcename="CP_Membership_Default_LinkTopPosters"></CP:hyperlink></li>
				    <li><CP:hyperlink id="linkVisitors" runat="server" navigateurl="#" resourcename="CP_Membership_Default_LinkVisitors"></CP:hyperlink></li>
				    <li><CP:hyperlink id="linkRegistrations" runat="server" navigateurl="#" resourcename="CP_Membership_Default_LinkRegistrations"></CP:hyperlink></li>
				    <li><CP:hyperlink id="linkApproval" runat="server" navigateurl="#" resourcename="CP_Membership_Default_LinkApproval"></CP:hyperlink></li>
			    </ul>
		    </fieldset>
		</div>

		</div>

    <script type="text/javascript">
    // <![CDATA[
    document.getElementById('<%=CommunityServer.Controls.CSControlUtility.Instance().FindControl(this, "lastPostDateComparer").ClientID%>').onchange = LastPostDateComparerChange;
    document.getElementById('<%=CommunityServer.Controls.CSControlUtility.Instance().FindControl(this, "joinedDateComparer").ClientID%>').onchange = JoinedDateComparerChange;
    LastPostDateComparerChange();
    JoinedDateComparerChange();
    // ]]>
    </script>
	</CP:Content>
</CP:Container>

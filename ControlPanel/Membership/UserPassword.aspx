<%@ Page language="c#" Codebehind="UserPassword.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Membership.UserPassword" %>
<%@ Import Namespace="CommunityServer.Components" %>
<CP:Container runat="server" id="MPContainer" thememasterfile="ControlPanelModalMaster.ascx">
	<CP:Content id="bcr" runat="Server">
			<cp:changepassword runat="server" id="ContentControl" isModal="true">
			    <SkinTemplate>
                    <div class="CommonContentArea">
                    <div class="CommonContent">
                                <cp:ResourceLabel runat="server" ResourceName="ChangePassword_Title"/><asp:Label id="UserName" runat=server />
			                    <p />
                                <table cellpadding="3" cellspacing="0" border="0" width="100%">            
                                  <asp:Panel id="showCurrentPassword" runat="Server" visible="true">
                                  <tr>
                                    <td class="CommonFormFieldName">
					                    <cp:ResourceLabel runat="server" ResourceName="ChangePassword_CurrentPassword" />
                                    </td>
                                    <td class="CommonFormField">
                                      <asp:textbox TextMode="Password" runat="server" id="Password" Columns="40" MaxLength="64"/>
        					                    <asp:RegularExpressionValidator id="passwordRegExValidator" runat="server" ControlToValidate="Password" Display=Dynamic Cssclass="validationWarning">*</asp:RegularExpressionValidator>
                                      <asp:requiredfieldvalidator id="ValidatePassword" runat="server" ControlToValidate="Password" Display=Dynamic Cssclass="validationWarning">*</asp:requiredfieldvalidator>
                                    </td>
                                  </tr>
                                  </asp:Panel>
                                  <tr>
                                    <td class="CommonFormFieldName">
					                    <cp:ResourceLabel runat="server" ResourceName="ChangePassword_NewPassword" />
                                    </td>
                                    <td class="CommonFormField">
                                      <asp:textbox TextMode="Password" runat="server" id="NewPassword1" Columns="40" MaxLength="64"/>
                                      <asp:requiredfieldvalidator id="ValidatePassword1" runat="server" ControlToValidate="NewPassword1" Display=Dynamic Cssclass="validationWarning">*</asp:requiredfieldvalidator>
        					                    <asp:RegularExpressionValidator id="newPasswordRegExValidator" runat="server" ControlToValidate="NewPassword1" Display=Dynamic Cssclass="validationWarning">*</asp:RegularExpressionValidator>
                                      <asp:CustomValidator ControlToValidate="NewPassword1" id="newPasswordContentValidator" EnableClientScript=false runat="server" Display=Dynamic Cssclass="validationWarning">*</asp:CustomValidator>
                                    </td>        
                                  </tr>        
                                  <tr>
                                    <td class="CommonFormFieldName">
                                        <cp:ResourceLabel runat="server" ResourceName="ChangePassword_ReEnterNewPassword" />
                                    </td>
                                    <td class="CommonFormField">
                                      <asp:textbox TextMode="Password" runat="server" id="NewPassword2" Columns="40" MaxLength="64"/>
                                      <asp:requiredfieldvalidator id="ValidatePassword2" runat="server" ControlToValidate="NewPassword2" Display=Dynamic>*</asp:requiredfieldvalidator>                 
                                      <asp:comparevalidator id="ComparePassword" runat="server" ControlToValidate="NewPassword2" ControlToCompare="NewPassword1" Display=Dynamic>*</asp:comparevalidator>
                                    </td>        
                                  </tr>
                                  <tr>
                                    <td class="CommonFormField" colspan="2">
                                      <asp:ValidationSummary id="ValidationMsg" runat="server" DisplayMode="BulletList" Width="100%" CssClass="validationError" />               
                                      <asp:button id="ChangePasswordButton" runat="server" />
                                    </td>
                                  </tr>            
                                </table>

                    </div>
                    </div>

			    </SkinTemplate>
			</cp:changepassword>
	</CP:Content>
</CP:Container>

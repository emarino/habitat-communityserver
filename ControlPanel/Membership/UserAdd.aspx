<%@ Page language="c#" Codebehind="UserAdd.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Membership.UserAdd" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="Resourcecontrol1" runat="server" resourcename="CP_Membership_UserAdd_Title"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="FixedWidthContainer">
			<TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
				<TR>
					<TD align="left">
						<DIV class="CommonFormFieldName">
							<cp:helpicon id="Helpicon2" runat="Server" resourcename="CP_Membership_UserAdd_UserName_Desc"></cp:helpicon>
							<cp:resourcecontrol id="Resourcecontrol2" runat="server" resourcename="CP_Membership_UserAdd_UserName"></cp:resourcecontrol></DIV>
						<DIV class="CommonFormField">
							<asp:textbox id="username" runat="server" columns="40" maxlength="64"></asp:textbox>
							<asp:regularexpressionvalidator id="usernameRegExValidator" runat="server" cssclass="validationWarning" controltovalidate="Username"
								enableclientscript="false">*</asp:regularexpressionvalidator>
							<asp:requiredfieldvalidator id="usernameValidator" runat="server" cssclass="validationWarning" controltovalidate="Username"
								enableclientscript="false">*</asp:requiredfieldvalidator>
							<asp:requiredfieldvalidator id="placeHolderValidator" runat="server" cssclass="validationWarning" controltovalidate="Username"
								enableclientscript="false">*</asp:requiredfieldvalidator>
							<asp:customvalidator id="usernameLengthValidator" runat="server" controltovalidate="Username" enableclientscript="false">*</asp:customvalidator></DIV>
					</TD>
				</TR>
				<TR>
					<TD align="left">
						<DIV class="CommonFormFieldName">
							<cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_Membership_UserAdd_Password_Desc"></cp:helpicon>
							<cp:resourcecontrol id="Resourcecontrol3" runat="server" resourcename="CP_Membership_UserAdd_Password"></cp:resourcecontrol></DIV>
						<DIV class="CommonFormField">
							<asp:textbox id="password" runat="server" columns="40" maxlength="64" textmode="Password"></asp:textbox>
							<asp:requiredfieldvalidator id="passwordValidator" runat="server" cssclass="validationWarning" controltovalidate="Password">*</asp:requiredfieldvalidator>
							<asp:regularexpressionvalidator id="passwordRegExValidator" runat="server" cssclass="validationWarning" controltovalidate="Password"
								enableclientscript="false">*</asp:regularexpressionvalidator>
							<asp:customvalidator id="passwordContentValidator" runat="server" cssclass="validationWarning" controltovalidate="Password"
								enableclientscript="false" display="Dynamic">*</asp:customvalidator></DIV>
					</TD>
				</TR>
				<TR>
					<TD align="left">
						<DIV class="CommonFormFieldName">
							<cp:resourcecontrol id="Resourcecontrol4" runat="server" resourcename="CP_Membership_UserAdd_ReEnterPassword"></cp:resourcecontrol></DIV>
						<DIV class="CommonFormField">
							<asp:textbox id="password2" runat="server" columns="40" maxlength="64" textmode="Password"></asp:textbox>
							<asp:requiredfieldvalidator id="password2Validator" runat="server" cssclass="validationWarning" controltovalidate="Password2">*</asp:requiredfieldvalidator>
							<asp:comparevalidator id="comparePassword" runat="server" cssclass="validationWarning" controltovalidate="Password2"
								controltocompare="Password">*</asp:comparevalidator></DIV>
					</TD>
				</TR>
				<TR>
					<TD align="left">
						<DIV class="CommonFormFieldName">
							<cp:helpicon id="Helpicon4" runat="Server" resourcename="CP_Membership_UserAdd_Email_Desc"></cp:helpicon>
							<cp:resourcecontrol id="Resourcecontrol5" runat="server" resourcename="CP_Membership_UserAdd_Email"></cp:resourcecontrol></DIV>
						<DIV class="CommonFormField">
							<asp:textbox id="email" runat="server" columns="40" maxlength="128"></asp:textbox>
							<asp:requiredfieldvalidator id="emailValidator" runat="server" cssclass="validationWarning" controltovalidate="Email">*</asp:requiredfieldvalidator>
							<asp:regularexpressionvalidator id="emailRegExValidator" runat="server" cssclass="validationWarning" controltovalidate="Email"
								validationexpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:regularexpressionvalidator></DIV>
					</TD>
				</TR>
				<TR>
					<TD>
						<DIV class="CommonFormFieldName">
							<cp:helpicon id="Helpicon3" runat="Server" resourcename="CP_Membership_UserAdd_TimeZone_Desc"></cp:helpicon>
							<cp:resourcecontrol id="Resourcecontrol8" runat="server" resourcename="CP_Membership_UserAdd_TimeZone"></cp:resourcecontrol></DIV>
						<DIV class="CommonFormField">
							<cp:timezonedropdownlist id="timezone" runat="server"></cp:timezonedropdownlist></DIV>
					</TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldDescription" align="left">
						<asp:validationsummary id="validationSummary" runat="server"></asp:validationsummary></TD>
				</TR>
			</TABLE>
		</DIV>
		<p class="PanelSaveButton DetailsFixedWidth">
							<cp:resourcelinkbutton id="createButton" runat="server" resourcename="CP_Membership_UserAdd_CreateAccountButton"
								cssclass="CommonTextButton"></cp:resourcelinkbutton></p>
	</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="UserDelete.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Membership.UserDelete" %>
<%@ Import Namespace="CommunityServer.Components" %>
<CP:Container runat="server" id="MPContainer" thememasterfile="ControlPanelModalMaster.ascx">
	<CP:Content id="bcr" runat="Server">
		<cp:deleteuser runat="server" id="ContentControl" isModal="true">
		    <SkinTemplate>
		        <div class="CommonContentArea">
	                <div class="CommonContent">
		                <cp:StatusMessage runat="server" id="StatusMessage" />
		                <cp:ResourceLabel runat="server" ResourceName="DeleteUser_Instructions" />
		                <p />
		                <table cellpadding="3" cellspacing="0" border="0" width="300">
			                <tr>
				                <td align="left" class="CommonFormFieldName" nowrap="nowrap">
					                <cp:ResourceLabel runat="server" ResourceName="DeleteUser_DeleteUserName" />
				                </td>
				                <td align="left" class="CommonFormField" nowrap="nowrap">
					                <asp:Label id="DeleteUserName" runat=server />
				                </td>
			                </tr>
			                <tr>
				                <td align="left" class="CommonFormFieldName" nowrap="nowrap" colspan="2">
					                <cp:ResourceLabel runat="server" ResourceName="DeleteUser_ReassignUserName" />
					                <br />
					                &nbsp;&nbsp;&nbsp;<asp:RadioButton ID="AssignToUser" GroupName="who" Runat="server" Checked="True" /><asp:textbox id="ReassignUserName" runat="server" MaxLength="256" />
					                <br />
					                &nbsp;&nbsp;&nbsp;<asp:RadioButton ID="AssignToAnonymous" GroupName="who" runat="server" />
				                </td>
			                </tr>
			                <tr>
				                <td class="CommonFormFieldName" colspan="2">
					                <asp:CheckBox ID="UseAnonymousUser" />
				                </td>
			                </tr>
			                <tr>
				                <td class="CommonFormField" colspan="2">
					                <asp:button id="DeleteUserButton" runat="server" />
				                </td>
			                </tr>
		                </table>
	                </div>
                </div>
		    </SkinTemplate>
		</cp:deleteuser>
	</CP:Content>
</CP:Container>

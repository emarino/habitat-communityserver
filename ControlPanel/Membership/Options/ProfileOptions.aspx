<%@ Page language="c#" Codebehind="ProfileOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Membership.ProfileOptions" %>
<CP:ControlPanelSelectedNavigation runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="Resourcecontrol1" runat="server" resourcename="CP_Membership_Settings_Profile_Title"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
			<CP:statusmessage id="formStatus" runat="server"></CP:statusmessage>
		<div class="FixedWidthContainer">
			<TABLE cellSpacing="0" cellPadding="3" border="0">
				<TR>
					<TD class="CommonFormFieldName" width="100%">
						<cp:helpicon id="Helpicon2" runat="Server" resourcename="CP_Membership_Settings_AllowSignatures_Descr"></cp:helpicon>
						<cp:ResourceLabel id="Resourcelabel28" runat="server" NAME="Resourcelabel28" ResourceName="CP_Membership_Settings_AllowSignatures"></cp:ResourceLabel>
					</TD>
					<TD class="CommonFormField" noWrap width="400">
						<cp:YesNoRadioButtonList id="optAllowSignatures" runat="server" RepeatColumns="2" CssClass="shorttxt"></cp:YesNoRadioButtonList>
					</TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName" width="100%">
						<cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_Membership_Settings_EnableSignatures_Descr"></cp:helpicon>
						<cp:ResourceLabel id="Resourcelabel30" runat="server" NAME="Resourcelabel30" ResourceName="CP_Membership_Settings_EnableSignatures"></cp:ResourceLabel>
					</TD>
					<TD class="CommonFormField" noWrap width="400">
						<cp:YesNoRadioButtonList id="optEnableSignatures" runat="server" RepeatColumns="2" CssClass="shorttxt"></cp:YesNoRadioButtonList>
					</TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon3" runat="Server" resourcename="CP_Membership_Settings_SignatureMaxLength_Descr"></cp:helpicon>
						<cp:ResourceLabel id="Resourcelabel32" runat="server" NAME="Resourcelabel32" ResourceName="CP_Membership_Settings_SignatureMaxLength"></cp:ResourceLabel>
					</TD>
					<TD class="CommonFormField">
						<asp:TextBox id="txtSignatureMaxLength" runat="server" CssClass="shorttxt" MaxLength="4"></asp:TextBox>
						<asp:RegularExpressionValidator id="txtSignatureMaxLengthValidator" runat="server" ErrorMessage="*" Cssclass="validationWarning"
							ValidationExpression="[0-9]*" ControlToValidate="txtSignatureMaxLength"></asp:RegularExpressionValidator>
						<asp:RequiredFieldValidator id="txtSignatureMaxLengthValidator2" runat="server" ErrorMessage="*" Cssclass="validationWarning"
							ControlToValidate="txtSignatureMaxLength"></asp:RequiredFieldValidator>
					</TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName" width="100%">
						<cp:helpicon id="Helpicon4" runat="Server" resourcename="CP_Membership_Settings_AllowGender_Descr"></cp:helpicon>
						<cp:ResourceLabel id="Resourcelabel33" runat="server" NAME="Resourcelabel33" ResourceName="CP_Membership_Settings_AllowGender"></cp:ResourceLabel>
					</TD>
					<TD class="CommonFormField" noWrap width="400">
						<cp:YesNoRadioButtonList id="optAllowGender" runat="server" RepeatColumns="2" CssClass="shorttxt"></cp:YesNoRadioButtonList>
					</TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon5" runat="Server" resourcename="CP_Membership_Settings_EnableUserTheme_Descr"></cp:helpicon>
						<cp:ResourceLabel id="Resourcelabel35" runat="server" NAME="Resourcelabel35" ResourceName="CP_Membership_Settings_EnableUserTheme"></cp:ResourceLabel>
					</TD>
					<TD class="CommonFormField">
						<cp:YesNoRadioButtonList id="optAllowUserToSelectTheme" runat="server" RepeatColumns="2" CssClass="shorttxt"></cp:YesNoRadioButtonList>
					</TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon6" runat="Server" resourcename="CP_Membership_Settings_RequireAuthenticationForProfileViewing_Descr"></cp:helpicon>
						<cp:ResourceLabel id="Resourcelabel1" runat="server" NAME="Resourcelabel35" ResourceName="CP_Membership_Settings_RequireAuthenticationForProfileViewing"></cp:ResourceLabel>
					</TD>
					<TD class="CommonFormField">
						<cp:YesNoRadioButtonList id="optRequireAuthenticationForProfileViewing" runat="server" RepeatColumns="2" CssClass="shorttxt"></cp:YesNoRadioButtonList>
					</TD>
				</TR>
			</TABLE>
		</DIV>
		<p class="PanelSaveButton DetailsFixedWidth">
			<cp:ResourceLinkButton id="btnSave" runat="server" resourcename="Save" cssclass="CommonTextButton"></cp:ResourceLinkButton>
		</p>
	</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="RegistrationOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Membership.RegistrationOptions" %>
<CP:ControlPanelSelectedNavigation runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="Resourcecontrol1" runat="server" resourcename="CP_Membership_Settings_Registration_Title"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
			<CP:statusmessage id="formStatus" runat="server"></CP:statusmessage>
		<div class="FixedWidthContainer">
			<TABLE cellSpacing="0" cellPadding="3" border="0">
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_Membership_Settings_AllowLogin_Descr"></cp:helpicon>
						<cp:ResourceLabel id="Resourcelabel7" runat="server" NAME="Resourcelabel7" ResourceName="CP_Membership_Settings_AllowLogin"></cp:ResourceLabel></TD>
					<TD class="CommonFormField">
						<cp:YesNoRadioButtonList id="optAllowLogin" runat="server" RepeatColumns="2" CssClass="shorttxt"></cp:YesNoRadioButtonList></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon2" runat="Server" resourcename="CP_Membership_Settings_EnableBannedUsersToLogin_Descr"></cp:helpicon>
						<cp:ResourceLabel id="Resourcelabel1" runat="server" NAME="Resourcelabel21" ResourceName="CP_Membership_Settings_EnableBannedUsersToLogin"></cp:ResourceLabel></TD>
					<TD class="CommonFormField">
						<cp:YesNoRadioButtonList id="optEnableBannedUsersToLogin" runat="server" RepeatColumns="2" CssClass="shorttxt"></cp:YesNoRadioButtonList></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon3" runat="Server" resourcename="CP_Membership_Settings_AllowNew_Descr"></cp:helpicon>
						<cp:ResourceLabel id="Resourcelabel9" runat="server" NAME="Resourcelabel9" ResourceName="CP_Membership_Settings_AllowNew"></cp:ResourceLabel></TD>
					<TD class="CommonFormField">
						<cp:YesNoRadioButtonList id="optAllowNewUserRegistration" runat="server" RepeatColumns="2" CssClass="shorttxt"></cp:YesNoRadioButtonList></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon runat="Server" resourcename="CP_Membership_Settings_ShowContactCheckboxes_Descr"></cp:helpicon>
						<cp:ResourceLabel  runat="server" ResourceName="CP_Membership_Settings_ShowContactCheckboxes"></cp:ResourceLabel></TD>
					<TD class="CommonFormField">
						<cp:YesNoRadioButtonList id="optShowContactCheckboxes" runat="server" RepeatColumns="2" CssClass="shorttxt"></cp:YesNoRadioButtonList></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon4" runat="Server" resourcename="CP_Membership_Settings_NewUserModLevel_Descr"></cp:helpicon>
						<cp:ResourceLabel id="Resourcelabel11" runat="server" NAME="Resourcelabel11" ResourceName="CP_Membership_Settings_NewUserModLevel"></cp:ResourceLabel></TD>
					<TD class="CommonFormField">
						<cp:ModerationLevelDropDownList id="ddlNewUserModerationLevel" runat="server" CssClass="shorttxt"></cp:ModerationLevelDropDownList></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon5" runat="Server" resourcename="CP_Membership_Settings_UserNameRegEx_Descr"></cp:helpicon>
						<cp:ResourceLabel id="ResourceLabel2" runat="server" ResourceName="CP_Membership_Settings_UserNameRegEx"></cp:ResourceLabel></TD>
					<TD class="CommonFormField" noWrap>
						<asp:TextBox id="txtUserNameRegEx" runat="server" CssClass="shorttxt"></asp:TextBox>
						<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserNameRegEx"
							Font-Bold="True" ErrorMessage="*"></asp:RequiredFieldValidator></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon6" runat="Server" resourcename="CP_Membership_Settings_UserNameMinLength_Descr"></cp:helpicon>
						<cp:ResourceLabel id="Resourcelabel13" runat="server" NAME="Resourcelabel13" ResourceName="CP_Membership_Settings_UserNameMinLength"></cp:ResourceLabel></TD>
					<TD class="CommonFormField" noWrap>
						<asp:TextBox id="txtUsernameMinLength" runat="server" CssClass="shorttxt" MaxLength="3"></asp:TextBox>
						<asp:RequiredFieldValidator id="UsernameMinLengthValidator" runat="server" ControlToValidate="txtUsernameMinLength"
							Font-Bold="True" ErrorMessage="*"></asp:RequiredFieldValidator>
					    <asp:RegularExpressionValidator runat="server" ControlToValidate="txtUsernameMinLength" ErrorMessage="*" 
                                Display="Dynamic" ValidationExpression="^[0-9]*$" />
					</TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon7" runat="Server" resourcename="CP_Membership_Settings_UserNameMaxLength_Descr"></cp:helpicon>
						<cp:ResourceLabel id="Resourcelabel14" runat="server" NAME="Resourcelabel14" ResourceName="CP_Membership_Settings_UserNameMaxLength"></cp:ResourceLabel></TD>
					<TD class="CommonFormField">
						<asp:TextBox id="txtUsernameMaxLength" runat="server" CssClass="shorttxt" MaxLength="3"></asp:TextBox>
						<asp:RequiredFieldValidator id="UsernameMaxLengthValidator" runat="server" ControlToValidate="txtUsernameMaxLength"
							Font-Bold="True" ErrorMessage="*"></asp:RequiredFieldValidator>
					    <asp:RegularExpressionValidator runat="server" ControlToValidate="txtUsernameMaxLength" ErrorMessage="*" 
                                Display="Dynamic" ValidationExpression="^[0-9]*$" />
					</TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon8" runat="Server" resourcename="CP_Membership_Settings_AccountActivate_Descr"></cp:helpicon>
						<cp:ResourceLabel id="Resourcelabel17" runat="server" NAME="Resourcelabel17" ResourceName="CP_Membership_Settings_AccountActivate"></cp:ResourceLabel></TD>
					<TD class="CommonFormField" noWrap>
						<cp:AccountActivationRadioButtonList id="optAccountActivation" runat="server" RepeatColumns="2" CssClass="shorttxt"></cp:AccountActivationRadioButtonList></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon9" runat="Server" resourcename="CP_Membership_Settings_PasswordRecovery_Descr"></cp:helpicon>
						<cp:ResourceLabel id="Resourcelabel19" runat="server" NAME="Resourcelabel19" ResourceName="CP_Membership_Settings_PasswordRecovery"></cp:ResourceLabel></TD>
					<TD class="CommonFormField">
						<cp:PasswordRecoveryRadioButtonList id="optPasswordRecovery" runat="server" RepeatColumns="3"></cp:PasswordRecoveryRadioButtonList></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon10" runat="Server" resourcename="CP_Membership_Settings_PasswordRegEx_Descr"></cp:helpicon>
						<cp:ResourceLabel id="ResourceLabel3" runat="server" ResourceName="CP_Membership_Settings_PasswordRegEx"></cp:ResourceLabel></TD>
					<TD class="CommonFormField" noWrap>
						<asp:TextBox id="txtPasswordRegEx" runat="server" CssClass="shorttxt"></asp:TextBox></TD>
				</TR>
			</TABLE>
		</DIV>
		<p class="PanelSaveButton DetailsFixedWidth">
				<cp:ResourceLinkButton id="btnSave" runat="server" resourcename="Save" cssclass="CommonTextButton"></cp:ResourceLinkButton></p>
	</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="AvatarOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Membership.AvatarOptions" %>
<cp:controlpanelselectednavigation runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server"><cp:resourcecontrol runat="server" resourcename="CP_Membership_Settings_Avatar_Title" /></CP:Content>
<CP:Content id="TaskRegion" runat="Server">
	<cp:statusmessage id="formStatus" runat="server"></cp:statusmessage>
	<div class="FixedWidthContainer">
		<table cellspacing="0" cellpadding="3" border="0">
			<tr>
				<td class="CommonFormFieldName" width="100%">
					<cp:helpicon runat="Server" resourcename="CP_Membership_Settings_Avatar_EnableAvatar_Help" />
					<cp:resourcelabel runat="server" resourcename="CP_Membership_Settings_Avatar_EnableAvatar" />
				</td>
				<td class="CommonFormField" nowrap width="400">
					<cp:yesnoradiobuttonlist id="optEnableAvatars" runat="server" repeatcolumns="2" cssclass="shorttxt" />
				</td>
			</tr>
			<tr>
				<td class="CommonFormFieldName" width="100%">
					<cp:helpicon runat="Server" resourcename="CP_Membership_Settings_Avatar_EnableRemoteAvatar_Help" />
					<cp:resourcelabel runat="server" resourcename="CP_Membership_Settings_Avatar_EnableRemoteAvatar" />
				</td>
				<td class="CommonFormField" nowrap width="400">
					<cp:yesnoradiobuttonlist id="optEnableRemoteAvatars" runat="server" repeatcolumns="2" cssclass="shorttxt" />
				</td>
			</tr>
			<tr>
				<td class="CommonFormFieldName">
					<cp:helpicon runat="Server" resourcename="CP_Membership_Settings_Avatar_AvatarSize_Help" />
					<cp:resourcelabel runat="server" resourcename="CP_Membership_Settings_Avatar_AvatarSize" />
				</td>
				<td class="CommonFormField">
				    <asp:textbox id="txtAvatarWidth" columns="1" maxlength="3" runat="server" cssclass="shorttxt" />
				    x
					<asp:textbox id="txtAvatarHeight" columns="1" maxlength="3" runat="server" cssclass="shorttxt" />
					<asp:requiredfieldvalidator id="AvatarHeightValidator" runat="server" controltovalidate="txtAvatarHeight" cssclass="validationWarning" errormessage="*" />
					<asp:regularexpressionvalidator id="AvatarHeightRegExValidator" runat="server" controltovalidate="txtAvatarHeight" validationexpression="[0-9]*" cssclass="validationWarning" errormessage="*" />
					<asp:requiredfieldvalidator id="AvatarWidthValidator" runat="server" controltovalidate="txtAvatarWidth" cssclass="validationWarning" errormessage="*" />
					<asp:regularexpressionvalidator id="AvatarWidthRegExValidator" runat="server" controltovalidate="txtAvatarWidth" validationexpression="[0-9]*" cssclass="validationWarning" errormessage="*" />
				</td>
			</tr>
		</table>
	</DIV>
	<p class="PanelSaveButton DetailsFixedWidth">
		<cp:resourcelinkbutton id="btnSave" runat="server" resourcename="Save" cssclass="CommonTextButton" />
	</p>
</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="PostingOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Membership.PostingOptions" %>
<CP:ControlPanelSelectedNavigation runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="SetupControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
	<cp:resourcecontrol runat="server" resourcename="CP_Membership_Settings_MemberList_Title" />
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">
	<CP:statusmessage id="formStatus" runat="server"></CP:statusmessage>
	<DIV class="CommonDescription">
		<cp:resourcecontrol runat="server" resourcename="CP_Membership_Settings_MemberList_SubTitle" />
	</DIV>
	<div class="FixedWidthContainer">
		<TABLE cellSpacing="0" cellPadding="3" border="0">
		<TR>
			<TD class="CommonFormFieldName">
				<cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_Membership_Settings_List_Display_Descr" />
				<cp:resourcecontrol id="ResourceControl60" runat="server" resourcename="CP_Membership_Settings_List_Display" />
			</TD>
			<TD class="CommonFormField" noWrap>
				<cp:yesnoradiobuttonlist id="PublicMemberList" runat="server" repeatcolumns="2" cssclass="shorttxt" />
			</TD>
		</TR>
		<TR>
			<TD class="CommonFormFieldName">
				<cp:helpicon id="Helpicon3" runat="Server" resourcename="CP_Membership_Settings_List_Searching_Descr" />
				<cp:resourcecontrol id="ResourceControl62" runat="server" resourcename="CP_Membership_Settings_List_Searching" />
			</TD>
			<TD class="CommonFormField">
				<cp:yesnoradiobuttonlist id="MemberListAdvancedSearch" runat="server" repeatcolumns="2" cssclass="shorttxt" />
			</TD>
		</TR>
		<TR>
			<TD class="CommonFormFieldName">
				<cp:helpicon id="Helpicon6" runat="Server" resourcename="CP_Membership_Settings_EnableUserModerationCounters_Descr" />
				<cp:ResourceLabel id="Resourcelabel3" runat="server" resourcename="CP_Membership_Settings_EnableUserModerationCounters" />
			</TD>
			<TD class="CommonFormField">
				<cp:yesnoradiobuttonlist id="optEnableUserModerationCounters" runat="server" repeatcolumns="2" cssclass="shorttxt" />
			</TD>
		</TR>
		<TR>
			<TD class="CommonFormFieldName">
				<cp:helpicon id="Helpicon4" runat="Server" resourcename="CP_Membership_Settings_List_MembersPerPage_Descr" />
				<cp:resourcecontrol id="ResourceControl64" runat="server" resourcename="CP_Membership_Settings_List_MembersPerPage" />
			</TD>
			<TD class="CommonFormField">
				<asp:textbox id="MemberListPageSize" runat="server" cssclass="shorttxt" maxlength="4"></asp:textbox>
				<asp:requiredfieldvalidator id="MemberListPageSizeValidator" runat="server" controltovalidate="MemberListPageSize" font-bold="True" errormessage="*" />
				<asp:RegularExpressionValidator runat="server" ControlToValidate="MemberListPageSize" ErrorMessage="*" 
                                Display="Dynamic" ValidationExpression="^[0-9]*$" />
			</TD>
		</TR>
		</TABLE>
	</DIV>
	<p class="PanelSaveButton DetailsFixedWidth">
		<cp:ResourceLinkButton id="btnSave" runat="server" resourcename="Save" cssclass="CommonTextButton" />
	</p>
</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="SecurityOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Membership.SecurityOptions" %>
<CP:ControlPanelSelectedNavigation runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="Resourcecontrol1" runat="server" resourcename="CP_Membership_Settings_Security_Title"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<CP:statusmessage id="formStatus" runat="server"></CP:statusmessage>
		<DIV class="CommonDescription">
			<cp:resourcecontrol id="ResourceControl115" runat="server" resourcename="CP_Membership_Settings_Cookie_SubTitle"></cp:resourcecontrol></DIV>
		<div class="FixedWidthContainer">
			<TABLE cellSpacing="0" cellPadding="3" border="0">
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon2" runat="Server" resourcename="CP_Membership_Settings_Cookie_Enabled_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Membership_Settings_Cookie_Enabled" /></TD>
					<TD class="CommonFormField" noWrap>
						<cp:yesnoradiobuttonlist id="EnableRolesCookie" runat="server" repeatcolumns="2" cssclass="shorttxt"></cp:yesnoradiobuttonlist></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_Membership_Settings_Cookie_Track_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Membership_Settings_Cookie_Track" /></TD>
					<TD class="CommonFormField">
						<cp:yesnoradiobuttonlist id="EnableAnonymousTracking" runat="server" repeatcolumns="2" cssclass="shorttxt"></cp:yesnoradiobuttonlist></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon3" runat="Server" resourcename="CP_Membership_Settings_Cookie_Name_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Membership_Settings_Cookie_Name" /></TD>
					<TD class="CommonFormField">
						<asp:textbox id="RolesCookieName" runat="server" cssclass="shorttxt" maxlength="64"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator1" runat="server" controltovalidate="RolesCookieName"
							font-bold="True" errormessage="*"></asp:requiredfieldvalidator></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon4" runat="Server" resourcename="CP_Membership_Settings_Cookie_Expire_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Membership_Settings_Cookie_Expire" /></TD>
					<TD class="CommonFormField">
						<asp:textbox id="RolesCookieExpiration" runat="server" cssclass="shorttxt" maxlength="6"></asp:textbox>
						<asp:requiredfieldvalidator id="RolesCookieExpirationValidator" runat="server" controltovalidate="RolesCookieExpiration"
							font-bold="True" errormessage="*"></asp:requiredfieldvalidator>
					    <asp:RegularExpressionValidator runat="server" ControlToValidate="RolesCookieExpiration" ErrorMessage="*" 
                                Display="Dynamic" ValidationExpression="^[0-9]*$" />
					</TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon5" runat="Server" resourcename="CP_Membership_Settings_Cookie_Anon_Name_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Membership_Settings_Cookie_Anon_Name" /></TD>
					<TD class="CommonFormField">
						<asp:textbox id="AnonymousCookieName" runat="server" cssclass="shorttxt" maxlength="64"></asp:textbox>
						<asp:requiredfieldvalidator id="Requiredfieldvalidator2" runat="server" controltovalidate="AnonymousCookieName"
							font-bold="True" errormessage="*"></asp:requiredfieldvalidator></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon6" runat="Server" resourcename="CP_Membership_Settings_Cookie_Anon_Expire_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Membership_Settings_Cookie_Anon_Expire" /></TD>
					<TD class="CommonFormField">
						<asp:textbox id="AnonymousCookieExpiration" runat="server" cssclass="shorttxt" maxlength="6"></asp:textbox>
						<asp:requiredfieldvalidator id="AnonymousCookieExpirationValidator" runat="server" controltovalidate="AnonymousCookieExpiration"
							font-bold="True" errormessage="*"></asp:requiredfieldvalidator>
					    <asp:RegularExpressionValidator runat="server" ControlToValidate="AnonymousCookieExpiration" ErrorMessage="*" 
                                Display="Dynamic" ValidationExpression="^[0-9]*$" />
					</TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon7" runat="Server" resourcename="CP_Membership_Settings_Cookie_Domain_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Membership_Settings_Cookie_Domain" /></TD>
					<TD class="CommonFormField">
						<asp:textbox id="CookieDomain" runat="server" cssclass="shorttxt" maxlength="100"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon8" runat="Server" resourcename="CP_Membership_Settings_Anon_Window_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Membership_Settings_Anon_Window" /></TD>
					<TD class="CommonFormField">
						<asp:textbox id="AnonymousUserOnlineTimeWindow" runat="server" cssclass="shorttxt" maxlength="64"></asp:textbox>
						<asp:requiredfieldvalidator id="AnonymousUserOnlineTimeWindowValidator" runat="server" controltovalidate="AnonymousUserOnlineTimeWindow"
							font-bold="True" errormessage="*"></asp:requiredfieldvalidator>
					    <asp:RegularExpressionValidator runat="server" ControlToValidate="AnonymousUserOnlineTimeWindow" ErrorMessage="*" 
                                Display="Dynamic" ValidationExpression="^[0-9]*$" />
					</TD>
				</TR>
			</TABLE>
		</div>
		<p class="PanelSaveButton DetailsFixedWidth">
				<cp:ResourceLinkButton id="btnSave" runat="server" resourcename="Save" cssclass="CommonTextButton"></cp:ResourceLinkButton></p>
	</CP:Content>
</CP:Container>

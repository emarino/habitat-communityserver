<%@ Page language="c#" Codebehind="UserName.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Membership.UserName" %>
<%@ Import Namespace="CommunityServer.Components" %>
<CP:Container runat="server" id="MPContainer" thememasterfile="ControlPanelModalMaster.ascx">
	<CP:Content id="bcr" runat="Server">
			<cp:changeusername runat="server" id="ContentControl" isModal="true">
			    <SkinTemplate>
                    <div class="CommonContentArea">
	                    <div class="CommonContent">
		                    <cp:StatusMessage runat="server" id="StatusMessage" />
		                    <cp:ResourceLabel runat="server" ResourceName="ChangeUserName_Instructions" />
		                    <p />
		                    <table cellpadding="3" cellspacing="0" border="0" width="300">
			                    <tr>
				                    <td align="left" class="CommonFormFieldName" nowrap="nowrap">
					                    <cp:ResourceLabel runat="server" ResourceName="ChangeUserName_CurrentUserName" />
				                    </td>
				                    <td align="left" class="CommonFormField" nowrap="nowrap">
					                    <asp:Label id="CurrentUserName" runat=server />
				                    </td>
			                    </tr>
			                    <tr>
				                    <td align="left" class="CommonFormFieldName" nowrap="nowrap">
					                    <cp:ResourceLabel runat="server" ResourceName="ChangeUserName_DesiredUserName" />
				                    </td>
				                    <td align="left" class="CommonFormField" nowrap="nowrap">
					                    <asp:textbox id="DesiredUserName" runat="server" MaxLength="256" />
				                    </td>
			                    </tr>
			                    <tr>
				                    <td class="CommonFormFieldName" colspan="2">
					                    <asp:CheckBox ID="IgnoreDisallowedNames" Runat="server" />
				                    </td>
			                    </tr>
			                    <tr>
				                    <td class="CommonFormField" colspan="2">
					                    <asp:button id="ChangeUserNameButton" runat="server" />
				                    </td>
			                    </tr>
		                    </table>
	                    </div>
                    </div>
			    </SkinTemplate>
			</cp:changeusername>
	</CP:Content>
</CP:Container>

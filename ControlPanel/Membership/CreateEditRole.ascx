<%@ Control CodeBehind="CreateEditRole.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Membership.CreateEditRole" %>
<%@ Import Namespace = "CommunityServer.Components" %>
<div class="CommonFormArea">
<h3 class="CommonSubTitle"><asp:literal runat="server" id="Title"/></h3>
<table cellpadding="3" cellspacing="0" border="0">
<tr>
	<td class="CommonFormFieldName" valign="top">
		<cp:resourcelabel runat="server" resourcename="ID" id="IDResourceLabel"/>
	</td>
	<td class="CommonFormField">
		<asp:literal id="RoleID" runat="server" />
    </td>
</tr>
<tr>
	<td class="CommonFormFieldName" valign="top">
		<cp:resourcelabel runat="server" resourcename="Name" id="Resourcelabel3"/>
	</td>
	<td class="CommonFormField">
        <asp:textbox id="Name" runat="server" maxlength="256" width="300" />
        <asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="Name" id="Requiredfieldvalidator1"/>
    </td>
</tr>
<tr>
	<td class="CommonFormFieldName" valign="top">
		<cp:resourcelabel runat="server" resourcename="Description" id="DescResourceLabel"/>
	</td>
	<td class="CommonFormField">
		<asp:textbox id="Description" columns="40" runat="server" maxlength="512" rows="5" textmode="MultiLine"/>
    </td>
</tr>
<tr>
	<td colspan="2" valign="top">
		<div class="CommonFormFieldName">
			<cp:resourcelabel runat="server" resourcename="CP_Membership_RoleEdit_RoleIcon" id="RoleIconResourceLabel"/>
		</div>
		<table border="0" cellpadding="2" cellspacing="0">
		<tr>
			<td rowspan="2" class="CommonFormFieldName">
				<cp:roleicons id="RoleIcon" runat="server" EnableLink="false"></cp:roleicons>
			</td>
			<td class="CommonFormField">
				<div class="CommonFormFieldName">
					<cp:resourcecontrol runat="server" resourcename="CP_Membership_RoleEdit_UploadRoleIcon" id="Resourcecontrol32"/>
				</div>
				<div class="CommonFormField">
					<input type="file" runat="server" id="RoleImageUpload" name="RoleImageUpload"/>
					<br />
					<cp:resourcelinkbutton id="DeleteImageButton" runat="server" cssclass="CommonTextButton" resourcename="CP_Membership_RoleEdit_DeleteRoleIcon"></cp:resourcelinkbutton>
				</div>
			</td>
		</tr>
		</table>	
	</td>
</tr>
<tr>
	<td colspan="2" class="CommonFormField" align="right" >
		<div class="PanelSaveButton">
			<cp:resourcelinkbutton id="SaveButton" runat="server" cssclass="CommonTextButton" resourcename="Save"></cp:resourcelinkbutton>
			<cp:resourcelinkbutton id="DeleteButton" runat="server" cssclass="CommonTextButton" resourcename="Delete"></cp:resourcelinkbutton>
		</div>
	</td>
</tr>
</table>
</div>
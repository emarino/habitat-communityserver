<%@ Page language="c#" Codebehind="MemberSearch.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Membership.MemberSearch" %>
<%@ Import Namespace = "CommunityServer.Components" %>
<cp:controlpanelselectednavigation selectednavitem="SearchMembers" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="SetupControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
	<cp:resourcecontrol runat="server" resourcename="CP_MemberSearch_Title" />
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">
<cp:contextmenu id="MemberContextMenu" runat="server" filename="~/ControlPanel/Membership/MemberSearchContextMenu.config">
    <SkinTemplate>
        <CA:Menu runat="server" 
	        id="Menu" 
	        AutoPostBackOnSelect="false" 
	        ContextMenu="Custom" 
	        Orientation="Vertical"
	        CssClass="CommonContextMenuGroup"
	        DefaultGroupCssClass="CommonContextMenuGroup"
	        DefaultItemLookID="DefaultItemLook" >
		        <ItemLooks>
			        <CA:ItemLook LookID="DefaultItemLook" 
				        CssClass="CommonContextMenuItem" 
				        HoverCssClass="CommonContextMenuItemHover" 
				        ExpandedCssClass="CommonContextMenuItemExpanded" 
				        LabelPaddingLeft="5" 
				        LabelPaddingRight="15" 
				        LabelPaddingTop="2" 
				        LabelPaddingBottom="2" />
		        </ItemLooks>	
        </CA:Menu>
    </SkinTemplate>
</cp:contextmenu>
<SCRIPT type="text/javascript">
<!--
function popupMenu(item, e, UserID, SearchPostsUrl)
{
window.<%=MemberContextMenu.ClientID%>.ShowContextMenu(item, e, UserID, SearchPostsUrl);
}
function refresh()
{
    window.location.replace( sUrl );
}
var actionsName = '<cp:resourcecontrol runat="server" resourcename="CP_MemberSearch_GridCol_Actions" />';
var sUrl = unescape(window.location.pathname);
-->
</SCRIPT>

<div class="CommonFormArea">
	<cp:usersearch id="UserSearchControl" runat="Server" destinationurl="MemberSearch.aspx">
	    <SkinTemplate>
                <script type="text/javascript">
                // <![CDATA[
                joinedDateValue = new Date();
                lastPostDateValue = new Date();
                
                function LastPostDateComparerChange()
                {
                    var dateComparerIndex = document.getElementById('<%=CommunityServer.Controls.CSControlUtility.Instance().FindControl(this, "lastPostDateComparer").ClientID%>').selectedIndex;
                    if (dateComparerIndex == 0)
                        document.getElementById('lastPostDateContainer').style.display = 'none';
                    else
                        document.getElementById('lastPostDateContainer').style.display = 'inline';
                }

                function JoinedDateComparerChange()
                {
                    var dateComparerIndex = document.getElementById('<%=CommunityServer.Controls.CSControlUtility.Instance().FindControl(this, "joinedDateComparer").ClientID%>').selectedIndex;
                    if (dateComparerIndex == 0)
                        document.getElementById('joinedDateContainer').style.display = 'none';
                    else
                        document.getElementById('joinedDateContainer').style.display = 'inline';
                }

                function disableJoinedDate(DropDownList) 
                {
	                joinedDateValue = <%= CommunityServer.Controls.CSControlUtility.Instance().FindControl(this, "joinedDate").ClientID %>.DateTime;
                }
                function disableLastPostDate(DropDownList) 
                {
		            lastPostDateValue = <%= CommunityServer.Controls.CSControlUtility.Instance().FindControl(this, "lastPostDate").ClientID %>.DateTime;
                }
                // ]]>
                </script>


                <table border="0" cellpadding="2" cellspacing="0">
                <tr>
	                <td align="right" class="CommonFormFieldName">
		                <cp:helpicon runat="Server" resourcename="UserSearchControl_Search_Desc" />
	                </td>
	                <td class="CommonFormField">
		                <asp:textbox id="searchText" runat="server" Columns=75 lass="shorttxt" maxlength="255"></asp:textbox>
	                </td>
	                <td align="right" class="CommonFormFieldName">
		                <asp:LinkButton id="searchButton" runat="server" cssclass="CommonTextButton" />
                    </td>
                </tr>
                </table>

                <p />

                <fieldset>
                    <legend><cp:resourcecontrol runat="server" resourcename="UserSearchControl_SortBy" id="Resourcecontrol14" /></legend>
                    <table border="0" cellpadding="2" cellspacing="0">
                        <tr>
	                        <td colspan="3" class="CommonFormField">
		                        <cp:membersortdropdownlist id="memberSortDropDown" runat="server" />
		                        <cp:sortorderdropdownlist id="sortOrderDropDown" runat="server" />
	                        </td>
                        </tr>
                    </table>
                </fieldset>

                <p />
                <fieldset>
                    <legend><cp:resourcecontrol ID="Resourcecontrol5" runat="server" resourcename="UserSearchControl_FilterByDate" /></legend>
                    <table border="0" cellpadding="2" cellspacing="0">
                        <tr>
	                        <td align="right" class="CommonFormFieldName"><cp:resourcecontrol ID="Resourcecontrol2" runat="server" resourcename="UserSearchControl_JoinedDate" /></td>
	                        <td class="CommonFormField" nowrap="nowrap">
		                        <asp:dropdownlist id="joinedDateComparer" runat="server" />
		                        <span id="joinedDateContainer">
		                            <TWC:DateTimeSelector runat="server" ID="joinedDate" DateTimeFormat="MMMM d yyyy" ShowCalendarPopup="true" />
		                        </span>
	                      </tr>
	                      <tr>
	                        <td align="right" class="CommonFormFieldName"><cp:resourcecontrol ID="Resourcecontrol3" runat="server" resourcename="UserSearchControl_LastPostDate" /></td>
	                        <td class="CommonFormField" nowrap="nowrap">
		                        <asp:dropdownlist id="lastPostDateComparer" runat="server" />
		                        <span id="lastPostDateContainer">
		                            <TWC:DateTimeSelector runat="server" ID="lastPostDate" DateTimeFormat="MMMM d yyyy" ShowCalendarPopup="true" />
		                        </span>
	                        </td>
                        </tr>
                    </table>
                </fieldset>

                <p />
                <fieldset>
                    <legend><cp:resourcecontrol ID="Resourcecontrol4" runat="server" resourcename="UserSearchControl_FilterBy" /></legend>
                    <table border="0" cellpadding="2" cellspacing="0">
	                    <tr>
	                        <td align="right" class="CommonFormFieldName">
                                <cp:resourcecontrol ID="Resourcecontrol1" runat="server" resourcename="UserSearchControl_Role" />
                            </td>
	                        <td class="CommonFormField">
	                            <asp:dropdownlist id="searchRole" runat="server" />
	                        </td>
	                    </tr>
                        <tr id="AdminRow1" runat="server">
	                        <td align="right" class="CommonFormFieldName">
		                        <cp:helpicon runat="Server" resourcename="UserSearchControl_SearchFor_Desc" />
		                        <cp:resourcecontrol runat="server" resourcename="UserSearchControl_SearchFor" />
	                        </td>
	                        <td class="CommonFormField">
        		                <asp:dropdownlist id="searchType" runat="server" align="absmiddle" />
	                        </td>
	                    </tr>
	                    <tr>
	                        <td align="right" class="CommonFormFieldName">
		                        <cp:helpicon runat="Server" resourcename="UserSearchControl_Status_Desc" />
		                        <cp:resourcecontrol runat="server" resourcename="UserSearchControl_Status" />
	                        </td>
        	                <td class="CommonFormField"><cp:accountstatusdropdownlist enableshowall = "true" id="currentAccountStatus" runat="server" /></td>
                        </tr>
                    </table>
                </fieldset>
	        </SkinTemplate>
	</cp:usersearch>
</div>

<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
<TR>
	<TD width="100%">
		<cp:resourcelabel id="DescResourceLabel" runat="server" resourcename="CP_Membership_Roles_ItemsPerPage"></cp:resourcelabel>&nbsp; 
		<asp:dropdownlist id="PageList" runat="Server" AutoPostBack="True">
			<asp:listitem value="10">10</asp:listitem>
			<asp:listitem value="20">20</asp:listitem>
			<asp:listitem value="30">30</asp:listitem>
			<asp:listitem value="40">40</asp:listitem>
			<asp:listitem value="50">50</asp:listitem>
		</asp:dropdownlist>
	</TD>
</TR>
</TABLE><BR>
<DIV id="GridWithMenu">
<DIV id="GrayGrid">
<CA:grid id="Grid1" runat="Server" cssclass="Grid" runningmode="Callback" showheader="false" showsearchbox="false" searchtextcssclass="GridHeaderText" searchonkeypress="true" headercssclass="GridHeader" footercssclass="GridFooter" groupbycssclass="GroupByCell" groupbytextcssclass="GroupByText" pagerstyle="Slider" pagertextcssclass="GridFooterText" pagerbuttonwidth="41" pagerbuttonheight="22" sliderheight="20" sliderwidth="150" slidergripwidth="9" sliderpopupoffsetx="20" groupingpagesize="5" preexpandongroup="true" imagesbaseurl="~/controlpanel/images/caimages" pagerimagesfolderurl="~/controlpanel/images/caimages/pager" treelineimagesfolderurl="~/controlpanel/images/caimages/lines" treelineimagewidth="22" treelineimageheight="19" indentcellwidth="22" groupingnotificationtextcssclass="GridHeaderText" groupbysortascendingimageurl="group_asc.gif" groupbysortdescendingimageurl="group_desc.gif" groupbysortimagewidth="10" groupbysortimageheight="10" loadingpanelclienttemplateid="Loading" loadingpanelposition="MiddleCenter" AllowEditing="false" AllowMultipleSelect="false" EditOnClickSelectedItem="false" ManualPaging="true">
	<levels>
		<CA:gridlevel datakeyfield="UserID" showtableheading="false" showselectorcells="false" rowcssclass="Row" columnreorderindicatorimageurl="reorder.gif" datacellcssclass="DataCell" headingcellcssclass="HeadingCell" headingcellhovercssclass="HeadingCellHover" headingcellactivecssclass="HeadingCellActive" headingrowcssclass="HeadingRow" headingtextcssclass="HeadingCellText" selectedrowcssclass="SelectedRow" groupheadingcssclass="GroupHeading" sortascendingimageurl="asc.gif" sortdescendingimageurl="desc.gif" sortimagewidth="10" sortimageheight="19" editcellcssclass="EditDataCell" editfieldcssclass="EditDataField" editcommandclienttemplateid="EditCommandTemplate" insertcommandclienttemplateid="InsertCommandTemplate" alternatingrowcssclass="AlternatingRow" allowsorting="true" allowreordering = "false" allowgrouping="false">
			<columns>
				<CA:gridcolumn width="150" datafield="Username" headingtext="ResourceManager.CP_MemberSearch_GridCol_Username" datacellclienttemplateid = "UsernameTemplate" />
				<CA:gridcolumn width="170" datafield="Email" headingtext="ResourceManager.CP_MemberSearch_GridCol_Email" datacellclienttemplateid = "EmailTemplate" />
				<CA:gridcolumn width="50" datafield="TotalPosts" headingtext="ResourceManager.CP_MemberSearch_GridCol_Posts" datacellclienttemplateid = "PostsTemplate"/>
				<CA:gridcolumn width="70" datafield="DateCreated" headingtext="ResourceManager.CP_MemberSearch_GridCol_Joined" FormatString="M/d/yyyy" />
				<CA:gridcolumn width="70" datafield="LastActivity" headingtext="ResourceManager.CP_MemberSearch_GridCol_LastActive" formatstring="M/d/yyyy" />
				<CA:gridcolumn width="70" headingtext="ResourceManager.CP_MemberSearch_GridCol_Actions" allowsorting="false" datacellclienttemplateid = "ActionsTemplate" />
				<CA:gridcolumn datafield="UserID" visible = "false" allowsorting="false" />
				<CA:gridcolumn datafield="SearchPostsUrl" visible = "false" allowsorting="false" />
			</columns>
		</CA:gridlevel>
	</levels>
	<clienttemplates>
		<CA:clienttemplate id="Loading">
			<img src = "../images/caimages/spinner.gif" /><strong>Loading</strong>...please wait.
		</CA:clienttemplate>
		<CA:clienttemplate id="UsernameTemplate">
			<a href="UserEdit.aspx?UserID=## DataItem.GetMember("UserID").Text ##">## DataItem.GetMember("Username").Text ##</a>
		</CA:clienttemplate>
		<CA:clienttemplate id="EmailTemplate">
			<a href="mailto:## DataItem.GetMember("Email").Text ##">## DataItem.GetMember("Email").Text ##</a>
		</CA:clienttemplate>
		<CA:clienttemplate id="PostsTemplate">
			<a href="## DataItem.GetMember("SearchPostsUrl").Text ##">## DataItem.GetMember("TotalPosts").Text ##</a>
		</CA:clienttemplate>		
		<CA:clienttemplate id="ActionsTemplate">
			<a href="#" oncontextmenu="popupMenu(this, event, '## DataItem.GetMember("UserID").Text ##', '## DataItem.GetMember("SearchPostsUrl").Text ##');" onclick="popupMenu(this, event, '## DataItem.GetMember("UserID").Text ##', '## DataItem.GetMember("SearchPostsUrl").Text ##'); return false" class="CommonTextButton">##actionsName##</a>
		</CA:clienttemplate>
	</clienttemplates>
</CA:grid>
</DIV>
</DIV>

<script type="text/javascript">
// <![CDATA[
document.getElementById('<%=CommunityServer.Controls.CSControlUtility.Instance().FindControl(this, "lastPostDateComparer").ClientID%>').onchange = LastPostDateComparerChange;
document.getElementById('<%=CommunityServer.Controls.CSControlUtility.Instance().FindControl(this, "joinedDateComparer").ClientID%>').onchange = JoinedDateComparerChange;
LastPostDateComparerChange();
JoinedDateComparerChange();
// ]]>
</script>

</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="RoleEdit.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Membership.RoleEdit" %>
<%@ Register TagPrefix="CP" TagName = "CreateEditRole" Src = "~/ControlPanel/Membership/CreateEditRole.ascx" %>
<%@ Import Namespace="CommunityServer.Components" %>
<CP:Container runat="server" id="MPContainer" thememasterfile="ControlPanelModalMaster.ascx">
	<CP:Content id="bcr" runat="Server">
		<CP:createeditrole id="CreateEditRole1" runat="Server"></CP:createeditrole>
	</CP:Content>
</CP:Container>

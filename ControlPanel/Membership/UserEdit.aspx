<%@ Import Namespace="CommunityServer.ControlPanel.Controls" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Page language="c#" Codebehind="UserEdit.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Membership.UserEdit" %>
<cp:controlpanelselectednavigation selectednavitem="SearchMembers" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<asp:literal id="PanelDescription" runat="server"></asp:literal>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV>
			<CP:statusmessage id="anonymousWarning" runat="server"></CP:statusmessage>
			<CP:statusmessage id="formStatus" runat="server"></CP:statusmessage>
		</DIV>
		<TWC:TabbedPanes id="UserEditTabs" runat="server"
		PanesCssClass="CommonPane"
		TabSetCssClass="CommonPaneTabSet"
		TabCssClasses="CommonPaneTab,CommonPaneTab1,CommonPaneTab2"
		TabSelectedCssClasses="CommonPaneTabSelected,CommonPaneTabSelected1,CommonPaneTabSelected2"
		TabHoverCssClasses="CommonPaneTabHover,CommonPaneTabHover1,CommonPaneTabHover2"
		>
		<TWC:TabbedPane runat="server">
			<Tab><CP:ResourceControl runat="server" ResourceName="CP_UserEdit_Tab_Admin" /></Tab>
			<Content>
				<table cellpadding="2" cellspacing="0" runat="server" id="UserSettingsTable">
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<CP:ResourceLabel runat="server" resourcename="UserAdmin_UserID" id="Resourcelabel2" />
						</td>
						<td class="CommonFormField">
							<asp:literal id="userID" runat="server" />
						</td>
					</tr>
					<tr id="isLockedOutRow" runat="server" visible="false">
						<td class="CommonFormFieldName" width="45%">
							<font color="#FF0000">
								<CP:ResourceLabel runat="server" resourcename="UserAdmin_IsLockedOut" id="Resourcelabel9" />
							</font>
						</td>
						<td class="CommonFormField">
							<cp:yesnoradiobuttonlist cssclass="txt1" id="unlockUser" runat="server" repeatdirection="Horizontal" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<cp:resourcelabel runat="server" resourcename="CP_UserEdit_UserName" />
						</td>
						<td class="CommonFormField">
							<a runat="server" id="changeUserName" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<cp:resourcelabel runat="server" resourcename="EditProfile_Password" id="Resourcelabel15" />
						</td>
						<td class="CommonFormField">
							<a runat="server" id="changePassword" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<cp:resourcelabel runat="server" resourcename="CP_UserEdit_Roles" id="Resourcelabel52" />
						</td>
						<td class="CommonFormField">
							<a runat="server" id="changeRoles" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<CP:ResourceLabel runat="server" resourcename="UserAdmin_ForceLogin" id="Resourcelabel6" />
						</td>
						<td class="CommonFormField">
							<cp:yesnoradiobuttonlist cssclass="txt1" id="forceLogin" runat="server" repeatdirection="Horizontal" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<CP:ResourceLabel runat="server" resourcename="UserAdmin_IsIgnored" id="Resourcelabel29" />
						</td>
						<td class="CommonFormField">
							<cp:yesnoradiobuttonlist cssclass="txt1" id="isIgnored" runat="server" repeatdirection="Horizontal" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<CP:ResourceLabel runat="server" resourcename="UserAdmin_AccountStatus" id="Resourcelabel7" />
						</td>
						<td class="CommonFormField">
							<cp:accountstatusdropdownlist cssclass="txt1" id="accountStatus" runat="server" AutoPostBack="true" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<CP:ResourceLabel runat="server" resourcename="UserAdmin_ModerationLevel" id="Resourcelabel8" />
						</td>
						<td class="CommonFormField">
							<cp:moderationleveldropdownlist cssclass="txt1" id="moderationLevel" runat="server" repeatdirection="Horizontal" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<CP:ResourceLabel runat="server" resourcename="UserAdmin_DummyTotalPosts" id="Resourcelabel1" />
						</td>
						<td class="CommonFormField">
							<asp:textbox id="dummyTotalPosts" size="10" runat="server" maxlength="10" />
						</td>
					</tr>
				</table>
			</Content>
		</TWC:TabbedPane>
		<TWC:TabbedPane runat="server">
			<Tab><CP:ResourceControl runat="server" ResourceName="CP_UserEdit_Tab_AboutUser" /></Tab>
			<Content>
				<table cellpadding="3" cellspacing="0" runat="server" id="AboutUserTable">
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<CP:ResourceLabel runat="server" resourcename="EditProfile_AboutUserName" id="Resourcelabel18" />
						</td>
						<td class="CommonFormField">
							<asp:textbox id="usernameEdit" runat="server" />
							<asp:requiredfieldvalidator id="UsernameEditValidator" runat="server" controltovalidate="UsernameEdit" enableclientscript="false" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<CP:ResourceLabel runat="server" resourcename="EditProfile_DateCreated" id="Resourcelabel19" />
						</td>
						<td class="CommonFormField">
							<asp:literal id="dateCreated" runat="server" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<CP:ResourceLabel runat="server" resourcename="EditProfile_LastLogin" id="Resourcelabel20" />
						</td>
						<td class="CommonFormField">
							<asp:literal id="lastLogin" runat="server" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<CP:ResourceLabel runat="server" resourcename="EditProfile_LastActivity" id="Resourcelabel21" />
						</td>
						<td class="CommonFormField">
							<asp:literal id="lastActivity" runat="server" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<cp:resourcecontrol runat="server" resourcename="EditProfile_CommonName" id="ResourceControl2" name="ResourceControl2" />
						</td>
						<td class="CommonFormField">
							<asp:textbox id="CommonName" size="30" maxlength="50" runat="server" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<CP:ResourceLabel runat="server" resourcename="EditProfile_AboutLocation" id="Resourcelabel22" />
						</td>
						<td class="CommonFormField">
							<asp:textbox id="location" size="50" runat="server" maxlength="50" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<CP:ResourceLabel runat="server" resourcename="EditProfile_AboutOccupation" id="Resourcelabel23" />
						</td>
						<td class="CommonFormField">
							<asp:textbox id="occupation" size="50" runat="server" maxlength="50" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<CP:ResourceLabel runat="server" resourcename="EditProfile_AboutInterests" id="Resourcelabel24" />
						</td>
						<td class="CommonFormField">
							<asp:textbox id="interests" size="50" runat="server" maxlength="50" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName">
							<cp:ResourceControl runat="server" ResourceName="EditProfile_Birthday" id="Resourcecontrol22"/>
						</td>
						<td class="CommonFormField">
							<asp:RadioButton GroupName="Birthday" ID="enableBirthdayRadioButton" runat="server" /><TWC:DateTimeSelector runat="server" id="birthdayDateTimeSelector" DateTimeFormat="MMMM d, yyyy" ShowCalendarPopup="true" /><br />
						    <asp:RadioButton GroupName="Birthday" ID="disableBirthdayRadioButton" runat="server" Text="Not Set" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<CP:ResourceLabel runat="server" resourcename="EditProfile_AboutGender" id="Resourcelabel25" />
						</td>
						<td class="CommonFormField">
							<cp:genderradiobuttonlist cssclass="txt1" id="gender" repeatcolumns="3" runat="server" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<CP:ResourceLabel runat="server" resourcename="EditProfile_AboutWebAddress" id="Resourcelabel26" />
						</td>
						<td class="CommonFormField">
							<asp:textbox id="webAddress" size="50" runat="server" maxlength="255" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<CP:ResourceLabel runat="server" resourcename="EditProfile_AboutWebLog" id="Resourcelabel27" />
						</td>
						<td class="CommonFormField">
							<asp:textbox id="webLog" size="50" runat="server" maxlength="255" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<CP:ResourceLabel runat="server" resourcename="EditProfile_AboutWebGallery" id="Resourcelabel51" />
						</td>
						<td class="CommonFormField">
							<asp:textbox id="webGallery" size="50" runat="server" maxlength="255" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%" valign="top">
							<CP:ResourceLabel runat="server" resourcename="EditProfile_AboutSignature" id="Resourcelabel28" />
						</td>
						<td class="CommonFormField" valign="top">
							<asp:textbox id="signature" textmode="MultiLine" columns="50" rows="4" runat="server" />
							<asp:rangevalidator controltovalidate="signature" id="signatureMaxLengthValidator" enableclientscript="false"
								runat="server" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%" valign="top">
							Bio
						</td>
						<td>
							<asp:textbox id="bio" textmode="MultiLine" columns="50" rows="4" runat="server" />
						</td>
					</tr>
				</table>
			</Content>
		</TWC:TabbedPane>
		<TWC:TabbedPane runat="server">
			<Tab><CP:ResourceControl runat="server" ResourceName="CP_UserEdit_Tab_Avatar" /></Tab>
			<Content>
				<table cellpadding="3" cellspacing="0" runat="server" id="AvatarSection">
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<CP:ResourceLabel runat="server" resourcename="ViewUserProfile_Avatar" id="Resourcelabel36" />
						</td>
						<td class="CommonFormField">
							<cp:useravatar OverrideUser="true" visible="False" id="avatar" runat="server" padimage="False" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<CP:ResourceLabel runat="server" resourcename="UserAdmin_IsAvatarApproved" id="Resourcelabel5" />
						</td>
						<td class="CommonFormField">
							<cp:yesnoradiobuttonlist cssclass="txt1" id="isAvatarApproved" runat="server" repeatdirection="Horizontal" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<CP:ResourceLabel runat="server" resourcename="EditProfile_AvatarEnable" id="Resourcelabel37" />
						</td>
						<td class="CommonFormField">
							<cp:yesnoradiobuttonlist cssclass="txt1" id="enableAvatar" runat="server" repeatdirection="Horizontal" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<CP:ResourceLabel runat="server" resourcename="EditProfile_UploadAvatar" id="Resourcelabel38" />
						</td>
						<td class="CommonFormField">
							<input type="file" runat="server" id="uploadedAvatar" name="uploadedAvatar" />
							<asp:button id="avatarUpdateButton" runat="server" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<CP:ResourceLabel runat="server" resourcename="EditProfile_AvatarUrl" id="Resourcelabel39" />
						</td>
						<td class="CommonFormField">
							<asp:textbox id="avatarUrl" size="50" runat="server" maxlength="256" />
						</td>
					</tr>
				</table>
			</Content>
		</TWC:TabbedPane>
		<TWC:TabbedPane runat="server">
			<Tab><CP:ResourceControl runat="server" ResourceName="CP_UserEdit_Tab_SiteOptions" /></Tab>
			<Content>
				<table cellpadding="3" cellspacing="0" runat="server" id="ForumOptionsTable">
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<cp:resourcelabel runat="server" resourcename="EditProfile_OptionsLanguage" id="Resourcelabel43" />
						</td>
						<td class="CommonFormField">
							<cp:languagedropdownlist id="language" runat="server" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<cp:resourcelabel runat="server" resourcename="EditProfile_AboutTimeZone" id="Resourcelabel41" />
						</td>
						<td class="CommonFormField">
							<cp:timezonedropdownlist id="timezone" runat="server" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<cp:resourcelabel runat="server" resourcename="EditProfile_OptionsDateFormat" id="Resourcelabel44" />
						</td>
						<td class="CommonFormField">
							<cp:dateformatdropdownlist id="dateFormat" runat="server" />
						</td>
					</tr>
					<tr id="ThreadSortOptions" runat="server">
						<td class="CommonFormFieldName" width="45%">
							<cp:resourcelabel runat="server" resourcename="EditProfile_OptionsSortOrder" id="Resourcelabel17"
								name="Resourcelabel17" />
						</td>
						<td class="CommonFormField">
							<cp:sortorderdropdownlist cssclass="txt1" id="sortOrder" runat="server" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<cp:resourcelabel runat="server" resourcename="EditProfile_Theme" id="Resourcelabel42" />
						</td>
						<td class="CommonFormField">
							<cp:themedropdownlist id="theme" runat="server" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<cp:resourcelabel runat="server" resourcename="EditProfile_FontSize" id="Resourcelabel35" />
						</td>
						<td class="CommonFormField">
							<cp:fontsizedropdownlist id="FontSize" runat="server" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<cp:resourcelabel runat="server" resourcename="EditProfile_Editor" id="Resourcelabel40" />
						</td>
						<td class="CommonFormField">
							<asp:dropdownlist id="EditorList" runat="server" />
						</td>
					</tr>
					<tr runat="server" visible = "<%# CSContext.Current.SiteSettings.EnableInk %>">
						<td class="CommonFormFieldName">
							<cp:resourcecontrol runat="server" resourcename="EditProfile_Ink" id="Resourcecontrol51" />
						</td>
						<td class="CommonFormField">
							<cp:yesnoradiobuttonlist cssclass="txt1" id="enableInk" runat="server" repeatcolumns="2" />
						</td>
					</tr>
					<tr runat="server" visible = "<%# CSContext.Current.SiteSettings.EnableDisplayNames %>">
						<td class="CommonFormFieldName">
							<cp:resourcecontrol runat="server" resourcename="EditProfile_DisplayName" id="Resourcecontrol17" />
						</td>
						<td class="CommonFormField">
							<cp:yesnoradiobuttonlist cssclass="txt1" id="enableDisplayNames" runat="server" repeatcolumns="2" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<cp:resourcelabel runat="server" resourcename="EditProfile_DisplayInMemberList" id="Resourcelabel45" />
						</td>
						<td class="CommonFormField">
							<cp:yesnoradiobuttonlist cssclass="txt1" id="displayInMemberList" runat="server" repeatdirection="Horizontal" />
						</td>
					</tr>
					<tr id="EnablePostPreviewPopupRow" runat="server">
						<td class="CommonFormFieldName" width="45%">
							<cp:resourcelabel runat="server" resourcename="Admin_SiteSettings_EnablePostPreviewPopup" id="Resourcelabel46" />
						</td>
						<td class="CommonFormField">
							<cp:yesnoradiobuttonlist cssclass="txt1" id="enablePostPreviewPopup" runat="server" repeatdirection="Horizontal" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<cp:resourcelabel runat="server" resourcename="Admin_SiteSettings_EnableCollapsingPanels" id="Resourcelabel47" />
						</td>
						<td class="CommonFormField">
							<cp:yesnoradiobuttonlist cssclass="txt1" id="EnableCollapsingPanels" runat="server" repeatcolumns="2" />
						</td>
					</tr>
					<tr id="EmoticonRow" runat="Server">
						<td class="CommonFormFieldName" width="45%">
							<cp:resourcelabel runat="server" resourcename="Admin_SiteSettings_EnableEmoticons" id="Resourcelabel14"
								name="Resourcelabel14" />
						</td>
						<td class="CommonFormField">
							<cp:yesnoradiobuttonlist cssclass="txt1" id="enableEmoticons" runat="server" repeatcolumns="2" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName">
							<cp:resourcecontrol runat="server" resourcename="EditProfile_EnableUserAvatars" id="Resourcecontrol18" />
						</td>
						<td class="CommonFormField" nowrap="nowrap">
							<cp:yesnoradiobuttonlist cssclass="txt1" id="enableUserAvtars" runat="server" repeatcolumns="2" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName">
							<cp:resourcecontrol runat="server" resourcename="EditProfile_EnableUserSignatures" id="Resourcecontrol19" />
						</td>
						<td class="CommonFormField" nowrap="nowrap">
							<cp:yesnoradiobuttonlist cssclass="txt1" id="enableUserSignatures" runat="server" repeatcolumns="2" />
						</td>
					</tr>
				</table>
			</Content>
		</TWC:TabbedPane>
		<TWC:TabbedPane runat="server">
			<Tab><CP:ResourceControl runat="server" ResourceName="CP_UserEdit_Tab_EmailContactInfo" /></Tab>
			<Content>	
				<div class="CommonGroupedContentArea">
					<h3 class="CommonSubTitle">
						<cp:resourcecontrol runat="server" resourcename="EditProfile_PrivateTitle" id="Resourcecontrol4" /></h3>
					<table cellpadding="2" cellspacing="0" border="0">
						<tr>
							<td class="CommonFormFieldName" width="45%">
								<cp:resourcelabel runat="server" resourcename="EditProfile_ContactPrivateEmail" id="Resourcelabel16" />
							</td>
							<td class="CommonFormField">
								<asp:textbox id="privateEmail" size="50" runat="server" maxlength="50" />
								<asp:requiredfieldvalidator runat="server" cssclass="validationWarning" controltovalidate="privateEmail">*</asp:requiredfieldvalidator>
							    <asp:regularexpressionvalidator runat="server" cssclass="validationWarning" controltovalidate="privateEmail"
								    validationexpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">Email not in correct format.</asp:regularexpressionvalidator>
							</td>
						</tr>
					</table>
				</div>
				<div class="CommonGroupedContentArea">
					<h3 class="CommonSubTitle">
						<cp:resourcecontrol runat="server" resourcename="EditProfile_EmailTitle" id="Resourcecontrol1" /></h3>
					<table cellpadding="2" cellspacing="0" border="0">
						<tr>
							<td class="CommonFormFieldName" width="45%">
								<CP:ResourceLabel runat="server" resourcename="EditProfile_OptionsReceiveEmails" id="Resourcelabel48" />
							</td>
							<td class="CommonFormField">
								<cp:yesnoradiobuttonlist cssclass="txt1" id="enableEmail" runat="server" repeatdirection="Horizontal" />
							</td>
						</tr>
						<tr>
							<td class="CommonFormFieldName" width="45%">
								<CP:ResourceLabel runat="server" resourcename="EditProfile_OptionsHtmlEmail" id="Resourcelabel49" />
							</td>
							<td class="CommonFormField">
								<cp:yesnoradiobuttonlist cssclass="txt1" id="enableHtmlEmail" runat="server" repeatdirection="Horizontal" />
							</td>
						</tr>
						<tr>
							<td class="CommonFormFieldName" width="45%">
								<CP:ResourceLabel runat="server" resourcename="EditProfile_OptionsEmailTracking" id="Resourcelabel50" />
							</td>
							<td class="CommonFormField">
								<cp:yesnoradiobuttonlist cssclass="txt1" id="enableEmailTracking" runat="server" repeatdirection="Horizontal" />
							</td>
						</tr>
					</table>
				</div>
				<div class="CommonGroupedContentArea">
					<h3 class="CommonSubTitle">
						<cp:resourcecontrol runat="server" resourcename="EditProfile_ContactTitle" id="Resourcecontrol3" /></h3>
					<table cellpadding="3" cellspacing="0">
						<tr>
							<td class="CommonFormFieldName" width="45%">
								<cp:resourcelabel runat="server" resourcename="EditProfile_ContactPublicEmail" id="Resourcelabel30" />
							</td>
							<td class="CommonFormField">
								<asp:textbox id="publicEmail" size="50" runat="server" maxlength="50" />
								<asp:regularexpressionvalidator runat="server" cssclass="validationWarning" controltovalidate="publicEmail"
								    validationexpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">Email not in correct format.</asp:regularexpressionvalidator>
							</td>
						</tr>
						<tr>
							<td class="CommonFormFieldName" width="45%">
								<cp:resourcelabel runat="server" resourcename="EditProfile_ContactMsn" id="Resourcelabel31" />
							</td>
							<td class="CommonFormField">
								<asp:textbox id="msnIM" size="50" runat="server" maxlength="50" />
							</td>
						</tr>
						<tr>
							<td class="CommonFormFieldName" width="45%">
								<cp:resourcelabel runat="server" resourcename="EditProfile_ContactAol" id="Resourcelabel32" />
							</td>
							<td class="CommonFormField">
								<asp:textbox id="aolIM" size="50" runat="server" maxlength="50" />
							</td>
						</tr>
						<tr>
							<td class="CommonFormFieldName" width="45%">
								<cp:resourcelabel runat="server" resourcename="EditProfile_ContactYahoo" id="Resourcelabel33" />
							</td>
							<td class="CommonFormField">
								<asp:textbox id="yahooIM" size="50" runat="server" maxlength="50" />
							</td>
						</tr>
						<tr>
							<td class="CommonFormFieldName" width="45%">
								<cp:resourcelabel runat="server" resourcename="EditProfile_ContactIcq" id="Resourcelabel34" />
							</td>
							<td class="CommonFormField">
								<asp:textbox id="icq" size="50" runat="server" maxlength="50" />
							</td>
						</tr>
					</table>
				</div>
			</Content>
		</TWC:TabbedPane>
		<TWC:TabbedPane runat="server">
			<Tab><CP:ResourceControl runat="server" ResourceName="CP_UserEdit_Tab_Ban" /></Tab>
			<Content>
				<table cellpadding="3" cellspacing="0" visible="false" runat="server" id="userBanSection">
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<cp:resourcelabel runat="server" resourcename="EditProfile_UserBanPeriod" id="Resourcelabel10" />
						</td>
						<td class="CommonFormField">
							<cp:userbandropdownlist cssclass="txt1" id="userBanPeriod" runat="server" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<cp:resourcelabel runat="server" resourcename="EditProfile_UserBanReason" id="Resourcelabel4" />
						</td>
						<td class="CommonFormField">
							<cp:userbanreasondropdownlist cssclass="txt1" id="userBanReason" runat="server" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<cp:resourcelabel runat="server" resourcename="EditProfile_BanedUntilDate" id="Resourcelabel11" />
						</td>
						<td class="CommonFormField">
							<asp:label id="bannedUntilDate" runat="server" />
						</td>
					</tr>
				</table>
				<table cellpadding="3" cellspacing="0" visible="false" runat="server" id="userApprovalSection">
					<tr>
						<td class="column" colspan="2">
							<cp:resourcelabel runat="server" resourcename="EditProfile_UserApprovalTitle" id="Resourcelabel12" />
						</td>
					</tr>
					<tr>
						<td class="CommonFormFieldName" width="45%">
							<cp:resourcelabel runat="server" resourcename="EditProfile_UserApproval" id="Resourcelabel13" />
						</td>
						<td class="CommonFormField">
							<cp:yesnoradiobuttonlist cssclass="txt1" id="isApproved" runat="server" repeatdirection="Horizontal" />
						</td>
					</tr>
				</table>
			</Content>
		</TWC:TabbedPane>
		</TWC:TabbedPanes>
		<p class="PanelSaveButton DetailsFixedWidth">
			<cp:resourcelinkbutton id="updateButton" runat="server" cssclass="CommonTextButton" resourcename="EditProfile_Update"></cp:resourcelinkbutton></p>
	</CP:Content>
</CP:Container>

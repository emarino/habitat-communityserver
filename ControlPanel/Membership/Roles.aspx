<%@ Page language="c#" Codebehind="Roles.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Membership.Roles" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="SetupControlPanelMaster.ascx">
<CP:Content id=DescriptionRegion runat="server">
<cp:resourcelabel id=Resourcelabel1 runat="server" resourcename="CP_Membership_Roles_Title"></cp:resourcelabel></CP:Content>
<CP:Content id=TaskRegion runat="Server">
<script language = "javascript" type="text/javascript">
function reloadRoles(res) {
    
    if(res)
    {
	    refresh();
	}
}

function onCallbackError(excString) {
	if (confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_CallBackWarning") %>')) alert(excString); 
	<%= Grid1.ClientID %>.Page(1); 
}
</script>
	<table cellpadding="0" cellspacing="0" border="0"  width="100%">
		<tr>
			<td width="100%" >
				<cp:resourcelabel id="DescResourceLabel" runat="server" resourcename="CP_Membership_Roles_ItemsPerPage"></cp:resourcelabel>&nbsp;
				<asp:dropdownlist id="PageList" runat="Server" AutoPostBack="True">
					<asp:listitem value="10">10</asp:listitem>
					<asp:listitem value="20">20</asp:listitem>
					<asp:listitem value="30">30</asp:listitem>
					<asp:listitem value="40">40</asp:listitem>
					<asp:listitem value="50">50</asp:listitem>
				</asp:dropdownlist>
			</td>
			<td nowrap="nowrap">
				<cp:ModalLink CssClass="CommonTextButton" ModalType="Link" Height="300" Width="450" runat="Server" ResourceName="CP_Membership_Roles_AddRole" Callback="reloadRoles" id="AddRole" />
			</td>
		</tr>
	</table>
<br />
<DIV id=GrayGrid>
<CA:grid id="Grid1" runat="Server" cssclass="Grid" loadingpanelposition="MiddleCenter" 
	loadingpanelclienttemplateid="Loading" groupbysortimageheight="10" groupbysortimagewidth="10" 
	groupbysortdescendingimageurl="group_desc.gif" groupbysortascendingimageurl="group_asc.gif" 
	groupingnotificationtextcssclass="GridHeaderText" indentcellwidth="22"  ClientSideOnCallbackError="onCallbackError"
	treelineimageheight="19" treelineimagewidth="22" treelineimagesfolderurl="~/controlpanel/images/caimages/lines" 
	pagerimagesfolderurl="~/controlpanel/images/caimages/pager" imagesbaseurl="~/controlpanel/images/caimages" 
	preexpandongroup="true" groupingpagesize="5" sliderpopupoffsetx="20" slidergripwidth="9" sliderwidth="150" 
	sliderheight="20" pagerbuttonheight="22" pagerbuttonwidth="41" pagertextcssclass="GridFooterText" 
	pagerstyle="Slider" groupbytextcssclass="GroupByText" groupbycssclass="GroupByCell" 
	footercssclass="GridFooter" headercssclass="GridHeader" searchonkeypress="true"
	searchtextcssclass="GridHeaderText" showsearchbox="false" showheader="false" runningmode="Client">
		<levels>
			<CA:gridlevel datakeyfield="RoleID" showtableheading="false" showselectorcells="false" rowcssclass="Row" columnreorderindicatorimageurl="reorder.gif" datacellcssclass="DataCell" headingcellcssclass="HeadingCell" headingcellhovercssclass="HeadingCellHover" headingcellactivecssclass="HeadingCellActive" headingrowcssclass="HeadingRow" headingtextcssclass="HeadingCellText" selectedrowcssclass="SelectedRow" groupheadingcssclass="GroupHeading" sortascendingimageurl="asc.gif" sortdescendingimageurl="desc.gif" sortimagewidth="10" sortimageheight="19" editcellcssclass="EditDataCell" editfieldcssclass="EditDataField" editcommandclienttemplateid="EditCommandTemplate" insertcommandclienttemplateid="InsertCommandTemplate" alternatingrowcssclass="AlternatingRow" allowsorting="true" allowreordering = "false" allowgrouping="false">
				<columns>
					<CA:gridcolumn width="150" datafield="Name" headingtext="ResourceManager.CP_Membership_Roles_GridCol_Name" />
					<CA:gridcolumn width="350" datafield="Description" headingtext="ResourceManager.CP_Membership_Roles_GridCol_Description" />
					<CA:gridcolumn width="100" datafield="RoleID" headingtext="ResourceManager.CP_Membership_Roles_GridCol_Members" allowsorting="false" />
				</columns>
			</CA:gridlevel>
		</levels>
		<clienttemplates>
			<CA:clienttemplate id="Loading">
				<img src = "../images/caimages/spinner.gif" /><strong>Loading</strong>...please wait.
			</CA:clienttemplate>
		</clienttemplates>
	</CA:grid></DIV>
</CP:Content>
</CP:Container>

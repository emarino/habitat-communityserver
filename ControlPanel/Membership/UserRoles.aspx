<%@ Page language="c#" Codebehind="UserRoles.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Membership.UserRoles" %>
<CP:Container runat="server" id="MPContainer" thememasterfile="ControlPanelModalMaster.ascx">

	<CP:Content id="AdditionalScripts" runat="server" >
	
		<script type="text/javascript">
		// <![CDATA[
			function deleteOption(object,index) {
				object.options[index] = null;
			}

			function addOption(object,text,value) {
				var defaultSelected = true;
				var selected = true;
				var optionName = new Option(text, value, defaultSelected, selected)
				object.options[object.length] = optionName;
			}

			function copySelected(fromObject,toObject) {
				for (var i=0, l=fromObject.options.length;i<l;i++) {
					if (fromObject.options[i].selected)
						addOption(toObject,fromObject.options[i].text,fromObject.options[i].value);
				}
				for (var i=fromObject.options.length-1;i>-1;i--) {
					if (fromObject.options[i].selected)
						deleteOption(fromObject,i);
				}
				setValues();
			}

			function copyAll(fromObject,toObject) {
				for (var i=0, l=fromObject.options.length;i<l;i++) {
					addOption(toObject,fromObject.options[i].text,fromObject.options[i].value);
				}
				for (var i=fromObject.options.length-1;i>-1;i--) {
					deleteOption(fromObject,i);
				}
				setValues();
			}
			
			function setValues() {
				for (var indx = 0; indx < document.getElementById('<%=this.listAssignedRoles.ClientID %>').options.length;indx++)
				{
					if (indx!=0)
					{
						document.getElementById('<%= this.Page.Form.ClientID %>').AssignedValues.value += ',' + document.getElementById('<%=this.listAssignedRoles.ClientID %>').options[indx].value;
					}
					else
					{
						document.getElementById('<%= this.Page.Form.ClientID %>').AssignedValues.value = document.getElementById('<%=this.listAssignedRoles.ClientID %>').options[indx].value;
					}
				}
				document.getElementById('<%= this.Page.Form.ClientID %>').ChangesMade.value = 'true';
			}
		// ]]>	
		</script>

	</CP:Content>
	
	<CP:Content id="bcr" runat="Server">

	<div class="CommonContentArea">
	<div class="CommonContent">
		<input type="hidden" name="AssignedValues" id="AssignedValues" value="" />
		<input type="hidden" name="ChangesMade" id="ChangesMade" value="" />
		<table cellspacing="0" cellpadding="1" border="0">
        <tr>
            <td class="CommonFormFieldName">Available roles</td>
			<td></td>
            <td class="CommonFormFieldName">Roles <asp:label id="lblUserName" runat="server" /> is in</td>
		</tr>
		<tr>
			<td class="CommonFormField">
				<select id="listAvailableRoles" name="listAvailableRoles" runat="server" class="RolesListBox" multiple="true" size="30" />
			</td>
			<td height="100%">
				<table height="100%" cols="1" cellpadding="0" width="100%">
				<tr>
					<td valign="middle">
						<p><input type="button" value=" > " onclick="if (document.images) copySelected(<%=this.listAvailableRoles.ClientID %>,<%=this.listAssignedRoles.ClientID %>)"></p>
						<p><input type="button" value=" < " onclick="if (document.images) copySelected(<%=this.listAssignedRoles.ClientID %>,<%=this.listAvailableRoles.ClientID %>)"></p>
						<p><input type="button" value=">>" onclick="if (document.images) copyAll(<%=this.listAvailableRoles.ClientID %>,<%=this.listAssignedRoles.ClientID %>)"></p>
						<p><input type="button" value="<<" onclick="if (document.images) copyAll(<%=this.listAssignedRoles.ClientID %>,<%=this.listAvailableRoles.ClientID %>)"></p>
					</td>
				</tr>
				</table>
			</td>
			<td class="CommonFormField">
				<select id="listAssignedRoles" name="listAssignedRoles" runat="server" class="RolesListBox" multiple="true" size="30" />
			</td>
		</tr>
		<tr>
			<td colspan="3" align="right" class="CommonFormField">
				<div class="PanelSaveButton">
					<cp:resourcelinkbutton id="btnSave" runat="server" cssclass="CommonTextButton" resourcename="Save"></cp:resourcelinkbutton>
				</div>
			</td>
		</tr>
        </table>
      </div>
      </div>

	</CP:Content>
</CP:Container>

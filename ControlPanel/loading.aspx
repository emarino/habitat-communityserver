<%@ Page language="c#" AutoEventWireup="true" EnableViewState="false" %>
<%@ Import Namespace="CommunityServer.Components" %>
<script language="C#" runat="server">
void Page_Load(object sender, EventArgs e)
{
	string msg = this.Request.QueryString["msg"];
	if (!Globals.IsNullorEmpty(msg))
		this.message.Text = HttpUtility.UrlDecode(msg);
}
</script>
<CP:Container id="MPContainer" ismodal="false" thememasterfile="ControlPanelModalMaster.ascx" runat="server">
<CP:Content id="bcr" runat="server">
<div class="CommonContentArea">
	<div class="CommonContent">
		<table cellspacing="0" cellpadding="4" border="0">
		<tr>
			<td><img src="<%=SiteUrls.Instance().LoadingImageUrl %>" width="16" height="16" border="0"></td>
			<td><asp:literal id="message" runat="server" text="Loading..." /></td>
		</tr>
		</table>
	</div>
</div>
</CP:Content>
</CP:Container>

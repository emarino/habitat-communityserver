<%@ Page Language="C#" AutoEventWireUp="true" %>
<%@ Import Namespace="CommunityServer.Reader.Components" %>
<% Response.ContentType = "text/xml"; %>
<script language="C#" runat="server">

void Page_Load(Object sender,EventArgs e)
{
	OpmlManager opml = new OpmlManager();
	
	Response.Write(opml.ExportOpml());   
}

</script>

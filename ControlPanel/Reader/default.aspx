<%@ Page %>
<%@ Import Namespace="System.Xml" %>
<%@ Import NameSpace="CommunityServer.Reader.Components" %>
<%@ Import NameSpace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Controls" %>

<script runat="server" language="C#">
	public string GetFeedId(Object item)
	{
		FeedPost post = (FeedPost) item;
		return post.FeedId.ToString();
	}
	
	public string GetTitle(Object item)
	{
		FeedPost post = (FeedPost) item;
		return post.Title;
	}
	
	public string GetPostId(Object item)
	{
		FeedPost post = (FeedPost) item;
		return post.FeedPostId.ToString();
	}
	
	public string GetItemClass(Object item, string itemType)
	{
		FeedPost post = (FeedPost) item;
		bool hasRead = post.HasRead;		
		string itemClass = (itemType == "alt" ? "Alternate" : "") + "PostListingItem" + (hasRead == false ? "Unread" : "");
		return itemClass;
	}
	
	public string GetPostBody(object item)
	{
		FeedPost post = (FeedPost) item;
		int length = 300;
		string description = post.Description;
		
		if (Formatter.RemoveHtml(description, 0).Length > length)
			description = Formatter.RemoveHtml(description, 0).Substring(0, length) + "... [<a href=\"" + post.Link + "\" target=\"_blank\">read more</a>]";
		else
			description = Formatter.RemoveHtml(description, 0);

		return description;
	}

    protected override void OnPreRender(System.EventArgs e)
    {
        ((ComponentArt.Web.UI.CallBack)CSControlUtility.Instance().FindControl(this, "ListingCallBack")).Parameter = "";
        ((ComponentArt.Web.UI.CallBack)CSControlUtility.Instance().FindControl(this, "DisplayCallBack")).Parameter = "";        
        base.OnPreRender(e);
    }

</script>

<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ReaderMaster.ascx" >
<CP:Content id="OuterTaskRegion" runat="server">
<CSReader:Reader runat="server" ID="ReaderControl">
<SkinTemplate>
	
<script type="text/javascript">
// <![CDATA[
function slResizeFolderListingPane(domElementId, newPaneHeight, newPaneWidth)
{
	var listing = document.getElementById('FeedReaderFolderListing');
	var header = document.getElementById('FeedReaderFolderListingHeader');
	if (listing && header)
	{
		try
		{
			listing.style.height = (newPaneHeight - parseInt(header.offsetHeight)) + 'px';
			
			<%=CSControlUtility.Instance().FindControl(this, "tvFolderList").ClientID%>.Render();
		}
		catch (err)
		{
		}
	}
}

function slResizePostListingPane(domElementId, newPaneHeight, newPaneWidth)
{
	var listing = document.getElementById('FeedReaderPostListing');
	var header = document.getElementById('FeedReaderPostListingHeader');
	if (listing && header)
	{
		try
		{
			listing.style.height = (newPaneHeight - parseInt(header.offsetHeight)) + 'px';
		}
		catch (err)
		{
		}
	}
}

var postDisplayPaneHeight = 300;
var postDisplayPaneWidth = 200;
function slResizePostDisplayPane(domElementId, newPaneHeight, newPaneWidth)
{
	postDisplayPaneHeight = newPaneHeight;
	postDisplayPaneWidth = newPaneWidth;
	slResizePostDisplayContents();	
}

function slResizePostDisplayContents()
{
	var container = document.getElementById('FeedReaderPostDisplayContainer');
	var body = document.getElementById('FeedReaderPostDisplayBody');
	var header = document.getElementById('FeedReaderPostDisplayHeader');

	if (container && body)
	{
		try
		{
			container.style.height = (postDisplayPaneHeight - 18) + 'px';
			container.style.width = (postDisplayPaneWidth - 18) + 'px';
			if (header)
			{
				header.style.width = (postDisplayPaneWidth - 36) + 'px';
				body.style.width = (postDisplayPaneWidth - 36) + 'px';
				body.style.height = (postDisplayPaneHeight - parseInt(header.offsetHeight) - 56) + 'px';
			}
			else
			{
				body.style.height = (postDisplayPaneHeight - 36) + 'px';
				body.style.width = (postDisplayPaneWidth - 36) + 'px';
			}
		} 
		catch (err)
		{
		}
	}
}

function classSwap(divObject, unreadClassName, readClassName)
{
	if (divObject.className != 'PostListingItemSelected')
	{
		if (divObject.className.indexOf('Unread') > 0)
			divObject.className = unreadClassName;
		else
			divObject.className = readClassName;
	}
}

function highlightRow(divObject)
{
	classSwap(divObject, 'PostListingItemHoverUnread', 'PostListingItemHover');
}

function unhighlightRow(divObject)
{
	classSwap(divObject, 'PostListingItemUnread', 'PostListingItem');
}

function highlightAltRow(divObject)
{
	classSwap(divObject, 'AlternatePostListingItemHoverUnread', 'AlternatePostListingItemHover');
}

function unhighlightAltRow(divObject)
{
	classSwap(divObject, 'AlternatePostListingItemUnread', 'AlternatePostListingItem');
}

var prevSelectedPostListDiv = null;
var prevSelectedPostListDivType = null;
function selectRow(divObject, feedPostId)
{
	if (divObject.className.indexOf('Selected') < 0)
	{
		var markRead = divObject.className.indexOf('Unread') > 0;
	
		if (prevSelectedPostListDiv != null)
			prevSelectedPostListDiv.className = (prevSelectedPostListDivType == 'alt' ? 'AlternatePostListingItem' : 'PostListingItem'); 

		prevSelectedPostListDiv = divObject;
		prevSelectedPostListDivType = (divObject.className.indexOf('Alternate') >= 0 ? 'alt' : 'item');
		
		divObject.className = 'PostListingItemSelected';
		
		<%=ReaderControl.ClientID%>.SelectPost(feedPostId, markRead);
	}
}
// ]]>
</script>

<CA:Splitter runat="server" ID="Splitter1" FillHeight="true" HeightAdjustment="0" FillWidth="true" WidthAdjustment="0">
	<Layouts>
		<CA:SplitterLayout>
			<Panes 
				SplitterBarCollapseImageUrl="../images/readerimages/splitter_col.gif" 
				SplitterBarExpandImageUrl="../images/readerimages/splitter_exp.gif" 
				SplitterBarCollapseImageWidth="6" 
				SplitterBarCollapseImageHeight="116" 
				SplitterBarCollapsedCssClass="SplitterBarCollapsed"
				SplitterBarCssClass="SplitterBar" 
				SplitterBarActiveCssClass="ActiveSplitterBar" 
				SplitterBarWidth="5" 
				Orientation="Horizontal">
				<CA:SplitterPane 
					PaneContentId="TreeViewContent" 
					ClientSideOnResize="slResizeFolderListingPane"
					Width="220" 
					MinWidth="100" 
					CssClass="SplitterPane" />
				<CA:SplitterPane 
					PaneContentId="GridContent" 
					ClientSideOnResize="slResizePostListingPane"
					Width="250" 
					MinWidth="100" 
					CssClass="SplitterPane" />
				<CA:SplitterPane 
					PaneContentId="DetailsContent" 
					ClientSideOnResize="slResizePostDisplayPane"
					CssClass="SplitterPane" />
			</Panes>
		</CA:SplitterLayout>
	</Layouts>
	<Content>
		<CA:SplitterPaneContent id="TreeViewContent">
			<div class="FolderListHeading" id="FeedReaderFolderListingHeader"><CSReader:ResourceControl ResourceName="reader_folder_list_heading" runat="server" ID="Resourcecontrol1" NAME="Resourcecontrol1"/></div>
			<div class="FolderListingContainer" id="FeedReaderFolderListing">
				<!-- Main tree view listing -->
				<CA:TreeView id="tvFolderList" 
					AutoPostBackOnSelect="False"
					NodeEditingEnabled="True"
					DragAndDropEnabled="True"
					KeyboardEnabled="True"
					ExpandNodeOnSelect="True"
					CollapseNodeOnSelect="False"
					CssClass="TreeView" 
					NodeCssClass="TreeNode" 
					SelectedNodeCssClass="SelectedTreeNode" 
					HoverNodeCssClass="HoverTreeNode"
					LineImageWidth="19" 
					LineImageHeight="20"
					DefaultImageWidth="16" 
					DefaultImageHeight="16"
					NodeLabelPadding="3"
					ImagesBaseUrl="../images/readerimages/"
					ParentNodeImageUrl="images/folder.gif" 
					LeafNodeImageUrl="images/folder.gif" 
					ShowLines="true" 
					LineImagesFolderUrl="../images/readerimages/images/lines/"
					EnableViewState="true"
					Width="100%"
					Height="100%"
					AutoScroll="true"
					HoverPopupEnabled="true"
					runat="server" >
				</CA:TreeView>
				
				<!-- Context menu for the root folder -->
				<CA:Menu id="rootMenu" 
					Orientation="Vertical"
					DefaultGroupCssClass="MenuGroup"
					DefaultItemLookID="DefaultItemLook"
					DefaultGroupItemSpacing="1"
					ImagesBaseUrl="images/"
					EnableViewState="false"
					ContextMenu="Custom"
					runat="server">
					<Items>	
						<CA:MenuItem ID="newFeed" Text="New Feed" />
						<CA:MenuItem ID="newFolder" Text="New Folder" />
						<CA:MenuItem ID="importOpml" Text="Import OPML" />
						<CA:MenuItem ID="exportOpml" Text="Export OPML" />
					</Items>
					<ItemLooks>
						<CA:ItemLook LookID="DefaultItemLook" CssClass="MenuItem" HoverCssClass="MenuItemHover" ExpandedCssClass="MenuItemHover" LeftIconWidth="20" LeftIconHeight="18" LabelPaddingLeft="10" LabelPaddingRight="10" LabelPaddingTop="3" LabelPaddingBottom="4" />
						<CA:ItemLook LookID="BreakItem" CssClass="MenuBreak" />
					</ItemLooks>
				</CA:Menu>
				
				<!-- Context menu for folder items -->
				<CA:Menu id="folderMenu" 
					Orientation="Vertical"
					DefaultGroupCssClass="MenuGroup"
					DefaultItemLookID="DefaultItemLook"
					DefaultGroupItemSpacing="1"
					ImagesBaseUrl="images/"
					EnableViewState="false"
					ContextMenu="Custom"
					runat="server">
					<Items>	
						<CA:MenuItem ID="folderNewFeed" Text="New Feed" />
						<CA:MenuItem ID="folderNewFolder" Text="New Folder" />
						<CA:MenuItem ID="deleteFolder" Text="Delete Folder" />
					</Items>
					<ItemLooks>
						<CA:ItemLook LookID="DefaultItemLook" CssClass="MenuItem" HoverCssClass="MenuItemHover" ExpandedCssClass="MenuItemHover" LeftIconWidth="20" LeftIconHeight="18" LabelPaddingLeft="10" LabelPaddingRight="10" LabelPaddingTop="3" LabelPaddingBottom="4" />
						<CA:ItemLook LookID="BreakItem" CssClass="MenuBreak" />
					</ItemLooks>
				</CA:Menu>

				<!-- Context menu for feed items -->
				<CA:Menu id="feedMenu" 
					Orientation="Vertical"
					DefaultGroupCssClass="MenuGroup"
					DefaultItemLookID="DefaultItemLook2"
					DefaultGroupItemSpacing="1"
					ImagesBaseUrl="images/"
					EnableViewState="false"
					ContextMenu="Custom"
					runat="server">
					<Items>	
						<CA:MenuItem ID="deleteFeed" Text="Delete Feed" />
						<CA:MenuItem ID="feedProperties" Text="Properties" />
					</Items>
					<ItemLooks>
						<CA:ItemLook LookID="DefaultItemLook2" CssClass="MenuItem" HoverCssClass="MenuItemHover" ExpandedCssClass="MenuItemHover" LeftIconWidth="20" LeftIconHeight="18" LabelPaddingLeft="10" LabelPaddingRight="10" LabelPaddingTop="3" LabelPaddingBottom="4" />
						<CA:ItemLook LookID="BreakItem2" CssClass="MenuBreak" />
					</ItemLooks>
				</CA:Menu>
			</div>
		</CA:SplitterPaneContent>
		<CA:SplitterPaneContent id="GridContent"> 
			<div class="PostListHeading" id="FeedReaderPostListingHeader"><CSReader:ResourceControl ResourceName="reader_post_list_heading" runat="server" ID="Resourcecontrol2" NAME="Resourcecontrol2"/></div>
			<div class="PostListingContainer" id="FeedReaderPostListing">
				<CA:CallBack id="ListingCallBack" CssClass="TreeGridCallBack" runat="server" >
					<Content>
						<asp:PlaceHolder id="PlaceHolder1" runat="server">
							<asp:Repeater id="rPosts" runat="server">
								<ItemTemplate>
									<div class="<%# GetItemClass(Container.DataItem, "item") %>" onMouseOver="highlightRow(this);" onMouseOut="unhighlightRow(this);" onClick="selectRow(this, <%# GetPostId(Container.DataItem) %>);" >
										<table border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td align="center" valign="middle">
													<img src="../images/readerimages/images/file.gif" alt="<%# GetTitle(Container.DataItem) %>">&nbsp;&nbsp;
												</td>
												<td nowrap>
													<%# GetTitle(Container.DataItem) %><br /><%# DataBinder.Eval(Container.DataItem, "PubDate") %>
												</td>
											</tr>
										</table>
									</div>
								</ItemTemplate>
								<AlternatingItemTemplate>
									<div class="<%# GetItemClass(Container.DataItem, "alt") %>" onMouseOver="highlightAltRow(this);" onMouseOut="unhighlightAltRow(this);" onClick="selectRow(this, <%# GetPostId(Container.DataItem) %>);" >
										<table border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td align="center" valign="middle">
													<img src="../images/readerimages/images/file.gif" alt="<%# GetTitle(Container.DataItem) %>">&nbsp;&nbsp;
												</td>
												<td nowrap>
													<%# GetTitle(Container.DataItem) %><br /><%# DataBinder.Eval(Container.DataItem, "PubDate") %>
												</td>
											</tr>
										</table>
									</div>
								</AlternatingItemTemplate>
							</asp:Repeater>
						</asp:PlaceHolder>
					</Content>
					<LoadingPanelClientTemplate>
						<div class="PostListingItem">
							Loading...&nbsp;<img src="../images/readerimages/images/spinner.gif" width="16" height="16" border="0">
						</div>
					</LoadingPanelClientTemplate>
				</CA:CallBack>           
			</div>
		</CA:SplitterPaneContent>
		<CA:SplitterPaneContent id="DetailsContent">
			<div style="width: 100%; height: 100%; overflow: hidden;">
				<CA:CallBack id="DisplayCallBack" CssClass="TreeGridCallBack" ClientSideOnCallbackComplete="slResizePostDisplayContents" runat="server" >
					<Content>
						<asp:PlaceHolder id="phPostDisplay" runat="server">
							<div class="PostDisplayContainer" id="FeedReaderPostDisplayContainer">
								<div class="PostDisplayBody">
									<div class="PostDisplayHeader" id="FeedReaderPostDisplayHeader">
										<div class="PostDisplayTitle"><a href='<asp:Literal ID="lLink" runat="server" />' target="_blank"><asp:Literal ID="lTitle" Runat="Server"/></a></div>
										<div class="PostDisplayPubDate">Posted on <asp:Literal ID="lPubDate" Runat="Server"/></div>
										<div class="PostDisplayCommentsUrl"><asp:Literal ID="lCommentsUrl" Runat="Server"/></div>
									</div>
									<div class="PostDisplayAuthor"><asp:Literal ID="lAuthor" Runat="Server"/></div>
									<div class="PostDisplayDescription" id="FeedReaderPostDisplayBody">
										<asp:Literal ID="lDescription" Runat="Server"/>
									</div>
								</div>
							</div>
						</asp:PlaceHolder>
						<asp:PlaceHolder id="phFolderSummary" Visible="False" runat="server">
							<div class="PostDisplayContainer" id="FeedReaderPostDisplayContainer">
								<div class="PostDisplayBody">
									<div class="FolderSummaryContainer" id="FeedReaderPostDisplayBody">
										<table width="100%">
											<tr>
												<td colspan="2" align="right">
													<a href='FolderRss.ashx?UserName=<%= CSContext.Current.User.Username %>&Token=<%= CSContext.Current.User.PublicToken %>&f=<asp:Literal id="lFolderId" runat="server" />&n=<asp:Literal id="lFolderName" runat="server" />'><img src="../../utility/images/securerss.gif" alt="<%= FeedReaderResourceManager.GetString("reader_rss_available") %>" border="0"></a>
												</td>
											</tr>
											<tr>
												<td valign="top" width="50%">
													<div class="MostActiveArticlesContainer">
														<div class="MostActiveArticlesHeader">Most Commented Articles</div>
														<asp:Repeater ID="rMostActiveArticles" Runat="server">
															<ItemTemplate>
																<div class="MostActiveArticlesTitle"><a href='<%# DataBinder.Eval(Container.DataItem, "Link") %>' target="_blank"><%# DataBinder.Eval(Container.DataItem, "Title") %></a></div>
															</ItemTemplate>
														</asp:Repeater>
													</div>
												</td>
												<td valign="top" width="50%">
													<div class="MostRecentArticlesContainer">
														<div class="MostRecentArticlesHeader">Most Recent Articles</div>
														<asp:Repeater ID="rMostRecentArticles" Runat="Server">
															<ItemTemplate>
																<div class="MostRecentArticlesTitle"><a href='<%# DataBinder.Eval(Container.DataItem, "Link") %>' target="_blank"><%# DataBinder.Eval(Container.DataItem, "Title") %></a></div>
															</ItemTemplate>
														</asp:Repeater>
													</div>
												</td>
											</tr>
											<tr>
												<td colspan="2" valign="top">
													<div class="PostSummaryHeader">Post Summary</div>
													<asp:Repeater ID="rPostSummary" Runat="Server">
														<ItemTemplate>
															<div class="PostSummaryItem">
																<div class="PostSummaryTitle"><a href='<%# DataBinder.Eval(Container.DataItem, "Link") %>' target="_blank"><%# DataBinder.Eval(Container.DataItem, "Title") %></a></div>
																<div class="PostSummaryBody"><%# GetPostBody(Container.DataItem) %></div>
																<br/>
																<div class="PostSummaryPubDate"><%# DataBinder.Eval(Container.DataItem, "PubDate") %></div>
															</div>
														</ItemTemplate>
														<AlternatingItemTemplate>
															<div class="AlternatePostSummaryItem">
																<div class="PostSummaryTitle"><a href='<%# DataBinder.Eval(Container.DataItem, "Link") %>' target="_blank"><%# DataBinder.Eval(Container.DataItem, "Title") %></a></div>
																<div class="PostSummaryBody"><%# GetPostBody(Container.DataItem) %></div>
																<br/>
																<div class="PostSummaryPubDate"><%# DataBinder.Eval(Container.DataItem, "PubDate") %></div>
															</div>
														</AlternatingItemTemplate>
													</asp:Repeater>
												</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</asp:PlaceHolder>
						<asp:PlaceHolder id="phFeedSummary" Visible="False" runat="server">
							<div class="PostDisplayContainer" id="FeedReaderPostDisplayContainer">
								<div class="PostDisplayBody">
									<div class="FeedSummaryContainer" id="FeedReaderPostDisplayBody">
									<asp:Repeater ID="rFeedsSummarized" Runat="server">
										<ItemTemplate>
											<div class="FeedSummaryItem">
												<div class="FeedSummaryTitle"><a href='<%# DataBinder.Eval(Container.DataItem, "Link") %>' target="_blank"><%# DataBinder.Eval(Container.DataItem, "Title") %></a></div>
												<div class="FeedSummaryBody"><%# GetPostBody(Container.DataItem) %></div>
												<br/>
												<div class="FeedSummaryPubDate"><%# DataBinder.Eval(Container.DataItem, "PubDate") %></div>
											</div>
										</ItemTemplate>
									</asp:Repeater>
									</div>
								</div>
							</div>
						</asp:PlaceHolder>
					</Content>
					<LoadingPanelClientTemplate>
					<div class="PostDisplayContainer">
						<div class="PostDisplayBody">
							Loading...&nbsp;<img src="../images/readerimages/images/spinner.gif" width="16" height="16" border="0">
						</div>
					</div>
					</LoadingPanelClientTemplate>
				</CA:CallBack>   
			</div>
		</CA:SplitterPaneContent>
	</Content>          
</CA:Splitter>
		    
		    
</SkinTemplate>
</CSReader:Reader>
</CP:Content>
</CP:Container>
<%@ Page %>

<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelModalMaster.ascx" >
	<CP:Content id="bhcr" runat="server">
		<CSControl:Script runat="server" Src="Utility/reader/feedreader.js" ID="Script2" />
	</CP:Content>
    <CP:Content id="bcr" runat="server">
	
		<div class="CommonContentArea">
			<div class="CommonContent">
				<CSReader:StatusMessage ResourceName="reader_add_feed_successful" ResourceFile="FeedReader.xml" Success="true" Visible="true" runat="server" />
				
				<div class="CommonFormActionButtons">
					<input type="button" value="Close" onclick="javascript: closeModalWindow(); window.top.location.reload();"/>
				</div>
			</div>
		</div>
		
	</CP:Content>
</CP:Container>
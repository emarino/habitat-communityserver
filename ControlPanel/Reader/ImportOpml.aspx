<%@ Page %>

<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelModalMaster.ascx" >
<CP:Content id="bhcr" runat="server">
	<CSControl:Script runat="server" Src="Utility/reader/feedreader.js" ID="Script2" />
</CP:Content>
<CP:Content id="bcr" runat="server">
<CSReader:ImportOpml id="importOpml" runat="Server">
<SkinTemplate>

<div class="CommonContentArea">
	<div class="CommonContent">
		<CSReader:StatusMessage runat="server" id="lStatus" Visible="false" />
		<div class="CommonFormFieldName"><CSReader:ResourceControl ResourceName="reader_import_opml_label" runat="server"/></div>
		<div class="CommonFormField"><input type="file" id="uploadedFile" runat="server" /></div>
		<div class="CommonFormActionButtons"><asp:Button ID="btnSubmit" Text="Import OPML" Runat="server" /></div>
	</div>
</div>

</SkinTemplate>
</CSReader:ImportOpml>
</CP:Content>
</CP:Container>
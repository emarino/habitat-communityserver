<%@ Page %>

<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelModalMaster.ascx" >
<CP:Content id="bhcr" runat="server">
	<CSControl:Script runat="server" Src="Utility/reader/feedreader.js" ID="Script2" />
</CP:Content>
<CP:Content id="bcr" runat="server">
<CSReader:FeedProperties id="feedProperties" runat="Server">
<SkinTemplate>

<div class="CommonContentArea">
<div class="CommonContent">
<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="CommonFormFieldName">
			<CSReader:ResourceControl ResourceName="reader_feed_props_title" runat="server" />
		</td>
		<td class="CommonFormField">
			<asp:Literal ID="lTitle" Runat="server" />
		</td>
	</tr>
	<tr>
		<td class="CommonFormFieldName">
			<CSReader:ResourceControl ResourceName="reader_feed_props_syndication_url" runat="server" ID="Resourcecontrol1"/>
		</td>
		<td class="CommonFormField">
			<a href='<asp:Literal ID="lUrlUrl" Runat="server" />' target="_blank"><asp:Literal ID="lUrl" Runat="server" /></a>
		</td>
	</tr>
	<tr>
		<td class="CommonFormFieldName">
			<CSReader:ResourceControl ResourceName="reader_feed_props_site_url" runat="server" ID="Resourcecontrol2"/>
		</td>
		<td class="CommonFormField">
			<a href='<asp:Literal ID="lLinkUrl" Runat="server" />' target="_blank"><asp:Literal ID="lLink" Runat="server" /></a>
		</td>
	</tr>
	<tr>
		<td class="CommonFormFieldName">
			<CSReader:ResourceControl ResourceName="reader_feed_props_language" runat="server" ID="Resourcecontrol3"/>
		</td>
		<td class="CommonFormField">
			<asp:Literal ID="lLanguage" Runat="server" />
		</td>
	</tr>
	<tr>
		<td class="CommonFormFieldName">
			<CSReader:ResourceControl ResourceName="reader_feed_props_generator" runat="server" ID="Resourcecontrol4"/>
		</td>
		<td class="CommonFormField">
			<asp:Literal ID="lGenerator" Runat="server" />
		</td>
	</tr>
	<tr>
		<td class="CommonFormFieldName">
			<CSReader:ResourceControl ResourceName="reader_feed_props_subscribed_on" runat="server" ID="Resourcecontrol5"/>
		</td>
		<td class="CommonFormField">
			<asp:Literal ID="lSubscribeDate" Runat="server" />
		</td>
	</tr>
	<tr>
		<td class="CommonFormFieldName">
			<CSReader:ResourceControl ResourceName="reader_feed_props_last_updated_on" runat="server" ID="Resourcecontrol6"/>
		</td>
		<td class="CommonFormField">
			<asp:Literal ID="lLastUpdateDate" Runat="server" />
		</td>
	</tr>
	<tr>
		<td class="CommonFormFieldName">
			<CSReader:ResourceControl ResourceName="reader_feed_props_feed_state" runat="server" ID="Resourcecontrol7"/>
		</td>
		<td class="CommonFormField">
			<asp:Literal ID="lState" Runat="server" />&nbsp;<asp:Literal ID="lStateDescription" Runat="server" />
		</td>
	</tr>
	<tr>
		<td class="CommonFormFieldName">
			<CSReader:ResourceControl ResourceName="reader_feed_props_total_subscribers" runat="server" ID="Resourcecontrol8"/>
		</td>
		<td class="CommonFormField">
			<asp:Literal ID="lSubscribeCount" Runat="server" />
		</td>
	</tr>
	<tr>
		<td class="CommonFormFieldName">
			<CSReader:ResourceControl ResourceName="reader_feed_props_total_posts" runat="server" ID="Resourcecontrol9"/>
		</td>
		<td class="CommonFormField">
			<asp:Literal ID="lPostCount" Runat="server" />
		</td>
	</tr>
</table>

<br/>
</div>

</SkinTemplate>
</CSReader:FeedProperties>
</CP:Content>
</CP:Container>
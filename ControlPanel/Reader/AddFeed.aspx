<%@ Page %>

<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelModalMaster.ascx" >
<CP:Content id="bhcr" runat="server">
	<CSControl:Script runat="server" Src="Utility/reader/feedreader.js" ID="Script2" />
</CP:Content>
<CP:Content id="bcr" runat="server">
<CSReader:AddFeed id="addFeed" runat="Server">
<SkinTemplate>

<div class="CommonContentArea">
	<div class="CommonContent">
		<CSReader:StatusMessage runat="server" id="lStatus" Visible="false" />
		<table cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td class="CommonFormFieldName"><CSReader:ResourceControl ResourceName="reader_add_feed_label" runat="server" /></td>
			<td class="CommonFormField"><asp:TextBox ID="txtFeedUrl" MaxLength="255" size="40" Runat="server" /></td>
			<td class="CommonFormField"><asp:Button ID="btnAddFeed" Text="Add Feed" Runat="server" /></td>
		</tr>
		</table>
	</div>
</div>

</SkinTemplate>
</CSReader:AddFeed>
</CP:Content>
</CP:Container>
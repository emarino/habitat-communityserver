<%@ Page language="c#" Codebehind="Default.aspx.cs" EnableViewState="false" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.ControlPanelHomePage" %>
<CP:ControlPanelSelectedNavigation SelectedTab="home" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<div class="CommonDashboardTitle"><cp:resourcecontrol id="Resourcecontrol1" runat="server" resourcename="CP_Dashboard_Title"></cp:resourcecontrol></div>
	</CP:Content>
	<CP:Content id="PanelDescription" runat="server">
		<cp:ResourceControl runat="server" resourcename="CP_Dashboard_Title" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
	    <div runat="server" id="LicenseArea" class="CommonDashboardArea">
	        <fieldset>
	            <legend><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_License" /></legend>
	            <div class="CommonDashboardContent">
	                <CP:ResourceControl runat="server" ResourceName="CP_Dashboard_License_Description" />
	                <ul class="CommonDashboardActionList">
	                    <li><asp:HyperLink runat="server" NavigateUrl="~/ControlPanel/Tools/ManageLicenses.aspx"><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_License_Action" /></asp:HyperLink></li>
	                </ul>
	            </div>
	        </fieldset>
	    </div>
		<div runat="server" id="MembershipArea" class="CommonDashboardArea">
		    <fieldset>
		        <legend><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_Membership" ID="Resourcecontrol4"/></legend>
			    <div class="CommonDashboardContent">
				    <asp:Literal runat="server" id="MembershipDescription" />
				    <ul class="CommonDashboardActionList">
					    <li><asp:HyperLink runat="server" NavigateUrl="~/ControlPanel/Membership/" ID="Hyperlink2"><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_Membership_Action" ID="Resourcecontrol8"/></asp:HyperLink></li>
				    </ul>
			    </div>
		    </fieldset>
		</div>

		<div runat="server" id="MyBlogsArea" class="CommonDashboardArea">
            <fieldset>
                <legend><CP:ResourceControl ID="ResourceControl18" runat="server" ResourceName="CP_Dashboard_MyBlogs" /></legend>
			    <div class="CommonDashboardContent">
				    <CP:ResourceControl ID="ResourceControl25" runat="server" ResourceName="CP_Dashboard_MyBlogs_Description" />
				    <ul class="CommonDashboardActionList">
					    <li runat="server" id="MyBlogsNewPostArea"><asp:HyperLink id="MyBlogsNewPost" runat="server" NavigateUrl="~/ControlPanel/Blogs/PostEditor.aspx?SelectedNavItem=NewPost" /></li>
					    <li runat="server" id="MyBlogsNewPostByEmailArea" visible="false"><asp:HyperLink id="MyBlogsNewPostByEmail" runat="server" /></li>
					    <li runat="server" id="MyBlogsReviewCommentsArea"><asp:HyperLink id="MyBlogsReviewComments" runat="server" NavigateUrl="~/ControlPanel/Blogs/comments.aspx?SelectedNavItem=ReviewComments" /></li>
					    <li runat="server" id="MyBlogsManageArea"><asp:HyperLink id="MyBlogsManage" runat="server" NavigateUrl="~/ControlPanel/Blogs/" /></li>
					    <li><asp:HyperLink runat="server" NavigateUrl="~/ControlPanel/Blogs/switch.aspx"><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_MyBlogs_Select" ID="Resourcecontrol28"/></asp:HyperLink></li>
				    </ul>
			    </div>
            </fieldset>	
		</div>
		
		<div runat="server" id="BlogAdministrationArea" class="CommonDashboardArea">
		    <fieldset>
		        <legend><CP:ResourceControl ID="ResourceControl29" runat="server" ResourceName="CP_Dashboard_BlogAdministration" /></legend>
			    <div class="CommonDashboardContent">
				    <asp:Literal runat="server" id="BlogAdministrationDescription" />
				    <ul class="CommonDashboardActionList">
					    <li><asp:HyperLink runat="server" NavigateUrl="~/ControlPanel/BlogAdmin/"><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_BlogAdministration_Action" ID="Resourcecontrol24"/></asp:HyperLink></li>
				    </ul>
			    </div>
		    </fieldset>
		</div>
		
		<div runat="server" id="MyPhotosArea" class="CommonDashboardArea">
		    <fieldset>
		        <legend><CP:ResourceControl ID="ResourceControl30" runat="server" ResourceName="CP_Dashboard_MyPhotos" /></legend>
			    <div class="CommonDashboardContent">
				    <CP:ResourceControl runat="server" ResourceName="CP_Dashboard_MyPhotos_Description" ID="Resourcecontrol2"/>
				    <ul class="CommonDashboardActionList">
					    <li runat="server" id="MyPhotosNewPostArea"><asp:HyperLink id="MyPhotosNewPost" runat="server" NavigateUrl="~/ControlPanel/Photos/PostEditor.aspx" /></li>
					    <li runat="server" id="MyPhotosReviewCommentsArea"><asp:HyperLink id="MyPhotosReviewComments" runat="server" NavigateUrl="~/ControlPanel/Photos/commentlist.aspx?SelectedNavItem=ReviewComments" /></li>
					    <li runat="server" id="MyPhotosManageArea"><asp:HyperLink id="MyPhotosManage" runat="server" NavigateUrl="~/ControlPanel/Photos/" /></li>
					    <li><asp:HyperLink runat="server" NavigateUrl="~/ControlPanel/Photos/switch.aspx"><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_MyPhotos_Select" ID="Resourcecontrol26"/></asp:HyperLink></li>
				    </ul>
			    </div>
		    </fieldset>
		</div>
		
		<div runat="server" id="PhotoAdministrationArea" class="CommonDashboardArea">
		    <fieldset>
                <legend><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_PhotoAdministration" ID="Resourcecontrol3"/></legend>
			    <div class="CommonDashboardContent">
				    <asp:Literal runat="server" id="PhotoAdministrationDescription" />
				    <ul class="CommonDashboardActionList">
					    <li><asp:HyperLink runat="server" NavigateUrl="~/ControlPanel/PhotoAdmin/"><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_PhotoAdministration_Action" ID="Resourcecontrol23"/></asp:HyperLink></li>
				    </ul>
			    </div>
		    </fieldset>
		</div>
		
		<div runat="server" id="MyFilesArea" class="CommonDashboardArea">
		    <fieldset>
                <legend><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_MyFiles" ID="Resourcecontrol5"/></legend>
			    <div class="CommonDashboardContent">
				    <CP:ResourceControl runat="server" ResourceName="CP_Dashboard_MyFiles_Description" ID="Resourcecontrol6"/>
				    <ul class="CommonDashboardActionList">
					    <li runat="server" id="MyFilesNewFileArea"><asp:HyperLink id="MyFilesNewFile" runat="server" NavigateUrl="~/ControlPanel/Files/FileEditor.aspx?SelectedNavItem=NewFile" /></li>
					    <li runat="server" id="MyFilesReviewFilesArea"><asp:HyperLink id="MyFilesReviewFiles" runat="server" NavigateUrl="~/ControlPanel/Files/filelist.aspx?SelectedNavItem=ReviewFiles&cid=-1&ip=Unpublished" /></li>
					    <li runat="server" id="MyFilesReviewCommentsArea"><asp:HyperLink id="MyFilesReviewComments" runat="server" NavigateUrl="~/ControlPanel/Files/comments.aspx?SelectedNavItem=ReviewComments&ip=Unpublished" /></li>
					    <li runat="server" id="MyFilesManageArea"><asp:HyperLink id="MyFilesManage" runat="server" NavigateUrl="~/ControlPanel/Files/" /></li>
					    <li><asp:HyperLink runat="server" NavigateUrl="~/ControlPanel/Files/switch.aspx"><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_MyFiles_Select" ID="Resourcecontrol27"/></asp:HyperLink></li>
				    </ul>
			    </div>
		    </fieldset>
		</div>
		
		<div runat="server" id="FileAdministrationArea" class="CommonDashboardArea">
		    <fieldset>
		        <legend><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_FileAdministration" ID="Resourcecontrol7"/></legend>
			    <div class="CommonDashboardContent">
				    <asp:Literal runat="server" id="FileAdministrationDescription" />
				    <ul class="CommonDashboardActionList">
					    <li><asp:HyperLink runat="server" NavigateUrl="~/ControlPanel/FileAdmin/"><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_FileAdministration_Action" ID="Resourcecontrol22"/></asp:HyperLink></li>
				    </ul>
			    </div>
		    </fieldset>
		</div>
		
		<div runat="server" id="ForumModerationArea" class="CommonDashboardArea">
            <fieldset>
                <legend><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_ForumsModeration" ID="Resourcecontrol9"/></legend>
		        <div class="CommonDashboardContent">
			        <asp:Literal runat="server" id="ForumsModerationDescription" />
			        <ul class="CommonDashboardActionList">
				        <li><asp:HyperLink runat="server" NavigateUrl="~/ControlPanel/Moderation/"><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_ForumsModeration_Action" ID="Resourcecontrol21"/></asp:HyperLink></li>
			        </ul>
		        </div>
            </fieldset>
		</div>
		
		<div runat="server" id="ForumAdministrationArea" class="CommonDashboardArea">
            <fieldset>
                <legend><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_ForumsAdministration" ID="Resourcecontrol11"/></legend>
			    <div class="CommonDashboardContent">
				    <asp:Literal runat="server" id="ForumsAdministrationDescription" />
				    <ul class="CommonDashboardActionList">
					    <li><asp:HyperLink runat="server" NavigateUrl="~/ControlPanel/Forums/" ID="Hyperlink1"><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_ForumsAdministration_Action" ID="Resourcecontrol20"/></asp:HyperLink></li>
				    </ul>
			    </div>
            </fieldset>
		</div>
		
		<div runat="server" id="ReaderAdministrationArea" class="CommonDashboardArea">
            <fieldset>
                <legend><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_ReaderAdministration" ID="Resourcecontrol12"/></legend>
			    <div class="CommonDashboardContent">
				    <asp:Literal runat="server" id="ReaderAdministrationDescription" />
				    <ul class="CommonDashboardActionList">
					    <li><asp:HyperLink runat="server" NavigateUrl="~/ControlPanel/Reader/" ID="Hyperlink3"><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_ReaderAdministration_Action" ID="Resourcecontrol14"/></asp:HyperLink></li>
				    </ul>
			    </div>
            </fieldset>
		</div>
		
		<div runat="server" id="SystemAdministrationArea" class="CommonDashboardArea">
            <fieldset>
                <legend><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_SystemAdministration" ID="Resourcecontrol13"/></legend>
			    <div class="CommonDashboardContent">
				    <CP:ResourceControl runat="server" ResourceName="CP_Dashboard_SystemAdministration_Description" ID="Resourcecontrol10"/>
				    <ul class="CommonDashboardActionList">
					    <li><asp:HyperLink runat="server" NavigateUrl="~/ControlPanel/Setup.aspx"><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_SystemAdministration_Action" ID="Resourcecontrol19"/></asp:HyperLink></li>
				    </ul>
			    </div>
            </fieldset>
		</div>
	</CP:Content>
	<CP:Content runat="server" id="RightColumnRegion">
		<div class="CommonSidebar">
			<div class="CommonSidebarArea">
				<h4 class="CommonSidebarHeader"><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_Announcements" ID="Resourcecontrol15"/></h4>
				<div class="CommonSidebarContent">
					<CP:RepeaterPlusNone runat="server" id="Announcements">
						<ItemTemplate>
							<div style="margin-bottom: 8px; margin-top: 8px;">
							<a href="<%# DataBinder.Eval(Container.DataItem, "Link")%>"><%#DataBinder.Eval(Container.DataItem, "Title")%></a>										
							<div><%#DataBinder.Eval(Container.DataItem, "Description")%></div>
							</div>
						</ItemTemplate>
						<NoneTemplate>
							<div style="margin-bottom: 8px; margin-top: 8px;">
							<CP:ResourceControl runat="server" ResourceName="CP_Dashboard_NoAnnouncements" />
							</div>
						</NoneTemplate>
					</CP:RepeaterPlusNone>
					<CP:ResourceControl runat="server" ResourceName="CP_Dashboard_Announcements_More" ID="Resourcecontrol16"/>
				</div>
			</div>

			<div class="CommonSidebarArea">
				<h4 class="CommonSidebarHeader"><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_Version" ID="Resourcecontrol17"/></h4>
				<div class="CommonSidebarContent">
					<asp:Literal runat="server" id="Version" />
					<asp:HyperLink runat="server" id="NewVersionAvailable" />
				</div>
			</div>		
		</div>
	</CP:Content>
</CP:Container>

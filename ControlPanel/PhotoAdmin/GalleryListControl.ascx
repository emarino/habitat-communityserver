<%@ Control Language="c#" AutoEventWireup="false" Codebehind="GalleryListControl.ascx.cs" Inherits="CommunityServer.ControlPanel.PhotoAdmin.GalleryListControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script type="text/javascript">
	function onCallbackError(excString)
	{
		if (confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_CallBackWarning") %>')) alert(excString); 
		<%= Grid1.ClientID %>.Page(1); 
	}

	function onDelete(item)
	{
		return confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_PhotosAdmin_GalleryList_Delete_Warning") %>'); 
	}
	  
	function deleteRow(rowId)
	{
		<%= Grid1.ClientID %>.Delete(<%= Grid1.ClientID %>.GetRowFromClientId(rowId)); 
	}
</script>
	<table cellpadding="0" cellspacing="0" border="0"  width="100%">
		<tr>
			<td width="100%">
				<CP:ResourceControl id="FeedbackFilterLabel" runat="Server" resourcename="CP_Photos_GridCol_Group"></CP:ResourceControl>&nbsp;<asp:DropDownList id="AdminGroupList" Runat="server" AutoPostBack="True"></asp:DropDownList>
			</td>
			<td nowrap="nowrap">
				<A href="SectionEdit.aspx?SectionID=-1&amp;GroupID=<%= groupID %>" class="CommonTextButton">
				<CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_PhotosAdmin_GalleryList_AddNewGallery"></CP:ResourceControl></A>
			</td>
		</tr>
	</table>
<br />
<div id="GrayGrid" >
<CA:Grid id="Grid1" runat="server" 
	AutoCallBackOnDelete="true" 
	ClientSideOnDelete="onDelete" 
	ClientSideOnCallbackError="onCallbackError" 
	>
	<Levels>
		<CA:GridLevel DataKeyField="SectionID" >

			<Columns>
				<CA:GridColumn DataField="ApplicationKey" HeadingText="ResourceManager.CP_Photos_GridCol_AppKey" />
				<CA:GridColumn DataField="Name" HeadingText="ResourceManager.CP_Photos_GridCol_Name"/>
				<CA:GridColumn DataField="Description" HeadingText="ResourceManager.CP_Photos_GridCol_Description" AllowGrouping="false" visible="false"/>
                <CA:GridColumn DataField="GroupName" HeadingText="ResourceManager.CP_Photos_GridCol_Group" />
				<CA:GridColumn DataField="Owners" HeadingText="ResourceManager.CP_Photos_GridCol_Owners" AllowGrouping="false" />
				<CA:gridcolumn DataField="IsActive" HeadingText="ResourceManager.CP_Photos_GridCol_Enabled" AllowGrouping="false" />
				<CA:GridColumn DataField="SectionID" Visible="false" AllowGrouping="false" />
				<CA:GridColumn HeadingText="ResourceManager.CP_Photos_GridCol_Actions" DataCellClientTemplateId="EditTemplate" Width="180" Align="Center" AllowGrouping="false" DataCellCssClass="LastDataCell" />
			</Columns>
		</CA:GridLevel>
	</Levels>
	<ClientTemplates>
		<CA:ClientTemplate Id="EditTemplate">
			<a href="SectionEdit.aspx?SectionID=## DataItem.GetMember("SectionID").Text ##" Class="CommonTextButton">Edit</a> 
 			<a href='../photos/default.aspx?sectionid=##  DataItem.GetMember("SectionID").Text ##' Class="CommonTextButton">Manage</a> 
			<a href="javascript:deleteRow('## DataItem.ClientId ##')" Class="CommonTextButton">Delete</a>
		</CA:ClientTemplate>
	</ClientTemplates>
</CA:Grid>
</div>

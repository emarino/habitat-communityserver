<%@ Register TagPrefix="CP" TagName = "TreeViewControl" Src = "~/ControlPanel/PhotoAdmin/GroupTreeViewControl.ascx" %>
<%@ Page language="c#" Codebehind="GroupManager.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.PhotoAdmin.GroupManager" %>
<CP:Container runat="server" id="Mpcontainer1" ThemeMasterFile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="SectionDescription" runat="server" ResourceName="CP_PhotosAdmin_GroupManager_Title"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<script type="text/javascript">
  // <![CDATA[
    // Disable grid hover style to avoid the flicker while resizing 
    function startResize(PaneDomObj)
    {
      return true; 
    }    

    // Restore the grid hover style 
    function endResize(PaneDomObj)
    {
      return true; 
    }    
    
    //for splitter support
	function resizeDetails(DomElementId, NewPaneHeight, NewPaneWidth)
    {
		if(DomElementId != null)
			resizeCategoryTree();
      // Forces the treeview to adjust to the new size of its container 
      
      //manually resize the iframe
      var sectionEditFrame = document.getElementById("sectionEditFrame");
      if(sectionEditFrame != null  && NewPaneHeight != null && NewPaneWidth != null)
		{
			sectionEditFrame.style.height = (NewPaneHeight - 19) + 'px';
			sectionEditFrame.style.width = NewPaneWidth + 'px';
		}
    }

    // ]]>
		</script>
		<div class="CommonManagerArea">
		<div class="CommonManagerContent">
		<CA:Splitter id="Splitter1" runat="server" ClientSideOnResizeEnd="endResize" ClientSideOnResizeStart="startResize"
			WidthAdjustment="305" FillWidth="true" FillHeight="true" HeightAdjustment="-240">
			<Layouts>
				<CA:SplitterLayout>
					<Panes SplitterBarCollapseImageUrl="../Images/splitter_col.gif" SplitterBarExpandImageUrl="../Images/splitter_exp.gif"
						SplitterBarExpandImageWidth="6"
						SplitterBarCollapseImageWidth="6" SplitterBarCollapseImageHeight="116" SplitterBarCssClass="SplitterBar"
						SplitterBarCollapsedCssClass="SplitterBarCollapsed" SplitterBarActiveCssClass="ActiveSplitterBar"
						SplitterBarWidth="5" Orientation="Horizontal">
						<CA:SplitterPane PaneContentId="TreeViewContent" Width="220" MinWidth="100" ClientSideOnResize="resizeCategoryTree"
							CssClass="SplitterPane"></CA:SplitterPane>
						<CA:SplitterPane PaneContentId="DetailsContent" CssClass="SplitterPane" ClientSideOnResize="resizeDetails" AllowScrolling="False"></CA:SplitterPane>
					</Panes>
				</CA:SplitterLayout>
			</Layouts>
			<Content>
				<CA:SplitterPaneContent id="TreeViewContent">
					<div class="SplitterListHeading" id="FolderListingHeader" style="height:19px;"><CP:ResourceControl ResourceName="CP_PhotosAdmin_GroupManager_Groups" runat="server" ID="Resourcecontrol1"
							NAME="Resourcecontrol1" /></div>
					<cp:TreeViewControl id="GroupTree" runat="server"></cp:TreeViewControl>
				</CA:SplitterPaneContent>
				<CA:SplitterPaneContent id="DetailsContent">
					<div class="SplitterListHeading" id="DetailsListingHeader" style="height:19px;"><CP:ResourceControl ResourceName="CP_PhotosAdmin_GroupManager_Details" runat="server" ID="Resourcecontrol2"
							NAME="Resourcecontrol1" /></div>
					<IFRAME id="sectionEditFrame" name="sectionEditFrame" src="IFrameHost.aspx" frameBorder="no"
						MARGINWIDTH="30" MARGINHEIGHT="30" scrolling="<%=IFrameScrolling%>" ></IFRAME>
				</CA:SplitterPaneContent>
			</Content>
		</CA:Splitter>
		</div>
		<div class="CommonManagerButtonsArea">
				<cp:hyperlink id="AddGroupButton" runat="server" resourcename="CP_Forums_Home_AddNewGroup" cssclass="CommonTextButton"
					navigateurl="javascript:AddGroup();"></cp:hyperlink>
				<cp:hyperlink id="AddSectionButton" runat="server" resourcename="CP_PhotosAdmin_GalleryList_AddNewGallery"
					cssclass="CommonTextButton" navigateurl="javascript:AddSection();"></cp:hyperlink>
				<cp:hyperlink id="DeleteButton" runat="server" resourcename="CP_GroupManager_DeleteSelected" cssclass="CommonTextButton"
					navigateurl="javascript:DeleteSelected();"></cp:hyperlink>
		</div>
		</div>
		<p class="PanelSaveButton">
			<CP:ControlPanelChangeManageView runat="server" CssClass="CommonTextButton" CurrentView="Tree" ViewChangeUrl="~/ControlPanel/PhotoAdmin/GroupList.aspx" ID="Controlpanelchangemanageview1"/>
		</p>
	</CP:Content>
</CP:Container>

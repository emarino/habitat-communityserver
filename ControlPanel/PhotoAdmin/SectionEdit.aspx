<%@ Register TagPrefix="CP" TagName = "SectionDetails" Src = "~/ControlPanel/PhotoAdmin/SectionEditControl.ascx" %>
<%@ Page language="c#" Codebehind="SectionEdit.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.PhotoAdmin.SectionEdit" %>
<CP:CONTROLPANELSELECTEDNAVIGATION id="SelectedNavigation1" runat="server" SelectedNavItem="gallery" />
<CP:Container runat="server" id="Mpcontainer1" ThemeMasterFile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<asp:literal id="RegionTitle" runat="server"></asp:literal>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<CP:StatusMessage id="Status" runat="server"></CP:StatusMessage>
		<DIV class="CommonDescription">
			<CP:ResourceControl id="RegionDescription" runat="Server" resourcename="CP_PhotosAdmin_SectionEdit_Description"></CP:ResourceControl>
		</DIV>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Galleries" />
		<CP:SectionDetails id="SectionEdit1" runat="Server"></CP:SectionDetails>
		<p class="PanelSaveButton DetailsFixedWidth">
			<cp:ResourceLinkButton id="SaveButton" runat="Server" CssClass="CommonTextButton" ResourceName="Save"></cp:ResourceLinkButton></p>
	</CP:Content>
</CP:Container>

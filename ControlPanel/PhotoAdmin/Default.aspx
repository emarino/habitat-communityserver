<%@ Page language="c#" Codebehind="Default.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.PhotoAdmin.HomePage" %>
<CP:CONTROLPANELSELECTEDNAVIGATION id="SelectedNavigation1" runat="server" SelectedNavItem="Photos"></CP:CONTROLPANELSELECTEDNAVIGATION>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="RegionTitle" runat="Server" resourcename="CP_PhotosAdmin_Default_Title"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<CP:ResourceControl id="RegionDescription" runat="Server" resourcename="CP_PhotosAdmin_Default_Description"></CP:ResourceControl></DIV>
		<DIV class="CommonDescription">
	</CP:Content>
</CP:Container>

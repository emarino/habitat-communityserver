<%@ Page language="c#" Codebehind="DefaultPermissionList.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.PhotoAdmin.DefaultPermissionList" %>
<CP:Container runat="server" id="Mpcontainer1" ThemeMasterFile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="SectionDescription" runat="server" ResourceName="CP_PhotosAdmin_DefaultPermissionsList_Title"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<CP:ResourceControl id="Section_Sub_Description" runat="server" ResourceName="CP_PhotosAdmin_DefaultPermissionsList_Description"></CP:ResourceControl><br />
			<EM>&nbsp;&nbsp;&nbsp;
				<CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_PhotosAdmin_DefaultPermissionsList_Tip" />
			</EM>
		</DIV>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Galleries" />
		<div id="GrayGrid">
        <asp:Repeater runat="server" ID="PermissionList">
            <HeaderTemplate>
                <table class="Grid" cellSpacing=0 cellPadding=0 width="100%">
                <tbody>
                    <tr class="HeadingRow HeadingCellText HeadingCell">
                        <th><CP:ResourceControl runat="server" ResourceName="CP_Photos_GridCol_Name" /></th>
                        <th><CP:ResourceControl runat="server" ResourceName="CP_Photos_GridCol_View" /></th>
                        <th><CP:ResourceControl runat="server" ResourceName="CP_Photos_GridCol_Reply" /></th>
                        <th><CP:ResourceControl runat="server" ResourceName="CP_Photos_GridCol_Post" /></th>
                    </tr>    
            </HeaderTemplate>
            <ItemTemplate>
                    <tr class="Row">
                        <td><%# Eval("Name") %><asp:HiddenField ID="RoleId" runat="server" Value='<%# Eval("RoleIDString") %>' Visible="false" /></td>
                        <td><asp:CheckBox runat="server" ID="ViewCheckbox" Checked='<%# Eval("View") %>' /></td>
                        <td><asp:CheckBox runat="server" ID="ReplyCheckbox" Checked='<%# Eval("Reply") %>' /></td>
                        <td><asp:CheckBox runat="server" ID="PostCheckbox" Checked='<%# Eval("Post") %>' /></td>
                    </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                    <tr class="AlternatingRow">
                        <td><%# Eval("Name") %><asp:HiddenField ID="RoleId" runat="server" Value='<%# Eval("RoleIDString") %>' Visible="false" /></td>
                        <td><asp:CheckBox runat="server" ID="ViewCheckbox" Checked='<%# Eval("View") %>' /></td>
                        <td><asp:CheckBox runat="server" ID="ReplyCheckbox" Checked='<%# Eval("Reply") %>' /></td>
                        <td><asp:CheckBox runat="server" ID="PostCheckbox" Checked='<%# Eval("Post") %>' /></td>
                    </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        </div>
		<P style="FLOAT: right"><br />
			<cp:ResourceLinkButton id="Save" runat="Server" CssClass="CommonTextButton" ResourceName="Save"></cp:ResourceLinkButton></p>
		<P>&nbsp;
			<cp:ResourceLinkButton id="resetPermissions" runat="Server" Visible="False" CssClass="CommonTextButton" ResourceName="CP_PhotosAdmin_DefaultPermissionsList_ResetPermissions"></cp:ResourceLinkButton></P>
	</CP:Content>
</CP:Container>

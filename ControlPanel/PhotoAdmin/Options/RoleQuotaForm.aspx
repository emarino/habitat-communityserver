<%@ Register TagPrefix="CP" TagName = "RoleQuotaEditControl" Src = "~/ControlPanel/Controls/Galleries/RoleQuotaEditControl.ascx" %>
<%@ Page language="c#" Codebehind="RoleQuotaForm.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.PhotoAdmin.RoleQuotaForm" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelModalMaster.ascx" IsModal="true">
	<CP:Content id="bcr" runat="server">
		<DIV class="CommonContentArea">
			<CP:RoleQuotaEditControl id="RoleQuotaEditControl1" runat="server"></CP:RoleQuotaEditControl>
			<DIV class="CommonContent">
				<DIV class="CommonFormField PanelSaveButton">
					<cp:ResourceLinkButton id="Save" runat="Server" CssClass="CommonTextButton" ResourceName="Save"></cp:ResourceLinkButton>
				</DIV>
			</DIV>
		</DIV>
	</CP:Content>
</CP:Container>

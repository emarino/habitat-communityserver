<%@ Page language="c#" Codebehind="GeneralOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.PhotoAdmin.GeneralOptionsPage" %>
<CP:Container id="Mpcontainer1" runat="server" ThemeMasterFile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="ResourceControl1" runat="Server" resourcename="CP_PhotosAdmin_Options_GeneralSettings"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<CP:FileOnlyStatusMessage id="FOStatus" runat="server" visible="false"></CP:FileOnlyStatusMessage>
		<asp:PlaceHolder id="OptionHolder" runat="Server">
			<CP:ConfigOKStatusMessage id="Status" runat="server" visible="false"></CP:ConfigOKStatusMessage>
			<DIV class="CommonDescription">
				<CP:ResourceControl id="Resourcecontrol2" runat="Server" resourcename="CP_PhotosAdmin_Options_GeneralSettings_Instructions"></CP:ResourceControl></DIV>
			<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Galleries" />
			<div class="FixedWidthContainer">
			<P class="CommonFormFieldName">
				<TABLE cellSpacing="1" cellPadding="4" border="0">
					<TR>
						<TD class="CommonFormFieldName">
							<cp:helpicon id="Helpicon2" runat="Server" resourcename="CP_PhotosAdmin_Options_GeneralSettings_CreateDirectories_Help"></cp:helpicon>
							<cp:formlabel id="Formlabel1" runat="Server" resourcename="CP_PhotosAdmin_Options_GeneralSettings_CreateDirectories"
								controltolabel="createDirectories"></cp:formlabel></TD>
						<TD class="CommonFormField" noWrap>
							<asp:checkbox id="createDirectories" runat="Server" cssclass="ControlPanelTextInput"></asp:checkbox></TD>
					</TR>
					<TR>
						<TD class="CommonFormFieldName">
							<cp:helpicon id="Helpicon6" runat="Server" resourcename="CP_PhotosAdmin_Options_GeneralSettings_AutoCreateGallery_Help"></cp:helpicon>
							<cp:formlabel id="Formlabel5" runat="Server" resourcename="CP_PhotosAdmin_Options_GeneralSettings_AutoCreateGallery"
								controltolabel="autoCreateGallery"></cp:formlabel></TD>
						<TD class="CommonFormField" noWrap>
							<asp:checkbox id="autoCreateGallery" runat="Server" cssclass="ControlPanelTextInput"></asp:checkbox></TD>
					</TR>
		            <tr>
		            <td class="CommonFormFieldName">
			            <cp:helpicon runat="Server" resourcename="CP_PhotosAdmin_Options_GeneralSettings_AutoCreateDefaultGroup_Help" />
			            <cp:formlabel ID="Formlabel6" runat="Server" resourcename="CP_PhotosAdmin_Options_GeneralSettings_AutoCreateDefaultGroup" controltolabel="GroupList" />&nbsp;
		            </td>
		            <td class="CommonFormField">
			            <asp:dropdownlist id="GroupList" runat="Server" cssclass="ControlPanelTextInput" width = "300px" />
		            </td>
		            </tr>
				</TABLE>
			</P>
			<P class="CommonFormFieldName">
				<TABLE cellSpacing="1" cellPadding="4" border="0">
					<TR>
						<TD class="CommonFormFieldName" colSpan="2">
							<cp:helpicon id="Helpicon5" runat="Server" resourcename="CP_PhotosAdmin_Options_GeneralSettings_MaxSize_Help"></cp:helpicon>
							<CP:FORMLABEL id="Formlabel4" runat="server" ControlToLabel="ImageX" ResourceName="CP_PhotosAdmin_Options_GeneralSettings_MaxSize"></CP:FORMLABEL></TD>
					</TR>
					<TR>
						<TD>&nbsp;&nbsp;</TD>
						<TD class="CommonFormFieldName">
							<cp:helpicon id="Helpicon4" runat="Server" resourcename="CP_PhotosAdmin_Options_GeneralSettings_MaxWidth_Help"></cp:helpicon>
							<CP:FORMLABEL id="Formlabel2" runat="server" ControlToLabel="ImageX" ResourceName="CP_PhotosAdmin_Options_GeneralSettings_MaxWidth"></CP:FORMLABEL></TD>
						<TD class="CommonFormField">
							<asp:textbox id="ImageX" runat="server" Size="4" MaxLength="4"></asp:textbox>
							<asp:regularexpressionvalidator id="Regularexpressionvalidator1" runat="server" ControlToValidate="ImageX" ValidationExpression="^[-]*[0-9]*"
								Cssclass="validationWarning" ErrorMessage="*" NAME="Regularexpressionvalidator1"></asp:regularexpressionvalidator></TD>
					</TR>
					<TR>
						<TD>&nbsp;&nbsp;</TD>
						<TD class="CommonFormFieldName">
							<cp:helpicon id="Helpicon3" runat="Server" resourcename="CP_PhotosAdmin_Options_GeneralSettings_MaxHeight_Help"></cp:helpicon>
							<CP:FORMLABEL id="Formlabel3" runat="server" ControlToLabel="ImageY" ResourceName="CP_PhotosAdmin_Options_GeneralSettings_MaxHeight"></CP:FORMLABEL></TD>
						<TD class="CommonFormField">
							<asp:textbox id="ImageY" runat="server" Size="4" MaxLength="4"></asp:textbox>
							<asp:regularexpressionvalidator id="Regularexpressionvalidator2" runat="server" ControlToValidate="ImageY" ValidationExpression="^[-]*[0-9]*"
								Cssclass="validationWarning" ErrorMessage="*" NAME="Regularexpressionvalidator2"></asp:regularexpressionvalidator></TD>
					</TR>
					<TR>
						<TD>&nbsp;&nbsp;</TD>
						<TD class="CommonFormFieldName">
							<cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_PhotosAdmin_Options_GeneralSettings_Quality_Help"></cp:helpicon>
							<CP:FORMLABEL id="Formlabel7" runat="server" ControlToLabel="ThumbnailQuality" ResourceName="CP_PhotosAdmin_Options_GeneralSettings_Quality"></CP:FORMLABEL></TD>
						<TD class="CommonFormField">
							<asp:textbox id="Quality" runat="server" MaxLength="3" size="3"></asp:textbox>%
							<asp:regularexpressionvalidator id="Regularexpressionvalidator3" runat="server" ControlToValidate="Quality" ValidationExpression="[0-9]*"
								Cssclass="validationWarning" ErrorMessage="*" NAME="Regularexpressionvalidator3"></asp:regularexpressionvalidator></TD>
					</TR>
				</TABLE>
			</P>
			<P class="CommonFormFieldName">
				<TABLE cellSpacing="1" cellPadding="4" border="0">
					<TR>
						<TD class="CommonFormFieldName">
							<cp:helpicon id="Helpicon26" runat="Server" resourcename="CP_Settings_RSS_Threads_Descr"></cp:helpicon>
							<cp:resourcecontrol runat="server" resourcename="CP_Settings_RSS_Threads" /></TD>
						<TD class="CommonFormField" noWrap>
							<asp:textbox id="RSSDefaultThreadsPerFeed" runat="server" size="3" maxlength="3"></asp:textbox>
							<asp:requiredfieldvalidator id="RSSDefaultThreadsPerFeedValidator" runat="server" errormessage="*" font-bold="True"
								controltovalidate="RSSDefaultThreadsPerFeed"></asp:requiredfieldvalidator></TD>
					</TR>
				</TABLE>
			</P>
			</div>
			<p class="PanelSaveButton DetailsFixedWidth">
				<cp:ResourceLinkButton id="SaveButton" runat="Server" CssClass="CommonTextButton" ResourceName="Save"></cp:ResourceLinkButton></p>
		</asp:PlaceHolder>
	</CP:Content>
</CP:Container>

<%@ Register TagPrefix="CP" TagName = "QuotaEditor" Src = "~/ControlPanel/Controls/Galleries/QuotaEditControl.ascx" %>
<%@ Page language="c#" Codebehind="Quotas.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.PhotoAdmin.Quotas" %>
<CP:Container runat="server" id="Mpcontainer1" ThemeMasterFile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="SectionDescription" runat="server" ResourceName="CP_PhotosAdmin_Quotas_Title"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<CP:ResourceControl id="Section_Sub_Description" runat="server" ResourceName="CP_PhotosAdmin_Quotas_Description"></CP:ResourceControl><BR>
			<EM>&nbsp;&nbsp;&nbsp;
				<CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_PhotosAdmin_Quotas_Tip"></CP:ResourceControl></EM></DIV>
		<CP:ConfigOKStatusMessage id="Status" runat="server" visible="false"></CP:ConfigOKStatusMessage>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Galleries" />
		<CP:QuotaEditor id="QuotaEditControl1" runat="Server"></CP:QuotaEditor>
		<P style="FLOAT: right"><BR>
			<cp:ResourceLinkButton id="Save" runat="Server" CssClass="CommonTextButton" ResourceName="Save"></cp:ResourceLinkButton></P>
	</CP:Content>
</CP:Container>

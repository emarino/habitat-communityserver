<%@ Register TagPrefix="CP" TagName = "GroupEditControl" Src = "~/ControlPanel/Controls/GroupEditControl.ascx" %>
<%@ Page language="c#" Codebehind="GroupForm.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.PhotoAdmin.GroupForm" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelModalMaster.ascx" IsModal="true">
<CP:Content id=bcr runat="server">
<DIV class="CommonContentArea">
	<CP:GroupEditControl id=GroupEditControl1 runat="server"></CP:GroupEditControl>
	<DIV class="CommonContent">
		<DIV class="CommonFormField PanelSaveButton">
			<cp:ResourceLinkButton id="Save" runat="Server" CssClass="CommonTextButton" ResourceName="Save"></cp:ResourceLinkButton>
		</DIV>
	</div>
</div>
</CP:Content>
</CP:Container>

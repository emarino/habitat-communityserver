<%@ Page language="c#" Codebehind="GroupList.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.PhotoAdmin.GroupList" %>
<%@ Register TagPrefix="CP" TagName = "GroupList" Src = "~/ControlPanel/PhotoAdmin/GroupListControl.ascx" %>
<CP:Container runat="server" id="Mpcontainer1" ThemeMasterFile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="SectionDescription" runat="server" ResourceName="CP_PhotosAdmin_GroupList_Title"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<CP:ResourceControl id="Section_Sub_Description" runat="server" ResourceName="CP_PhotosAdmin_GroupList_Description"></CP:ResourceControl></DIV>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Galleries" />
		<CP:GroupList id="GroupList1" runat="Server"></CP:GroupList>
		<p class="PanelSaveButton">
			<CP:ControlPanelChangeManageView runat="server" CssClass="CommonTextButton" CurrentView="Grid" ViewChangeUrl="~/ControlPanel/PhotoAdmin/GroupManager.aspx" ID="Controlpanelchangemanageview1"/>
		</p>
	</CP:Content>
</CP:Container>

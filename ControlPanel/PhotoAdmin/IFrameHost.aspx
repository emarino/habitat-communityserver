<%@ Import Namespace="CommunityServer.Components" %>
<%@ Register TagPrefix="CP" TagName = "GroupEditControl" Src = "~/ControlPanel/Controls/GroupEditControl.ascx" %>
<%@ Register TagPrefix="CP" TagName = "SectionEditControl" Src = "~/ControlPanel/PhotoAdmin/SectionEditControl.ascx" %>
<%@ Page language="c#" Codebehind="IFrameHost.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.PhotoAdmin.IFrameHost" %>
<CP:Container id="MPContainer" IsModal="false" ThemeMasterFile="ControlPanelModalMaster.ascx" runat="server">
	<CP:Content id="bcr" runat="server">
		<DIV class="CommonContentArea">
			<DIV class="CommonContent">
		<cp:statusmessage id="Status" runat="server"></cp:statusmessage>
		<cp:sectioneditcontrol id="SectionEdit" runat="server" visible="false"></cp:sectioneditcontrol>
		<cp:groupeditcontrol id="GroupEdit" runat="server" visible="false"></cp:groupeditcontrol>
		
					<DIV class="CommonFormField PanelSaveButton">
						<cp:ResourceLinkButton id="Save" runat="Server" CssClass="CommonTextButton" ResourceName="Save"></cp:ResourceLinkButton></DIV>
			</DIV>
		</DIV>
	</CP:Content>
</CP:Container>

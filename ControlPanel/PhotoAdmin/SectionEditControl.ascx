<%@ Register TagPrefix="CP" TagName = "Layout" Src = "~/ControlPanel/Controls/Galleries/SkinOptionsLayoutControl.ascx" %>
<%@ Register TagPrefix="CP" TagName = "LocalQuotas" Src = "~/ControlPanel/Controls/Galleries/LocalQuotasControl.ascx" %>
<%@ Register TagPrefix="CP" TagName = "SectionDetails" Src = "~/ControlPanel/PhotoAdmin/SectionEditDetailsControl.ascx" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="SectionEditControl.ascx.cs" Inherits="CommunityServer.ControlPanel.PhotoAdmin.SectionEditControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<cp:DirectoryWriter id="Directorywriter1" runat="server" ForeColor="Red" SecurityResourceName="ManageGalleries_SecurityException"
	UnAuthorizedResourceName="ManageGalleries_UnAuthorizedException" Location="galleries"></cp:DirectoryWriter>
<TWC:TabbedPanes id="SettingTabs" runat="server"
	PanesCssClass="CommonPane"
	TabSetCssClass="CommonPaneTabSet"
	TabCssClasses="CommonPaneTab,CommonPaneTab1,CommonPaneTab2"
	TabSelectedCssClasses="CommonPaneTabSelected,CommonPaneTabSelected1,CommonPaneTabSelected2"
	TabHoverCssClasses="CommonPaneTabHover,CommonPaneTabHover1,CommonPaneTabHover2"
	>
	<TWC:TabbedPane runat="server">
		<Tab><CP:ResourceControl runat="server" ResourceName="CP_PhotosAdmin_SectionEdit_Tab_Settings" /></Tab>
		<Content>
		<CP:SectionDetails id="SectionDetails1" runat="Server"></CP:SectionDetails>
		</Content>
	</TWC:TabbedPane>
	<TWC:TabbedPane runat="server">
		<Tab><CP:ResourceControl runat="server" ResourceName="CP_PhotosAdmin_SectionEdit_Tab_Permissions" /></Tab>
		<Content>
		<div class="CommonDescription"><br />
			<CP:ResourceControl id="Resourcecontrol1" runat="Server" resourcename="CP_PhotosAdmin_SectionEdit_PermissionsTip" />
		</div>
		<div id="GrayGrid">
        <asp:Repeater runat="server" ID="PermissionList">
            <HeaderTemplate>
                <table class="Grid" cellSpacing=0 cellPadding=0 width="100%">
                <tbody>
                    <tr class="HeadingRow HeadingCellText HeadingCell">
                        <th><CP:ResourceControl runat="server" ResourceName="CP_Photos_GridCol_Name" /></th>
                        <th><CP:ResourceControl runat="server" ResourceName="CP_Photos_GridCol_View" /></th>
                        <th><CP:ResourceControl runat="server" ResourceName="CP_Photos_GridCol_Reply" /></th>
                        <th><CP:ResourceControl runat="server" ResourceName="CP_Photos_GridCol_Post" /></th>
                    </tr>    
            </HeaderTemplate>
            <ItemTemplate>
                    <tr class="Row">
                        <td><%# Eval("Name") %><asp:HiddenField ID="RoleId" runat="server" Value='<%# Eval("RoleIDString") %>' Visible="false" /></td>
                        <td><asp:CheckBox runat="server" ID="ViewCheckbox" Checked='<%# Eval("View") %>' /></td>
                        <td><asp:CheckBox runat="server" ID="ReplyCheckbox" Checked='<%# Eval("Reply") %>' /></td>
                        <td><asp:CheckBox runat="server" ID="PostCheckbox" Checked='<%# Eval("Post") %>' /></td>
                    </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                    <tr class="AlternatingRow">
                        <td><%# Eval("Name") %><asp:HiddenField ID="RoleId" runat="server" Value='<%# Eval("RoleIDString") %>' Visible="false" /></td>
                        <td><asp:CheckBox runat="server" ID="ViewCheckbox" Checked='<%# Eval("View") %>' /></td>
                        <td><asp:CheckBox runat="server" ID="ReplyCheckbox" Checked='<%# Eval("Reply") %>' /></td>
                        <td><asp:CheckBox runat="server" ID="PostCheckbox" Checked='<%# Eval("Post") %>' /></td>
                    </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        </div>
		</Content>
	</TWC:TabbedPane>
	<TWC:TabbedPane runat="server">
		<Tab><CP:ResourceControl runat="server" ResourceName="CP_PhotosAdmin_SectionEdit_Tab_LayoutOptions" /></Tab>
		<Content>
		<CP:Layout id="Layout1" runat="Server" />
		</Content>
	</TWC:TabbedPane>
	<TWC:TabbedPane runat="server">
		<Tab><CP:ResourceControl runat="server" ResourceName="CP_PhotosAdmin_SectionEdit_Tab_Quotas" /></Tab>
		<Content>
		<CP:LocalQuotas id="Quotas1" runat="Server" />
		</Content>
	</TWC:TabbedPane>
</TWC:TabbedPanes>
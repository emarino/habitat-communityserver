<%@ Page language="c#" Codebehind="FileEditor.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Files.FileEditor" %>

<CP:ControlPanelSelectedNavigation SelectedNavItem="Files" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="FileControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server"><CP:ResourceControl runat="server" ResourceName="CP_Files_FileEditor_Title" /></CP:Content>
<CP:Content id="TaskRegion" runat="Server">
		
<script type="text/javascript">
	// <![CDATA[
function SelectPreview()
{
    $('PreviewBody').innerHTML = '';
    Element.show('loading');
    FileEditor.PreviewPost(<%= PostBody.GetContentScript() %>,previewCallBack);
    $('PreviewTitle').innerHTML = $('<%= PostSubject.ClientID %>').value;
}

function previewCallBack(res)
{
    Element.hide('loading');
    $('PreviewBody').innerHTML = res.value;
}
 
function getAttachmentData(result)
{
	// result = new Array(TemporaryPostAttachmentGuid, FileName, FriendlyFileName, IsRemote)
	if (result != null)
	{
		document.getElementById('<%= TempAttachmentGuid.ClientID %>').value = result[0] + ':' + result[1];
		document.getElementById('<%= FileName.ClientID %>').innerHTML = result[1];
	
		document.getElementById('<%= UploadFile.ClientID %>').innerHTML = '<CP:ResourceControl ResourceName="CP_Files_FileEditor_FileOrUrl_UpdateUpload" runat="server" />';
	}
}
// ]]>  
</script>
<br>

<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Files" />

<TWC:TabbedPanes id="EditorTabs" runat="server"
	PanesCssClass="CommonPane"
	TabSetCssClass="CommonPaneTabSet"
	TabCssClasses="CommonPaneTab,CommonPaneTab1,CommonPaneTab2"
	TabSelectedCssClasses="CommonPaneTabSelected,CommonPaneTabSelected1,CommonPaneTabSelected2"
	TabHoverCssClasses="CommonPaneTabHover,CommonPaneTabHover1,CommonPaneTabHover2"
	>
	<TWC:TabbedPane runat="server">
		<Tab><CP:ResourceControl runat="server" ResourceName="CP_Files_FileEditor_Tab_File" /></Tab>
		<Content>
			<div class="CommonFormFieldName">
				<CP:ResourceControl runat="server" ResourceName="CP_Files_FileEditor_FileOrUrlField" ID="Resourcelabel2"/>
			</div>
			<div class="CommonFormField">
				<asp:Label ID="FileName" Runat="server" />
				<input type="hidden" id="TempAttachmentGuid" runat="server" NAME="TempAttachmentGuid"/>
				<cp:ModalLink ID="UploadFile" Runat="server" CssClass="CommonTextButton" Width="500" Height="300" Callback="getAttachmentData" />
				<span runat="server" id="FileValidation" class="CommonValidationWarning" style="display: none;">*</span>
			</div>
			<p />
			<div class="CommonFormFieldName">
				<CP:FormLabel id="tt" runat="Server" ControlToLabel="PostSubject" ResourceName="CP_Files_FileEditor_TitleField" />
				<asp:requiredfieldvalidator id="postSubjectValidator" runat="server" CssClass="validationWarning" ControlToValidate="PostSubject">*</asp:requiredfieldvalidator>
			</div>
			<div class="CommonFormField">
				<asp:TextBox id="PostSubject" CssClass="ControlPanelTextInput" MaxLength="256" runat="server" />
			</div>
			<p />
			<div class="CommonFormFieldName">
				<CP:FormLabel id="Formlabel1" runat="Server" ControlToLabel="PostBody" ResourceName="CP_Files_FileEditor_BodyField" />
				<asp:requiredfieldvalidator id="Requiredfieldvalidator1" Display="Dynamic" runat="server" CssClass="validationWarning"
					ControlToValidate="PostBody">*</asp:requiredfieldvalidator>
			</div>
			<div class="CommonFormField">
				<CSControl:Editor runat="Server" id="PostBody" width="100%" columns="110" />
			</div>
			<p />
			<div class="CommonFormFieldName">
				<cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_Files_FileEditor_CategoriesField_Detail" />
				<CP:FormLabel id="Formlabel3" runat="Server" ResourceName="CP_Files_FileEditor_CategoriesField" />
			</div>
			<div class="CommonFormField">
				<cp:TagEditor runat="server" id="Tags">
				    <SkinTemplate>
                        <table cellpadding="0" cellspacing="0" border="0">
                        <tr valign="top">
                        <td>
	                        <asp:TextBox Columns="70" ID="Tags" runat="server" />
                        </td>
                        <td>
	                        <cp:Modal modaltype="Button" width="400" height="300" runat="Server" id="SelectTags" ResourceName="TagEditor_SelectTags" />
                        </td>
                        </tr>
                        </table>
				    </SkinTemplate>
				</cp:TagEditor>
			</div>
		</Content>
	</TWC:TabbedPane>
	<TWC:TabbedPane runat="server">
		<Tab><CP:ResourceControl runat="server" ResourceName="CP_Files_FileEditor_Tab_Options" /></Tab>
		<Content>
			<table cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td class="CommonFormFieldName">
						<cp:helpicon id="Helpicon4" runat="Server" resourcename="CP_Files_FileEditor_Published_Detail" />
						<cp:formlabel id="Formlabel6" runat="Server" controltolabel="ynPublished" resourcename="CP_Files_FileEditor_Published" />
					</td>
					<td class="CommonFormField">
						<cp:yesnoradiobuttonlist id="IsApproved" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput1" />
					</td>
				</tr>
			</table>
		</Content>
	</TWC:TabbedPane>
	<TWC:TabbedPane runat="server">
		<Tab OnClickClientFunction="SelectPreview"><CP:ResourceControl runat="server" ResourceName="CP_Files_FileEditor_Tab_Preview" /></Tab>
		<Content>
			<h3 class="CommonSubTitle" id="PreviewTitle"></h3>
			<span id="loading" style="display:none">Loading <img src = "<%=CommunityServer.Components.SiteUrls.Instance().LoadingImageUrl%>"/></span>
			<span id="PreviewBody"></span>
		</Content>
	</TWC:TabbedPane>
</TWC:TabbedPanes>

<div id="ErrorPanel" class="CommonMessageError" runat="server" visible="False">
	<CP:ResourceControl runat="Server" id="ErrorMessage" />
</div>
<div id="Post" runat="server" visible="true">
</div>
<p class="PanelSaveButton DetailsFixedWidth">
	<cp:ResourceLinkButton id="PostButton" runat="Server" CssClass="CommonTextButtonBig" ResourceName="Save"></cp:ResourceLinkButton>
</p>

<script type="text/javascript">
// <![CDATA[
function validateFile()
{
	if (!document.getElementById('<%= TempAttachmentGuid.ClientID %>').value)
	{
		document.getElementById('<%= FileValidation.ClientID %>').style.display = 'inline';
		return false;
	}
	else
	{
		document.getElementById('<%= FileValidation.ClientID %>').style.display = 'none';	
		return true;
	}	
}
// ]]>
</script>

</CP:Content>
</CP:Container>

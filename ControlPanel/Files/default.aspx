<%@ Page language="c#" Codebehind="default.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Files._default" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="FileControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server"><CP:ResourceControl runat="server" ResourceName="CP_Files_Default_Title" /></CP:Content>
	<CP:Content id="TaskRegion" runat="Server">

			<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Files" />
			<fieldset>
			<TABLE cellSpacing="0" cellPadding="0" border="0">
				<TR>
					<TD class="CommonFormFieldName"><CP:ResourceControl runat="server" ResourceName="CP_Files_Default_Owners" /></TD>
					<TD class="CommonFormField">
						<asp:Literal id="Owners" runat="Server" /></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName"><CP:ResourceControl runat="server" ResourceName="CP_Files_Default_Created" /></TD>
					<TD class="CommonFormField">
						<asp:Literal id="DateCreated" runat="Server" /></TD>
				</TR>
			</TABLE>
			</fieldset>

		<P />

				<fieldset>
				<legend><CP:ResourceControl runat="server" ResourceName="CP_Files_Default_FilesHeader" /></legend>
				<TABLE cellSpacing="0" cellPadding="0" border="0">
					<TR>
						<TD class="CommonFormFieldName"><CP:ResourceControl runat="server" ResourceName="CP_Files_Default_FileCount" /></TD>
						<TD class="CommonFormField">
							<asp:Literal id="PostCount" runat="Server" /></TD>
					</TR>
					<TR>
						<TD class="CommonFormFieldName"><CP:ResourceControl runat="server" ResourceName="CP_Files_Default_MostRecentFile" /></TD>
						<TD class="CommonFormField">
							<asp:PlaceHolder runat="server" id="RecentPostArea">
								<CP:ResourceControl runat="server" ResourceName="CP_By" />&nbsp; 
								<asp:Literal id="PostRecentAuthor" runat="Server" />&nbsp; 
								<CP:ResourceControl runat="server" ResourceName="CP_On" />&nbsp; 
								<asp:Literal id="PostRecentDate" runat="Server" />
							</asp:PlaceHolder>
							<CP:ResourceControl id="NoRecentPost" runat="server" ResourceName="CP_None" visible="false"></CP:ResourceControl>
						</TD>
					</TR>
				</TABLE>
				</fieldset>

		<P />

				<fieldset>
				<legend><CP:ResourceControl runat="server" ResourceName="CP_Files_Default_CommentsHeader" /></legend>
				<TABLE cellSpacing="0" cellPadding="0" border="0">
					<TR>
						<TD class="CommonFormFieldName"><CP:ResourceControl runat="server" ResourceName="CP_Files_Default_Comments" /></TD>
						<TD class="CommonFormField">
							<asp:Literal id="CommentCount" runat="Server" /></TD>
					</TR>
				</TABLE>
				</fieldset>

	</CP:Content>
</CP:Container>

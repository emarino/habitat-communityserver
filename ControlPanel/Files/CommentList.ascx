<%@ Control Language="c#" AutoEventWireup="false" Codebehind="CommentList.ascx.cs" Inherits="CommunityServer.ControlPanel.Files.CommentList" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Import Namespace="CommunityServer.Components" %>
<script type="text/javascript">
  // <![CDATA[
  function reloadComments()
  {
	window.location = window.location;
  }
  
  function onCallbackError(excString)
  {
    if (confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_CallBackWarning") %>')) alert(excString); 
    <%= Grid1.ClientID %>.Page(1); 
  }

  function onDelete(item)
  {
      return confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Files_CommentList_DeleteWarning") %>'); 
  }

  function deleteRow(rowId)
  {
    <%= Grid1.ClientID %>.Delete(<%= Grid1.ClientID %>.GetRowFromClientId(rowId)); 
  }
// ]]>
</script>
<script type="text/javascript">
// <![CDATA[
	function validate_click()
	{
		var element = $('<%=  ActionList.ClientID %>');
		
		if(element.options[0].selected)
		    return false;
		
		if(element.options[3].selected)
		{
			return confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Files_CommentList_DeleteSelectedWarning") %>');
		}
		return true;
	}
	// ]]>
</script>
<div id="Filters">
		<CP:ResourceControl id="FeedbackFilterLabel" runat="Server" resourcename="CP_Files_CommentList_FilterBy"></CP:ResourceControl>
		<asp:dropdownlist id="filterPublished" runat="server"></asp:dropdownlist>&nbsp;
		<CP:ResourceButton id="FilterButton" Runat="server" ResourceName="CP_ApplyFilter" />
</div>
<div id="GrayGrid">
	<CA:GRID id="Grid1" runat="server" ClientSideOnCallbackError="onCallbackError"
		ClientSideOnDelete="onDelete" AutoCallBackOnDelete="true"
		EditOnClickSelectedItem="false" CssClass="Grid">
		<Levels>
			<CA:GridLevel DataKeyField="PostID">
				<Columns>
					<CA:GridColumn ColumnType="CheckBox" Align="Center" AllowEditing="True" AllowSorting="false" />
					<CA:GridColumn DataField="Subject" HeadingText="ResourceManager.CP_Files_CommentGrid_Title" DataCellClientTemplateId="CommentTemplate" TextWrap="True" AllowSorting="false" />
					<CA:GridColumn DataCellServerTemplateId="PostDateTemplate" HeadingText="ResourceManager.CP_Files_CommentGrid_PublishedDate" AllowSorting="false" />
					<CA:GridColumn DataField="IsApproved" HeadingText="ResourceManager.CP_Files_CommentGrid_Approved" AllowSorting="false" />
					<CA:GridColumn DataField="PostID" Visible="False" />
					<CA:GridColumn DataField="PostDate" Visible="False" />
					<CA:GridColumn DataField="SectionID" Visible="False" />
					<CA:GridColumn DataField="DisplayName" Visible="False" />
					<CA:GridColumn DataField="ViewAuthorURL" Visible="False" />
					<CA:GridColumn DataField="ForceExcerpt" Visible="False" />
					<CA:GridColumn DataField="ViewPostURL" Visible="False" />
					<CA:GridColumn HeadingText="ResourceManager.CP_Files_CommentGrid_Actions" AllowGrouping="false" AllowSorting="false"
						DataCellClientTemplateId="EditTemplate" EditControlType="EditCommand" Align="Center" DataCellCssClass="LastDataCell"  TextWrap="True" />
				</Columns>
			</CA:GridLevel>
		</Levels>
		<ClientTemplates>
			<CA:ClientTemplate Id="CommentTemplate">
			<a href="javascript:Telligent_Modal.Open('commenteditor.aspx?cid=##  DataItem.GetMember("PostID").Text ##', 625, 475, reloadComments);"><strong>## DataItem.GetMember("Subject").Text ##</strong></a><br />## DataItem.GetMember("ForceExcerpt").Text ##
        </CA:ClientTemplate>
			<CA:ClientTemplate Id="EditTemplate">
			<a href="## DataItem.GetMember('ViewPostURL').Text  ##" class="CommonTextButton">View</a>
			<a href="javascript:Telligent_Modal.Open('commenteditor.aspx?cid=##  DataItem.GetMember("PostID").Text ##', 625, 475, reloadComments);" class="CommonTextButton">Edit</a>
			<a href="javascript:deleteRow('## DataItem.ClientId ##')" class="CommonTextButton">Delete</a>
		</CA:ClientTemplate>
			<CA:ClientTemplate Id="EditCommandTemplate">
            <a href="javascript:editRow();" class="CommonTextButton">Update</a>
            <a href="javascript:editRowCancel();" class="CommonTextButton">Cancel</a>
        </CA:ClientTemplate>
			<CA:ClientTemplate Id="InsertCommandTemplate">
            <a href="javascript:insertRow();" class="CommonTextButton">Insert</a>
            <a href="javascript:editRowCancel();" class="CommonTextButton">Cancel</a>
        </CA:ClientTemplate>
		</ClientTemplates>
		<ServerTemplates>
		    <CA:GridServerTemplate id="PostDateTemplate">
		        <Template>
		        <table width="100%">
				    <tr>
					    <td class="CellText" align="right"> 
					        <CP:ResourceControl runat="server" ResourceName="CP_By" /> 
					        <a style="color:#595959;" href=<%# Container.DataItem["ViewAuthorURL"]%>><b><%# Container.DataItem["DisplayName"]%></b></a>
					    </td>
				    </tr>
				    <tr>
					    <td class="CellText" align="right">
					        <font color="#595959"><%# UserTime.ConvertToUserTime((DateTime)Container.DataItem["PostDate"]).ToString(CSContext.Current.User.Profile.DateFormat + " hh:mm tt")%></font></td>
				    </tr>
			    </table>
		        </Template>
		    </CA:GridServerTemplate>
		</ServerTemplates>
	</CA:GRID></div>
<p id="Actions"  class="PanelSaveButton"><cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_Moderate_Help"></cp:helpicon>&nbsp;<asp:dropdownlist id="ActionList" runat="server"></asp:dropdownlist>&nbsp;<CP:ResourceButton id="ActionButton" runat="Server"  ResourceName="CP_Go"></CP:ResourceButton></p>

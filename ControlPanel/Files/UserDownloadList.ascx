<%@ Control Language="c#" AutoEventWireup="false" Codebehind="UserDownloadList.ascx.cs" Inherits="CommunityServer.ControlPanel.Files.UserDownloadList" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="Panel" TagName = "ActivityQueryControl" Src = "~/ControlPanel/Reporting/ActivityQueryControl.ascx" %>
<script type="text/javascript">
  function onCallbackError(excString)
  {
    if (confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_CallBackWarning") %>')) alert(excString); 
    <%= Grid1.ClientID %>.Page(1); 
  }
</script>
<table cellSpacing="0" cellPadding="2" width="100%" border="0">
	<tr>
		<td>
			<panel:ActivityQueryControl id="ActivityQuery" runat="Server" ></panel:ActivityQueryControl>
		</td>
	</tr>
</table>
<p />
<div id="GrayGrid">
	<CA:GRID id="Grid1" runat="server" RunningMode="Callback" ClientSideOnCallbackError="onCallbackError" CssClass="Grid">
		<Levels>
			<CA:GridLevel DataKeyField="PostID">
				<Columns>
					<CA:GridColumn DataField="Username" AllowSorting="false" HeadingText="ResourceManager.CP_Files_UserDownloadGrid_User" DataCellClientTemplateId="UserTemplate" TextWrap="True" />
					<CA:GridColumn DataField="UserDisplayName" AllowSorting="false" HeadingText="ResourceManager.CP_Files_UserDownloadGrid_UserDisplayName" TextWrap="True" />
					<CA:GridColumn DataField="UserEmail" AllowSorting="false" HeadingText="ResourceManager.CP_Files_UserDownloadGrid_UserEmail" TextWrap="True" />
					<CA:GridColumn DataField="TotalDownloads" AllowSorting="false" HeadingText="ResourceManager.CP_Files_UserDownloadGrid_TotalDownloads" />
					<CA:GridColumn DataField="LatestDownloadDate" AllowSorting="false" HeadingText="ResourceManager.CP_Files_UserDownloadGrid_LatestDownloadDate" FormatString="MMM dd yyyy, hh:mm tt" />
					<CA:GridColumn DataField="ViewUserURL" Visible="False" />
				</Columns>
			</CA:GridLevel>
		</Levels>
		<ClientTemplates>
			<CA:ClientTemplate Id="UserTemplate">
				<a href="## DataItem.GetMember('ViewUserURL').Text ##" target="_blank"><strong>## DataItem.GetMember('Username').Text ##</strong></a>
			</CA:ClientTemplate>
		</ClientTemplates>
	</CA:GRID>
</div>

<%@ Page language="c#" Codebehind="FileCategories.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Files.FileCategories" %>
<%@ Register TagPrefix="CP" TagName = "FileCategoryList" Src = "~/ControlPanel/Files/FileCategoryList.ascx" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="FileCategories" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="FileControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl runat="server" resourcename="CP_Files_FileCategories_Title" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonDescription">
			<CP:ResourceControl runat="server" resourcename="CP_Files_FileCategories_Description" />
		</div>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Files" />
		<CP:FileCategoryList id="Postlist1" runat="Server"></CP:FileCategoryList>
	</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="SyndicationOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Files.SyndicationOptions" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="SyndicationOptions" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="FileControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl runat="server" ResourceName="CP_Files_SyndicationOptions_Title" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonDescription">
			<CP:ResourceControl runat="server" ResourceName="CP_Files_SyndicationOptions_Description" />
		</div>
		<CP:StatusMessage runat="server" id="Status" />
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Files" />
		
		<div class="FixedWidthContainer">
			<table cellspacing="0" border="0" cellpadding="0">
			<tr>
			<td class="CommonFormFieldName">
				<CP:FormLabel runat="Server" ResourceName="CP_Files_SyndicationOptions_EnableRssSyndication" ControlToLabel="EnableRssSyndication" ID="Formlabel3"/>
			</td>
			<td class="CommonFormField">
				<asp:CheckBox Runat="Server" ID="EnableRssSyndication" />
			</td>
			</tr>
			<tr>
			<td class="CommonFormFieldName">
				<CP:FormLabel runat="Server" ResourceName="CP_Files_SyndicationOptions_EnableTagsRssSyndication" ControlToLabel="EnableTagsRssSyndication" ID="Formlabel1"/>
			</td>
			<td class="CommonFormField">
				<asp:CheckBox Runat="Server" ID="EnableTagsRssSyndication" />
			</td>
			</tr>
			</table>
		</div>
		<p class="PanelSaveButton DetailsFixedWidth">
				<cp:ResourceLinkButton id="SaveButton" runat="Server" CssClass="CommonTextButton" ResourceName="Save"></cp:ResourceLinkButton></p>
	</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="UserDownloads.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Files.UserDownloads" %>
<%@ Register TagPrefix="CP" TagName = "DownloadList" Src = "~/ControlPanel/Files/UserDownloadList.ascx" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="Files" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="FileControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<asp:Literal runat="server" id="DownloadsTitle" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonDescription">
			<CP:ResourceControl runat="server" ResourceName="CP_Files_UserDownloads_Description" ID="Resourcecontrol2" NAME="Resourcecontrol2"/>
		</div>
		<CP:DownloadList id="DownloadList1" runat="Server"></CP:DownloadList>
	</CP:Content>
</CP:Container>

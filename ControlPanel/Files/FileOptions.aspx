<%@ Page language="c#" Codebehind="FileOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Files.FileOptions" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="FileOptions" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="FileControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl runat="server" ResourceName="CP_Files_FileOptions_Title" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonDescription">
				<CP:ResourceControl runat="server" ResourceName="CP_Files_FileOptions_Description" />
		</div>
		<CP:StatusMessage runat="server" id="Status" />
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Files" />
		
		<div class="FixedWidthContainer">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td class="CommonFormFieldName">
						<cp:helpicon id="Helpicon2" runat="Server" resourcename="CP_Files_FileOptions_ModerateFiles_Help" />
						<CP:FormLabel runat="Server" ResourceName="CP_Files_FileOptions_ModerateFiles" ControlToLabel="ModerateFiles" ID="Formlabel1"/>
					</td>
					<td class="CommonFormField"><cp:YesNoRadioButtonList id="ModerateFiles" runat="server" CssClass="txt1" RepeatColumns="2" /></td>
				</tr>
				<tr>
					<td class="CommonFormFieldName">
						<cp:helpicon id="Helpicon6" runat="Server" resourcename="CP_Files_FileOptions_EnableFileNotifications_Help" />
						<cp:formlabel runat="Server" controltolabel="EnableFileNotification" resourcename="CP_Files_FileOptions_EnableFileNotifications" ID="Formlabel5" />
					</td>
					<td class="CommonFormField"><cp:YesNoRadioButtonList id="EnableFileNotification" runat="server" CssClass="txt1" RepeatColumns="2" /></td>
				</tr>
				<tr>
					<td class="CommonFormFieldName">
						<cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_Files_FileOptions_EnableRatings_Help" />
						<CP:FormLabel runat="Server" ResourceName="CP_Files_FileOptions_EnableRatings" ControlToLabel="EnableRatings" ID="Formlabel4"/>
					</td>
					<td class="CommonFormField"><cp:YesNoRadioButtonList id="EnableRatings" runat="server" CssClass="txt1" RepeatColumns="2" /></td>
				</tr>
				<tr>
					<td class="CommonFormFieldName">
						<cp:helpicon id="Helpicon3" runat="Server" resourcename="CP_Files_FileOptions_EnableComments_Help" />
						<CP:FormLabel runat="Server" ResourceName="CP_Files_FileOptions_EnableComments" ControlToLabel="EnableComments" ID="Formlabel2"/>
					</td>
					<td class="CommonFormField"><cp:YesNoRadioButtonList id="EnableComments" runat="server" CssClass="txt1" RepeatColumns="2" /></td>
				</tr>
				<tr>
					<td class="CommonFormFieldName">
						<cp:helpicon id="Helpicon4" runat="Server" resourcename="CP_Files_FileOptions_ModerateComments_Help" />
						<CP:FormLabel runat="Server" ResourceName="CP_Files_FileOptions_ModerateComments" ControlToLabel="ModerateComments" ID="Formlabel3"/>
					</td>
					<td class="CommonFormField"><cp:YesNoRadioButtonList id="ModerateComments" runat="server" CssClass="txt1" RepeatColumns="2" /></td>
				</tr>
				<tr>
					<td class="CommonFormFieldName">
						<cp:helpicon id="Helpicon5" runat="Server" resourcename="CP_Files_FileOptions_EnableCommentNotifications_Help" />
						<cp:formlabel runat="Server" controltolabel="EnableCommentNotification" resourcename="CP_Files_FileOptions_EnableCommentNotifications" />
					</td>
					<td class="CommonFormField"><cp:YesNoRadioButtonList id="EnableCommentNotification" runat="server" CssClass="txt1" RepeatColumns="2" /></td>
				</tr>
			</table>
		</div>
		<p class="PanelSaveButton DetailsFixedWidth">
			<cp:ResourceLinkButton id="SaveButton" runat="Server" CssClass="CommonTextButton" ResourceName="Save"></cp:ResourceLinkButton></p>
	</CP:Content>
</CP:Container>

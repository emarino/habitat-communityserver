<%@ Page language="c#" Codebehind="Comments.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Files.Comments" %>
<%@ Register TagPrefix="CP" TagName = "CommentList" Src = "~/ControlPanel/Files/CommentList.ascx" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="Comments" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="FileControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl runat="server" resourcename="CP_Files_Comments_Title" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonDescription">
			<CP:ResourceControl runat="server" resourcename="CP_Files_Comments_Description" />
		</div>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Files" />
		<CP:CommentList id="Postlist1" runat="Server"></CP:CommentList>
	</CP:Content>
</CP:Container>


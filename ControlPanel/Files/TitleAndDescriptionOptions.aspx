<%@ Page language="c#" Codebehind="TitleAndDescriptionOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Files.TitleAndDescriptionOptions" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="TitleAndDescriptionOptions" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="FileControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl ResourceName="CP_Files_TitleAndDescriptionOptions_Title" runat="server" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<CP:StatusMessage runat="server" id="Status" />
		<div class="CommonDescription">
			<CP:ResourceControl ResourceName="CP_Files_TitleAndDescriptionOptions_Description" runat="server" />
		</div>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Files" />
		
		<div class="FixedWidthContainer">
			<div class="CommonFormFieldName">
				<CP:FormLabel runat="server" ResourceName="CP_Files_TitleAndDescriptionOptions_TitleField" ControlToLabel="GalleryTitle" id="FormLabel1" />
			</div>
			<div class="CommonFormField">
				<asp:TextBox Runat="Server" id="GalleryTitle" CssClass="ControlPanelTextInput" />
			</div>
			
			<p />
			<div class="CommonFormFieldName">
				<CP:FormLabel runat="server" ResourceName="CP_Files_TitleAndDescriptionOptions_DescriptionField" ControlToLabel="Description" id="FormLabel2" />
			</div>
			<div class="CommonFormField">
				<CSControl:Editor runat="Server" id="Description" Height="225px" />
			</div>
		</div>
		<p class="PanelSaveButton DetailsFixedWidth">
				<cp:ResourceLinkButton id="SaveButton" runat="Server" CssClass="CommonTextButton" ResourceName="Save"></cp:ResourceLinkButton>
			</p>
	</CP:Content>
</CP:Container>


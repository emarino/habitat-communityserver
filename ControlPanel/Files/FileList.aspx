<%@ Page language="c#" Codebehind="FileList.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Files.FileList" %>
<%@ Register TagPrefix="CP" TagName = "FileList" Src = "~/ControlPanel/Files/FileList.ascx" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="Files" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="FileControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl runat="server" ResourceName="CP_Files_FileList_Title" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonDescription">
			<CP:ResourceControl runat="server" ResourceName="CP_Files_FileList_Description" />
		</div>
		<CP:FileList id="Postlist1" runat="Server"></CP:FileList>
	</CP:Content>
</CP:Container>


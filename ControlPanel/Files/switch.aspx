<%@ Register TagPrefix="CP" TagName = "SectionList" Src = "~/ControlPanel/Files/SwitchControl.ascx" %>
<%@ Page language="c#" Codebehind="switch.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Files._switch" %>
<CP:ControlPanelSelectedNavigation id="SelectedNavigation1" runat="server" SelectedTab="Files" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelMaster.ascx">
	<CP:Content id="PanelNavigation" runat="server">
		<CP:ControlPanelNavigationSidebar id="Controlpanelnavigationsidebar1" runat="server"></CP:ControlPanelNavigationSidebar>
	</CP:Content>
	<CP:Content id="TopLeft" runat="server">
        <table cellpadding="0" cellspacing="0" border="0">
        <tr>
        <td  width="100%"><CP:ResourceControl runat="server" ResourceName="CP_Files_CurrentFolder" /></td>
        <td  style="padding-right: 3px;" nowrap="nowrap"><asp:HyperLink ID="HyperLink1" Runat="server" CssClass="CommonTextButton" NavigateUrl="~/ControlPanel/Files/Switch.aspx"><CP:ResourceControl runat="server" ResourceName="CP_Files_ChangeFolder" /></asp:HyperLink></td>
        </tr>
        </table>
	</CP:Content>
	<CP:Content id="PanelDescription" runat="server">
		<CP:ResourceControl id="ResourceControl1" runat="server" ResourceName="CP_Files_Title"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="ResourceControl2" runat="server" ResourceName="CP_Files_Switch_Title"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<CP:ResourceControl id="ResourceControl3" runat="server" ResourceName="CP_Files_Switch_Description"></CP:ResourceControl></DIV>
		<CP:StatusMessage id="Status" runat="server" Visible="false"></CP:StatusMessage>
		<CP:SectionList id="SectionList1" runat="Server"></CP:SectionList>
	</CP:Content>
</CP:Container>

<%@ Control Language="c#" AutoEventWireup="false" Codebehind="SwitchControl.ascx.cs" Inherits="CommunityServer.ControlPanel.Files.SwitchControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<DIV id="GrayGrid">
	<CA:GRID id="Grid1" runat="Server" RunningMode="Client" >
		<LEVELS>
			<CA:GRIDLEVEL datakeyfield="SectionID">
				<COLUMNS>
					<CA:GRIDCOLUMN datafield="ApplicationKey" HeadingText="ResourceManager.CP_Files_SwitchGrid_AppKey" />
					<CA:GRIDCOLUMN datafield="Name" HeadingText="ResourceManager.CP_Files_SwitchGrid_Title" />
					<CA:GRIDCOLUMN datafield="Description" HeadingText="ResourceManager.CP_Files_SwitchGrid_Description" visible="false"/>
					<CA:GRIDCOLUMN datafield="Owners" HeadingText="ResourceManager.CP_Files_SwitchGrid_Owners" width="150" />
					<CA:GRIDCOLUMN datafield="GroupName" HeadingText="ResourceManager.CP_Files_SwitchGrid_Group" width="150" />
					<CA:GRIDCOLUMN datafield="IsActive" HeadingText="ResourceManager.CP_Files_SwitchGrid_Enabled" DataCellCssClass="LastDataCell" />
					<CA:GRIDCOLUMN datafield="SectionID" visible="false" />
				</COLUMNS>
			</CA:GRIDLEVEL>
		</LEVELS>
	</CA:GRID>
</DIV>

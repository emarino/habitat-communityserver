<%@ Control Language="c#" AutoEventWireup="false" Codebehind="FileCategoryList.ascx.cs" Inherits="CommunityServer.ControlPanel.Files.FileCategoryList" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Import Namespace="CommunityServer.Components" %>
<script language = "javascript" type="text/javascript">

 
  function reloadCategories()
  {
	window.location = window.location;
  }

  function onCallbackError(excString)
  {
    if (confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_CallBackWarning") %>')) alert(excString); 
    <%= Grid1.ClientID %>.Page(1); 
  }

  function onDelete(item)
  {
	return confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Files_FileCategoryList_Delete") %>'); 
  }
  function deleteRow(rowId)
  {
    <%= Grid1.ClientID %>.Delete(<%= Grid1.ClientID %>.GetRowFromClientId(rowId)); 
  }

</script>

<div id="Filters">
	<cp:ModalLink CssClass="CommonTextButton" ModalType="Link" Height="300" Width="400" runat="Server" Callback="reloadCategories" id="NewCategory" ><CP:ResourceControl ResourceName="CP_Files_FileCategoryList_New" Runat="server" /></cp:ModalLink>
</div>

<div id="GrayGrid" >
<CA:Grid id="Grid1" runat="Server" 
	AutoCallBackOnDelete="true" 
	ClientSideOnDelete="onDelete" 
	ClientSideOnCallbackError="onCallbackError" 
	RunningMode="Client" 
	CssClass="Grid"
	>
    <Levels>
        <CA:GridLevel DataKeyField="CategoryID">
            <Columns>
                <CA:GridColumn DataField="Name" HeadingText="ResourceManager.CP_Files_FileCategoryGrid_Name" />
                <CA:GridColumn DataField="Description" HeadingText="ResourceManager.CP_Files_FileCategoryGrid_Description" />
                <CA:GridColumn DataField="MostRecentPostDate" HeadingText="ResourceManager.CP_Files_FileCategoryGrid_LastPost" FormatString="MMM dd yyyy, hh:mm tt" />
                <CA:GridColumn DataField="TotalThreads" HeadingText="ResourceManager.CP_Files_FileCategoryGrid_Posts" Align="Center" />
                <CA:GridColumn DataField="IsEnabled" HeadingText="ResourceManager.CP_Files_FileCategoryGrid_Published" />
                <CA:GridColumn DataField="CategoryID" Visible="False" />
                <CA:GridColumn DataField="ParentID" Visible="False" />
                <CA:GridColumn DataField="SectionID" Visible="False" />
                <CA:GridColumn DataField="Path" Visible="False" />
                <CA:GridColumn HeadingText="ResourceManager.CP_Files_FileCategoryGrid_Actions" AllowGrouping="false" AllowSorting="false" SortedDataCellCssClass="SortedDataCell" DataField="GalleryPostType" DataCellClientTemplateId = "ActionTemplate" Align="Center" />
            </Columns>
        </CA:GridLevel>
    </Levels>
    <ClientTemplates>
        <CA:ClientTemplate Id="ActionTemplate">
			<a href="javascript:Telligent_Modal.Open('CategoryForm.aspx?sectionid=##  DataItem.GetMember("SectionID").Text ##&categoryID=## DataItem.GetMember("CategoryID").Text ##', 400, 300, reloadCategories);" class="CommonTextButton">Edit</a>
            <a href="javascript:deleteRow('## DataItem.ClientId ##')" class="CommonTextButton">Delete</a>
        </CA:ClientTemplate>
    </ClientTemplates>
</CA:Grid>
</div>
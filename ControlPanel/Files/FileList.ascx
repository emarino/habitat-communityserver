<%@ Control Language="c#" AutoEventWireup="false" Codebehind="FileList.ascx.cs" Inherits="CommunityServer.ControlPanel.Files.FileList1" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Import Namespace="CommunityServer.Components" %>
<script type="text/javascript">
// <![CDATA[
function SelectColumn(column)
{
    alert(column.GetMember('PostID').Text);
}

function onCallbackError(excString)
{
	if (confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_CallBackWarning") %>')) alert(excString); 
	<%=	Grid1.ClientID %>.Page(1); 
}

function onDelete(item)
{
	return confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Files_FileList_Delete") %>'); 
}
function deleteRow(rowId)
{
	<%= Grid1.ClientID %>.Delete(<%= Grid1.ClientID %>.GetRowFromClientId(rowId)); 
}
// ]]>
</script>
<script type="text/javascript">
// <![CDATA[
	function validate_click()
	{
		var element = $('<%=  ActionList.ClientID %>');
		
		if(element.options[0].selected)
		    return false;
		
		if(element.options[3].selected)
		{
			return confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Files_FileList_DeleteSelected") %>');
		}
		return true;
	}
// ]]>
</script>
<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Files" />
	<table cellpadding="0" cellspacing="0" border="0"  width="100%">
		<tr>
			<td width="100%">
				<CP:ResourceControl resourcename="CP_Files_FileList_FilterBy" runat="Server" id="FeedbackFilterLabel"/>
				<asp:DropDownList id="CategoryList" Runat="Server" />
				<asp:DropDownList id="PublishedStatusList" Runat="Server" />
				<CP:ResourceButton runat="server" ID="Button1" ResourceName="CP_Files_FileList_ApplyFilter" />
			</td>
			<td nowrap="nowrap">
				<asp:LinkButton Runat="server" ID="NewPost" CssClass="CommonTextButton"><CP:ResourceControl runat="server" resourcename="CP_Files_FileList_New" ID="Resourcecontrol1" NAME="Resourcecontrol1"/></asp:LinkButton>
				<asp:LinkButton Runat="server" CssClass="CommonTextButton" ID="Categories"><CP:ResourceControl runat="server" resourcename="CP_Files_FileList_ManageCategories" ID="Resourcecontrol2" NAME="Resourcecontrol2"/></asp:LinkButton>
			</td>
		</tr>
	</table>
<br />

<div id="GrayGrid">
<CA:grid runat="Server" 
	id="Grid1" 
	CssClass="Grid" 
	RunningMode="Client" 
	AutoCallBackOnDelete="true" 
	ClientSideOnDelete="onDelete" 
	ClientSideOnCallbackError="onCallbackError" >
    <Levels>
        <CA:gridlevel datakeyfield="PostID" >
            <Columns>
				<CA:GridColumn ColumnType="CheckBox" Align="Center" AllowEditing="True" />
                <CA:GridColumn DataField="Subject" HeadingText="ResourceManager.CP_Files_FileGrid_Title" />
                <CA:GridColumn DataField="Views" HeadingText="ResourceManager.CP_Files_FileGrid_Views" Align = "Center" />
                <CA:GridColumn DataField="Downloads" HeadingText="ResourceManager.CP_Files_FileGrid_Downloads" Align = "Center" />
                <CA:GridColumn DataField="Replies" HeadingText="ResourceManager.CP_Files_FileGrid_Comments" Align = "Center" />
                <CA:GridColumn DataCellServerTemplateId="PostDateTemplate" HeadingText="ResourceManager.CP_Files_FileGrid_PublishedDate" AllowSorting="false" />
                <CA:GridColumn DataField="IsApproved" HeadingText="ResourceManager.CP_Files_FileGrid_Published" Align = "Center" />
                <CA:GridColumn DataField="PostDate" Visible="false" />
                <CA:GridColumn DataField="Username" Visible = "false" />
				<CA:GridColumn DataField="DisplayName" Visible="false" />
                <CA:GridColumn DataField="SectionID" Visible = "false" />
                <CA:GridColumn DataField="PostID" Visible = "false" />
                <CA:GridColumn DataField="BlogPostType" Visible = "false" />
                <CA:GridColumn DataField="PostID" Visible = "false" />
                <CA:GridColumn datafield="ViewPostURL" visible = "false" />
                <CA:GridColumn datafield="AuthorURL" visible = "false" />
                <CA:gridcolumn headingtext="ResourceManager.CP_Files_FileGrid_Actions" datacellclienttemplateid = "ActionTemplate" />
            </Columns>
        </CA:gridlevel>
    </Levels>
    <ClientTemplates>
        <CA:ClientTemplate Id="ActionTemplate">
            <a href="fileeditor.aspx?sectionid=## DataItem.GetMember('SectionID').Text ##&PostID=## DataItem.GetMember('PostID').Text ##" class="CommonTextButton">Edit</a>
            <a href="javascript:deleteRow('## DataItem.ClientId ##')" class="CommonTextButton">Delete</a>
            <a href="## DataItem.GetMember('ViewPostURL').Text ##" class="CommonTextButton">View</a>
            <a href="userdownloads.aspx?pid=## DataItem.GetMember('PostID').Text ##" class="CommonTextButton">Download(s) Report</a>
        </CA:ClientTemplate>
    </ClientTemplates>
    <ServerTemplates>
	    <CA:GridServerTemplate id="PostDateTemplate">
	        <Template>
	        <table width="100%" cellspacing="0" cellpadding="1" border="0">
			    <tr>
				    <td class="CellText" align="right"> 
				        <CP:ResourceControl runat="server" ResourceName="CP_By" /> 
				        
				        
				        <a style="color:#595959;" href=<%# Container.DataItem["AuthorUrl"]%>><b><%# Container.DataItem["DisplayName"]%></b></a>
				    </td>
			    </tr>
			    <tr>
				    <td class="CellText" align="right">
				        <font color="#595959"><%# UserTime.ConvertToUserTime((DateTime)Container.DataItem["PostDate"]).ToString(CSContext.Current.User.Profile.DateFormat + " hh:mm tt")%></font></td>
			    </tr>
		    </table>
	        </Template>
	    </CA:GridServerTemplate>
	</ServerTemplates>
</CA:grid>
</div>
<p id="Actions"  class="PanelSaveButton">
			<cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_Moderate_Help"></cp:helpicon>&nbsp;
			<asp:dropdownlist id="ActionList" runat="server"></asp:dropdownlist>&nbsp;
			<asp:button id="ActionButton" runat="Server" Text="Go"></asp:button>
</p>

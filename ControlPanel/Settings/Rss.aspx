<%@ Page language="c#" Codebehind="Rss.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Settings.Rss" %>
<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="CP_Settings_RSS_Title"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<cp:statusmessage id="Status" runat="server"></cp:statusmessage>
		<DIV class="CommonDescription"></DIV>
		<DIV class="CommonFormArea">
		<div class="FixedWidthContainer">
			<TABLE cellSpacing="0" cellPadding="4" border="0">
				<TR>
					<TD class="CommonFormFieldName" width="200">
						<cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_Settings_RSS_Secure_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Settings_RSS_Secure" /></TD>
					<TD class="CommonFormField" noWrap>
						<cp:yesnoradiobuttonlist id="SecureRss" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput"></cp:yesnoradiobuttonlist></TD>
				</TR>
			</TABLE>
			<TABLE cellSpacing="0" cellPadding="4" border="0">
				<TR>
					<TD class="CommonFormFieldName" width="200">
						<cp:helpicon id="Helpicon2" runat="Server" resourcename="CP_Settings_RSS_Search_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Settings_RSS_Search" /></TD>
					<TD class="CommonFormField" noWrap>
						<cp:yesnoradiobuttonlist id="RssSearch" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput"></cp:yesnoradiobuttonlist></TD>
				</TR>
			</TABLE>
			</div>
			<p class="PanelSaveButton DetailsFixedWidth">
				<cp:resourcelinkbutton id="SaveButton" runat="server" resourcename="Save" cssclass="CommonTextButton" /></p>
		</DIV>
	</CP:Content>
</CP:Container>

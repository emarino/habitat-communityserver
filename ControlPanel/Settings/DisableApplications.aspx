<%@ Page language="c#" Codebehind="DisableApplications.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Settings.DisableApplications" %>
<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="Admin_SiteSettings_DisableApplications"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<CP:statusmessage id="Status" runat="server"></CP:statusmessage>
		<DIV class="CommonDescription"></DIV>
		<DIV class="CommonFormArea">
		<div class="FixedWidthContainer">
			<TABLE cellSpacing="0" cellPadding="2" border="0">
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon2" runat="Server" resourcename="Admin_SiteSettings_DisableForum_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_DisableForum" /></TD>
					<TD class="CommonFormField" noWrap>
						<cp:yesnoradiobuttonlist id="Forum" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput"></cp:yesnoradiobuttonlist></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon1" runat="Server" resourcename="Admin_SiteSettings_DisableBlog_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_DisableBlog" /></TD>
					<TD class="CommonFormField" noWrap>
						<cp:yesnoradiobuttonlist id="Weblog" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput"></cp:yesnoradiobuttonlist></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon3" runat="Server" resourcename="Admin_SiteSettings_DisableGallery_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_DisableGallery" /></TD>
					<TD class="CommonFormField" noWrap>
						<cp:yesnoradiobuttonlist id="Gallery" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput"></cp:yesnoradiobuttonlist></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon4" runat="Server" resourcename="Admin_SiteSettings_DisableFileGallery_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_DisableFileGallery" /></TD>
					<TD class="CommonFormField" noWrap>
						<cp:yesnoradiobuttonlist id="FileGallery" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput"></cp:yesnoradiobuttonlist></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon5" runat="Server" resourcename="Admin_SiteSettings_DisableFeedReader_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_DisableFeedReader" /></TD>
					<TD class="CommonFormField" noWrap>
						<cp:yesnoradiobuttonlist id="FeedReader" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput"></cp:yesnoradiobuttonlist></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon6" runat="Server" resourcename="Admin_SiteSettings_DisableBlogRoller_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_DisableBlogRoller"/></TD>
					<TD class="CommonFormField" noWrap>
						<cp:yesnoradiobuttonlist id="BlogRoller" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput"></cp:yesnoradiobuttonlist></TD>
				</TR>
			</TABLE>
			</div>
			<p class="PanelSaveButton DetailsFixedWidth">
				<cp:resourcelinkbutton id="SaveButton" runat="server" resourcename="Save" cssclass="CommonTextButton"></cp:resourcelinkbutton></p>
		</DIV>
	</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="Posts.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Settings.Posts" %>
<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
	<cp:resourcecontrol runat="server" resourcename="CP_Settings_Posts_Title" />
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">
	<cp:statusmessage id="Status" runat="server" />
	<DIV class="CommonDescription">
		<cp:resourcecontrol runat="server" resourcename="CP_Settings_Posts_SubTitle" />
	</DIV>
	<DIV class="CommonFormArea">
	<div class="FixedWidthContainer">
		<TABLE cellSpacing="0" cellPadding="2" border="0">
		<TR>
			<TD class="CommonFormFieldName">
				<cp:helpicon id="Helpicon12" runat="Server" resourcename="Admin_SiteSettings_AnonymousPosts_Descr" />
				<cp:resourcecontrol id="Resourcecontrol148" runat="server" resourcename="Admin_SiteSettings_AnonymousPosts" />
			</TD>
			<TD class="CommonFormField" noWrap>
				<cp:yesnoradiobuttonlist id="AnonymousUserPosting" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput" />
			</TD>
		</TR>
		<TR>
			<TD class="CommonFormFieldName">
				<cp:helpicon id="Helpicon8" runat="Server" resourcename="Admin_SiteSettings_Posting_EnableCensorship_Descr" />
				<cp:resourcecontrol id="Resourcecontrol9" runat="server" resourcename="Admin_SiteSettings_Posting_EnableCensorship" />
			</TD>
			<TD class="CommonFormField" nowrap="nowrap">
				<cp:yesnoradiobuttonlist id="EnableCensorship" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput" />
			</TD>
		</TR>
		<TR>
			<TD class="CommonFormFieldName">
				<cp:helpicon id="Helpicon2" runat="Server" resourcename="Admin_SiteSettings_Posting_Emoticons_Descr" />
				<cp:resourcecontrol id="ResourceControl73" runat="server" resourcename="Admin_SiteSettings_Posting_Emoticons" />
			</TD>
			<TD class="CommonFormField" nowrap="nowrap">
				<cp:yesnoradiobuttonlist id="EnableEmoticons" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput" />
			</TD>
		</TR>
		<TR>
			<TD class="CommonFormFieldName">
				<cp:helpicon id="Helpicon14" runat="Server" resourcename="Admin_SiteSettings_ForumRatingType_Descr" />
				<cp:resourcecontrol id="Resourcecontrol154" runat="server" resourcename="Admin_SiteSettings_ForumRatingType" />
			</TD>
			<TD class="CommonFormField" nowrap="nowrap">
				<cp:forumratingtyperadiobuttonlist id="ForumRatingType" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput" />
			</TD>
		</TR>
		<TR>
			<TD class="CommonFormFieldName">
				<cp:helpicon id="Helpicon10" runat="Server" resourcename="Admin_SiteSettings_Posting_UserActivityDisplay_Descr" />
				<cp:resourcecontrol id="Resourcecontrol130" runat="server" resourcename="Admin_SiteSettings_Posting_UserActivityDisplay" />
			</TD>
			<TD class="CommonFormField" nowrap="nowrap">
				<cp:useractivitydisplayradiobuttonlist id="PostingActivityDisplay" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput" />
			</TD>
		</TR>
		<TR>
			<TD class="CommonFormFieldName">
				<cp:helpicon id="Helpicon11" runat="Server" resourcename="Admin_SiteSettings_Posting_DisplayRankAsPicture_Descr" />
				<cp:resourcecontrol id="Resourcecontrol146" runat="server" resourcename="Admin_SiteSettings_Posting_DisplayRankAsPicture" />
			</TD>
			<TD class="CommonFormField" nowrap="nowrap">
				<cp:yesnoradiobuttonlist id="DisplayUserRankAsPicture" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput" />
			</TD>
		</TR>
		<TR>
			<TD class="CommonFormFieldName">
				<cp:helpicon id="Helpicon1" runat="Server" resourcename="Admin_SiteSettings_SearchPerPage_Descr" />
				<cp:resourcecontrol id="ResourceControl1116" runat="server" resourcename="Admin_SiteSettings_SearchPerPage" />
			</TD>
			<TD class="CommonFormField">
				<asp:textbox id="SearchPerPage" runat="Server" maxlength="4"></asp:textbox>
				<asp:requiredfieldvalidator id="SearchPerPageValidator" runat="server" controltovalidate="SearchPerPage" font-bold="True" errormessage="*" />
			</TD>
		</TR>
		<TR>
			<TD class="CommonFormFieldName">
				<cp:helpicon id="Helpicon5" runat="Server" resourcename="Admin_SiteSettings_List_TopPosters_Descr" />
				<cp:resourcecontrol id="ResourceControl66" runat="server" resourcename="Admin_SiteSettings_List_TopPosters" />
			</TD>
			<TD class="CommonFormField">
				<asp:textbox id="TopPostersToDisplay" runat="server" cssclass="shorttxt" maxlength="3" />
				<asp:requiredfieldvalidator id="TopPostersToDisplayValidator" runat="server" controltovalidate="TopPostersToDisplay" font-bold="True" errormessage="*" />
			</TD>
		</TR>
		</TABLE>
	</div>
	<p class="PanelSaveButton DetailsFixedWidth">
		<cp:resourcelinkbutton id="SaveButton" runat="server" resourcename="Save" cssclass="CommonTextButton" />
	</p>
</DIV>
</CP:Content>
</CP:Container>

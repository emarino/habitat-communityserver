<%@ Page language="c#" Codebehind="FilesBrowseModal.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Settings.FilesBrowseModal" %>
<%@ Import Namespace="CommunityServer.Components" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelModalMaster.ascx" IsModal="true">
<CP:Content id="bcr" runat="server">
<script type="text/javascript">
// <![CDATA[
function closeModal()
{
	window.parent.Telligent_Modal.Close();
}

var url = null;
function ProcessUrl(cUrl)
{
    url = cUrl;
	if (url != null && content != null)
	    window.parent.Telligent_Modal.Close(new Array(content, url));
}

var content = null;
function ProcessContent(cContent)
{
    content = cContent;
    content = content.replace(/<\/?a[^>]*>/ig, '');
    if (url != null && content != null)
        window.parent.Telligent_Modal.Close(new Array(content, url));
}

// ]]>
</script>
<div class="CommonContentArea">
<div class="CommonContent">
<CSControl:SelectContentForm runat="server" 
    ContentUrlSelectedClientFunction="ProcessUrl" 
    ContentSelectedClientFunction="ProcessContent" 
    BrowseableListType="CommunityServer.Components.Providers.SiteFilesBrowseableList, CommunityServer.Components"
    ItemsAreaHtmlGenericControlId="ItemsArea" 
    NavigationTreeId="Tree" 
    OptionsAreaHtmlGenericControlId="OptionsArea" 
    SelectButtonId="SelectButton"
    ItemCssClass="CommonContentSelectorItem" 
	SelectedItemCssClass="CommonContentSelectorItemSelected" 
	ItemNameCssClass="CommonContentSelectorItemName"
	SelectedItemNameCssClass="CommonContentSelectorItemNameSelected"
	ItemAreaCssClass="CommonContentSelectorItemArea"
	ErrorMessageTextId="ErrorMessage"
	SuccessMessageTextId="SuccessMessage"
	>
    <FormTemplate>
        <CSControl:WrappedLiteral Tag="Div" CssClass="CommonMessageSuccess" ID="SuccessMessage" runat="server" />
        <CSControl:WrappedLiteral Tag="Div" CssClass="CommonMessageError" ID="ErrorMessage" runat="server" />
    
		<div>
		    <table cellpadding="0" cellspacing="0" border="0" width="100%">
		        <tr valign="top"><td width="33%"><div class="CommonContentSelectorTreeArea">
			        <CA:TreeView id="Tree" ExpandNodeOnSelect="false"
			        
				        DragAndDropEnabled="false" 
				        NodeEditingEnabled="false"
				        KeyboardEnabled="true"
				        CssClass="CommonContentSelectorTree" 
				        NodeCssClass="CommonContentSelectorTreeNode" 
				        SelectedNodeCssClass="CommonContentSelectorTreeNodeSelected" 
				        HoverNodeCssClass="CommonContentSelectorTreeNodeHover"
				        LineImageWidth="19" 
				        LineImageHeight="20"
				        DefaultImageWidth="16" 
				        DefaultImageHeight="16"
				        ItemSpacing="0" 
				        ImagesBaseUrl="~/controlpanel/images/CATreeView/small_icons/"
				        NodeLabelPadding="3"
				        ParentNodeImageUrl="folder.gif" 
				        ExpandedParentNodeImageUrl="folder_open.gif" 
				        LeafNodeImageUrl="folder_open.gif" 
				        ShowLines="true" 
				        LineImagesFolderUrl="~/controlpanel/images/CATreeView/small_icons/lines/"
				        EnableViewState="true"
				        Width="100%"
				        Height="100%"
				        runat="server"  />
		        </div></td>
		        <td width="60%">
		            <div class="CommonContentSelectorItemsArea" id="itemsArea"><div runat="server" ID="ItemsArea"></div></div>
		            <div class="CommonContentSelectorOptionsArea" id="optionsArea"><div runat="server" id="OptionsArea"></div></div>
		        </td></tr></table>        
        </div>
        <div class="CommonContentSelectorButtonArea">
	        <CSControl:ResourceLinkButton ID="SelectButton" Runat="server" ResourceName="OK" CssClass="CommonTextButton" />
	        <CSControl:ResourceLinkButton Runat="server" ResourceName="Cancel" OnClientClick="window.parent.Telligent_Modal.Close(); return false;" CssClass="CommonTextButton" />
        </div>
    </FormTemplate>
</CSControl:SelectContentForm>
</div>
</div>

<script type="text/javascript">
// <![CDATA[

function resizeContent()
{
    try
    {
        var optionsArea = document.getElementById('optionsArea');
        var itemsArea = document.getElementById('itemsArea');

        if (optionsArea.childNodes[0].innerHTML == '')
        {
            optionsArea.style.display = 'none';
            itemsArea.style.height = '322px';
        }
        else
        {
            optionsArea.style.display = 'block';
            itemsArea.style.height = (316 - optionsArea.offsetHeight) + 'px';
        }
    }
    catch (e) {}
}

setInterval(resizeContent, 249);

// ]]>
</script>
</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="ContactInfo.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Settings.ContactInfo" %>
<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="Admin_SiteSettings_Contact"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<CP:statusmessage id="Status" runat="server"></CP:statusmessage>
		<DIV class="CommonDescription"></DIV>
		<DIV class="CommonFormArea">
		<div class="FixedWidthContainer">
			<TABLE cellSpacing="0" cellPadding="2" border="0">
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon10" runat="Server" resourcename="Admin_SiteSettings_Contact_AdminEmail_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Contact_AdminEmail" /></TD>
					<TD class="CommonFormField">
						<asp:textbox id="AdminEmailAddress" runat="server" maxlength="128"></asp:textbox>
						<asp:regularexpressionvalidator id="RegularExpressionValidator1" runat="server" validationexpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
							controltovalidate="AdminEmailAddress" cssclass="validationWarning">
							<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Contact_InvalidEmail" />
						</asp:regularexpressionvalidator></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon1" runat="Server" resourcename="Admin_SiteSettings_Contact_CompanyName_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Contact_CompanyName" /></TD>
					<TD class="CommonFormField" noWrap>
						<asp:textbox id="CompanyName" runat="server" maxlength="128"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon2" runat="Server" resourcename="Admin_SiteSettings_Contact_Company_Email_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Contact_Company_Email" /></TD>
					<TD class="CommonFormField">
						<asp:textbox id="CompanyEmailAddress" runat="server" maxlength="128"></asp:textbox>
						<asp:regularexpressionvalidator id="RegularExpressionValidator2" runat="server" validationexpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
							controltovalidate="CompanyEmailAddress" cssclass="validationWarning">
							<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Contact_InvalidEmail" />
						</asp:regularexpressionvalidator></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon3" runat="Server" resourcename="Admin_SiteSettings_Contact_Fax_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Contact_Fax" /></TD>
					<TD class="CommonFormField">
						<asp:textbox id="CompanyFaxNumber" runat="server" maxlength="128"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon4" runat="Server" resourcename="Admin_SiteSettings_Contact_Address_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Contact_Address" /></TD>
					<TD class="CommonFormField">
						<asp:textbox id="CompanyAddress" runat="server" maxlength="256"></asp:textbox></TD>
				</TR>
			</TABLE>
			</div>
			<p class="PanelSaveButton DetailsFixedWidth">
				<cp:resourcelinkbutton id="SaveButton" runat="server" resourcename="Save" cssclass="CommonTextButton"></cp:resourcelinkbutton></p>
		</DIV>
	</CP:Content>
</CP:Container>

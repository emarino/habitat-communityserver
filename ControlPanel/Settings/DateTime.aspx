<%@ Page language="c#" Codebehind="DateTime.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Settings.DateTime" %>
<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="Admin_SiteSettings_DateTime"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<CP:statusmessage id="Status" runat="server"></CP:statusmessage>
		<DIV class="CommonDescription"></DIV>
		<DIV class="CommonFormArea">
		<div class="FixedWidthContainer">
			<TABLE cellSpacing="0" cellPadding="2" border="0">
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon4" runat="Server" resourcename="Admin_SiteSettings_TimeZone_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_TimeZone" /></TD>
					<TD class="CommonFormField" noWrap>
						<cp:timezonedropdownlist id="Timezone" runat="server"></cp:timezonedropdownlist></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon1" runat="Server" resourcename="Admin_SiteSettings_DateTime_Date_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_DateTime_Date" /></TD>
					<TD class="CommonFormField" noWrap>
						<cp:dateformatdropdownlist id="DateFormat" runat="server"></cp:dateformatdropdownlist></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon2" runat="Server" resourcename="Admin_SiteSettings_DateTime_Time_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_DateTime_Time" /></TD>
					<TD class="CommonFormField">
						<cp:timeformatdropdownlist id="TimeFormat" runat="server"></cp:timeformatdropdownlist></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon3" runat="Server" resourcename="Admin_SiteSettings_DateTime_Filter_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_DateTime_Filter" /></TD>
					<TD class="CommonFormField">
						<cp:datefilter id="ThreadDateFilter" runat="server" AddText="false" /></TD>
				</TR>
			</TABLE>
			</div>
			<p class="PanelSaveButton DetailsFixedWidth">
				<cp:resourcelinkbutton id="SaveButton" runat="server" resourcename="Save" cssclass="CommonTextButton"></cp:resourcelinkbutton></p>
		</DIV>
	</CP:Content>
</CP:Container>

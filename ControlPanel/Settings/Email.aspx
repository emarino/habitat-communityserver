<%@ Register TagPrefix="CP" Namespace="CommunityServer.ControlPanel.Controls" Assembly="CommunityServer.Web" %>
<%@ Register TagPrefix="TWC" Namespace="Telligent.Glow" Assembly="Telligent.Glow" %>
<%@ Register TagPrefix="MG" TagName="Accounts" Src = "~/ControlPanel/Settings/MGAccounts.ascx" %>
<%@ Page language="c#" Codebehind="Email.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Settings.EmailSettings" %>
<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="CP_Settings_Email_Title"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<CP:statusmessage id="Status" runat="server"></CP:statusmessage>
		<DIV class="CommonDescription"></DIV>
		<div class="FixedWidthContainer">
            <TWC:TabbedPanes id="EmailTabs" runat="server"
	            PanesCssClass="CommonPane"
	            TabSetCssClass="CommonPaneTabSet"
	            TabCssClasses="CommonPaneTab,CommonPaneTab1,CommonPaneTab2"
	            TabSelectedCssClasses="CommonPaneTabSelected,CommonPaneTabSelected1,CommonPaneTabSelected2"
	            TabHoverCssClasses="CommonPaneTabHover,CommonPaneTabHover1,CommonPaneTabHover2"
	            >
	            <TWC:TabbedPane ID="TabbedPane1" runat="server">
		            <Tab>Email</Tab>
		            <Content>
		<DIV class="CommonGroupedContentArea">
			<H3 class="CommonSubTitle">
				<cp:resourcecontrol id="Resourcecontrol27" runat="server" resourcename="CP_Settings_Email_SubTitle_General"></cp:resourcecontrol></H3>
			<TABLE cellSpacing="0" cellPadding="2" border="0">
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon13" runat="Server" resourcename="CP_Settings_Email_Enable_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Settings_Email_Enable"></cp:resourcecontrol></TD>
					<TD class="CommonFormField" noWrap>
						<cp:yesnoradiobuttonlist id="EnableEmail" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput"></cp:yesnoradiobuttonlist></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon8" runat="Server" resourcename="CP_Settings_Email_Encoding_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Settings_Email_Encoding"></cp:resourcecontrol></TD>
					<TD class="CommonFormField">
						<asp:textbox id="EmailEncoding" runat="server" maxlength="64"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon runat="Server" resourcename="CP_Settings_Email_SubjectEncoding_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Settings_Email_SubjectEncoding"></cp:resourcecontrol></TD>
					<TD class="CommonFormField">
						<asp:textbox id="EmailSubjectEncoding" runat="server" maxlength="64"></asp:textbox></TD>
				</TR>
				<TR id="EmailThrottle_Row">
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon5" runat="Server" resourcename="CP_Settings_Email_EmailThrottle_Descr"></cp:helpicon>
						<cp:resourcecontrol id="lblEmailThrottle" runat="server" resourcename="CP_Settings_Email_EmailThrottle"></cp:resourcecontrol></TD>
					<TD class="CommonFormField">
						<asp:textbox id="EmailThrottle" runat="server" maxlength="5"></asp:textbox>
						<asp:requiredfieldvalidator id="EmailThrottleValidator" runat="server" controltovalidate="EmailThrottle" font-bold="True"
							errormessage="*"></asp:requiredfieldvalidator></TD>
				</TR>
				<TR id="SmtpServer_Row">
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_Settings_Email_Server_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Settings_Email_Server"></cp:resourcecontrol></TD>
					<TD class="CommonFormField">
						<asp:textbox id="SmtpServer" runat="server" maxlength="64"></asp:textbox></TD>
				</TR>
			</TABLE>
		</DIV>
		    <div id="SMTP_Login_Info_Area">
			<H3 class="CommonSubTitle">
				<cp:resourcecontrol id="Resourcecontrol1" runat="server" resourcename="CP_Settings_Email_SubTitle_SMTP"></cp:resourcecontrol></H3>
			<TABLE cellSpacing="0" cellPadding="2" border="0">
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon9" runat="Server" resourcename="CP_Settings_Email_UsingSsl_Descr"></cp:helpicon>
						<cp:resourcecontrol ID="Resourcecontrol2" runat="server" resourcename="CP_Settings_Email_UsingSsl"></cp:resourcecontrol></TD>
					<TD class="CommonFormField">
						<cp:yesnoradiobuttonlist id="SmtpServerUsingSsl" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput"></cp:yesnoradiobuttonlist></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon7" runat="Server" resourcename="CP_Settings_Email_UsingNtlm_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Settings_Email_UsingNtlm"></cp:resourcecontrol></TD>
					<TD class="CommonFormField">
						<cp:yesnoradiobuttonlist id="SmtpServerUsingNtlm" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput"></cp:yesnoradiobuttonlist></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon2" runat="Server" resourcename="CP_Settings_Email_NeedsLogin_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Settings_Email_NeedsLogin"></cp:resourcecontrol></TD>
					<TD class="CommonFormField">
						<cp:yesnoradiobuttonlist id="SmtpServerRequiredLogin" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput"></cp:yesnoradiobuttonlist></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon3" runat="Server" resourcename="CP_Settings_Email_UserName_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Settings_Email_UserName"></cp:resourcecontrol></TD>
					<TD class="CommonFormField">
						<asp:textbox id="SmtpServerUserName" runat="server" maxlength="64"></asp:textbox>
						<asp:requiredfieldvalidator id="SmtpServerUserNameValidator" runat="server" controltovalidate="SmtpServerUserName"
							font-bold="True" errormessage="*"></asp:requiredfieldvalidator></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon4" runat="Server" resourcename="CP_Settings_Email_Password_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Settings_Email_Password"></cp:resourcecontrol></TD>
					<TD class="CommonFormField">
						<asp:textbox id="SmtpServerPassword" runat="server" maxlength="64"></asp:textbox>
						<asp:requiredfieldvalidator id="SmtpServerPasswordValidator" runat="server" controltovalidate="SmtpServerPassword"
							font-bold="True" errormessage="*"></asp:requiredfieldvalidator></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon6" runat="Server" resourcename="CP_Settings_Email_PortNumber_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Settings_Email_PortNumber"></cp:resourcecontrol></TD>
					<TD class="CommonFormField">
						<asp:textbox id="SmtpPortNumber" runat="server" maxlength="10"></asp:textbox></TD>
				</TR>
			</TABLE>
			</div>
		            </Content>
	            </TWC:TabbedPane>
	            <TWC:TabbedPane ID="EnableMailGateway_Row" runat="server">
		            <Tab><CP:ResourceControl ID="ResourceControl7" resourcename="CP_MailGateway" ResourceFile="MGResources.xml" runat = "Server" /></Tab>
		            <Content>
    			            <DIV class="CommonGroupedContentArea">
			                    <H3 class="CommonSubTitle">
				                    <cp:resourcecontrol runat="server" resourcename="CP_Settings_Email_SubTitle_General"></cp:resourcecontrol></H3>
			                    <TABLE cellSpacing="0" cellPadding="2" border="0">
				                    <TR>
					                    <TD class="CommonFormFieldName">
						                    <cp:helpicon id="Helpicon14" runat="Server" resourcename="CP_Settings_Email_EnableMG_Descr" ResourceFile="MGResources.xml"></cp:helpicon>
						                    <cp:resourcecontrol runat="server" resourcename="CP_Settings_Email_EnableMG" ResourceFile="MGResources.xml"></cp:resourcecontrol></TD>
					                    <TD class="CommonFormField" noWrap>
						                    <cp:yesnoradiobuttonlist id="EnableMailGateway" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput" onclick="DisableFields();"></cp:yesnoradiobuttonlist></TD>
				                    </TR>
				                    <TR>
					                    <TD class="CommonFormFieldName">
						                    <cp:helpicon id="Helpicon16" runat="Server" resourcename="CP_Settings_Email_MG_RemoteAccessCode_Descr" ResourceFile="MGResources.xml" ></cp:helpicon>
						                    <cp:resourcecontrol runat="server" resourcename="CP_Settings_Email_MG_RemoteAccessCode" ResourceFile="MGResources.xml" ></cp:resourcecontrol></TD>
					                    <TD class="CommonFormField" noWrap>
						                    <asp:textbox id="MGSecurityCode" runat="server" maxlength="256"></asp:textbox></TD>
				                    </TR>
				                    <TR>
					                    <TD class="CommonFormFieldName">
						                    <cp:helpicon id="Helpicon17" runat="Server" resourcename="CP_Settings_Email_MG_EmailDomain_Descr" ResourceFile="MGResources.xml" ></cp:helpicon>
						                    <cp:resourcecontrol runat="server" resourcename="CP_Settings_Email_MG_EmailDomain" ResourceFile="MGResources.xml" ></cp:resourcecontrol></TD>
					                    <TD class="CommonFormField" noWrap>
						                    <asp:textbox id="MGEmailDomain" runat="server" maxlength="256"></asp:textbox></TD>
				                    </TR>
			                    </TABLE>
        		            </DIV>
			                    <H3 class="CommonSubTitle">
				                    <cp:resourcecontrol runat="server" resourcename="CP_Settings_Email_MG_Accounts" ResourceFile="MGResources.xml"></cp:resourcecontrol></H3>
		                            <MG:Accounts id="Accounts" runat="server"></MG:Accounts>
		            </Content>
	            </TWC:TabbedPane>
            </TWC:TabbedPanes>
		</div>
		<p class="PanelSaveButton DetailsFixedWidth">
			<cp:resourcelinkbutton id="SaveButton" runat="server" resourcename="Save" cssclass="CommonTextButton"></cp:resourcelinkbutton></p>

        <script type="text/javascript">
        // <![CDATA[
        function DisableFields()
        {
            var enableMailGateway = document.getElementsByName('<%=EnableMailGateway.ClientID.Replace("_", "$") %>');

            if(enableMailGateway.length > 0)
            {
                var yesButton = enableMailGateway[0];
                if(yesButton.checked)
                {
                    HideOrShowControl(document.getElementById('<%=MGSecurityCode.ClientID %>'), true);
                    HideOrShowControl(document.getElementById('<%=MGEmailDomain.ClientID %>'), true);
                }
                else
                {
                    HideOrShowControl(document.getElementById('<%=MGSecurityCode.ClientID %>'), false);
                    HideOrShowControl(document.getElementById('<%=MGEmailDomain.ClientID %>'), false);
                }
            }
        }
        
        function HideOrShowControl(control, visible)
        {
            if(visible)
                control.disabled = false;
            else
                control.disabled = true;
        }
        
        function EnableForSubmit()
        {
            HideOrShowControl(document.getElementById('<%=MGSecurityCode.ClientID %>'), true);
            HideOrShowControl(document.getElementById('<%=MGEmailDomain.ClientID %>'), true);
        }

        DisableFields();
        // ]]> 
        </script>

	</CP:Content>
</CP:Container>
<%@ Page language="c#" Codebehind="ThemeOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Settings.ThemeOptions" %>
<CP:ControlPanelSelectedNavigation runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol runat="server" resourcename="CP_ThemeOptions_Title"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
			<CP:statusmessage id="formStatus" runat="server"></CP:statusmessage>
		<div class="FixedWidthContainer">
			<table cellSpacing="0" cellPadding="3" border="0">
				<tr>
					<td class="CommonFormFieldName">
						<cp:helpicon runat="Server" resourcename="CP_Membership_Settings_EnableUserTheme_Descr"></cp:helpicon>
						<cp:ResourceLabel runat="server" ResourceName="CP_Membership_Settings_EnableUserTheme"></cp:ResourceLabel>
					</td>
					<td class="CommonFormField">
						<cp:YesNoRadioButtonList id="optAllowUserToSelectTheme" runat="server" RepeatColumns="2" CssClass="shorttxt"></cp:YesNoRadioButtonList>
					</td>
				</tr>
				<tr>
					<td class="CommonFormFieldName">
						<cp:helpicon runat="Server" resourcename="CP_ThemeOptions_DefaultTheme_Descr"></cp:helpicon>
						<cp:ResourceLabel runat="server" ResourceName="CP_ThemeOptions_DefaultTheme"></cp:ResourceLabel>
					</td>
					<td class="CommonFormField">
						<asp:DropDownList runat="server" id="defaultTheme" runat="server" />
					</td>
				</tr>
				<tr>
					<td class="CommonFormFieldName">
						<cp:helpicon runat="Server" resourcename="CP_ThemeOptions_DisabledThemes_Descr"></cp:helpicon>
						<cp:ResourceLabel runat="server" ResourceName="CP_ThemeOptions_DisabledThemes"></cp:ResourceLabel>
					</td>
					<td class="CommonFormField">
						<asp:CheckBoxList runat="server" id="disabledThemes" runat="server" RepeatColumns="3" />
					</td>
				</tr>
			</table>
		</div>
		<p class="PanelSaveButton DetailsFixedWidth">
			<cp:ResourceLinkButton id="btnSave" runat="server" resourcename="Save" cssclass="CommonTextButton"></cp:ResourceLinkButton>
		</p>
	</CP:Content>
</CP:Container>

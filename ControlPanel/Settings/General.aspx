<%@ Page language="c#" Codebehind="General.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Settings.GeneralSettings" %>
<cp:controlpanelselectednavigation runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="CP_Settings_General_Title"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<cp:statusmessage id="Status" runat="server"></cp:statusmessage>
		<DIV class="CommonDescription"></DIV>
		<DIV class="CommonFormArea">
		<div class="FixedWidthContainer">
			<TABLE cellSpacing="0" cellPadding="2" border="0">
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon12" runat="Server" resourcename="CP_Settings_General_EnableInk_Descr"></cp:helpicon>
						<cp:resourcecontrol id="lblEnableCollapsingPanels" runat="server" resourcename="CP_Settings_General_EnableInk"></cp:resourcecontrol></TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<cp:yesnoradiobuttonlist id="EnableInk" runat="server" cssclass="ControlPanelTextInput" repeatcolumns="2"></cp:yesnoradiobuttonlist></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon10" runat="Server" resourcename="CP_Settings_General_DisplayDescription_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Settings_General_DisplayDescription" /></TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<cp:yesnoradiobuttonlist id="DisplaySiteDescription" runat="server" cssclass="ControlPanelTextInput" repeatcolumns="2"></cp:yesnoradiobuttonlist></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon4" runat="Server" resourcename="CP_Settings_General_Menu_CurrentTime_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Settings_General_Menu_CurrentTime" /></TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<cp:yesnoradiobuttonlist id="DisplayCurrentTime" runat="server" cssclass="ControlPanelTextInput" repeatcolumns="2"></cp:yesnoradiobuttonlist></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon16" runat="Server" resourcename="CP_Settings_General_Menu_WhosOnline_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Settings_General_Menu_WhosOnline" /></TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<cp:yesnoradiobuttonlist id="DisplayWhoIsOnline" runat="server" cssclass="ControlPanelTextInput" repeatcolumns="2"></cp:yesnoradiobuttonlist></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon17" runat="Server" resourcename="CP_Settings_General_Menu_Statistics_Descr"></cp:helpicon>
						<cp:resourcecontrol  runat="server" resourcename="CP_Settings_General_Menu_Statistics" /></TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<cp:yesnoradiobuttonlist id="DisplayStatistics" runat="server" cssclass="ControlPanelTextInput" repeatcolumns="2"></cp:yesnoradiobuttonlist></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_Settings_General_Menu_DisplayNames_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Settings_General_Menu_DisplayNames" /></TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<cp:yesnoradiobuttonlist id="DisplayNames" runat="server" cssclass="ControlPanelTextInput" repeatcolumns="2"></cp:yesnoradiobuttonlist></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon2" runat="Server" resourcename="CP_Settings_General_Menu_SiteUrl_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Settings_General_Menu_SiteUrl" /></TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<asp:TextBox id="SiteUrl" runat="server" />
						<asp:RequiredFieldValidator runat="server" ErrorMessage="*" Font-Bold="True" ControlToValidate="SiteUrl" />
					</TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon5" runat="Server" resourcename="CP_Settings_General_Menu_TOS_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Settings_General_Menu_TOS" /></TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<asp:TextBox id="TermsOfServiceUrl" runat="server" />
					</TD>
				</TR>	
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon6" runat="Server" resourcename="CP_Settings_General_Menu_EnableSectionLocalization_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="CP_Settings_General_Menu_EnableSectionLocalization" /></TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<cp:yesnoradiobuttonlist id="EnableSectionLocalization" runat="server" cssclass="ControlPanelTextInput" repeatcolumns="2"></cp:yesnoradiobuttonlist></TD>
				</TR>			
				<TR runat="server" id="DisplayEulaArea">
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon3" runat="Server" resourcename="CP_Settings_General_Menu_Eula_Descr"></cp:helpicon><cp:resourcecontrol runat="server" resourcename="CP_Settings_General_Menu_Eula" /></TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<cp:yesnoradiobuttonlist id="DisplayEula" runat="server" cssclass="ControlPanelTextInput" repeatcolumns="2"></cp:yesnoradiobuttonlist></TD>
				</TR>
			</TABLE>
			</div>
			<p class="PanelSaveButton DetailsFixedWidth">
				<cp:resourcelinkbutton id="SaveButton" runat="server" resourcename="Save" cssclass="CommonTextButton"></cp:resourcelinkbutton></p>
		</DIV>
	</CP:Content>
</CP:Container>

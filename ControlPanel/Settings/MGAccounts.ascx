<%@ Register TagPrefix="CP" Namespace="CommunityServer.ControlPanel.Controls" Assembly="CommunityServer.Web" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MGAccounts.ascx.cs" Inherits="CommunityServerWeb.ControlPanel.Settings.MGAccounts" %>
			<CSControl:ModalLink CssClass="CommonTextButton" runat = "Server" Height="300" Width="400" callback="refreshCallback" resourcename="CP_Settings_Email_MG_Accounts_CreateNew" Url = 'MGAccountsForm.aspx' ResourceFile="MGResources.xml" />
				<asp:Repeater Runat="server" id="AccountsList">
					<HeaderTemplate>
						<div class="CommonListArea">
							<table id="Listing" cellSpacing="0" cellPadding="0" border="0" width="100%">
								<thead>
									<tr>
										<th class="CommonListHeaderLeftMost">
											<cp:resourcecontrol runat="server" resourcename="CP_Settings_Email_MG_Accounts_Account_HostName" ResourceFile="MGResources.xml" /></th>
										<th class="CommonListHeader">
											<cp:resourcecontrol runat="server" resourcename="CP_Settings_Email_MG_Accounts_Account_UserName" ResourceFile="MGResources.xml" /></th>
										<th class="CommonListHeader">
											<cp:resourcecontrol runat="server" resourcename="CP_Settings_Email_MG_Accounts_Account_Enabled" ResourceFile="MGResources.xml" /></th>
										<th class="CommonListHeader">
											<cp:resourcecontrol runat="server" resourcename="CP_Settings_Email_MG_Accounts_Account_Actions" ResourceFile="MGResources.xml" /></th>
									</tr>
								</thead>
					</HeaderTemplate>
					<ItemTemplate>
						<tr>
							<td class="CommonListCellLeftMost">
								<%# DataBinder.Eval(Container.DataItem, "Hostname")%>
							</td>
							<td class="CommonListCell">
								<%# DataBinder.Eval(Container.DataItem, "Username")%>
								&nbsp;
							</td>
							<td class="CommonListCell">
								<center>
									<%# String.Format(@"<img src=""{0}images/{1}.gif"" width=""16"" height=""15"" />", CommunityServer.Components.SiteUrls.Instance().Locations["ControlPanel"], DataBinder.Eval(Container.DataItem,"IsEnabled")) %>
								</center>
							</td>
							<td class="CommonListCell">
								<center>
								<CSControl:ModalLink CssClass="CommonTextButton" runat = "Server" Height="300" Width="400" callback="refreshCallback" resourcename="Edit" Url = '<%# "MGAccountsForm.aspx?AccountID=" + Container.ItemIndex %>' />
								<CSControl:ModalLink CssClass="CommonTextButton" runat = "Server" Height="150" Width="400" callback="refreshCallback" resourcename="Delete" Url = '<%# "SettingsCommand.aspx?Command=CommunityServer.ControlPanel.Settings.DeleteMailAccountCommand,CommunityServer.Web&AccountID=" + Container.ItemIndex %>' />
								</center>
							</td>
						</tr>
					</ItemTemplate>

					<FooterTemplate>
						</table>
						</div>
					</FooterTemplate>
				</asp:Repeater>

<%@ Page language="c#" Codebehind="Files.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Settings.FilesPage" %>
<%@ Import Namespace = "CommunityServer.Components" %>
<%@ Import Namespace = "CommunityServer.ControlPanel" %>
<cp:controlpanelselectednavigation runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
	<cp:resourcelabel id="title" runat="server" resourcename="CP_Settings_FileStorage_Title" />
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">
<script>
function refreshPage(res)
{
    refresh();
}
</script>

<div class="CommonDescription">
	<CP:ResourceControl runat="server" ResourceName="CP_Settings_FileStorage_SubTitle" />
<div class="CommonTextBig"><asp:literal id="StatusMessage" runat="server" /></div>
</div>

<asp:placeholder id="ManageFilesPlaceholder" runat="server">
<div class="PanelSaveButton">
	<CP:Modallink id="NewFolder" runat="Server" resourcename="CP_Settings_FileStorage_NewFolder" height="150"
		width="300" cssclass="CommonTextButton" CallBack="refreshCallback" />
</div>
<div class="CommonListArea">
<asp:literal id="FolderLinks" runat="server" visible="False" /><br />
<table id="CommentListing" cellSpacing="0" cellPadding="0" border="0" width="100%">
<thead>
    <tr>
    <th class="CommonListHeaderLeftMost"><cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Name" /></th>
    <th class="CommonListHeader"><cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Size" /></th>
    <th class="CommonListHeader"><cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Actions" /></th>
    </tr>
</thead>
<tr>
	<td class="CommonListCellLeftMost" colspan="3"><cp:href id="NavigateUpFolder" runat="server" text=".." visible="False" /></td>
</tr>
<asp:Repeater runat="Server" id="FileGrid">
<ItemTemplate>
	<tr>
	<td class="CommonListCellLeftMost">
		<asp:literal id="Icon" Runat="server" />
		<cp:href id="Name" Runat="server" />
	</td>
	<td class="CommonListCell"><asp:literal id="Size" Runat="server" /></td>
	<td  class="CommonListCell" nowrap="nowrap"><CP:ResourceLinkButton CommandName="Delete" Runat="server" ID = "DeleteButton" ResourceName="CP_Blog_GridCol_Delete" CssClass="CommonTextButton" /></td>
	</tr>
</ItemTemplate>
<AlternatingItemTemplate>
	<tr class="AltListRow">
	<td class="CommonListCellLeftMost">
		<asp:literal id="Icon" Runat="server" />
		<cp:href id="Name" Runat="server" />
	</td>
	<td class="CommonListCell"><asp:literal id="Size" Runat="server" /></td>
	<td  class="CommonListCell" nowrap="nowrap"><CP:ResourceLinkButton CommandName="Delete" Runat="server" ID = "DeleteButton" ResourceName="CP_Blog_GridCol_Delete" CssClass="CommonTextButton" /></td>
	</tr>
</AlternatingItemTemplate>
</asp:Repeater>
</table>
</div>

<div class"CommonSearchArea">
<table>
<tr>
    <td class="CommonFormFieldName" colspan="2">
        <CP:FormLabel id="tt" runat="Server" ResourceName="CP_Settings_FileStorage_UploadFile" ControlToLabel="SiteFileUpload" />
    </td>
</tr>
<tr>
    <td class="CommonFormFieldName"><asp:FileUpload runat="server" id="SiteFileUpload" /></td>
    <td class="CommonFormFieldName"><CP:ResourceLinkButton id="btnUpload" ResourceName="CP_Settings_FileStorage_UploadButton" Runat="server"  CssClass="CommonTextButton" /></td>
</tr>
</table>
</div>
</asp:placeholder>

</CP:Content>
</CP:Container>

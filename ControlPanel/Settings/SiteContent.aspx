<%@ Page language="c#" Codebehind="SiteContent.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Settings.SiteContent" %>
<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
	<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="CP_Settings_SiteContent_Title" />
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">
	<cp:statusmessage id="Status" runat="server"></cp:statusmessage>
	<DIV class="CommonDescription"></DIV>
	<DIV class="CommonFormArea">
	<div class="FixedWidthContainer">
		<TABLE cellSpacing="0" cellPadding="2" border="0">
		<TR>
			<TD class="CommonFormFieldName">
				<cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_Settings_SiteContent_Name_Descr" />
				<cp:resourcecontrol id="ResourceControl10" runat="server" resourcename="CP_Settings_SiteContent_Name" />
			</TD>
			<TD class="CommonFormField" noWrap>
				<asp:textbox id="SiteName" runat="server" maxlength="512" columns="55" />
			</TD>
		</TR>
		<TR>
			<TD class="CommonFormFieldName" vAlign="top">
				<cp:helpicon id="Helpicon2" runat="Server" resourcename="CP_Settings_SiteContent_Description_Descr" />
				<cp:resourcecontrol id="ResourceControl12" runat="server" resourcename="CP_Settings_SiteContent_Description" />
			</TD>
			<TD class="CommonFormField">
				<asp:textbox id="SiteDescription" runat="server" columns="55" textmode="Multiline" rows="6" />
			</TD>
		</TR>
		<TR>
			<TD class="CommonFormFieldName">
				<cp:helpicon id="Helpicon13" runat="Server" resourcename="CP_Settings_SiteContent_SearchMetaDescription_Descr" />
				<cp:resourcecontrol id="lblSearchMetaDescription" runat="server" resourcename="CP_Settings_SiteContent_SearchMetaDescription" />
			</TD>
			<TD class="CommonFormField">
				<asp:textbox id="SearchMetaDescription" runat="Server" maxlength="512" columns="55" />
			</TD>
		</TR>
		<TR>
			<TD class="CommonFormFieldName">
				<cp:helpicon id="Helpicon14" runat="Server" resourcename="CP_Settings_SiteContent_SearchMetaKeyword_Descr" />
				<cp:resourcecontrol id="lblSearchMetaKeyword" runat="server" resourcename="CP_Settings_SiteContent_SearchMetaKeyword" />
			</TD>
			<TD class="CommonFormField">
				<asp:textbox id="SearchMetaKeywords" runat="Server" maxlength="512" columns="55" />
			</TD>
		</TR>
		<TR>
			<TD class="CommonFormFieldName" vAlign="top">
				<cp:helpicon id="Helpicon3" runat="Server" resourcename="CP_Settings_SiteContent_AdditionHeader_Descr" />
				<cp:resourcecontrol id="Resourcecontrol1" runat="server" resourcename="CP_Settings_SiteContent_AdditionHeader"/>
			</TD>
			<TD class="CommonFormField">
				<asp:textbox id="GenericHeader" runat="server" columns="55" textmode="Multiline" rows="6" />
			</TD>
		</TR>
		<TR>
			<TD class="CommonFormFieldName">
				<cp:helpicon id="Helpicon15" runat="Server" resourcename="CP_Settings_SiteContent_Copyright_Descr" />
				<cp:resourcecontrol id="lblCopyright" runat="server" resourcename="CP_Settings_SiteContent_Copyright" />
			</TD>
			<TD class="CommonFormField">
				<asp:textbox id="Copyright" runat="Server" columns="55" />
			</TD>
		</TR>
		</TABLE>
		</div>
		<p class="PanelSaveButton DetailsFixedWidth">
			<cp:resourcelinkbutton id="SaveButton" runat="server" resourcename="Save" cssclass="CommonTextButton"></cp:resourcelinkbutton></p>
	</DIV>
</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="IPAddressTracking.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Settings.IPAddressTracking" %>
<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="Admin_SiteSettings_IP"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<cp:statusmessage id="Status" runat="server"></cp:statusmessage>
		<DIV class="CommonDescription"></DIV>
		<DIV class="CommonFormArea">
		<div class="FixedWidthContainer">
			<TABLE cellSpacing="0" cellPadding="2" border="0">
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon4" runat="Server" resourcename="Admin_SiteSettings_IP_Enable_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_IP_Enable" /></TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<cp:yesnoradiobuttonlist id="EnableTrackPostsByIP" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput"></cp:yesnoradiobuttonlist></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon1" runat="Server" resourcename="Admin_SiteSettings_IP_Display_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_IP_Display" /></TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<cp:yesnoradiobuttonlist id="DisplayPostIP" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput"></cp:yesnoradiobuttonlist></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon2" runat="Server" resourcename="Admin_SiteSettings_IP_AdminOnly_Descr"></cp:helpicon>
						<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_IP_AdminOnly" /></TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<cp:yesnoradiobuttonlist id="DisplayPostIPAdminsModeratorsOnly" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput"></cp:yesnoradiobuttonlist></TD>
				</TR>
			</TABLE>
		</div>
		<p class="PanelSaveButton DetailsFixedWidth">
				<cp:resourcelinkbutton id="SaveButton" runat="server" resourcename="Save" cssclass="CommonTextButton"></cp:resourcelinkbutton></p>
		</DIV>
	</CP:Content>
</CP:Container>

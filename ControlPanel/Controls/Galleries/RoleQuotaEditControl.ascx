<%@ Import Namespace="CommunityServer.Components" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="RoleQuotaEditControl.ascx.cs" Inherits="CommunityServer.ControlPanel.Controls.RoleQuotaEditControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<DIV class="CommonContent">
		<DIV class="CommonFormFieldName">
			<CP:FormLabel runat="server" ResourceName="CP_PhotosAdmin_Options_GeneralSettings_Role" />
		</DIV>
		<DIV class="CommonFormField">
			<asp:label id="RoleName" runat="server"></asp:label>
		</DIV>
		<DIV class="CommonFormFieldName">
			<CP:FormLabel id="Formlabel1" runat="server" ResourceName="CP_PhotosAdmin_Options_GeneralSettings_ImageQuota"
				ControlToLabel="ImageQuota"></CP:FormLabel>
		</DIV>
		<DIV class="CommonFormField">
			<asp:textbox id="ImageQuota" runat="server" MaxLength="6" Size="6"></asp:textbox>
			<asp:regularexpressionvalidator id="ImageQuotaValidator" runat="server" ErrorMessage="*" Cssclass="validationWarning"
				ValidationExpression="^[-]*[0-9]*" ControlToValidate="ImageQuota"></asp:regularexpressionvalidator>
				<asp:label id="MaxImageQuotaLabel" Runat="server"></asp:label></DIV>

		<DIV class="CommonFormFieldName">
			<CP:FormLabel id="Formlabel2" runat="server" ResourceName="CP_PhotosAdmin_Options_GeneralSettings_DiskQuota"
				ControlToLabel="DiskQuota"></CP:FormLabel>
		</DIV>
		<DIV class="CommonFormField">
			<asp:textbox id="DiskQuota" runat="server" MaxLength="12" Size="6"></asp:textbox>
			<asp:regularexpressionvalidator id="DiskQuotaValidator" runat="server" ErrorMessage="*" Cssclass="validationWarning"
				ValidationExpression="^[-]*[0-9]*" ControlToValidate="DiskQuota"></asp:regularexpressionvalidator>
				<asp:label id="MaxDiskQuotaLabel" Runat="server"></asp:label>
		</DIV>
</DIV>

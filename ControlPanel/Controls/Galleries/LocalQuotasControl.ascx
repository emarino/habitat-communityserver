<%@ Control language="c#" Codebehind="LocalQuotasControl.ascx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Controls.LocalQuotasControl" %>
<CP:FileOnlyStatusMessage id="FOStatus" runat="server" visible="false"></CP:FileOnlyStatusMessage>
<asp:PlaceHolder id="OptionHolder" runat="Server">
	<CP:ConfigOKStatusMessage id="Status" visible="false" runat="server"></CP:ConfigOKStatusMessage>
	<table cellSpacing="1" cellPadding="4" border="0">
		<tr>
			<td class="CommonFormFieldName" colSpan="2">
				<CP:helpicon id="Helpicon7" runat="Server" resourcename="CP_PhotosAdmin_Options_GeneralSettings_Quotas_Help"></CP:helpicon>
				<CP:FORMLABEL id="Formlabel6" runat="server" ResourceName="CP_PhotosAdmin_Options_GeneralSettings_Quotas" ControlToLabel="ImageQuota"></CP:FORMLABEL></td>
		</tr>
		<tr>
			<td>&nbsp;&nbsp;</td>
			<td class="CommonFormFieldName">
				<CP:helpicon id="Helpicon8" runat="Server" resourcename="CP_PhotosAdmin_Options_GeneralSettings_ImageQuota_Help"></CP:helpicon>
				<CP:FORMLABEL id="Formlabel8" runat="server" ResourceName="CP_PhotosAdmin_Options_GeneralSettings_ImageQuota"
					ControlToLabel="ImageQuota"></CP:FORMLABEL></td>
			<td class="CommonFormField">
				<asp:textbox id="ImageQuota" runat="server" MaxLength="6" ></asp:textbox>
				<asp:regularexpressionvalidator id="Regularexpressionvalidator4" runat="server"
					ErrorMessage="*" Cssclass="validationWarning" ValidationExpression="^[-]*[0-9]*" ControlToValidate="ImageQuota"></asp:regularexpressionvalidator>
				<asp:label id="MaxImageQuotaLabel" Runat="server"></asp:label></td>
		</tr>
		<tr>
			<td>&nbsp;&nbsp;</td>
			<td class="CommonFormFieldName">
				<CP:helpicon id="Helpicon9" runat="Server" resourcename="CP_PhotosAdmin_Options_GeneralSettings_DiskQuota_Help"></CP:helpicon>
				<CP:FORMLABEL id="Formlabel9" runat="server" ResourceName="CP_PhotosAdmin_Options_GeneralSettings_DiskQuota"
					ControlToLabel="DiskQuota"></CP:FORMLABEL></td>
			<td class="CommonFormField">
				<asp:textbox id="DiskQuota" runat="server" MaxLength="12" ></asp:textbox>
				<asp:regularexpressionvalidator id="Regularexpressionvalidator5" runat="server"
					ErrorMessage="*" Cssclass="validationWarning" ValidationExpression="^[-]*[0-9]*" ControlToValidate="DiskQuota"></asp:regularexpressionvalidator>
				<asp:label id="MaxDiskQuotaLabel" Runat="server"></asp:label></td>
		</tr>
	</table>
</asp:PlaceHolder>

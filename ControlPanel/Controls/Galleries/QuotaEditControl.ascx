<%@ Control Language="c#" AutoEventWireup="false" Codebehind="QuotaEditControl.ascx.cs" Inherits="CommunityServer.ControlPanel.Controls.QuotaEditControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="CP" TagName = "RoleQuotas" Src = "~/ControlPanel/Controls/Galleries/RoleQuotasControl.ascx" %>
<%@ Register TagPrefix="CP" TagName = "GlobalQuotas" Src = "~/ControlPanel/Controls/Galleries/GlobalQuotasControl.ascx" %>

<DIV class="CommonContent">
	<TWC:TabbedPanes id="EditorTabs" runat="server"
	PanesCssClass="CommonPane"
	TabSetCssClass="CommonPaneTabSet"
	TabCssClasses="CommonPaneTab,CommonPaneTab1,CommonPaneTab2"
	TabSelectedCssClasses="CommonPaneTabSelected,CommonPaneTabSelected1,CommonPaneTabSelected2"
	TabHoverCssClasses="CommonPaneTabHover,CommonPaneTabHover1,CommonPaneTabHover2"
	>
	<TWC:TabbedPane runat="server">
		<Tab><CP:ResourceControl runat="server" ResourceName="CP_PhotosAdmin_Quotas_Tab_OwnerQuotas" /></Tab>
		<Content>
			<div class="CommonDescription"><br />
				<CP:ResourceControl runat="Server" resourcename="CP_PhotosAdmin_Quotas_RoleQuotas_Description"></CP:ResourceControl><br>
			<em>&nbsp;&nbsp;&nbsp;
				<CP:ResourceControl runat="server" ResourceName="CP_PhotosAdmin_Quotas_RoleQuotas_Tip"></CP:ResourceControl></em></div>
				<CP:RoleQuotas id="RoleQuotasControl1" runat="Server"></CP:RoleQuotas>
		</Content>
	</TWC:TabbedPane>
	<TWC:TabbedPane runat="server">
		<Tab><CP:ResourceControl runat="server" ResourceName="CP_PhotosAdmin_Quotas_Tab_GlobalQuotas" /></Tab>
		<Content>
			<div class="CommonDescription"><br />
				<CP:ResourceControl runat="Server" resourcename="CP_PhotosAdmin_Quotas_GlobalQuotas_Description"></CP:ResourceControl></div>
			<CP:GlobalQuotas id="GlobalQuotasControl1" runat="Server"></CP:GlobalQuotas>
		</Content>
	</TWC:TabbedPane>
	</TWC:TabbedPanes>
</DIV>

<%@ Control Language="c#" AutoEventWireup="false" Codebehind="RoleQuotasControl.ascx.cs" Inherits="CommunityServer.ControlPanel.Controls.RoleQuotasControl"  %>
<script type="text/javascript">
  
  function reloadQuotas()
  {
	window.location = window.location;
  }
  var editName = '<cp:resourcecontrol runat="server" resourcename="Edit" />';
</script>
<div id="GrayGrid">
	<CA:Grid id="Grid1" runat="server">
		<Levels>
			<CA:GridLevel DataKeyField="RoleID">
				<Columns>
					<CA:GridColumn DataField="RoleID" Visible="False" />
					<CA:GridColumn AllowEditing="false" DataField="Name" HeadingText="ResourceManager.CP_Photos_GridCol_Name" Width="200" />
					<CA:GridColumn AllowEditing="false" DataCellServerTemplateId="imageQuota" columntype="Default" DataField="ImageQuota" HeadingText="ResourceManager.CP_Photos_GridCol_ImageQuota"
						width="150" Align="Center" />
					<CA:GridColumn AllowEditing="false" DataCellServerTemplateId="diskQuota" columntype="Default" DataField="DiskQuota" HeadingText="ResourceManager.CP_Photos_GridCol_DiskQuota"
						width="150" Align="Center" />
					<CA:GridColumn HeadingText="ResourceManager.CP_Photos_GridCol_Actions" DataCellClientTemplateId="EditTemplate"
						EditControlType="EditCommand" Width="80" Align="Center" DataCellCssClass="LastDataCell" />
				</Columns>
			</CA:GridLevel>
		</Levels>
		<ClientTemplates>
			<CA:ClientTemplate Id="EditTemplate">
				<a href="javascript:Telligent_Modal.Open('RoleQuotaForm.aspx?RoleID=##  DataItem.GetMember("RoleID").Text ##', 350, 250, reloadQuotas);" class="CommonTextButton">##editName##</a> 
			</CA:ClientTemplate>
		</ClientTemplates>
		<ServerTemplates>
			<CA:GridServerTemplate ID="diskQuota">
				<Template><asp:Literal ID="diskText" runat="server" /></Template>
			</CA:GridServerTemplate>
			<CA:GridServerTemplate ID="imageQuota">
				<Template><asp:Literal ID="imageText" runat="server" /></Template>
			</CA:GridServerTemplate>
		</ServerTemplates>
	</CA:Grid>
</div>

<%@ Control Language="c#" AutoEventWireup="false" Codebehind="SkinOptionsLayoutControl.ascx.cs" Inherits="CommunityServer.ControlPanel.Controls.SkinOptionsLayoutControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<CP:StatusMessage id="Status" runat="server"></CP:StatusMessage>
<table cellspacing="1" cellpadding="2" width="500" border="0" id="SkinOptions" runat="server">
	<tr runat="server" id="AlbumListingArrangement">
		<td valign="top" class="CommonFormFieldName">
			<cp:helpicon id="Helpicon2" runat="Server" resourcename="CP_PhotosShared_LayoutOptions_CategoryListingArrangement_Detail" />
			<CP:FormLabel runat="server" ResourceName="CP_PhotosShared_LayoutOptions_CategoryListingArrangement"
				id="Formlabel2" />
		</td>
		<td class="CommonFormFieldName">
			<CP:FormLabel runat="server" ResourceName="CP_PhotosShared_LayoutOptions_Columns" ControlToLabel="CategoryListingColumns"
				id="Formlabel3" />
			<asp:TextBox id="CategoryListingColumns" Size="2" MaxLength="2" runat="server" />
			<asp:RegularExpressionValidator runat="server" ControlToValidate="CategoryListingColumns" ValidationExpression="[0-9]*"
				Cssclass="validationWarning" ErrorMessage="*" id="Regularexpressionvalidator1" NAME="Regularexpressionvalidator1" />
			<asp:RequiredFieldValidator runat="server" ErrorMessage="*" Font-Bold="True" ControlToValidate="CategoryListingColumns"
				ID="Requiredfieldvalidator1" NAME="Requiredfieldvalidator1" />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<CP:FormLabel runat="server" ResourceName="CP_PhotosShared_LayoutOptions_Rows" ControlToLabel="CategoryListingRows"
				id="Formlabel4" />
			<asp:TextBox id="CategoryListingRows" Size="2" MaxLength="2" runat="server" />
			<asp:RegularExpressionValidator runat="server" ControlToValidate="CategoryListingRows" ValidationExpression="[0-9]*"
				Cssclass="validationWarning" ErrorMessage="*" id="Regularexpressionvalidator2" NAME="Regularexpressionvalidator2" />
			<asp:RequiredFieldValidator runat="server" ErrorMessage="*" Font-Bold="True" ControlToValidate="CategoryListingRows"
				ID="Requiredfieldvalidator2" NAME="Requiredfieldvalidator2" />
		</td>
	</tr>
	<tr>
		<td valign="top" class="CommonFormFieldName">
			<cp:helpicon id="Helpicon3" runat="Server" resourcename="CP_PhotosShared_LayoutOptions_PictureListingArrangement_Detail" />
			<CP:FormLabel runat="server" ResourceName="CP_PhotosShared_LayoutOptions_PictureListingArrangement"
				id="Formlabel8" />
		</td>
		<td class="CommonFormFieldName">
			<CP:FormLabel runat="server" ResourceName="CP_PhotosShared_LayoutOptions_Columns" ControlToLabel="PictureListingColumns"
				id="Formlabel5" />
			<asp:TextBox id="PictureListingColumns" Size="2" MaxLength="2" runat="server" />
			<asp:RegularExpressionValidator runat="server" ControlToValidate="PictureListingColumns" ValidationExpression="[0-9]*"
				Cssclass="validationWarning" ErrorMessage="*" id="Regularexpressionvalidator3" NAME="Regularexpressionvalidator3" />
			<asp:RequiredFieldValidator runat="server" ErrorMessage="*" Font-Bold="True" ControlToValidate="PictureListingColumns"
				ID="Requiredfieldvalidator3" NAME="Requiredfieldvalidator3" />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<CP:FormLabel runat="server" ResourceName="CP_PhotosShared_LayoutOptions_Rows" ControlToLabel="PictureListingRows"
				id="Formlabel6" />
			<asp:TextBox id="PictureListingRows" Size="2" MaxLength="2" runat="server" />
			<asp:RegularExpressionValidator runat="server" ControlToValidate="PictureListingRows" ValidationExpression="[0-9]*"
				Cssclass="validationWarning" ErrorMessage="*" id="Regularexpressionvalidator4" NAME="Regularexpressionvalidator4" />
			<asp:RequiredFieldValidator runat="server" ErrorMessage="*" Font-Bold="True" ControlToValidate="PictureListingRows"
				ID="Requiredfieldvalidator4" NAME="Requiredfieldvalidator4" />
		</td>
	</tr>
	<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon id="Helpicon4" runat="Server" resourcename="CP_PhotosShared_LayoutOptions_PicturesSortBy_Detail" />
			<CP:FormLabel runat="server" ResourceName="CP_PhotosShared_LayoutOptions_PicturesSortBy" ControlToLabel="PicturesSortBy"
				id="Formlabel7" />
		</td>
		<td class="CommonFormFieldName">
			<cp:GalleryThreadSortDropDownList id="PicturesSortBy" runat="server" />
		</td>
	</tr>
	<tr runat="server" id="AlbumSortOption">
		<td class="CommonFormFieldName">
			<cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_PhotosShared_LayoutOptions_AlbumsSortBy_Detail" />
			<CP:FormLabel runat="server" ResourceName="CP_PhotosShared_LayoutOptions_AlbumsSortBy" ControlToLabel="AlbumsSortBy"
				id="Formlabel1" />
		</td>
		<td class="CommonFormFieldName">
			<cp:PostCategoriesSortDropDownList id="AlbumsSortBy" runat="server" />
		</td>
	</tr>
</table>

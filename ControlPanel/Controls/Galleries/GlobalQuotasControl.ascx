<%@ Control language="c#" Codebehind="GlobalQuotasControl.ascx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Controls.GlobalQuotasControl" %>
		<CP:FileOnlyStatusMessage id="FOStatus" runat="server" visible="false"></CP:FileOnlyStatusMessage>
		<asp:PlaceHolder id="OptionHolder" runat="Server">
<TABLE cellSpacing="1" cellPadding="4" border="0">
	<TR>
		<TD class="CommonFormFieldName" colSpan="2">
			<cp:helpicon id="Helpicon7" runat="Server" resourcename="CP_PhotosAdmin_Options_GeneralSettings_Quotas_Help"></cp:helpicon>
			<CP:FORMLABEL id="Formlabel6" runat="server" ResourceName="CP_PhotosAdmin_Options_GeneralSettings_Quotas"
				ControlToLabel="ImageQuota"></CP:FORMLABEL></TD>
	</TR>
	<TR>
		<TD>&nbsp;&nbsp;</TD>
		<TD class="CommonFormFieldName">
			<cp:helpicon id="Helpicon8" runat="Server" resourcename="CP_PhotosAdmin_Options_GeneralSettings_ImageQuota_Help"></cp:helpicon>
			<CP:FORMLABEL id="Formlabel8" runat="server" ResourceName="CP_PhotosAdmin_Options_GeneralSettings_ImageQuota"
				ControlToLabel="ImageQuota"></CP:FORMLABEL></TD>
		<TD class="CommonFormField">
			<asp:textbox id="ImageQuota" runat="server" MaxLength="8" Size="6"></asp:textbox>
			<asp:regularexpressionvalidator id="Regularexpressionvalidator4" runat="server" NAME="Regularexpressionvalidator1"
				ErrorMessage="*" Cssclass="validationWarning" ValidationExpression="^[-]*[0-9]*" ControlToValidate="ImageQuota"></asp:regularexpressionvalidator></TD>
	</TR>
	<TR>
		<TD>&nbsp;&nbsp;</TD>
		<TD class="CommonFormFieldName">
			<cp:helpicon id="Helpicon9" runat="Server" resourcename="CP_PhotosAdmin_Options_GeneralSettings_DiskQuota_Help"></cp:helpicon>
			<CP:FORMLABEL id="Formlabel9" runat="server" ResourceName="CP_PhotosAdmin_Options_GeneralSettings_DiskQuota"
				ControlToLabel="DiskQuota"></CP:FORMLABEL></TD>
		<TD class="CommonFormField">
			<asp:textbox id="DiskQuota" runat="server" MaxLength="8" Size="6"></asp:textbox>
			<asp:regularexpressionvalidator id="Regularexpressionvalidator5" runat="server" NAME="Regularexpressionvalidator2"
				ErrorMessage="*" Cssclass="validationWarning" ValidationExpression="^[-]*[0-9]*" ControlToValidate="DiskQuota"></asp:regularexpressionvalidator></TD>
	</TR>
</TABLE>
		</asp:PlaceHolder>

<%@ Import Namespace="CommunityServer.Components" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="GroupEditControl.ascx.cs" Inherits="CommunityServer.ControlPanel.Controls.GroupEditControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
	<DIV class="CommonContent">
			<DIV class="CommonFormFieldName">
				<CP:FormLabel id="tt" runat="Server" resourcename="ForumGroupAdmin_ForumGroupName" ControlToLabel="GroupName"></CP:FormLabel>
				<asp:requiredfieldvalidator id="ForumGroupNameValidator" runat="server" controltovalidate="GroupName" errormessage="*"></asp:requiredfieldvalidator></DIV>
			<DIV class="CommonFormField">
				<asp:TextBox id="GroupName" Runat="server" CssClass="ControlPanelTextInput" MaxLength="256"></asp:TextBox></DIV>
		<P />
			<DIV class="CommonFormFieldName">
				<CP:FormLabel id="Formlabel1" runat="Server" ControlToLabel="GroupDesc" ResourceName="CP_Description"></CP:FormLabel></DIV>
			<DIV class="CommonFormField">
				<asp:TextBox id="GroupDesc" Runat="server" CssClass="ControlPanelTextInput" MaxLength="256"  Rows="8" TextMode="MultiLine"></asp:TextBox></DIV>
	</DIV>

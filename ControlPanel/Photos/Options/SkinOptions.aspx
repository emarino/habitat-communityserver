<%@ Register TagPrefix="CP" TagName = "Layout" Src = "~/ControlPanel/Controls/Galleries/SkinOptionsLayoutControl.ascx" %>
<%@ Page language="c#" Codebehind="SkinOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Photos.Options.SkinsOptionsPage" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="GalleryControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_Photos_SkinOptions_Title"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<CP:StatusMessage id="Status" runat="server"></CP:StatusMessage>
		<DIV class="CommonDescription">
			<CP:ResourceControl id="Resourcecontrol2" runat="server" ResourceName="CP_Photos_SkinOptions_SubTitle"></CP:ResourceControl></DIV>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Galleries" />
		<TWC:TabbedPanes id="EditorTabs" runat="server"
		PanesCssClass="CommonPane"
		TabSetCssClass="CommonPaneTabSet"
		TabCssClasses="CommonPaneTab,CommonPaneTab1,CommonPaneTab2"
		TabSelectedCssClasses="CommonPaneTabSelected,CommonPaneTabSelected1,CommonPaneTabSelected2"
		TabHoverCssClasses="CommonPaneTabHover,CommonPaneTabHover1,CommonPaneTabHover2"
		>
		<TWC:TabbedPane runat="server">
			<Tab><CP:ResourceControl runat="server" ResourceName="CP_Photos_SkinOptions_Tab_LayoutOptions" /></Tab>
			<Content>
				<CP:Layout id="Layout1" runat="Server"></CP:Layout>
			</Content>
		</TWC:TabbedPane>
		</TWC:TabbedPanes>
		<p class="PanelSaveButton DetailsFixedWidth">
			<cp:ResourceLinkButton id="SaveButton" runat="Server" CssClass="CommonTextButton" ResourceName="Save"></cp:ResourceLinkButton></p>
	</CP:Content>
</CP:Container>

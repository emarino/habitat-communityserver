<%@ Page language="c#" Codebehind="PostOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Galleries.Options.PostOptionsPage" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="GalleryControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
	<CP:ResourceControl runat="server" ResourceName="CP_Photos_PostOptions_Title" />
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">
	<CP:StatusMessage id="Status" runat="server" ResourceName="CP_Settings_SiteContent_SettingsSavedSuccessfully" />
	<DIV class="CommonDescription">
		<CP:ResourceControl runat="server" ResourceName="CP_Photos_PostOptions_SubTitle" />
	</DIV>
	<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Galleries" />

    <TABLE cellSpacing="1" cellPadding="4" border="0">
	<TR>
		<TD class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_Photos_PostOptions_Override_AllowReplies_Help" />
			<CP:FormLabel runat="Server" ResourceName="CP_Photos_PostOptions_Override_AllowReplies" ControlToLabel="ynEnableRepliesOverride" />
		</TD>
		<TD class="CommonFormField">
			<cp:YesNoRadioButtonList id="ynEnableRepliesOverride" runat="server" CssClass="txt1" RepeatColumns="2" />
		</TD>
	</TR>
	<TR>
		<TD class="CommonFormFieldName">
			<cp:helpicon  runat="Server" resourcename="CP_Photos_PostOptions_Override_ModerateComments_Help" />
			<CP:FormLabel runat="Server" ResourceName="CP_Photos_PostOptions_Override_ModerateComments"
				ControlToLabel="ModerationDDLOverride" /></TD>
		<TD class="CommonFormField">
			<cp:CommentModerationDropDownList id="ModerationDDLOverride" runat="Server" AllowIgnore="true" />
		</TD>
	</TR>
	<TR>
		<TD class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_Photos_PostOptions_Override_EnableRatings_Help" />
			<CP:FormLabel runat="Server" ResourceName="CP_Photos_PostOptions_Override_EnableRatings" ControlToLabel="ynEnableRatingsOverride" />
		</TD>
		<TD class="CommonFormField">
			<cp:YesNoRadioButtonList id="ynEnableRatingsOverride" runat="server" CssClass="txt1" RepeatColumns="2" />
		</TD>
	</TR>
	<TR>
		<TD class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_Photos_PostOptions_Override_EnableTrackBacks_Help" />
			<CP:FormLabel runat="Server" ResourceName="CP_Photos_PostOptions_Override_EnableTrackBacks" ControlToLabel="ynEnableTrackbacksOverride" />
		</TD>
		<TD class="CommonFormField">
			<cp:YesNoRadioButtonList id="ynEnableTrackbacksOverride" runat="server" CssClass="txt1" RepeatColumns="2" />
		</TD>
	</TR>
	<TR>
		<TD class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_Photos_PostOptions_Override_EnableOrderPrints_Help" />
			<CP:FormLabel runat="Server" ResourceName="CP_Photos_PostOptions_Override_EnableOrderPrints" ControlToLabel="ynEnableOrderPrintsOverride" />
		</TD>
		<TD class="CommonFormField">
			<cp:YesNoRadioButtonList id="ynEnableOrderPrintsOverride" runat="server" CssClass="txt1" RepeatColumns="2" />
		</TD>
	</TR>
	<TR>
		<TD class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_Photos_PostOptions_Override_ExpiredDays_Help" />
			<CP:FormLabel runat="server" ResourceName="CP_Photos_PostOptions_Override_ExpiredDays" ControlToLabel="ExpiredDays" />
		</TD>
		<TD class="CommonFormField">
			<asp:DropDownList id="ExpiredDays" Runat="server" />
		</TD>
	</TR>
	</TABLE>

	<P class="PanelSaveButton DetailsFixedWidth">
		<cp:ResourceLinkButton id="SaveButton" runat="Server" CssClass="CommonTextButton" ResourceName="Save" />
	</p>
</CP:Content>
</CP:Container>

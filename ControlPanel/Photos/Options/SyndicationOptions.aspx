<%@ Page language="c#" Codebehind="SyndicationOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Photos.Options.SyndicationOptionsPage" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="GalleryControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_Photos_SyndicationOptions_Title"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
	<CP:StatusMessage id="Status" runat="server" ResourceName="CP_Settings_SiteContent_SettingsSavedSuccessfully"/>
		<DIV class="CommonDescription">
			<CP:ResourceControl id="Resourcecontrol2" runat="server" ResourceName="CP_Photos_SyndicationOptions_SubTitle"></CP:ResourceControl></DIV>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Galleries" />
		<div class="FixedWidthContainer">
			<DIV class="CommonFormField">
				<CP:FormLabel id="FormLabel1" runat="server" ResourceName="CP_Photos_SyndicationOptions_EnableRSS"
					ControlToLabel="EnableRss"></CP:FormLabel>
				<asp:CheckBox id="EnableRss" Runat="Server" CssClass="ControlPanelTextInput"></asp:CheckBox>
				</DIV>
			<DIV class="CommonFormField">
				<CP:FormLabel id="FormLabel2" runat="Server" ResourceName="CP_Photos_SyndicationOptions_EnableRssComments" 
					ControlToLabel="EnableRssComments" />
				<asp:CheckBox id="EnableRssComments" runat="Server" CssClass="ControlPanelTextInput" />
				</DIV>
		</div>
		<p class="PanelSaveButton DetailsFixedWidth">
			<cp:ResourceLinkButton id="SaveButton" runat="Server" CssClass="CommonTextButton" ResourceName="Save"></cp:ResourceLinkButton></p>
	</CP:Content>
</CP:Container>

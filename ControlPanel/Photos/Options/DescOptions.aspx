<%@ Page language="c#" Codebehind="DescOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Controls.DescriptiveOptionsPage" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="GalleryControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
	<CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_Photos_DescOptions_Title" />
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">
	<DIV class="CommonDescription">
		<CP:ResourceControl id="Resourcecontrol2" runat="server" ResourceName="CP_Photos_DescOptions_SubTitle" />
	</DIV>
	<CP:StatusMessage id="Status" runat="server" />
	<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Galleries" />
	<DIV class="CommonFormArea">
		<div class="FixedWidthContainer">
		<DIV class="CommonFormFieldName">
			<cp:helpicon id="HelpIcon1" runat="Server" resourcename="CP_Photos_DescOptions_Name_Help" />
			<CP:FormLabel id="FormLabel1" runat="server" ResourceName="CP_Photos_DescOptions_Name" ControlToLabel="Name" />
			<asp:RequiredFieldValidator id="RequiredFieldValidator1" ControlToValidate="Name" Display="Dynamic" Runat="server" Text="Please enter a title" />
		</DIV>
		<DIV class="CommonFormField">
			<asp:TextBox id="Name" Runat="Server" CssClass="ControlPanelTextInput" />
		</DIV>
		<DIV class="CommonFormFieldName">
			<cp:helpicon id="Helpicon2" runat="Server" resourcename="CP_Photos_DescOptions_Desc_Help" />
			<CP:FormLabel id="FormLabel2" runat="server" ResourceName="CP_Photos_DescOptions_Desc" ControlToLabel="Description" />
		</DIV>
		<DIV class="CommonFormField">
			<asp:TextBox id="Description" Runat="Server" CssClass="ControlPanelTextInput" rows="3" TextMode="MultiLine" />
		</DIV>
		
		<div class="CommonFormFieldName">
			<cp:helpicon runat="server" resourcename="CP_Photos_DescOptions_MetaTagDescription_Help" id="Helpicon4"/>
			<cp:formlabel runat="server" resourcename="CP_Photos_DescOptions_MetaTagDescription" controltolabel="MetaTagDescription" id="Formlabel4"/>
		</div>
		<div class="CommonFormField">
			<asp:textbox id="MetaTagDescription" runat="Server" maxlength="512" cssclass="ControlPanelTextInput" />
		</div>
		<div class="CommonFormFieldName">
			<cp:helpicon runat="server" resourcename="CP_Photos_DescOptions_MetaTagKeyword_Help" id="Helpicon3"/>
			<cp:formlabel runat="server" resourcename="CP_Photos_DescOptions_MetaTagKeyword" controltolabel="MetaTagKeywords" id="Formlabel3"/>
		</div>
		<div class="CommonFormField">
			<asp:textbox id="MetaTagKeywords" runat="Server" maxlength="512" cssclass="ControlPanelTextInput" />
		</div>
		</DIV>

		<p class="PanelSaveButton DetailsFixedWidth">
			<cp:ResourceLinkButton id="SaveButton" runat="Server" CssClass="CommonTextButton" ResourceName="Save" />
		</p>
</CP:Content>
</CP:Container>

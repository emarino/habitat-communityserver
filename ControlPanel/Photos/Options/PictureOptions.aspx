<%@ Page language="c#" Codebehind="PictureOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Galleries.Options.PictureOptionsPage" %>
<%@ Register TagPrefix="CP" TagName = "ImageSettings" Src = "~/ControlPanel/Photos/Options/ImageSettings.ascx" %>
<CP:Container id="MPContainer" runat="server" ThemeMasterFile="GalleryControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_Photos_PictureOptions_Title"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<CSControl:Script id="GalleryAdminScript" runat="server" src="~/utility/GalleryAdmin.js"></CSControl:Script>
		<DIV class="CommonDescription">
			<CP:ResourceControl id="Resourcecontrol2" runat="server" ResourceName="CP_Photos_PictureOptions_SubTitle"></CP:ResourceControl></DIV>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Galleries" />
		<TWC:TabbedPanes id="EditorTabs" runat="server"
		PanesCssClass="CommonPane"
		TabSetCssClass="CommonPaneTabSet"
		TabCssClasses="CommonPaneTab,CommonPaneTab1,CommonPaneTab2"
		TabSelectedCssClasses="CommonPaneTabSelected,CommonPaneTabSelected1,CommonPaneTabSelected2"
		TabHoverCssClasses="CommonPaneTabHover,CommonPaneTabHover1,CommonPaneTabHover2"
		>
		<TWC:TabbedPane runat="server">
			<Tab><CP:ResourceControl runat="server" ResourceName="CP_Photos_PictureOptions_Tab_Size" /></Tab>
			<Content>
				<TABLE cellSpacing="1" cellPadding="4" border="0">
					<TR>
						<TD nowrap="nowrap" class="CommonFormFieldName">
							<CP:FormLabel id="Formlabel1" runat="server" ResourceName="CP_Photos_PictureOptions_Size" ControlToLabel="ThumbnailSettings"></CP:FormLabel></TD>
						<td nowrap="nowrap" class="CommonFormFieldName">
							<cp:helpicon id="Helpicon15" runat="Server" resourcename="CP_Photos_PictureOptions_Width_Help"></cp:helpicon>
							<CP:FORMLABEL id="Formlabel21" runat="server" ControlToLabel="ImageX" ResourceName="CP_Photos_PictureOptions_Width"></CP:FORMLABEL>
						</td>
						<td nowrap="nowrap" class="CommonFormFieldName">
							<cp:helpicon id="Helpicon16" runat="Server" resourcename="CP_Photos_PictureOptions_Height_Help"></cp:helpicon>
							<CP:FORMLABEL id="Formlabel22" runat="server" ControlToLabel="ImageY" ResourceName="CP_Photos_PictureOptions_Height"></CP:FORMLABEL>
						</td>
						<td nowrap="nowrap" class="CommonFormFieldName">
							<cp:helpicon id="Helpicon17" runat="Server" resourcename="CP_Photos_PictureOptions_Quality_Help"></cp:helpicon>
							<CP:FORMLABEL id="Formlabel23" runat="server" ControlToLabel="ThumbnailQuality" ResourceName="CP_Photos_PictureOptions_Quality"></CP:FORMLABEL>
						</td>
						<td nowrap="nowrap" class="CommonFormFieldName" nowrap="nowrap">
							<cp:helpicon id="HelpiconBrightness" runat="Server" resourcename="CP_Photos_PictureOptions_Brightness_Help"></cp:helpicon>
							<CP:FORMLABEL id="Formlabel10" runat="server" ControlToLabel="Brightness" ResourceName="CP_Photos_PictureOptions_Brightness"></CP:FORMLABEL>
						</td>
						<TD nowrap="nowrap" class="CommonFormFieldName">
							&nbsp;
						</TD>
					</TR>
					<!--Thumbnail-->
					<TR>
						<TD valign="top" nowrap="nowrap" class="CommonFormFieldName">
							<cp:helpicon id="HelpIcon1" runat="Server" ResourceName="CP_Photos_PictureOptions_Thumbnail_Help"></cp:helpicon>
							<CP:FormLabel id="Formlabel5" runat="server" ResourceName="CP_Photos_PictureOptions_Thumbnail"></CP:FormLabel>
						</TD>
						<CP:ImageSettings runat="server" id="ThumbnailSettings" />
						<TD class="CommonFormFieldName">
							<A id="ThumbnailPreview" href="javascript:void(0)" runat="server" Class="CommonTextButton">
								<CP:ResourceLabel id="Resourcelabel6" runat="server" ResourceName="CP_Photos_PictureOptions_Preview"
									NAME="Resourcelabel6"></CP:ResourceLabel></A>
							<asp:LinkButton id="ThumbnailRebuild" runat="server" CssClass="CommonTextButton"></asp:LinkButton>
						</TD>
					</TR>
					<!--Secondary Thumbnail-->
					<TR>
						<TD nowrap="nowrap" class="CommonFormFieldName">
							<cp:helpicon id="Helpicon3" runat="Server" resourcename="CP_Photos_PictureOptions_SecondaryThumbnail_Help"></cp:helpicon>
							<CP:FormLabel id="Formlabel9" runat="server" ResourceName="CP_Photos_PictureOptions_SecondaryThumbnail"></CP:FormLabel>
						</TD>
						<CP:ImageSettings runat="server" id="SecondaryThumbnailSettings" />
						<TD class="CommonFormFieldName">
							<A id="SecondaryThumbnailPreview" href="javascript:void(0)" runat="server" Class="CommonTextButton">
								<CP:ResourceLabel id="Resourcelabel15" runat="server" ResourceName="CP_Photos_PictureOptions_Preview"></CP:ResourceLabel></A>
							<asp:LinkButton id="SecondaryThumbnailRebuild" runat="server" CssClass="CommonTextButton"></asp:LinkButton>
						</TD>
					</TR>
					<!--Picture Details-->
					<TR>
						<TD nowrap="nowrap" class="CommonFormFieldName">
							<cp:helpicon id="Helpicon4" runat="Server" resourcename="CP_Photos_PictureOptions_PictureDetails_Help"></cp:helpicon>
							<CP:FormLabel id="Formlabel12" runat="server" ResourceName="CP_Photos_PictureOptions_PictureDetails"></CP:FormLabel>
						</TD>
						<CP:ImageSettings runat="server" id="PictureDetailsSettings" />
						<TD class="CommonFormFieldName">
							<A id="PictureDetailsPreview" href="javascript:void(0)" runat="server" Class="CommonTextButton">
								<CP:ResourceLabel id="Resourcelabel20" runat="server" ResourceName="CP_Photos_PictureOptions_Preview"
									NAME="Resourcelabel20"></CP:ResourceLabel></A>
						</TD>
					</TR>
					<!--Slideshow-->
					<TR>
						<TD nowrap="nowrap" class="CommonFormFieldName">
							<cp:helpicon id="Helpicon5" runat="Server" resourcename="CP_Photos_PictureOptions_Slideshow_Help"></cp:helpicon>
							<CP:FormLabel id="Formlabel15" runat="server" ResourceName="CP_Photos_PictureOptions_Slideshow"></CP:FormLabel>
						</TD>
						<CP:ImageSettings runat="server" id="SlideshowSettings" />
						<TD class="CommonFormFieldName">
							<A id="SlideshowPreview" href="javascript:void(0)" runat="server" Class="CommonTextButton">
								<CP:ResourceLabel id="Resourcelabel43" runat="server" ResourceName="CP_Photos_PictureOptions_Preview" NAME="Resourcelabel36"></CP:ResourceLabel></A>
						</TD>
					</TR>
					<!--Max Size-->
					<TR>
						<TD nowrap="nowrap" class="CommonFormFieldName">
							<cp:helpicon id="Helpicon2" runat="Server" resourcename="CP_Photos_PictureOptions_Max_Help"></cp:helpicon>
							<CP:FormLabel id="Formlabel14" runat="server" ResourceName="CP_Photos_PictureOptions_Max"></CP:FormLabel>
						</TD>
						<CP:ImageSettings runat="server" id="MaxSettings" />
						<TD class="CommonFormFieldName">
							&nbsp;
						</TD>
					</TR>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<!--CACHE-->
					<TR>
						<TD vAlign="top" nowrap="nowrap" class="CommonFormFieldName">
							<cp:helpicon id="Helpicon7" runat="Server" resourcename="CP_Photos_PictureOptions_CacheSize_Help"></cp:helpicon>
							<CP:FormLabel id="Formlabel20" runat="server" ResourceName="CP_Photos_PictureOptions_CacheSize"></CP:FormLabel></TD>
						<TD class="CommonFormFieldName" colspan="4">
							<asp:Literal id="CacheSize" runat="server"></asp:Literal>&nbsp;&nbsp;
						</TD>
						<TD class="CommonFormFieldName">
							<asp:LinkButton id="CleanCache" runat="server" CssClass="CommonTextButton"></asp:LinkButton>
							<asp:LinkButton id="ClearCache" runat="server" CssClass="CommonTextButton"></asp:LinkButton>
						</TD>
					</TR>
				</TABLE>
			</Content>
		</TWC:TabbedPane>
		<TWC:TabbedPane runat="server">
			<Tab><CP:ResourceControl runat="server" ResourceName="CP_Photos_PictureOptions_Tab_Watermark" /></Tab>
			<Content>
				<TABLE cellSpacing="0" cellPadding="0" border="0">
					<tr>
						<TD class="CommonFormFieldName">
						</TD>
						<TD class="CommonFormFieldName">
							<TABLE cellSpacing="1" cellPadding="4" border="0">
								<TR>
									<td nowrap="nowrap" class="CommonFormFieldName">
										<cp:helpicon id="Helpicon9" runat="Server" resourcename="CP_Photos_PictureOptions_WatermarkType_Help"></cp:helpicon>
										<CP:FormLabel id="Formlabel3" runat="server" ResourceName="CP_Photos_PictureOptions_WatermarkType"
											ControlToLabel="ImageWatermarkType"></CP:FormLabel>
									</td>
									<td class="CommonFormField">
										<asp:dropdownlist id="ImageWatermarkType" runat="server"></asp:dropdownlist>
									</td>
								</TR>
								<TR>
									<td nowrap="nowrap" class="CommonFormFieldName">
										<cp:helpicon id="Helpicon10" runat="Server" resourcename="CP_Photos_PictureOptions_WatermarkPosition_Help"></cp:helpicon>
										<CP:FormLabel id="Formlabel4" runat="server" ResourceName="CP_Photos_PictureOptions_WatermarkPosition"
											ControlToLabel="ImageWatermarkPosition"></CP:FormLabel>
									</td>
									<td class="CommonFormField">
										<asp:dropdownlist id="ImageWatermarkPosition" runat="server"></asp:dropdownlist>
									</td>
								</TR>
								<tr>
									<td class="CommonFormFieldName">
										<cp:helpicon id="Helpicon13" runat="Server" resourcename="CP_Photos_PictureOptions_WatermarkMinWidth_Help"></cp:helpicon>
										<CP:FORMLABEL id="Formlabel8" runat="server" ControlToLabel="WatermarkMinImageX" ResourceName="CP_Photos_PictureOptions_WatermarkMinWidth"></CP:FORMLABEL></td>
									<td class="CommonFormField">
										<asp:textbox id="WatermarkMinImageX" Size="4" MaxLength="4" runat="server"></asp:textbox>
										<asp:regularexpressionvalidator id="Regularexpressionvalidator1" runat="server" ControlToValidate="WatermarkMinImageX"
											ValidationExpression="[0-9]*" Cssclass="validationWarning" ErrorMessage="*" NAME="Regularexpressionvalidator1"></asp:regularexpressionvalidator>
										<asp:requiredfieldvalidator id="ImageXValidator" runat="server" ControlToValidate="WatermarkMinImageX" ErrorMessage="*"
											Font-Bold="True"></asp:requiredfieldvalidator></td>
								</tr>
								<tr>
									<td class="CommonFormFieldName">
										<cp:helpicon id="Helpicon14" runat="Server" resourcename="CP_Photos_PictureOptions_WatermarkMinHeight_Help"></cp:helpicon>
										<CP:FORMLABEL id="Formlabel11" runat="server" ControlToLabel="WatermarkMinImageY" ResourceName="CP_Photos_PictureOptions_WatermarkMinHeight"></CP:FORMLABEL></td>
									<td class="CommonFormField">
										<asp:textbox id="WatermarkMinImageY" Size="4" MaxLength="4" runat="server"></asp:textbox>
										<asp:regularexpressionvalidator id="Regularexpressionvalidator2" runat="server" ControlToValidate="WatermarkMinImageY"
											ValidationExpression="[0-9]*" Cssclass="validationWarning" ErrorMessage="*" NAME="Regularexpressionvalidator2"></asp:regularexpressionvalidator>
										<asp:requiredfieldvalidator id="ImageYValidator" runat="server" ControlToValidate="WatermarkMinImageY" ErrorMessage="*"
											Font-Bold="True"></asp:requiredfieldvalidator></td>
								</tr>
								<TR>
									<td nowrap="nowrap" class="CommonFormFieldName">
										<cp:helpicon id="Helpicon11" runat="Server" resourcename="CP_Photos_PictureOptions_WatermarkText_Help"></cp:helpicon>
										<CP:FormLabel id="Formlabel6" runat="server" ResourceName="CP_Photos_PictureOptions_WatermarkText"
											ControlToLabel="WatermarkText"></CP:FormLabel>
									</td>
									<td class="CommonFormField">
										<asp:TextBox id="WatermarkText" runat="server" MaxLength="50" Size="10"></asp:TextBox>
									</td>
								</TR>
							</TABLE>
						</TD>
						<TD vAlign="top" nowrap="nowrap" class="CommonFormFieldName">
							<TABLE cellSpacing="1" cellPadding="4" border="0">
								<TR>
									<td nowrap="nowrap" class="CommonFormFieldName">
										<cp:helpicon id="Helpicon12" runat="Server" resourcename="CP_Photos_PictureOptions_WatermarkImage_Help"></cp:helpicon>
										<CP:FormLabel id="Formlabel7" runat="server" ResourceName="CP_Photos_PictureOptions_WatermarkImage"
											ControlToLabel="WatermarkImageThumb"></CP:FormLabel>
									</td>
								</TR>
								<tr>
									<td colspan="2" align="center" class="CommonFormField">
										<asp:Image id="imgWatermark" runat="server" />
									</td>
								</tr>
					</tr>
					<td class="CommonFormField">
						<input id="WatermarkData" type="file" name="WatermarkData" runat="server">
					</td>
					</TR>
				</TABLE>
						</TD>
					</TR>
				</TABLE>
			</Content>
		</TWC:TabbedPane>
		<TWC:TabbedPane runat="server">
			<Tab><CP:ResourceControl runat="server" ResourceName="CP_Photos_PictureOptions_Tab_EXIF" /></Tab>
			<Content>
				<TABLE cellSpacing="1" cellPadding="4" border="0">
					<TR>
						<TD nowrap="nowrap" class="CommonFormFieldName">
							<cp:helpicon id="Helpicon6" runat="Server" resourcename="CP_Photos_PictureOptions_EnableEXIF_Help"></cp:helpicon>
							<CP:FormLabel id="Formlabel18" runat="server" ResourceName="CP_Photos_PictureOptions_EnableEXIF"
								ControlToLabel="ynEnableExif"></CP:FormLabel></TD>
						<TD class="CommonFormFieldName">
							<cp:YesNoRadioButtonList id="ynEnableExif" runat="server" CssClass="txt1" RepeatColumns="2"></cp:YesNoRadioButtonList></TD>
					</TR>
					<TR>
						<TD vAlign="top" class="CommonFormFieldName">
							<cp:helpicon id="Helpicon8" runat="Server" resourcename="CP_Photos_PictureOptions_EXIF_Help"></cp:helpicon>
							<CP:FormLabel id="Formlabel19" runat="server" ResourceName="CP_Photos_PictureOptions_EXIF" ControlToLabel="ExifTypes"></CP:FormLabel></TD>
						<TD>
							<asp:ListBox id="ExifTypes" runat="server" Size="70" SelectionMode="Multiple" Rows="10"></asp:ListBox></TD>
					</TR>
				</TABLE>
			</Content>
		</TWC:TabbedPane>
		<TWC:TabbedPane runat="server">
			<Tab><CP:ResourceControl runat="server" ResourceName="CP_Photos_PictureOptions_Tab_Import" /></Tab>
			<Content>
				<cp:DirectoryWriter id="Directorywriter1" runat="server" ForeColor="Red" SecurityResourceName="ManageGalleries_SecurityException"
					UnAuthorizedResourceName="ManageGalleries_UnAuthorizedException" Location="galleries"></cp:DirectoryWriter>
			    <CP:STATUSMESSAGE id="ImportStatus" runat="server"></CP:STATUSMESSAGE>
				<TABLE cellSpacing="1" cellPadding="4" border="0">
					<TR>
						<TD vAlign="top" nowrap="nowrap" class="CommonFormFieldName">
							<cp:helpicon id="Helpicon18" runat="Server" resourcename="CP_Photos_PictureOptions_ImportCount_Help"></cp:helpicon>
							<CP:FormLabel id="Formlabel2" runat="server" ResourceName="CP_Photos_PictureOptions_ImportCount"></CP:FormLabel>:</TD>
						<TD class="CommonFormFieldName">
							<asp:Literal id="ImportCount" runat="server"></asp:Literal>&nbsp;&nbsp;
						</TD>
					</TR>
					<TR>
						<TD vAlign="top" nowrap="nowrap" class="CommonFormFieldName">
							<cp:helpicon id="Helpicon19" runat="Server" resourcename="CP_Photos_PictureOptions_ImportInto_Help"></cp:helpicon>
							<CP:FormLabel id="Formlabel13" runat="server" ResourceName="CP_Photos_PictureOptions_ImportInto"></CP:FormLabel>:</TD>
						</td>
						<TD class="CommonFormFieldName">
							<cp:ParentCategoryDropDown id="ImportCategoryID" runat="server" />
						</TD>
					</TR>
					<TR>
						<TD class="CommonFormFieldName" colspan="2" align="right">
							<cp:ResourceLinkButton id="ImportButton" runat="server" CssClass="CommonTextButton" ResourceName="CP_Photos_PictureOptions_Import"></cp:ResourceLinkButton>
						</TD>
					</TR>
				</TABLE>
			</Content>
		</TWC:TabbedPane>
		</TWC:TabbedPanes>
		<TABLE class="tableBorder" id="PreviewWindow" style="DISPLAY: none; Z-INDEX: 100; BACKGROUND: #dddddd; VISIBILITY: hidden; POSITION: absolute"
			cellSpacing="0" cellPadding="5">
			<TR>
				<TD><A onclick="TogglePreview();" href="javascript:void(0)">close</A><BR>
					This is the maximum<BR>
					size the image will be.<BR>
					<IMG id="PreviewImage" src="../../../utility/images/1x1.gif" border="1">
				</TD>
			</TR>
		</TABLE>
		<asp:Literal id="CacheMessage" Runat="server" Visible="false"></asp:Literal>
		<P class="PanelSaveButton DetailsFixedWidth">
			<CP:ResourceLinkButton id="SaveButton" runat="Server" CssClass="CommonTextButton" ResourceName="Save"></CP:ResourceLinkButton></P>
	</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="CategorizationOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServerWeb.ControlPanel.Photos.Options.CategorizationOptions" %>

<CP:Container runat="server" id="MPContainer" ThemeMasterFile="GalleryControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_Photos_CategorizationOptions_Title"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<CP:ResourceControl id="Resourcecontrol2" runat="server" ResourceName="CP_Photos_CategorizationOptions_SubTitle"></CP:ResourceControl></DIV>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Galleries" />
		<DIV class="CommonFormArea">
			<div class="FixedWidthContainer">
			<table cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td nowrap="nowrap" class="CommonFormFieldName">
						<cp:helpicon id="Helpicon3" runat="Server" resourcename="CP_Photos_CategorizationOptions_CategorizeBy_Help"></cp:helpicon>
						<CP:FormLabel id="Formlabel7" runat="Server" ControlToLabel="CategorizeBy" ResourceName="CP_Photos_CategorizationOptions_CategorizeBy"></CP:FormLabel>&nbsp;
					</td>
					<td nowrap="nowrap" class="CommonFormFieldName">
						<asp:DropDownList runat="server" id="CategorizeBy" />
					</td>
				</tr>
			</table>
			<p class="PanelSaveButton DetailsFixedWidth">
				<cp:ResourceLinkButton id="SaveButton" runat="Server" CssClass="CommonTextButton" ResourceName="Save"></cp:ResourceLinkButton></asp:LinkButton></p>
		</DIV>
	</CP:Content>
</CP:Container>

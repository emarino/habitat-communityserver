<%@ Page language="c#" Codebehind="AboutOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Photos.Options.AboutOptionsPage" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="GalleryControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_Photos_AboutOptions_Title"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<CP:ResourceControl id="Resourcecontrol2" runat="server" ResourceName="CP_Photos_AboutOptions_SubTitle"></CP:ResourceControl></DIV>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Galleries" />
		<DIV class="CommonFormArea">
			<div class="FixedWidthContainer">
			<table cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td nowrap="nowrap">
						<div  class="CommonFormFieldName">
							<cp:helpicon id="Helpicon3" runat="Server" resourcename="CP_Photos_AboutOptions_EnableAbout_Help"></cp:helpicon>
							<CP:FormLabel id="Formlabel7" runat="Server" ControlToLabel="ynEnableAbout" ResourceName="CP_Photos_AboutOptions_EnableAbout"></CP:FormLabel>&nbsp;
						</div>
					</td>
					<td nowrap="nowrap">
						<div  class="CommonFormFieldName">
						<cp:YesNoRadioButtonList id="ynEnableAbout" runat="server" CssClass="ControlPanelTextInput1" RepeatColumns="2"></cp:YesNoRadioButtonList>
						</div>
					</td>
				</tr>
			</table>
			<DIV class="CommonFormFieldName">
				<cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_Photos_AboutOptions_AboutTitle_Help"></cp:helpicon>
				<CP:FormLabel id="FormLabel1" runat="server" resourcename="CP_Photos_AboutOptions_AboutTitle"
					ControlToLabel="AboutTitle"></CP:FormLabel></DIV>
			<DIV class="CommonFormField">
				<asp:TextBox id="AboutTitle" CssClass="ControlPanelTextInput" Runat="Server"></asp:TextBox></DIV>
			<DIV class="CommonFormFieldName">
				<cp:helpicon id="Helpicon2" runat="Server" resourcename="CP_Photos_AboutOptions_AboutDescription_Help"></cp:helpicon>
				<CP:FormLabel id="FormLabel2" runat="server" resourcename="CP_Photos_AboutOptions_AboutDescription" ControlToLabel="Description"></CP:FormLabel></DIV>
			<DIV class="CommonFormField">
				<CSControl:Editor id="AboutDescription" runat="Server" Height="225px" /></DIV>
			</div> 
			<p class="PanelSaveButton DetailsFixedWidth">
				<cp:ResourceLinkButton id="SaveButton" runat="Server" CssClass="CommonTextButton" ResourceName="Save"></cp:ResourceLinkButton></asp:LinkButton></p>
		</DIV>
	</CP:Content>
</CP:Container>

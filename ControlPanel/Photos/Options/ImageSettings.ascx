<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ImageSettings.ascx.cs" Inherits="CommunityServer.ControlPanel.Galleries.Options.ImageSettings" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<td class="CommonFormField"><asp:textbox id="ImageX" runat="server" MaxLength="4" Size="4"></asp:textbox><asp:regularexpressionvalidator id="Regularexpressionvalidator1" runat="server" NAME="Regularexpressionvalidator1"
		ErrorMessage="*" Cssclass="validationWarning" ValidationExpression="[0-9]*" ControlToValidate="ImageX"></asp:regularexpressionvalidator><asp:requiredfieldvalidator id="ImageXValidator" runat="server" ErrorMessage="*" ControlToValidate="ImageX"
		Font-Bold="True"></asp:requiredfieldvalidator></td>
<td class="CommonFormField"><asp:textbox id="ImageY" runat="server" MaxLength="4" Size="4"></asp:textbox><asp:regularexpressionvalidator id="Regularexpressionvalidator2" runat="server" NAME="Regularexpressionvalidator2"
		ErrorMessage="*" Cssclass="validationWarning" ValidationExpression="[0-9]*" ControlToValidate="ImageY"></asp:regularexpressionvalidator><asp:requiredfieldvalidator id="ImageYValidator" runat="server" ErrorMessage="*" ControlToValidate="ImageY"
		Font-Bold="True"></asp:requiredfieldvalidator></td>
<td class="CommonFormField"><asp:textbox id="Quality" runat="server" MaxLength="3" size="3"></asp:textbox>%
	<asp:regularexpressionvalidator id="Regularexpressionvalidator3" runat="server" NAME="Regularexpressionvalidator3"
		ErrorMessage="*" Cssclass="validationWarning" ValidationExpression="[0-9]*" ControlToValidate="Quality"></asp:regularexpressionvalidator><asp:requiredfieldvalidator id="QualityValidator" runat="server" ErrorMessage="*" ControlToValidate="Quality"
		Font-Bold="True"></asp:requiredfieldvalidator></td>
<td class="CommonFormField"><asp:panel id="BrightnessPanel" runat="server">
		<asp:TextBox id="Brightness" runat="server" MaxLength="3" Size="4"></asp:TextBox>
		<asp:RegularExpressionValidator id="RegularexpressionBrightnessValidator" runat="server" NAME="RegularexpressionBrightnessValidator"
			ErrorMessage="*" Cssclass="validationWarning" ValidationExpression="^[-]*[0-9]*" ControlToValidate="Brightness"></asp:RegularExpressionValidator>
		<asp:RequiredFieldValidator id="BrightnessValidator" runat="server" ErrorMessage="*" ControlToValidate="Brightness"
			Font-Bold="True"></asp:RequiredFieldValidator>
	</asp:panel></td>

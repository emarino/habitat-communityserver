<%@ Page language="c#" Codebehind="DefaultPostSettings.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Galleries.Options.DefaultPostSettings" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="GalleryControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
	<CP:ResourceControl runat="server" ResourceName="CP_Photos_DefaultPostSettings_Title" />
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">
	<CP:StatusMessage id="Status" runat="server" ResourceName="CP_Settings_SiteContent_SettingsSavedSuccessfully" />
	<DIV class="CommonDescription">
		<CP:ResourceControl runat="server" ResourceName="CP_Photos_DefaultPostSettings_SubTitle" />
	</DIV>
	<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Galleries" />

	<TABLE cellSpacing="1" cellPadding="4" border="0">
	<TR>
		<TD class="CommonFormFieldName">
			<cp:helpicon  runat="Server" resourcename="CP_Photos_PostOptions_AllowReplies_Help" />
			<CP:FormLabel  runat="Server" ResourceName="CP_Photos_PostOptions_AllowReplies" ControlToLabel="ynEnableReplies" />
		</TD>
		<TD class="CommonFormField">
			<cp:YesNoRadioButtonList id="ynEnableReplies" runat="server" CssClass="txt1" RepeatColumns="2" />
		</TD>
	</TR>
	<TR>
		<TD class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_Photos_PostOptions_ModerateComments_Help" />
			<CP:FormLabel runat="Server" ResourceName="CP_Photos_PostOptions_ModerateComments" ControlToLabel="ModerationDDL" />
		</TD>
		<TD class="CommonFormField">
			<cp:CommentModerationDropDownList id="ModerationDDL" runat="Server" />
		</TD>
	</TR>
	<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_Photos_PostOptions_FeedbackNotify_Help" />
			<cp:formlabel runat="Server" controltolabel="NotificationType" resourcename="CP_Photos_PostOptions_FeedbackNotify" />
		</td>
		<td class="CommonFormField">
			<cp:feedbacknotificationdropdownlist id="NotificationType" runat="server" cssclass="ControlPanelTextInput1" />
		</td>
	</tr>
	<TR>
		<TD class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_Photos_PostOptions_EnableRatings_Help" />
			<CP:FormLabel runat="Server" ResourceName="CP_Photos_PostOptions_EnableRatings" ControlToLabel="ynEnableRatings" />
		</TD>
		<TD class="CommonFormField">
			<cp:YesNoRadioButtonList id="ynEnableRatings" runat="server" CssClass="txt1" RepeatColumns="2" />
		</TD>
	</TR>
	<TR>
		<TD class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_Photos_PostOptions_EnableTrackBacks_Help" />
			<CP:FormLabel runat="Server" ResourceName="CP_Photos_PostOptions_EnableTrackBacks" ControlToLabel="ynEnableTrackbacks" />
		</TD>
		<TD class="CommonFormField">
			<cp:YesNoRadioButtonList id="ynEnableTrackbacks" runat="server" CssClass="txt1" RepeatColumns="2" />
		</TD>
	</TR>
	<TR>
		<TD class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_Photos_PostOptions_EnableOrderPrints_Help" />
			<CP:FormLabel runat="Server" ResourceName="CP_Photos_PostOptions_EnableOrderPrints" ControlToLabel="ynEnableOrderPrints" />
		</TD>
		<TD class="CommonFormField">
			<cp:YesNoRadioButtonList id="ynEnableOrderPrints" runat="server" CssClass="txt1" RepeatColumns="2" />
		</TD>
	</TR>
	<TR>
		<TD class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_Photos_PostOptions_AggregatePost_Help" />
			<CP:FormLabel runat="Server" ResourceName="CP_Photos_PostOptions_AggregatePost" ControlToLabel="ynAggregatePost" />
		</TD>
		<TD class="CommonFormField">
			<cp:YesNoRadioButtonList id="ynAggregatePost" runat="server" CssClass="txt1" RepeatColumns="2" />
		</TD>
	</TR>
	<TR>
		<TD class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_Photos_PostOptions_CommunityParticipation_Help" />
			<CP:FormLabel runat="Server" ResourceName="CP_Photos_PostOptions_CommunityParticipation" ControlToLabel="ynCommunity" />
		</TD>
		<TD class="CommonFormField">
			<cp:YesNoRadioButtonList id="ynCommunity" runat="server" CssClass="txt1" RepeatColumns="2" />
		</TD>
	</TR>
	<TR>
		<TD class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_Photos_PostOptions_SyndicateExcerpt_Help" />
			<CP:FormLabel runat="Server" ResourceName="CP_Photos_PostOptions_SyndicateExcerpt" ControlToLabel="ynSyndicateExcerpt" />
		</TD>
		<TD class="CommonFormField">
			<cp:YesNoRadioButtonList id="ynSyndicateExcerpt" runat="server" CssClass="txt1" RepeatColumns="2" />
		</TD>
	</TR>
    </TABLE>

	<P class="PanelSaveButton DetailsFixedWidth">
		<cp:ResourceLinkButton id="SaveButton" runat="Server" CssClass="CommonTextButton" ResourceName="Save" />
	</p>
</CP:Content>
</CP:Container>

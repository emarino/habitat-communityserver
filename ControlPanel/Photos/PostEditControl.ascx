<%@ Import Namespace = "CommunityServer.Components" %>
<%@ Control CodeBehind="PostEditControl.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Photos.PostEditControl" %>
<%@ Register TagPrefix="CP" TagName = "CategoryChecklist" Src = "~/ControlPanel/Photos/CategoryChecklistControl.ascx" %>
<script type="text/javascript">
	// <![CDATA[
	   function getAttachment(res)
       {
			if (res)
			{
				$('<%= ServeruploadtempID.ClientID %>').value = res[0];
				$('<%= ServerFileName.ClientID %>').value = res[1];
				$('<%= ServerFriendlyFileName.ClientID %>').value = res[2];
				Element.show('RemoveTempServerUploadLink');
	            
				var fileOrLink = $('<%= FileOrLinkMessage.ClientID %>');
				if(res[3])
				{
					fileOrLink.innerHTML = 'File';
				}
				else
				{
					fileOrLink.innerHTML = 'Link';
				}
            }
       }
       
       function removeServerID()
       {
            $('<%= ServeruploadtempID.ClientID %>').value = '';
            $('<%= ServerFileName.ClientID %>').value = '';
            $('<%= ServerFriendlyFileName.ClientID %>').value = '';
             Element.hide('RemoveTempServerUploadLink');
       }
       
       function SelectPreview()
       {
            $('PreviewBody').innerHTML = '';
            Element.show('loading');
            PostEditControl.PreviewPost('<%= this.ClientID %>',<%= PostBody.GetContentScript() %>,previewCallBack);
            $('PreviewTitle').innerHTML = $('<%= ServerFriendlyFileName.ClientID %>').value;
            SelectPreviewAttachment();
       }
       
       function previewCallBack(res)
       {
            Element.hide('loading');
            $('PreviewBody').innerHTML = res.value;
       }
       
       function SelectPreviewAttachment()
       {
            $('PreviewAttachment').innerHTML = '';
            var attachmentVal = '<%= this.PostID.ToString() %>';
            if($('<%= ServeruploadtempID.ClientID %>').value != '')
				var attachmentVal = $('<%= ServeruploadtempID.ClientID %>').value
				
          PostEditControl.PreviewAttachment('<%= this.ClientID %>',attachmentVal,previewAttachmentCallBack);
       }

       function previewAttachmentCallBack(res)
       {
            $('PreviewAttachment').innerHTML = res.value;
       }

       // ]]>
</script>
<CP:STATUSMESSAGE id="Status" runat="server"></CP:STATUSMESSAGE>
<TWC:TabbedPanes id="EditorTabs" runat="server"
	PanesCssClass="CommonPane"
	TabSetCssClass="CommonPaneTabSet"
	TabCssClasses="CommonPaneTab,CommonPaneTab1,CommonPaneTab2"
	TabSelectedCssClasses="CommonPaneTabSelected,CommonPaneTabSelected1,CommonPaneTabSelected2"
	TabHoverCssClasses="CommonPaneTabHover,CommonPaneTabHover1,CommonPaneTabHover2"
	>
<TWC:TabbedPane runat="server">
	<Tab><CP:ResourceControl runat="server" ResourceName="CP_Photos_PostEdit_Tab_Content" /></Tab>
	<Content>
		<div style="float:right;">
			<cp:GalleryImage id="Galleryimage1" ImageType="Thumbnail" runat="server" />
		</div>
		<asp:Panel id="ExistingAttachmentPanel" Runat="server">
			<div class="CommonFormFieldName">
				<cp:helpicon id="Helpicon4" runat="Server" ResourceName="CP_Photos_PostEdit_FileToUpload_Detail" />
				<CP:FormLabel id="Formlabel17" runat="Server" ControlToLabel="PictureData" ResourceName="CP_Photos_PostEdit_File" />
			</div>
			<div class="CommonFormField">
				<asp:Literal Runat="server" id="ExistingFileName" />
			</div>
		</asp:Panel>
		<asp:Panel id="ServerUploadPanel" Runat="server">
			<div class="CommonFormFieldName">
				<cp:helpicon id="Helpicon9" runat="Server" ResourceName="CP_Photos_PostEdit_FileToUpload_Detail" />
				<CP:FormLabel id="FileOrLinkMessage" runat="Server" ControlToLabel="PictureData" ResourceName="CP_Photos_PostEdit_FileToUpload" />
				<asp:CustomValidator EnableClientScript="True" ClientValidationFunction="CheckFileUploadID" runat="server" ErrorMessage="*" ControlToValidate="ServerFriendlyFileName"
					ID="pictureDataValidator" OnServerValidate="ValidatePictureData" />
			</div>
			<div class="CommonFormField">
				<asp:TextBox Class="ControlPanelTextInput" id="ServerFileName" Runat="server" ReadOnly="false" />
				<input type="hidden" id="ServeruploadtempID" runat="server" NAME="ServeruploadtempID">
				<cp:Modal ModalType="button" Url="Attachment.aspx?Upload=true" Width="640" Height="180" runat="Server"
					callback="getAttachment" ResourceName="CP_Photos_PostEdit_UploadPhoto" ResourceFile="ControlPanelResources.xml" id="Modal2" /><button id="RemoveTempServerUploadLink" onclick="javascript:removeServerID();" style="display:none"><CP:ResourceControl ResourceName="CP_Photos_PostEdit_Remove" ID="RemoveResource" runat="server" /></button>
			</div>
			<div class="CommonFormFieldName">
				<cp:helpicon id="Helpicon14" runat="Server" ResourceName="CP_Photos_PostEdit_Subject_Detail" />
				<CP:FormLabel id="Formlabel12" runat="Server" ControlToLabel="PostSubject" ResourceName="CP_Photos_PostEdit_Subject" />
				<asp:requiredfieldvalidator id="postSubjectValidator" EnableClientScript="True" runat="server" CssClass="validationWarning"
					ControlToValidate="ServerFriendlyFileName">*</asp:requiredfieldvalidator>
			</div>
			<div class="CommonFormField">
				<asp:TextBox Runat="server" CssClass="ControlPanelTextInput" id="ServerFriendlyFileName" />
			</div>
		</asp:Panel>
		<asp:PlaceHolder Runat="server" ID="TagSelectionContainer">
			<div class="CommonFormFieldName">
				<cp:helpicon id="Helpicon17" runat="Server" resourcename="CP_Photos_PostEdit_Tags_Detail" />
				<cp:formlabel id="Formlabel14" runat="Server" resourcename="CP_Photos_PostEdit_Tags" />
			</div>
			<div class="CommonFormField">
				<cp:TagEditor runat="server" id="Tags">
				    <SkinTemplate>
                        <table cellpadding="0" cellspacing="0" border="0">
                        <tr valign="top">
                        <td>
	                        <asp:TextBox Columns="70" ID="Tags" runat="server" />
                        </td>
                        <td>
	                        <cp:Modal modaltype="Button" width="400" height="300" runat="Server" id="SelectTags" ResourceName="TagEditor_SelectTags" />
                        </td>
                        </tr>
                        </table>
				    </SkinTemplate>
				</cp:TagEditor>
			</div>
		</asp:PlaceHolder>
		<div class="CommonFormFieldName">
			<cp:helpicon id="Helpicon2" runat="Server" ResourceName="CP_Photos_PostEdit_Desc_Detail" />
			<CP:FormLabel id="Formlabel1" runat="Server" ControlToLabel="PostBody" ResourceName="CP_Photos_PostEdit_Desc" />
		</div>
		<div class="CommonFormField">
			<CSControl:Editor runat="Server" id="PostBody" width="100%" columns="110" height="250" />
		</div>
	</Content>
</TWC:TabbedPane>
<TWC:TabbedPane runat="server">
	<Tab><CP:ResourceControl runat="server" ResourceName="CP_Photos_PostEdit_Tab_Options" /></Tab>
	<Content>
		<div class="CommonFormFieldName">
			<cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_Photos_PostEdit_Name_Detail" />
			<CP:FormLabel id="Formlabel8" runat="Server" ControlToLabel="postName" ResourceName="CP_Photos_PostEdit_Name" />
			<asp:RegularExpressionValidator id="vRegexPostName" ControlToValidate="postName" runat="server" Display="Dynamic" />
		</div>
		<div class="CommonFormField">
			<asp:TextBox id="postName" CssClass="ControlPanelTextInput" MaxLength="256" runat="server" />
		</div>
		<div class="CommonFormFieldName">
			<cp:helpicon id="Helpicon8" runat="Server" resourcename="CP_Photos_PostEdit_Excerpt_Detail" />
			<CP:FormLabel id="Formlabel2" runat="Server" ControlToLabel="postExcerpt" ResourceName="CP_Photos_PostEdit_Excerpt" />
		</div>
		<div class="CommonFormField">
			<asp:TextBox id="postExcerpt" Columns="70" CssClass="ControlPanelTextInput" MaxLength="500" runat="server"
				TextMode="MultiLine" Rows="5" />
		</div>
		
		<asp:PlaceHolder Runat="server" ID="AlbumSelectionContainer">
			<div class="CommonFormFieldName">
				<cp:helpicon id="Helpicon3" runat="Server" ResourceName="CP_Photos_PostEdit_Categories_Detail" />
				<CP:FormLabel id="Formlabel4" runat="Server" ControlToLabel="Categories" ResourceName="CP_Photos_PostEdit_Categories" />
			</div>
			<div class="CommonFormField">
				<CP:CategoryChecklist id="CategoryChecklist1" runat="server" />
			</div>
		</asp:PlaceHolder>
		
		<div class="CommonFormFieldName">
			<cp:helpicon id="Helpicon5" runat="Server" resourcename="CP_Photos_PostEdit_PostDate_Detail" />
			<CP:FormLabel id="Formlabel5" runat="Server" ControlToLabel="DatePicker" ResourceName="CP_Photos_PostEdit_PostDate" />
				<br />
				<nobr>
					<TWC:DateTimeSelector runat="server" id="DatePicker" DateTimeFormat="MMMM d yyyy" ShowCalendarPopup="true" />
					<CP:ResourceControl ResourceName="CP_Photos_PostEdit_At" ID="ResourceControl2" runat="server" />
					<cp:DropDownRange Start="1" Factor="1" Max="12" runat="Server" id="HourPicker" />
					:
					<cp:DropDownRange Start="0" Factor="1" Max="59" runat="Server" id="MinutePicker" Format="00" />
					:
					<asp:DropDownList id="AMPM" Runat="server">
						<asp:ListItem Value="AM">AM</asp:ListItem>
						<asp:ListItem Value="PM">PM</asp:ListItem>
					</asp:DropDownList>
				</nobr>
			</div>
	</Content>
</TWC:TabbedPane>
<TWC:TabbedPane runat="server">
	<Tab><CP:ResourceControl runat="server" ResourceName="CP_Photos_PostEdit_Tab_AdvancedOptions" /></Tab>
	<Content>
		<table cellspacing="0" cellpadding="0" border="0" id="AdvancedProperties" runat="server">
			<tr>
				<td class="CommonFormFieldName">
					<cp:helpicon id="Helpicon6" runat="Server" resourcename="CP_Photos_PostEdit_Published_Detail" />
					<CP:FormLabel id="Formlabel6" runat="Server" ControlToLabel="ynPublished" ResourceName="CP_Photos_PostEdit_Published" />
				</td>
				<td class="CommonFormField">
					<cp:YesNoRadioButtonList id="ynPublished" runat="server" RepeatColumns="2" CssClass="txt1" />
				</td>
			</tr>
			<tr>
				<td class="CommonFormFieldName">
					<cp:helpicon id="Helpicon7" runat="Server" resourcename="CP_Photos_PostEdit_AllowReplies_Detail" />
					<CP:FormLabel id="Formlabel7" runat="Server" ControlToLabel="ynEnableReplies" ResourceName="CP_Photos_PostEdit_AllowReplies" />
				</td>
				<td class="CommonFormField">
					<cp:YesNoRadioButtonList id="ynEnableReplies" runat="server" RepeatColumns="2" CssClass="txt1" />
				</td>
			</tr>
			<TR>
				<TD class="CommonFormFieldName">
					<cp:helpicon id="Helpicon10" runat="Server" resourcename="CP_Photos_PostEdit_ModerateComments_Detail" />
					<CP:FormLabel id="Formlabel9" runat="Server" ControlToLabel="ModerationDDL" ResourceName="CP_Photos_PostEdit_ModerateComments" />
				</TD>
				<TD class="CommonFormField">
					<cp:CommentModerationDropDownList runat="Server" id="ModerationDDL" />
				</TD>
			</TR>
			<tr>
				<td class="CommonFormFieldName">
					<cp:helpicon id="Helpicon13" runat="Server" resourcename="CP_Photos_PostEdit_FeedbackNotify_Detail" />
					<cp:formlabel id="Formlabel3" runat="Server" controltolabel="NotificationType" resourcename="CP_Photos_PostEdit_FeedbackNotify" />
				</td>
				<td class="CommonFormField">
					<cp:feedbacknotificationdropdownlist id="NotificationType" runat="server" cssclass="ControlPanelTextInput1" />
				</td>
			</tr>
			<tr runat="server" id="ynNotifyAllOwners_Row">
				<td class="CommonFormFieldName">
					<cp:helpicon id="Helpicon15" runat="Server" resourcename="CP_Photos_PostEdit_NotifyAllOwners_Detail" />
					<cp:formlabel id="Formlabel16" runat="Server" controltolabel="ynNotifyAllOwners" resourcename="CP_Photos_PostEdit_NotifyAllOwners" />
				</td>
				<td class="CommonFormField">
					<cp:yesnoradiobuttonlist id="ynNotifyAllOwners" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput1" />
				</td>
			</tr>
			<tr runat="server" id="ynEnableRatings_Row">
				<td class="CommonFormFieldName" >
					<cp:helpicon id="Helpicon11" runat="Server" resourcename="CP_Photos_PostEdit_EnableRatings_Detail" />
					<CP:FormLabel id="Formlabel10" runat="Server" ControlToLabel="ynEnableRatings" ResourceName="CP_Photos_PostEdit_EnableRatings" />
				</td>
				<td class="CommonFormField">
					<cp:YesNoRadioButtonList id="ynEnableRatings" runat="server" RepeatColumns="2" CssClass="txt1" />
				</td>
			</tr>
			<tr runat="server" id="ynEnableTrackbacks_Row">
				<td class="CommonFormFieldName"  >
					<cp:helpicon id="Helpicon12" runat="Server" resourcename="CP_Photos_PostEdit_EnableTrackBacks_Detail" />
					<CP:FormLabel id="Formlabel11" runat="Server" ControlToLabel="ynEnableTrackbacks" ResourceName="CP_Photos_PostEdit_EnableTrackBacks" />
				</td>
				<td class="CommonFormField">
					<cp:YesNoRadioButtonList id="ynEnableTrackbacks" runat="server" RepeatColumns="2" CssClass="txt1" />
				</td>
			</tr>
			<tr runat="server" id="ynEnableOrderPrints_Row">
				<td class="CommonFormFieldName">
					<cp:helpicon id="Helpicon16" runat="Server" resourcename="CP_Photos_PostOptions_EnableOrderPrints_Help" />
					<CP:FormLabel id="Formlabel13" runat="Server" ControlToLabel="ynEnableOrderPrints" ResourceName="CP_Photos_PostOptions_EnableOrderPrints" />
				</td>
				<td class="CommonFormField">
					<cp:YesNoRadioButtonList id="ynEnableOrderPrints" runat="server" RepeatColumns="2" CssClass="txt1" />
				</td>
			</tr>
		</table>
	</Content>
</TWC:TabbedPane>
<TWC:TabbedPane runat="server">
	<Tab OnClickClientFunction="SelectPreview"><CP:ResourceControl runat="server" ResourceName="CP_Photos_PostEdit_Tab_Preview" /></Tab>
	<Content>
		<table cellspacing="1" cellpadding="4" border="0">
			<tr>
				<td class="CommonFormFieldName">
					<h3 id="PreviewTitle"></h3>
					<span id="PreviewAttachment"></span>
					<p><span id="PreviewBody"></span></p>
					<span id="loading" style="display:none;">
						<center>
							<CP:ResourceControl id="ResourceControl1" runat="Server" resourcename="CP_Loading" />&nbsp;<img src="<%=CommunityServer.Components.SiteUrls.Instance().LoadingImageUrl%>" />
						</center>
					</span>
				</td>
			</tr>
		</table>
	</Content>
</TWC:TabbedPane>
</TWC:TabbedPanes>

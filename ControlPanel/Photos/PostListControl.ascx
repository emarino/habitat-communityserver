<%@ Control Language="c#" AutoEventWireup="false" Codebehind="PostListControl.ascx.cs" Inherits="CommunityServer.ControlPanel.Photos.PostListControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Import Namespace="CommunityServer.Components" %>
<script type="text/javascript">
// <![CDATA[
  function SelectColumn(column)
  {
	alert(column.GetMember('PostID').Text);
  }
  function onCallbackError(excString)
  {
    if (confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_CallBackWarning") %>')) alert(excString); 
    <%= Grid1.ClientID %>.Page(1); 
  }

  function onDelete(item)
  {
      return confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Photos_PostList_Delete_Warning") %>'); 
  }
  function deleteRow(rowId)
  {
    <%= Grid1.ClientID %>.Delete(<%= Grid1.ClientID %>.GetRowFromClientId(rowId)); 
  }
  
  var viewName = '<cp:resourcecontrol runat="server" resourcename="View" />';
  var editName = '<cp:resourcecontrol runat="server" resourcename="Edit" />';
  var deleteName = '<cp:resourcecontrol runat="server" resourcename="Delete" />';
  var referralsName = '<cp:resourcecontrol runat="server" resourcename="CP_Referrals" />';  
// ]]>
</script>
	<table cellpadding="0" cellspacing="0" border="0"  width="100%">
		<tr>
			<td width="100%">
				<CP:ResourceControl id="FeedbackFilterLabel" runat="Server" resourcename="CP_Photos_Referrals_CategoryFilter" />
				<cp:PARENTCATEGORYDROPDOWN id="CategoryList" AutoPostBack="True" Runat="server" />
			</td>
			<td nowrap="nowrap">
				<A href='<%= String.Format("{0}?cid={1}",CommunityServer.Galleries.Components.GalleryUrls.Instance().ControlPanel_Photos_NewPost(), Globals.SafeInt(CategoryList.SelectedValue, -1).ToString()) %>' class="CommonTextButton" ><CP:ResourceControl id="Resourcecontrol1" runat="Server" resourcename="CP_Photos_AddNewPhoto" /></A>
			</td>
		</tr>
	</table>
<br />
<div id="GrayGrid">
	<CA:GRID id="Grid1" runat="Server" ClientSideOnCallbackError="onCallbackError"
		ClientSideOnDelete="onDelete" AutoCallBackOnDelete="true" EditOnClickSelectedItem="false" CssClass="Grid">
		<Levels>
			<CA:GridLevel DataKeyField="PostID">
				<Columns>
					<CA:GridColumn DataField="ThumbnailURL" HeadingText=" " AllowGrouping="false" AllowSorting="false" FixedWidth="True" DataCellClientTemplateId="ThumbnailTemplate" width="55" />
					<CA:GridColumn DataField="Subject" HeadingText="ResourceManager.CP_Photos_GridCol_Title" />
					<CA:GridColumn DataField="Views" HeadingText="ResourceManager.CP_Photos_GridCol_Views" Align="Center"
						width="60" />
					<CA:GridColumn DataField="AggViews" HeadingText="ResourceManager.CP_Photos_GridCol_AggViews" Align="Center"
						width="70" />
					<CA:GridColumn DataField="Replies" HeadingText="ResourceManager.CP_Photos_GridCol_Comments" Align="Center"
						width="70" />
					<CA:GridColumn DataCellServerTemplateId="PostDateTemplate" HeadingText="ResourceManager.CP_Photos_GridCol_PublishedDate" width="150" AllowSorting="false" />
					<CA:GridColumn DataField="IsApproved" HeadingText="ResourceManager.CP_Photos_GridCol_Published" />
					<CA:GridColumn DataField="Username" Visible="false" />
					<CA:GridColumn DataField="PostDate" Visible="false" />
					<CA:GridColumn DataField="DisplayName" Visible="false" />
					<CA:GridColumn DataField="SectionID" Visible="false" />
					<CA:GridColumn DataField="PostID" Visible="false" />
					<CA:GridColumn DataField="AuthorUrl" Visible="false" />
					<CA:GridColumn DataField="ViewPictureURL" Visible="false" />
					<CA:GridColumn DataField="GalleryPostType" Visible="false" />
					<CA:GridColumn DataField="PostID" Visible="false" />
					<CA:GridColumn HeadingText="ResourceManager.CP_Photos_GridCol_Actions" AllowGrouping="false" AllowSorting="false" DataField="GalleryPostType" DataCellClientTemplateId="ActionTemplate" width="250" DataCellCssClass="LastDataCell" Align="Center" />
				</Columns>
			</CA:GridLevel>
		</Levels>
		<ClientTemplates>
			<CA:ClientTemplate Id="ThumbnailTemplate">
				<a href='posteditor.aspx?sectionid=##  DataItem.GetMember("SectionID").Text ##&PostID=## DataItem.GetMember("PostID").Text ##'><img src='## DataItem.GetMember("ThumbnailURL").Text ##' width="50" border="0" /></a>
			</CA:ClientTemplate>
			<CA:ClientTemplate Id="ActionTemplate">
			<a href='##  DataItem.GetMember("ViewPictureURL").Text ##' class="CommonTextButton">
					##viewName##</a> 
			<a href='posteditor.aspx?sectionid=##  DataItem.GetMember("SectionID").Text ##&PostID=## DataItem.GetMember("PostID").Text ##' class="CommonTextButton">
					##editName##</a> 
            <a href="javascript:deleteRow('## DataItem.ClientId ##')" class="CommonTextButton">##deleteName##</a> 
            <a href="referrals.aspx?pid=## DataItem.GetMember("PostID").Text ##" class="CommonTextButton">##referralsName##</a><!-- |
            <a href='## DataItem.GetMember("ViewPictureURL").Text ##'>View</a>-->
        </CA:ClientTemplate>
		</ClientTemplates>
		<ServerTemplates>
		    <CA:GridServerTemplate id="PostDateTemplate">
		        <Template>
		        <table width="100%" cellspacing="0" cellpadding="1" border="0">
				    <tr>
					    <td class="CellText" align="right"> 
					        <CP:ResourceControl runat="server" ResourceName="CP_By" /> 
					        
					        
					        <a style="color:#595959;" href=<%# Container.DataItem["AuthorUrl"]%>><b><%# Container.DataItem["DisplayName"]%></b></a>
					    </td>
				    </tr>
				    <tr>
					    <td class="CellText" align="right">
					        <font color="#595959"><%# UserTime.ConvertToUserTime((DateTime)Container.DataItem["PostDate"]).ToString(CSContext.Current.User.Profile.DateFormat + " hh:mm tt")%></font></td>
				    </tr>
			    </table>
		        </Template>
		    </CA:GridServerTemplate>
		</ServerTemplates>
	</CA:GRID></div>
<p class="PanelSaveButton"><asp:RegularExpressionValidator id="AttachmentDataValidator" Text="CP_Photos_Attachment_UploadAttachment_InvlalidFileType" Runat="server" ControlToValidate="PictureData"></asp:RegularExpressionValidator><CP:ResourceControl id="ResourceControl2" runat="Server" resourcename="CP_Photos_PostList_QuickAdd" /> <input id="PictureData" type="file" name="PictureData" runat="server"></p>
<CP:STATUSMESSAGE id="Status" runat="server"></CP:STATUSMESSAGE>

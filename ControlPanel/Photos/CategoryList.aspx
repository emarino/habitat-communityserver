<%@ Page language="c#" Codebehind="CategoryList.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Photos.CategoryList" %>
<%@ Register TagPrefix="CP" TagName = "CategoryList" Src = "~/ControlPanel/Photos/CategoryListControl.ascx" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="GalleryControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_Photos_Categories_Title"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<CP:ResourceControl id="Section_Sub_Description" runat="server" ResourceName="CP_Photos_Categories_SubTitle"></CP:ResourceControl></DIV>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Galleries" />
		<CP:CategoryList id="Categorylist1" runat="Server"></CP:CategoryList>
	</CP:Content>
</CP:Container>

<%@ Control Language="c#" AutoEventWireup="false" Codebehind="CommentListControl.ascx.cs" Inherits="CommunityServer.ControlPanel.Photos.CommentListControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Import Namespace="CommunityServer.Components" %>
<script type="text/javascript">
  function onCallbackError(excString)
  {
    if (confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_CallBackWarning") %>')) alert(excString); 
    <%= Grid1.ClientID %>.Page(1); 
  }

  function onDelete(item)
  {
		return confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Photos_Comments_Delete_Warning") %>'); 
  }

  function deleteRow(rowId)
  {
    <%= Grid1.ClientID %>.Delete(<%= Grid1.ClientID %>.GetRowFromClientId(rowId)); 
  }

  function validate_click()
  {
	var element = $('<%=  ActionList.ClientID %>');
	
	if(element.options[0].selected)
		return false;
	
	if(element.options[3].selected)
	{
		return confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Photos_Comments_Delete_Warning") %>'); 
	}
	return true;
  }

	var viewName = '<cp:resourcecontrol runat="server" resourcename="View" />';
	var editName = '<cp:resourcecontrol runat="server" resourcename="Edit" />';
	var deleteName = '<cp:resourcecontrol runat="server" resourcename="Delete" />';
</SCRIPT>
<DIV id="Filters">
		<CP:ResourceControl id="FeedbackFilterLabel" runat="Server" resourcename="Feedback_Filter"></CP:ResourceControl>
		<asp:dropdownlist id="filterPost" runat="server"></asp:dropdownlist>
		<asp:dropdownlist id="filterPublished" runat="server"></asp:dropdownlist>&nbsp;
		<CP:ResourceButton id="FilterButton" Runat="server" ResourceName="CP_ApplyFilter" />
</DIV>
<div id="GrayGrid"><CA:GRID id="Grid1" runat="server" ClientSideOnCallbackError="onCallbackError"
		ClientSideOnDelete="onDelete" AutoCallBackOnDelete="true"
		EditOnClickSelectedItem="false">
		<Levels>
			<CA:GridLevel DataKeyField="PostID">
				<Columns>
					<CA:GridColumn ColumnType="CheckBox" Width="40" Align="Center" AllowEditing="True" AllowSorting="false" />
					<CA:GridColumn DataField="Subject" HeadingText="ResourceManager.CP_Photos_GridCol_Title" DataCellClientTemplateId="CommentTemplate"  TextWrap="True" AllowSorting="false" />
					<CA:GridColumn DataCellServerTemplateId="PostDateTemplate"  HeadingText="ResourceManager.CP_Photos_GridCol_PublishedDate" AllowSorting="false" />
					<CA:GridColumn DataField="IsApproved" HeadingText="ResourceManager.CP_Photos_GridCol_Approved"  AllowSorting="false" />
					<CA:GridColumn DataField="PostID" Visible="False" />
					<CA:GridColumn DataField="PostDate" Visible="False" />
					<CA:GridColumn DataField="SectionID" Visible="False" />
					<CA:GridColumn DataField="DisplayName" Visible="False" />
					<CA:GridColumn DataField="AuthorUrl" Visible="False" />
					<CA:GridColumn DataField="ForceExcerpt" Visible="False" />
					<CA:GridColumn DataField="ViewPictureURL" Visible="False" />
					<CA:GridColumn HeadingText="ResourceManager.CP_Photos_GridCol_Actions" AllowGrouping="false" AllowSorting="false" 
 DataCellClientTemplateId="EditTemplate" EditControlType="EditCommand" Align="Center" Width="130"
						FixedWidth="True" DataCellCssClass="LastDataCell"  TextWrap="True" />
				</Columns>
			</CA:GridLevel>
		</Levels>
		<ClientTemplates>
			<CA:ClientTemplate Id="CommentTemplate">
			<a href="javascript:Telligent_Modal.Open('commenteditor.aspx?pid=##  DataItem.GetMember("PostID").Text ##', 625, 450, null);"><strong>## 
						DataItem.GetMember("Subject").Text ##</strong></a><br />## DataItem.GetMember("ForceExcerpt").Text ##
        </CA:ClientTemplate>
			<CA:ClientTemplate Id="EditTemplate">
			<a href="## DataItem.GetMember('ViewPictureURL').Text  ##" class="CommonTextButton" >##viewName##</a> 
			<!-- a href="javascript:authorizeItem('## DataItem.ClientId ##');">Authorize</a> | -->
			<a href="javascript:Telligent_Modal.Open('commenteditor.aspx?pid=##  DataItem.GetMember("PostID").Text ##', 625, 458, null);" class="CommonTextButton">##editName##</a> 
			<a href="javascript:deleteRow('## DataItem.ClientId ##')" class="CommonTextButton">##deleteName##</a>
		</CA:ClientTemplate>
			<CA:ClientTemplate Id="EditCommandTemplate">
            <a href="javascript:editRow();" class="CommonTextButton">Update</a> <a href="javascript:editRowCancel();" class="CommonTextButton">Cancel</a>
        </CA:ClientTemplate>
			<CA:ClientTemplate Id="InsertCommandTemplate">
            <a href="javascript:insertRow();" class="CommonTextButton">Insert</a> <a href="javascript:editRowCancel();" class="CommonTextButton">Cancel</a>
        </CA:ClientTemplate>
		</ClientTemplates>
		<ServerTemplates>
		    <CA:GridServerTemplate id="PostDateTemplate">
		        <Template>
		        <table width="100%">
				    <tr>
					    <td class="CellText" align="right"> 
					        <CP:ResourceControl runat="server" ResourceName="CP_By" /> 
					        <a style="color:#595959;" href=<%# Container.DataItem["AuthorUrl"]%>><b><%# Container.DataItem["DisplayName"]%></b></a>
					    </td>
				    </tr>
				    <tr>
					    <td class="CellText" align="right">
					        <font color="#595959"><%# UserTime.ConvertToUserTime((DateTime)Container.DataItem["PostDate"]).ToString(CSContext.Current.User.Profile.DateFormat + " hh:mm tt")%></font></td>
				    </tr>
			    </table>
		        </Template>
		    </CA:GridServerTemplate>
		</ServerTemplates>
	</CA:GRID></div>
<p id="Actions"  class="PanelSaveButton"><cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_Moderate_Help"></cp:helpicon>&nbsp;<asp:dropdownlist id="ActionList" runat="server"></asp:dropdownlist>&nbsp;<cp:resourcebutton id="ActionButton" runat="Server" resourcename="CP_Go"></cp:resourcebutton></p>

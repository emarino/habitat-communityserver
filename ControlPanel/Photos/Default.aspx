<%@ Page language="c#" Codebehind="Default.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Photos.GalleryEditorHomePage" %>
<CP:CONTROLPANELSELECTEDNAVIGATION id="SelectedNavigation1" runat="server" SelectedNavItem="CommonTasks"></CP:CONTROLPANELSELECTEDNAVIGATION>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="GalleryControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_Photos_Manage_Title"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Galleries" />
	    <fieldset>
	        <legend><asp:label id="SectionName" runat="Server"></asp:label></legend>
		    <table cellpadding="0" cellspacing="0" border="0">
			    <tr>
			    <td class="CommonFormFieldName">
			    <CP:ResourceControl id="Resourcecontrol7" runat="server" ResourceName="CP_Photos_GridCol_Owners"></CP:ResourceControl>:
			    </td>
			    <td class="CommonFormField">
			    <asp:label id="Owners" runat="Server"></asp:label>
			    </td>
			    </tr>
    			
			    <tr>
			    <td class="CommonFormFieldName">			
			    <CP:ResourceControl id="Resourcecontrol9" runat="server" ResourceName="CP_Tools_JobsReport_Created"></CP:ResourceControl>
			    </td>
			    <td class="CommonFormField">
			    <asp:label id="DateCreated" runat="Server"></asp:label>
			    </td>
			    </tr>
		    </table>
	    </fieldset>
			
		<p />
		
		<fieldset>
		    <legend><CP:ResourceControl id="ResourceControl3" runat="server" ResourceName="CP_Photos_GridCol_Posts"></CP:ResourceControl></legend>
		    <table cellpadding="0" cellspacing="0" border="0">
    		
			    <tr>
			    <td class="CommonFormFieldName">
			    <CP:ResourceControl id="Resourcecontrol5" runat="server" ResourceName="CP_Photos_PostCount"></CP:ResourceControl>:
			    </td>
			    <td class="CommonFormField">
			    <asp:label id="PostCount" runat="Server"></asp:label>
			    <asp:label id="PostLimit" runat="Server"></asp:label>
			    </td>
			    </tr>
    			
			    <tr>
			    <td class="CommonFormFieldName">
			    <CP:ResourceControl id="Resourcecontrol13" runat="server" ResourceName="CP_Photos_DiskUsage"></CP:ResourceControl>:
			    </td>
			    <td class="CommonFormField">
			    <asp:label id="DiskUsage" runat="Server"></asp:label>
			    <asp:label id="DiskLimit" runat="Server"></asp:label>
			    </td>
			    </tr>
    			
			    <tr>
			    <td class="CommonFormFieldName">
			    <CP:ResourceControl id="Resourcecontrol6" runat="server" ResourceName="CP_Photos_MostRecentPost"></CP:ResourceControl>:
			    </td>
			    <td class="CommonFormField">
				    <asp:panel id="RecentPostInfo" runat="server">
					    <asp:label id="PostRecentTitle" runat="Server"></asp:label>&nbsp; 
					    <CP:ResourceControl id="Resourcecontrol10" runat="server" ResourceName="CP_By"></CP:ResourceControl>&nbsp; 
					    <asp:label id="PostRecentAuthor" runat="Server"></asp:label>&nbsp; 
					    <CP:ResourceControl id="Resourcecontrol11" runat="server" ResourceName="CP_On"></CP:ResourceControl>&nbsp; 
					    <asp:label id="PostRecentDate" runat="Server"></asp:label>
				    </asp:panel>
				    <CP:ResourceControl id="NoRecentPost" runat="server" ResourceName="CP_None" visible="false"></CP:ResourceControl>
			    </td>
			    </tr>
		    </table>
		</fieldset>

		<p />

        <fieldset>
            <legend><CP:ResourceControl id="Resourcecontrol4" runat="server" ResourceName="CP_Blog_Home_Feedback"></CP:ResourceControl></legend>
		    <table cellpadding="0" cellspacing="0" border="0">
			    <tr>
			    <td class="CommonFormFieldName">
			    <CP:ResourceControl id="Resourcecontrol2" runat="server" ResourceName="CP_Blog_Home_TotalComments"></CP:ResourceControl>
			    </td>
			    <td class="CommonFormField">
			    <asp:label id="CommentCount" runat="Server"></asp:label>
			    </td>
			    </tr>
    			
			    <tr>
			    <td class="CommonFormFieldName">
			    <CP:ResourceControl id="Resourcecontrol12" runat="server" ResourceName="CP_Blog_Home_TotalTrackbacks"></CP:ResourceControl>
			    </td>
			    <td class="CommonFormField">
			    <asp:label id="TrackbackCount" runat="Server"></asp:label>
			    </td>
			    </tr>
		    </table>
        </fieldset>
	</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="CommentEditor.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Photos.CommentEditor" %>

<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelModalMaster.ascx" IsModal="true">
	<CP:Content id="bcr" runat="server">
		<DIV class="CommonContentArea">
			<DIV class="CommonContent">
				<TWC:TabbedPanes id="EditorTabs" runat="server"
				PanesCssClass="CommonPane"
				TabSetCssClass="CommonPaneTabSet"
				TabCssClasses="CommonPaneTab,CommonPaneTab1,CommonPaneTab2"
				TabSelectedCssClasses="CommonPaneTabSelected,CommonPaneTabSelected1,CommonPaneTabSelected2"
				TabHoverCssClasses="CommonPaneTabHover,CommonPaneTabHover1,CommonPaneTabHover2"
				>
				<TWC:TabbedPane runat="server">
					<Tab><CP:ResourceControl runat="server" ResourceName="FeedbackEditor_Tab_Content" /></Tab>
					<Content>
						<div style="float:right;padding-right:3px;">
							<cp:GalleryImage id="Galleryimage1" ImageType="Thumbnail" runat="server" /></div>
						<DIV class="CommonFormFieldName">
							<asp:checkbox runat="Server" id="Approved" />
						</DIV>
						<DIV class="CommonFormFieldName" style="clear:right;">
							<CP:ResourceControl resourcename="FeedbackEditor_Title" runat="Server" id="FeedbackEditor_Title" />
						</DIV>
						<div class="CommonFormField">
							<asp:textbox runat="server" id="Subject" width="550" cssclass="ControlPanelTextInput" />
						</div>
						<DIV class="CommonFormFieldName">
							<CP:ResourceControl resourcename="FeedbackEditor_Comment" runat="Server" id="FeedbackEditor_Comment" />
						</DIV>
						<div class="CommonFormField">
							<CSControl:editor runat="Server" id="Comment" width="550" height="80" />
						</div>
					</Content>
				</TWC:TabbedPane>
				<TWC:TabbedPane runat="server">
					<Tab><CP:ResourceControl runat="server" ResourceName="FeedbackEditor_Tab_Metadata" /></Tab>
					<Content>
						<DIV class="CommonFormFieldName" align="right">
							<CP:ResourceControl resourcename="FeedbackEditor_ModeratedBy" runat="Server" id="FeedbackEditor_ModeratedBy" />
							<asp:textbox runat="server" id="ModeratedBy" enabled="false" readonly="true" />
						</DIV>
						<!-- 
						<DIV class="CommonFormFieldName">
							<asp:checkbox runat="Server" id="PersonalComment" /> 
						</div>
						-->
						<DIV class="CommonFormFieldName">
							<CP:ResourceControl resourcename="FeedbackEditor_Notes" runat="Server" id="FeedbackEditor_Notes" />
						</DIV>
						<div class="CommonFormField">
							<asp:textbox runat="server" id="Notes" textmode="MultiLine" width="550" rows="4" cssclass="ControlPanelTextInput" />
						</div>
						<DIV class="CommonFormFieldName">
							<CP:ResourceControl resourcename="FeedbackEditor_OriginalComment" runat="Server" id="FeedbackEditor_OriginalComment" />
						</DIV>
						<div class="CommonFormField">
							<asp:textbox runat="server" id="OriginalContent" readonly="true" textmode="MultiLine" width="550"
								rows="5" cssclass="ControlPanelTextInput" />
						</div>
					</Content>
				</TWC:TabbedPane>
				</TWC:TabbedPanes>
				<DIV class="CommonFormField PanelSaveButton">
					<asp:linkbutton id="Save" runat="server" CssClass="CommonTextButton"></asp:linkbutton></DIV>
			</DIV>
	</CP:Content>
</CP:Container></DIV>

<%@ Register TagPrefix="CP" TagName = "ReferalsList" Src = "~/ControlPanel/Photos/ReferralsListControl.ascx" %>
<%@ Page CodeBehind="Referrals.aspx.cs" Language="c#" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Photos.Referrals" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="GalleryControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server"><CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_Photos_Referrals_Title"></CP:ResourceControl></CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<CP:ResourceControl id="Resourcecontrol2" runat="server" ResourceName="CP_Photos_Referrals_SubTitle"></CP:ResourceControl></DIV>
		<CP:ReferalsList id="ReferalsList1" runat="Server"></CP:ReferalsList>
	</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="ModalQuickAdd.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Photos.ModalQuickAdd" %>
<%@ Register TagPrefix="CP" Namespace="CommunityServer.ControlPanel.Controls" Assembly="CommunityServer.Web" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelModalMaster.ascx" IsModal="true">
	<CP:Content id="bcr" runat="server">
		<DIV class="CommonContentArea">
			<DIV class="CommonContent">
				<CENTER>
					<P class="CommonFormInstructions">
						<cp:ResourceControl id="ResourceControl1" runat="server" ResourceName="ContentSelector_AddPhoto_Instructions" />
					<CP:QuickAddPhoto id="QuickAddPhoto1" runat="Server">
					<SkinTemplate>
					<CP:STATUSMESSAGE id="Status" runat="server"></CP:STATUSMESSAGE>
					<asp:RegularExpressionValidator id="AttachmentDataValidator" Text="CP_Photos_Attachment_UploadAttachment_InvlalidFileType" Runat="server" ControlToValidate="PictureData"></asp:RegularExpressionValidator>
                    <br />Quick Add:<input id="PictureData" type="file" name="PictureData" runat="server" /><span style="display:none"><asp:Button id="Submit" runat="server" /></span>
                    </SkinTemplate>
                </CP:QuickAddPhoto>
					<cp:ResourceButton id="Cancel" runat="server" ResourceName="Cancel" CausesValidation="false" />
				</CENTER>
			</DIV>
		</DIV>
	</CP:Content>
</CP:Container>

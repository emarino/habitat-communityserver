<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ReferralsListControl.ascx.cs" Inherits="CommunityServer.ControlPanel.Photos.ReferralsListControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<DIV id="Filters">
	<CP:ResourceControl id="ResourceControl3" runat="server" ResourceName="CP_Photos_Referrals_PostFilter"></CP:ResourceControl>:
	<asp:dropdownlist id="filterPost" runat="server" AutoPostBack="True"></asp:dropdownlist>
</DIV>
<DIV id="GrayGrid">
	<CA:grid id="Grid1" runat="Server">
		<levels>
			<CA:gridlevel datakeyfield="ReferralID">
				<columns>
					<CA:gridcolumn headingtext="URL" datafield="Url" />
					<CA:gridcolumn headingtext="Post Title" datafield="Title" />
					<CA:gridcolumn headingtext="Hits" width="40" datafield="Hits" />
					<CA:gridcolumn headingtext="Last Date" datafield="LastDate" formatstring="MMM dd yyyy, hh:mm tt"
						DataCellCssClass="LastDataCell" />
					<CA:gridcolumn visible="False" datafield="ReferralID" />
					<CA:gridcolumn datafield="ViewPostURL" visible="false" />
				</columns>
			</CA:gridlevel>
		</levels>
	</CA:grid>
</DIV>

<%@ Register TagPrefix="CP" TagName = "CommentList" Src = "~/ControlPanel/Photos/CommentListControl.ascx" %>
<%@ Page language="c#" Codebehind="CommentList.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Photos.CommentListPage" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="GalleryControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_Photos_Comments_Title"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<CP:ResourceControl id="Resourcecontrol2" runat="server" ResourceName="CP_Photos_Comments_SubTitle"></CP:ResourceControl></DIV>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Galleries" />
		<CP:CommentList id="Postlist1" runat="Server"></CP:CommentList>
	</CP:Content>
</CP:Container>

<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ManageCategoriesTreeControl.ascx.cs" Inherits="CommunityServer.ControlPanel.Photos.ManageCategoriesTreeControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<style>.hidden { DISPLAY: none }
</style>
<script type="text/javascript">

var nodeSelected = null;
var nodeLastSelected = null;
var currentCategoryID = <%= this.CategoryID %>;

function nodeSelect(node)
{
	nodeLastSelected = node;
	if (node.ID < 0)
	{
		nodeSelected = node;
	}
}
function nodeRename(sourceNode, newName)
{ 
	newName = newName.replace(/^\s*|\s*$/g,"");
	if(newName.length == 0)
		{
			alert('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Photos_ManageCategories_BlankCategoryName") %>');
			return false;
		}
		
	window.<%=this.TreeView1.ClientID%>.SelectedNode.Text = newName;
	window.<%=this.TreeView1.ClientID%>.SelectedNode.SaveState();
	

	// Determine whether this is a new node (doesn't exist server-side yet) or an existing one
	var categoryID = sourceNode.ID;

	if (categoryID > 0)
	{
		// Existing Node - just rename
		RenameCategory(categoryID, newName);
		return true; 
	}
	else
	{
		return false;
	}

}
function RenameCategory(categoryID, newName)
{
	ManageCategoriesTreeControl.RenameCategory('<%= this.ClientID %>',categoryID, newName, RenameCategoryCallBack);
}


function RenameCategoryCallBack(res)
{
	// Reload group edit control
	var categoryID = res.value;
	if (categoryID <= 0)
	{
		alert('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Photos_ManageCategories_DuplicateCategoryName") %>');
		//refresh the page
		window.location = window.location;
	}
}
       
function AddCategory(categoryID)
{
	//toggleButtons();
	if (categoryID == null)
	{
		if (window.<%=this.TreeView1.ClientID%>.SelectedNode == null)
			categoryID = 0;
		else
			categoryID = window.<%=this.TreeView1.ClientID%>.SelectedNode.ID;
	}
	ManageCategoriesTreeControl.AddCategory('<%= this.ClientID %>',categoryID, AddCategoryCallBack);
}

function AddCategoryCallBack(res)
{

	var retVals = res.value.split("^")
	var categoryID = retVals[0];
	var parentCategoryID = retVals[1];
	var categoryName = retVals[2];
	if (categoryID == '-1')
	{
		alert('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Photos_ManageCategories_DuplicateCategoryName") %>');
	}
	else
	{
		 AddCategoryNode(categoryID, parentCategoryID, categoryName)
	}
	return;
}

function nodeMoveCategoryTree(sourceNode, targetNode, dropIndex, targetTree)
{
	//ensure we can only move categories to the category tree
	if (sourceNode.ParentTreeView.TreeViewID == targetTree.TreeViewID)
		MoveCategoryTree(sourceNode.ID, targetNode.ID);
	return true;
}

function MoveCategoryTree(categoryID, parentCategoryID)
{
	ManageCategoriesTreeControl.UpdateParentCategory('<%= this.ClientID %>',categoryID, parentCategoryID, MoveCategoryTreeCallBack);
}


function MoveCategoryTreeCallBack(res)
{
	// Reload group edit control
	var retVal = res.value;
	if (retVal < 0)
	{
		//there was a problem - refresh the page
		window.location = window.location;
	}
}

function nodeCollapse(node)
{
	if(node.ID > 0)
		return true;
	else
		return false;
}
function AddCategoryNode(nodeID, parentCategoryID, newAlbumName)
{
	//all this code is well intended, but does not work, so just refresh the page
	//the new nodes do not respond properly to events (at this point it looks like custom client side events need
	//to be created for the drag() and drop()
	window.location = window.location;
	var newGroupNode = new ComponentArt_TreeViewNode();
	newGroupNode.Text = newAlbumName;
	newGroupNode.ImageUrl = "../Images/CATreeView/large_icons/folder.gif";
	newGroupNode.EditingEnabled = true;
	newGroupNode.DraggingEnabled = true;
	newGroupNode.DroppingEnabled = true;
	newGroupNode.AutoPostBackOnSelect = false;
	newGroupNode.AutoPostBackOnNodeMove= true;
	newGroupNode.Selectable = true;
	newGroupNode.ClientSideCommand = "window.location = 'CategoryManager.aspx?CategoryID=" + nodeID + "';";
	newGroupNode.ID = nodeID;
	var rootNode = window.<%=this.TreeView1.ClientID%>.FindNodeById(parentCategoryID);
	if (rootNode != null)
	{
		rootNode.AddNode(newGroupNode);
		newGroupNode.SaveState();
		window.<%=this.TreeView1.ClientID%>.Render();
		//newGroupNode.Edit();
	}
	//toggleButtons();
	
}
function DeleteSelectedCategory(categoryID)
{
	// If a GroupID was not passed, get it from the selected node
	if (categoryID == null)
	{
		if (window.<%=this.TreeView1.ClientID%>.SelectedNode == null)
			return;
		
		categoryID = window.<%=this.TreeView1.ClientID%>.SelectedNode.ID;
	}

	if(categoryID == '0')
	{
		alert('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Photos_ManageCategories_DeleteRootWarning") %>');
		return;
	}


	DeleteCategory(categoryID);
}
function DeleteCategory(categoryID)
{
	ManageCategoriesTreeControl.DeleteCategory('<%= this.ClientID %>', categoryID, DeleteCategoryCallBack);
}
function DeleteCategoryCallBack(res)
{
	if (res.value == null)
	{
		alert('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_DeleteGroupError") %>');
		return;
	}
	
	if (res.value == 0) //not an admin
	{
		alert('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_DeleteGroupError") %>');
		return;
	}
	
	var deletedNode = window.<%=this.TreeView1.ClientID%>.FindNodeById(res.value);
	if (deletedNode != null)
	{
		if(deletedNode.ID == currentCategoryID)
		{
			//var rootNode = window.<%=this.TreeView1.ClientID%>.FindNodeById(0);
			//nodeSelect(rootNode);
			window.location = '<%= CommunityServer.Components.Globals.GetSiteUrls().UrlData.FormatUrl("gallery_ControlPanel_Photos_CategoryManager") %>';
		}
		else
		{
			deletedNode.Remove();
			window.<%=this.TreeView1.ClientID%>.Render();
		}
	}
	
	return;
}
function toggleButtons()
{
	toggleVisibility('%= this.AddAlbumButton.ClientID %>');
	toggleVisibility('%= this.DeleteAlbumButton.ClientID  %>');
}
function toggleVisibility( targetId ){ 
  if (document.getElementById){ 
        target = document.getElementById( targetId ); 
           if (target.style.display == "none"){ 
              target.style.display = ""; 
           } else { 
              target.style.display = "none"; 
           } 
     } 
} 

function treeContextMenu(treeNode, e)
{
	var categoryID = treeNode.ID;
	if(treeNode.Checked == 1) //used for enabled
		window.<%=this.EnabledAlbumContextMenu.ClientID%>.ShowContextMenu(treeNode, e, categoryID);
	else
		window.<%=this.DisabledAlbumContextMenu.ClientID%>.ShowContextMenu(treeNode, e, categoryID);
	
	return true;
}
function EditCategory(categoryID)
{
	Telligent_Modal.Open('CategoryForm.aspx?sectionid=<%= this.CurrentGallery.SectionID %>&categoryID=' + categoryID, 400, 300, reloadCategories);
}
function reloadCategories()
{
	window.location = window.location;
}
function ChangeCategoryEnabled(categoryID)
{
	ManageCategoriesTreeControl.ChangeCategoryEnabled('<%= this.ClientID %>', categoryID, ChangeCategoryEnabledCallBack);
}
function ChangeCategoryEnabledCallBack(res)
{
	if(res.value == -1) //not an admin
		return;
		
	var changedNode = window.<%=this.TreeView1.ClientID%>.FindNodeById(res.value);
	if (changedNode != null)
	{
		if(changedNode.Checked == 1) //used for enabled
		{
			changedNode.ImageUrl = "folderdisabled.gif";
			changedNode.Checked = null;
		}
		else
		{
			changedNode.ImageUrl = "folder.gif";
			changedNode.Checked = 1;
		}
		changedNode.SaveState();
		window.<%=this.TreeView1.ClientID%>.Render();
	}
	
	return;
}
function ViewCategory(categoryID)
{
	var viewURL
	
	if(categoryID > 0)
	{
		viewURL = '<%= CommunityServer.Galleries.Components.GalleryUrls.Instance().ViewCategory(CurrentGallery.ApplicationKey, 99999999) %>';
		viewURL = viewURL.replace('99999999',  categoryID);
	}
	else
	{
		viewURL = '<%= CommunityServer.Galleries.Components.GalleryUrls.Instance().ViewGallery(CurrentGallery.ApplicationKey) %>';
	}
	window.location = viewURL;
}

//for splitter support
function resizeCategoryTree(DomElementId, NewPaneHeight, NewPaneWidth)
    {
      // Forces the treeview to adjust to the new size of its container 
		window.<%=this.TreeView1.ClientID%>.Render();
		if(DomElementId != null)
			resizeDetailsTree();
    }

</script>
<cp:contextmenu id="EnabledAlbumContextMenu" runat="server" onmenuclosescript="" onmenuopenscript="" filename="~/ControlPanel/Photos/ManageCategoriesTreeEnabledContextMenu.config">
    <SkinTemplate>
        <CA:Menu runat="server" 
	        id="Menu" 
	        AutoPostBackOnSelect="false" 
	        ContextMenu="Custom" 
	        Orientation="Vertical"
	        CssClass="CommonContextMenuGroup"
	        DefaultGroupCssClass="CommonContextMenuGroup"
	        DefaultItemLookID="DefaultItemLook" >
		        <ItemLooks>
			        <CA:ItemLook LookID="DefaultItemLook" 
				        CssClass="CommonContextMenuItem" 
				        HoverCssClass="CommonContextMenuItemHover" 
				        ExpandedCssClass="CommonContextMenuItemExpanded" 
				        LabelPaddingLeft="5" 
				        LabelPaddingRight="15" 
				        LabelPaddingTop="2" 
				        LabelPaddingBottom="2" />
		        </ItemLooks>	
        </CA:Menu>
    </SkinTemplate>
</cp:contextmenu>
<cp:contextmenu id="DisabledAlbumContextMenu" runat="server" onmenuclosescript="" onmenuopenscript="" filename="~/ControlPanel/Photos/ManageCategoriesTreeDisabledContextMenu.config">
    <SkinTemplate>
        <CA:Menu runat="server" 
	        id="Menu" 
	        AutoPostBackOnSelect="false" 
	        ContextMenu="Custom" 
	        Orientation="Vertical"
	        CssClass="CommonContextMenuGroup"
	        DefaultGroupCssClass="CommonContextMenuGroup"
	        DefaultItemLookID="DefaultItemLook" >
		        <ItemLooks>
			        <CA:ItemLook LookID="DefaultItemLook" 
				        CssClass="CommonContextMenuItem" 
				        HoverCssClass="CommonContextMenuItemHover" 
				        ExpandedCssClass="CommonContextMenuItemExpanded" 
				        LabelPaddingLeft="5" 
				        LabelPaddingRight="15" 
				        LabelPaddingTop="2" 
				        LabelPaddingBottom="2" />
		        </ItemLooks>	
        </CA:Menu>
    </SkinTemplate>
</cp:contextmenu>
<CA:TREEVIEW Width="100%" Height="100%" oncontextmenu="treeContextMenu" id="TreeView1" runat="server"
	EnableViewState="true" ImagesBaseUrl="~/ControlPanel/Images/CATreeView/large_icons" LineImagesFolderUrl="~/ControlPanel/Images/CATreeView/large_icons/lines" ShowLines="True" LeafNodeImageUrl="file.gif"
	ExpandedParentNodeImageUrl="folder_open.gif" ParentNodeImageUrl="folder.gif" NodeLabelPadding="3" AllowTextSelection="true" ItemSpacing="0" DefaultImageHeight="32" DefaultImageWidth="32" LineImageHeight="32"
	LineImageWidth="32" NodeEditCssClass="NodeEdit" HoverNodeCssClass="DetailsHoverTreeNode" SelectedNodeCssClass="DetailsSelectedTreeNode" NodeCssClass="DetailsTreeNode" CssClass="DetailsTreeView" ClientSideOnNodeRename="nodeRename"
	dropsiblingenabled="true" ClientSideOnNodeSelect="nodeSelect" ClientSideOnNodeCollapse="nodeCollapse" ClientSideOnNodeMove="nodeMoveCategoryTree" DragAndDropAcrossTreesEnabled="true" KeyboardEnabled="true"
	NodeEditingEnabled="true" DragAndDropEnabled="true" AutoPostBackOnNodeRename="false" AutoPostBackOnNodeMove="false" AutoPostBackOnSelect="true" BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="0"
	FillContainer="True"></CA:TREEVIEW>
<br>
<div class="CommonFormField"><nobr><cp:hyperlink id="AddAlbumButton" run at="server" resourcename="CP_Photos_ManageCategories_AddNewCategory"
			cssclass="CommonTextButton" navigateurl="javascript:AddCategory();"></cp:hyperlink></nobr><br>
</div>
<div class="CommonFormField"><nobr><cp:hyperlink id="DeleteAlbumButton" run at="server" resourcename="CP_Photos_ManageCategories_DeleteCategory"
			cssclass="CommonTextButton" navigateurl="javascript:DeleteSelectedCategory();"></cp:hyperlink></nobr></div>

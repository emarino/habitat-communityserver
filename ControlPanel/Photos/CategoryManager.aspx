<%@ Page language="c#" Codebehind="CategoryManager.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Photos.CategoryManager" %>
<%@ Register TagPrefix="CP" TagName = "ManageCategoriesControl" Src = "~/ControlPanel/Photos/ManageCategoriesControl.ascx" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="GalleryControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_Photos_ManageCategories_Title"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
			<DIV class="CommonDescription">
			<cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_Photos_ManageCategories_Help"></cp:helpicon>		
			<CP:ResourceControl id="Resourcecontrol2" runat="server" ResourceName="CP_Photos_ManageCategories_SubTitle"></CP:ResourceControl>
			</DIV>
		<CP:ManageCategoriesControl id="ManageCategoriesControl1" runat="server"></CP:ManageCategoriesControl>
	</CP:Content>
</CP:Container>

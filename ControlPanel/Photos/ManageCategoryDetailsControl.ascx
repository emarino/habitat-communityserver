<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ManageCategoryDetailsControl.ascx.cs" Inherits="CommunityServer.ControlPanel.Photos.ManageCategoryDetailsControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script type="text/javascript">
	//depends on currentCategoryID var from TreeControl
	
function nodeMoveImageTree(sourceNode, targetNode, dropIndex, targetTree)
{
	//sourceNode.Text
	//sourceNode.ParentTreeView.TreeViewID
	//targetNode.Text
	//targetTree.TreeViewID 
	if(targetTree.TreeViewID != sourceNode.ParentTreeView.TreeViewID)
	{
		if(currentCategoryID != targetNode.ID)
			MovePhoto(sourceNode.ID, targetNode.ID, currentCategoryID);
	}
	
	return false;
}
function MovePhoto(PostID, newCategoryID, oldCategoryID)
{
	ManageCategoryDetailsControl.MovePhoto('<%= this.ClientID %>',PostID,newCategoryID,oldCategoryID, MovePhotoCallBack);
}
function MovePhotoCallBack(res)
{
	// Reload group edit control
	var postID = res.value;
	if (postID > 0)
	{
		var movedNode = window.<%=this.TreeView1.ClientID%>.FindNodeById(postID);
			movedNode.Remove();
			window.<%=this.TreeView1.ClientID%>.Render();
	}
	else
	{
			alert('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Photos_ManageCategories_DuplicatePhoto") %>');
	}
}
//for splitter support
function resizeDetailsTree(DomElementId, NewPaneHeight, NewPaneWidth)
    {
		if(DomElementId != null)
			resizeCategoryTree();
      // Forces the treeview to adjust to the new size of its container 
      if(window.<%=this.TreeView1.ClientID%> != null)
			window.<%=this.TreeView1.ClientID%>.Render();
    }

</script>
<CA:TREEVIEW id="TreeView1" Height="100%" Width="100%" BorderColor="#CCCCCC" BorderWidth="0"
	BorderStyle="Solid" runat="server" EnableViewState="true" DragAndDropAcrossTreesEnabled="true"
	ClientSideOnNodeMove="nodeMoveImageTree" ShowLines="false" NodeEditCssClass="DetailsNodeEdit"
	HoverNodeCssClass="DetailsHoverTreeNode" SelectedNodeCssClass="DetailsSelectedTreeNode" NodeCssClass="DetailsTreeNode"
	CssClass="DetailsTreeView" KeyboardEnabled="true" NodeEditingEnabled="false" DragAndDropEnabled="true"
	AutoPostBackOnNodeMove="false" AutoPostBackOnSelect="false" FillContainer="True" ExtendNodeCells="True">
	<Templates>
		<ca:NavigationCustomTemplate ID="ItemTemplate">
			<Template>
				<table class="DetailsTreeItem" title='<%# DataBinder.Eval(Container.DataItem, "Text") %>' cellpadding="5" cellspacing="0" border="0">
					<tr>
						<td class="DetailsTreeItemCell">
							<a href='posteditor.aspx?sectionid=<%# Container.Attributes["SectionID"] %>&PostID=<%# Container.Attributes["PostID"] %>' >
								<img src='<%# Container.Attributes["ThumbnailURL"] %>' width="50" style="border-color: #000000; border-width: 1px;" /></a>
						</td>
						<td class="DetailsTreeItemCell"><div style="width: 200px; overflow: hidden;"><%# DataBinder.Eval(Container.DataItem, "Text") %></div></td>
						<td>
							<img src='../images/<%# Container.Attributes["Published"] %>.gif' width="16" height="15" />
						</td>
						<td class="DetailsTreeItemCell">
							<a href='posteditor.aspx?sectionid=<%# Container.Attributes["SectionID"] %>&PostID=<%# Container.Attributes["PostID"] %>' class="CommonTextButton">Edit</a>
							<a href='<%# Container.Attributes["ViewPictureURL"] %>' class="CommonTextButton">View</a>
						</td>
					</tr>
				</table>
			</Template>
		</ca:NavigationCustomTemplate>
	</Templates>
</CA:TREEVIEW><asp:panel id="NoPhotosMessage" Runat="server" Visible="False">
	<DIV style="PADDING-RIGHT: 10px; PADDING-LEFT: 10px;  PADDING-TOP: 10px; HEIGHT: 400px; BACKGROUND-COLOR: lightgrey; TEXT-ALIGN: center"><%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Photos_ManageCategories_EmptyAlbum") %></DIV>
</asp:panel>

<%@ Page language="c#" Codebehind="Attachment.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Galleries.AttachmentPage" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelModalMaster.ascx" IsModal="true">
	<CP:Content id="bcr" runat="server">
		<script type="text/javascript">
		// <![CDATA[
		function closeModal(resultArray)
		{
			window.parent.Telligent_Modal.Close(resultArray);
		}
		
		function toggleForm(isServer)
		{
		    Element.show('uploadform');
		    Element.hide('starter');
		    
		    var ut = $('<%= uploadType.ClientID %>');
		    ut.value = isServer;
		    if(isServer)
		        Element.show('Server');
		    else
		        Element.show('Remote');

		}
		// ]]>
		</script>
		<DIV class="CommonContentArea">
			<DIV class="CommonContent">
				<DIV id="uploadtypeQuestion" style="TEXT-ALIGN: center" runat="server" visible="false">
					<H4 class="CommonSubTitle"><CP:ResourceControl id="Resourcecontrol2" runat="server" ResourceName="CP_Photos_Attachment_SubTitle"></CP:ResourceControl></H4>
					<A href="?Upload=true"><CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_Photos_Attachment_UploadToServer"></CP:ResourceControl></A> | <A href="?Remote=true"><CP:ResourceControl id="Resourcecontrol3" runat="server" ResourceName="CP_Photos_Attachment_UploadRemote"></CP:ResourceControl></A>
				</DIV>
				<DIV id="FriendlyNameTag" runat="server" visible="false">
					<DIV class="CommonFormFieldName">
						<CP:FormLabel id="Formlabel2" runat="server" ResourceName="CP_Photos_Attachment_FriendlyName" ControlToLabel="FriendlyName"></CP:FormLabel></DIV>
					<DIV class="CommonFormField">
						<asp:TextBox id="FriendlyName" Runat="Server" CssClass="ControlPanelTextInput"></asp:TextBox></DIV>
				</DIV>
				<DIV id="UploadTag" runat="server" visible="false">
					<DIV class="CommonFormFieldName">
						<CP:FormLabel id="FormLabel1" runat="server" ResourceName="CP_Photos_Attachment_UploadAttachment" ControlToLabel="AttachmentData"></CP:FormLabel>
						<asp:RegularExpressionValidator id="AttachmentDataValidator" Text="CP_Photos_Attachment_UploadAttachment_InvlalidFileType" Runat="server" ControlToValidate="AttachmentData"></asp:RegularExpressionValidator></DIV>
					<DIV class="CommonFormField"><INPUT class="ControlPanelTextInput" id="AttachmentData" type="file" name="AttachmentData"
							runat="server">
					</DIV>
				</DIV>
				<DIV id="RemoteTag" runat="server" visible="false">
					<DIV class="CommonFormFieldName">
						<CP:FormLabel id="Formlabel3" runat="server" ResourceName="CP_Photos_Attachment_Link" ControlToLabel="FileName"></CP:FormLabel>
						<asp:RegularExpressionValidator id="FileNameValidator" Text="CP_Photos_Attachment_UploadAttachment_InvlalidFileType" Runat="server" ControlToValidate="FileName"></asp:RegularExpressionValidator></DIV>
					<DIV class="CommonFormField">
						<asp:TextBox id="FileName" Runat="Server" CssClass="ControlPanelTextInput"></asp:TextBox></DIV>
				</DIV>
				<DIV id="ButtonTag" runat="server" visible="false">
					<DIV class="CommonFormField PanelSaveButton">
						<asp:LinkButton id="DescButton" runat="Server" Text="CP_Photos_Attachment_Upload" CssClass="CommonTextButton"></asp:LinkButton><INPUT id="uploadType" type="hidden" runat="server">
					</DIV>
				</DIV>
	</CP:Content>
</CP:Container></DIV></DIV>

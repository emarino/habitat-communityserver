<%@ Import Namespace="CommunityServer.Components" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="CategoryListControl.ascx.cs" Inherits="CommunityServer.ControlPanel.Photos.CategoryListControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script language = "javascript" type="text/javascript">

 
  function reloadCategories()
  {
	window.location = window.location;
  }

  function onCallbackError(excString)
  {
    if (confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_CallBackWarning") %>')) alert(excString); 
    <%= Grid1.ClientID %>.Page(1); 
  }

  function onDelete(item)
  {
	  if(item.GetMember("TotalThreads").Value > 0)
		{
			if (!confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Photos_Categories_Delete_NotEmptyWarning") %>'))
				{ return false;}
		}

		return confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Photos_Categories_Delete_Warning") %>'); 
  }
  function deleteRow(rowId)
  {
    <%= Grid1.ClientID %>.Delete(<%= Grid1.ClientID %>.GetRowFromClientId(rowId)); 
  }
  
  var editName = '<cp:resourcecontrol runat="server" resourcename="Edit" />';
  var deleteName = '<cp:resourcecontrol runat="server" resourcename="Delete" />';

</script>
	<table cellpadding="0" cellspacing="0" border="0"  width="100%">
		<tr>
			<td width="100%">
				<asp:PlaceHolder runat="server" id="ParentAlbumContainer">
					<CP:ResourceControl resourcename="CP_Photos_ParentCategory" runat="Server" id="FeedbackFilterLabel"/>: 
					<cp:ParentCategoryDropDown id="CategoryList" Runat="server" AutoPostBack="True"/>
				</asp:PlaceHolder>
			</td>
			<td nowrap="nowrap">
				<cp:ModalLink ModalType="Link" Height="300" Width="400" Text="CP_Photos_Categories_AddNewCategory" runat="Server" Callback="reloadCategories" id="NewCategory" CssClass="CommonTextButton"/>
			</td>
		</tr>
	</table>
<br />
<div id="GrayGrid" >
<CA:Grid id="Grid1" runat="Server" 
	AutoCallBackOnDelete="true" 
	ClientSideOnDelete="onDelete" 
	ClientSideOnCallbackError="onCallbackError" 
	RunningMode="Client" 
	>
    <Levels>
        <CA:GridLevel DataKeyField="CategoryID">
            <Columns>
  				<CA:GridColumn DataField="ThumbnailURL" HeadingText=" " AllowGrouping="false" AllowSorting="false"
					FixedWidth="True" DataCellClientTemplateId="ThumbnailTemplate" width="55" />
				 <CA:GridColumn DataField="Name" HeadingText="ResourceManager.CP_Photos_GridCol_Name" width="175"  />
                <CA:GridColumn DataField="Description" HeadingText="ResourceManager.CP_Photos_GridCol_Description" />
                <CA:GridColumn DataField="MostRecentPostDate" DataCellClientTemplateId="RecentPostDateTemplate" HeadingText="ResourceManager.CP_Photos_GridCol_LastPost" Width="150" FixedWidth="True" FormatString="MMM dd yyyy, hh:mm tt" />
                <CA:GridColumn DataField="TotalThreads" HeadingText="ResourceManager.CP_Photos_GridCol_Posts" Align="Center" Width="65" FixedWidth="True" />
                <CA:GridColumn DataField="IsEnabled" HeadingText="ResourceManager.CP_Photos_GridCol_Published" />
                <CA:GridColumn DataField="CategoryID" Visible="False" />
                <CA:GridColumn DataField="ParentID" Visible="False" />
                <CA:GridColumn DataField="SectionID" Visible="False" />
                <CA:GridColumn DataField="FeaturedPostID" Visible="False" />
                <CA:GridColumn DataField="Path" Visible="False" />
                <CA:GridColumn HeadingText="ResourceManager.CP_Photos_GridCol_Actions" AllowGrouping="false" AllowSorting="false" DataField="GalleryPostType" DataCellClientTemplateId = "ActionTemplate" Align="Center" Width="110" FixedWidth="True" DataCellCssClass="LastDataCell" />
            </Columns>
        </CA:GridLevel>
    </Levels>
    <ClientTemplates>
		<CA:ClientTemplate Id="RecentPostDateTemplate">
            ## 
                if(DataItem.GetMember('MostRecentPostDate').Text == 'Jan 01 1753, 12:00 AM') 
                {
                    ''
                }
                else
                {
                    DataItem.GetMember('MostRecentPostDate').Text
                }
            ## 
		</CA:ClientTemplate>
		<CA:ClientTemplate Id="ThumbnailTemplate">
			<a href="javascript:Telligent_Modal.Open('CategoryForm.aspx?sectionid=##  DataItem.GetMember("SectionID").Text ##&categoryID=## DataItem.GetMember("CategoryID").Text ##', 400, 300, reloadCategories);"><img src='## DataItem.GetMember("ThumbnailURL").Text ##' width="50" border="0" /></a>
		</CA:ClientTemplate>
        <CA:ClientTemplate Id="ActionTemplate">
			<a href="javascript:Telligent_Modal.Open('CategoryForm.aspx?sectionid=##  DataItem.GetMember("SectionID").Text ##&categoryID=## DataItem.GetMember("CategoryID").Text ##', 400, 300, reloadCategories);" class="CommonTextButton">##editName##</a> 
           <a href="javascript:deleteRow('## DataItem.ClientId ##')" class="CommonTextButton">##deleteName##</a>
        </CA:ClientTemplate>
    </ClientTemplates>
</CA:Grid></div>

<%@ Register TagPrefix="CP" TagName = "Editor" Src = "~/ControlPanel/Photos/PostEditControl.ascx" %>
<%@ Page language="c#" Codebehind="PostEditor.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Photos.PostEditor" %>
<CP:CONTROLPANELSELECTEDNAVIGATION id="SelectedNavigation1" runat="server" SelectedNavItem="CommonTasks"></CP:CONTROLPANELSELECTEDNAVIGATION>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="GalleryControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<asp:literal id="RegionTitle" runat="server"></asp:literal>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<CP:ResourceControl id="RegionDescription" runat="Server" resourcename="CP_Photos_PostEdit_Description"></CP:ResourceControl>
		</DIV>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Galleries" />
		<CP:Editor id="PostEditControl1" runat="Server" />
        <p class="PanelSaveButton">
			<asp:Linkbutton id="SaveButton" CssClass="CommonTextButtonBig" Runat="server" Text="Post" />
	    </p>
	</CP:Content>
</CP:Container>

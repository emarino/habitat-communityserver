<%@ Page language="c#" Codebehind="GalleryCommand.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Photos.GalleryCommandPage" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<asp:Literal id="CommandTitle" runat="Server" Text="Blog Command" /></title>
		<CSControl:Style id="ControlPanelStyle" runat="server" Href="~/ControlPanel/style/style.css" />
	</HEAD>
	<body class="modal" runat="server" id="body" style="PADDING-RIGHT:0px;PADDING-LEFT:0px;PADDING-BOTTOM:0px;MARGIN:2px;PADDING-TOP:0px">
		<form id="Form1" method="post" runat="server">
			<p>
				<asp:Literal Runat="server" id="CommandMessage" Text="Message" />
			</p>
			<p align="center">
				<asp:Button Runat="server" Text="OK" id="OK_Button" />
				<input type="button" onclick="window.parent.Telligent_Modal.Close(false); return false;" value="Cancel"
					runat="server" id="Cancel_Button">
			</p>
		</form>
	</body>
</HTML>

<%@ Control Language="c#" AutoEventWireup="false" Codebehind="SwitchControl.ascx.cs" Inherits="CommunityServer.ControlPanel.Photos.SwitchControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<DIV id="GrayGrid">
	<CA:GRID id="Grid1" runat="Server">
		<LEVELS>
			<CA:GRIDLEVEL datakeyfield="SectionID">
				<COLUMNS>
					<CA:GRIDCOLUMN datafield="ApplicationKey" HeadingText="ResourceManager.CP_Photos_GridCol_AppKey" />
					<CA:GRIDCOLUMN datafield="Name" HeadingText="ResourceManager.CP_Photos_GridCol_Name" />
					<CA:GRIDCOLUMN datafield="Description" HeadingText="ResourceManager.CP_Photos_GridCol_Description"
						visible="false" />
					<CA:GRIDCOLUMN datafield="Owners" HeadingText="ResourceManager.CP_Photos_GridCol_Owners" width="150" />
					<CA:GRIDCOLUMN datafield="GroupName" HeadingText="ResourceManager.CP_Photos_GridCol_Group" width="150" />
					<CA:GRIDCOLUMN datafield="IsActive" HeadingText="ResourceManager.CP_Photos_GridCol_Enabled" DataCellCssClass="LastDataCell" />
					<CA:GRIDCOLUMN datafield="SectionID" visible="false" />
				</COLUMNS>
			</CA:GRIDLEVEL>
		</LEVELS>
	</CA:GRID></DIV>

<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ManageCategoriesControl.ascx.cs" Inherits="CommunityServer.ControlPanel.Photos.ManageCategoriesControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Register TagPrefix="Tree" TagName = "ManageCategoriesTreeControl" Src = "~/ControlPanel/Photos/ManageCategoriesTreeControl.ascx" %>
<%@ Register TagPrefix="CP" TagName = "ManageCategoryDetailsControl" Src = "~/ControlPanel/Photos/ManageCategoryDetailsControl.ascx" %>
<!--<asp:Label Runat="server" id="debugComment"></asp:Label>
<table cellpadding="10">
	<tr valign="top">
		<td width="250" rowspan="2">
			<Tree:ManageCategoriesTreeControl run at="Server" id="ManageCategoriesTreeControl1" />
		</td>
		<td width="100%">
			<CP:ManageCategoryDetailsControl run at="Server" id="ManageCategoryDetailsControl1" />
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>-->
<script type="text/javascript">
// <![CDATA[
    // Disable grid hover style to avoid the flicker while resizing 
    function startResize(PaneDomObj)
    {
      return true; 
    }    

    // Restore the grid hover style 
    function endResize(PaneDomObj)
    {
      return true; 
    }    
    
    // ]]>
</script>
<div class="CommonManagerArea">
<div class="CommonManagerContent">
<CA:Splitter runat="server" ID="Splitter1" FillHeight="true" FillWidth="true" WidthAdjustment="300"
	 ClientSideOnResizeStart="startResize" ClientSideOnResizeEnd="endResize">
	<Layouts>
		<CA:SplitterLayout>
			<Panes SplitterBarCollapseImageUrl="../Images/splitter_col.gif" SplitterBarExpandImageUrl="../Images/splitter_exp.gif"
						SplitterBarExpandImageWidth="6"
						SplitterBarCollapseImageWidth="6" SplitterBarCollapseImageHeight="116" SplitterBarCssClass="SplitterBar"
						SplitterBarCollapsedCssClass="SplitterBarCollapsed" SplitterBarActiveCssClass="ActiveSplitterBar"
						SplitterBarWidth="5" Orientation="Horizontal">
				<CA:SplitterPane PaneContentId="TreeViewContent" Width="220" MinWidth="100" ClientSideOnResize="resizeCategoryTree"	CssClass="SplitterPane"></CA:SplitterPane>
				<CA:SplitterPane PaneContentId="DetailsContent" CssClass="SplitterPane" ClientSideOnResize="resizeDetailsTree"></CA:SplitterPane>
			</Panes>
		</CA:SplitterLayout>
	</Layouts>
	<Content>
		<CA:SplitterPaneContent id="TreeViewContent">
			<div class="SplitterListHeading" id="FolderListingHeader" style="height:19px;"><CP:ResourceControl ResourceName="CP_Photos_AllCategories" runat="server" ID="Resourcecontrol1" NAME="Resourcecontrol1"/></div>
				<Tree:ManageCategoriesTreeControl runat="Server" id="ManageCategoriesTreeControl1" />
		</CA:SplitterPaneContent>
		<CA:SplitterPaneContent id="DetailsContent">
				<div class="SplitterListHeading" id="DetailsListingHeader" style="height:19px;"><CP:ResourceControl ResourceName="CP_Photos_AllPosts" runat="server" ID="Resourcecontrol2" NAME="Resourcecontrol1"/></div>
				<CP:ManageCategoryDetailsControl runat="Server" id="ManageCategoryDetailsControl1" />
		</CA:SplitterPaneContent>
	</Content>
</CA:Splitter>
</div>


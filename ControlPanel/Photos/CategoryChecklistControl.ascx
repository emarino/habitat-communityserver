<%@ Control Language="c#" AutoEventWireup="false" Codebehind="CategoryChecklistControl.ascx.cs" Inherits="CommunityServer.ControlPanel.Photos.CategoryChecklistControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<CA:TreeView id="TreeView1" Width="100%" AutoPostBackOnSelect="false" AutoPostBackOnNodeMove="false"
	AutoPostBackOnNodeRename="false" DragAndDropEnabled="false" NodeEditingEnabled="false" KeyboardEnabled="true"
	CssClass="TreeView" NodeCssClass="TreeNode" SelectedNodeCssClass="SelectedTreeNode" HoverNodeCssClass="HoverTreeNode"
	NodeEditCssClass="NodeEdit" LineImageWidth="19" LineImageHeight="20" DefaultImageWidth="16"
	DefaultImageHeight="16" ItemSpacing="0" NodeLabelPadding="3" ParentNodeImageUrl="folders.gif"
	LeafNodeImageUrl="folder.gif" ExpandedParentNodeImageUrl="folder.gif" ShowLines="True" LineImagesFolderUrl="~/ControlPanel/Images/CATreeView/small_icons/lines"
	ImagesBaseUrl="~/ControlPanel/Images/CATreeView/small_icons" EnableViewState="true" runat="server"></CA:TreeView>

<%@ Page language="c#" Codebehind="CategoryForm.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Photos.CategoryForm" %>
<%@ Import Namespace="CommunityServer.Components" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelModalMaster.ascx" IsModal="true">
	<CP:Content id="bcr" runat="server">
		<script type="text/javascript">
		// <![CDATA[
				function SelectPreview()
				{
						$('<%= PreviewHightlight.ClientID %>').innerHTML = '';
						Element.show('loading');
						CategoryForm.GetHightlightPreview(selectHighlightID(), previewHighlightX);
				}
			       
				function previewHighlightX(res)
				{
						Element.hide('loading');
						$('<%= PreviewHightlight.ClientID %>').innerHTML = res.value;
				}
			       
					function selectHighlightID()
					{
						var element = $('<%=  HighlightID.ClientID %>');
						var index = element.selectedIndex;
						var text = element.options[index].value;
						return text
					}
		// ]]>
		</script>
		<DIV class="CommonContentArea">
			<DIV class="CommonContent">
				<TABLE cellSpacing="0" cellPadding="0">
					<TR>
						<TD width="100%">
							<DIV class="CommonFormFieldName">
								<cp:helpicon id="HelpIcon1" runat="Server" ResourceName="CP_Photos_CategoryEdit_Name_Detail"></cp:helpicon>
								<CP:FormLabel id="Formlabel3" runat="server" ResourceName="CP_Photos_CategoryEdit_Name" ControlToLabel="CategoryName"></CP:FormLabel>
								<asp:RequiredFieldValidator id="RequiredFieldValidator1" Runat="server" ControlToValidate="CategoryName" Display="Dynamic"
									Text="<br />Please include a name"></asp:RequiredFieldValidator></DIV>
							<DIV class="CommonFormField">
								<asp:TextBox id="CategoryName" Runat="server" MaxLength="256" CssClass="ControlPanelTextInput"></asp:TextBox></DIV>
							<DIV class="CommonFormFieldName">
								<cp:helpicon id="Helpicon4" runat="Server" ResourceName="CP_Photos_CategoryEdit_IsActive_Detail"></cp:helpicon>
								<CP:FormLabel id="Formlabel2" runat="server" ResourceName="CP_Photos_CategoryEdit_IsActive" ControlToLabel="ParentID"></CP:FormLabel></DIV>
							<DIV class="CommonFormField">
								<asp:CheckBox id="CategoryEnabled" Runat="server"></asp:CheckBox></DIV>
						</TD>
						<TD>
							<asp:PlaceHolder runat="server" id="PreviewContainer">
							<DIV class="CommonFormFieldName"><SPAN id="PreviewHightlight" runat="server"></SPAN><SPAN id="loading" style="DISPLAY: none">
									<CENTER>
										<CP:ResourceControl id="ResourceControl1" runat="Server" resourcename="CP_Loading"></CP:ResourceControl>&nbsp;<IMG 
      src="<%=CommunityServer.Components.SiteUrls.Instance().LoadingImageUrl%>"></CENTER>
								</SPAN></DIV>
							</asp:PlaceHolder>
						</TD>
					</TR>
					<asp:PlaceHolder runat="server" id="ParentHighlightContainer">
					<TR>
						<TD noWrap>
							<DIV class="CommonFormFieldName">
								<cp:helpicon id="Helpicon3" runat="Server" ResourceName="CP_Photos_CategoryEdit_Parent_Detail"></cp:helpicon>
								<CP:FormLabel id="Formlabel4" runat="server" ResourceName="CP_Photos_CategoryEdit_Parent" ControlToLabel="ParentID"></CP:FormLabel></DIV>
							<DIV class="CommonFormField">
								<cp:ParentCategoryDropDown id="CategoryParentID" runat="server" />
								<asp:RequiredFieldValidator id="ParentIDValidator" runat="server" ControlToValidate="CategoryParentID" CssClass="ValidationMessage"
									ErrorMessage="*"></asp:RequiredFieldValidator></DIV>
						</TD>
						<TD noWrap>
							<DIV class="CommonFormFieldName">
								<cp:helpicon id="Helpicon5" runat="Server" ResourceName="CP_Photos_CategoryEdit_Highlight_Detail"></cp:helpicon>
								<CP:FormLabel id="Formlabel5" runat="server" ResourceName="CP_Photos_CategoryEdit_Highlight" ControlToLabel="HighlightID"></CP:FormLabel></DIV>
							<DIV class="CommonFormField">
								<ASP:DropdownList id="HighlightID" runat="server" onchange="SelectPreview();"></ASP:DropdownList></DIV>
						</TD>
					</TR>
					</asp:PlaceHolder>
				</TABLE>
				<DIV class="CommonFormFieldName">
					<cp:helpicon id="Helpicon2" runat="Server" ResourceName="CP_Photos_CategoryEdit_Desc_Detail"></cp:helpicon>
					<CP:FormLabel id="Formlabel1" runat="server" ResourceName="CP_Photos_CategoryEdit_Desc" ControlToLabel="CategoryDesc"></CP:FormLabel></DIV>
				<DIV class="CommonFormField">
					<asp:TextBox id="CategoryDesc" Runat="server" MaxLength="256" CssClass="ControlPanelTextInput"
						TextMode="MultiLine"></asp:TextBox></DIV>
				<DIV class="PanelSaveButton">
						<CP:ResourceLinkButton id="SaveButton" ResourceName="Save" Runat="server"  CssClass="CommonTextButton"></CP:ResourceLinkButton></div></p>
			</DIV>
		</DIV>
	</CP:Content>
</CP:Container>

<%@ Register TagPrefix="CP" TagName = "SectionList" Src = "~/ControlPanel/Photos/SwitchControl.ascx" %>
<%@ Page language="c#" Codebehind="Switch.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Photos.GallerySwitchPage" %>
<CP:ControlPanelSelectedNavigation id="SelectedNavigation1" runat="server" SelectedTab="Photos" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelMaster.ascx">
	<CP:Content id="PanelDescription" runat="server">
		<CP:ResourceControl id="PanelDescriptionResource" runat="Server" resourcename="CP_Photos_PanelTitleDefault"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="SectionDescription" runat="server" ResourceName="CP_Photos_MyGalleries"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="TopLeft" runat="server">
		<TABLE cellSpacing="0" cellPadding="0" border="0" width="100%">
			<TR>
				<TD width="100%">
					<CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_Photos_CurrentGallery"></CP:ResourceControl>&nbsp;</TD>
				<TD style="padding-right: 3px;" nowrap="nowrap">
					<CP:HyperLink id="HyperLink1" ResourceName="CP_Photos_ChangeGallery" Runat="server" CssClass="CommonTextButton"
						UrlName="gallery_ControlPanel_Photos_Switch"></CP:HyperLink></TD>
			</TR>
		</TABLE>
	</CP:Content>
	<CP:Content id="PanelNavigation" runat="server">
		<CP:ControlPanelNavigationSidebar id="Controlpanelnavigationsidebar1" runat="server"></CP:ControlPanelNavigationSidebar>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<CP:ResourceControl id="Section_Sub_Description" runat="server" ResourceName="MyGalleries_SubTitle"></CP:ResourceControl></DIV>
		<CP:StatusMessage id="Status" runat="server" Visible="false"></CP:StatusMessage>
		<CP:SectionList id="SectionList1" runat="Server"></CP:SectionList>
	</CP:Content>
</CP:Container>

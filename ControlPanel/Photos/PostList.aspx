<%@ Register TagPrefix="CP" TagName = "PictureList" Src = "~/ControlPanel/Photos/PostListControl.ascx" %>
<%@ Page language="c#" Codebehind="PostList.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Galleries.PostList" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="GalleryControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_Photos_PostList_Title"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<CP:ResourceControl id="Resourcecontrol2" runat="server" ResourceName="CP_Photos_PostList_SubTitle"></CP:ResourceControl></DIV>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Galleries" />
		<CP:PictureList id="Postlist1" runat="Server"></CP:PictureList>
	</CP:Content>
</CP:Container>

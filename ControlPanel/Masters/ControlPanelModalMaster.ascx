﻿<%@ Control Language="C#" %>
<%@ Import Namespace="CommunityServer.Components" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional-dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <cp:Region id="HeaderRegion" runat="server">
        <CSControl:Head runat="Server" ID="Head1">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <CSControl:Script runat="server" ID="Script1"/>
 			<CSControl:Style id="UserStyles" runat="server" />
			<CSControl:Style runat="server" Href="../style/ControlPanel.css" ID="Style3" />
			<CSControl:Style runat="server" Href="../style/ControlPanelModal.css" />
			<CP:Region id="AdditionalScripts" runat="server" ></CP:Region>
        </CSControl:Head>
    </cp:Region>
	<body>
		<cp:Form runat="server">
			<TWC:Modal runat="server" CssClasses="CommonModal" TitleCssClasses="CommonModalTitle" CloseCssClasses="CommonModalClose" ContentCssClasses="CommonModalContent" FooterCssClasses="CommonModalFooter" ResizeCssClasses="CommonModalResize" MaskCssClasses="CommonModalMask" LoadingUrl="~/utility/loading.htm" />			
			<cp:Region id="bscr" runat="server" />
				<div id="CommonOuter"><div id="Common">
					<div id="CommonHeader">
						<cp:Region id="bhcr" runat="server" ></cp:Region>
					</div>

					<div id="CommonBody">
						<table cellspacing="0" cellpadding="0" border="0" width="100%" id="CommonBodyTable">
							<tr>
								<td valign="top" id="CommonLeftColumn">
									<cp:Region id="lcr" runat="server" />
								</td>
								
								<td valign="top" width="100%" id="CommonBodyColumn"><table cellpadding="0" cellspacing="0" border="0" style="table-layout: fixed;" width="100%"><tr><td>
									<cp:Region id="bcr" runat="server" />
								</td></tr></table></td>
								
								<td valign="top" id="CommonRightColumn">
									<cp:Region id="rcr" runat="server" />
								</td>
							</tr>
						</table>
					</div>
				
					<div id="CommonFooter">
						<cp:Region id="BodyFooterRegion" runat="server" ></cp:Region>
					</div>
				</div></div>
		</cp:Form>
		<cp:Region id="outsidetheform" runat="server" />
	</body>
</html>
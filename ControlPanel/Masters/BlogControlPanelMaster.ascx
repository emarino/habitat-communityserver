<%@ Control Language="c#" %>
<%@ Import Namespace="CommunityServer.Components" %>

<CP:ControlPanelSelectedNavigation SelectedTab="blogs" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelMaster.ascx">
	<CP:Content id="StyleRegion" runat="server">
		<CSControl:Style id="UserStyles" runat="server" />
		<CSControl:Style id="Style2" runat="server" Href="../style/blogcontrolpanel.css" />
	</CP:Content>
	<CP:Content id="PanelDescription" runat="Server"><CP:ResourceControl id="MasterTitle" runat="server" ResourceName="CP_Blog_TitleDefault" /></CP:Content>
    <CP:Content id="TopLeft" runat="server" >
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tr >
        <td width="100%"><CP:ResourceControl id="MasterCurrentSection" runat="server" ResourceName="CP_Blog_Switch_CurrentBlog" />&nbsp;<CP:CurrentWeblog runat = "Server" ID="Currentweblog1"/></td>
        <td style="padding-right: 3px;" nowrap="nowrap"><CP:HyperLink ID="HyperLink1" Runat="server" CssClass="CommonTextButton" urlname="blog_ControlPanel_Switch" ResourceName="CP_Blog_Switch_ChangeBlog" />
        </tr>
        </table>
    </CP:Content>
    <CP:Content id="PanelNavigation" runat="server" >
		<CP:ControlPanelNavigationSidebar runat="server"  UseDirectNavigation="true" Filename="Blogs/NavBar.config" ID="Controlpanelnavigationsidebar1"/>
    </CP:Content>
</CP:Container>
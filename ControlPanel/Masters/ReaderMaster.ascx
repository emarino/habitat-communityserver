<%@ Control Language="C#" %>
<%@ Import Namespace="CommunityServer.Components" %>
<CP:ControlPanelSelectedNavigation SelectedTab="reader" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelMaster.ascx" >
	<CP:Content id="StyleRegion" runat="server" >
        <CSControl:Head runat="Server" ID="Head1">
			<CSControl:Style id="Style2" runat="server" Href="../style/reader/Reader.css" />
			<CSControl:Style id="style3" runat="server" href="../style/reader/treeStyle.css" />
			<CSControl:Style id="Style4" runat="server" href="../style/reader/menuStyle.css" />
			<script type="text/javascript">
            // <![CDATA[
				function PageInit()
				{
<%
	if (Request.QueryString["URL"] != null)
	{
%>
			
				Telligent_Modal.Open('AddFeed.aspx?ID=folder-0-0&URL=<%= Server.UrlEncode(Request.QueryString["URL"]) %>', 500, 130, null);
			
<%
	}
%>
				}
				// ]]>
			</script>
        </CSControl:Head>
	</CP:Content>
	<CP:Content id="PanelDescription" runat="Server">
		<cp:ResourceControl runat="server" ResourceName="reader" />
	</CP:Content>
</CP:Container>

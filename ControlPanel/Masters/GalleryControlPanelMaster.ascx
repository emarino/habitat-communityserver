<%@ Control Language="c#" %>
<%@ Import Namespace="CommunityServer.Components" %>
<CP:ControlPanelSelectedNavigation SelectedTab="photos" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelMaster.ascx">
	<CP:Content id="PanelDescription" runat="Server"><CP:ResourceControl id="PanelDescriptionResource" runat="Server" resourcename="CP_Photos_PanelTitleDefault"></CP:ResourceControl></CP:Content>
    <CP:Content id="TopLeft" runat="server" >
        <table cellpadding="0" cellspacing="0" border="0"  width="100%">
        <tr>
        <td width="100%"><CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_Photos_CurrentGallery"></CP:ResourceControl>&nbsp;<CP:CurrentGallery runat = "Server" ID="Currentgallery1"/></td>
        <td style="padding-right: 3px;" nowrap="nowrap"><CP:HyperLink ID="HyperLink1" Runat="server" CssClass="CommonTextButton" UrlName="gallery_ControlPanel_Photos_Switch" ResourceName="CP_Photos_ChangeGallery" /></td>
        </tr>
        </table>
    </CP:Content>
    <CP:Content id="PanelNavigation" runat="server" >
		<CP:ControlPanelNavigationSidebar runat="server"  UseDirectNavigation="true" Filename="Photos/NavBar.config" ID="Controlpanelnavigationsidebar1"/>
    </CP:Content>
</CP:Container>
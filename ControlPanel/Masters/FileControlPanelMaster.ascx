<%@ Control Language="c#" %>
<%@ Import Namespace="CommunityServer.Components" %>

<CP:ControlPanelSelectedNavigation SelectedTab="files" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelMaster.ascx">
	<CP:Content id="StyleRegion" runat="server">
		<CSControl:Style id="UserStyles" runat="server" />
		<CSControl:Style id="Style2" runat="server" Href="../style/filecontrolpanel.css" />
	</CP:Content>
	<CP:Content id="PanelDescription" runat="Server">
		<CP:ResourceControl runat="server" ResourceName="CP_Files_Title" />
	</CP:Content>
    <CP:Content id="TopLeft" runat="server" >
        <table cellpadding="0" cellspacing="0" border="0">
        <tr>
        <td  width="100%"><CP:ResourceControl runat="server" ResourceName="CP_Files_CurrentFolder" />&nbsp;<CP:CurrentFileFolder runat = "Server" ID="CurrentFolder"/></td>
        <td  style="padding-right: 3px;" nowrap="nowrap"><asp:HyperLink ID="HyperLink1" Runat="server" CssClass="CommonTextButton" NavigateUrl="~/ControlPanel/Files/Switch.aspx"><CP:ResourceControl runat="server" ResourceName="CP_Files_ChangeFolder" /></asp:HyperLink></td>
        </tr>
        </table>
    </CP:Content>
    <CP:Content id="PanelNavigation" runat="server" >
		<CP:ControlPanelNavigationSidebar runat="server"  UseDirectNavigation="true" Filename="Files/NavBar.config" ID="Controlpanelnavigationsidebar1"/>
    </CP:Content>
</CP:Container>
<%@ Control Language="c#" %>
<%@ Import Namespace="CommunityServer.Components" %>

<CP:ControlPanelSelectedNavigation SelectedTab="setup" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelMaster.ascx">
	<CP:Content id="StyleRegion" runat="server">
		<CSControl:Style id="UserStyles" runat="server" />
		<CSControl:Style id="Style2" runat="server" Href="../style/setupcontrolpanel.css" />
	</CP:Content>
	<CP:Content id="PanelDescription" runat="Server"><CP:ResourceControl id = "HeaderTitle" runat="server" ResourceName="CP_Settings_Title"/></CP:Content>
    <CP:Content id="PanelNavigation" runat="server" >
		<CP:ControlPanelNavigationSidebar runat="server" UseDirectNavigation="true" Filename="SetupNavBar.config" ID="Controlpanelnavigationsidebar1"/>
    </CP:Content>
</CP:Container>
<%@ Control Language="c#" %>
<%@ Import Namespace="CommunityServer.Components" %>
<CP:ControlPanelSelectedNavigation SelectedTab="Reporting" runat="server" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelMaster.ascx">
	<CP:Content id="StyleRegion" runat="server">
		<CSControl:Style id="UserStyles" runat="server" />
		<CSControl:Style id="Style2" runat="server" Href="../style/moderationcontrolpanel.css" />
		<CSControl:Style  runat="server" Href="../style/forum.css" />
	</CP:Content>
	<CP:Content id="PanelDescription" runat="Server"><CP:ResourceControl id = "HeaderTitle" runat="server" ResourceName="CP_Reporting_Title"/></CP:Content>
	<CP:Content id="TopLeft" runat="server" ></CP:Content>
	<CP:Content id="PanelNavigation" runat="server" >
		<CP:ControlPanelNavigationSidebar runat="server"  UseDirectNavigation="true" Filename="Reporting/NavBar.config" />
	</CP:Content>
</CP:Container>
﻿<%@ Control Language="c#" %>
<%@ Import Namespace="CommunityServer.Components" %>

<script runat="server">
	void Page_Load()
	{
		if(!Telligent.Registration.Licensing.CommunityServer.IsCommercial)
		{
			EulaHyperlink.NavigateUrl = "http://communityserver.org/r.ashx?1";
			EulaImage.ImageUrl = "~/utility/PoweredByCS_personal.GIF";
			EulaImage.AlternateText = "Community Server (Non-Commercial Edition)";
		}
		else
		{
			EulaHyperlink.NavigateUrl = "http://communityserver.org/r.ashx?j";
			EulaImage.ImageUrl = "~/utility/PoweredByCS_commercial.GIF";
			EulaImage.AlternateText = "Community Server (Commercial Edition)";
		}
	}
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional-dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head runat="server">
	    <title>
			<CP:Region id="PageTitleRegion" runat="server" ><CP:ResourceControl id = "HeaderTitle" runat="server" ResourceName="Admin_Title"/></CP:Region>
		</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<CSControl:Style id="UserStyles" runat="server" />
	    <CSControl:Style id="Style2" runat="server" Href="../style/controlpanel.css" />
 		<CSControl:Script id="Script2" runat="server" Src="~/utility/global.js" />
	    <CP:Region id="StyleRegion" runat="server" ></CP:Region>
    </head>
    <body>    
		<CP:Form runat="server" id="AdminForm">
			<TWC:Modal runat="server" CssClasses="CommonModal" TitleCssClasses="CommonModalTitle" CloseCssClasses="CommonModalClose" ContentCssClasses="CommonModalContent" FooterCssClasses="CommonModalFooter" ResizeCssClasses="CommonModalResize" MaskCssClasses="CommonModalMask" LoadingUrl="~/utility/loading.htm" />			
			<div id="CommonOuter"><div id="Common">
				<div class="CommonTitleBar">
					<div class="CommonTitleBarImage">
						<table cellpadding=0 cellspacing=0 border=0 width="100%">
							<tr>
								<td class="CommonTitleBarTitleArea" nowrap="nowrap">
									<h1 class="CommonTitleBarTitle"><CP:Region id="PanelTitle" runat="server" ><CP:ResourceControl id="Resourcecontrol2" runat="server" ResourceName="CP" /></CP:Region></h1>
									<div class="CommonTitleBarDescription"><CP:Region id="PanelDescription" runat="server"></CP:Region></div>
								</td>
								<td width="450" style="padding: 0 25px;">
									<CP:LicenseCheck runat="server" />
								</td>
								<td class="CommonTitleBarSearchArea" nowrap="nowrap">
									<div class="CommonUserArea">
										<cp:DisplayUserWelcome runat="server" id="Displayuserwelcome1">
										    <SkinTemplate>
										        <div id="welcome">
	                                                <cp:LoginView runat="Server">
		                                                <AnonymousTemplate>
			                                                <cp:AnonymousUserControl runat="Server">
			                                                    <SkinTemplate>
                                                                    <asp:Literal id="Message" runat="Server" />
                                                                    <asp:PlaceHolder runat="Server" id="FormsPanel" visible = "false">
	                                                                    <asp:HyperLink id="Login" runat="Server" />  <cp:ResourceControl runat="Server" id="RegisterArea" ResourceName = "Utility_ForumAnchorType_MenuSpacer" /> <asp:HyperLink id="Register" runat="Server" />
                                                                    </asp:PlaceHolder>
                                                                    <asp:PlaceHolder runat="Server" id="PassportPanel" visible = "false">
	                                                                    <asp:Literal id="PassportButton" runat="server" />
                                                                    </asp:PlaceHolder>
                                                                    <asp:PlaceHolder runat="Server" id="WindowsPanel" visible = "false">

                                                                    </asp:PlaceHolder>
			                                                    </SkinTemplate>
			                                                </cp:AnonymousUserControl>
		                                                </AnonymousTemplate>
		                                                <LoggedInTemplate>
			                                                <cp:RegisteredUserControl runat="Server">
			                                                    <SkinTemplate>
                                                                    <asp:Literal id="Message" runat="Server" />
                                                                    <asp:PlaceHolder runat="Server" id="FormsPanel" visible = "false">
	                                                                    <cp:ResourceControl runat="Server" ResourceName = "Utility_ForumAnchorType_MenuSpacer" /> <asp:HyperLink id="logout" runat="Server" /> <cp:ResourceControl runat="Server" ResourceName = "Utility_ForumAnchorType_MenuSpacer" ID="InviteArea" /> <asp:HyperLink id="invite" runat="Server" />
                                                                    </asp:PlaceHolder>
                                                                    <asp:PlaceHolder runat="Server" id="PassportPanel" visible = "false">
	                                                                    <asp:Literal id="PassportButton" runat="server" /> <cp:ResourceControl runat="Server" ResourceName = "Utility_ForumAnchorType_MenuSpacer" ID="InviteAreaPassport" /> <asp:HyperLink id="InvitePassport" runat="Server" />
                                                                    </asp:PlaceHolder>
                                                                    <asp:PlaceHolder runat="Server" id="WindowsPanel" visible = "false">

                                                                    </asp:PlaceHolder>
			                                                    </SkinTemplate>
			                                                </cp:RegisteredUserControl>
		                                                </LoggedInTemplate>
	                                                </cp:LoginView>

	                                                <cp:ResourceControl runat="Server" ResourceName = "Utility_ForumAnchorType_MenuSpacer" />
	                                                <CSControl:SiteUrl runat="server" UrlName="faq" ResourceName="help" />
	                                                <asp:placeholder runat="Server" ID="PrivateMessages"/>
                                                </div>
										    </SkinTemplate>
										</cp:DisplayUserWelcome>
									</div>
									<div class="CommonTitleBarSiteLink"><cp:Href runat="server" NavigateUrl="~/" CssClass="CommonTextButtonBig" ID="Href1"><CP:ResourceControl runat="server" ResourceName="CP_ReturnToSite" ID="Resourcecontrol1"/></cp:Href></div>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="CommonTabBar">
				    <div class="CommonTabBarInner">				
					    <CP:ControlPanelTabStrip runat = "Server" id="PanelTabs" 
					        FileName="~/ControlPanel/Tabs.Config" 
					        CssClass="CommonTopGroup"
		                    TabCssClasses="CommonTopLevelTab"
		                    TabSelectedCssClasses="CommonSelectedTopLevelTab"
		                    TabHoverCssClasses="CommonTopLevelTabHover"
		                    EnableResizing="false"
					    />
					</div>
				</div>

					<table width="100%" cellpadding="0" cellspacing="0" border="0" id="PanelContentContainer"  style="clear:both;" >
						<tr valign="top">
							<td id="PanelNavigationColumn" rowspan="2">
								<CP:Region id="PanelNavigation" runat="server"></CP:Region>
							</td>
							<td width="100%">
								<div id="PanelWorkingObjectContainer">
									<CP:Region id="TopLeft" runat="server" ></CP:Region>
								</div>
							</td>
						</tr>
						<tr valign="top">
							<td width="100%">
								<table width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr valign="top">
								<td width="100%"><table width="100%" cellpadding="0" cellspacing="0" border="0" style="table-layout: fixed;"><tr><td width="100%">
								    <CP:Region id="OuterTaskRegion" runat="server">
									    <div class="CommonContentArea">
										    <h2 class="CommonTitle"><CP:Region id="DescriptionRegion" runat="server" ></CP:Region></h2>
										    <div class="CommonContent">
											    <CP:Region id="TaskRegion" runat="server" ></CP:Region>
										    </div>
									    </div>
									</CP:Region>
								</td></tr></table></td>
								<td id="CommonRightColumn">
									<CP:Region id="RightColumnRegion" runat="server" />
								</td>
								</tr>
								</table>
							</td>
						</tr>
					</table>

				<div id="ControlPanelFooter">
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr valign="middle">
						<td align="left">
							<asp:HyperLink Runat = "server" id="EulaHyperlink">
								<asp:Image Runat = "server" id="EulaImage" />
							</asp:HyperLink>
						</td>
						<td align="right">
							<a href="http://communityserver.org/r.ashx?2"><asp:Image ImageUrl = "~/utility/images/telligent-cp.gif" Runat = "server" AlternateText = "Telligent" /></a> 
							<br />
							Copyright  <a href="http://communityserver.org/r.ashx?2">Telligent</a>, 2005-2007. All rights reserved.
						</td>
					</table>
				</div>
			</div></div>
		</CP:Form>
    </body>
</html>


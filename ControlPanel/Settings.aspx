<%@ Page language="c#" Codebehind="Settings.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.SettingsPage" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">Control Panel Dashboard 
Settings</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">Use the options below to configure the default 
			behaviors of the Control Panel.</DIV>
		<TABLE cellPadding="4">
			<TR>
				<TD>
					<CP:HELPICON id="Helpicon4" runat="Server" Text="Auto scale the pagesize for multiple row grids"></CP:HELPICON>
					<CP:FormLabel id="Formlabel4" runat="server" Text="Auto Scale Page Size" ControlToLabel="ynScalePageSize"></CP:FormLabel></TD>
				<TD>
					<CP:YESNORADIOBUTTONLIST id="ynScalePageSize" runat="server" CssClass="txt1" RepeatColumns="2"></CP:YESNORADIOBUTTONLIST></TD>
			</TR>
			<TR>
				<TD>
					<CP:HELPICON id="Helpicon5" runat="Server" Text="Enables the Componet Art Grouping features of a Grid where supported."></CP:HELPICON>
					<CP:FormLabel id="Formlabel5" runat="server" Text="Enable Grid Groups" ControlToLabel="ynEnableGrouping"></CP:FormLabel></TD>
				<TD>
					<CP:YESNORADIOBUTTONLIST id="ynEnableGrouping" runat="server" CssClass="txt1" RepeatColumns="2"></CP:YESNORADIOBUTTONLIST></TD>
			</TR>
			<TR>
				<TD>
					<CP:HELPICON id="Helpicon3" runat="Server" Text="Auto expand grid groupings by default"></CP:HELPICON>
					<CP:FormLabel id="Formlabel3" runat="server" Text="Expand Grid Groups" ControlToLabel="ynExpandGroups"></CP:FormLabel></TD>
				<TD>
					<CP:YESNORADIOBUTTONLIST id="ynExpandGrouping" runat="server" CssClass="txt1" RepeatColumns="2"></CP:YESNORADIOBUTTONLIST></TD>
			</TR>
			<TR>
				<TD>
					<CP:HELPICON id="Helpicon6" runat="Server" Text="Enables the Componet Art client side searching of a Grid where supported."></CP:HELPICON>
					<CP:FormLabel id="Formlabel6" runat="server" Text="Enable Grid Search Boxes" ControlToLabel="ynEnableSearchBox"></CP:FormLabel></TD>
				<TD>
					<CP:YESNORADIOBUTTONLIST id="ynEnableSearchBox" runat="server" CssClass="txt1" RepeatColumns="2"></CP:YESNORADIOBUTTONLIST></TD>
			</TR>
			<TR>
				<TD>
					<CP:HELPICON id="Helpicon7" runat="Server" Text="The way that data is passed back to the grid (All at once [client], one page at a time [callback] or [auto] where grids under about 500 rows will be [client] and over 500 rows will be [callback])"></CP:HELPICON>
					<CP:FormLabel id="Formlabel7" runat="server" Text="Grid Running Mode" ControlToLabel="GridMode"></CP:FormLabel></TD>
				<TD>
					<asp:DropDownList id="GridMode" Runat="Server">
						<asp:ListItem Value="Auto">Automatic</asp:ListItem>
						<asp:ListItem Value="Client">Always send all grid data to the client</asp:ListItem>
						<asp:ListItem Value="Callback">Send only one screen of grid data to the client at a time</asp:ListItem>
					</asp:DropDownList></TD>
			</TR>
			<TR>
				<TD>
					<CP:HELPICON id="Helpicon8" runat="Server" Text="Enables advanced options and features in the control panel"></CP:HELPICON>
					<CP:FormLabel id="Formlabel8" runat="server" Text="Show Advanced Features" ControlToLabel="ynShowAdvanced"></CP:FormLabel></TD>
				<TD>
					<CP:YESNORADIOBUTTONLIST id="ynShowAdvanced" runat="server" CssClass="txt1" RepeatColumns="2"></CP:YESNORADIOBUTTONLIST></TD>
			</TR>
		</TABLE>
		<DIV class="CommonFormField PanelSaveButton">
			<cp:ResourceLinkButton id="Save" runat="Server" CssClass="CommonTextButton" ResourceName="Save"></cp:ResourceLinkButton></DIV>
	</CP:Content>
</CP:Container>

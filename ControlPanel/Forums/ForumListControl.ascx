<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ForumListControl.ascx.cs" Inherits="CommunityServer.ControlPanel.Forums.ForumListControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script type="text/javascript">
function onCallbackError(excString)
{
	if (confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_CallBackWarning") %>')) alert(excString); 
	<%= Grid1.ClientID %>.Page(1); 
}
function DeleteCallback(res)
{
    refresh();
}
function onDelete(item)
{
	return confirm('<%= DeleteWarning %>'); 
}

function deleteRow(rowId)
{
	<%= Grid1.ClientID %>.Delete(<%= Grid1.ClientID %>.GetRowFromClientId(rowId)); 
}
</script>
<div id="Filters">
	<cp:resourcecontrol id="FeedbackFilterLabel" runat="Server" resourcename="CP_Photos_GridCol_Group"></cp:resourcecontrol>&nbsp;<asp:dropdownlist id="AdminGroupList" runat="server" autopostback="True"></asp:dropdownlist>
</div>
<div id="GrayGrid" >
<ca:grid id="Grid1" runat="server" 
	autocallbackoninsert="false" 
	autocallbackonupdate="false" 
	autocallbackondelete="true" 
	clientsideoncallbackerror="onCallbackError" 
	ClientSideOnDelete="onDelete" 
	>
	<levels>
	<ca:gridlevel datakeyfield="SectionID" >
		<columns>
			<ca:gridcolumn datafield="Name" headingtext="ResourceManager.CP_Photos_GridCol_Name" />
			<ca:gridcolumn datafield="Description" headingtext="ResourceManager.CP_Photos_GridCol_Description" allowgrouping="false" />
            <ca:gridcolumn datafield="GroupName" headingtext="ResourceManager.CP_Photos_GridCol_Group" />
			<ca:gridcolumn datafield="IsActive" width="55" headingtext="ResourceManager.CP_Photos_GridCol_Enabled" allowgrouping="false" />
			<ca:gridcolumn datafield="SectionID" visible="false" allowgrouping="false" />
			<ca:gridcolumn datafield="Url" headingtext="ResourceManager.CP_Photos_GridCol_Actions" width="120" align="Center" allowgrouping="false" datacellcssclass="LastDataCell" />
		</columns>
	</ca:gridlevel>
	</levels>
</ca:grid>
</div>

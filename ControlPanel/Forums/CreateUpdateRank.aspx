<%@ Page language="c#" Codebehind="CreateUpdateRank.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Forums.CreateUpdateRank" %>
<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="ControlPanelModalMaster.ascx">
	<CP:Content id="bcr" runat="Server">
		<div class="CommonContentArea">
		<div class="CommonContent">
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="CommonFormFieldName">
					<cp:resourcelabel runat="server" resourcename="Name" id="Resourcelabel4"/>
				</td>
				<td class="CommonFormField">
					<asp:textbox id="RankName" runat="server" />
					<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="RankName" id="RankNameValidator" />
				</td>
			</tr>
			<tr>
				<td class="CommonFormFieldName">
					<cp:resourcelabel runat="server" resourcename="CP_Forums_CreateUpdateRank_MinPosts" id="Resourcelabel5"/>
				</td>
				<td class="CommonFormField">
					<asp:textbox id="RankMinPost" runat="server" columns="10" />
					<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="RankMinPost" id="RankMinPostValidator" />
				</td>
			</tr>
			<tr>
				<td class="CommonFormFieldName">
					<cp:resourcelabel runat="server" resourcename="CP_Forums_CreateUpdateRank_MaxPosts" id="Resourcelabel6"/>
				</td>
				<td class="CommonFormField">
					<asp:textbox id="RankMaxPost" runat="server" columns="10" />
					<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="RankMaxPost" id="RankMaxPostValidator" />
				</td>
			</tr>
			<tr>
				<td colspan="2" valign="top">
					<div class="CommonFormFieldName">
						<cp:resourcelabel runat="server" resourcename="CP_Forums_CreateUpdateRank_RankIcon" id="RoleIconResourceLabel"/>
					</div>
					<table border="0" cellpadding="1" cellspacing="0">
					<tr>
						<td class="CommonFormFieldName">
							<asp:Image runat="server" id="RankImage" />
						</td>
						<td class="CommonFormField">
							<div class="CommonFormFieldName">
								<cp:resourcecontrol runat="server" resourcename="CP_Forums_CreateUpdateRank_UploadRankIcon" id="Resourcecontrol32"/>
							</div>
							<div class="CommonFormField">
								<input type="file" runat="server" id="RankImageUpload" name="RankImageUpload"/>
								<br />
								<cp:resourcelinkbutton id="DeleteImageButton" runat="server" cssclass="CommonTextButton" resourcename="CP_Forums_CreateUpdateRank_DeleteRankIcon"></cp:resourcelinkbutton>
							</div>
						</td>
					</tr>
					</table>	
				</td>
			</tr>
		</table>
		<div class="CommonFormFieldName PanelSaveButton" align="right">
			<cp:resourcelinkbutton id="SaveButton" runat="server" cssclass="CommonTextButton" resourcename="Save" />
			&nbsp;
			<cp:resourcelinkbutton id="DeleteButton" runat="server" cssclass="CommonTextButton" resourcename="Delete" causesvalidation="false" />
		</div>
		</div>
		</div>
	</CP:Content>
</CP:Container>

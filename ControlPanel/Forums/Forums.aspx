<%@ Page language="c#" Codebehind="Forums.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Forums.Forums" %>
<%@ Register TagPrefix="CP" TagName="ForumListControl" Src="~/ControlPanel/Forums/ForumListControl.ascx" %>
<cp:controlpanelselectednavigation runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="SectionDescription" runat="server" resourcename="CP_Forums_ForumList_Title" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonDescription">
			<cp:resourcecontrol id="Section_Sub_Description" runat="server" resourcename="CP_Forums_ForumList_SubTitle" />
		</div>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Forums" />
		<cp:ForumListControl id="ForumList" runat="server" />
		<p class="PanelSaveButton">
			<cp:resourcelinkbutton id="AddButton" runat="server" resourcename="CP_Forums_TreeView_NewForum" cssclass="CommonTextButton" /> &nbsp;&nbsp;&nbsp;
			<cp:controlpanelchangemanageview runat="server" cssclass="CommonTextButton" currentview="Grid" viewchangeurl="~/ControlPanel/Forums/GroupManager.aspx" id="Controlpanelchangemanageview1"/>
		</p>
	</CP:Content>
</CP:Container>

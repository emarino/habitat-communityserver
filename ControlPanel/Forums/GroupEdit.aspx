<%@ Page language="c#" Codebehind="GroupEdit.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Forums.GroupEdit" %>
<%@ Register TagPrefix="CP" TagName="GroupEditControl" Src="~/ControlPanel/Forums/GroupEditControl.ascx" %>
<cp:controlpanelselectednavigation runat="server" id="SelectedNavigation1" SelectedNavItem="ForumGroupList" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<asp:literal id="RegionTitle" runat="server"></asp:literal>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<cp:GroupEditControl id="GroupEdit1" runat="server" />
		<p class="PanelSaveButton DetailsFixedWidth">
			<cp:resourcelinkbutton id="SaveButton" runat="server" resourcename="Save" cssclass="CommonTextButton" />
			<cp:resourcelinkbutton runat="server" id="DeleteButton" cssclass="CommonTextButton" causesvalidation="false" resourcename="Delete" />
		</p>
	</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="SectionEdit.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Forums.SectionEdit" %>
<%@ Register TagPrefix="CP" TagName="SectionEditControl" Src="~/ControlPanel/Forums/SectionEditControl.ascx" %>
<cp:controlpanelselectednavigation runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<asp:literal id="RegionTitle" runat="server"></asp:literal>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonDescription">
			<cp:resourcecontrol id="Section_Sub_Description" runat="server" resourcename="CP_Forums_SectionEdit_SubTitle" />
		</div>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Forums" />
		<cp:SectionEditControl id="SectionEdit1" runat="server" />
		<p class="PanelSaveButton DetailsFixedWidth">
			<cp:resourcelinkbutton id="SaveButton" runat="server" resourcename="Save" cssclass="CommonTextButton" />
			<cp:resourcelinkbutton runat="server" id="DeleteButton" cssclass="CommonTextButton" causesvalidation="false" resourcename="Delete"></cp:resourcelinkbutton>
		</p>
	</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="Default.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Forums.ForumsHome" %>
<cp:controlpanelselectednavigation selectednavitem="Forums" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="CP_Forums_DefaultHome_Title"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<CP:ResourceControl runat="server" ResourceName="CP_Forums_DefaultHome_Description" />
		</DIV>
	</CP:Content>
</CP:Container>

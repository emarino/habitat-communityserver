<%@ Register TagPrefix="CP" TagName = "RankListControl" Src = "~/ControlPanel/Forums/RankListControl.ascx" %>
<%@ Page language="c#" Codebehind="ManageRanks.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Forums.ManageRanks" %>
<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="SectionDescription" runat="server" ResourceName="CP_Forums_ManageRanks_Title"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<cp:statusmessage id="Status" runat="server"></cp:statusmessage>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Forums" />
		<DIV class="CommonDescription">
			<cp:resourcecontrol id="Section_Sub_Description" runat="server" resourcename="CP_Forums_ManageRanks_SubTitle"></cp:resourcecontrol></DIV>
		<cp:RankListControl id="RankList" runat="server"></cp:RankListControl>
	</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="ForumGroups.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Forums.ForumGroupsPage" %>
<%@ Register TagPrefix="CP" TagName="ForumGroupListControl" Src="~/ControlPanel/Forums/ForumGroupListControl.ascx" %>
<cp:controlpanelselectednavigation runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="SectionDescription" runat="server" resourcename="CP_Forums_ForumGroupList_Title" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonDescription">
			<cp:resourcecontrol id="Section_Sub_Description" runat="server" resourcename="CP_Forums_ForumGroupList_SubTitle" />
		</div>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Forums" />
		<div class="PanelSaveButton">
			<cp:resourcelinkbutton id="AddButton" runat="server" resourcename="CP_Forums_TreeView_NewGroup" cssclass="CommonTextButton" />
		</div>
		<cp:ForumGroupListControl id="ForumGroupList" runat="server" />
		<p class="PanelSaveButton">
			<cp:controlpanelchangemanageview runat="server" cssclass="CommonTextButton" currentview="Grid" viewchangeurl="~/ControlPanel/Forums/GroupManager.aspx" id="Controlpanelchangemanageview1"/>
		</p>
	</CP:Content>
</CP:Container>

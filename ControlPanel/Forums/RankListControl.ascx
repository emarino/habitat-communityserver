<%@ Control Language="c#" AutoEventWireup="false" Codebehind="RankListControl.ascx.cs" Inherits="CommunityServer.ControlPanel.Forums.RankListControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script type="text/javascript">
	function onCallbackError(excString)
	{
		if (confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_CallBackWarning") %>')) alert(excString); 
		<%= Grid1.ClientID %>.Page(1); 
	}
	
	var editName = '<cp:resourcecontrol runat="server" resourcename="Edit" />';
	var deleteName = '<cp:resourcecontrol runat="server" resourcename="Delete" />';
</script>

<p class="PanelSaveButton">
	<cp:hyperlink id="AddRank" runat="server" NavigateUrl="javascript:Telligent_Modal.Open('CreateUpdateRank.aspx', 450, 300, null);" cssclass="CommonTextButton" resourcename="CP_Forums_ManageRanks_Add"></cp:hyperlink>
</p>

<div id="GrayGrid" >
<ca:grid id="Grid1" runat="server" 
	autocallbackoninsert="false" 
	autocallbackonupdate="false" 
	autocallbackondelete="false" 
	clientsideoncallbackerror="onCallbackError" 
	>
	<levels>
	<ca:gridlevel datakeyfield="RankID">
		<columns>
			<ca:gridcolumn datafield="RankId" visible="false" allowgrouping="false" />
			<ca:gridcolumn datafield="RankIconUrl" headingtext=" " allowgrouping="false" allowsorting="false" datacellclienttemplateid="IconTemplate" />
			<ca:gridcolumn datafield="RankName" headingtext="ResourceManager.Name" />
			<ca:gridcolumn datafield="PostingCountMinimum" headingtext="ResourceManager.Rank_MinPosts" allowgrouping="false" />
            <ca:gridcolumn datafield="PostingCountMaximum" headingtext="ResourceManager.Rank_MaxPosts" />
			<ca:gridcolumn headingtext="ResourceManager.Actions" datacellclienttemplateid="EditTemplate" width="180" align="Center" allowgrouping="false" datacellcssclass="LastDataCell" />
		</columns>
	</ca:gridlevel>
	</levels>
	<clienttemplates>
		<ca:clienttemplate id="IconTemplate">
            ## 
                if(DataItem.GetMember('RankIconUrl').Text != '') 
                {
                    '<img src="' + DataItem.GetMember("RankIconUrl").Text + '" border="0" />'
                }
            ##
		</ca:clienttemplate>
		<ca:clienttemplate id="EditTemplate">
			<a href="javascript:Telligent_Modal.Open('CreateUpdateRank.aspx?RankID=## DataItem.GetMember("RankId").Text ##', 500, 400, null);" class="CommonTextButton">##editName##</a>
			<a href="javascript:Telligent_Modal.Open('CreateUpdateRank.aspx?Delete=1&RankID=## DataItem.GetMember("RankId").Text ##', 500, 400, null);" class="CommonTextButton">##deleteName##</a>
		</ca:clienttemplate>
	</clienttemplates>
</ca:grid>
</div>


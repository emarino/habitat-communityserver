<%@ Page language="c#" Codebehind="GroupManager.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Forums.GroupManager" %>
<%@ Register TagPrefix="CP" TagName = "ForumTreeViewControl" Src = "~/ControlPanel/Forums/ForumTreeViewControl.ascx" %>
<CP:Container runat="server" id="Mpcontainer1" thememasterfile="SetupControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
	<cp:resourcecontrol id="SectionDescription" runat="server" resourcename="CP_Forums_ForumManagement"></cp:resourcecontrol>
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">
<script type="text/javascript">
// <![CDATA[
// Disable grid hover style to avoid the flicker while resizing 
function startResize(PaneDomObj)
{
    return true; 
}    

// Restore the grid hover style 
function endResize(PaneDomObj)
{
    return true; 
}    

// For splitter support
function resizeDetails(DomElementId, NewPaneHeight, NewPaneWidth)
{
	if(DomElementId != null)
		resizeCategoryTree();
    // Forces the treeview to adjust to the new size of its container 
    
    //manually resize the iframe
    var sectionEditFrame = document.getElementById("sectionEditFrame");
    if(sectionEditFrame != null  && NewPaneHeight != null && NewPaneWidth != null)
	{
		sectionEditFrame.style.height = (NewPaneHeight - 19) + 'px';
		sectionEditFrame.style.width = NewPaneWidth + 'px';
	}
}
// ]]>
</script>

<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Forums" />
<div class="CommonManagerArea">
	<div class="CommonManagerContent">
	<ca:splitter id="Splitter1" runat="server" clientsideonresizeend="endResize" clientsideonresizestart="startResize"
		widthadjustment="305" fillwidth="true" fillheight="true" heightadjustment="-240">
		<layouts>
			<ca:splitterlayout>
				<panes splitterbarcollapseimageurl="../Images/splitter_col.gif" splitterbarexpandimageurl="../Images/splitter_exp.gif"
					splitterbarexpandimagewidth="6"
					splitterbarcollapseimagewidth="6" splitterbarcollapseimageheight="116" splitterbarcssclass="SplitterBar"
					splitterbarcollapsedcssclass="SplitterBarCollapsed" splitterbaractivecssclass="ActiveSplitterBar"
					splitterbarwidth="5" orientation="Horizontal">
					<ca:splitterpane panecontentid="TreeViewContent" width="220" minwidth="100" clientsideonresize="resizeCategoryTree"
						cssclass="SplitterPane"></ca:splitterpane>
					<ca:splitterpane panecontentid="DetailsContent" cssclass="SplitterPane" clientsideonresize="resizeDetails" allowscrolling="False"></ca:splitterpane>
				</panes>
			</ca:splitterlayout>
		</layouts>
		<content>
			<ca:splitterpanecontent id="TreeViewContent">
				<div class="SplitterListHeading" id="FolderListingHeader" style="height:19px;"><cp:resourcecontrol resourcename="CP_Forums_TreeView_TreeViewHeader" runat="server" ID="Resourcecontrol1"/></div>
				<cp:forumtreeviewcontrol runat="server" id="ForumTree" />
			</ca:splitterpanecontent>
			<ca:splitterpanecontent id="DetailsContent">
				<div class="SplitterListHeading" id="DetailsListingHeader" style="height:19px;"><cp:resourcecontrol resourcename="CP_Forums_TreeView_Details" runat="server" ID="Resourcecontrol2"/></div>
				<iframe id="sectionEditFrame" name="sectionEditFrame" src="IFrameHost.aspx" frameborder="no"
					marginwidth="30" marginheight="30" scrolling="<%=IFrameScrolling%>" ></iframe>
			</ca:splitterpanecontent>
		</content>
	</ca:splitter>
	</div>
	<div class="CommonManagerButtonsArea">
		<cp:hyperlink id="AddGroupButton" runat="server" resourcename="CP_Forums_Home_AddNewGroup" cssclass="CommonTextButton" navigateurl="javascript:AddGroup();" />
		<cp:hyperlink id="AddSectionButton" runat="server" resourcename="CP_Forums_Home_AddNewForum" cssclass="CommonTextButton" navigateurl="javascript:AddSection();" />
		<cp:hyperlink id="DeleteButton" runat="server" resourcename="CP_Forums_Home_DeleteSelected" cssclass="CommonTextButton" navigateurl="javascript:DeleteSelected();" />
	</div>
</div>
<p class="PanelSaveButton">
		<cp:controlpanelchangemanageview runat="server" cssclass="CommonTextButton" currentview="Tree" viewchangeurl="~/ControlPanel/Forums/ForumGroups.aspx" id="Controlpanelchangemanageview1"/>
	</p>
</CP:Content>
</CP:Container>

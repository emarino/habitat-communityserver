<%@ Page language="c#" Codebehind="DefaultPermissionList.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Forums.DefaultPermissionList" %>

<CP:Container runat="server" id="Mpcontainer1" ThemeMasterFile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="SectionDescription" runat="server" ResourceName="CP_Forums_Default_Title"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<CP:ResourceControl id="Section_Sub_Description" runat="server" ResourceName="CP_Forums_Default_Description"></CP:ResourceControl><BR>
			<EM>&nbsp;&nbsp;&nbsp;
				<CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_Forums_Default_Tip"></CP:ResourceControl></EM></DIV>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Forums" />
		<TWC:TabbedPanes id="EditorTabs" runat="server"
		PanesCssClass="CommonPane"
		TabSetCssClass="CommonPaneTabSet"
		TabCssClasses="CommonPaneTab,CommonPaneTab1,CommonPaneTab2"
		TabSelectedCssClasses="CommonPaneTabSelected,CommonPaneTabSelected1,CommonPaneTabSelected2"
		TabHoverCssClasses="CommonPaneTabHover,CommonPaneTabHover1,CommonPaneTabHover2"
		>
		<TWC:TabbedPane runat="server">
			<Tab><CP:ResourceControl runat="server" ResourceName="CP_Forums_Default_Tab_UserPermissions" /></Tab>
			<Content>
				<div id="GrayGrid">
                <asp:Repeater runat="server" ID="PermissionList">
                    <HeaderTemplate>
                        <table class="Grid" cellSpacing=0 cellPadding=0 width="100%">
                        <tbody>
                            <tr class="HeadingRow HeadingCellText HeadingCell">
                                <th><CP:ResourceControl runat="server" ResourceName="CP_Forums_GridCol_Name" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_View" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Read" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Post" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Reply" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Vote" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Ink" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Video" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_LocalAttachment" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_RemoteAttachment" /></th>
                            </tr>    
                    </HeaderTemplate>
                    <ItemTemplate>
                            <tr class="Row">
                                <td><%# Eval("Name") %><asp:HiddenField ID="RoleId" runat="server" Value='<%# Eval("RoleIDString") %>' Visible="false" /></td>
                                <td><asp:CheckBox runat="server" ID="ViewCheckbox" Checked='<%# Eval("View") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="ReadCheckbox" Checked='<%# Eval("Read") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="PostCheckbox" Checked='<%# Eval("Post") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="ReplyCheckbox" Checked='<%# Eval("Reply") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="VoteCheckbox" Checked='<%# Eval("Vote") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="InkCheckbox" Checked='<%# Eval("Ink") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="VideoCheckbox" Checked='<%# Eval("Video") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="LocalAttachmentCheckbox" Checked='<%# Eval("LocalAttachment") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="RemoteAttachmentCheckbox" Checked='<%# Eval("RemoteAttachment") %>' /></td>
                            </tr>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                            <tr class="AlternatingRow">
                                <td><%# Eval("Name") %><asp:HiddenField ID="RoleId" runat="server" Value='<%# Eval("RoleIDString") %>' Visible="false" /></td>
                                <td><asp:CheckBox runat="server" ID="ViewCheckbox" Checked='<%# Eval("View") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="ReadCheckbox" Checked='<%# Eval("Read") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="PostCheckbox" Checked='<%# Eval("Post") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="ReplyCheckbox" Checked='<%# Eval("Reply") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="VoteCheckbox" Checked='<%# Eval("Vote") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="InkCheckbox" Checked='<%# Eval("Ink") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="VideoCheckbox" Checked='<%# Eval("Video") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="LocalAttachmentCheckbox" Checked='<%# Eval("LocalAttachment") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="RemoteAttachmentCheckbox" Checked='<%# Eval("RemoteAttachment") %>' /></td>
                            </tr>
                    </AlternatingItemTemplate>
                    <FooterTemplate>
                        </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
			</Content>
		</TWC:TabbedPane>
		<TWC:TabbedPane runat="server">
			<Tab><CP:ResourceControl runat="server" ResourceName="CP_Forums_Default_Tab_AdminPermissions" /></Tab>
			<Content>
				<div id="GrayGrid">
                <asp:Repeater runat="server" ID="AdminPermissionList">
                    <HeaderTemplate>
                        <table class="Grid" cellSpacing=0 cellPadding=0 width="100%">
                        <tbody>
                            <tr class="HeadingRow HeadingCellText HeadingCell">
                                <th><CP:ResourceControl runat="server" ResourceName="CP_Forums_GridCol_Name" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Edit" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Delete" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_CreatePoll" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_MarkAsAnswer" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Sticky" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Announcement" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_EditOthers" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Moderate" /></th>
                            </tr>    
                    </HeaderTemplate>
                    <ItemTemplate>
                            <tr class="Row">
                                <td><%# Eval("Name") %><asp:HiddenField ID="RoleId" runat="server" Value='<%# Eval("RoleIDString") %>' Visible="false" /></td>
                                <td><asp:CheckBox runat="server" ID="EditCheckbox" Checked='<%# Eval("Edit") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="DeleteCheckbox" Checked='<%# Eval("Delete") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="CreatePollCheckbox" Checked='<%# Eval("CreatePoll") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="MarkAsAnswerCheckbox" Checked='<%# Eval("MarkAsAnswer") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="StickyCheckbox" Checked='<%# Eval("Sticky") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="AnnouncementCheckbox" Checked='<%# Eval("Announcement") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="EditOthersCheckbox" Checked='<%# Eval("EditOthers") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="ModerateCheckbox" Checked='<%# Eval("Moderate") %>' /></td>
                            </tr>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                            <tr class="AlternatingRow">
                                <td><%# Eval("Name") %><asp:HiddenField ID="RoleId" runat="server" Value='<%# Eval("RoleIDString") %>' Visible="false" /></td>
                                <td><asp:CheckBox runat="server" ID="EditCheckbox" Checked='<%# Eval("Edit") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="DeleteCheckbox" Checked='<%# Eval("Delete") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="CreatePollCheckbox" Checked='<%# Eval("CreatePoll") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="MarkAsAnswerCheckbox" Checked='<%# Eval("MarkAsAnswer") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="StickyCheckbox" Checked='<%# Eval("Sticky") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="AnnouncementCheckbox" Checked='<%# Eval("Announcement") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="EditOthersCheckbox" Checked='<%# Eval("EditOthers") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="ModerateCheckbox" Checked='<%# Eval("Moderate") %>' /></td>
                            </tr>
                    </AlternatingItemTemplate>
                    <FooterTemplate>
                        </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
			</Content>
		</TWC:TabbedPane>
		</TWC:TabbedPanes>
		<P class="PanelSaveButton DetailsFixedWidth">
			<CP:ResourceLinkButton id="SaveButton" runat="server" resourcename="Save" CssClass="CommonTextButton"></CP:ResourceLinkButton></P>
	</CP:Content>
</CP:Container>

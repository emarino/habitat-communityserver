<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ForumGroupListControl.ascx.cs" Inherits="CommunityServer.ControlPanel.Forums.ForumGroupListControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script type="text/javascript">
function onCallbackError(excString)
{
	if (confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_CallBackWarning") %>')) alert(excString); 
	<%= Grid1.ClientID %>.Page(1); 
    window.location = window.location;
}
function DeleteCallback(res)
{
    refresh();
}
function onDelete(item)
{
	return confirm('<%= DeleteWarning %>'); 
}

function deleteRow(rowId)
{
	<%= Grid1.ClientID %>.Delete(<%= Grid1.ClientID %>.GetRowFromClientId(rowId)); 
}
</script>
<div id="GrayGrid" >
<ca:grid id="Grid1" runat="server" 
	autocallbackoninsert="false" 
	autocallbackonupdate="false" 
	autocallbackondelete="true" 
	clientsideoncallbackerror="onCallbackError" 
	ClientSideOnDelete="onDelete" 
	>
	<levels>
	<ca:gridlevel datakeyfield="GroupID" >
		<columns>
			<ca:gridcolumn datafield="Name" headingtext="ResourceManager.CP_Photos_GridCol_Name" />
			<ca:gridcolumn datafield="GroupID" headingtext="ResourceManager.CP_Photos_GridCol_Actions" width="120" align="Center" allowgrouping="false" datacellcssclass="LastDataCell" />
		</columns>
	</ca:gridlevel>
	</levels>
</ca:grid>
</div>

<%@ Control language="c#" Codebehind="SectionEditControl.ascx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Forums.SectionEditControl" %>
<%@ Import Namespace="CommunityServer.Discussions.Components" %>
<span class="CommonDescription"><cp:resourcecontrol id="AddSectionHelpResource" runat="server" visible="false" resourcename="CP_Forums_Home_AddNewForumHelp"></cp:resourcecontrol></span>
<cp:statusmessage id="Status" runat="server"></cp:statusmessage>
<TWC:TabbedPanes id="TabStrip" runat="server"
	PanesCssClass="CommonPane"
	TabSetCssClass="CommonPaneTabSet"
	TabCssClasses="CommonPaneTab,CommonPaneTab1,CommonPaneTab2"
	TabSelectedCssClasses="CommonPaneTabSelected,CommonPaneTabSelected1,CommonPaneTabSelected2"
	TabHoverCssClasses="CommonPaneTabHover,CommonPaneTabHover1,CommonPaneTabHover2"
	>
	<TWC:TabbedPane runat="server">
		<Tab><CP:ResourceControl runat="server" ResourceName="CP_Forums_SectionEdit_Tab_Basic" /></Tab>
		<Content>
		<table cellspacing="0" cellpadding="2" border="0">
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon id="Helpicon16" runat="Server" resourcename="CP_Forums_SectionEdit_Name_Detail"></cp:helpicon>
				<cp:resourcelabel id="Resourcelabel2" runat="server" resourcename="CP_Forums_SectionEdit_Name"></cp:resourcelabel></td>
			<td class="CommonFormField">
				<asp:textbox id="Name" runat="server" CssClass="ControlPanelTextInputBig" columns="40" maxlength="256"></asp:textbox>
				<asp:requiredfieldvalidator id="NameRequired" runat="server" controltovalidate="Name" font-bold="True" errormessage="*"></asp:requiredfieldvalidator>
				<asp:RegularExpressionValidator ValidationExpression="^.*[^\s]+.*$" 
                     ControlToValidate="Name" Display="Dynamic" cssclass="validationWarning"
                     ErrorMessage="<br/>Forum Name does not contain an alphanumeric character." runat="server" />
            </td>
		</tr>
		<tr>
			<td class="CommonFormFieldName" valign="top">
				<cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_Forums_SectionEdit_ForumDescription_Detail"></cp:helpicon>
				<cp:resourcelabel id="Resourcelabel4" runat="server" resourcename="CP_Forums_SectionEdit_ForumDescription"></cp:resourcelabel></td>
			<td class="CommonFormField">
				<asp:textbox id="Description" runat="server" columns="40" maxlength="1000" textmode="MultiLine"
					rows="3"></asp:textbox>
				<asp:customvalidator id="DescriptionRequired" runat="server" controltovalidate="Description" font-bold="True"
					errormessage="*"></asp:customvalidator></td>
		</tr>
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon id="Helpicon2" runat="Server" resourcename="CP_Forums_SectionEdit_Group_Detail"></cp:helpicon>
				<cp:resourcelabel id="Resourcelabel6" runat="server" resourcename="CP_Forums_SectionEdit_Group"></cp:resourcelabel></td>
			<td class="CommonFormField">
				<asp:literal id="GroupValue" runat="server"></asp:literal>
				<asp:dropdownlist id="AdminGroupList" runat="server" autopostback="True" />
			</td>
		</tr>
		<tr id="ParentForumRow" runat="server" visible="false">
			<td class="CommonFormFieldName">
				<cp:helpicon id="Helpicon15" runat="Server" resourcename="CP_Forums_SectionEdit_Parent_Detail"></cp:helpicon>
				<cp:resourcelabel id="Resourcelabel7" runat="server" resourcename="CP_Forums_SectionEdit_Parent"></cp:resourcelabel>
			</td>
			<td class="CommonFormField">
				<cp:hyperlink id="ParentForumName" runat="server" />
				<asp:dropdownlist id="ForumList" runat="server" visible="False"/>
			</td>
		</tr>
		<tr>
			<td class="CommonFormFieldName" nowrap="nowrap">
				<cp:helpicon id="Helpicon3" runat="Server" resourcename="CP_Forums_SectionEdit_IsActive_Detail"></cp:helpicon>
				<cp:resourcelabel id="Resourcelabel8" runat="server" resourcename="CP_Forums_SectionEdit_IsActive"></cp:resourcelabel></td>
			<td class="CommonFormField">
				<cp:yesnoradiobuttonlist id="IsActive" runat="server" cssclass="txt1" repeatcolumns="2"></cp:yesnoradiobuttonlist></td>
		</tr>
		<tr id="EnableAnonymousPostingForUsersRow" runat="server">
			<td class="CommonFormFieldName">
				<cp:helpicon id="Helpicon11" runat="Server" resourcename="CP_Forums_SectionEdit_EnableAnonymousPostingForUsers_Detail"></cp:helpicon>
				<cp:resourcelabel id="Resourcelabel22" runat="server" resourcename="CP_Forums_SectionEdit_EnableAnonymousPostingForUsers"></cp:resourcelabel></td>
			<td class="CommonFormField">
				<cp:yesnoradiobuttonlist id="EnableAnonymousPostingForUsers" runat="server" cssclass="txt1" repeatcolumns="2"></cp:yesnoradiobuttonlist></td>
		</tr>
		<tr runat="server" id="SectionLocalizationArea">
			<td class="CommonFormFieldName">
				<cp:helpicon id="Helpicon23" runat="Server" resourcename="CP_Forums_SectionEdit_DefaultLanguage_Detail" />
				<cp:formlabel id="Formlabel5" runat="Server" resourcename="CP_Forums_SectionEdit_DefaultLanguage" controltolabel="DefaultLanguage" />
			</td>
			<td class="CommonFormField">
				<cp:FilterLanguageDropDownList runat="server" id="DefaultLanguage" />
			</td>
		</tr>
		</table>
		</Content>
	</TWC:TabbedPane>
	<TWC:TabbedPane runat="server">
		<Tab><CP:ResourceControl runat="server" ResourceName="CP_Forums_SectionEdit_Tab_Advanced" /></Tab>
		<Content>
		<table cellspacing="0" cellpadding="2" border="0">
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon id="Helpicon14" runat="Server" resourcename="CP_Forums_SectionEdit_Url_Detail"></cp:helpicon>
				<cp:resourcelabel id="Resourcelabel3" runat="server" resourcename="CP_Forums_SectionEdit_Url"></cp:resourcelabel></td>
			<td class="CommonFormField">
				<asp:textbox id="Url" runat="server" columns="40" maxlength="512"></asp:textbox>
		</tr>
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon id="Helpicon4" runat="Server" resourcename="CP_Forums_SectionEdit_IsSearchable_Detail"></cp:helpicon>
				<cp:resourcelabel id="Resourcelabel10" runat="server" resourcename="CP_Forums_SectionEdit_IsSearchable"></cp:resourcelabel></td>
			<td class="CommonFormField">
				<cp:yesnoradiobuttonlist id="IsSearchable" runat="server" cssclass="txt1" repeatcolumns="2"></cp:yesnoradiobuttonlist></td>
		</tr>
		<tr>
			<td class="CommonFormFieldName" nowrap="nowrap">
				<cp:helpicon id="Helpicon5" runat="Server" resourcename="CP_Forums_SectionEdit_IsModerated_Detail"></cp:helpicon>
				<cp:resourcelabel id="Resourcelabel12" runat="server" resourcename="CP_Forums_SectionEdit_IsModerated"></cp:resourcelabel></td>
			<td class="CommonFormField">
				<cp:yesnoradiobuttonlist id="IsModerated" runat="server" cssclass="txt1" repeatcolumns="2"></cp:yesnoradiobuttonlist></td>
		</tr>
		<tr>
			<td class="CommonFormFieldName" nowrap="nowrap">
				<cp:helpicon runat="Server" resourcename="CP_Forums_SectionEdit_EnableModeratedNotifications_Detail"></cp:helpicon>
				<cp:resourcelabel runat="server" resourcename="CP_Forums_SectionEdit_EnableModeratedNotifications"></cp:resourcelabel>
            </td>
			<td class="CommonFormField">
				<cp:yesnoradiobuttonlist id="EnableModeratedNotifications" runat="server" cssclass="txt1" repeatcolumns="2" />
			</td>
		</tr>
		<tr>
			<td class="CommonFormFieldName" nowrap="nowrap">
				<cp:helpicon id="Helpicon6" runat="Server" resourcename="CP_Forums_SectionEdit_EnablePostStatistics_Detail"></cp:helpicon>
				<cp:resourcelabel id="Resourcelabel14" runat="server" resourcename="CP_Forums_SectionEdit_EnablePostStatistics"></cp:resourcelabel></td>
			<td class="CommonFormField">
				<cp:yesnoradiobuttonlist id="EnablePostStatistics" runat="server" cssclass="txt1" repeatcolumns="2"></cp:yesnoradiobuttonlist></td>
		</tr>
		<tr>
			<td class="CommonFormFieldName" nowrap="nowrap">
				<cp:helpicon id="Helpicon7" runat="Server" resourcename="CP_Forums_SectionEdit_EnablePostPoints_Detail"></cp:helpicon>
				<cp:resourcelabel id="Resourcelabel28" runat="server" resourcename="CP_Forums_SectionEdit_EnablePostPoints"></cp:resourcelabel></td>
			<td class="CommonFormField">
				<cp:yesnoradiobuttonlist id="EnablePostPoints" runat="server" cssclass="txt1" repeatcolumns="2"></cp:yesnoradiobuttonlist></td>
		</tr>
		<tr id="EnableAnonymousPostingRow" runat="server">
			<td class="CommonFormFieldName">
				<cp:helpicon id="Helpicon10" runat="Server" resourcename="CP_Forums_SectionEdit_EnableAnonymousPosting_Detail"></cp:helpicon>
				<cp:resourcelabel id="Resourcelabel20" runat="server" resourcename="CP_Forums_SectionEdit_EnableAnonymousPosting"></cp:resourcelabel></td>
			<td class="CommonFormField">
				<cp:yesnoradiobuttonlist id="EnableAnonymousPosting" runat="server" cssclass="txt1" repeatcolumns="2"></cp:yesnoradiobuttonlist></td>
		</tr>
		<tr id="EnableThreadStatusTrackingRow" runat="server">
			<td class="CommonFormFieldName">
				<cp:helpicon id="Helpicon12" runat="Server" resourcename="CP_Forums_SectionEdit_EnableThreadStatusTracking_Detail"></cp:helpicon>
				<cp:resourcelabel id="Resourcelabel24" runat="server" resourcename="CP_Forums_SectionEdit_EnableThreadStatusTracking"></cp:resourcelabel></td>
			<td class="CommonFormField">
				<cp:yesnoradiobuttonlist id="EnableThreadStatusTracking" runat="server" cssclass="txt1" repeatcolumns="2"></cp:yesnoradiobuttonlist></td>
		</tr>
		<tr id="EnableThreadStatusDefaultValueRow" runat="server">
			<td class="CommonFormFieldName">
				<cp:helpicon id="Helpicon13" runat="Server" resourcename="CP_Forums_SectionEdit_DefaultThreadStatusValue_Detail"></cp:helpicon>
				<cp:resourcelabel id="Resourcelabel26" runat="server" resourcename="CP_Forums_SectionEdit_DefaultThreadStatusValue"></cp:resourcelabel></td>
			<td class="CommonFormField">
				<cp:statusdropdownlist id="DefaultThreadStatusValue" runat="server" cssclass="txt1" />
			</td>
		</tr>
		</table>
		</Content>
	</TWC:TabbedPane>
	<TWC:TabbedPane runat="server">
		<Tab><CP:ResourceControl runat="server" ResourceName="CP_Forums_SectionEdit_Tab_UserPermissions" /></Tab>
		<Content>
		    <div id="GrayGrid">
                <asp:Repeater runat="server" ID="PermissionList">
                    <HeaderTemplate>
                        <table class="Grid" cellSpacing=0 cellPadding=0 width="100%">
                        <tbody>
                            <tr class="HeadingRow HeadingCellText HeadingCell">
                                <th><CP:ResourceControl runat="server" ResourceName="CP_Forums_GridCol_Name" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_View" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Read" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Post" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Reply" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Vote" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Ink" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Video" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_LocalAttachment" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_RemoteAttachment" /></th>
                            </tr>    
                    </HeaderTemplate>
                    <ItemTemplate>
                            <tr class="Row">
                                <td><%# Eval("Name") %><asp:HiddenField ID="RoleId" runat="server" Value='<%# Eval("RoleIDString") %>' Visible="false" /></td>
                                <td><asp:CheckBox runat="server" ID="ViewCheckbox" Checked='<%# Eval("View") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="ReadCheckbox" Checked='<%# Eval("Read") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="PostCheckbox" Checked='<%# Eval("Post") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="ReplyCheckbox" Checked='<%# Eval("Reply") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="VoteCheckbox" Checked='<%# Eval("Vote") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="InkCheckbox" Checked='<%# Eval("Ink") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="VideoCheckbox" Checked='<%# Eval("Video") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="LocalAttachmentCheckbox" Checked='<%# Eval("LocalAttachment") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="RemoteAttachmentCheckbox" Checked='<%# Eval("RemoteAttachment") %>' /></td>
                            </tr>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                            <tr class="AlternatingRow">
                                <td><%# Eval("Name") %><asp:HiddenField ID="RoleId" runat="server" Value='<%# Eval("RoleIDString") %>' Visible="false" /></td>
                                <td><asp:CheckBox runat="server" ID="ViewCheckbox" Checked='<%# Eval("View") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="ReadCheckbox" Checked='<%# Eval("Read") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="PostCheckbox" Checked='<%# Eval("Post") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="ReplyCheckbox" Checked='<%# Eval("Reply") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="VoteCheckbox" Checked='<%# Eval("Vote") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="InkCheckbox" Checked='<%# Eval("Ink") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="VideoCheckbox" Checked='<%# Eval("Video") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="LocalAttachmentCheckbox" Checked='<%# Eval("LocalAttachment") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="RemoteAttachmentCheckbox" Checked='<%# Eval("RemoteAttachment") %>' /></td>
                            </tr>
                    </AlternatingItemTemplate>
                    <FooterTemplate>
                        </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
		</Content>
	</TWC:TabbedPane>
	<TWC:TabbedPane runat="server">
		<Tab><CP:ResourceControl runat="server" ResourceName="CP_Forums_SectionEdit_Tab_AdminPermissions" /></Tab>
		<Content>
		    <div id="GrayGrid">
                <asp:Repeater runat="server" ID="AdminPermissionList">
                    <HeaderTemplate>
                        <table class="Grid" cellSpacing=0 cellPadding=0 width="100%">
                        <tbody>
                            <tr class="HeadingRow HeadingCellText HeadingCell">
                                <th><CP:ResourceControl runat="server" ResourceName="CP_Forums_GridCol_Name" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Edit" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Delete" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_CreatePoll" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_MarkAsAnswer" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Sticky" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Announcement" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_EditOthers" /></th>
                                <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Moderate" /></th>
                            </tr>    
                    </HeaderTemplate>
                    <ItemTemplate>
                            <tr class="Row">
                                <td><%# Eval("Name") %><asp:HiddenField ID="RoleId" runat="server" Value='<%# Eval("RoleIDString") %>' Visible="false" /></td>
                                <td><asp:CheckBox runat="server" ID="EditCheckbox" Checked='<%# Eval("Edit") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="DeleteCheckbox" Checked='<%# Eval("Delete") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="CreatePollCheckbox" Checked='<%# Eval("CreatePoll") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="MarkAsAnswerCheckbox" Checked='<%# Eval("MarkAsAnswer") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="StickyCheckbox" Checked='<%# Eval("Sticky") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="AnnouncementCheckbox" Checked='<%# Eval("Announcement") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="EditOthersCheckbox" Checked='<%# Eval("EditOthers") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="ModerateCheckbox" Checked='<%# Eval("Moderate") %>' /></td>
                            </tr>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                            <tr class="AlternatingRow">
                                <td><%# Eval("Name") %><asp:HiddenField ID="RoleId" runat="server" Value='<%# Eval("RoleIDString") %>' Visible="false" /></td>
                                <td><asp:CheckBox runat="server" ID="EditCheckbox" Checked='<%# Eval("Edit") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="DeleteCheckbox" Checked='<%# Eval("Delete") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="CreatePollCheckbox" Checked='<%# Eval("CreatePoll") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="MarkAsAnswerCheckbox" Checked='<%# Eval("MarkAsAnswer") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="StickyCheckbox" Checked='<%# Eval("Sticky") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="AnnouncementCheckbox" Checked='<%# Eval("Announcement") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="EditOthersCheckbox" Checked='<%# Eval("EditOthers") %>' /></td>
                                <td><asp:CheckBox runat="server" ID="ModerateCheckbox" Checked='<%# Eval("Moderate") %>' /></td>
                            </tr>
                    </AlternatingItemTemplate>
                    <FooterTemplate>
                        </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
		</Content>
	</TWC:TabbedPane>
	<TWC:TabbedPane runat="server" id="MailingList">
		<Tab><CP:ResourceControl runat="server" ResourceName="CP_Forums_SectionEdit_Tab_MailingList" /></Tab>
		<Content>
		<table cellspacing="0" cellpadding="2" border="0">
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon id="Helpicon17" runat="Server" resourcename="CP_Forums_SectionEdit_LE_EnableMailingList_Detail"></cp:helpicon>
				<cp:resourcelabel id="Resourcelabel9" runat="server" resourcename="CP_Forums_SectionEdit_LE_EnableMailingList"></cp:resourcelabel></td>
			<td class="CommonFormField">
				<cp:yesnoradiobuttonlist id="LEEnableMailingList" runat="server" cssclass="txt1" repeatcolumns="2"></cp:yesnoradiobuttonlist>
			</td>
		</tr>
		<tr>
			<td class="CommonFormFieldName" nowrap="nowrap">
				<cp:helpicon id="Helpicon20" runat="Server" resourcename="CP_Forums_SectionEdit_LE_ListName_Detail"></cp:helpicon>
				<cp:resourcelabel id="Resourcelabel15" runat="server" resourcename="CP_Forums_SectionEdit_LE_ListName"></cp:resourcelabel></td>
			<td class="CommonFormField">
				<asp:textbox id="LEListName" runat="server" columns="50" maxlength="256"></asp:textbox>
			</td>
		</tr>
		<tr>
			<td class="CommonFormFieldName" nowrap="nowrap">
				<cp:helpicon id="Helpicon18" runat="Server" resourcename="CP_Forums_SectionEdit_LE_EmailAddress_Detail"></cp:helpicon>
				<cp:resourcelabel id="Resourcelabel11" runat="server" resourcename="CP_Forums_SectionEdit_LE_EmailAddress"></cp:resourcelabel></td>
			<td class="CommonFormField">
				<asp:textbox id="LEEmailAddress" runat="server" columns="20" maxlength="256"></asp:textbox>
				<asp:Literal ID="LEEmailDomain" Runat="server" />
				<asp:RegularExpressionValidator id="emailRegExValidator" runat="server" ControlToValidate="LEEmailAddress" Cssclass="validationWarning" ValidationExpression="\w+([-+.]\w+)*">*</asp:RegularExpressionValidator>
				<asp:customvalidator id="LEEmailValidator" runat="server" controltovalidate="LEEmailAddress" font-bold="True"
					errormessage="*"></asp:customvalidator>
			</td>
		</tr>
		<tr>
			<td class="CommonFormFieldName" valign="top">
				<cp:helpicon id="Helpicon19" runat="Server" resourcename="CP_Forums_SectionEdit_LE_FooterMessage_Detail"></cp:helpicon>
				<cp:resourcelabel id="Resourcelabel13" runat="server" resourcename="CP_Forums_SectionEdit_LE_FooterMessage"></cp:resourcelabel></td>
			<td class="CommonFormField">
				<asp:textbox id="LEFooterMessage" runat="server" columns="50" maxlength="1000" textmode="MultiLine" rows="3"></asp:textbox>
			</td>
		</tr>
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon id="Helpicon21" runat="Server" resourcename="CP_Forums_SectionEdit_LE_PassiveMode_Detail"></cp:helpicon>
				<cp:resourcelabel id="Resourcelabel17" runat="server" resourcename="CP_Forums_SectionEdit_LE_PassiveMode"></cp:resourcelabel></td>
			<td class="CommonFormField">
				<cp:yesnoradiobuttonlist id="LEPassiveMode" runat="server" cssclass="txt1" repeatcolumns="2"></cp:yesnoradiobuttonlist></td>
		</tr>
		<tr>
			<td class="CommonFormFieldName" nowrap="nowrap">
				<cp:helpicon id="Helpicon22" runat="Server" resourcename="CP_Forums_SectionEdit_LE_PassiveModeEmail_Detail"></cp:helpicon>
				<cp:resourcelabel id="Resourcelabel19" runat="server" resourcename="CP_Forums_SectionEdit_LE_PassiveModeEmail"></cp:resourcelabel></td>
			<td class="CommonFormField">
				<asp:textbox id="LEPassiveModeAddress" runat="server" columns="50" maxlength="256"></asp:textbox>
				<asp:RegularExpressionValidator id="Regularexpressionvalidator1" runat="server" ControlToValidate="LEPassiveModeAddress" Cssclass="validationWarning" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
			</td>
		</tr>
		</table>
		</Content>
	</TWC:TabbedPane>
</TWC:TabbedPanes>

<%@ Import Namespace="CommunityServer.Components" %>
<%@ Register TagPrefix="CP" TagName = "GroupEditControl" Src = "~/ControlPanel/Forums/GroupEditControl.ascx" %>
<%@ Register TagPrefix="CP" TagName = "SectionEditControl" Src = "~/ControlPanel/Forums/SectionEditControl.ascx" %>
<%@ Page language="c#" Codebehind="IFrameHost.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Forums.ForumsIFrameHost" %>
<CP:Container id="MPContainer" ismodal="false" thememasterfile="ControlPanelModalMaster.ascx" runat="server">
<CP:Content id="bcr" runat="server">
	<div class="CommonContentArea">
		<div class="CommonContent">
	<cp:statusmessage id="Status" runat="server"></cp:statusmessage>
	<cp:sectioneditcontrol runat="server" id="SectionEdit1" visible="false" />
	<cp:groupeditcontrol runat="server" id="GroupEdit1" visible="false" />
			<div class="CommonFormField PanelSaveButton">
				<cp:resourcelinkbutton id="SaveButton" runat="server" cssclass="CommonTextButton" resourcename="Save" />
				<cp:resourcelinkbutton runat="server" id="DeleteButton" cssclass="CommonTextButton" causesvalidation="false" resourcename="Delete"></cp:resourcelinkbutton>
			</div>
		</div>
	</div>
</CP:Content>
</CP:Container>

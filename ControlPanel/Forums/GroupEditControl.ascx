<%@ Control language="c#" Codebehind="GroupEditControl.ascx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Forums.GroupEditControl" %>
<%@ Import Namespace="CommunityServer.Discussions.Components" %>
<div class="CommonFormArea">
<cp:statusmessage id="Status" runat="server"></cp:statusmessage>
<div class="CommonFormFieldName">
	<cp:resourcelabel runat="server" resourcename="CP_Forums_GroupEdit_ForumGroupName" id="Resourcelabel3"/>
</div>
<div class="CommonFormField">
	<asp:textbox id="ForumGroupName" runat="server"  CssClass="ControlPanelTextInput" MaxLength="256"></asp:textbox>
	<asp:requiredfieldvalidator runat="server" errormessage="*" controltovalidate="ForumGroupName" id="ForumGroupNameValidator" />
</div>
<p />
<div class="CommonFormFieldName">
		<cp:resourcelabel runat="server" resourcename="CP_Forums_GroupEdit_ForumGroupDesc" id="Resourcelabel4"/>
</div>
<div class="CommonFormField">
		<asp:textbox id="ForumGroupDesc" runat="server" CssClass="ControlPanelTextInput" MaxLength="256"  Rows="8" TextMode="MultiLine"></asp:textbox>
</div>
</div>
<%@ Page language="c#" Codebehind="PostOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Forums.PostOptions" %>
<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server"><CP:ResourceControl runat="Server" resourcename="CP_ForumsAdmin_PostOptions_Title" ID="Resourcecontrol2"/></CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
	<div class="CommonDescription">
		<CP:ResourceControl runat="Server" resourcename="CP_ForumsAdmin_PostOptions_SubTitle" ID="Resourcecontrol3"/>
	</div>
		<CP:FileOnlyStatusMessage id="FOStatus" runat="server" visible="false"></CP:FileOnlyStatusMessage>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Forums" />
		<asp:PlaceHolder id="OptionHolder" runat="Server">
			<cp:statusmessage id="Status" runat="server"></cp:statusmessage>
			<DIV class="CommonDescription"></DIV>
			<TWC:TabbedPanes id="EditorTabs" runat="server"
				PanesCssClass="CommonPane"
				TabSetCssClass="CommonPaneTabSet"
				TabCssClasses="CommonPaneTab,CommonPaneTab1,CommonPaneTab2"
				TabSelectedCssClasses="CommonPaneTabSelected,CommonPaneTabSelected1,CommonPaneTabSelected2"
				TabHoverCssClasses="CommonPaneTabHover,CommonPaneTabHover1,CommonPaneTabHover2"
				>
				<TWC:TabbedPane runat="server">
					<Tab><CP:ResourceControl runat="server" ResourceName="CP_ForumsAdmin_PostOptions_Tab_General" /></Tab>
					<Content>
						<h3 class="CommonSubTitle"><CP:ResourceControl runat="Server" resourcename="CP_ForumsAdmin_PostOptions_GeneralSettings" ID="Resourcecontrol4"/></h3>
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<td class="CommonFormFieldName">
									<cp:helpicon id="Helpicon2" runat="Server" resourcename="Admin_SiteSettings_RSS_Feeds_Descr" />
									<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_RSS_Feeds" />
								</td>
								<td class="CommonFormField" nowrap="nowrap">
									<cp:yesnoradiobuttonlist id="EnableForumsRSS" runat="server" cssclass="ControlPanelTextInput" repeatcolumns="2"></cp:yesnoradiobuttonlist>
								</td>
							</tr>
							<tr>
								<td class="CommonFormFieldName">
									<cp:helpicon id="Helpicon13" runat="Server" resourcename="Admin_SiteSettings_ThreadStatus_Allow_Descr" />
									<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_ThreadStatus_Allow" />
								</td>
								<td class="CommonFormField" nowrap="nowrap">
									<cp:yesnoradiobuttonlist id="EnableThreadStatus" runat="server" cssclass="ControlPanelTextInput" repeatcolumns="2"></cp:yesnoradiobuttonlist></td>
							</tr>
							<tr>
								<td class="CommonFormFieldName">
									<cp:helpicon id="Helpicon12" runat="Server" resourcename="CP_Settings_General_EnablePostPreviewPopup_Descr"></cp:helpicon>
									<cp:resourcecontrol runat="server" resourcename="CP_Settings_General_EnablePostPreviewPopup" /></td>
								<td class="CommonFormField" nowrap="nowrap">
									<cp:yesnoradiobuttonlist id="EnablePostPreviewPopup" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput"></cp:yesnoradiobuttonlist></td>
							</tr>
							
				            <TR>
					            <TD class="CommonFormFieldName">
						            <cp:helpicon id="Helpicon8" runat="Server" resourcename="Admin_SiteSettings_UserAsAnon_Allow_Descr"></cp:helpicon>
						            <cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_UserAsAnon_Allow"></cp:resourcecontrol></TD>
					            <TD class="CommonFormField" noWrap>
						            <cp:yesnoradiobuttonlist id="PostAsAnonymousUser" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput"></cp:yesnoradiobuttonlist></TD>
				            </TR>		
				            
				             <TR>
					            <TD class="CommonFormFieldName">
						            <cp:helpicon id="Helpicon11" runat="Server" resourcename="Admin_SiteSettings_Tagging_Allow_Descr"></cp:helpicon>
						            <cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Tagging_Allow"></cp:resourcecontrol></TD>
					            <TD class="CommonFormField" noWrap>
						            <cp:yesnoradiobuttonlist id="Tagging" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput"></cp:yesnoradiobuttonlist></TD>
				            </TR>							
							
							<tr>
								<td class="CommonFormFieldName">
									<cp:helpicon id="Helpicon26" runat="Server" resourcename="CP_Settings_RSS_Threads_Descr" />
									<cp:resourcecontrol runat="server" resourcename="CP_Settings_RSS_Threads" />
								</td>
								<td class="CommonFormField" nowrap="nowrap">
									<asp:textbox id="RSSDefaultThreadsPerFeed" runat="server" maxlength="3"></asp:textbox>
									<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="RSSDefaultThreadsPerFeed"
										id="RSSDefaultThreadsPerFeedValidator" />
								</td>
							</tr>
							<tr>
								<td class="CommonFormFieldName">
									<cp:helpicon id="Helpicon6" runat="Server" resourcename="Admin_SiteSettings_ThreadsPerPage_Descr" />
									<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_ThreadsPerPage" />
								</td>
								<td class="CommonFormField">
									<asp:textbox id="ThreadsPerPage" runat="Server" maxlength="4"></asp:textbox>
									<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="ThreadsPerPage"
										id="ThreadsPerPageValidator" />
								</td>
							</tr>
							<tr>
								<td class="CommonFormFieldName">
									<cp:helpicon id="Helpicon7" runat="Server" resourcename="Admin_SiteSettings_PostPerPage_Descr" />
									<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_PostPerPage" />
								</td>
								<td class="CommonFormField">
									<asp:textbox id="PostsPerPage" runat="Server" maxlength="4"></asp:textbox>
									<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="PostsPerPage"
										id="PostsPerPageValidator" />
								</td>
							</tr>
							<tr>
								<td class="CommonFormFieldName">
									<cp:helpicon id="Helpicon10" runat="Server" resourcename="Admin_SiteSettings_DaysPostMarkedAsRead_Descr" />
									<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_DaysPostMarkedAsRead" />
								</td>
								<td class="CommonFormField">
									<asp:textbox id="DaysPostMarkedAsRead" runat="Server"></asp:textbox>
									<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="DaysPostMarkedAsRead"
										id="DaysPostMarkedAsReadValidator" />
								</td>
							</tr>
						</table>
					</Content>
				</TWC:TabbedPane>
				<TWC:TabbedPane runat="server">
					<Tab><CP:ResourceControl runat="server" ResourceName="CP_ForumsAdmin_PostOptions_Tab_Editing" /></Tab>
					<Content>
						<h3 class="CommonSubTitle"><CP:ResourceControl runat="Server" resourcename="CP_ForumsAdmin_PostOptions_PostEditing" /></h3>
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<td class="CommonFormFieldName">
									<cp:helpicon id="Helpicon9" runat="Server" resourcename="Admin_SiteSettings_Posting_RequireEditNotes_Descr" />
									<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Posting_RequireEditNotes" />
								</td>
								<td class="CommonFormField">
									<cp:yesnoradiobuttonlist id="RequireEditNotes" runat="server" cssclass="ControlPanelTextInput" repeatcolumns="2"></cp:yesnoradiobuttonlist></td>
							</tr>
							<tr>
								<td class="CommonFormFieldName">
									<cp:helpicon id="Helpicon22" runat="Server" resourcename="Admin_SiteSettings_Editing_Notes_Descr" />
									<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Editing_Notes" />
								</td>
								<td class="CommonFormField" nowrap="nowrap">
									<cp:yesnoradiobuttonlist id="DisplayEditNotes" runat="server" cssclass="ControlPanelTextInput" repeatcolumns="2"></cp:yesnoradiobuttonlist></td>
							</tr>
							<tr>
								<td class="CommonFormFieldName">
									<cp:helpicon id="Helpicon23" runat="Server" resourcename="Admin_SiteSettings_Editing_Age_Descr" />
									<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Editing_Age" />
								</td>
								<td class="CommonFormField">
									<asp:textbox id="PostEditBodyAgeInMinutes" runat="server"></asp:textbox>
									<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="PostEditBodyAgeInMinutes"
										id="PostEditBodyAgeInMinutesValidator" />
								</td>
							</tr>
						</table>
					</Content>
				</TWC:TabbedPane>
				<TWC:TabbedPane runat="server">
					<Tab><CP:ResourceControl runat="server" ResourceName="CP_ForumsAdmin_PostOptions_Tab_Attachments" /></Tab>
					<Content>
						<h3 class="CommonSubTitle"><CP:ResourceControl runat="Server" resourcename="CP_ForumsAdmin_PostOptions_PostAttachments" /></h3>
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<td class="CommonFormFieldName">
									<cp:helpicon id="Helpicon15" runat="Server" resourcename="Admin_SiteSettings_Attachments_Allow_Desc" />
									<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Attachments_Allow" />
								</td>
								<td class="CommonFormField" nowrap="nowrap">
									<cp:yesnoradiobuttonlist id="EnableAttachments" runat="server" cssclass="ControlPanelTextInput" repeatcolumns="2"></cp:yesnoradiobuttonlist></td>
							</tr>
							<tr>
								<td class="CommonFormFieldName">
									<cp:helpicon id="Helpicon17" runat="Server" resourcename="Admin_SiteSettings_Attachments_Types_Desc" />
									<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Attachments_Types" />
								</td>
								<td class="CommonFormField">
									<asp:textbox id="AllowedAttachmentTypes" runat="server"></asp:textbox></td>
							</tr>
							<tr>
								<td class="CommonFormFieldName">
									<cp:helpicon id="Helpicon18" runat="Server" resourcename="Admin_SiteSettings_Attachments_MaxSize_Desc" />
									<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Attachments_MaxSize" />
								</td>
								<td class="CommonFormField">
									<asp:textbox id="MaxAttachmentSize" runat="server"></asp:textbox>
									<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="MaxAttachmentSize"
										id="MaxAttachmentSizeValidator" />
								</td>
							</tr>
							<tr>
								<td class="CommonFormFieldName">
									<cp:helpicon id="Helpicon19" runat="Server" resourcename="Admin_SiteSettings_Attachments_EnableInlinedImages_Desc" />
									<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Attachments_EnableInlinedImages" />
								</td>
								<td class="CommonFormField">
									<cp:yesnoradiobuttonlist id="EnableInlinedImages" runat="server" cssclass="ControlPanelTextInput" repeatcolumns="2"></cp:yesnoradiobuttonlist>
								</td>
							</tr>
							<tr>
								<td class="CommonFormFieldName">
									<cp:helpicon id="Helpicon20" runat="Server" resourcename="Admin_SiteSettings_Attachments_EnableInlinedImages_MaxSize_Desc" />
									<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Attachments_EnableInlinedImages_MaxSize" />
								</td>
								<td class="CommonFormField">
									<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Attachments_EnableInlinedImages_Width" />
									<asp:textbox id="InlinedImageWidth" runat="server" maxlength="4" columns="4" />
									<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="InlinedImageWidth"
										id="InlinedImageWidthValidator" />
									&nbsp;
									<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Attachments_EnableInlinedImages_Height" />
									<asp:textbox id="InlinedImageHeight" runat="server" maxlength="4" columns="4" />
								</td>
							</tr>
							<tr>
								<td class="CommonFormFieldName">
									<cp:helpicon id="Helpicon21" runat="Server" resourcename="Admin_SiteSettings_Attachments_EnableInlinedImages_ImageTypes_Desc" />
									<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Attachments_EnableInlinedImages_ImageTypes" />
								</td>
								<td class="CommonFormField">
									<asp:textbox id="SupportedInlinedImageTypes" runat="server" columns="55"></asp:textbox></td>
							</tr>
						</table>
					</Content>
				</TWC:TabbedPane>
				<TWC:TabbedPane runat="server">
					<Tab><CP:ResourceControl runat="server" ResourceName="CP_ForumsAdmin_PostOptions_Tab_DuplicateFlooding" /></Tab>
					<Content>
					<div class="CommonGroupedContentArea">
						<h3 class="CommonSubTitle"><CP:ResourceControl runat="Server" resourcename="CP_ForumsAdmin_PostOptions_DuplicatePosts" /></h3>
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<td class="CommonFormFieldName">
									<cp:helpicon id="Helpicon16" runat="Server" resourcename="Admin_SiteSettings_Posting_Dups_Descr" />
									<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Posting_Dups" />
								</td>
								<td class="CommonFormField" nowrap="nowrap">
									<cp:yesnoradiobuttonlist id="AllowDuplicatePosts" runat="server" cssclass="ControlPanelTextInput" repeatcolumns="2"></cp:yesnoradiobuttonlist></td>
							</tr>
							<tr>
								<td class="CommonFormFieldName">
									<cp:helpicon id="Helpicon1" runat="Server" resourcename="Admin_SiteSettings_Posting_DupsInterval_Descr" />
									<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Posting_DupsInterval" />
								</td>
								<td class="CommonFormField" nowrap="nowrap">
									<asp:textbox id="DuplicatePostIntervalInMinutes" runat="Server"></asp:textbox>
									<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="DuplicatePostIntervalInMinutes"
										id="DuplicatePostIntervalInMinutesValidator" />
								</td>
							</tr>
						</table>
					</div>
						<h3 class="CommonSubTitle"><CP:ResourceControl runat="Server" resourcename="CP_ForumsAdmin_PostOptions_PostFlooding" /></h3>
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<td class="CommonFormFieldName">
									<cp:helpicon id="Helpicon24" runat="Server" resourcename="Admin_SiteSettings_Flooding_Check_Descr" />
									<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Flooding_Check" />
								</td>
								<td class="CommonFormField" nowrap="nowrap">
									<cp:yesnoradiobuttonlist id="EnableFloodInterval" runat="server" cssclass="ControlPanelTextInput" repeatcolumns="2"></cp:yesnoradiobuttonlist></td>
							</tr>
							<tr>
								<td class="CommonFormFieldName">
									<cp:helpicon id="Helpicon25" runat="Server" resourcename="Admin_SiteSettings_Flooding_Time_Descr" />
									<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Flooding_Time" />
								</td>
								<td class="CommonFormField">
									<asp:textbox id="PostInterval" runat="Server"></asp:textbox>
									<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="PostInterval"
										id="PostIntervalValidator" />
								</td>
							</tr>
						</table>
					</Content>
				</TWC:TabbedPane>
				<TWC:TabbedPane runat="server">
					<Tab><CP:ResourceControl runat="server" ResourceName="CP_ForumsAdmin_PostOptions_Tab_PopularPosts" /></Tab>
					<Content>
						<h3 class="CommonSubTitle"><CP:ResourceControl runat="Server" resourcename="CP_ForumsAdmin_PostOptions_PopularCriteria" /></h3>
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<td class="CommonFormFieldName">
									<cp:helpicon id="Helpicon3" runat="Server" resourcename="Admin_SiteSettings_Posting_Popular_Descr" />
									<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Posting_Popular" />
								</td>
								<td class="CommonFormField">
									<asp:textbox id="PopularPostThresholdPosts" runat="Server"></asp:textbox>
									<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="PopularPostThresholdPosts"
										id="PopularPostThresholdPostsValidator" />
								</td>
							</tr>
							<tr>
								<td class="CommonFormFieldName">
									<cp:helpicon id="Helpicon4" runat="Server" resourcename="Admin_SiteSettings_Posting_Popular_View_Descr" />
									<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Posting_Popular_View" />
								</td>
								<td class="CommonFormField">
									<asp:textbox id="PopularPostThresholdViews" runat="Server"></asp:textbox>
									<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="PopularPostThresholdViews"
										id="PopularPostThresholdViewsValidator" />
								</td>
							</tr>
							<tr>
								<td class="CommonFormFieldName">
									<cp:helpicon id="Helpicon5" runat="Server" resourcename="Admin_SiteSettings_Posting_Popular_Age_Descr" />
									<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Posting_Popular_Age" />
								</td>
								<td class="CommonFormField">
									<asp:textbox id="PopularPostThresholdDays" runat="Server"></asp:textbox>
									<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="PopularPostThresholdDays"
										id="PopularPostThresholdDaysValidator" />
								</td>
							</tr>
						</table>
					</Content>
				</TWC:TabbedPane>
			</TWC:TabbedPanes>
			<p class="PanelSaveButton DetailsFixedWidth">
				<cp:ResourceLinkButton id="SaveButton" runat="server" cssclass="CommonTextButton" resourcename="Save"></cp:ResourceLinkButton></p>
		</asp:PlaceHolder>
	</CP:Content>
</CP:Container>

<%@ Control Language="c#" AutoEventWireup="false" enableviewstate="false" Codebehind="ForumTreeViewControl.ascx.cs" Inherits="CommunityServer.ControlPanel.Forums.ForumTreeViewControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Import Namespace="CommunityServer.Components" %>
<script type="text/javascript">
// <![CDATA[
var nodeMoved = null;
var nodeNeedsResorted = false;
var newNodeSeq = 0;
var newNodeGroupID = 0;

// Splits the node's value and returns the GroupID part
function getGroupID(node)
{
	splits = node.GetProperty('Value').split(":");
	return splits[0];
}

// Splits the node's value and returns the SectionID part
function getSectionID(node)
{
	splits = node.GetProperty('Value').split(":");
	return splits[1];
}

// Splits the PARENT node's value and returns the SectionID part
function getParentSectionID(node)
{
	splits = node.ParentNode.Value.split(":");
	return splits[1];
}

function treeContextMenu(treeNode, e)
{
	groupID = getGroupID(treeNode);
	sectionID = getSectionID(treeNode);
	
	// Determine whether to call Group or Section context menu
	if (sectionID > 0)
	{
		if(treeNode.Checked == 1) //used for enabled
			window.<%=this.SectionContextMenu.ClientID%>.ShowContextMenu(treeNode, e, groupID, sectionID);
		else
			window.<%=this.SectionDisabledContextMenu.ClientID%>.ShowContextMenu(treeNode, e, groupID, sectionID);
	}
	else
		window.<%=this.GroupContextMenu.ClientID%>.ShowContextMenu(treeNode, e, groupID, sectionID);
		
	return true;
}

function nodeSelect(node)
{
	if (getSectionID(node) < 0)
	{
		window.frames["sectionEditFrame"].location.href = "IFrameHost.aspx?SectionID=0&GroupID=" + getGroupID(node);
	}
	else
	{
		window.frames["sectionEditFrame"].location.href = "IFrameHost.aspx?SectionID=" + getSectionID(node) + "&GroupID=" + getGroupID(node);
	}
	node.Expand();
	node.ParentTreeView.Render();
}

function checkNodeIndex(node)
{
	if (nodeNeedsResorted == true && nodeMoved != null)
	{
		nodeNeedsResorted = false;
		index = nodeMoved.GetCurrentIndex();
		groupID = getGroupID(nodeMoved);
		sectionID = getSectionID(nodeMoved);
		nodeMoved = null;
		
		ReorderForumOrGroup(groupID, sectionID, index);
	}
}

function updateGroupNode(groupID, newName)
{
	var updateNode = window.<%=this.Tree.ClientObjectId%>.FindNodeById(groupID + ":-1");
	if(updateNode == null)
		window.location = window.location;
		
	updateNode.SetProperty('Text',newName);
	window.<%=this.Tree.ClientObjectId%>.Render();
}

function updateSectionNode(groupID, sectionID, newName, isActive)
{
	var updateNode = window.<%=this.Tree.ClientObjectId%>.FindNodeById(groupID + ":" + sectionID);
	if(updateNode == null)
		window.location = window.location;
	
	if(isActive == 'True') //used for enabled
	{
		updateNode.SetProperty('ImageUrl', "notes.gif");
		updateNode.SetProperty('Checked', 1);
	}
	else
	{
		updateNode.SetProperty('ImageUrl', "notesdisabled.gif");
		updateNode.SetProperty('Checked', null);
	}
	
	updateNode.SetProperty('Text',newName);
	window.<%=this.Tree.ClientObjectId%>.Render();
}



//## MOVE / REORDER ##

//## MOVE ##
function nodeMove(sourceNode, targetNode)
{
	nodeMoved = sourceNode;
	nodeNeedsResorted = false;

	sourceSectionID = getSectionID(sourceNode);
	sourceGroupID = getGroupID(sourceNode);
	targetGroupID = getGroupID(targetNode);
	targetSectionID = getSectionID(targetNode);
	if (sourceNode.ParentNode != null)
		parentSectionID = getParentSectionID(sourceNode);
	else
		parentSectionID = -1;
		
	// alert(sourceGroupID + ':' + sourceSectionID + ':' + targetGroupID + ':' + targetSectionID);
	
	// Check if user is trying to drop a group inside a group or forum
	if (sourceSectionID < 0)
	{
		if (targetGroupID > 0)
		{
			//dragging a group into a group or section
			alert("<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_NoDropGroup") %>");
			return false;
		}
		nodeNeedsResorted = true;
	}


	// Determine if the user moved the Forum to another group and/or parent forum...
	//	or if this move was just a change in sort order in the same group & parent forum
	var isReSort = false;
	if (sourceGroupID == targetGroupID)
	{
		if (parentSectionID > 0)
		{
			if (parentSectionID == targetSectionID)
			{
				// This is just a change in sort order. We can't do anything until we know
				//	the new index that the node was dropped in. So let client-side function know this...
				isReSort = true;
			}
		}
		else
		{
			if (targetSectionID < 0)
			{
				// This is just a change in sort order. We can't do anything until we know
				//	the new index that the node was dropped in. So let client-side function know this...
				isReSort = true;
			}
		}
	}

	if (isReSort)
	{
		nodeNeedsResorted = true;
	}
	else
	{
		if (sourceSectionID > 0)
		{
			if (targetNode == null || targetNode.ID == "RootNode")
			{
				alert('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_NodeMoveError") %>');
				return false;
			}

			window.frames["sectionEditFrame"].location.href = "<%= SiteUrls.Instance().ControlPanelLoading(CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_MoveNodeWait")) %>";
			MoveSection(sourceGroupID, sourceSectionID, targetGroupID, targetSectionID);
		}
	}
	
	return true; 
}

function MoveSection(groupID ,sectionID, targetGroupID, targetSectionID)
{
	ForumTreeViewControl.MoveForum('<%= this.ClientID %>',groupID ,sectionID, targetGroupID, targetSectionID, MoveSectionCallBack);
}

function MoveSectionCallBack(res)
{
	window.frames["sectionEditFrame"].location.href = "<%= SiteUrls.Instance().BlankPage %>";

	if (res == null)
		return;

	// Check if this was now alloed
	if (res.value == "0")
	{
		//dragging a group into a group or section
		alert("<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_NoMoveSpecialForum") %>");
		return false;
	}
	else
	{
		var retVals = res.value.split("^");
		var groupID = retVals[0];
		var newGroupID = retVals[1];
		var sectionID = retVals[2];
		var newSectionID = retVals[3];
		
		nodeMoved = window.<%=this.Tree.ClientObjectId%>.FindNodeById(groupID + ":" + sectionID);
		nodeMoved.SetProperty('Value', newGroupID + ":" + sectionID);
		nodeMoved.SetProperty('ID', newGroupID + ":" + sectionID);
		window.<%=this.Tree.ClientObjectId%>.Render();
		
		var selectedNode = window.<%=this.Tree.ClientObjectId%>.SelectedNode;
		if (selectedNode != null && getSectionID(selectedNode) == sectionID)
		{
			nodeSelect(nodeMoved);
		}
	}
}
//## END MOVE ##

//## REORDER ##
function ReorderForumOrGroup(groupID, sectionID, index)
{
	window.frames["sectionEditFrame"].location.href = "<%= SiteUrls.Instance().ControlPanelLoading(CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_MoveNodeWait")) %>";
	ForumTreeViewControl.ReorderForumOrGroup('<%= this.ClientID %>', groupID, sectionID, index, ReOrderCallBack);
}
function ReOrderCallBack(res)
{
	window.frames["sectionEditFrame"].location.href = "<%= SiteUrls.Instance().BlankPage %>";
}
//## END REORDER ##

//## END MOVE / REORDER ##


//## RENAME ##
function nodeRename(sourceNode, newName)
{ 
	newName = newName.replace(/^\s*|\s*$/g,"");
	if(newName.length == 0)
	{
		alert('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_BlankGroupName") %>');
		return false;
	}

	window.<%=this.Tree.ClientObjectId%>.SelectedNode.SetProperty('Text', newName);
	
	// Determine whether this is a new node (doesn't exist server-side yet) or an existing one
	var groupID = getGroupID(sourceNode);
	var sectionID = getSectionID(sourceNode);
	var parentSectionID = getParentSectionID(sourceNode);
	
	if (sectionID > 0)
	{
		window.location = window.location;
	}
	if (sectionID == 0)
	{
		window.<%=this.Tree.ClientObjectId%>.Render();
		window.frames["sectionEditFrame"].location.href = "<%= SiteUrls.Instance().ControlPanelLoading(CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_AddSectionWait")) %>";
		ForumTreeViewControl.AddSection('<%= this.ClientID %>', groupID, parentSectionID, newName, AddSectionCallBack);
		return false;
	}
	if (groupID > 0)
	{
		// Existing Node - just rename
		RenameGroup(groupID, newName);
	}
	return true; 
}

function RenameGroup(groupID, newName)
{
	ForumTreeViewControl.RenameGroup('<%= this.ClientID %>', groupID, newName, RenameGroupCallBack);
}

function RenameGroupCallBack(res)
{
	if (res == null)
		window.location = window.location;

	// Reload group edit control
	groupID = res.value;
	if (groupID > 0)
	{
		window.frames["sectionEditFrame"].location.href = "IFrameHost.aspx?SectionID=0&GroupID=" + groupID;
	}
	else
	{
		alert('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_DuplicateGroupName") %>');
		window.location = window.location;
	}
}

//## END RENAME ##


//## ADD ##

//## ADD GROUP ##
function AddGroup()
{
	toggleButtons();
	window.frames["sectionEditFrame"].location.href = "<%= SiteUrls.Instance().ControlPanelLoading(CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_AddGroupWait")) %>";
	ForumTreeViewControl.AddGroup('<%= this.ClientID %>', AddGroupCallBack);
}
       
function AddGroupCallBack(res)
{
	if (res == null)
		window.location = window.location;

	var retVals = res.value.split("^");
	var groupID = retVals[0];
	var groupName = retVals[1];

	if (groupID == '-1')
	{
		alert('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_DuplicateGroupName") %>');
		window.frames["sectionEditFrame"].location.href = "<%= SiteUrls.Instance().BlankPage %>";
	}
	else
	{
		AddGroupNode(groupID, groupName)
		window.frames["sectionEditFrame"].location.href = "IFrameHost.aspx?SectionID=0&GroupID=" + groupID;
	}
	toggleButtons();
	return;
}

function AddGroupNode(groupID, groupName)
{
	window.<%=this.Tree.ClientObjectId%>.SelectedNode = null;
	var newGroupNode = new ComponentArt_TreeViewNode();

	newGroupNode.SetProperty('Text', groupName);
	newGroupNode.SetProperty('ImageUrl', "folders.gif");
	newGroupNode.SetProperty('EditingEnabled', true);
	newGroupNode.SetProperty('DraggingEnabled', true);
	newGroupNode.SetProperty('DroppingEnabled', true);
	newGroupNode.SetProperty('ClientSideOnNodeSelect', "nodeSelect");
	newGroupNode.SetProperty('Selectable', true);
	newGroupNode.SetProperty('Value', groupID + ":-1");
	newGroupNode.SetProperty('ID', groupID + ":-1");

	var rootNode = window.<%=this.Tree.ClientObjectId%>.FindNodeById("RootNode");
	if (rootNode != null)
	{
		rootNode.AddNode(newGroupNode);
		window.<%=this.Tree.ClientObjectId%>.SelectedNode = newGroupNode;
		window.<%=this.Tree.ClientObjectId%>.Render();
	}
	
}

//## ADD SECTION ##
function AddSection(groupID, parentSectionID)
{
	toggleButtons();
	
	selectedNode = window.<%=this.Tree.ClientObjectId%>.SelectedNode;

	// If a GroupID and SectionID were not passed, get them from the selected node
	if (groupID == null)
	{
		if (selectedNode == null)
		{
			alert("<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_PleaseSelectAGroup") %>");
			toggleButtons();
			return;
		}
		else
		{
			groupID = getGroupID(selectedNode);
			if (groupID == -1)
			{
				alert("<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_PleaseSelectAGroup") %>");
				toggleButtons();
				return;
			}
		}
	}
	if (parentSectionID == null)
	{
		if (selectedNode == null)
		{
			alert("<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_PleaseSelectAGroup") %>");
			toggleButtons();
			return;
		}
		else
			parentSectionID = getSectionID(selectedNode);
	}

	newNodeSeq = newNodeSeq + 1; //ensure unique key names
	newNodeGroupID = groupID;
	AddSectionNode(groupID, parentSectionID, '0:'+newNodeSeq, '')
	var newNode = window.<%=this.Tree.ClientObjectId%>.FindNodeById(groupID + ":0:"+newNodeSeq);
	
	if (newNode == null)
	{
			alert("<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_PleaseSelectAGroup") %>");
			toggleButtons();
			return;
	}
	
	newNode.SetProperty('EditingEnabled', true);
	newNode.SetProperty('DraggingEnabled', false);
	newNode.SetProperty('DroppingEnabled', false);
	window.<%=this.Tree.ClientObjectId%>.Render();
	newNode.Edit();
}

function AddSectionCallBack(res)
{
	cleanupNewNode();
	if (res.value == '0')
	{
		alert("<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_PhotosAdmin_SectionEdit_Settings_AppKey_DuplicateNameException") %>");
		window.frames["sectionEditFrame"].location.href = "<%= SiteUrls.Instance().BlankPage %>";
	}
	else if(res.value != '-1')
	{
		var retVals = res.value.split("^");
		var groupID = retVals[0];
		var parentSectionID = retVals[1];
		var sectionID = retVals[2];
		var sectionName = retVals[3];
	
		AddSectionNode(groupID, parentSectionID, sectionID, sectionName)
		window.frames["sectionEditFrame"].location.href = "IFrameHost.aspx?SectionID=" + sectionID + "&GroupID=" + groupID;
	}
	toggleButtons();
}
function cleanupNewNode()
{
	window.<%=this.Tree.ClientObjectId%>.SelectedNode = null;
	var newNode = window.<%=this.Tree.ClientObjectId%>.FindNodeById(newNodeGroupID + ":0:"+newNodeSeq);
	if (newNode != null)
	{
		newNode.Remove();
		newNode.SaveState();
	}
	window.<%=this.Tree.ClientObjectId%>.Render();
}
function AddSectionNode(groupID, parentSectionID, sectionID, name)
{
	var rootNode;
	if (parentSectionID > 0)
		rootNode = window.<%=this.Tree.ClientObjectId%>.FindNodeById(groupID + ":" + parentSectionID);
	else
		rootNode = window.<%=this.Tree.ClientObjectId%>.FindNodeById(groupID + ":-1");
	
	if(rootNode != null)
	{
		var newSectionNode = new ComponentArt_TreeViewNode();
		newSectionNode.SetProperty('Text', name);
		newSectionNode.SetProperty('ImageUrl', "notesdisabled.gif");
		newSectionNode.SetProperty('Checked', null);
		newSectionNode.SetProperty('EditingEnabled', false);
		newSectionNode.SetProperty('DraggingEnabled', true);
		newSectionNode.SetProperty('DroppingEnabled', true);
		newSectionNode.SetProperty('ClientSideOnNodeSelect', "nodeSelect");
		newSectionNode.SetProperty('Selectable', true);
		newSectionNode.SetProperty('Value', groupID + ":" + sectionID);
		newSectionNode.SetProperty('ID', groupID + ":" + sectionID);

		rootNode.AddNode(newSectionNode);
		window.<%=this.Tree.ClientObjectId%>.SelectedNode = newSectionNode;
		window.<%=this.Tree.ClientObjectId%>.Render();
		
		rootNode.Expand();
		window.<%=this.Tree.ClientObjectId%>.Render();
	}
	else
	{
		window.location = window.location;
	}
}

//## END ADD ##


//## DELETE ##
function DeleteSelected(groupID, sectionID)
{
	var deletedNode = window.<%=this.Tree.ClientObjectId%>.SelectedNode;

	// If a GroupID or SectionID was not passed, get it from the selected node
	if (groupID == null || sectionID == null)
	{
		// Check if no node is selected
		if (deletedNode == null)
			return;

		groupID = getGroupID(deletedNode);
		sectionID = getSectionID(deletedNode);
	}
	
	if (sectionID == '-1' && groupID == '-1')
		return;
	
	// Clear the IFrame if we are deleting the current item
	if (deletedNode != null)
	{
		if(getSectionID(deletedNode) == sectionID && getGroupID(deletedNode) == groupID)
			window.frames["sectionEditFrame"].location.href = "IFrameHost.aspx?SectionID=0&GroupID=0";
	}
		
	if (sectionID != '-1')
		DeleteSection(groupID, sectionID);
	else
		DeleteGroup(groupID);
}

//## DELETE GROUP ##
function DeleteGroup(groupID)
{
	if (window.confirm("<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_DeleteGroupConfirmation") %>"))
	{
		window.frames["sectionEditFrame"].location.href = "<%= SiteUrls.Instance().ControlPanelLoading(CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_DeleteGroupWait")) %>";
		ForumTreeViewControl.DeleteGroup('<%= this.ClientID %>', groupID, DeleteGroupCallBack);
	}
}
function DeleteGroupCallBack(res)
{
	window.frames["sectionEditFrame"].location.href = "<%= SiteUrls.Instance().BlankPage %>";

	if (res == null)
		window.location = window.location;
	
	if (res.value == null)
	{
		alert('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_DeleteGroupError") %>');
		return;
	}
	
	if (res.value == 0)
	{
		alert('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_GroupNotEmptyException") %>');
		return;
	}
	
	var deletedNode = window.<%=this.Tree.ClientObjectId%>.FindNodeById(res.value + ":-1");
	if (deletedNode != null)
	{
		deletedNode.Remove();
		window.<%=this.Tree.ClientObjectId%>.Render();
	}
		
	return;
}

//## DELETE SECTION ##
function DeleteSection(groupID, sectionID)
{
	if(window.confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_DeleteSectionConfirmation") %>'))
	{
		window.frames["sectionEditFrame"].location.href = "<%= SiteUrls.Instance().ControlPanelLoading(CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_DeleteSectionWait")) %>";
		ForumTreeViewControl.DeleteSection('<%= this.ClientID %>', groupID, sectionID,  DeleteSectionCallBack);
	}
}

function DeleteSectionCallBack(res)
{
	window.frames["sectionEditFrame"].location.href = "<%= SiteUrls.Instance().BlankPage %>";

	if (res == null)
		window.location = window.location;

	if (res.value == null)
	{
		alert('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_DeleteSectionError") %>');
		return;
	}
	
	if (res.value == "0")
	{
		alert('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_DeleteSectionError") %>');
		return;
	}
	
	var retVals = res.value.split("^");
	var groupID = retVals[0];
	var sectionID = retVals[1];
	
	var deletedNode = window.<%=this.Tree.ClientObjectId%>.FindNodeById(groupID + ":" + sectionID);
	if (deletedNode != null)
	{
		deletedNode.Remove();
		window.<%=this.Tree.ClientObjectId%>.Render();
	}
		
	return;
}
//## END DELETE ##


//## ENABLE SECTION TOGGLE ##
function ChangeSectionEnabled(groupID, sectionID )
{
	ForumTreeViewControl.ChangeSectionEnabled('<%= this.ClientID %>', groupID, sectionID, ChangeSectionEnabledCallBack);
}
function ChangeSectionEnabledCallBack(res)
{
	if (res == null)
		window.location = window.location;

	if(res.value == '-1') //not an admin
		return;
		
	var retVals = res.value.split("^");
	var groupID = retVals[0];
	var sectionID = retVals[1];

	var changedNode = window.<%=this.Tree.ClientObjectId%>.FindNodeById(groupID + ":" + sectionID);
	if (changedNode != null)
	{
		if(changedNode.Checked == 1) //used for enabled
		{
			changedNode.SetProperty('ImageUrl', "notesdisabled.gif");
			changedNode.SetProperty('Checked', null);
		}
		else
		{
			changedNode.SetProperty('ImageUrl', "notes.gif");
			changedNode.SetProperty('Checked',  1);
		}
		window.<%=this.Tree.ClientObjectId%>.Render();

		//update the iframe
		if(window.<%=this.Tree.ClientObjectId%>.SelectedNode != null && changedNode.ID == window.<%=this.Tree.ClientObjectId%>.SelectedNode.ID)
			nodeSelect(changedNode); 
	}
	
	return;
}

function toggleButtons()
{
	toggleVisibility('<%=this.AddGroupButtonClientID %>');
	toggleVisibility('<%=this.DeleteButtonClientID  %>');
	toggleVisibility('<%=this.AddSectionButtonClientID  %>');
}
function toggleVisibility( targetId ){ 
  if (document.getElementById){ 
        target = document.getElementById( targetId ); 
           if (target.style.display == "none"){ 
              target.style.display = ""; 
           } else { 
              target.style.display = "none"; 
           }
     }
}

//for splitter support
function resizeCategoryTree(DomElementId, NewPaneHeight, NewPaneWidth)
{
	// Forces the treeview to adjust to the new size of its container 
	window.<%=this.Tree.ClientObjectId%>.Render();
	if(DomElementId != null)
		resizeDetails();
}

// ]]>
</script>

<ca:treeview id="Tree" Width="100%" Height="100%" AutoScroll="true"
	draganddropenabled="true" nodeeditingenabled="true"
	keyboardenabled="true" cssclass="ForumFolderTree" 
	nodecssclass="ForumFolderTreeNode" selectednodecssclass="ForumFolderTreeNodeSelected" 
	hovernodecssclass="ForumFolderTreeNodeHover"
	lineimagewidth="19" lineimageheight="20" allowtextselection="true" 
	defaultimagewidth="16" defaultimageheight="16"
	ExpandCollapseImageWidth="17" ExpandCollapseImageHeight="15"
	itemspacing="0" imagesbaseurl="~/ControlPanel/Images/CATreeView/small_icons/"
	nodelabelpadding="0" parentnodeimageurl="folder.gif" 
	expandedparentnodeimageurl="folder_open.gif" showlines="true" 
	lineimagesfolderurl="~/ControlPanel/Images/CATreeView/small_icons/lines/"
	enableviewstate="false" ClientSideOnNodeMove="nodeMove" 
	ClientSideOnNodeRename="nodeRename" ClientSideOnNodeMouseOut="checkNodeIndex"
	ClientSideOnNodeMouseOver="checkNodeIndex" ClientSideOnNodeSelect="nodeSelect"
	ExpandNodeOnSelect="true" CollapseNodeOnSelect="false"  AutoPostBackOnSelect="false"
	AutoPostBackOnNodeMove="false" dropsiblingenabled="true"
	OnContextMenu="treeContextMenu" runat="server" FillContainer="True"
	BorderColor="#CCCCCC" BorderWidth="0" BorderStyle="Solid" >
</ca:treeview>

<cp:contextmenu 
	filename="~/ControlPanel/Forums/GroupNodeContextMenu.config" 
	runat="server" id="GroupContextMenu" 
	onmenuopenscript="" onmenuclosescript="" >
    <SkinTemplate>
        <CA:Menu runat="server" 
	        id="Menu" 
	        AutoPostBackOnSelect="false" 
	        ContextMenu="Custom" 
	        Orientation="Vertical"
	        CssClass="CommonContextMenuGroup"
	        DefaultGroupCssClass="CommonContextMenuGroup"
	        DefaultItemLookID="DefaultItemLook" >
		        <ItemLooks>
			        <CA:ItemLook LookID="DefaultItemLook" 
				        CssClass="CommonContextMenuItem" 
				        HoverCssClass="CommonContextMenuItemHover" 
				        ExpandedCssClass="CommonContextMenuItemExpanded" 
				        LabelPaddingLeft="5" 
				        LabelPaddingRight="15" 
				        LabelPaddingTop="2" 
				        LabelPaddingBottom="2" />
		        </ItemLooks>	
        </CA:Menu>
    </SkinTemplate>
</cp:contextmenu>

<cp:contextmenu 
	filename="~/ControlPanel/Forums/SectionNodeContextMenu.config" 
	runat="server" id="SectionContextMenu" 
	onmenuopenscript="" onmenuclosescript="" >
    <SkinTemplate>
        <CA:Menu runat="server" 
	        id="Menu" 
	        AutoPostBackOnSelect="false" 
	        ContextMenu="Custom" 
	        Orientation="Vertical"
	        CssClass="CommonContextMenuGroup"
	        DefaultGroupCssClass="CommonContextMenuGroup"
	        DefaultItemLookID="DefaultItemLook" >
		        <ItemLooks>
			        <CA:ItemLook LookID="DefaultItemLook" 
				        CssClass="CommonContextMenuItem" 
				        HoverCssClass="CommonContextMenuItemHover" 
				        ExpandedCssClass="CommonContextMenuItemExpanded" 
				        LabelPaddingLeft="5" 
				        LabelPaddingRight="15" 
				        LabelPaddingTop="2" 
				        LabelPaddingBottom="2" />
		        </ItemLooks>	
        </CA:Menu>
    </SkinTemplate>
</cp:contextmenu>
	
<cp:contextmenu 
	filename="~/ControlPanel/Forums/SectionNodeDisabledContextMenu.config" 
	runat="server" id="SectionDisabledContextMenu" 
	onmenuopenscript="" onmenuclosescript="" >
    <SkinTemplate>
        <CA:Menu runat="server" 
	        id="Menu" 
	        AutoPostBackOnSelect="false" 
	        ContextMenu="Custom" 
	        Orientation="Vertical"
	        CssClass="CommonContextMenuGroup"
	        DefaultGroupCssClass="CommonContextMenuGroup"
	        DefaultItemLookID="DefaultItemLook" >
		        <ItemLooks>
			        <CA:ItemLook LookID="DefaultItemLook" 
				        CssClass="CommonContextMenuItem" 
				        HoverCssClass="CommonContextMenuItemHover" 
				        ExpandedCssClass="CommonContextMenuItemExpanded" 
				        LabelPaddingLeft="5" 
				        LabelPaddingRight="15" 
				        LabelPaddingTop="2" 
				        LabelPaddingBottom="2" />
		        </ItemLooks>	
        </CA:Menu>
    </SkinTemplate>
</cp:contextmenu>

<%@ Register TagPrefix="Panel" TagName = "ActivityQueryControl" Src = "~/ControlPanel/Reporting/ActivityQueryControl.ascx" %>
<%@ Page language="c#" Codebehind="ForumPostViewsReport.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Tools.Reports.ForumPostViewsReport" %>
<cp:controlpanelselectednavigation selectednavitem="Forums_PostViewsReport" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="ReportingControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="CP_Reporting_Forums_PostViews_Title"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<cp:StatusMessage runat="server" id="Status" />
		<TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
			<TR>
				<TD>
					<panel:ActivityQueryControl id="UserActivityQueryControl1" runat="Server" destinationurl="ForumPostViewsReport.aspx"></panel:ActivityQueryControl></TD>
			</TR>
		</TABLE>
		<BR>
		<DIV id="GridWithMenu">
			<DIV id="GrayGrid">
				<CA:grid id="Grid1" runat="Server" AllowPaging="false" EditOnClickSelectedItem="false" AllowMultipleSelect="false"
					AllowEditing="false" loadingpanelposition="MiddleCenter" loadingpanelclienttemplateid="Loading"
					groupbysortimageheight="10" groupbysortimagewidth="10" groupbysortdescendingimageurl="group_desc.gif"
					groupbysortascendingimageurl="group_asc.gif" groupingnotificationtextcssclass="GridHeaderText"
					indentcellwidth="22" treelineimageheight="19" treelineimagewidth="22" treelineimagesfolderurl="~/controlpanel/images/caimages/lines"
					preexpandongroup="true" groupingpagesize="5"
					groupbytextcssclass="GroupByText"
					groupbycssclass="GroupByCell" headercssclass="GridHeader"
					searchonkeypress="true" searchtextcssclass="GridHeaderText" showsearchbox="false" showheader="false"
					cssclass="Grid" runningmode="Callback">
					<levels>
						<CA:gridlevel datakeyfield="pageURL" showtableheading="false" showselectorcells="false" rowcssclass="Row"
							columnreorderindicatorimageurl="reorder.gif" datacellcssclass="DataCell" headingcellcssclass="HeadingCell"
							headingcellhovercssclass="HeadingCellHover" headingcellactivecssclass="HeadingCellActive"
							headingrowcssclass="HeadingRow" headingtextcssclass="HeadingCellText" selectedrowcssclass="SelectedRow"
							groupheadingcssclass="GroupHeading" sortascendingimageurl="asc.gif" sortdescendingimageurl="desc.gif"
							sortimagewidth="10" sortimageheight="19" editcellcssclass="EditDataCell" editfieldcssclass="EditDataField"
							editcommandclienttemplateid="EditCommandTemplate" insertcommandclienttemplateid="InsertCommandTemplate"
							alternatingrowcssclass="AlternatingRow" allowsorting="false" allowreordering="false" allowgrouping="false">
							<columns>
								<CA:gridcolumn width="450" datafield="PostSubject" TextWrap="true" headingtext="ResourceManager.CP_Reporting_Forums_PostViews_GridCol_PostSubject" />
								<CA:gridcolumn width="70" datafield="PostViews" headingtext="ResourceManager.CP_Reporting_Forums_PostViews_GridCol_Views" FormatString="#,###" />
							</columns>
						</CA:gridlevel>
					</levels>
					<clienttemplates>
						<CA:clienttemplate id="Loading">
					<img src="../../images/caimages/spinner.gif" /><strong>Loading</strong>...please wait.
				</CA:clienttemplate>
					</clienttemplates>
				</CA:grid></DIV>
		</DIV>
	</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="ForumPostActivityReport.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Tools.Reports.ForumPostActivityReport" %>
<%@ Register TagPrefix="Panel" TagName = "ActivityQueryControl" Src = "~/ControlPanel/Reporting/ActivityQueryControl.ascx" %>
<cp:controlpanelselectednavigation selectednavitem="Forums_PostActivityReport" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="ReportingControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="CP_Tools_ForumPostActivityReport_Title"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<cp:statusmessage id="Status" runat="server"></cp:statusmessage>
		<table cellspacing="0" cellpadding="2" width="100%" border="0">
			<tr>
				<td>
					<panel:activityquerycontrol id="ForumActivityQueryControl1" runat="Server" destinationurl="ForumPostActivityReport.aspx"></panel:activityquerycontrol></td>
			</tr>
		</table>
		<fieldset>
			<legend>
				<cp:resourcecontrol id="Summary" runat="server" resourcename="CP_Tools_ForumPostActivityReport_Summary"></cp:resourcecontrol>
			</legend>
			<table cellspacing="0" cellpadding="2" width="100%" border="0">
				<tr>
					<td align="left">
						<cp:resourcecontrol id="UniqueCount" runat="Server" resourcename="CP_Tools_ForumPostActivityReport_Count"></cp:resourcecontrol>&nbsp;
						<asp:label id="countLabel" runat="server"></asp:label></td>
				</tr>
			</table>
		</fieldset>
		<br>
		<div id="GridWithMenu">
			<div id="GrayGrid">
				<ca:grid id="Grid1" runat="Server" runningmode="Callback" cssclass="Grid" showheader="false"
					showsearchbox="false" searchtextcssclass="GridHeaderText" searchonkeypress="true" headercssclass="GridHeader"
					footercssclass="GridFooter" groupbycssclass="GroupByCell" groupbytextcssclass="GroupByText"
					pagerstyle="Slider" pagertextcssclass="GridFooterText" pagerbuttonwidth="41" pagerbuttonheight="22"
					sliderheight="20" sliderwidth="150" slidergripwidth="9" sliderpopupoffsetx="20" groupingpagesize="5"
					preexpandongroup="true" imagesbaseurl="~/controlpanel/images/caimages" pagerimagesfolderurl="~/controlpanel/images/caimages/pager"
					treelineimagesfolderurl="~/controlpanel/images/caimages/lines" treelineimagewidth="22"
					treelineimageheight="19" indentcellwidth="22" groupingnotificationtextcssclass="GridHeaderText"
					groupbysortascendingimageurl="group_asc.gif" groupbysortdescendingimageurl="group_desc.gif"
					groupbysortimagewidth="10" groupbysortimageheight="10" loadingpanelclienttemplateid="Loading"
					loadingpanelposition="MiddleCenter" width="100%" allowediting="false" allowmultipleselect="false"
					editonclickselecteditem="false" manualpaging="true">
					<levels>
						<ca:gridlevel datakeyfield="IP" showtableheading="false" showselectorcells="false" rowcssclass="Row"
							columnreorderindicatorimageurl="reorder.gif" datacellcssclass="DataCell" headingcellcssclass="HeadingCell"
							headingcellhovercssclass="HeadingCellHover" headingcellactivecssclass="HeadingCellActive"
							headingrowcssclass="HeadingRow" headingtextcssclass="HeadingCellText" selectedrowcssclass="SelectedRow"
							groupheadingcssclass="GroupHeading" sortascendingimageurl="asc.gif" sortdescendingimageurl="desc.gif"
							sortimagewidth="10" sortimageheight="19" editcellcssclass="EditDataCell" editfieldcssclass="EditDataField"
							editcommandclienttemplateid="EditCommandTemplate" insertcommandclienttemplateid="InsertCommandTemplate"
							alternatingrowcssclass="AlternatingRow" allowsorting="false" allowreordering="false" allowgrouping="false">
							<columns>
								<ca:gridcolumn visible="false" datafield="Name" />
								<ca:gridcolumn visible="false" datafield="SectionID" />
								<ca:gridcolumn width="150" DataCellServerTemplateId="linkColumn" headingtext="ResourceManager.CP_Tools_ForumPostActivityReport_GridCol_ForumName"></ca:gridcolumn>
								<ca:gridcolumn width="150" datafield="Replies" headingtext="ResourceManager.CP_Tools_ForumPostActivityReport_GridCol_Replies" />
								<ca:gridcolumn width="150" datafield="Threads" headingtext="ResourceManager.CP_Tools_ForumPostActivityReport_GridCol_Threads" />
								<ca:gridcolumn width="150" datafield="Users" headingtext="ResourceManager.CP_Tools_ForumPostActivityReport_GridCol_Users" />
							</columns>
						</ca:gridlevel>
					</levels>
					<clienttemplates>
						<ca:clienttemplate id="Loading">
							<img src="../../images/caimages/spinner.gif" /><strong>Loading</strong>...please wait.
						</ca:clienttemplate>
					</clienttemplates>
					<servertemplates>
						<ca:gridservertemplate id="linkColumn">
							<template>
								<a href="/forums/<%# Container.DataItem["SectionID"] %>/ShowForum.aspx" target="_blank"><%# Container.DataItem["Name"] %></a>
							</template>
						</ca:gridservertemplate>
					</servertemplates>
				</ca:grid></div>
		</div>
	</CP:Content>
</CP:Container>

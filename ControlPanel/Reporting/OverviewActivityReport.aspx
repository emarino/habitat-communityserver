<%@ Page language="c#" Codebehind="OverviewActivityReport.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Tools.Reports.OverviewActivityReport" %>
<%@ Register TagPrefix="Panel" TagName = "ActivityQueryControl" Src = "~/ControlPanel/Reporting/ActivityQueryControl.ascx" %>
<cp:controlpanelselectednavigation selectednavitem="Site_OverviewActivity" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="ReportingControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="CP_Tools_OverviewActivityReport_Title"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<cp:statusmessage id="Status" runat="server"></cp:statusmessage>
		<table cellspacing="0" cellpadding="2" width="100%" border="0">
			<tr>
				<td>
					<panel:activityquerycontrol id="ForumActivityQueryControl1" runat="Server" destinationurl="OverviewActivityReport.aspx"></panel:activityquerycontrol></td>
			</tr>
		</table>
		<fieldset>
			<legend>
				<cp:resourcecontrol id="Summary" runat="server" resourcename="CP_Tools_OverviewActivityReport_Summary"></cp:resourcecontrol>
			</legend>
			<table cellspacing="0" cellpadding="2" width="100%" border="0">
				<tr>
					<td align="left">
						<cp:resourcecontrol id="NewUsers" runat="Server" resourcename="CP_Tools_OverviewActivityReport_UserCount"></cp:resourcecontrol>&nbsp;
						<asp:label id="lblUserCount" runat="server"></asp:label></td>
				</tr>
				<tr>
					<td align="left">
						<cp:resourcecontrol id="NewThreads" runat="Server" resourcename="CP_Tools_OverviewActivityReport_ThreadCount"></cp:resourcecontrol>&nbsp;
						<asp:label id="lblThreadCount" runat="server"></asp:label></td>
				</tr>
				<tr>
					<td align="left">
						<cp:resourcecontrol id="NewPosts" runat="Server" resourcename="CP_Tools_OverviewActivityReport_PostCount"></cp:resourcecontrol>&nbsp;
						<asp:label id="lblPostCount" runat="server"></asp:label></td>
				</tr>
			</table>
		</fieldset>
		<br>

		</div>
	</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="BlogActivityReport.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Tools.Reports.BlogActivityReport" %>
<%@ Register TagPrefix="Panel" TagName = "ActivityQueryControl" Src = "~/ControlPanel/Reporting/ActivityQueryControl.ascx" %>
<cp:controlpanelselectednavigation selectednavitem="Blogs_BlogActivityReport" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="ReportingControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="CP_Tools_BlogActivityReport_Title"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<cp:StatusMessage runat="server" id="Status" />
		<TABLE cellSpacing="0" cellPadding="2" width="100%" border="0">
			<TR>
				<TD>
					<panel:ActivityQueryControl id="BlogActivityQueryControl1" runat="Server" destinationurl="BlogActivityReport.aspx"></panel:ActivityQueryControl></TD>
			</TR>
		</TABLE>
		<FIELDSET>
			<LEGEND>
				<cp:ResourceControl id="Summary" runat="server" ResourceName="CP_Tools_BlogActivityReport_Summary" />
			</LEGEND>
			<TABLE cellSpacing="0" cellPadding="2" width="90%" border="0">
				<TR>
					<TD align="left">
						<cp:resourcecontrol id="TotalBlogs" runat="Server" resourcename="CP_Tools_BlogActivityReport_TotalBlogs"></cp:resourcecontrol>&nbsp;
						<asp:Label id="blogCountLabel" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD align="left">
						<cp:resourcecontrol id="TotalEnabledBlogs" runat="Server" resourcename="CP_Tools_BlogActivityReport_TotalEnabledBlogs"></cp:resourcecontrol>&nbsp;
						<asp:Label id="enabledBlogCountLabel" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD align="left">
						<cp:resourcecontrol id="TotalDisabledBlogs" runat="Server" resourcename="CP_Tools_BlogActivityReport_TotalDisabledBlogs"></cp:resourcecontrol>&nbsp;
						<asp:Label id="disabledBlogCountLabel" runat="server"></asp:Label></TD>
				</TR>
				<TR>
					<TD align="left">
						<cp:resourcecontrol id="RangeTotal" runat="Server" resourcename="CP_Tools_BlogActivityReport_RangeTotalBlogs"></cp:resourcecontrol>&nbsp;
						<asp:Label id="rangeTotalBlogs" runat="server"></asp:Label></TD>
				</TR>
			</TABLE>
		</FIELDSET>
		<BR>
		<DIV id="GridWithMenu">
			<DIV id="GrayGrid">
				<CA:grid id="Grid1" runat="Server" runningmode="Callback" cssclass="Grid" showheader="false"
					showsearchbox="false" searchtextcssclass="GridHeaderText" searchonkeypress="true" headercssclass="GridHeader"
					footercssclass="GridFooter" groupbycssclass="GroupByCell" groupbytextcssclass="GroupByText"
					pagerstyle="Slider" pagertextcssclass="GridFooterText" pagerbuttonwidth="41" pagerbuttonheight="22"
					sliderheight="20" sliderwidth="150" slidergripwidth="9" sliderpopupoffsetx="20" groupingpagesize="5"
					preexpandongroup="true" imagesbaseurl="~/controlpanel/images/caimages" pagerimagesfolderurl="~/controlpanel/images/caimages/pager"
					treelineimagesfolderurl="~/controlpanel/images/caimages/lines" treelineimagewidth="22"
					treelineimageheight="19" indentcellwidth="22" groupingnotificationtextcssclass="GridHeaderText"
					groupbysortascendingimageurl="group_asc.gif" groupbysortdescendingimageurl="group_desc.gif"
					groupbysortimagewidth="10" groupbysortimageheight="10" loadingpanelclienttemplateid="Loading"
					loadingpanelposition="MiddleCenter" AllowEditing="false" AllowMultipleSelect="false"
					EditOnClickSelectedItem="false" ManualPaging="true">
					<levels>
						<CA:gridlevel datakeyfield="ThreadID" showtableheading="false" showselectorcells="false" rowcssclass="Row"
							columnreorderindicatorimageurl="reorder.gif" datacellcssclass="DataCell" headingcellcssclass="HeadingCell"
							headingcellhovercssclass="HeadingCellHover" headingcellactivecssclass="HeadingCellActive"
							headingrowcssclass="HeadingRow" headingtextcssclass="HeadingCellText" selectedrowcssclass="SelectedRow"
							groupheadingcssclass="GroupHeading" sortascendingimageurl="asc.gif" sortdescendingimageurl="desc.gif"
							sortimagewidth="10" sortimageheight="19" editcellcssclass="EditDataCell" editfieldcssclass="EditDataField"
							editcommandclienttemplateid="EditCommandTemplate" insertcommandclienttemplateid="InsertCommandTemplate"
							alternatingrowcssclass="AlternatingRow" allowsorting="false" allowreordering="false" allowgrouping="false">
							<columns>
								<CA:gridcolumn width="150" DataCellServerTemplateId="blogLink" headingtext="ResourceManager.CP_Tools_BlogActivityReport_GridCol_Blog" />
								<CA:gridcolumn width="150" datafield="BlogPosts" headingtext="Blog Posts" FormatString="#,###" />
								<CA:gridcolumn width="150" datafield="Articles" headingtext="Article Posts" FormatString="#,### "/>
								<CA:gridcolumn width="150" datafield="Views" headingtext="ResourceManager.CP_Tools_BlogActivityReport_GridCol_Views" FormatString="#,###" />
								<CA:gridcolumn width="150" datafield="Comments" headingtext="ResourceManager.CP_Tools_BlogActivityReport_GridCol_Comments" FormatString="#,###" />
								<CA:gridcolumn width="150" datafield="Trackbacks" headingtext="ResourceManager.CP_Tools_BlogActivityReport_GridCol_Trackbacks" FormatString="#,###" />
								<CA:gridcolumn datafield="ApplicationKey" visible="false" />
							</columns>
						</CA:gridlevel>
					</levels>
					<clienttemplates>
						<CA:clienttemplate id="Loading">
							<img src="../../images/caimages/spinner.gif" /><strong>Loading</strong>...please wait.
						</CA:clienttemplate>
					</clienttemplates>
					<ServerTemplates>
						<CA:GridServerTemplate ID="blogLink">
							<Template>
								<asp:HyperLink ID="blogHyperLink" NavigateUrl="~/blogs/" runat="server">Blog</asp:HyperLink>
							</Template>
						</CA:GridServerTemplate>
					</ServerTemplates>
				</CA:grid></DIV>
		</DIV>
	</CP:Content>
</CP:Container>

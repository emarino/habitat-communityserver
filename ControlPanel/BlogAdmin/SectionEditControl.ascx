<%@ Register TagPrefix="CP" TagName = "SectionDetails" Src = "~/ControlPanel/BlogAdmin/SectionEditDetailsControl.ascx" %>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="SectionEditControl.ascx.cs" Inherits="CommunityServer.ControlPanel.BlogAdmin.SectionEditControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<cp:directorywriter location = "weblogs" unauthorizedresourcename="BlogSettings_UnAuthorizedException" 
	securityresourcename="BlogSettings_SecurityException" forecolor="Red" runat="Server" id="WeblogDirectoryWriter" />

<TWC:TabbedPanes id="SettingTabs" runat="server"
	PanesCssClass="CommonPane"
	TabSetCssClass="CommonPaneTabSet"
	TabCssClasses="CommonPaneTab,CommonPaneTab1,CommonPaneTab2"
	TabSelectedCssClasses="CommonPaneTabSelected,CommonPaneTabSelected1,CommonPaneTabSelected2"
	TabHoverCssClasses="CommonPaneTabHover,CommonPaneTabHover1,CommonPaneTabHover2"
	>
	<TWC:TabbedPane runat="server">
		<Tab><CP:ResourceControl runat="server" ResourceName="CP_BlogAdmin_SectionEdit_Tab_Settings" /></Tab>
		<Content>
			<CP:SectionDetails id="SectionDetails1" runat="Server"></CP:SectionDetails>
		</Content>
	</TWC:TabbedPane>
	<TWC:TabbedPane ID="SectionPermissions" runat="server">
		<Tab><CP:ResourceControl runat="server" ResourceName="CP_BlogAdmin_SectionEdit_Tab_Permissions" /></Tab>
		<Content>
			<div class="CommonDescription"><br />
			<CP:ResourceControl id="Resourcecontrol1" runat="Server" resourcename="CP_BlogAdmin_SectionEdit_PermissionsTip"></CP:ResourceControl></div>
		    <div id="GrayGrid">
            <asp:Repeater runat="server" ID="PermissionList">
                <HeaderTemplate>
                    <table class="Grid" cellSpacing=0 cellPadding=0 width="100%">
                    <tbody>
                        <tr class="HeadingRow HeadingCellText HeadingCell">
                            <th><CP:ResourceControl runat="server" ResourceName="CP_Blog_GridCol_Name" /></th>
                            <th><CP:ResourceControl runat="server" ResourceName="CP_Blog_GridCol_View" /></th>
                            <th><CP:ResourceControl runat="server" ResourceName="CP_Blog_GridCol_Post" /></th>
                            <th><CP:ResourceControl runat="server" ResourceName="CP_Blog_GridCol_Reply" /></th>
                            <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Ink" /></th>
                            <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Video" /></th>
                            <th><CP:ResourceControl runat="server" ResourceName="CP_Blog_GridCol_LocalAttachment" /></th>
                            <th><CP:ResourceControl runat="server" ResourceName="CP_Blog_GridCol_RemoteAttachment" /></th>
                        </tr>    
                </HeaderTemplate>
                <ItemTemplate>
                        <tr class="Row">
                            <td><%# Eval("Name") %><asp:HiddenField ID="RoleId" runat="server" Value='<%# Eval("RoleIDString") %>' Visible="false" /></td>
                            <td><asp:CheckBox runat="server" ID="ViewCheckbox" Checked='<%# Eval("View") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="PostCheckbox" Checked='<%# Eval("Post") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="ReplyCheckbox" Checked='<%# Eval("Reply") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="InkCheckbox" Checked='<%# Eval("Ink") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="VideoCheckbox" Checked='<%# Eval("Video") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="LocalAttachmentCheckbox" Checked='<%# Eval("LocalAttachment") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="RemoteAttachmentCheckbox" Checked='<%# Eval("RemoteAttachment") %>' /></td>
                        </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                        <tr class="AlternatingRow">
                            <td><%# Eval("Name") %><asp:HiddenField ID="RoleId" runat="server" Value='<%# Eval("RoleIDString") %>' Visible="false" /></td>
                            <td><asp:CheckBox runat="server" ID="ViewCheckbox" Checked='<%# Eval("View") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="PostCheckbox" Checked='<%# Eval("Post") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="ReplyCheckbox" Checked='<%# Eval("Reply") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="InkCheckbox" Checked='<%# Eval("Ink") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="VideoCheckbox" Checked='<%# Eval("Video") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="LocalAttachmentCheckbox" Checked='<%# Eval("LocalAttachment") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="RemoteAttachmentCheckbox" Checked='<%# Eval("RemoteAttachment") %>' /></td>
                        </tr>
                </AlternatingItemTemplate>
                <FooterTemplate>
                    </tbody>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            </div>
		</Content>
	</TWC:TabbedPane>
	<TWC:TabbedPane id="MailingList" runat="server">
		<Tab><CP:ResourceControl runat="server" ResourceName="CP_BlogAdmin_SectionEdit_Tab_BlogByEmail" /></Tab>
		<Content>
		<table cellspacing="0" cellpadding="2" border="0">
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon id="Helpicon17" runat="Server" resourcename="CP_BlogAdmin_SectionEdit_LE_EnableMailingList_Detail"></cp:helpicon>
				<cp:resourcelabel id="Resourcelabel9" runat="server" resourcename="CP_BlogAdmin_SectionEdit_LE_EnableMailingList"></cp:resourcelabel></td>
			<td class="CommonFormField">
				<cp:yesnoradiobuttonlist id="LEEnableMailingList" runat="server" cssclass="txt1" repeatcolumns="2"></cp:yesnoradiobuttonlist>
			</td>
		</tr>
		</table>
		</Content>
	</TWC:TabbedPane>
</TWC:TabbedPanes>

<%@ Register TagPrefix="CP" TagName = "BlogList" Src = "~/ControlPanel/BlogAdmin/BlogListControlForFeeds.ascx" %>
<%@ Page language="c#" Codebehind="BlogFeeds.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.BlogAdmin.BlogFeeds" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<CP:Container runat="server" id="Mpcontainer1" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="SectionDescription" runat="server" resourcename="CP_BlogAdmin_Blogs_Title"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
	    <CP:BlogList id="BlogList1" runat="Server"></CP:BlogList>
	</CP:Content>
</CP:Container>
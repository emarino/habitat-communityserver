<%@ Import Namespace="CommunityServer.Components" %>
<%@ Register TagPrefix="CP" TagName = "GroupEditControl" Src = "~/ControlPanel/Controls/GroupEditControl.ascx" %>
<%@ Register TagPrefix="CP" TagName = "SectionEditControl" Src = "~/ControlPanel/BlogAdmin/SectionEditControl.ascx" %>
<%@ Page language="c#" Codebehind="IFrameHost.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.BlogAdmin.IFrameHost" %>
<CP:Container id="MPContainer" IsModal="true" ThemeMasterFile="ControlPanelModalMaster.ascx" runat="server">
<CP:Content id="bcr" runat="server">
<DIV class="CommonContentArea">
	<DIV class="CommonContent">
		<cp:statusmessage id="Status" runat="server"></cp:statusmessage>
		<cp:sectioneditcontrol id="SectionEdit" runat="server" visible="false"></cp:sectioneditcontrol>
		<cp:groupeditcontrol id="GroupEdit" runat="server" visible="false"></cp:groupeditcontrol>
		<DIV class="CommonFormField PanelSaveButton">
			<cp:resourcelinkbutton id="Save" runat="server" cssclass="CommonTextButton" resourcename="Save" />
		</DIV>
	</DIV>
</DIV>
</CP:Content>
</CP:Container>

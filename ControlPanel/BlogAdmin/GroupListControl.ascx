<%@ Control Language="c#" AutoEventWireup="false" Codebehind="GroupListControl.ascx.cs" Inherits="CommunityServer.ControlPanel.BlogAdmin.GroupListControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script type="text/javascript">
  
  function reloadGroups()
  {
	window.location = window.location;
  }

  function onCallbackError(excString)
  {
    if (confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_CallBackWarning") %>')) alert(excString); 
    <%= Grid1.ClientID %>.Page(1); 
    window.location = window.location;
  }

  function onDelete(item)
  {
      return confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_BlogAdmin_GroupList_Delete_Warning") %>'); 
  }
  
  function deleteRow(rowId)
  {
    <%= Grid1.ClientID %>.Delete(<%= Grid1.ClientID %>.GetRowFromClientId(rowId)); 
  }
</script>
<p class="PanelSaveButton">
	<CP:ModalLink CssClass="CommonTextButton" ModalType="Link" Height="300" Width="400" runat="Server" resourcename="CP_BlogAdmin_GroupList_AddNew" Callback="reloadGroups" id="NewGroup" />
</p>
<div id="GrayGrid">
<CA:Grid id="Grid1" runat="server" AllowEditing="false" 
	AutoCallBackOnDelete="true" ClientSideOnDelete="onDelete"
	ClientSideOnCallbackError="onCallbackError" >
	<Levels>
		<CA:GridLevel DataKeyField="GroupID">
			<Columns>
				<CA:GridColumn DataField="Name" HeadingText="ResourceManager.CP_Blog_GridCol_Name" />
				<CA:GridColumn DataField="Description" HeadingText="ResourceManager.CP_Blog_GridCol_Description" />
				<CA:GridColumn DataField="GroupID" Visible="false" />
				<CA:GridColumn HeadingText="ResourceManager.CP_Blog_GridCol_Actions" DataCellClientTemplateId="EditTemplate" EditControlType="EditCommand" Width="150" Align="Center" />
			</Columns>
		</CA:GridLevel>
	</Levels>
	<ClientTemplates>
		<CA:ClientTemplate Id="EditTemplate">
			<a href="javascript:Telligent_Modal.Open('GroupForm.aspx?GroupID=##  DataItem.GetMember("GroupID").Text ##', 400, 300, reloadGroups);" class="CommonTextButton">Edit</a> 
			<a href='Blogs.aspx?GroupID=##  DataItem.GetMember("GroupID").Text ##' class="CommonTextButton">Manage</a> 
			<a href="javascript:deleteRow('## DataItem.ClientId ##')" class="CommonTextButton">Delete</a>
		</CA:ClientTemplate>
	</ClientTemplates>
</CA:Grid>
</div>


<%@ Page language="c#" Codebehind="SectionEdit.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.BlogAdmin.SectionEdit" %>
<%@ Register TagPrefix="CP" TagName = "SectionDetails" Src = "~/ControlPanel/BlogAdmin/SectionEditControl.ascx" %>
<CP:CONTROLPANELSELECTEDNAVIGATION id="SelectedNavigation1" runat="server" SelectedNavItem="BlogCreate" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<asp:literal id="RegionTitle" runat="server"></asp:literal>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<CP:StatusMessage id="Status" runat="server"></CP:StatusMessage>
		<DIV class="CommonDescription">
			<CP:ResourceControl id="RegionDescription" runat="Server" resourcename="CP_BlogAdmin_SectionEdit_Description"></CP:ResourceControl>
		</DIV>

		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Blogs" />
		<CP:SectionDetails id="SectionDetails1" runat="Server"></CP:SectionDetails>
		<p class="PanelSaveButton DetailsFixedWidth">
			<cp:resourcelinkbutton id="SaveButton" runat="server" resourcename="Save" cssclass="CommonTextButton" />
		</p>
	</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="Groups.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.BlogAdmin.GroupsPage" %>
<%@ Register TagPrefix="CP" TagName = "GroupList" Src = "~/ControlPanel/BlogAdmin/GroupListControl.ascx" %>
<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="SectionDescription" runat="server" ResourceName="CP_BlogAdmin_GroupList_Title" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<CP:ResourceControl id="Section_Sub_Description" runat="server" ResourceName="CP_BlogAdmin_GroupList_Description" /></DIV>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Blogs" />
		<CP:GroupList id="GroupList1" runat="Server" />
		<p class="PanelSaveButton">
			<CP:ControlPanelChangeManageView runat="server" CssClass="CommonTextButton" CurrentView="Grid" ViewChangeUrl="~/ControlPanel/BlogAdmin/GroupManager.aspx" ID="Controlpanelchangemanageview1" />
		</p>
	</CP:Content>
</CP:Container>

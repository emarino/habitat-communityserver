<%@ Page language="c#" Codebehind="InterestingPosts.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.BlogAdmin.InterestingPostsOptionsPage" %>
<CP:Container id="Mpcontainer1" runat="server" thememasterfile="SetupControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
	<CP:ResourceControl runat="Server" resourcename="CP_BlogAdmin_Options_InterestingPosts" />
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">
	<div class="CommonDescription">
		<CP:ResourceControl runat="Server" resourcename="CP_BlogAdmin_Options_InterestingPosts_Instructions" />
	</div>
	<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Blogs" />
	<CP:FileOnlyStatusMessage id="FOStatus" runat="server" visible="false" />
	<asp:PlaceHolder id="OptionHolder" runat="Server">
	<CP:ConfigOKStatusMessage id="Status" runat="server" visible="false" />
	<div class="FixedWidthContainer">
		<table cellpadding="0" cellspacing="0" border="0">
		
		<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_BlogAdmin_Options_InterestingPosts_Enable_Help" />
			<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Options_InterestingPosts_Enable" controltolabel="createDirectories" />
		</td>
		<td class="CommonFormField">
			<asp:checkbox id="enableFactors" runat="Server" cssclass="ControlPanelTextInput" />
		</td>
		</tr>
		
		<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_BlogAdmin_Options_InterestingPosts_Posts_Help" ID="Helpicon1"/>
			<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Options_InterestingPosts_Posts" controltolabel="PostsToUse" ID="Formlabel1"/>&nbsp;
		</td>
		<td class="CommonFormField">
			<asp:dropdownlist id="PostsToUse" runat="Server" cssclass="ControlPanelTextInput" width = "300px" >
			    <asp:ListItem text="100" value = "100" />
			    <asp:ListItem text="250" value = "250" />
			    <asp:ListItem text="500" value = "500" />
			    <asp:ListItem text="1000" value = "1000" />
			</asp:dropdownlist>
		</td>
		</tr>		
		
		<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_BlogAdmin_Options_InterestingPosts_DateFactor_Help" ID="Helpicon2"/>
			<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Options_InterestingPosts_DateFactor" controltolabel="createDirectories" ID="Formlabel2"/>
		</td>
		<td class="CommonFormField">
			<asp:TextBox id="DateFactor" runat="Server" cssclass="ControlPanelTextInput" width="200px" />
			<asp:CompareValidator runat = "Server" ControlToValidate="DateFactor" Operator = "DataTypeCheck" Type="Double">Invalid Format</asp:CompareValidator>
		</td>
		</tr>		
		
		<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_BlogAdmin_Options_InterestingPosts_RatingFactor_Help" ID="Helpicon3"/>
			<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Options_InterestingPosts_RatingFactor" controltolabel="createDirectories" ID="Formlabel3"/>
		</td>
		<td class="CommonFormField">
			<asp:TextBox id="RatingFactor" runat="Server" cssclass="ControlPanelTextInput" width="200px" />
			<asp:CompareValidator runat = "Server" ControlToValidate="RatingFactor" Operator = "DataTypeCheck" Type="Double" ID="Comparevalidator1">Invalid Format</asp:CompareValidator>
		</td>
		</tr>
		
		<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_BlogAdmin_Options_InterestingPosts_CommentFactor_Help" ID="Helpicon4"/>
			<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Options_InterestingPosts_CommentFactor" controltolabel="createDirectories" ID="Formlabel4"/>
		</td>
		<td class="CommonFormField">
			<asp:TextBox id="CommentFactor" runat="Server" cssclass="ControlPanelTextInput" width="200px" />
			<asp:CompareValidator runat = "Server" ControlToValidate="CommentFactor" Operator = "DataTypeCheck" Type="Double" ID="Comparevalidator2">Invalid Format</asp:CompareValidator>
		</td>
		</tr>
		
		<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_BlogAdmin_Options_InterestingPosts_ViewFactor_Help" ID="Helpicon5"/>
			<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Options_InterestingPosts_ViewFactor" controltolabel="createDirectories" ID="Formlabel5"/>
		</td>
		<td class="CommonFormField">
			<asp:TextBox id="ViewFactor" runat="Server" cssclass="ControlPanelTextInput" width="200px" />
			<asp:CompareValidator runat = "Server" ControlToValidate="ViewFactor" Operator = "DataTypeCheck" Type="Double" ID="Comparevalidator3">Invalid Format</asp:CompareValidator>
		</td>
		</tr>						
		
		</table>
	</div>
	<p class="PanelSaveButton DetailsFixedWidth">
		<cp:ResourceLinkButton id="SaveButton" runat="Server" cssclass="CommonTextButton" resourcename="Save" />
	</p>
	</asp:PlaceHolder>
</CP:Content>
</CP:Container>

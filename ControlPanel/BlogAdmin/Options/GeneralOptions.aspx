<%@ Page language="c#" Codebehind="GeneralOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.BlogAdmin.GeneralOptionsPage" %>
<CP:Container id="Mpcontainer1" runat="server" thememasterfile="SetupControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
	<CP:ResourceControl runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings" />
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">
	<div class="CommonDescription">
		<CP:ResourceControl runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_Instructions" />
	</div>
	<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Blogs" />
	<CP:FileOnlyStatusMessage id="FOStatus" runat="server" visible="false" />
	<asp:PlaceHolder id="OptionHolder" runat="Server">
	<CP:ConfigOKStatusMessage id="Status" runat="server" visible="false" />
	<div class="FixedWidthContainer">
		<table cellpadding="0" cellspacing="0" border="0">
		
		<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_CreateDirectories_Help" />
			<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_CreateDirectories" controltolabel="createDirectories" />
		</td>
		<td class="CommonFormField">
			<asp:checkbox id="createDirectories" runat="Server" cssclass="ControlPanelTextInput" />
		</td>
		</tr>
		
		<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_AutoCreate_Help" />
			<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_AutoCreate" controltolabel="checkAutoCreate" />
		</td>
		<td class="CommonFormField">
			<asp:checkbox id="checkAutoCreate" runat="Server" cssclass="ControlPanelTextInput" />
		</td>
		</tr>
		
		<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_EnableCrossPosting_Help" />
			<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_EnableCrossPosting" controltolabel="EnableCrossPosting" />
		</td>
		<td class="CommonFormField">
			<asp:checkbox id="EnableCrossPosting" runat="Server" cssclass="ControlPanelTextInput" />
		</td>
		</tr>
		
		<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_EnableRawHeadEditing_Help" />
			<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_EnableRawHeadEditing" controltolabel="EnableRawHeadEditing" />
		</td>
		<td class="CommonFormField">
			<asp:checkbox id="EnableRawHeadEditing" runat="Server" cssclass="ControlPanelTextInput" />
		</td>
		</tr>
		
		<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_EnableEmailSubscriptions_Help" />
			<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_EnableEmailSubscriptions" controltolabel="EnableEmailSubscriptions" />
		</td>
		<td class="CommonFormField">
			<asp:checkbox id="EnableEmailSubscriptions" runat="Server" cssclass="ControlPanelTextInput" />
		</td>
		</tr>

		<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_AutoCreateDefaultGroup_Help" />
			<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_AutoCreateDefaultGroup" controltolabel="GroupList" />&nbsp;
		</td>
		<td class="CommonFormField">
			<asp:dropdownlist id="GroupList" runat="Server" cssclass="ControlPanelTextInput" width = "300px" />
		</td>
		</tr>
		
		<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_PingServices_Help" />
			<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_PingServices" controltolabel="PingServices" />&nbsp;
		</td>
		<td class="CommonFormField">
			<asp:textbox id="PingServices" runat="Server" cssclass="ControlPanelTextInput" width="300" TextMode="Multiline" Rows="5"></asp:textbox>
		</td>
		</tr>

		<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_AggregateTags_Help" />
			<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_AggregateTags" controltolabel="PingServices" />&nbsp;
		</td>
		<td class="CommonFormField">
			<asp:textbox id="AggregateTags" runat="Server" cssclass="ControlPanelTextInput" width="300" TextMode="Multiline"></asp:textbox>
		</td>
		</tr>
		
		<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_ServicePostCountLimit_Help" />
			<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_ServicePostCountLimit" controltolabel="ServicePostCountLimit" />&nbsp;
		    <asp:CompareValidator runat = "Server" ControlToValidate="ServicePostCountLimit" Operator = "DataTypeCheck" Type="Integer" Display="Dynamic">Limit must be an integer</asp:CompareValidator>
		</td>
		<td class="CommonFormField">
			<asp:textbox id="ServicePostCountLimit" runat="Server" cssclass="ControlPanelTextInput" width="300"></asp:textbox>
		</td>
		</tr>		

		</table>
	</div>
	<p class="PanelSaveButton DetailsFixedWidth">
		<cp:ResourceLinkButton id="SaveButton" runat="Server" cssclass="CommonTextButton" resourcename="Save" />
	</p>
	</asp:PlaceHolder>
</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="SkinOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.BlogAdmin.SkinOptionsPage" %>
<CP:Container id="Mpcontainer1" runat="server" thememasterfile="SetupControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
	<CP:ResourceControl runat="server" resourcename="CP_BlogAdmin_Options_ThemeSettings" />
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">
	<div class="CommonDescription">
		<CP:ResourceControl runat="server" resourcename="CP_BlogAdmin_Options_ThemeSettings_Instructions" />
	</div>
	<CP:FileOnlyStatusMessage id="FOStatus" runat="server" visible="false" />
	<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Blogs" />
	<asp:PlaceHolder id="OptionHolder" runat="Server">
		<CP:ConfigOKStatusMessage id="Status" runat="server" visible="false" />
		<div class="FixedWidthContainer">
		<table cellSpacing="0" cellPadding="0" border="0">
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_BlogAdmin_Options_ThemeSettings_EnableThemes_Help" />
				<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Options_ThemeSettings_EnableThemes" controltolabel="enableThemes" />
			</td>
			<td class="CommonFormField">
				<asp:checkbox id="enableThemes" runat="Server" cssclass="ControlPanelTextInput" />
			</td>
		</tr>
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon runat="server" resourcename="CP_BlogAdmin_Options_ThemeSettings_DefaultTheme_Help" />
				<cp:formlabel runat="server" resourcename="CP_BlogAdmin_Options_ThemeSettings_DefaultTheme" controltolabel="defaultTheme" />
			</td>
			<td class="CommonFormField">
			    <TWC:DropDownList runat="server" ID="defaultTheme" ShowHtmlWhenSelected="false" SelectListWidth="440" SelectListHeight="375" />
			</td>
		</tr>
		
		<tr><td colspan="2">&nbsp;</td></tr>

		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_AggregatePostCount_Help" />
				<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_AggregatePostCount" controltolabel="aggregatePostCount" />&nbsp;
			</td>
			<td class="CommonFormField">
				<asp:textbox id="aggregatePostCount" runat="Server" cssclass="ControlPanelTextInput" width="50" />
				<asp:requiredfieldvalidator runat="Server" controltovalidate="aggregatePostCount" display="Dynamic" text="*" id="aggregatePostCountValidator"/>
			</td>
		</tr>
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_IndividualPostCount_Help" />
				<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_IndividualPostCount" controltolabel="individualPostCount" />&nbsp;
			</td>
			<td class="CommonFormField">
				<asp:textbox id="individualPostCount" runat="Server" cssclass="ControlPanelTextInput" width="50" />
				<asp:requiredfieldvalidator runat="Server" controltovalidate="individualPostCount" display="Dynamic" text="*" id="individualePostCountValidator"/>
			</td>
		</tr>
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_ServicePostCountLimit_Help" />
				<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Options_GeneralSettings_ServicePostCountLimit" controltolabel="servicePostCountLimit" />&nbsp;
			</td>
			<td class="CommonFormField">
				<asp:textbox id="servicePostCountLimit" runat="Server" cssclass="ControlPanelTextInput" width="50" />
				<asp:requiredfieldvalidator runat="Server" controltovalidate="servicePostCountLimit" display="Dynamic" text="*" id="servicePostCountValidator"/>
			</td>
		</tr>
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon runat="server" resourcename="CP_BlogAdmin_Options_ThemeSettings_AggregatePostSize_Help" />
				<cp:formlabel runat="server" resourcename="CP_BlogAdmin_Options_ThemeSettings_AggregatePostSize" controltolabel="aggregatePostSize" />
			</td>
			<td class="CommonFormField">
				<asp:textbox id="aggregatePostSize" runat="Server" width="50" CssClass="ControlPanelTextInput" />
				<asp:requiredfieldvalidator id="PostSizeValidator" runat="server" text="*" display="Dynamic" controltovalidate="aggregatePostSize" />
			</td>
		</tr>
		</table>
		</div>
		<P class="PanelSaveButton DetailsFixedWidth">
			<cp:ResourceLinkButton id="SaveButton" runat="Server" resourcename="Save" cssclass="CommonTextButton" />
		</P>
	</asp:PlaceHolder>
</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="FileStorageOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.BlogAdmin.FileStorageOptions" %>
<CP:Container id="Mpcontainer1" runat="server" thememasterfile="SetupControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
	<CP:ResourceControl runat="Server" resourcename="CP_BlogAdmin_Options_FileStorage_Title" />
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">
	<div class="CommonDescription">
		<CP:ResourceControl runat="Server" resourcename="CP_BlogAdmin_Options_FileStorage_SubTitle" />
	</div>
	<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Blogs" />
	<CP:FileOnlyStatusMessage id="FOStatus" runat="server" visible="false" />
	<asp:PlaceHolder id="OptionHolder" runat="Server">
	<CP:ConfigOKStatusMessage id="Status" runat="server" visible="false" />
	
	<div class="FixedWidthContainer">
	<table cellpadding="0" cellspacing="0" border="0">
	
	<tr>
	<td class="CommonFormField" colspan="2">
		<CP:ResourceControl runat="Server" resourcename="CP_BlogAdmin_Options_FileStorage_WritePath" />
		<asp:Literal id="WritePath" runat="Server"/>
	</td>
	</tr>		
	<tr>
	<td class="CommonFormField" colspan="2">
		<CP:ResourceControl runat="Server" resourcename="CP_BlogAdmin_Options_FileStorage_ReadPath" />
		<asp:Literal id="ReadPath" runat="Server"/>
	</td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr>
	<td class="CommonFormFieldName">
		<cp:helpicon runat="Server" resourcename="CP_BlogAdmin_Options_FileStorage_EnableBlogFileStorage_Help" />
		<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Options_FileStorage_EnableBlogFileStorage" controltolabel="EnableBlogFileStorage" />
	</td>
	<td class="CommonFormField">
		<asp:checkbox id="EnableBlogFileStorage" runat="Server" cssclass="ControlPanelTextInput" />
	</td>
	</tr>		

	<tr>
	<td class="CommonFormFieldName">
		<cp:helpicon runat="Server" resourcename="CP_BlogAdmin_Options_FileStorage_BlogFilesQuota_Help" />
		<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Options_FileStorage_BlogFilesQuota" controltolabel="BlogFilesQuota" />
	</td>
	<td class="CommonFormField">
		<asp:TextBox id="BlogFilesQuota" runat="Server" cssclass="ControlPanelTextInput" />
		<CP:ResourceControl runat="Server" resourcename="CP_Blog_Files_KiloBytes" />
	</td>
	</tr>		

	<tr>
	<td class="CommonFormFieldName">
		<cp:helpicon runat="Server" resourcename="CP_BlogAdmin_Options_FileStorage_BlogFilesMaxSize_Help" />
		<cp:formlabel ID="Formlabel1" runat="Server" resourcename="CP_BlogAdmin_Options_FileStorage_BlogFilesMaxSize" controltolabel="BlogFilesQuota" />
	</td>
	<td class="CommonFormField">
		<asp:TextBox id="BlogFilesMaxSize" runat="Server" cssclass="ControlPanelTextInput" />
		<CP:ResourceControl runat="Server" resourcename="CP_Blog_Files_KiloBytes" />
	</td>
	</tr>		
	
	<tr>
	<td class="CommonFormFieldName">
		<cp:helpicon runat="Server" resourcename="CP_BlogAdmin_Options_FileStorage_BlogFileStorageExtensions_Help" />
		<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Options_FileStorage_BlogFileStorageExtensions" controltolabel="BlogFileStorageExtensions" />
	</td>
	<td class="CommonFormField">
		<asp:TextBox id="BlogFileStorageExtensions" runat="Server" cssclass="ControlPanelTextInput" width="200px" />
	</td>
	</tr>
	
	<tr>
	<td class="CommonFormFieldName">
		<cp:helpicon runat="Server" resourcename="CP_BlogAdmin_Options_FileStorage_BlogFileStorageHiddenFiles_Help" />
		<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Options_FileStorage_BlogFileStorageHiddenFiles" controltolabel="BlogFileStorageHiddenFiles" />
	</td>
	<td class="CommonFormField">
		<asp:TextBox id="BlogFileStorageHiddenFiles" runat="Server" cssclass="ControlPanelTextInput" width="200px" />
	</td>
	</tr>
	
	</table>
	</div>
	
	<p class="PanelSaveButton DetailsFixedWidth">
		<cp:ResourceLinkButton id="SaveButton" runat="Server" cssclass="CommonTextButton" resourcename="Save" />
	</p>
	</asp:PlaceHolder>
</CP:Content>
</CP:Container>

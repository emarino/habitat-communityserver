<%@ Page language="c#" Codebehind="Organize.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.BlogAdmin.OrganizePage" %>
<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		Organize Blogs
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
	
	<script type="text/javascript" >
	// <![CDATA[
	function Docked(item)
	{
	   var id = arguments[0].Id;
	   var groupid = arguments[1];
       var index = 	  arguments[2];

	   
	   OrganizePage.MoveBlog(id,groupid,index,null);
	}
	// ]]>
	</script>
		
		<div class="CommonDescription">
			Drag the blogs below to a new group.
		</div>
		
		<asp:DataList runat = "Server" id = "GroupList" RepeatDirection = "Horizontal" RepeatColumns = "2" style="margin-top:5px">
		
		    <ItemTemplate>
		        <div style="width:300px;vertical-align:top;" >
		        <div style="font-weight:bold;"><%# DataBinder.Eval(Container.DataItem, "Name") %> (<%# DataBinder.Eval(Container.DataItem, "GroupID") %>)</div>
		        <div style="Height:200px;width:300px" id = "<%# DataBinder.Eval(Container.DataItem, "GroupID") %>"></div>
		        </div>
		        
		    </ItemTemplate>
		    <ItemStyle backcolor="white" BorderColor="black" BorderWidth="1" VerticalAlign="top" />
		    <AlternatingItemStyle backcolor = "#F5F5F5" BorderColor="black" BorderWidth="1" />
		    
		    
		
		</asp:DataList>
		
		<asp:Repeater runat = "Server" id = "BlogList" >
		    <ItemTemplate>
		    
            <CA:Snap id="Snap1" Height="70" Width="160"
             CurrentDockingContainer='<%# DataBinder.Eval(Container.DataItem, "GroupID") %>' CurrentDockingIndex="0" CollapseDuration="200"
            ExpandDuration="200" DraggingStyle="GhostCopy" DockingStyle="TransparentRectangle" MustBeDocked="True"
            IsCollapsed="false" runat="server" ClientSideOnDock = "Docked">
		    <Content />

		    </CA:Snap>
		    
		    </ItemTemplate>
		    
		</asp:Repeater>
		

	</CP:Content>
</CP:Container>

<%@ Import Namespace = "CommunityServer.Components" %>
<%@ Page language="c#" Codebehind="BlogFeedsManage.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.BlogAdmin.BlogFeedsManage" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Import Namespace = "CommunityServer.Components" %>
<%@ Import Namespace = "CommunityServer.Blogs.Components" %>
<%@ Import Namespace = "CommunityServer.ControlPanel" %>
<cp:controlpanelselectednavigation selectednavitem="Blogs" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="Mpcontainer1" thememasterfile="SetupControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
	<cp:resourcecontrol id="SectionDescription" runat="server" resourcename="CP_BlogAdmin_Blogs_FeedsTitle" />
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">

<script language = "javascript" type="text/javascript">
function checkReload(res)
{
	if(res)
	{
		refresh();
	}
}
</script>	

<div class="CommonDescription">
	<ASP:Literal id="ExistingMessage" runat="Server" />
</div>

<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Mirrors" />

<div class="PanelSaveButton">
	<CP:ModalLink CssClass="CommonTextButtonBig" ModalType="Link" Height="300" Width="600" runat="Server" ResourceName="CP_BlogAdmin_Feeds_CreateNewFeed" Callback="checkReload" id="NewFeed" />
</div>

<div class="CommonListArea">
<table id="BlogLists" cellSpacing="0" cellPadding="0" border="0" width="100%">
<thead>
<tr>
    <th class="CommonListHeaderLeftMost" ><CP:ResourceControl runat="server" ResourceName="CP_BlogAdmin_Feeds_List_Name" /></CP:ResourceControl></th>
    <th class="CommonListHeader"><CP:ResourceControl runat="server" ResourceName="CP_BlogAdmin_Feeds_List_Url" /></th>
    <th class="CommonListHeader"><CP:ResourceControl runat="server" ResourceName="CP_BlogAdmin_Feeds_List_PullInterval" /></th>
    <th class="CommonListHeader"><CP:ResourceControl runat="server" ResourceName="CP_BlogAdmin_Feeds_List_LastPull" /></th>    
    <th class="CommonListHeader"><CP:ResourceControl runat="server" ResourceName="CP_BlogAdmin_Feeds_List_Actions" /></th>    
</tr>
</thead>
<asp:Repeater runat = "Server" id = "mirrorFeeds">
<ItemTemplate>
<tr>
    <td class="CommonListCellLeftMost"><%# DataBinder.Eval(Container.DataItem,"Title")%></td>
    <td class="CommonListCell"><a href="<%# DataBinder.Eval(Container.DataItem,"Url")%>" target="_blank"><%# DataBinder.Eval(Container.DataItem,"Url")%></a></td>
    <td  class="CommonListCell"><%# DataBinder.Eval(Container.DataItem,"IntervalMinutes") %></td>
    <td  class="CommonListCell"><%# DataBinder.Eval(Container.DataItem,"LastUpdateDate") %></td>
	<td class="CommonListCell" nowrap ="true">
		<a href="javascript:Telligent_Modal.Open('FeedForm.aspx?SectionID=<%# DataBinder.Eval(Container.DataItem,"SectionID") %>&UrlID=<%# DataBinder.Eval(Container.DataItem,"UrlId") %>', 600, 300, checkReload);" class="CommonTextButton">Edit</a>
		<a onclick="return confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_BlogAdmin_Feeds_DeleteWarning") %>');" href="BlogFeedsManage.aspx?action=delete&amp;SectionID=<%# DataBinder.Eval(Container.DataItem,"SectionID")%>&amp;UrlID=<%# DataBinder.Eval(Container.DataItem,"UrlId")%>" class="CommonTextButton">Delete</a> 
	</td>    
</tr>
</ItemTemplate>
<AlternatingItemTemplate>
<tr class="AltListRow">
    <td class="CommonListCellLeftMost"><%# DataBinder.Eval(Container.DataItem,"Title")%></td>
    <td class="CommonListCell"><a href="<%# DataBinder.Eval(Container.DataItem,"Url")%>" target="_blank"><%# DataBinder.Eval(Container.DataItem,"Url")%></a></td>
    <td  class="CommonListCell"><%# DataBinder.Eval(Container.DataItem,"IntervalMinutes") %></td>
    <td  class="CommonListCell"><%# DataBinder.Eval(Container.DataItem,"LastUpdateDate") %></td>
	<td class="CommonListCell" nowrap ="true">
    	<a href="javascript:Telligent_Modal.Open('FeedForm.aspx?SectionID=<%# DataBinder.Eval(Container.DataItem,"SectionID") %>&UrlID=<%# DataBinder.Eval(Container.DataItem,"UrlId") %>', 600, 300, checkReload);" class="CommonTextButton">Edit</a>
		<a onclick="return confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_BlogAdmin_Feeds_DeleteWarning") %>');" href="BlogFeedsManage.aspx?action=delete&amp;SectionID=<%# DataBinder.Eval(Container.DataItem,"SectionID")%>&amp;UrlID=<%# DataBinder.Eval(Container.DataItem,"UrlId")%>" class="CommonTextButton">Delete</a> 
	</td> 
</tr>
</AlternatingItemTemplate>
</asp:Repeater>
</table>
</div>		

</CP:Content>
</CP:Container>

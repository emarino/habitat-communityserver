<%@ Control Language="c#" AutoEventWireup="false" Codebehind="GroupTreeViewControl.ascx.cs" Inherits="CommunityServer.ControlPanel.BlogAdmin.GroupTreeViewControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<script type="text/javascript">
// <![CDATA[
var nodeLastSelected = null;
var nodeMoved = null;
var nodeNeedsResorted = false;
var newSectionNode;
var groupNodeSelected = null;
var newNodeSeq = 0;
var newNodeGroupID = 0;
window.top.location

// Splits the node's value and returns the GroupID part
function getGroupID(node)
{
	splits = node.GetProperty('Value').split(":");
	return splits[0];
}

// Splits the node's value and returns the SectionID part
function getSectionID(node)
{
	splits = node.GetProperty('Value').split(":");
	return splits[1];
}
function updateGroupNode(newName)
{
	var updateNode = window.<%=this.Tree.ClientObjectId%>.SelectedNode
	if(updateNode == null)
		window.location = window.location;
		
	updateNode.SetProperty('Text',newName);
	//updateNode.SaveState();
	window.<%=this.Tree.ClientObjectId%>.Render();
}
function updateSectionNode(newName, isActive)
{
	var updateNode = window.<%=this.Tree.ClientObjectId%>.SelectedNode
	if(updateNode == null)
		window.location = window.location;
	
	if(isActive == 'True') //used for enabled
	{
		//updateNode.ImageUrl = "notes.gif";
		//updateNode.Checked = 1;
		updateNode.SetProperty('ImageUrl', "notes.gif");
		updateNode.SetProperty('Checked', 1);
	}
	else
	{
		//updateNode.ImageUrl = "notesdisabled.gif";
		//updateNode.Checked = null;
		updateNode.SetProperty('ImageUrl', "notesdisabled.gif");
		updateNode.SetProperty('Checked', null);
	}
	
	updateNode.SetProperty('Text',newName);
	//updateNode.SaveState();
	window.<%=this.Tree.ClientObjectId%>.Render();
}
function treeContextMenu(treeNode, e)
{
	nodeLastSelected = treeNode;
	groupID = getGroupID(treeNode);
	sectionID = getSectionID(treeNode);
	
	// Determine whether to call Group or Section context menu
	if (sectionID > 0)
	{
		if(treeNode.Checked == 1) //used for enabled
			window.<%=this.SectionEnabledContextMenu.ClientID%>.ShowContextMenu(treeNode, e, groupID, sectionID);
		else
			window.<%=this.SectionDisabledContextMenu.ClientID%>.ShowContextMenu(treeNode, e, groupID, sectionID);
	}
	else
		window.<%=this.GroupContextMenu.ClientID%>.ShowContextMenu(treeNode, e, groupID, sectionID);
		
	return true;
}

function nodeSelect(node)
{
	nodeLastSelected = node;
	if (getSectionID(node) < 0)
	{
		groupNodeSelected = node;
		window.frames["sectionEditFrame"].location  = "IFrameHost.aspx?SectionID=0&GroupID=" + getGroupID(node);
	}
	else
	{
		window.frames["sectionEditFrame"].location  = "IFrameHost.aspx?SectionID=" + getSectionID(node) + "&GroupID=" + getGroupID(node);
	}
}

function nodeRename(sourceNode, newName)
{ 
	newName = newName.replace(/^\s*|\s*$/g,"");
	if(newName.length == 0)
	{
		alert('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_BlankGroupName") %>');
		return false;
	}

	window.<%=this.Tree.ClientObjectId%>.SelectedNode.SetProperty('Text', newName);
	
	// Determine whether this is a new node (doesn't exist server-side yet) or an existing one
	var groupID = getGroupID(sourceNode);
	var sectionID = getSectionID(sourceNode);
	
	//CA is allowing the user to edit the new section node so trap it here
	if (sectionID > 0)
	{
		window.location = window.location;
	}
	if (sectionID == 0)
	{
		window.<%=this.Tree.ClientObjectId%>.Render();
		GroupTreeViewControl.AddSection('<%= this.ClientID %>', groupID, newName, AddSectionCallBack);
		return false;
	}
	if (groupID > 0)
	{
		// Existing Node - just rename
		RenameGroup(groupID, newName);
	}
	else
	{
		// New node - add it to the Groups collection server-side
	  	AddGroup(newName);
	}


  return true; 
}

function checkNodeIndex(node)
{
	if (nodeNeedsResorted == true && nodeMoved != null)
	{
		nodeNeedsResorted = false;
		index = nodeMoved.GetCurrentIndex();
		groupID = getGroupID(nodeMoved);
		sectionID = getSectionID(nodeMoved);
		nodeMoved = null;
		
		ReorderForumOrGroup(groupID, sectionID, index);
	}
}



function UpdateGroupNode(name)
{
	if (groupNodeSelected != null)
	{
		//groupNodeSelected.Text = name;
		groupNodeSelected.SetProperty('Text', name);
		//groupNodeSelected.SaveState();
		groupNodeSelected.ParentTreeView.Render();
	}
}

//function UpdateSectionNode(groupID, sectionID, name)
//{
//	if (newSectionNode != null)
//	{
//		newSectionNode.Text = name;
//		newSectionNode.Value = groupID + ":" + sectionID;
//		newSectionNode.NavigateUrl = "IFrameHost.aspx?GroupID=" + groupID + "&SectionID=" + sectionID;
//		newSectionNode.Target = "sectionEditFrame";
//		newSectionNode.SaveState();
//		newSectionNode.ParentTreeView.Render();
//	}
//}

//## MOVE ##

function nodeMove(sourceNode, targetNode)
{
	nodeMoved = sourceNode;
	nodeNeedsResorted = false;

	sourceSectionID = getSectionID(sourceNode);
	sourceGroupID = getGroupID(sourceNode);
	targetGroupID = getGroupID(targetNode);
	targetSectionID = getSectionID(targetNode);
	
	//alert(sourceGroupID + ':' + sourceSectionID + ':' + targetGroupID + ':' + targetSectionID);
	
	// Check if user is trying to drop a group inside a group or forum
	if (sourceSectionID < 0)
	{
		if (targetGroupID > 0)
		{
			//dragging a group into a group or section
			alert("<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_BlogAdmin_GroupManager_NestGroupError") %>");
			return false;
		}
		
		nodeNeedsResorted = true;
	}

	if (sourceSectionID > 0)
	{
		if (targetNode == null || targetNode.ID == "RootNode")
		{
			//dragging a section to the root
			alert('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_BlogAdmin_GroupManager_FolderToRootError") %>');
			return false;
		}
	
		//check if the user is trying to drop a section in a section
		//(it might be safe just to assume the selected group andpass in -1 for the section id)
		if (targetSectionID != -1)
		{
			alert('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_NodeMoveError") %>');
			return false;
		}
		MoveSection(sourceGroupID, sourceSectionID, targetGroupID, targetSectionID);
	}
	

	return true; 
}
function MoveSection(groupID ,sectionID, targetGroupID, targetSectionID)
{
	GroupTreeViewControl.MoveSection('<%= this.ClientID %>',groupID ,sectionID, targetGroupID, targetSectionID, MoveSectionCallBack);
}
function MoveSectionCallBack(res)
{
	// Check if this is a section that needs the sort order changed
	if (res.value == "-1")
	{
		if (nodeMoved != null)
		{
			index = nodeMoved.GetCurrentIndex();
			groupID = getGroupID(nodeMoved);
			sectionID = getSectionID(nodeMoved);
			nodeMoved = null;
		
			ReorderForumOrGroup(groupID, sectionID, index);
		}
	}
	else
	{
		var retVals = res.value.split("^");
		var groupID = retVals[0];
		var newGroupID = retVals[1];
		var sectionID = retVals[2];
		var newSectionID = retVals[3];
		
		var nodeMoved = window.<%=this.Tree.ClientObjectId%>.FindNodeById(groupID + ":" + sectionID);
		
		//nodeMoved.Value = newGroupID + ":" + sectionID;
		//nodeMoved.ID = newGroupID + ":" + sectionID;
		//nodeMoved.NavigateUrl = 'IFrameHost.aspx?GroupID=' + newGroupID + '&SectionID=' + sectionID;
		//nodeMoved.Target = "sectionEditFrame";

		nodeMoved.SetProperty('Value', newGroupID + ":" + sectionID);
		nodeMoved.SetProperty('ID', newGroupID + ":" + sectionID);
		nodeMoved.SetProperty('NavigateUrl', 'IFrameHost.aspx?GroupID=' + newGroupID + '&SectionID=' + sectionID);
		nodeMoved.SetProperty('Target', "sectionEditFrame");

		window.<%=this.Tree.ClientObjectId%>.Render();
		
		var selectedNode = window.<%=this.Tree.ClientObjectId%>.SelectedNode;
		if (selectedNode != null && getSectionID(selectedNode) == sectionID)
		{
			nodeSelect(nodeMoved);
		}
	}
}

//## MOVE REORDER ##
function ReorderForumOrGroup(groupID, sectionID, index)
{
	GroupTreeViewControl.ReorderForumOrGroup('<%= this.ClientID %>', groupID, sectionID, index, NoResponseCallBack);
}
function NoResponseCallBack(res)
{
	// Do Nothing
}
//## END MOVE REORDER ##

//## END MOVE ##

function RenameGroup(groupID, newName)
{
	GroupTreeViewControl.RenameGroup('<%= this.ClientID %>',groupID, newName, RenameGroupCallBack);
}

function RenameGroupCallBack(res)
{
	// Reload group edit control
	groupID = res.value;
	if (groupID > 0)
	{
		window.frames["sectionEditFrame"].location  = "IFrameHost.aspx?SectionID=0&GroupID=" + groupID;
	}
	else
	{
		alert('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_DuplicateGroupName") %>');
		//refresh the page
		window.location = window.location;
	}
}

//## ADD ##

//## ADD GROUP ##
function AddGroup()
{
	toggleButtons();
	GroupTreeViewControl.AddGroup('<%= this.ClientID %>', AddGroupCallBack);
}
       
function AddGroupCallBack(res)
{
	var retVals = res.value.split("^");
	var groupID = retVals[0];
	var groupName = retVals[1];

	if (groupID == '-1')
	{
		alert('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_DuplicateGroupName") %>');
	}
	else
	{
		AddGroupNode(groupID, groupName)
		toggleButtons();
		window.frames["sectionEditFrame"].location  = "IFrameHost.aspx?SectionID=0&GroupID=" + groupID;
	}
	return;
}

function AddGroupNode(groupID, groupName)
{
	window.<%=this.Tree.ClientObjectId%>.SelectedNode = null;
	var newGroupNode = new ComponentArt_TreeViewNode();
	//newGroupNode.Text = groupName;
	//newGroupNode.ImageUrl = "folders.gif";
	//newGroupNode.EditingEnabled = true;
	//newGroupNode.DraggingEnabled = true;
	//newGroupNode.DroppingEnabled = true;
	//newGroupNode.ClientSideOnNodeSelect="nodeSelect";
	//newGroupNode.Selectable = true;
	//newGroupNode.Value = groupID + ":-1";
	//newGroupNode.ID = groupID + ":-1";
	
	newGroupNode.SetProperty('Text', groupName);
	newGroupNode.SetProperty('ImageUrl', "folders.gif");
	newGroupNode.SetProperty('EditingEnabled', true);
	newGroupNode.SetProperty('DraggingEnabled', true);
	newGroupNode.SetProperty('DroppingEnabled', true);
	newGroupNode.SetProperty('ClientSideOnNodeSelect', "nodeSelect");
	newGroupNode.SetProperty('Selectable', true);
	newGroupNode.SetProperty('Value', groupID + ":-1");
	newGroupNode.SetProperty('ID', groupID + ":-1");
	
	var rootNode = window.<%=this.Tree.ClientObjectId%>.FindNodeById("RootNode");
	if (rootNode != null)
	{
		groupNodeSelected = newGroupNode;
		rootNode.AddNode(newGroupNode);
		window.<%=this.Tree.ClientObjectId%>.SelectedNode = newGroupNode;
		window.<%=this.Tree.ClientObjectId%>.Render();
	}
	
}

//## ADD SECTION ##

function AddSection(groupID)
{
	toggleButtons();
	
	selectedNode = window.<%=this.Tree.ClientObjectId%>.SelectedNode;

	// If a GroupID was not passed, get it from the selected node
	if (groupID == null)
	{
		if (selectedNode == null)
		{
			alert("<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_GroupManager_PleaseSelectAGroup") %>");
			toggleButtons();
			return;
		}
		else
			groupID = getGroupID(selectedNode);
	}

	newNodeSeq = newNodeSeq + 1; //ensure unique key names
	newNodeGroupID = groupID;
	AddSectionNode(groupID, '0:'+newNodeSeq, '')
	var newNode = window.<%=this.Tree.ClientObjectId%>.FindNodeById(groupID + ":0:"+newNodeSeq);
	//newNode.EditingEnabled = true;
	//newNode.DraggingEnabled = false;
	//newNode.DroppingEnabled = false;
	
	newNode.SetProperty('EditingEnabled', true);
	newNode.SetProperty('DraggingEnabled', false);
	newNode.SetProperty('DroppingEnabled', false);

	//newNode.SaveState();
	window.<%=this.Tree.ClientObjectId%>.Render();
	newNode.Edit();
}

function AddSectionCallBack(res)
{
	cleanupNewNode();
	if (res.value == '0')
	{
		alert("<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_BlogAdmin_SectionEdit_Settings_AppKey_DuplicateNameException") %>");
	}
	else if(res.value != '-1')
	{
		var retVals = res.value.split("^");
		var groupID = retVals[0];
		var sectionID = retVals[1];
		var sectionName = retVals[2];
		
	
		AddSectionNode(groupID, sectionID, sectionName)
		window.frames["sectionEditFrame"].location  = "IFrameHost.aspx?SectionID=" + sectionID + "&GroupID=" + groupID;
	}
	toggleButtons();
}
function cleanupNewNode()
{
	var newNode = window.<%=this.Tree.ClientObjectId%>.FindNodeById(newNodeGroupID + ":0:"+newNodeSeq);
	window.<%=this.Tree.ClientObjectId%>.SelectedNode = null;
		//var parentNode = sourceNode.ParentNode;
	newNode.Remove();
	//newNode.SaveState();
	window.<%=this.Tree.ClientObjectId%>.Render();
}
function AddSectionNode(groupID, sectionID, name)
{
	var rootNode = window.<%=this.Tree.ClientObjectId%>.FindNodeById(groupID + ":-1");
	if(rootNode != null)
	{
		var newSectionNode = new ComponentArt_TreeViewNode();
		//newSectionNode.Text = name;
		//newSectionNode.ImageUrl = "notesdisabled.gif";
		//newSectionNode.Checked = null;
		//newSectionNode.EditingEnabled = false;
		//newSectionNode.NodeEditingEnabled = false;
		//newSectionNode.DraggingEnabled = true;
		//newSectionNode.DroppingEnabled = true;
		//newSectionNode.ClientSideOnNodeSelect="nodeSelect";
		//newSectionNode.Selectable = true;
		//newSectionNode.Value = groupID + ":" + sectionID;
		//newSectionNode.ID = groupID + ":" + sectionID;

		newSectionNode.SetProperty('Text', name);
		newSectionNode.SetProperty('ImageUrl', "notesdisabled.gif");
		newSectionNode.SetProperty('Checked', null);
		newSectionNode.SetProperty('EditingEnabled', false);
		newSectionNode.SetProperty('DraggingEnabled', true);
		newSectionNode.SetProperty('DroppingEnabled', true);
		newSectionNode.SetProperty('ClientSideOnNodeSelect', "nodeSelect");
		newSectionNode.SetProperty('Selectable', true);
		newSectionNode.SetProperty('Value', groupID + ":" + sectionID);
		newSectionNode.SetProperty('ID', groupID + ":" + sectionID);
		rootNode.AddNode(newSectionNode);
		window.<%=this.Tree.ClientObjectId%>.SelectedNode = newSectionNode;
		window.<%=this.Tree.ClientObjectId%>.Render();
		
		rootNode.Expand();
		window.<%=this.Tree.ClientObjectId%>.Render();

	}
	else
	{
		window.location = window.location;
	}
	
}

//## END ADD ##

//## ENABLE SECTION TOGGLE ##

ChangeSectionEnabled
function ChangeSectionEnabled(groupID, sectionID )
{
	GroupTreeViewControl.ChangeSectionEnabled('<%= this.ClientID %>', groupID, sectionID, ChangeSectionEnabledCallBack);
}
function ChangeSectionEnabledCallBack(res)
{
	if(res.value == '-1') //not an admin
		return;
		
	var retVals = res.value.split("^");
	var groupID = retVals[0];
	var sectionID = retVals[1];

	var changedNode = window.<%=this.Tree.ClientObjectId%>.FindNodeById(groupID + ":" + sectionID);
	if (changedNode != null)
	{
		if(changedNode.Checked == 1) //used for enabled
		{
			//changedNode.ImageUrl = "notesdisabled.gif";
			//changedNode.Checked = null;
			changedNode.SetProperty('ImageUrl', "notesdisabled.gif");
			changedNode.SetProperty('Checked', null);
		}
		else
		{
			//changedNode.ImageUrl = "notes.gif";
			//changedNode.Checked = 1;
			changedNode.SetProperty('ImageUrl', "notes.gif");
			changedNode.SetProperty('Checked',  1);
		}
		//changedNode.SaveState();
		window.<%=this.Tree.ClientObjectId%>.Render();
		
		//update the iframe
		if(window.<%=this.Tree.ClientObjectId%>.SelectedNode != null && changedNode.ID == window.<%=this.Tree.ClientObjectId%>.SelectedNode.ID)
			nodeSelect(changedNode); 
	}
	
	return;
}

function toggleButtons()
{
	toggleVisibility('<%=this.AddGroupButtonClientID %>');
	toggleVisibility('<%=this.DeleteButtonClientID  %>');
	toggleVisibility('<%=this.AddSectionButtonClientID  %>');
}
function toggleVisibility( targetId ){ 
  if (document.getElementById){ 
        target = document.getElementById( targetId ); 
           if (target.style.display == "none"){ 
              target.style.display = ""; 
           } else { 
              target.style.display = "none"; 
           } 
     } 
} 

//## DELETE ##
function DeleteSelected(groupID, sectionID)
{
	// If a GroupID was not passed, get it from the selected node
	if (groupID == null || sectionID == null)
	{
		if (window.<%=this.Tree.ClientObjectId%>.SelectedNode == null)
			return;
		else
			nodeID = window.<%=this.Tree.ClientObjectId%>.SelectedNode.ID;

		var vals = nodeID.split(":");
		groupID = vals[0];
		sectionID = vals[1];
	}
	
	if( sectionID == '-1' && groupID == '-1')
		return;
	
	//clear the iframe if we are deleting the current item
	if (window.<%=this.Tree.ClientObjectId%>.SelectedNode != null)
	{
		var theCurrentNode = window.<%=this.Tree.ClientObjectId%>.SelectedNode
		if(getSectionID(theCurrentNode) == sectionID &&  getGroupID(theCurrentNode) == groupID)
			window.frames["sectionEditFrame"].location  = "IFrameHost.aspx?SectionID=0&GroupID=0";
	}
		
	if( sectionID != '-1')
		DeleteSection(groupID, sectionID);
	else
		DeleteGroup(groupID);
}

//## DELETE GROUP ##

function DeleteGroup(groupID)
{
	GroupTreeViewControl.DeleteGroup('<%= this.ClientID %>', groupID, DeleteGroupCallBack);
}

function DeleteGroupCallBack(res)
{
	if (res.value == null)
	{
		alert('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Forums_TreeView_DeleteGroupError") %>');
		return;
	}
	
	if (res.value == 0)
	{
		alert('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_BlogAdmin_GroupList_GroupNotEmptyException") %>');
		return;
	}
	
	var deletedNode = window.<%=this.Tree.ClientObjectId%>.FindNodeById(res.value + ":-1");
	if (deletedNode != null)
	{
		deletedNode.Remove();
		window.<%=this.Tree.ClientObjectId%>.Render();
	}
		
	return;
}

//## DELETE SECTION ##
function DeleteSection(groupID, sectionID)
{
	if(confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_BlogAdmin_Blogs_Delete_Warning") %>'))
		GroupTreeViewControl.DeleteSection('<%= this.ClientID %>', groupID, sectionID,  DeleteSectionCallBack);
}

function DeleteSectionCallBack(res)
{

	if (res.value == null)
	{
		alert('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_BlogAdmin_GroupList_SectionDeleteException") %>');
		return;
	}
	
	if (res.value == "0")
	{
		alert('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_BlogAdmin_GroupList_SectionDeleteException") %>');
		return;
	}
	
	var retVals = res.value.split("^");
	var groupID = retVals[0];
	var sectionID = retVals[1];
	
	var deletedNode = window.<%=this.Tree.ClientObjectId%>.FindNodeById(groupID + ":" + sectionID);
	if (deletedNode != null)
	{
		deletedNode.Remove();
		window.<%=this.Tree.ClientObjectId%>.Render();
	}
		
	return;
}
//## END DELETE ##

//for splitter support
function resizeCategoryTree(DomElementId, NewPaneHeight, NewPaneWidth)
    {
      // Forces the treeview to adjust to the new size of its container 
		window.<%=this.Tree.ClientID%>.Render();
		if(DomElementId != null)
			resizeDetails();
    }

// ]]>
</script>
<cp:contextmenu filename="~/ControlPanel/BlogAdmin/GroupNodeContextMenu.config" runat="server" id="GroupContextMenu" onmenuopenscript="" onmenuclosescript="">
    <SkinTemplate>
        <CA:Menu runat="server" 
	        id="Menu" 
	        AutoPostBackOnSelect="false" 
	        ContextMenu="Custom" 
	        Orientation="Vertical"
	        CssClass="CommonContextMenuGroup"
	        DefaultGroupCssClass="CommonContextMenuGroup"
	        DefaultItemLookID="DefaultItemLook" >
		        <ItemLooks>
			        <CA:ItemLook LookID="DefaultItemLook" 
				        CssClass="CommonContextMenuItem" 
				        HoverCssClass="CommonContextMenuItemHover" 
				        ExpandedCssClass="CommonContextMenuItemExpanded" 
				        LabelPaddingLeft="5" 
				        LabelPaddingRight="15" 
				        LabelPaddingTop="2" 
				        LabelPaddingBottom="2" />
		        </ItemLooks>	
        </CA:Menu>
    </SkinTemplate>
</cp:contextmenu>
<cp:contextmenu filename="~/ControlPanel/BlogAdmin/SectionNodeEnabledContextMenu.config" runat="server" id="SectionEnabledContextMenu" onmenuopenscript="" onmenuclosescript="">
    <SkinTemplate>
        <CA:Menu runat="server" 
	        id="Menu" 
	        AutoPostBackOnSelect="false" 
	        ContextMenu="Custom" 
	        Orientation="Vertical"
	        CssClass="CommonContextMenuGroup"
	        DefaultGroupCssClass="CommonContextMenuGroup"
	        DefaultItemLookID="DefaultItemLook" >
		        <ItemLooks>
			        <CA:ItemLook LookID="DefaultItemLook" 
				        CssClass="CommonContextMenuItem" 
				        HoverCssClass="CommonContextMenuItemHover" 
				        ExpandedCssClass="CommonContextMenuItemExpanded" 
				        LabelPaddingLeft="5" 
				        LabelPaddingRight="15" 
				        LabelPaddingTop="2" 
				        LabelPaddingBottom="2" />
		        </ItemLooks>	
        </CA:Menu>
    </SkinTemplate>
</cp:contextmenu>
<cp:contextmenu filename="~/ControlPanel/BlogAdmin/SectionNodeDisabledContextMenu.config" runat="server" id="SectionDisabledContextMenu" onmenuopenscript="" onmenuclosescript="">
    <SkinTemplate>
        <CA:Menu runat="server" 
	        id="Menu" 
	        AutoPostBackOnSelect="false" 
	        ContextMenu="Custom" 
	        Orientation="Vertical"
	        CssClass="CommonContextMenuGroup"
	        DefaultGroupCssClass="CommonContextMenuGroup"
	        DefaultItemLookID="DefaultItemLook" >
		        <ItemLooks>
			        <CA:ItemLook LookID="DefaultItemLook" 
				        CssClass="CommonContextMenuItem" 
				        HoverCssClass="CommonContextMenuItemHover" 
				        ExpandedCssClass="CommonContextMenuItemExpanded" 
				        LabelPaddingLeft="5" 
				        LabelPaddingRight="15" 
				        LabelPaddingTop="2" 
				        LabelPaddingBottom="2" />
		        </ItemLooks>	
        </CA:Menu>
    </SkinTemplate>
</cp:contextmenu>
<ca:treeview id="Tree" Width="100%" Height="100%" draganddropenabled="true" nodeeditingenabled="true"
	keyboardenabled="true" AllowTextSelection="true" cssclass="ForumFolderTree" nodecssclass="ForumFolderTreeNode"
	selectednodecssclass="ForumFolderTreeNodeSelected" hovernodecssclass="ForumFolderTreeNodeHover"
	lineimagewidth="19" lineimageheight="20" defaultimagewidth="16" defaultimageheight="16" ExpandCollapseImageWidth="17"
	ExpandCollapseImageHeight="15" itemspacing="0" imagesbaseurl="~/ControlPanel/Images/CATreeView/small_icons/"
	nodelabelpadding="0" parentnodeimageurl="folder.gif" expandedparentnodeimageurl="folder_open.gif"
	showlines="true" lineimagesfolderurl="~/ControlPanel/Images/CATreeView/small_icons/lines/"
	enableviewstate="false" ClientSideOnNodeMove="nodeMove" ClientSideOnNodeRename="nodeRename"
	ClientSideOnNodeMouseOut="checkNodeIndex" ClientSideOnNodeMouseOver="checkNodeIndex" ClientSideOnNodeSelect="nodeSelect"
	ExpandNodeOnSelect="true" CollapseNodeOnSelect="false" AutoPostBackOnSelect="false" AutoPostBackOnNodeMove="false" dropsiblingenabled="true"
	OnContextMenu="treeContextMenu" runat="server" MultipleSelectEnabled="False" BorderColor="#CCCCCC"
	BorderStyle="Solid" BorderWidth="0" FillContainer="True"></ca:treeview>

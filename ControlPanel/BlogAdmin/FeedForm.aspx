<%@ Page language="c#" Codebehind="FeedForm.aspx.cs" AutoEventWireup="false" Inherits="CommunityServerWeb.ControlPanel.BlogAdmin.FeedForm" %>
<%@ Import Namespace="CommunityServer.Components" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelModalMaster.ascx" IsModal="true">
	<CP:Content id="bcr" runat="server">
		<script type="text/javascript">
		// <![CDATA[
		function closeModal()
		{
			window.parent.Telligent_Modal.Close(true);
		}
		// ]]>
		</script>
		<div class="CommonContentArea">
			<div class="CommonContent">
			
			<TABLE cellSpacing="0" cellPadding="0" border="0">
			<TR>
				<TD class="CommonFormFieldName">
					<cp:helpicon runat="server" resourcename="CP_BlogAdmin_Blogs_FeedUrlLabel_Help" ID="Helpicon1"/>
					<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Blogs_FeedUrlLabel" controltolabel="FeedUrlTxt" ID="Formlabel1"/>
				</TD>
				<TD class="CommonFormField">
					<asp:textbox id="FeedUrlTxt" runat="Server" style="width: 300px;" cssclass="ControlPanelTextInput" />
					<asp:RequiredFieldValidator id="FeedUrlTxtValidator" Runat="server" ControlToValidate="FeedUrlTxt" Display="Dynamic" Text="*" />
				</TD>
			</TR>
			<TR>
				<TD class="CommonFormFieldName">
					<cp:helpicon runat="server" resourcename="CP_BlogAdmin_Blogs_FeedSiteUrlLabel_Help" ID="Helpicon2"/>
					<cp:formlabel ID="Formlabel2" runat="Server" resourcename="CP_BlogAdmin_Blogs_FeedSiteUrlLabel" controltolabel="FeedSiteUrlTxt" />
				</TD>
				<TD class="CommonFormField">
					<asp:textbox id="FeedSiteUrlTxt" runat="Server" style="width: 300px;" cssclass="ControlPanelTextInput" />
				</TD>
			</TR>
			<TR>
				<TD class="CommonFormFieldName">
					<cp:helpicon runat="server" resourcename="CP_BlogAdmin_Blogs_FeedTitleLabel_Help" ID="Helpicon3"/>
					<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Blogs_FeedTitleLabel" controltolabel="FeedTitleTxt" ID="Formlabel3"/></TD>
				<TD class="CommonFormField">
					<asp:textbox id=FeedTitleTxt runat="Server" style="width: 300px;" cssclass="ControlPanelTextInput" />
					<asp:RequiredFieldValidator id="FeedTitleTxtValidator" Runat="server" ControlToValidate="FeedTitleTxt" Display="Dynamic" Text="*" />
				</TD>
			</TR>
			<TR>
				<TD class="CommonFormFieldName">
					<cp:helpicon runat="server" resourcename="CP_BlogAdmin_Blogs_FeedIntervalLabel_Help" ID="Helpicon4"/>
					<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Blogs_FeedIntervalLabel" controltolabel="FeedIntervalTxt" ID="Formlabel4"/>
				</TD>
				<TD class="CommonFormField">
					<asp:textbox id="FeedIntervalTxt" runat="Server" style="width: 50px;" Text="60" cssclass="ControlPanelTextInput" />
					<asp:RegularExpressionValidator runat="server"
                        ControlToValidate="FeedIntervalTxt"
                        ValidationExpression="0*[1-9][0-9]*"
                        ErrorMessage="* Your entry is not a number"
                        display="dynamic" Text="* Your entry is not a number" />
					<asp:RequiredFieldValidator id="FeedIntervalTxtValidator" Runat="server" ControlToValidate="FeedIntervalTxt" Display="Dynamic" Text="*" />
				</TD>
			</TR>

			<TR>
				<TD class="CommonFormFieldName">
					<cp:helpicon runat="server" resourcename="CP_BlogAdmin_Blogs_FeedExcerptSize_Help" ID="Helpicon5"/>
					<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Blogs_FeedExcerptSize" controltolabel="FeedExerptSize" ID="Formlabel5"/>
				</TD>
				<TD class="CommonFormField">
					<asp:textbox id="FeedExerptSize" runat="Server" style="width: 50px;" Text="250" cssclass="ControlPanelTextInput" />
					<asp:RegularExpressionValidator runat="server"
                        ControlToValidate="FeedExerptSize"
                        ValidationExpression="0*[1-9][0-9]*"
                        ErrorMessage="* Your entry is not a number"
                        display="dynamic" Text="* Your entry is not a number" />
					<asp:RequiredFieldValidator id="FeedExerptSizeValidator" Runat="server" ControlToValidate="FeedExerptSize" Display="Dynamic" Text="*" />
				</TD>
			</TR>

			<TR>
				<TD class="CommonFormFieldName">
					<cp:helpicon runat="server" resourcename="CP_BlogAdmin_Blogs_FeedShowFullPost_Help" ID="Helpicon6"/>
					<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Blogs_FeedShowFullPost" controltolabel="checkShowFullPost" ID="Formlabel6"/></TD>
				<TD class="CommonFormField">
					<asp:checkbox id="checkShowFullPost" runat="Server" cssclass="ControlPanelTextInput" />
				</TD>
			</TR>

			<TR>
				<TD class="CommonFormFieldName">
					<cp:helpicon runat="server" resourcename="CP_BlogAdmin_Blogs_BlogRollAgg_Help" ID="Helpicon7"/>
					<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Blogs_BlogRollAgg" controltolabel="checkBlogRollAgg" ID="Formlabel7"/></TD>
				<TD class="CommonFormField">
					<asp:checkbox id="checkBlogRollAgg" runat="Server" checked="true" cssclass="ControlPanelTextInput" />
				</TD>
			</TR>

			<TR>
				<TD class="CommonFormFieldName">
					<cp:helpicon runat="server" resourcename="CP_BlogAdmin_Blogs_BlogAgg_Help" ID="Helpicon8"/>
					<cp:formlabel runat="Server" resourcename="CP_BlogAdmin_Blogs_BlogAgg" controltolabel="checkBlogAgg" ID="Formlabel8"/></TD>
				<TD class="CommonFormField">
					<asp:checkbox id="checkBlogAgg" runat="Server" checked="true" cssclass="ControlPanelTextInput" />
				</TD>
			</TR>

			<TR>
				<TD class="CommonFormFieldName">&nbsp;</TD>
				<TD class="CommonFormField" align="right">
					<cp:ResourceLinkButton id="AddFeedButton" runat="Server" cssclass="CommonTextButton" resourcename="CP_Add" />
				</TD>
			</TR>
			</TABLE>

				
			</DIV>
		</DIV>
	</CP:Content>
</CP:Container>

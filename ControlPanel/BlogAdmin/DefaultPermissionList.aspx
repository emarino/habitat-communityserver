<%@ Page language="c#" Codebehind="DefaultPermissionList.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.BlogAdmin.DefaultPermissionList" %>
<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="SectionDescription" runat="server" resourcename="CP_BlogAdmin_DefaultPermissionsList_Title"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<CP:ResourceControl id="Section_Sub_Description" runat="server" ResourceName="CP_BlogAdmin_DefaultPermissionsList_SubTitle"></CP:ResourceControl><BR>
			<EM>&nbsp;&nbsp;&nbsp;
				<CP:ResourceControl runat="server" ResourceName="CP_BlogAdmin_DefaultPermissionsList_Tip" /></EM></DIV>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Blogs" />
		<div id="GrayGrid">
        <asp:Repeater runat="server" ID="PermissionList">
            <HeaderTemplate>
                <table class="Grid" cellSpacing=0 cellPadding=0 width="100%">
                <tbody>
                    <tr class="HeadingRow HeadingCellText HeadingCell">
                        <th><CP:ResourceControl runat="server" ResourceName="CP_Blog_GridCol_Name" /></th>
                        <th><CP:ResourceControl runat="server" ResourceName="CP_Blog_GridCol_View" /></th>
                        <th><CP:ResourceControl runat="server" ResourceName="CP_Blog_GridCol_Post" /></th>
                        <th><CP:ResourceControl runat="server" ResourceName="CP_Blog_GridCol_Reply" /></th>
                        <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Ink" /></th>
                        <th><CP:ResourceControl runat="server" ResourceName="CP_PermissionList_Video" /></th>
                        <th><CP:ResourceControl runat="server" ResourceName="CP_Blog_GridCol_LocalAttachment" /></th>
                        <th><CP:ResourceControl runat="server" ResourceName="CP_Blog_GridCol_RemoteAttachment" /></th>
                    </tr>    
            </HeaderTemplate>
            <ItemTemplate>
                    <tr class="Row">
                        <td><%# Eval("Name") %><asp:HiddenField ID="RoleId" runat="server" Value='<%# Eval("RoleIDString") %>' Visible="false" /></td>
                        <td><asp:CheckBox runat="server" ID="ViewCheckbox" Checked='<%# Eval("View") %>' /></td>
                        <td><asp:CheckBox runat="server" ID="PostCheckbox" Checked='<%# Eval("Post") %>' /></td>
                        <td><asp:CheckBox runat="server" ID="ReplyCheckbox" Checked='<%# Eval("Reply") %>' /></td>
                        <td><asp:CheckBox runat="server" ID="InkCheckbox" Checked='<%# Eval("Ink") %>' /></td>
                        <td><asp:CheckBox runat="server" ID="VideoCheckbox" Checked='<%# Eval("Video") %>' /></td>
                        <td><asp:CheckBox runat="server" ID="LocalAttachmentCheckbox" Checked='<%# Eval("LocalAttachment") %>' /></td>
                        <td><asp:CheckBox runat="server" ID="RemoteAttachmentCheckbox" Checked='<%# Eval("RemoteAttachment") %>' /></td>
                    </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                    <tr class="AlternatingRow">
                        <td><%# Eval("Name") %><asp:HiddenField ID="RoleId" runat="server" Value='<%# Eval("RoleIDString") %>' Visible="false" /></td>
                        <td><asp:CheckBox runat="server" ID="ViewCheckbox" Checked='<%# Eval("View") %>' /></td>
                        <td><asp:CheckBox runat="server" ID="PostCheckbox" Checked='<%# Eval("Post") %>' /></td>
                        <td><asp:CheckBox runat="server" ID="ReplyCheckbox" Checked='<%# Eval("Reply") %>' /></td>
                        <td><asp:CheckBox runat="server" ID="InkCheckbox" Checked='<%# Eval("Ink") %>' /></td>
                        <td><asp:CheckBox runat="server" ID="VideoCheckbox" Checked='<%# Eval("Video") %>' /></td>
                        <td><asp:CheckBox runat="server" ID="LocalAttachmentCheckbox" Checked='<%# Eval("LocalAttachment") %>' /></td>
                        <td><asp:CheckBox runat="server" ID="RemoteAttachmentCheckbox" Checked='<%# Eval("RemoteAttachment") %>' /></td>
                    </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        </div>
        <p class="PanelSaveButton">
	        <cp:resourcelinkbutton id="SaveButton" runat="server" resourcename="Save" cssclass="CommonTextButton" />
        </p>
	</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="GroupForm.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.BlogAdmin.GroupForm" %>
<%@ Register TagPrefix="CP" TagName = "GroupEditControl" Src = "~/ControlPanel/Controls/GroupEditControl.ascx" %>
<%@ Import Namespace="CommunityServer.Components" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelModalMaster.ascx" IsModal="true">
<CP:Content id="bcr" runat="server">
<DIV class="CommonContentArea">
	<CP:GroupEditControl id="GroupEditControl1" runat="server"></CP:GroupEditControl>
	<DIV class="CommonContent">
		<div class="CommonFormField PanelSaveButton">
			<cp:resourcelinkbutton id="SaveButton" runat="server" resourcename="Save" cssclass="CommonTextButton" />
		</div>
	</div>
</div>
</CP:Content>
</CP:Container>

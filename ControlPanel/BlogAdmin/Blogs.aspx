<%@ Register TagPrefix="CP" TagName = "BlogList" Src = "~/ControlPanel/BlogAdmin/BlogListControl.ascx" %>
<%@ Page language="c#" EnableViewState="False" Codebehind="Blogs.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.BlogAdmin.BlogsPage" %>
<%@ Import Namespace = "CommunityServer.Components" %>
<%@ Import Namespace = "CommunityServer.Blogs.Components" %>
<%@ Import Namespace = "CommunityServer.ControlPanel" %>
<CP:Container runat="server" id="Mpcontainer1" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="SectionDescription" runat="server" resourcename="CP_BlogAdmin_Blogs_Title"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonDescription">
			<cp:resourcecontrol id="Section_Sub_Description" runat="server" resourcename="CP_BlogAdmin_Blogs_Description"></cp:resourcecontrol></DIV>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Blogs" />
			<h3><CP:CSLiteral runat="server" ID="message" /></h3>

<table border="0" cellpadding="3" cellspacing="0">
    <tr>
        <td width="75px"><CP:ResourceControl resourcename="CP_BlogAdmin_Blogs_Filter" runat = "Server" ID="Resourcecontrol1"/></td>
        <td><cp:AlphaPicker runat = "Server" id = "picker" /></td>
    </tr>
    <tr>
        <td width="75px"><CP:ResourceControl resourcename="CP_BlogAdmin_Blogs_Search" runat = "Server" ID="Resourcecontrol2"/></td>
        <td><input type="text" id = "q" name="q" value ="<%= Request.QueryString["q"] %>"></input> <input type="button" onclick="javascript:window.location = window.location.pathname + '?q=' + escape(q.value)" value ="Go" /></td>
    </tr>
</table>			

			
<div class="CommonListArea" style="clear:both">
<asp:Repeater runat = "Server" id = "theBlogs">
<HeaderTemplate>
<table id="BlogLists" cellSpacing="0" cellPadding="0" border="0" width="100%">
<thead>
<tr>
    <th class="CommonListHeaderLeftMost" ><cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Name" ID="Resourcecontrol5" NAME="Resourcecontrol3"/></th>
    <th class="CommonListHeader"><cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Title" ID="Resourcecontrol6" NAME="Resourcecontrol4"/></th>
    <th class="CommonListHeader"><cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Owners" ID="Resourcecontrol7" NAME="Resourcecontrol4"/></th>
    <th class="CommonListHeader"><cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Group" ID="Resourcecontrol8" NAME="Resourcecontrol4"/></th>
    <th class="CommonListHeader"><cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Enabled" ID="Resourcecontrol9" NAME="Resourcecontrol4"/></th>    
    <th class="CommonListHeader"></th>
</tr>
</thead>
</HeaderTemplate>
<ItemTemplate>
<tr>
    <td class="CommonListCellLeftMost"><a href="SectionEdit.aspx?tab=blog&amp;SectionID=<%# DataBinder.Eval(Container.DataItem,"SectionID") %>"><%# Formatter.RemoveHtml(DataBinder.Eval(Container.DataItem,"ApplicationKey").ToString(), 50) %></a></td>
    <td class="CommonListCell"><%# Formatter.RemoveHtml(DataBinder.Eval(Container.DataItem,"Name").ToString(), 50)%></td>
    <td class="CommonListCell"><%# FormatOwners(DataBinder.Eval(Container.DataItem,"Owners")) %></td>
    <td  class="CommonListCell" nowrap="nowrap"><%# DataBinder.Eval(Container.DataItem,"GroupName") %></td>
	<td class="CommonListCell" align="center"><img src="<%=SiteUrls.Instance().Locations["ControlPanel"]%>images/<%# DataBinder.Eval(Container.DataItem,"IsActive").ToString() %>.gif" width="16" height="15" /></td>    
	<td class ="CommonListCell" nowrap="nowrap">
			<a href="SectionEdit.aspx?tab=blog&amp;SectionID=<%# DataBinder.Eval(Container.DataItem,"SectionID") %>" class="CommonTextButton">Edit</a> 
 			<a href="BlogFeedsManage.aspx?SectionID=<%# DataBinder.Eval(Container.DataItem,"SectionID") %>" class="CommonTextButton">Mirrors</a>
 			<a href='../blogs/default.aspx?tab=manage&sectionid=<%# DataBinder.Eval(Container.DataItem,"SectionID") %>' class="CommonTextButton">Manage</a> 
			<a href="<%# DeleteLink(DataBinder.Eval(Container.DataItem,"SectionID")) %>" onclick="return confirm('<CP:ResourceControl runat = "Server" ResourceName = "CP_BlogAdmin_Blogs_DeleteConfirmation" />');" class="CommonTextButton">Delete</a>	
			
	</td>
</tr>
</ItemTemplate>
<AlternatingItemTemplate>
<tr class="AltListRow">
    <td class="CommonListCellLeftMost"><a href="SectionEdit.aspx?tab=blog&amp;SectionID=<%# DataBinder.Eval(Container.DataItem,"SectionID") %>"><%# Formatter.RemoveHtml(DataBinder.Eval(Container.DataItem,"ApplicationKey").ToString(), 50) %></a></td>
    <td class="CommonListCell"><%# Formatter.RemoveHtml(DataBinder.Eval(Container.DataItem,"Name").ToString(), 50)%></td>
    <td class="CommonListCell"><%# FormatOwners(DataBinder.Eval(Container.DataItem,"Owners")) %></td>
    <td  class="CommonListCell" nowrap="nowrap"><%# DataBinder.Eval(Container.DataItem,"GroupName") %></td>
	<td class="CommonListCell" align="center"><img src="<%=SiteUrls.Instance().Locations["ControlPanel"]%>images/<%# DataBinder.Eval(Container.DataItem,"IsActive").ToString() %>.gif" width="16" height="15" /></td>    
	<td class ="CommonListCell" nowrap="nowrap">
			<a href="SectionEdit.aspx?tab=blog&amp;SectionID=<%# DataBinder.Eval(Container.DataItem,"SectionID") %>" class="CommonTextButton">Edit</a> 
            <a href="BlogFeedsManage.aspx?SectionID=<%# DataBinder.Eval(Container.DataItem,"SectionID") %>" class="CommonTextButton">Mirrors</a>  			
 			<a href='../blogs/default.aspx?tab=manage&sectionid=<%# DataBinder.Eval(Container.DataItem,"SectionID") %>' class="CommonTextButton">Manage</a> 
			<a href="<%# DeleteLink(DataBinder.Eval(Container.DataItem,"SectionID")) %>" onclick="return confirm('<CP:ResourceControl runat = "Server" ResourceName = "CP_BlogAdmin_Blogs_DeleteConfirmation" />');" class="CommonTextButton">Delete</a>	
	</td>	
</tr>
</AlternatingItemTemplate>
<FooterTemplate>
</table>
</FooterTemplate>
</asp:Repeater>
</div>				
			
	</CP:Content>
</CP:Container>

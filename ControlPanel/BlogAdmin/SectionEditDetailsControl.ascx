<%@ Control language="c#" Codebehind="SectionEditDetailsControl.ascx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.BlogAdmin.SectionEditDetailsControl" %>
<%@ Import Namespace="CommunityServer.Components" %>

<CP:StatusMessage id="Status" runat="server"></CP:StatusMessage>
<table id="AdvancedProperties" cellSpacing="1" cellPadding="2" width="550" border="0" runat="server">
	<tr vAlign="top">
		<td class="CommonFormFieldName">
			<CP:helpicon id="Helpicon8" runat="Server" resourcename="CP_BlogAdmin_SectionEdit_Settings_Name_Detail" />
			<CP:FORMLABEL id="Formlabel4" runat="Server" resourcename="CP_BlogAdmin_SectionEdit_Settings_Name" controltolabel="SectionName" />
		</td>
		<td class="CommonFormField">
			<asp:textbox id="SectionName" runat="server" cssclass="txt1" columns="50" maxlength="256" />
			<asp:requiredfieldvalidator id="SectionNameValidator" runat="server" cssclass="validationWarning" controltovalidate="SectionName">*</asp:requiredfieldvalidator>
			<asp:RegularExpressionValidator ValidationExpression="^.*[^\s]+.*$" 
                     ControlToValidate="SectionName" Display="dynamic" cssclass="validationWarning"
                     ErrorMessage="<br/>Name does not contain an alphanumeric character." runat="server" />
		</td>
	</tr>
	<tr>
		<td class="CommonFormFieldName" vAlign="top">
			<CP:helpicon id="Helpicon4" runat="Server" resourcename="CP_BlogAdmin_SectionEdit_Settings_AppKey_Detail" />
			<CP:FORMLABEL id="Formlabel17" runat="Server" resourcename="CP_BlogAdmin_SectionEdit_Settings_AppKey" controltolabel="AppKey" />
		</td>
		<td class="CommonFormField">
			<ASP:Literal Runat="server" ID="AppKeyUrlPrefix" /><asp:textbox id="AppKey" runat="server" cssclass="txt1" columns="50" maxlength="256" style="width: 100px;" /><ASP:Literal Runat="server" ID="AppKeyUrlSuffix" />
		</td>
	</tr>
	<tr>
		<td class="CommonFormFieldName">
			<CP:helpicon id="Helpicon5" runat="Server" resourcename="CP_BlogAdmin_SectionEdit_Settings_Owner_Detail" />
			<CP:FORMLABEL id="Formlabel18" runat="Server" resourcename="CP_BlogAdmin_SectionEdit_Settings_Owner" controltolabel="Owners" />
			<em><CP:RESOURCECONTROL id="Resourcecontrol38" runat="server" resourcename="CP_BlogAdmin_SectionEdit_Settings_Optional" /></em>
		</td>
		<td class="CommonFormField">
			<asp:textbox id="Owners" runat="server" columns="50" />
		</td>
	</tr>
	<tr>
		<td class="CommonFormFieldName" vAlign="top">
			<CP:helpicon id="Helpicon6" runat="Server" resourcename="CP_BlogAdmin_SectionEdit_Settings_Group_Detail" />
			<CP:FORMLABEL id="Formlabel2" runat="Server" resourcename="CP_BlogAdmin_SectionEdit_Settings_Group" controltolabel="AdminGroupList" />
		</td>
		<td class="CommonFormField">
			<asp:dropdownlist id="AdminGroupList" Runat="server" />
			<asp:requiredfieldvalidator id="AdminGroupValidator" runat="server" CssClass="ValidationMessage" ControlToValidate="AdminGroupList" ErrorMessage="*"></asp:requiredfieldvalidator>
		</td>
	</tr>
	<tr runat="server" id="IsActive">
		<td class="CommonFormFieldName">
			<CP:helpicon id="HelpIcon1" runat="Server" resourcename="CP_BlogAdmin_SectionEdit_Settings_IsActive_Detail" />
			<CP:FORMLABEL id="Formlabel6" runat="Server" ResourceName="CP_BlogAdmin_SectionEdit_Settings_IsActive" controltolabel="ynIsActive" />
		</td>
		<td class="CommonFormField">
			<CP:yesnoradiobuttonlist id="ynIsActive" runat="server" cssclass="txt1" repeatcolumns="2" />
		</td>
	</tr>
	<tr runat="server" id="EnableAggBugs">
		<td class="CommonFormFieldName">
			<CP:helpicon id="Helpicon2" runat="Server" resourcename="BlogSettings_EnableAggBugs_Detail" />
			<CP:FORMLABEL id="Formlabel1" runat="Server" resourcename="BlogSettings_EnableAggBugs" controltolabel="ynEnableAggBugs" />
		</td>
		<td class="CommonFormField">
			<cp:yesnoradiobuttonlist id="ynEnableAggBugs" runat="server" cssclass="txt1" repeatcolumns="2" />
		</td>
	</tr>
	<tr runat="server" id="IsSearchable">
		<td class="CommonFormFieldName">
			<CP:helpicon id="Helpicon3" runat="Server" resourcename="CP_BlogAdmin_SectionEdit_Settings_IsSearchable_Detail" />
			<CP:FORMLABEL id="Formlabel16" runat="Server" resourcename="CP_BlogAdmin_SectionEdit_Settings_IsSearchable" controltolabel="ynIsSearchable" />
		</td>
		<td class="CommonFormField">
			<cp:yesnoradiobuttonlist id="ynIsSearchable" runat="server" cssclass="txt1" repeatcolumns="2" />
		</td>
	</tr>
	<tr runat="server" id="IsCommunityAggregated">
		<td class="CommonFormFieldName">
			<CP:helpicon id="Helpicon7" runat="Server" resourcename="CP_BlogAdmin_SectionEdit_Settings_IsCommunityAggregated_Detail" />
			<CP:formlabel id="Formlabel3" runat="Server" resourcename="CP_BlogAdmin_SectionEdit_Settings_IsCommunityAggregated" controltolabel="ynIsCommunityAggregated" />
		</td>
		<td class="CommonFormField">
			<CP:yesnoradiobuttonlist id="ynIsCommunityAggregated" runat="server" cssclass="txt1" repeatcolumns="2" />
		</td>
	</tr>
	<tr runat="server" id="SectionLocalizationArea">
		<td class="CommonFormFieldName">
			<CP:helpicon id="Helpicon9" runat="Server" resourcename="CP_BlogAdmin_SectionEdit_Settings_DefaultLanguage_Detail" />
			<CP:formlabel id="Formlabel5" runat="Server" resourcename="CP_BlogAdmin_SectionEdit_Settings_DefaultLanguage" controltolabel="DefaultLanguage" />
		</td>
		<td class="CommonFormField">
			<CP:FilterLanguageDropDownList runat="server" id="DefaultLanguage" />
		</td>
	</tr>
</table>
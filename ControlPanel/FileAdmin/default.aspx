<%@ Page language="c#" Codebehind="default.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.FileAdmin._default" %>
<cp:controlpanelselectednavigation selectednavitem="Files" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="CP_FilesAdmin_Default_Title"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<cp:ResourceControl runat="server" resourcename="CP_FilesAdmin_Default_Description" />
		</DIV>
	</CP:Content>
</CP:Container>
<%@ Control Language="c#" AutoEventWireup="false" Codebehind="FolderEditControlDetails.ascx.cs" Inherits="CommunityServer.ControlPanel.FileAdmin.FolderEditControlDetails" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="CommunityServer.Components" %>
<table cellspacing="1" cellpadding="2" width="550px" border="0" id="AdvancedProperties"
	runat="server">
	<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon id="Helpicon2" runat="Server" resourcename="CP_FilesAdmin_FolderEditControl_Name_Detail" />
			<CP:FormLabel id="Formlabel1" runat="Server" controltolabel="Name" ResourceName="CP_FilesAdmin_FolderEditControl_Name" />
		</td>
		<td class="CommonFormField">
			<asp:textbox id="Name" runat="server" maxlength="256" columns="50" cssclass="txt1"></asp:textbox>
			<asp:requiredfieldvalidator id="Requiredfieldvalidator1" runat="server" cssclass="validationWarning" controltovalidate="Name">*</asp:requiredfieldvalidator>
			<asp:RegularExpressionValidator ValidationExpression="^.*[^\s]+.*$" 
                     ControlToValidate="Name" Display="dynamic" cssclass="validationWarning"
                     ErrorMessage="<br/>Name does not contain an alphanumeric character." runat="server" />
		</td>
	</tr>
	<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon id="Helpicon4" runat="Server" resourcename="CP_FilesAdmin_FolderEditControl_AppKey_Detail" />
			<CP:FormLabel id="Formlabel17" runat="Server" controltolabel="AppKey" resourcename="CP_FilesAdmin_FolderEditControl_AppKey" />
		</td>
		<td class="CommonFormField">
			<ASP:Literal Runat="server" ID="AppKeyUrlPrefix" /><asp:textbox id="AppKey" runat="server" maxlength="256" columns="50" cssclass="txt1" style="width: 100px;"></asp:textbox><ASP:Literal Runat="server" ID="AppKeyUrlSuffix" />
			<asp:requiredfieldvalidator id="AppKeyValidator" runat="server" cssclass="validationWarning" controltovalidate="Name">*</asp:requiredfieldvalidator>
		</td>
	</tr>
	<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon id="Helpicon6" runat="Server" resourcename="CP_FilesAdmin_FolderEditControl_Group_Detail" />
			<CP:FormLabel id="Formlabel2" runat="Server" controltolabel="AdminGroupList" resourcename="CP_FilesAdmin_FolderEditControl_Group" />
		</td>
		<td class="CommonFormField">
			<asp:DropDownList id="AdminGroupList" Runat="server"></asp:DropDownList>
			<asp:RequiredFieldValidator runat="server" ErrorMessage="*" ControlToValidate="AdminGroupList" CssClass="ValidationMessage"
				ID="AdminGroupValidator" />
		</td>
	</tr>
	<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon id="HelpIcon1" runat="Server" resourcename="CP_FilesAdmin_FolderEditControl_Enable_Detail" />
			<CP:FormLabel id="Formlabel6" runat="Server" controltolabel="ynIsActive" ResourceName="CP_FilesAdmin_FolderEditControl_Enable" />
		</td>
		<td class="CommonFormField">
			<cp:yesnoradiobuttonlist id="ynIsActive" runat="server" repeatcolumns="2" cssclass="txt1" />
		</td>
	</tr>
	<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon id="Helpicon3" runat="Server" resourcename="CP_FilesAdmin_FolderEditControl_Index_Detail" />
			<CP:FormLabel id="Formlabel16" runat="Server" controltolabel="ynIsSearchable" resourcename="CP_FilesAdmin_FolderEditControl_Index" />
		</td>
		<td class="CommonFormField">
			<cp:yesnoradiobuttonlist id="ynIsSearchable" runat="server" repeatcolumns="2" cssclass="txt1" />
		</td>
	</tr>
	<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon id="Helpicon5" runat="Server" resourcename="CP_FilesAdmin_FolderEditControl_Owners_Detail" />
			<CP:FormLabel id="Formlabel18" runat="Server" controltolabel="Owners" resourcename="CP_FilesAdmin_FolderEditControl_Owners" />
			<em>
				<CP:ResourceControl runat="server" resourcename="CP_FilesAdmin_FolderEditControl_Optional" id="Resourcecontrol38" /></em>
		</td>
		<td class="CommonFormField">
			<asp:textbox id="Owners" runat="server" columns="50"></asp:textbox>
		</td>
	</tr>
	<tr runat="server" id="SectionLocalizationArea">
		<td class="CommonFormFieldName">
			<cp:helpicon id="Helpicon9" runat="Server" resourcename="CP_FilesAdmin_FolderEditControl_DefaultLanguage_Detail" />
			<cp:formlabel id="Formlabel5" runat="Server" resourcename="CP_FilesAdmin_FolderEditControl_DefaultLanguage" controltolabel="DefaultLanguage" />
		</td>
		<td class="CommonFormField">
			<cp:FilterLanguageDropDownList runat="server" id="DefaultLanguage" />
		</td>
	</tr>
</table>

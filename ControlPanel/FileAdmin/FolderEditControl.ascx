<%@ Control Language="c#" AutoEventWireup="false" Codebehind="FolderEditControl.ascx.cs" Inherits="CommunityServer.ControlPanel.FileAdmin.FolderEditControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Register TagPrefix="CP" TagName = "FolderDetails" Src = "~/ControlPanel/FileAdmin/FolderEditControlDetails.ascx" %>
<%@ Register TagPrefix="CP" TagName = "FolderOptions" Src = "~/ControlPanel/FileAdmin/FolderOptionsControl.ascx" %>

<TWC:TabbedPanes id="EditorTabs" runat="server"
	PanesCssClass="CommonPane"
	TabSetCssClass="CommonPaneTabSet"
	TabCssClasses="CommonPaneTab,CommonPaneTab1,CommonPaneTab2"
	TabSelectedCssClasses="CommonPaneTabSelected,CommonPaneTabSelected1,CommonPaneTabSelected2"
	TabHoverCssClasses="CommonPaneTabHover,CommonPaneTabHover1,CommonPaneTabHover2"
	>
	<TWC:TabbedPane runat="server">
		<Tab><CP:ResourceControl runat="server" ResourceName="CP_FilesAdmin_CreateEditFolder_Tab_Settings" /></Tab>
		<Content>
			<CP:FolderDetails id="SectionDetails1" runat="Server"></CP:FolderDetails>
		</Content>
	</TWC:TabbedPane>
	<TWC:TabbedPane runat="server">
		<Tab><CP:ResourceControl runat="server" ResourceName="CP_FilesAdmin_CreateEditFolder_Tab_Options" /></Tab>
		<Content>
			<CP:FolderOptions id="FolderOptions1" runat="Server"></CP:FolderOptions>
		</Content>
	</TWC:TabbedPane>
	<TWC:TabbedPane runat="server">
		<Tab><CP:ResourceControl runat="server" ResourceName="CP_FilesAdmin_CreateEditFolder_Tab_Permissions" /></Tab>
		<Content>
			<div class="CommonDescription"><br />
			    <CP:ResourceControl id="Resourcecontrol1" runat="Server" resourcename="CP_FilesAdmin_CreateEditFolder_PermissionsTip"></CP:ResourceControl>
			</div>
			<div id="GrayGrid">
            <asp:Repeater runat="server" ID="PermissionList">
                <HeaderTemplate>
                    <table class="Grid" cellSpacing=0 cellPadding=0 width="100%">
                    <tbody>
                        <tr class="HeadingRow HeadingCellText HeadingCell">
                            <th><CP:ResourceControl runat="server" ResourceName="CP_FilesAdmin_PermissionsGrid_Name" /></th>
                            <th><CP:ResourceControl runat="server" ResourceName="CP_FilesAdmin_PermissionsGrid_View" /></th>
                            <th><CP:ResourceControl runat="server" ResourceName="CP_FilesAdmin_PermissionsGrid_Download" /></th>
                            <th><CP:ResourceControl runat="server" ResourceName="CP_FilesAdmin_PermissionsGrid_Delete" /></th>
                            <th><CP:ResourceControl runat="server" ResourceName="CP_FilesAdmin_PermissionsGrid_Edit" /></th>
                            <th><CP:ResourceControl runat="server" ResourceName="CP_FilesAdmin_PermissionsGrid_Reply" /></th>
                            <th><CP:ResourceControl runat="server" ResourceName="CP_FilesAdmin_PermissionsGrid_Post" /></th>
                            <th><CP:ResourceControl runat="server" ResourceName="CP_FilesAdmin_PermissionsGrid_LocalAttachment" /></th>
                            <th><CP:ResourceControl runat="server" ResourceName="CP_FilesAdmin_PermissionsGrid_RemoteAttachment" /></th>
                        </tr>    
                </HeaderTemplate>
                <ItemTemplate>
                        <tr class="Row">
                            <td><%# Eval("Name") %><asp:HiddenField ID="RoleId" runat="server" Value='<%# Eval("RoleIDString") %>' Visible="false" /></td>
                            <td><asp:CheckBox runat="server" ID="ViewCheckbox" Checked='<%# Eval("View") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="ReadCheckbox" Checked='<%# Eval("Download") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="DeleteCheckbox" Checked='<%# Eval("Delete") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="EditCheckbox" Checked='<%# Eval("Edit") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="ReplyCheckbox" Checked='<%# Eval("Reply") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="PostCheckbox" Checked='<%# Eval("Post") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="LocalAttachmentCheckbox" Checked='<%# Eval("LocalAttachment") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="RemoteAttachmentCheckbox" Checked='<%# Eval("RemoteAttachment") %>' /></td>
                        </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                        <tr class="AlternatingRow">
                            <td><%# Eval("Name") %><asp:HiddenField ID="RoleId" runat="server" Value='<%# Eval("RoleIDString") %>' Visible="false" /></td>
                            <td><asp:CheckBox runat="server" ID="ViewCheckbox" Checked='<%# Eval("View") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="ReadCheckbox" Checked='<%# Eval("Download") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="DeleteCheckbox" Checked='<%# Eval("Delete") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="EditCheckbox" Checked='<%# Eval("Edit") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="ReplyCheckbox" Checked='<%# Eval("Reply") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="PostCheckbox" Checked='<%# Eval("Post") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="LocalAttachmentCheckbox" Checked='<%# Eval("LocalAttachment") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="RemoteAttachmentCheckbox" Checked='<%# Eval("RemoteAttachment") %>' /></td>
                        </tr>
                </AlternatingItemTemplate>
                <FooterTemplate>
                    </tbody>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            </div>
		</Content>
	</TWC:TabbedPane>
</TWC:TabbedPanes>	

<%@ Page language="c#" Codebehind="FolderGroups.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.FileAdmin.FolderGroups" %>
<%@ Register TagPrefix="CP" TagName="GroupList" Src = "~/ControlPanel/FileAdmin/FolderGroupListControl.ascx" %>
<CP:Container runat="server" id="Mpcontainer1" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl runat="server" ResourceName="CP_FilesAdmin_FolderGroups_Title" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonDescription">
			<CP:ResourceControl runat="server" ResourceName="CP_FilesAdmin_FolderGroups_Description" />
		</div>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Files" />
		<CP:GroupList id="GroupList1" runat="Server"></CP:GroupList>
		<p class="PanelSaveButton">
			<CP:ControlPanelChangeManageView CssClass="CommonTextButton" runat="server" CurrentView="Grid" ViewChangeUrl="~/ControlPanel/FileAdmin/FolderGroupManager.aspx" />
		</p>		
	</CP:Content>
</CP:Container>
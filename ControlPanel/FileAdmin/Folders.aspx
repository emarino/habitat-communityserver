<%@ Register TagPrefix="CP" TagName = "FolderList" Src = "~/ControlPanel/FileAdmin/FolderListControl.ascx" %>
<%@ Page language="c#" Codebehind="Folders.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.FileAdmin.Folders" %>
<CP:Container runat="server" id="Mpcontainer1" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="ResourceControl1" runat="server" ResourceName="CP_FilesAdmin_Folders_Title"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<CP:ResourceControl id="ResourceControl2" runat="server" ResourceName="CP_FilesAdmin_Folders_Description"></CP:ResourceControl></DIV>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Files" />
		<cp:folderlist id="FolderList1" runat="Server"></cp:folderlist>
		<P class="PanelSaveButton">
			<CP:ControlPanelChangeManageView id="Controlpanelchangemanageview1" runat="server" ViewChangeUrl="~/ControlPanel/FileAdmin/FolderGroupManager.aspx"
				CurrentView="Grid" CssClass="CommonTextButton"></CP:ControlPanelChangeManageView></P>
	</CP:Content>
</CP:Container>

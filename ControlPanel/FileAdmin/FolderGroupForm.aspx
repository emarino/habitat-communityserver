<%@ Page language="c#" Codebehind="FolderGroupForm.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.FileAdmin.FolderGroupForm" %>
<%@ Import Namespace="CommunityServer.Components" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelModalMaster.ascx" IsModal="true">
	<CP:Content id="bcr" runat="server">
		<script type="text/javascript">
		// <![CDATA[
		function closeModal()
		{
			window.parent.Telligent_Modal.Close(true);
		}
		// ]]>
		</script>
		<DIV class="CommonContentArea">
			<DIV class="CommonContent">
					<DIV class="CommonFormFieldName">
						<CP:FormLabel id="tt" runat="Server" ControlToLabel="GroupName" resourcename="CP_FilesAdmin_FolderGroup_GroupName"></CP:FormLabel>
						<asp:requiredfieldvalidator id="ForumGroupNameValidator" runat="server" controltovalidate="GroupName" errormessage="*"></asp:requiredfieldvalidator></DIV>
					<DIV class="CommonFormField">
						<asp:TextBox id="GroupName" Runat="server" CssClass="ControlPanelTextInput" MaxLength="256"></asp:TextBox></DIV>
					<DIV class="CommonFormFieldName">
						<CP:FormLabel id="Formlabel1" runat="Server" ControlToLabel="GroupDesc" ResourceName="CP_FilesAdmin_FolderGroup_Description"></CP:FormLabel></DIV>
					<DIV class="CommonFormField">
						<asp:TextBox id="GroupDesc" Runat="server" CssClass="ControlPanelTextInput" MaxLength="256" TextMode="MultiLine" Rows="7"></asp:TextBox></DIV>
					<DIV class="CommonFormField PanelSaveButton">
						<cp:ResourceLinkButton id="Save" runat="Server" CssClass="CommonTextButton" ResourceName="Save"></cp:ResourceLinkButton></DIV>
				<P></P>
			</DIV>
		</DIV>
	</CP:Content>
</CP:Container>

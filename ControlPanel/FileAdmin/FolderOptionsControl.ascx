<%@ Control Language="c#" AutoEventWireup="false" Codebehind="FolderOptionsControl.ascx.cs" Inherits="CommunityServer.ControlPanel.FileAdmin.FolderOptionsControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<%@ Import Namespace="CommunityServer.Components" %>
<table cellspacing="1" cellpadding="2" width="550px" border="0" id="AdvancedProperties"
	runat="server">
	<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon id="Helpicon4" runat="Server" resourcename="CP_Files_FolderOptionsControl_EnableExternalLinks_Detail" />
			<CP:FormLabel runat="Server" ResourceName="CP_Files_FolderOptionsControl_EnableExternalLinks" ControlToLabel="EnableExternalLinks" ID="Formlabel5"/>
		</td>
		<td class="CommonFormField">
			<cp:yesnoradiobuttonlist id="EnableExternalLinks" runat="server" repeatcolumns="2" cssclass="txt1" />
		</td>
	</tr>
	<tr>
		<td class="CommonFormFieldName">
			<cp:helpicon id="Helpicon3" runat="Server" resourcename="CP_Files_FolderOptionsControl_EnableDownloadDisclaimer_Detail" />
			<CP:FormLabel runat="Server"  ResourceName="CP_Files_FolderOptionsControl_EnableDownloadDisclaimer" ControlToLabel="EnableDownloadDisclaimer" ID="Formlabel6"/>
		</td>
		<td class="CommonFormField">
			<cp:yesnoradiobuttonlist id="EnableDownloadDisclaimer" runat="server" repeatcolumns="2" cssclass="txt1" />
		</td>
	</tr>
</table>

<div class="CommonFormFieldName">
	<cp:helpicon id="Helpicon2" runat="Server" resourcename="CP_Files_FolderOptionsControl_AllowedFileExtensions_Detail" />
	<CP:FormLabel runat="server" ResourceName="CP_Files_FolderOptionsControl_AllowedFileExtensions" ControlToLabel="AllowedFileExtensions" id="FormLabel1" />
</div>
<div class="CommonFormField">
	<asp:TextBox Runat="Server" id="AllowedFileExtensions" TextMode="MultiLine" CssClass="ControlPanelTextInput" Height="100" />
</div>

<p />
<div class="CommonFormFieldName">
	<cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_Files_FolderOptionsControl_RestrictedFileExtensions_Detail" />
	<CP:FormLabel runat="server" ResourceName="CP_Files_FolderOptionsControl_RestrictedFileExtensions" ControlToLabel="RestrictedFileExtensions" id="FormLabel2" />
</div>
<div class="CommonFormField">
	<asp:TextBox Runat="Server" id="RestrictedFileExtensions" TextMode="MultiLine" CssClass="ControlPanelTextInput" Height="100" />
</div>
<%@ Page language="c#" Codebehind="CreateEditFolder.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.FileAdmin.CreateEditFolder" %>
<%@ Register TagPrefix="CP" TagName = "FolderDetails" Src = "~/ControlPanel/FileAdmin/FolderEditControl.ascx" %>

<CP:CONTROLPANELSELECTEDNAVIGATION id="SelectedNavigation1" runat="server" SelectedNavItem="FileFolderCreate" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<asp:literal id="RegionTitle" runat="server"></asp:literal>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonDescription">
			<CP:ResourceControl id="RegionDescription" runat="Server" resourcename="CP_FilesAdmin_CreateEditFolder_Description"></CP:ResourceControl>
		</div>
		<CP:StatusMessage runat="server" id="Status" Visible="false"/>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Files" />
		<CP:FolderDetails runat="server" id="FolderDetails" />
		<p class="PanelSaveButton DetailsFixedWidth">
			<cp:ResourceLinkButton id="SaveButton" runat="Server" CssClass="CommonTextButton" ResourceName="Save"></cp:ResourceLinkButton>
		</p>
	</CP:Content>
</CP:Container>
<%@ Page language="c#" Codebehind="DefaultPermissions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.FileAdmin.DefaultPermissions" %>

<CP:Container runat="server" id="Mpcontainer1" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl runat="server" ResourceName="CP_FilesAdmin_DefaultPermissions_Title" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonDescription">
			<CP:ResourceControl id="Section_Sub_Description" runat="server" ResourceName="CP_FilesAdmin_DefaultPermissions_Description"></CP:ResourceControl><br>
			<em>&nbsp;&nbsp;&nbsp;
				<CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_FilesAdmin_DefaultPermissions_Tip"></CP:ResourceControl></em>
		</div>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Files" />
		<div id="GrayGrid">
            <asp:Repeater runat="server" ID="PermissionList">
                <HeaderTemplate>
                    <table class="Grid" cellSpacing=0 cellPadding=0 width="100%">
                    <tbody>
                        <tr class="HeadingRow HeadingCellText HeadingCell">
                            <th><CP:ResourceControl runat="server" ResourceName="CP_FilesAdmin_PermissionsGrid_Name" /></th>
                            <th><CP:ResourceControl runat="server" ResourceName="CP_FilesAdmin_PermissionsGrid_View" /></th>
                            <th><CP:ResourceControl runat="server" ResourceName="CP_FilesAdmin_PermissionsGrid_Download" /></th>
                            <th><CP:ResourceControl runat="server" ResourceName="CP_FilesAdmin_PermissionsGrid_Delete" /></th>
                            <th><CP:ResourceControl runat="server" ResourceName="CP_FilesAdmin_PermissionsGrid_Edit" /></th>
                            <th><CP:ResourceControl runat="server" ResourceName="CP_FilesAdmin_PermissionsGrid_Reply" /></th>
                            <th><CP:ResourceControl runat="server" ResourceName="CP_FilesAdmin_PermissionsGrid_Post" /></th>
                            <th><CP:ResourceControl runat="server" ResourceName="CP_FilesAdmin_PermissionsGrid_LocalAttachment" /></th>
                            <th><CP:ResourceControl runat="server" ResourceName="CP_FilesAdmin_PermissionsGrid_RemoteAttachment" /></th>
                        </tr>    
                </HeaderTemplate>
                <ItemTemplate>
                        <tr class="Row">
                            <td><%# Eval("Name") %><asp:HiddenField ID="RoleId" runat="server" Value='<%# Eval("RoleIDString") %>' Visible="false" /></td>
                            <td><asp:CheckBox runat="server" ID="ViewCheckbox" Checked='<%# Eval("View") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="ReadCheckbox" Checked='<%# Eval("Download") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="DeleteCheckbox" Checked='<%# Eval("Delete") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="EditCheckbox" Checked='<%# Eval("Edit") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="ReplyCheckbox" Checked='<%# Eval("Reply") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="PostCheckbox" Checked='<%# Eval("Post") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="LocalAttachmentCheckbox" Checked='<%# Eval("LocalAttachment") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="RemoteAttachmentCheckbox" Checked='<%# Eval("RemoteAttachment") %>' /></td>
                        </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                        <tr class="AlternatingRow">
                            <td><%# Eval("Name") %><asp:HiddenField ID="RoleId" runat="server" Value='<%# Eval("RoleIDString") %>' Visible="false" /></td>
                            <td><asp:CheckBox runat="server" ID="ViewCheckbox" Checked='<%# Eval("View") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="ReadCheckbox" Checked='<%# Eval("Download") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="DeleteCheckbox" Checked='<%# Eval("Delete") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="EditCheckbox" Checked='<%# Eval("Edit") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="ReplyCheckbox" Checked='<%# Eval("Reply") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="PostCheckbox" Checked='<%# Eval("Post") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="LocalAttachmentCheckbox" Checked='<%# Eval("LocalAttachment") %>' /></td>
                            <td><asp:CheckBox runat="server" ID="RemoteAttachmentCheckbox" Checked='<%# Eval("RemoteAttachment") %>' /></td>
                        </tr>
                </AlternatingItemTemplate>
                <FooterTemplate>
                    </tbody>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <p style="FLOAT:right">
	        <br />
	        <cp:ResourceLinkButton id="SaveButton" runat="Server" CssClass="CommonTextButton" ResourceName="Save" />
        </p>
	</CP:Content>
</CP:Container>

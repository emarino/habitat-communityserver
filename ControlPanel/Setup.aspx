<%@ Page language="c#" Codebehind="Setup.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Setup" %>
<CP:ControlPanelSelectedNavigation SelectedTab="setup" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="SetupControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
	<div class="CommonDashboardTitle"><cp:resourcecontrol id="Resourcecontrol1" runat="server" resourcename="CP_Settings_Title"></cp:resourcecontrol></div>
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">
	<div runat="server" id="MembershipArea" class="CommonDashboardArea">
	    <fieldset>
	        <legend><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_Membership" ID="Resourcecontrol4"/></legend>
		    <div class="CommonDashboardContent">
			    <asp:Literal runat="server" id="MembershipDescription" />
			    <ul class="CommonDashboardActionList">
				    <li><asp:HyperLink runat="server" NavigateUrl="~/ControlPanel/Membership/" ID="Hyperlink1"><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_Membership_Action" ID="Resourcecontrol8"/></asp:HyperLink></li>
			    </ul>
		    </div>
	    </fieldset>
	</div>

	<div runat="server" id="BlogAdministrationArea" class="CommonDashboardArea">
	    <fieldset>
	        <legend><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_BlogAdministration" ID="Resourcecontrol3"/></legend>
		    <div class="CommonDashboardContent">
			    <asp:Literal runat="server" id="BlogAdministrationDescription" />
			    <ul class="CommonDashboardActionList">
				    <li><asp:HyperLink runat="server" NavigateUrl="~/ControlPanel/BlogAdmin/" ID="Hyperlink2"><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_BlogAdministration_Action" ID="Resourcecontrol24"/></asp:HyperLink></li>
			    </ul>
		    </div>
	    </fieldset>
	</div>

	<div runat="server" id="PhotoAdministrationArea" class="CommonDashboardArea">
        <fieldset>
            <legend><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_PhotoAdministration" ID="Resourcecontrol6"/></legend>
		    <div class="CommonDashboardContent">
			    <asp:Literal runat="server" id="PhotoAdministrationDescription" />
			    <ul class="CommonDashboardActionList">
				    <li><asp:HyperLink runat="server" NavigateUrl="~/ControlPanel/PhotoAdmin/" ID="Hyperlink4"><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_PhotoAdministration_Action" ID="Resourcecontrol23"/></asp:HyperLink></li>
			    </ul>
		    </div>
        </fieldset>
	</div>

	<div runat="server" id="FileAdministrationArea" class="CommonDashboardArea">
        <fieldset>
            <legend><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_FileAdministration" ID="Resourcecontrol9"/></legend>
		    <div class="CommonDashboardContent">
			    <asp:Literal runat="server" id="FileAdministrationDescription" />
			    <ul class="CommonDashboardActionList">
				    <li><asp:HyperLink runat="server" NavigateUrl="~/ControlPanel/FileAdmin/" ID="Hyperlink6"><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_FileAdministration_Action" ID="Resourcecontrol22"/></asp:HyperLink></li>
			    </ul>
		    </div>
        </fieldset>
	</div>

	<div runat="server" id="ForumAdministrationArea" class="CommonDashboardArea">
        <fieldset>
            <legend><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_ForumsAdministration" ID="Resourcecontrol11"/></legend>
		    <div class="CommonDashboardContent">
			    <asp:Literal runat="server" id="ForumsAdministrationDescription" />
			    <ul class="CommonDashboardActionList">
				    <li><asp:HyperLink runat="server" NavigateUrl="~/ControlPanel/Forums/" ID="Hyperlink8"><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_ForumsAdministration_Action" ID="Resourcecontrol20"/></asp:HyperLink></li>
			    </ul>
		    </div>
        </fieldset>
	</div>
	
	<div runat="server" id="ReaderAdministrationArea" class="CommonDashboardArea">
        <fieldset>
            <legend><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_ReaderAdministration" ID="Resourcecontrol5"/></legend>
		    <div class="CommonDashboardContent">
			    <asp:Literal runat="server" id="ReaderAdministrationDescription" />
			    <ul class="CommonDashboardActionList">
				    <li><asp:HyperLink runat="server" NavigateUrl="~/ControlPanel/Reader/" ID="Hyperlink3"><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_ReaderAdministration_Action" ID="Resourcecontrol7"/></asp:HyperLink></li>
			    </ul>
		    </div>
        </fieldset>
	</div>

</CP:Content>
<CP:Content runat="server" id="RightColumnRegion">
	<div class="CommonSidebar">
		<div class="CommonSidebarArea">
			<h4 class="CommonSidebarHeader"><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_Announcements" ID="Resourcecontrol15"/></h4>
			<div class="CommonSidebarContent">
				<cp:RepeaterPlusNone runat="server" id="Announcements">
					<ItemTemplate>
						<div style="margin-bottom: 8px; margin-top: 8px;">
						<a href="<%# DataBinder.Eval(Container.DataItem, "Link")%>"><%#DataBinder.Eval(Container.DataItem, "Title")%></a>										
						<div><%#DataBinder.Eval(Container.DataItem, "Description")%></div>
						</div>
					</ItemTemplate>
					<NoneTemplate>
						<div style="margin-bottom: 8px; margin-top: 8px;">
						<CP:ResourceControl runat="server" ResourceName="CP_Dashboard_NoAnnouncements" ID="Resourcecontrol2"/>
						</div>
					</NoneTemplate>
				</cp:RepeaterPlusNone>
				<CP:ResourceControl runat="server" ResourceName="CP_Dashboard_Announcements_More" ID="Resourcecontrol16"/>
			</div>
		</div>

		<div class="CommonSidebarArea">
			<h4 class="CommonSidebarHeader"><CP:ResourceControl runat="server" ResourceName="CP_Dashboard_Version" ID="Resourcecontrol17"/></h4>
			<div class="CommonSidebarContent">
				<asp:Literal runat="server" id="Version" />
				<asp:HyperLink runat="server" id="NewVersionAvailable" />
			</div>
		</div>		
	</div>
</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="ManageDisallowedNames.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Tools.ManageDisallowedNames" %>
<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="DisallowedNames_Title"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<cp:resourcecontrol id="Resourcecontrol1" runat="Server" resourcename="DisallowedNames_SubTitle"></cp:resourcecontrol></DIV>
		<DIV class="FixedWidthContainer">
		<asp:datalist id="ManageNames" runat="server" width="100%">
			<headertemplate>
				<table id="tblManageNames" class="tableBorder" cellspacing="1" cellpadding="3" width="100%"
					border="0">
					<tr>
						<td class="column" width="75px">
							<cp:resourcelabel runat="server" resourcename="DisallowedNames_Name" id="Resourcelabel3" />
						</td>
						<td class="column" width="140px" nowrap="nowrap">
							<cp:resourcelabel runat="server" resourcename="Actions" id="Resourcelabel5" />
						</td>
					</tr>
			</headertemplate>
			<itemtemplate>
				<tr>
					<td class="f">
						<asp:label runat="server" id="Name" />
					</td>
					<td class="fh">
						<asp:LinkButton id="EditName" commandname="Edit" runat="server" CssClass="CommonTextButton"></asp:LinkButton>
						<asp:LinkButton id="DeleteName" commandname="Delete" runat="server" CssClass="CommonTextButton"></asp:LinkButton>
					</td>
				</tr>
			</itemtemplate>
			<edititemtemplate>
				<tr>
					<td class="f">
						<asp:textbox id="DisallowedName" runat="server"></asp:textbox>
					</td>
					<td class="fh">
						<asp:LinkButton id="UpdateName" runat="server" commandname="Update" CssClass="CommonTextButton"></asp:LinkButton>
						<asp:LinkButton id="CancelName" runat="server" commandname="Cancel" CssClass="CommonTextButton"></asp:LinkButton>
					</td>
				</tr>
			</edititemtemplate>
			<footertemplate>
					<tr>
					<td class="f">
						<asp:textbox runat="server" id="NewName"></asp:textbox>
					</td>
					<td class="fh">
						<asp:LinkButton id="CreateName" commandname="Create" runat="server" CssClass="CommonTextButton" />
					</td>
				</tr>
				</table>
			</footertemplate>
		</asp:datalist>
		</div>
	</CP:Content>
</CP:Container>

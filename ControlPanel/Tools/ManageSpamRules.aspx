<%@ Page language="c#" AutoEventWireup="false" Inherits="CommunityServerWeb.ControlPanel.Tools.ManageSpamRules" CodeBehind="ManageSpamRules.aspx.cs" %>
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="Spam_ManageSpamRules_Title" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<cp:statusmessage runat="server" id="Status" />
		<asp:panel id="ConfigurationPanel" runat="server">
			<div class="CommonGroupedContentArea">
				<h3 class="CommonSubTitle"><cp:resourcecontrol runat="Server" resourcename="Spam_ManageSpamRules_SubTitle_Settings" /></h3>
				<div class="CommonDescription">
					<cp:resourcecontrol runat="Server" resourcename="Spam_ManageSpamRules_ConfigDescription" />
				</div>
				<table cellpadding="0" cellspacing="0" border="0">
				<tr>
				<td class="CommonFormFieldName">
					<asp:literal id="AutoModerateLabel" runat="Server" />
				</td>
				<td class="CommonFormField">
					<asp:textbox id="AutoModerateValue" runat="server"  />
					<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="AutoModerateValue" id="AutoModerateValueValidator"/>
				</td>
				</tr>

				<tr>
				<td class="CommonFormFieldName">
					<asp:literal id="AutoDeleteLabel" runat="Server" />
				</td>
				<td class="CommonFormField">
					<asp:textbox id="AutoDeleteValue" runat="server" />
					<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="AutoDeleteValue" id="AutoDeleteValueValidator"/><br />
				</td>
				</tr>
				
				<tr>
				<td class="CommonFormFieldName" colspan="2">
					<asp:checkbox id="EnableAudit" runat="server" />
					<asp:literal id="EnableAuditLabel" runat="server" />
				</td>
				</tr>
				</table>
			</div>
			
			<div class="CommonGroupedContentArea">
				<h3 class="CommonSubTitle"><cp:resourcecontrol runat="Server" resourcename="Spam_ManageSpamRules_SubTitle_Rules" /></h3>
				<div class="CommonDescription">
					<cp:resourcecontrol runat="Server" resourcename="Spam_ManageSpamRules_RulesDescription" />
				</div>
				<asp:repeater id="RulesList" runat="server">
					<headertemplate>
						<table cellspacing="0" cellpadding="3" border="0" style="border: 1px solid #999999;">
						<tr>
							<th><cp:resourcecontrol runat="Server" resourcename="Spam_ManageSpamRules_EnabledColumnHeader" /></th>
							<th><cp:resourcecontrol runat="Server" resourcename="Spam_ManageSpamRules_RuleNameColumnHeader" /></th>
							<th><cp:resourcecontrol runat="Server" resourcename="Spam_ManageSpamRules_SettingsColumnHeader" /></th>
						</tr>
					</headertemplate>
					<itemtemplate>
						<tr>
							<td rowspan="2" align="center" valign="top"><asp:checkbox id="RuleEnabled" value runat="Server" /><asp:literal id="RuleID" runat="Server" visible="false" /></td>
							<td><b><asp:literal id="RuleName" runat="Server" /></b></td>
							<td align="right" rowspan="2">
								<asp:Hyperlink id="Settings" runat="server" cssclass="CommonTextButton" />
							</td>
						</tr>
						<tr>
							<td><asp:literal id="RuleDesc" runat="Server" /></td>
						</tr>
					</itemtemplate>
					<AlternatingItemTemplate> 
						<tr bgcolor="#F0F0F0">
							<td rowspan="2" align="center" valign="top"><asp:checkbox id="RuleEnabled" value runat="Server" /><asp:literal id="RuleID" runat="Server" visible="false" /></td>
							<td><b><asp:literal id="RuleName" runat="Server" /></b></td>
							<td align="right" rowspan="2">
								<asp:hyperlink id="Settings" runat="server" cssclass="CommonTextButton" />
							</td>
						</tr>
						<tr bgcolor="#F0F0F0">
							<td><asp:literal id="RuleDesc" runat="Server" /></td>
						</tr>
					</AlternatingItemTemplate>
					<footertemplate>
						</table>
					</footertemplate>
				</asp:repeater>
			</div>
		
			<div class="CommonFormField PanelSaveButton">
				<cp:resourcelinkbutton id="SaveButton" runat="server" cssclass="CommonTextButton" resourcename="Save" />
				<CSControl:CSLinkButton id="RefreshButton" runat="server" cssclass="CommonTextButton" />
			</div>
		</asp:panel>
	</CP:Content>
</CP:Container>

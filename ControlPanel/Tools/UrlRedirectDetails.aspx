<%@ Page language="c#" Codebehind="UrlRedirectDetails.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Tools.UrlRedirectDetails" %>
<%@ Import Namespace="CommunityServer.Components" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelModalMaster.ascx" IsModal="true">
	<CP:Content id="bcr" runat="server">
		<DIV class="CommonContentArea">
			<DIV class="CommonContent">
				<P>
					<div align="left">
						<p>
							<cp:ResourceLabel runat="server" ResourceName="UrlRedirect_Details" />
						</p>
						<asp:textbox id="TinyUrl" runat="server" size="90" />
						<p>
							<cp:ResourceLabel runat="server" ResourceName="UrlRedirect_Details_Text1" />
							<asp:label id="Url" runat="server" />
						</p>
						<p>
							<cp:ResourceLabel runat="server" ResourceName="UrlRedirect_Details_Text2" />
							<b><asp:label id="Impressions" runat="server" /></b>
							<cp:ResourceLabel runat="server" ResourceName="UrlRedirect_Details_Text3" />
						</p>
					</div>
					<asp:label id="UrlID" runat="server" visible="false" />
					<asp:label id="Description" runat="server" visible="false" />
				</P>
			</DIV>
		</DIV>
	</CP:Content>
</CP:Container>

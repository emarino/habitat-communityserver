<%@ Page language="c#" Codebehind="PointSystem.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Settings.PointSystem" %>
<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="Admin_SiteSettings_Points"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<cp:statusmessage id="LicenseMessage" runat="server" />
		<cp:statusmessage id="Status" runat="server" />
		<DIV class="CommonDescription"></DIV>
		<TWC:TabbedPanes id="EditorTabs" runat="server"
		PanesCssClass="CommonPane"
		TabSetCssClass="CommonPaneTabSet"
		TabCssClasses="CommonPaneTab,CommonPaneTab1,CommonPaneTab2"
		TabSelectedCssClasses="CommonPaneTabSelected,CommonPaneTabSelected1,CommonPaneTabSelected2"
		TabHoverCssClasses="CommonPaneTabHover,CommonPaneTabHover1,CommonPaneTabHover2"
		>
		<TWC:TabbedPane runat="server">
			<Tab><CP:ResourceControl runat="server" ResourceName="Admin_SiteSettings_Points_Tab_General" /></Tab>
			<Content>
				<div class="CommonFormArea">
					<table border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td class="CommonFormFieldName">
								<cp:helpicon id="Helpicon4" runat="Server" resourcename="Admin_SiteSettings_Points_EnablePointSystem_Descr" />
								<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Points_EnablePointSystem" />
							</td>
							<td class="CommonFormField" nowrap="nowrap">
								<cp:yesnoradiobuttonlist id="EnablePointSystem" runat="server" repeatcolumns="2"></cp:yesnoradiobuttonlist>
							</td>
						</tr>
						<tr>
							<td class="CommonFormFieldName">
								<cp:helpicon id="Helpicon1" runat="Server" resourcename="Admin_SiteSettings_Points_PostDisplayLevel_Descr" />
								<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Points_PostDisplayLevel" />
							</td>
							<td class="CommonFormField" nowrap="nowrap">
								<cp:displayleveldropdownlist id="PointsPostDisplayLevel" runat="server"></cp:displayleveldropdownlist>
							</td>
						</tr>
						<tr>
							<td class="CommonFormFieldName">
								<cp:helpicon id="Helpicon2" runat="Server" resourcename="Admin_SiteSettings_Points_UserDisplayLevel_Descr" />
								<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Points_UserDisplayLevel" />
							</td>
							<td class="CommonFormField" nowrap="nowrap">
								<cp:displayleveldropdownlist id="PointsUserDisplayLevel" runat="server"></cp:displayleveldropdownlist>
							</td>
						</tr>
					</table>
				</div>
			</Content>
		</TWC:TabbedPane>
		<TWC:TabbedPane runat="server">
			<Tab><CP:ResourceControl runat="server" ResourceName="Admin_SiteSettings_Points_Tab_FactorValues" /></Tab>
			<Content>
				<div class="CommonFormArea">
					<table border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td class="CommonFormFieldName">
								<cp:helpicon id="Helpicon3" runat="Server" resourcename="Admin_SiteSettings_Points_PostFactor_Descr" />
								<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Points_PostFactor" />
							</td>
							<td class="CommonFormField" nowrap="nowrap">
								<asp:textbox id="PointsPostFactor" runat="server" class="ControlPanelTextInput" maxlength="64"></asp:textbox>
								<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="PointsPostFactor"
									id="PointsPostFactorValidator" />
							</td>
						</tr>
						<tr>
							<td class="CommonFormFieldName">
								<cp:helpicon id="Helpicon5" runat="Server" resourcename="Admin_SiteSettings_Points_ReplyFactor_Descr" />
								<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Points_ReplyFactor" />
							</td>
							<td class="CommonFormField" nowrap="nowrap">
								<asp:textbox id="PointsReplyFactor" runat="server" class="ControlPanelTextInput" maxlength="64"></asp:textbox>
								<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="PointsReplyFactor"
									id="PointsReplyFactorValidator" />
							</td>
						</tr>
						<tr>
							<td class="CommonFormFieldName">
								<cp:helpicon id="Helpicon6" runat="Server" resourcename="Admin_SiteSettings_Points_ReplierFactor_Descr" />
								<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Points_ReplierFactor" />
							</td>
							<td class="CommonFormField" nowrap="nowrap">
								<asp:textbox id="PointsReplierFactor" runat="server" class="ControlPanelTextInput" maxlength="64"></asp:textbox>
								<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="PointsReplierFactor"
									id="PointsReplierFactorValidator" />
							</td>
						</tr>
						<tr>
							<td class="CommonFormFieldName">
								<cp:helpicon id="Helpicon7" runat="Server" resourcename="Admin_SiteSettings_Points_RatingFactor_Descr" />
								<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Points_RatingFactor" />
							</td>
							<td class="CommonFormField" nowrap="nowrap">
								<asp:textbox id="PointsRatingFactor" runat="server" class="ControlPanelTextInput" maxlength="64"></asp:textbox>
								<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="PointsRatingFactor"
									id="PointsRatingFactorValidator" />
							</td>
						</tr>
						<tr>
							<td class="CommonFormFieldName">
								<cp:helpicon id="Helpicon8" runat="Server" resourcename="Admin_SiteSettings_Points_DownloadFactor_Descr" />
								<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Points_DownloadFactor" />
							</td>
							<td class="CommonFormField" nowrap="nowrap">
								<asp:textbox id="PointsDownloadFactor" runat="server" class="ControlPanelTextInput" maxlength="64"></asp:textbox>
								<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="PointsDownloadFactor"
									id="PointsDownloadFactorValidator" />
							</td>
						</tr>
						<tr>
							<td class="CommonFormFieldName">
								<cp:helpicon id="Helpicon9" runat="Server" resourcename="Admin_SiteSettings_Points_DownloaderFactor_Descr" />
								<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Points_DownloaderFactor" />
							</td>
							<td class="CommonFormField" nowrap="nowrap">
								<asp:textbox id="PointsDownloaderFactor" runat="server" class="ControlPanelTextInput" maxlength="64"></asp:textbox>
								<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="PointsDownloaderFactor"
									id="PointsDownloaderFactorValidator" />
							</td>
						</tr>
						<tr>
							<td class="CommonFormFieldName">
								<cp:helpicon id="Helpicon10" runat="Server" resourcename="Admin_SiteSettings_Points_FavoritePostFactor_Descr" />
								<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Points_FavoritePostFactor" />
							</td>
							<td class="CommonFormField" nowrap="nowrap">
								<asp:textbox id="PointsFavoritePostFactor" runat="server" class="ControlPanelTextInput" maxlength="64"></asp:textbox>
								<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="PointsFavoritePostFactor"
									id="PointsFavoritePostFactorValidator" />
							</td>
						</tr>
						<tr>
							<td class="CommonFormFieldName">
								<cp:helpicon id="Helpicon11" runat="Server" resourcename="Admin_SiteSettings_Points_FavoriteUserFactor_Descr" />
								<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Points_FavoriteUserFactor" />
							</td>
							<td class="CommonFormField" nowrap="nowrap">
								<asp:textbox id="PointsFavoriteUserFactor" runat="server" class="ControlPanelTextInput" maxlength="64"></asp:textbox>
								<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="PointsFavoriteUserFactor"
									id="PointsFavoriteUserFactorValidator" />
							</td>
						</tr>
						<tr>
							<td class="CommonFormFieldName">
								<cp:helpicon id="Helpicon12" runat="Server" resourcename="Admin_SiteSettings_Points_RaterFactor_Descr" />
								<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Points_RaterFactor" />
							</td>
							<td class="CommonFormField" nowrap="nowrap">
								<asp:textbox id="PointsRaterFactor" runat="server" class="ControlPanelTextInput" maxlength="64"></asp:textbox>
								<asp:requiredfieldvalidator runat="server" errormessage="*" font-bold="True" controltovalidate="PointsRaterFactor"
									id="PointsRaterFactorValidator" />
							</td>
						</tr>
					</table>
				</div>
			</Content>
		</TWC:TabbedPane>
		<TWC:TabbedPane runat="server">
			<Tab><CP:ResourceControl runat="server" ResourceName="Admin_SiteSettings_Points_Tab_Calculate" /></Tab>
			<Content>
				<div class="CommonFormArea">
					<div>
						<cp:resourcecontrol runat="server" resourcename="Admin_SiteSettings_Points_Calculate" />
					</div>
					<p />
					<div class="CommonFormField">
						<asp:button id="CalculateButton" runat="server" width="80px"></asp:button>
					</div>
				</div>
			</Content>
		</TWC:TabbedPane>
		</TWC:TabbedPanes>
		<p class="PanelSaveButton DetailsFixedWidth">
			<cp:resourcelinkbutton id="SaveButton" runat="server" resourcename="Save" cssclass="CommonTextButton"></cp:resourcelinkbutton></p>
		<DIV></DIV>
	</CP:Content>
</CP:Container>

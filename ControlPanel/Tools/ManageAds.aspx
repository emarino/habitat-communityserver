<%@ Page language="c#" Codebehind="ManageAds.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Tools.ManageAds" %>
<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="CP_Ads"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
			<DIV class="CommonDescription">
				<cp:resourcecontrol id="Resourcecontrol1" runat="Server" resourcename="CP_Ads_SubTitle"></cp:resourcecontrol><BR>
			</DIV>
		<CP:FileOnlyStatusMessage id="FOStatus" runat="server" visible="false"></CP:FileOnlyStatusMessage>
		<asp:PlaceHolder id="OptionHolder" runat="Server">
			<CP:ConfigOKStatusMessage id="Status" runat="server" visible="false"></CP:ConfigOKStatusMessage>
		<DIV class="FixedWidthContainer">
			<table cellpadding="0" cellspacing="0" border="0">
			<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_Ads_Enable_Help"></cp:helpicon>
				<cp:formlabel id="Formlabel3" runat="Server" resourcename="CP_Ads_Enable" controltolabel="enableAds"></cp:formlabel>
			</td>
			<td class="CommonFormField">
				<asp:checkbox id="enableAds" runat="Server" cssclass="ControlPanelTextInput"></asp:checkbox>
			</td>
			</tr>
			
			<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon id="Helpicon3" runat="Server" resourcename="CP_Ads_Inline_Enable_Help"></cp:helpicon>
				<cp:formlabel id="Formlabel2" runat="Server" resourcename="CP_Ads_Inline_Enable" controltolabel="enableInline"></cp:formlabel>
			</td>
			<td class="CommonFormField">
				<asp:checkbox id="enableInline" runat="Server" cssclass="ControlPanelTextInput"></asp:checkbox>
			</td>
			</tr>
			</table>
			
			<div class="CommonFormFieldName" colspan="2">
				<cp:helpicon id="Helpicon2" runat="Server" resourcename="CP_Ads_Roles_Help"></cp:helpicon>
				<cp:formlabel id="Formlabel1" runat="Server" resourcename="CP_Ads_Roles" controltolabel="enableAds"></cp:formlabel>
				<asp:checkboxlist id="rolelist" runat="Server" cssclass="ControlPanelTextInput" TextAlign="Right"
					RepeatLayout="Table" RepeatDirection="Vertical" RepeatColumns="2" CellSpacing="3" CellPadding="3" align="center"></asp:checkboxlist>
			</div>
		</div> 
			<p class="PanelSaveButton DetailsFixedWidth">
				<cp:ResourceLinkButton id="SaveButton" runat="Server" CssClass="CommonTextButton" ResourceName="Save"></cp:ResourceLinkButton></p>
		</asp:PlaceHolder>
	</CP:Content>
</CP:Container>

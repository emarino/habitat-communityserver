<%@ Page language="c#" Codebehind="ManageLicenses.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Tools.ManageLicenses" %>

<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="CP_Tools_ManageLicenses_Title" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonDescription">
			<cp:resourcecontrol id="Resourcecontrol1" runat="Server" resourcename="CP_Tools_ManageLicenses_Description" />
		</div>
		<div class="FixedWidthContainer">
		<cp:StatusMessage runat="server" id="Status" Visible="false" />
		
		<table cellspacing="0" border="0" cellpadding="0" width="100%">
			<tr>
				<td>
					<table cellspacing="0" border="0" cellpadding="0">
						<tr>
							<td class="CommonFormFieldName"><cp:ResourceControl runat="server" resourceName="CP_Tools_ManageLicenses_UploadLicenseFile" /></td>
							<td class="CommonFormField">
								<input type="file" runat="server" id="LicenseFile" />
							</td>
							<td class="CommonFormField">
								<asp:Button runat="server" id="UploadLicense" />
							</td>
						</tr>
					</table>
				</td>
				<td align="right">
					<table cellspacing="0" border="0" cellpadding="0">
						<tr>
							<td class="CommonFormFieldName"><cp:ResourceControl runat="server" resourceName="CP_Tools_ManageLicenses_UploadLicenseKey" /></td>
							<td class="CommonFormField">
								<asp:Textbox id="LicenseKey" runat="server" size="45" />
							</td>
							<td class="CommonFormField">
								<asp:Button runat="server" id="UploadLicenseKey" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
		<cp:RepeaterPlusNone id="Products" runat="server">
			<headertemplate>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
				<thead>
				<tr>
					<td class="CommonListHeaderLeftMost" width="300">
						<cp:resourcecontrol runat="Server" resourcename="CP_Tools_ManageLicenses_ProductList_Product" />
					</td>
					<td class="CommonListHeader">
						<cp:resourcecontrol runat="Server" resourcename="CP_Tools_ManageLicenses_ProductList_LicenseInstalled" ID="Resourcecontrol3"/>
					</td>
				</tr>
				</thead>
				<tbody>
			</headertemplate>
			<itemtemplate>
				<tr>
					<td class="CommonListCellLeftMost" valign="top">
						<b><cp:ResourceControl runat="server" resourceName="CP_Tools_ManageLicenses_Name" />:</b> <asp:Literal id="Name" runat="server" /><br />
						<b><cp:ResourceControl runat="server" resourceName="CP_Tools_ManageLicenses_Email" />:</b> <asp:Literal id="Email" runat="server" /><br/>
						<b><cp:ResourceControl runat="server" resourceName="CP_Tools_ManageLicenses_Company" />:</b> <asp:Literal id="Company" runat="server" /><br />
						<b><cp:ResourceControl runat="server" resourceName="CP_Tools_ManageLicenses_LicenseKey" />:</b> <asp:Literal id="LicenseID" runat="server" /><br />
						<b><cp:ResourceControl runat="server" resourceName="CP_Tools_ManageLicenses_Installed" />:</b> <asp:Literal id="InstalledDate" runat="server" /><br />
						<asp:Placeholder id="LeasedArea" runat="server">
							<b><i><cp:ResourceControl runat="server" resourceName="CP_Tools_ManageLicenses_Leased" /></i></b><br />
						</asp:Placeholder>
						<asp:LinkButton id="Delete" runat="server" CssClass="CommonTextButton" />
					</td>
					<td class="CommonListCell" valign="top">
						<CP:RepeaterPlusNone ID="Licenses" runat="server">
							<ItemTemplate>
								<b><asp:Literal id="ProductName" runat="server" /></b><br />
								<cp:ResourceControl runat="server" resourceName="CP_Tools_ManageLicenses_Version" />: <asp:Literal id="ProductVersion" runat="server" /><br />
								<span style="color: Red" id="Expires" runat="server"><cp:ResourceControl runat="server" resourceName="CP_Tools_ManageLicenses_Expires" />: <asp:Literal id="ExpirationDate" runat="server" /></span>
								<ul>
									<asp:Literal id="ProductAttributes" runat="server" />
								</ul>
							</ItemTemplate>
						</CP:RepeaterPlusNone>
					</td>
				</tr>
			</itemtemplate>
			<footertemplate>
				</tbody>
				</table>
			</footertemplate>
			<nonetemplate>
				<tr>
					<td class="CommonListCellLeftMost" colspan="3" align="center">
						<cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_ProductList_NoProducts" />
					</td>
				</tr>
			</nonetemplate>
		</cp:repeaterplusnone>
		
		<br />
		
		<p id="OverUsage" runat="server" visible="False">
			<cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_OverUsage" />
		</p>

		<table cellspacing="0" cellpadding="0" border="0" width="100%">
			<thead>
				<tr>
					<td class="CommonListHeaderLeftMost" colspan="2">
						<cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_Totals" />
					</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="CommonListCellLeftMost" valign="top" width="50%">
						<p>
							<b><cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_CommunityServer" /></b>
							<ul>
								<li>
									<cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_Branding" />
									<ul>
										<li><cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_Licensed" />: <asp:Literal id="CS_BrandingRestrictions" runat="server" /></li>
									</ul>
								</li>
								<li>
									<cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_Domains" />
									<ul>
										<li><cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_Licensed" />: <asp:Literal id="CS_NumberOfDomains" runat="server" /></li>
									</ul>
								</li>
								<li id="BlogsUsage" runat="server">
									<cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_Blogs" />
									<ul>
										<li><cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_Licensed" />: <asp:Literal id="CS_NumberOfBlogs_Licensed" runat="server" /></li>
										<li><cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_InUse" />: <asp:Literal id="CS_NumberOfBlogs_Usage" runat="server" /></li>
									</ul>
								</li>
								<li id="ForumsUsage" runat="server">
									<cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_Forums" />
									<ul>
										<li><cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_Licensed" />: <asp:Literal id="CS_NumberOfForums_Licensed" runat="server" /></li>
										<li><cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_InUse" />: <asp:Literal id="CS_NumberOfForums_Usage" runat="server" /></li>
									</ul>
								</li>
								<li id="GalleriesUsage" runat="server">
									<cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_Galleries" />
									<ul>
										<li><cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_Licensed" />: <asp:Literal id="CS_NumberOfGalleries_Licensed" runat="server" /></li>
										<li><cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_InUse" />: <asp:Literal id="CS_NumberOfGalleries_Usage" runat="server" /></li>
									</ul>
								</li>
								<li id="FilesUsage" runat="server">
									<cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_Files" />
									<ul>
										<li><cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_Licensed" />: <asp:Literal id="CS_NumberOfFiles_Licensed" runat="server" /></li>
										<li><cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_InUse" />: <asp:Literal id="CS_NumberOfFiles_Usage" runat="server" /></li>
									</ul>
								</li>
								<li id="MirrorsUsage" runat="server">
									<cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_Mirrors" />
									<ul>
										<li><cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_Licensed" />: <asp:Literal id="CS_NumberOfContentMirrors_Licensed" runat="server" /></li>
										<li><cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_InUse" />: <asp:Literal id="CS_NumberOfContentMirrors_Usage" runat="server" /></li>
									</ul>
								</li>
								<li>
									<cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_Components" />
									<ul>
										<li><cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_Licensed" />: <asp:Literal id="CS_EnabledComponents" runat="server" /></li>
									</ul>
								</li>
							</ul>
						</p>
					</td>
					<td class="CommonListCell" valign="top" width="50%">
						<p>
							<b><cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_MailGateway" /></b>
							<ul>
								<li id="AddressesUsage" runat="server">
									<cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_Addresses" />
									<ul>
										<li><cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_Licensed" />: <asp:Literal id="MG_NumberOfAddresses_Licensed" runat="server" /></li>
										<li><cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_InUse" />: <asp:Literal id="MG_NumberOfAddresses_Usage" runat="server" /></li>
									</ul>
								</li>
							</ul>
						</p>
						<p>
							<b><cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_NewsGateway" /></b>
							<ul>
								<li>
									<cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_Connections" />
									<ul>
										<li><cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_Licensed" />: <asp:Literal id="NNTP_NumberOfConnections" runat="server" /></li>
									</ul>
								</li>
							</ul>
						</p>
						<p>
							<b><cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_FtpGateway" /></b>
							<ul>
								<li>
									<cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_Connections" />
									<ul>
										<li><cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_Licensed" />: <asp:Literal id="FTP_NumberOfConnections" runat="server" /></li>
									</ul>
								</li>
							</ul>
						</p>
						<p>
							<b><cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_SecurityModules" /></b>
							<ul>
								<li>
									<cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_ModuleTypes" />
									<ul>
										<li><cp:resourcecontrol runat="server" ResourceName="CP_Tools_ManageLicenses_Licensed" />: <asp:Literal id="SM_ModuleTypes" runat="server" /></li>
									</ul>
								</li>
							</ul>
						</p>
					</td>
				</tr>
			</tbody>
		</table>

		</div>
	</CP:Content>
</CP:Container>


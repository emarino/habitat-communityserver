<%@ Page language="c#" Codebehind="MassEmailingAdmin.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Tools.MassEmailingAdmin" %>
<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="MassEmailing_Title"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<cp:resourcecontrol id="Resourcecontrol1" runat="Server" resourcename="MassEmailing_SubTitle"></cp:resourcecontrol></DIV>
		<cp:statusmessage id="Status" runat="server"></cp:statusmessage>
		<DIV class="FixedWidthContainer">
			<TABLE id="tblMassEmail" cellSpacing="0" cols="2" cellPadding="3" border="0">
				<TR>
					<TD class="CommonFormFieldName" noWrap>
						<cp:resourcelabel id="Resourcelabel1" runat="server" resourcename="MassEmailing_Recipients" name="Resourcelabel1"></cp:resourcelabel></TD>
					<TD class="CommonFormField">
						<cp:roledropdownlist id="RoleList" runat="server"></cp:roledropdownlist></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName" noWrap>
						<cp:resourcelabel id="Resourcelabel2" runat="server" resourcename="MassEmailing_Subject" name="Resourcelabel2"></cp:resourcelabel></TD>
					<TD class="CommonFormField">
						<asp:textbox id="Subject" runat="server" columns="80" maxlength="255"></asp:textbox></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName" vAlign="top" noWrap>
						<cp:resourcelabel id="Resourcelabel3" runat="server" resourcename="MassEmailing_Message" name="Resourcelabel3"></cp:resourcelabel></TD>
					<TD class="CommonFormField">
						<CSControl:editor id="EditorMessage" runat="server" columns="110" height="250" width="500px" /></TD>
				</TR>
			</TABLE>
		</DIV>
		<P class="PanelSaveButton DetailsFixedWidth">
			<asp:LinkButton id="SendButton" runat="server" CssClass="CommonTextButton"></asp:LinkButton></P>
	</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="ManageUrlRedirects.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Tools.ManageUrlRedirects" %>
<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="UrlRedirects_Title"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<cp:resourcecontrol id="Resourcecontrol1" runat="Server" resourcename="UrlRedirects_SubTitle"></cp:resourcecontrol></DIV>
		<asp:datalist id="ManageRedirects" runat="server" width="100%">
			<headertemplate>
				<table id="tblManageUrlRedirects" class="tableBorder" cellspacing="1" cellpadding="3" width="100%"
					border="0">
					<tr>
						<td class="column">
							<cp:resourcelabel runat="server" resourcename="UrlRedirect_ID" id="Resourcelabel2" />
						</td>
						<td class="column">
							<cp:resourcelabel runat="server" resourcename="UrlRedirect_Url" id="Resourcelabel3" />
						</td>
						<td class="column">
							<cp:resourcelabel runat="server" resourcename="UrlRedirect_Description" id="Resourcelabel4" />
						</td>
						<td class="column">
							<cp:resourcelabel runat="server" resourcename="UrlRedirect_Impressions" id="Resourcelabel1" />
						</td>
						<td class="column" nowrap="nowrap">
							<cp:resourcelabel runat="server" resourcename="Actions" id="Resourcelabel5" />
						</td>
					</tr>
			</headertemplate>
			<itemtemplate>
				<tr>
					<td class="f">
						<asp:label runat="server" id="ID" />
					</td>
					<td class="fh">
						<asp:label runat="server" id="Url" />
					</td>
					<td class="fh">
						<asp:label runat="server" id="Description" />
					</td>
					<td class="fh">
						<asp:label runat="server" id="Impressions" />
					</td>
					<td class="fh" nowrap="nowrap">
						<asp:Hyperlink id="ViewUrlRedirect" runat="server" cssclass="CommonTextButton" />
						<asp:LinkButton id="EditUrlRedirect" commandname="Edit" runat="server" CssClass="CommonTextButton"></asp:LinkButton>
						<asp:LinkButton id="DeleteUrlRedirect" commandname="Delete" runat="server" CssClass="CommonTextButton"></asp:LinkButton>
					</td>
				</tr>
			</itemtemplate>
			<edititemtemplate>
				<tr>
					<td class="f">
						<asp:label runat="server" id="ID" />
					</td>
					<td class="f">
						<asp:textbox id="UrlRedirectUrl" size="40" runat="server"></asp:textbox>
					</td>
					<td class="fh">
						<asp:textbox id="UrlRedirectDescription" size="35" runat="server"></asp:textbox>
					</td>
					<td class="fh">
						<asp:label runat="server" id="Impressions" />
					</td>
					<td class="fh">
						<asp:LinkButton id="UpdateUrlRedirect" runat="server" commandname="Update" CssClass="CommonTextButton"></asp:LinkButton>
						<asp:LinkButton id="CancelUrlRedirect" runat="server" commandname="Cancel" CssClass="CommonTextButton"></asp:LinkButton>
					</td>
				</tr>
			</edititemtemplate>
			<footertemplate>
					<tr>
					<td class="f">
						&nbsp;
					</td>
					<td class="fh">
						<asp:textbox runat="server" size="40" id="NewUrlRedirectUrl"></asp:textbox>
					</td>
					<td class="fh">
						<asp:textbox runat="server" size="35" id="NewUrlRedirectDescription"></asp:textbox>
					</td>
					<td class="fh">
						&nbsp;
					</td>
					<td class="fh">
						<asp:LinkButton id="CreateUrlRedirect" commandname="Create" runat="server" CssClass="CommonTextButton" />
					</td>
				</tr>
				</table>
			</footertemplate>
		</asp:datalist>
	</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="ManageCensorships.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Tools.ManageCensorships" %>
<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="Censorship_Title"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<cp:resourcecontrol id="Resourcecontrol1" runat="Server" resourcename="Censorship_SubTitle"></cp:resourcecontrol></DIV>
		<DIV class="FixedWidthContainer">
		<asp:datalist id="ManageCensors" runat="server" width="100%">
			<headertemplate>
				<table id="tblManageCensors" class="tableBorder" cellspacing="1" cellpadding="3" width="100%"
					border="0">
					<tr>
						<td class="column" width="75px">
							<cp:resourcelabel runat="server" resourcename="Censorship_Word" id="Resourcelabel3" />
						</td>
						<td class="column" width="100%" nowrap="nowrap">
							<cp:resourcelabel runat="server" resourcename="Censorship_Replacement" id="Resourcelabel4" />
						</td>
						<td class="column" width="140px" nowrap="nowrap">
							<cp:resourcelabel runat="server" resourcename="Actions" id="Resourcelabel5" />
						</td>
					</tr>
			</headertemplate>
			<itemtemplate>
				<tr>
					<td class="f">
					    <span  title="aa<%# DataBinder.Eval(Container.DataItem, "Word") %>">
						<asp:label runat="server" id="Word" />
						</span>
					</td>
					<td class="fh">
					    <span  title="aa<%# DataBinder.Eval(Container.DataItem, "Replacement") %>">
						<asp:label runat="server" id="Replacement" />
						</span>
					</td>
					<td class="fh">
						<asp:LinkButton id="EditCensor" commandname="Edit" runat="server" CssClass="CommonTextButton"></asp:LinkButton>
						<asp:LinkButton id="DeleteCensor" commandname="Delete" runat="server" CssClass="CommonTextButton"></asp:LinkButton>
					</td>
				</tr>
			</itemtemplate>
			<edititemtemplate>
				<tr>
					<td class="f">
						<asp:textbox id="CensorWord" runat="server" MaxLength="256"></asp:textbox>
					</td>
					<td class="fh">
						<asp:textbox id="CensorReplacement" runat="server" MaxLength="256"></asp:textbox>
					</td>
					<td class="fh">
						<asp:LinkButton id="UpdateCensor" runat="server" commandname="Update" CssClass="CommonTextButton"></asp:LinkButton>
						<asp:LinkButton id="CancelCensor" runat="server" commandname="Cancel" CssClass="CommonTextButton"></asp:LinkButton>
					</td>
				</tr>
			</edititemtemplate>
			<footertemplate>
					<tr>
					<td class="f">
						<asp:textbox runat="server" id="NewCensorWord" MaxLength="256"></asp:textbox>
					</td>
					<td class="fh">
						<asp:textbox runat="server" id="NewCensorReplacement" MaxLength="256"></asp:textbox>
					</td>
					<td class="fh">
						<asp:LinkButton id="CreateCensor" commandname="Create" runat="server" CssClass="CommonTextButton" />
					</td>
				</tr>
				</table>
			</footertemplate>
		</asp:datalist>
		</div>
	</CP:Content>
</CP:Container>

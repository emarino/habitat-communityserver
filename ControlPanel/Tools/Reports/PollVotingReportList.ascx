<%@ Control Language="c#" AutoEventWireup="false" Codebehind="PollVotingReportList.ascx.cs" Inherits="CommunityServerWeb.ControlPanel.Tools.Reports.PollVotingReportList" TargetSchema="http://schemas.microsoft.com/intellisense/ie5"%>
<script type="text/javascript">
  function onCallbackError(excString)
  {
    if (confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_CallBackWarning") %>')) alert(excString); 
    <%= Grid1.ClientID %>.Page(1); 
  }
</script>
<div id="GrayGrid">
	<CA:GRID id="Grid1" runat="server" RunningMode="Callback" ClientSideOnCallbackError="onCallbackError" CssClass="Grid">
		<Levels>
			<CA:GridLevel DataKeyField="PostID">
				<Columns>
					<CA:GridColumn DataField="Username" AllowSorting="false" HeadingText="ResourceManager.CP_Tools_PollVotingGrid_User" DataCellClientTemplateId="UserTemplate" TextWrap="True" />
					<CA:GridColumn DataField="UserDisplayName" AllowSorting="false" HeadingText="ResourceManager.CP_Tools_PollVotingGrid_UserDisplayName" TextWrap="True" />
					<CA:GridColumn DataField="UserEmail" AllowSorting="false" HeadingText="ResourceManager.CP_Tools_PollVotingGrid_UserEmail" TextWrap="True" />
					<CA:GridColumn DataField="Answer" AllowSorting="false" HeadingText="ResourceManager.CP_Tools_PollVotingGrid_Vote" FormatString="MMM dd yyyy, hh:mm tt" />
					<CA:GridColumn DataField="ViewUserURL" Visible="False" />
				</Columns>
			</CA:GridLevel>
		</Levels>
		<ClientTemplates>
			<CA:ClientTemplate Id="UserTemplate">
				<a href="## DataItem.GetMember('ViewUserURL').Text ##" target="_blank"><strong>## DataItem.GetMember('Username').Text ##</strong></a>
			</CA:ClientTemplate>
		</ClientTemplates>
	</CA:GRID>
</div>

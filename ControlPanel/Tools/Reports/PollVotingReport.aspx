<%@ Page language="c#" Codebehind="PollVotingReport.aspx.cs" AutoEventWireup="false" Inherits="CommunityServerWeb.ControlPanel.Tools.Reports.PollVotingReport" %>
<%@ Register TagPrefix="CP" TagName = "VoteList" Src = "~/ControlPanel/Tools/Reports/PollVotingReportList.ascx" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<asp:Literal runat="server" id="PollTitle" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonDescription">
			<CP:ResourceControl runat="server" ResourceName="CP_Tools_PollVotingReport_Description" ID="Resourcecontrol2" NAME="Resourcecontrol2"/>
		</div>
		<CP:VoteList id="VoteList1" runat="Server" />
		<p />
		<asp:HyperLink runat="server" id="DownloadCSV" CssClass="CommonImageTextButton" style="background-image: url(/../../images/excel.gif);" />
	</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="ExceptionsReport.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Tools.ExceptionsReport" %>

<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
	<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="CP_Tools_ExceptionsReport_Title"></cp:resourcecontrol>
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">
	<fieldset>
	<legend><cp:resourcecontrol runat="server" resourcename="CP_Tools_ExceptionsReport_Criteria" id="Resourcecontrol1" name="Resourcecontrol1"/></legend>
	<table cellspacing="0" cellpadding="2" width="90%" border="0">
	<tr>
		<td><cp:resourcecontrol runat="server" resourcename="CP_Tools_ExceptionsReport_ExceptionType" id="Resourcecontrol2" name="Resourcecontrol2"/></td>
		<td><asp:dropdownlist id="ExceptionType" runat="server"></asp:dropdownlist></td>
		<td align="center" rowspan="4"><cp:resourcebutton id="SelectDomain" runat="server" resourcename="CP_Tools_ExceptionsReport_Go"></cp:resourcebutton></td>
	</tr>
	<tr>
		<td><cp:resourcecontrol runat="server" resourcename="CP_Tools_ExceptionsReport_MinFrequency" id="Resourcecontrol3" name="Resourcecontrol3"/></td>
		<td><asp:dropdownlist id="MinFrequency" runat="server"></asp:dropdownlist></td>
	</tr>
	<tr>
		<td><cp:resourcecontrol runat="server" resourcename="CP_Tools_ExceptionsReport_SortBy" /></td>
		<td><asp:dropdownlist id="SortBy" runat="server" /></td>
	</tr>
	<tr>
		<td><cp:resourcecontrol runat="server" resourcename="CP_Tools_ExceptionsReport_IncludeUnknown" /></td>
		<td><asp:checkbox id="IncludeUnknown" runat="server" EnableViewState="true"/></td>
	</tr>
	</table>
	</fieldset>
	<asp:repeater id="ReportRepeater" runat="server">
		<headertemplate>
			<div class="CommonListArea">
			<table id="Listing" cellspacing="0" cellpadding="0" border="0" width="100%">
			<thead>
			<tr>
				<th class="CommonListHeaderLeftMost" width="2%">&nbsp;</th>
				<th class="CommonListHeader"><cp:resourcecontrol runat="server" resourcename="CP_Tools_ExceptionsReport_ExceptionType" id="Resourcecontrol4" /></th>
				<th class="CommonListHeader"><cp:resourcecontrol runat="server" resourcename="CP_Tools_ExceptionsReport_IPAddress" id="Resourcecontrol5" /></th>
				<th class="CommonListHeader"><cp:resourcecontrol runat="server" resourcename="CP_Tools_ExceptionsReport_LastOccurred" id="Resourcecontrol6" /></th>
				<th class="CommonListHeader"><cp:resourcecontrol runat="server" resourcename="CP_Tools_ExceptionsReport_Total" id="Resourcecontrol7" /></th>
			</tr>
			</thead>
		</headertemplate>
		<itemtemplate>
			<tr>
				<td class="CommonListCellLeftMost"><asp:checkbox id="BulkEdit" runat="Server" /><asp:literal id="ExceptionID" runat="Server" visible="false" /></td>
				<td class="CommonListCell"><strong><asp:literal id="ExceptionType" runat="Server" /></strong>&nbsp;</td>
				<td class="CommonListCell"><asp:literal id="IPAddress" runat="Server" />&nbsp;</td>
				<td class="CommonListCell"><asp:literal id="DateLastOccurred" runat="Server" />&nbsp;</td>
				<td class="CommonListCell"><asp:literal id="Frequency" runat="Server" />&nbsp;</td>
			</tr>
			<tr>
				<td colspan="5" align="left" class="CommonListCellLeftMost">
					<b><cp:resourcecontrol runat="server" resourcename="CP_Tools_ExceptionsReport_UserAgent" /></b> <asp:literal id="UserAgent" runat="Server" /><br />
					<b><cp:resourcecontrol runat="server" resourcename="CP_Tools_ExceptionsReport_Path" /></b> <asp:literal id="Path" runat="Server" /> 
					<cp:resourcecontrol runat="server" resourcename="CP_Tools_ExceptionsReport_AsHttp" /> <asp:literal id="HttpVerb" runat="Server" /><br />
					<b><cp:resourcecontrol runat="server" resourcename="CP_Tools_ExceptionsReport_Referrer" /></b>
					<asp:literal id="HttpReferrer" runat="Server" /><br />
					<b><cp:resourcecontrol runat="server" resourcename="CP_Tools_ExceptionsReport_Message" /></b>
					<asp:literal id="Message" runat="Server" /><br />
					<asp:literal id="LoggedStackTrace" runat="Server" />
				</td>
			</tr>
		</itemtemplate>
		<footertemplate>
			</table>
			</div>		
		</footertemplate>
	</asp:repeater>   
    <div align="CommonPagingArea">
        <cp:LinkButtonPager id="pager" runat="server" PageSize="10" />
    </div>
	<p class="PanelSaveButton">
		<cp:resourcebutton id="DeleteSelected" runat="server" resourcename="CP_Tools_ExceptionsReport_Delete_All" />
		<cp:resourcebutton id="DeleteAll" runat="server" resourcename="CP_Tools_ExceptionsReport_Delete_Selected" />
	</p>

</CP:Content>
</CP:Container>

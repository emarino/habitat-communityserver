<%@ Page language="c#" Codebehind="JobsReport.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Tools.JobsReport" %>
<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="CP_Tools_JobsReport_Title"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<cp:resourcecontrol id="Resourcecontrol1" runat="Server" resourcename="CP_Tools_JobsReport_Description"></cp:resourcecontrol></DIV>

		<asp:repeater id="ThreadsRepeater" runat="server">
			<itemtemplate>

				<DIV class="CommonGroupedContentArea">
					<H3 class="CommonSubTitle">
						<asp:label id="ThreadTitle" runat="server"></asp:label></H3>
					<UL class="CommonFloatList">
						<LI>
							<cp:resourcecontrol id="Resourcecontrol10" runat="server" resourcename="CP_Tools_JobsReport_Created"></cp:resourcecontrol>&nbsp;
							<asp:label id="Created" runat="server"></asp:label>
						<LI>
							<cp:resourcecontrol id="Resourcecontrol6" runat="server" resourcename="CP_Tools_JobsReport_LastStart"></cp:resourcecontrol>&nbsp;
							<asp:label id="LastStart" runat="server"></asp:label>
						<LI>
							<cp:resourcecontrol id="Resourcecontrol7" runat="server" resourcename="CP_Tools_JobsReport_LastStop"></cp:resourcecontrol>&nbsp;
							<asp:label id="LastStop" runat="server"></asp:label>
						<LI>
							<cp:resourcecontrol id="Resourcecontrol8" runat="server" resourcename="CP_Tools_JobsReport_IsRunning"></cp:resourcecontrol>&nbsp;
							<asp:label id="IsRunning" runat="server"></asp:label>
						<LI>
							<cp:resourcecontrol id="Resourcecontrol9" runat="server" resourcename="CP_Tools_JobsReport_Interval"></cp:resourcecontrol>&nbsp;
							<asp:label id="Minutes" runat="server"></asp:label>&nbsp;
							<cp:resourcecontrol id="Resourcecontrol22" runat="server" resourcename="CP_Tools_JobsReport_Minutes"></cp:resourcecontrol>
						</LI>
					</UL>

					<asp:repeater id="TasksRepeater" runat="server">
						<headertemplate>
							<div class="CommonListArea">
								<table id="Listing" cellspacing="0" cellpadding="0" border="0" width="100%">
									<thead>
										<tr>
											<th class="CommonListHeaderLeftMost">
												<cp:resourcecontrol runat="server" resourcename="CP_Tools_JobsReport_JobType" id="Resourcecontrol15" /></th>
											<th class="CommonListHeader">
												<cp:resourcecontrol runat="server" resourcename="CP_Tools_JobsReport_JobEnabled" id="Resourcecontrol16" /></th>
											<th class="CommonListHeader">
												<cp:resourcecontrol runat="server" resourcename="CP_Tools_JobsReport_JobIsRunning" id="Resourcecontrol17" /></th>
											<th class="CommonListHeader">
												<cp:resourcecontrol runat="server" resourcename="CP_Tools_JobsReport_JobStart" id="Resourcecontrol18" /></th>
											<th class="CommonListHeader">
												<cp:resourcecontrol runat="server" resourcename="CP_Tools_JobsReport_JobEnd" id="Resourcecontrol19" /></th>
											<th class="CommonListHeader">
												<cp:resourcecontrol runat="server" resourcename="CP_Tools_JobsReport_JobSuccess" id="Resourcecontrol20" /></th>
										</tr>
									</thead>
						</headertemplate>
						<itemtemplate>
							<tr>
								<td class="CommonListCellLeftMost"><%# DataBinder.Eval(Container.DataItem, "JobType") %></td>
								<td class="CommonListCell"><%# CommunityServer.ControlPanel.Components.ResourceManager.GetString(DataBinder.Eval(Container.DataItem, "Enabled").ToString()) %></td>
								<td class="CommonListCell"><%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsRunning")) ? "<b>" + CommunityServer.ControlPanel.Components.ResourceManager.GetString("True") + "</b>" : CommunityServer.ControlPanel.Components.ResourceManager.GetString("False") %></td>
								<td class="CommonListCell"><%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "LastStarted")) == DateTime.MinValue ? "&nbsp;" : Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "LastStarted")).ToString(CommunityServer.Components.CSContext.Current.User.Profile.DateFormat + ", hh:mm tt")%></td>
								<td class="CommonListCell"><%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "LastEnd")) == DateTime.MinValue ? "&nbsp;" : Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "LastEnd")).ToString(CommunityServer.Components.CSContext.Current.User.Profile.DateFormat + ", hh:mm tt")%></td>
								<td class="CommonListCell"><%# Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "LastSuccess")) == DateTime.MinValue ? "&nbsp;" : Convert.ToDateTime(DataBinder.Eval(Container.DataItem, "LastSuccess")).ToString(CommunityServer.Components.CSContext.Current.User.Profile.DateFormat + ", hh:mm tt")%></td>
							</tr>
						</itemtemplate>
						<footertemplate>
							</table>
							</div>
						</footertemplate>
					</asp:repeater>
				</DIV>
			</itemtemplate>
		</asp:repeater>
	</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="ManageIP.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Tools.ManageIPAddresses" %>
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
<CP:Content id=DescriptionRegion runat="server"><cp:resourcecontrol id=RegionTitle runat="Server" resourcename="CP_Tools_ManageIP_Title" /></CP:Content> 
<CP:Content id=TaskRegion runat="Server">
<cp:StatusMessage runat="server" id="Status" />
<DIV><cp:resourcecontrol id=RegionSubTitle runat="Server" resourcename="CP_Tools_ManageIP_SubTitle" /></DIV>
<asp:label id=StatusMessage runat="Server"></asp:label>
<TABLE cellSpacing=0 cellPadding=2 border=0>
  <TR>
    <TH class=CommonFormFieldName><cp:resourcecontrol id="Resourcecontrol1" runat="Server" resourcename="CP_Tools_ManageIP_IPAddress" /></TH>
    <TH class=CommonFormFieldName><cp:resourcecontrol id="Resourcecontrol2" runat="Server" resourcename="CP_Tools_ManageIP_Range" /></TH>
    <TH class=CommonFormFieldName>&nbsp;</TH></TR>
  <TR>
    <TD class=CommonFormField>
<asp:textbox id=IPStart runat="Server"></asp:textbox></TD>
    <TD class=CommonFormField>
<asp:textbox id=IPEnd runat="Server"></asp:textbox></TD>
    <TD class=CommonFormField>
      <DIV class=PanelSaveButton>
<cp:resourcelinkbutton id=AddButton runat="server" resourcename="CP_Tools_ManageIP_AddIPAddress" cssclass="CommonTextButton"></cp:resourcelinkbutton></DIV></TD></TR></TABLE>
<asp:repeater id=BannedIPS runat="Server">
		<headertemplate>
			<table width = "400px" class = "Listing">
			<tr>
				<th class="CommonFormFieldName"><cp:resourcecontrol id="Resourcecontrol3" runat="Server" resourcename="CP_Tools_ManageIP_IPAddressRange" /></th>
				<th class="CommonFormFieldName"><cp:resourcecontrol id="Resourcecontrol4" runat="Server" resourcename="Delete" /></th>
			</tr>
		</headertemplate>
		<itemtemplate>
			<tr>
				<td class="CommonFormField">
<asp:literal id = "IP" runat = "Server" /></td>
				<td class="CommonFormField">
<cp:resourcelinkbutton id = "DeleteButton" resourcename="Delete" cssclass="CommonTextButton" runat = "Server" /></td>
			</tr>
		</itemtemplate>
		<alternatingitemtemplate>
			<tr class="Alt">
				<td class="CommonFormField">
<asp:literal id = "IP" runat = "Server" /></td>
				<td class="CommonFormField">
<cp:resourcelinkbutton id = "DeleteButton" resourcename="Delete" cssclass="CommonTextButton" runat = "Server" /></td>
			</tr>
		</alternatingitemtemplate>
		<footertemplate>
			</table>
		</footertemplate>
		</asp:repeater>
</CP:Content>
</CP:Container>

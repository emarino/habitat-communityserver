<%@ Page language="c#" Codebehind="ManageArticles.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Tools.ManageArticles" %>
<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="CP_Tools_ManageArticle_Title" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<cp:resourcecontrol id="Resourcecontrol1" runat="Server" resourcename="CP_Tools_ManageArticle_SubTitle" /><BR>
		</DIV>
		<asp:repeater id="articleslist" runat="server">
			<headertemplate>
				<p>
					<asp:hyperlink text='<%# CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Tools_ManageArticle_Create") %>' runat="server" NavigateUrl="<%# CommunityServer.Components.Contents.NewArticleUrl()  %>" CssClass="CommonTextButton" />
				</p>
				<DIV class="FixedWidthContainer">
				<table cellspacing="10">
					<tr>
						<th align="left">
							<cp:resourcecontrol runat="Server" resourcename="CP_Tools_ManageArticle_LastModified" /></th>
						<th align="left">
							<cp:resourcecontrol runat="Server" resourcename="CP_Tools_ManageArticle_ArticleName" /></th>
						<th align="left">
							<cp:resourcecontrol runat="Server" resourcename="CP_Tools_ManageArticle_Actions" /></th>
					</tr>
			</headertemplate>
			<itemtemplate>
				<tr>
					<td>
						<asp:Literal id="LastModified" runat="Server" /></td>
					<td>
						<asp:Literal id="ContentName" runat="Server" />
					</td>
					<td>
						<cp:hyperlink ResourceName="CP_Tools_ManageArticle_View" id="ViewLink" runat="server" CssClass="CommonTextButton" />&nbsp;
						<cp:hyperlink ResourceName="CP_Tools_ManageArticle_Edit" id="EditLink" runat="server" CssClass="CommonTextButton" />
					</td>
				</tr>
			</itemtemplate>
			<footertemplate>
				</table>
				</div>
			</footertemplate>
		</asp:repeater>
	</CP:Content>
</CP:Container>

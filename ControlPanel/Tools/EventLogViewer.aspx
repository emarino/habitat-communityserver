<%@ Page language="c#" Codebehind="EventLogViewer.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Tools.EventLogViewer" %>

<script runat="server">
string ShowMessage(string message)
{
	int maxLength = 255;
	string output = message;
	if (message.Length > maxLength + 3)
		output = message.Substring(0, maxLength) + "...";

	return Context.Server.HtmlEncode(output);
}
</script>
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
<CP:Content id=DescriptionRegion runat="server">
<cp:resourcecontrol id=RegionTitle runat="Server" resourcename="CP_Tools_EventLogViewer_Title" /></CP:Content>
<CP:Content id=TaskRegion runat="Server">
<DIV id=GridWithMenu>
<DIV id=GrayGrid>
<CA:grid id=Grid1 runat="Server" ManualPaging="true" EditOnClickSelectedItem="false" AllowMultipleSelect="false" AllowEditing="false" loadingpanelposition="MiddleCenter" loadingpanelclienttemplateid="Loading" groupbysortimageheight="10" groupbysortimagewidth="10" groupbysortdescendingimageurl="group_desc.gif" groupbysortascendingimageurl="group_asc.gif" groupingnotificationtextcssclass="GridHeaderText" indentcellwidth="22" treelineimageheight="19" treelineimagewidth="22" treelineimagesfolderurl="~/controlpanel/images/caimages/lines" pagerimagesfolderurl="~/controlpanel/images/caimages/pager" imagesbaseurl="~/controlpanel/images/caimages" preexpandongroup="true" groupingpagesize="5" sliderpopupoffsetx="20" slidergripwidth="9" sliderwidth="150" sliderheight="20" pagerbuttonheight="22" pagerbuttonwidth="41" pagertextcssclass="GridFooterText" pagerstyle="Slider" groupbytextcssclass="GroupByText" groupbycssclass="GroupByCell" footercssclass="GridFooter" headercssclass="GridHeader" searchonkeypress="true" searchtextcssclass="GridHeaderText" showsearchbox="false" showheader="false" cssclass="Grid" runningmode="Client">
					<levels>
						<CA:gridlevel datakeyfield="EntryID" showtableheading="false" showselectorcells="false" rowcssclass="Row" columnreorderindicatorimageurl="reorder.gif" datacellcssclass="DataCell" headingcellcssclass="HeadingCell" headingcellhovercssclass="HeadingCellHover" headingcellactivecssclass="HeadingCellActive" headingrowcssclass="HeadingRow" headingtextcssclass="HeadingCellText" selectedrowcssclass="SelectedRow" groupheadingcssclass="GroupHeading" sortascendingimageurl="asc.gif" sortdescendingimageurl="desc.gif" sortimagewidth="10" sortimageheight="19" editcellcssclass="EditDataCell" editfieldcssclass="EditDataField" editcommandclienttemplateid="EditCommandTemplate" insertcommandclienttemplateid="InsertCommandTemplate" alternatingrowcssclass="AlternatingRow" allowsorting="false" allowreordering = "false" allowgrouping="false">
							<columns>
								<CA:gridcolumn datafield="EntryID" headingtext="ResourceManager.CP_Tools_EventLogViewer_GridCol_ID" width="35" datacellclienttemplateid="DetailTemplate" />
								<CA:gridcolumn datafield="Message" headingtext="ResourceManager.CP_Tools_EventLogViewer_GridCol_Message" datacellservertemplateid="MessageTemplate" TextWrap="True" />
								<CA:gridcolumn datafield="Category" headingtext="ResourceManager.CP_Tools_EventLogViewer_GridCol_Category" />
								<CA:gridcolumn datafield="EventDate" headingtext="ResourceManager.CP_Tools_EventLogViewer_GridCol_EventDate" />
								<CA:gridcolumn datafield="MachineName" headingtext="ResourceManager.CP_Tools_EventLogViewer_GridCol_MachineName" />
								<CA:gridcolumn datafield="EventID" headingtext="ResourceManager.CP_Tools_EventLogViewer_GridCol_EventID" width="45" />
								<CA:gridcolumn datafield="EventType" headingtext="ResourceManager.CP_Tools_EventLogViewer_GridCol_EventType" width="55" />
								<CA:gridcolumn datafield="SettingsID" headingtext="ResourceManager.CP_Tools_EventLogViewer_GridCol_SettingsID" width="55" />
							</columns>
						</CA:gridlevel>
					</levels>
					<clienttemplates>
						<CA:clienttemplate id="Loading">
							<img src = "../images/caimages/spinner.gif" /><strong>Loading</strong>...please wait.
						</CA:clienttemplate>
						<CA:clienttemplate id="DetailTemplate">
							<a href="javascript:Telligent_Modal.Open('EventEntryDetails.aspx?EntryID=##DataItem.GetMember("EntryID").Text##', 600, 400, null);">##DataItem.GetMember("EntryID").Text##</a>
						</CA:clienttemplate>
					</clienttemplates>
					<servertemplates>
						<CA:gridservertemplate id="MessageTemplate">
							<template>
								<%# ShowMessage(Container.DataItem["Message"].ToString().Trim()) %>
							</template>
						</CA:gridservertemplate>
					</servertemplates>
				</CA:grid></DIV></DIV></CP:Content>
</CP:Container>

<%@ Page language="c#" AutoEventWireup="false" Inherits="CommunityServerWeb.ControlPanel.Tools.SpamRuleSettings" CodeBehind="SpamRuleSettings.aspx.cs" %>
<CP:Container runat="server" id="MPContainer" thememasterfile="ControlPanelModalMaster.ascx">
	<CP:Content id="bcr" runat="Server">
		<div class="CommonContentArea">
		<div class="CommonContent">
		<div class="CommonFormArea">
			<h3 class="CommonSubTitle"><cp:resourcecontrol runat="Server" resourcename="Spam_SpamRuleSettings_Title" /></h3>

			<asp:repeater id="SettingsList" runat="server">
				<itemtemplate> 
					<div class="CommonFormFieldName">
						<asp:literal id="Key" runat="Server" visible="false" />
						<asp:literal id="Desc" runat="Server" />
					</div>
					<div class="CommonFormField">
						<asp:textbox id="ConfigValue" runat="server" />
					</div>
				</itemtemplate>
			</asp:repeater>

			<div class="CommonFormField PanelSaveButton">
				<cp:resourcelinkbutton id="SaveButton" runat="server" cssclass="CommonTextButton" resourcename="Save" />
			</div>
		</div>
		</div>
		</div>
	</CP:Content>
</CP:Container>

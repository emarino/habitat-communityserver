<%@ Page CodeBehind="AddAggregatedFeed.aspx.cs" Language="c#" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Reader.AddAggregatedFeed" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="Reader" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_Reader_AddAggFeed_title"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<TABLE>
			<TR>
				<TD>
					<asp:listbox id="lFeeds" runat="server" SelectionMode="Multiple" height="250"></asp:listbox></TD>
			</TR>
		</TABLE>
		<P>* =
			<CP:ResourceControl id="Resourcecontrol2" runat="server" ResourceName="CP_Reader_AddAggFeed_Disclaimer"></CP:ResourceControl><BR>
			<BR>
			<asp:Literal id="lStatus" runat="server" />
			<CP:ResourceLinkButton id="AddFeeds" ResourceName="CP_Reader_AggFeeds_AddSelectedFeed" CssClass="CommonTextButton"
				Runat="server"></CP:ResourceLinkButton>&nbsp;&nbsp;
			<CP:ResourceLinkButton id="Done" ResourceName="CP_Reader_AggFeeds_Done" CssClass="CommonTextButton" Runat="server"></CP:ResourceLinkButton>
		</P>
	</CP:Content>
</CP:Container>

<%@ Control CodeBehind="AggregatedFeedListControl.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Reader.AggregatedFeedListControl" %>

<div id="GrayGrid">
<CA:grid runat="Server" id="Grid1" CssClass="Grid" AutoCallBackOnDelete="true" >
    <Levels>
        <CA:gridlevel datakeyfield="PostID" >
            <Columns>
				<CA:GridColumn DataField="FeedId" HeadingText="ResourceManager.CP_Reader_AggFeeds_Col_FeedId" visible="true" />
                <CA:GridColumn DataField="Title" HeadingText="ResourceManager.CP_Reader_AggFeeds_Col_Title" />
                <CA:GridColumn DataField="Link" HeadingText="ResourceManager.CP_Reader_AggFeeds_Col_Link" />
                <CA:GridColumn DataField="Url" HeadingText="ResourceManager.CP_Reader_AggFeeds_Col_Url" visible="false" />
                <CA:GridColumn DataField="LastUpdateDate" HeadingText="ResourceManager.CP_Reader_AggFeeds_Col_LastUpdateDate" />
                <CA:gridcolumn headingtext="ResourceManager.CP_Reader_AggFeeds_Col_Actions" DataCellClientTemplateId="ActionTemplate" />
            </Columns>
        </CA:gridlevel>
    </Levels>
    <ClientTemplates>
        <CA:ClientTemplate Id="ActionTemplate">
            <a href="RemoveAggregatedFeed.aspx?FeedId=## DataItem.GetMember("FeedId").Text ##" onclick="return confirm('Are you sure you want to stop aggregating this feed?');" class="CommonTextButton">Remove</a> 
        </CA:ClientTemplate>
    </ClientTemplates>
</CA:grid>
</div>

<p>
	<CP:ResourceLinkButton Runat="server" ResourceName="CP_Reader_AggFeeds_AddFeed" ID="NewFeed" CssClass="CommonTextButton" />
</p>
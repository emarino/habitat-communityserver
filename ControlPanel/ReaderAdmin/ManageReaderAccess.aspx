<%@ Page language="c#" Codebehind="ManageReaderAccess.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Reader.ManageReaderAccess" %>
<cp:controlpanelselectednavigation selectednavitem="CommonTasks" runat="server" id="sn" />
<CP:Container runat="server" id="MPContainer" thememasterfile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="RegionTitle" runat="Server" resourcename="CP_Reader_ManageRoles"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
			<DIV class="CommonDescription">
				<cp:resourcecontrol id="Resourcecontrol1" runat="Server" resourcename="CP_Reader_ManageRoles_Subtitle"></cp:resourcecontrol><BR>
			</DIV>
		<CP:FileOnlyStatusMessage id="FOStatus" runat="server" visible="false"></CP:FileOnlyStatusMessage>
		<asp:PlaceHolder id="OptionHolder" runat="Server">
			<CP:ConfigOKStatusMessage id="Status" runat="server" visible="false"></CP:ConfigOKStatusMessage>
		<DIV class="FixedWidthContainer">
			<DIV class="CommonFormFieldName">
				<cp:helpicon id="Helpicon2" runat="Server" resourcename="CP_Reader_Roles_Help"></cp:helpicon>
				<cp:formlabel id="Formlabel1" runat="Server" resourcename="CP_Reader_Roles" controltolabel="enableAds"></cp:formlabel>
				<asp:checkboxlist id="rolelist" runat="Server" cssclass="ControlPanelTextInput" TextAlign="Right"
					RepeatLayout="Table" RepeatDirection="Vertical" RepeatColumns="2" CellSpacing="3" CellPadding="3" align="center"></asp:checkboxlist></DIV>
		</div> 
			<p class="PanelSaveButton DetailsFixedWidth">
				<cp:ResourceLinkButton id="SaveButton" runat="Server" CssClass="CommonTextButton" ResourceName="Save"></cp:ResourceLinkButton></p>
		</asp:PlaceHolder>
	</CP:Content>
</CP:Container>

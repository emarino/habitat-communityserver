<%@ Page language="c#" Codebehind="Settings.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Reader.Settings" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server"><CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_Reader_Settings_Title"></CP:ResourceControl></CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<CP:FileOnlyStatusMessage id="FOStatus" runat="server" visible="false"></CP:FileOnlyStatusMessage>

		<div class="FixedWidthContainer">
		
		<H3 class="CommonSubTitle"><CP:ResourceControl id="Resourcecontrol2" runat="server" ResourceName="CP_Reader_Settings_SubTitle"></CP:ResourceControl></H3>
			<table cellpadding="0" cellspacing="0" border="0">
			
			<tr>
			<td class="CommonFormFieldName">
			<cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_Reader_Settings_LastModifiedInterval_Description"></cp:helpicon>
			<cp:formlabel id="Formlabel2" runat="Server" resourcename="CP_Reader_Settings_LastModifiedInterval"
				controltolabel="lastModifiedInterval"></cp:formlabel>
			</td>
			<td class="CommonFormField">
			<asp:textbox id="lastModifiedInterval" runat="Server" width="50" cssclass="ControlPanelTextInput"></asp:textbox>
			<asp:requiredfieldvalidator id="Requiredfieldvalidator1" runat="Server" name="Requiredfieldvalidator1" controltovalidate="lastModifiedInterval"
				display="Dynamic" text="*"></asp:requiredfieldvalidator>
			</td>
			</tr>
			</table>
		
		<H3 class="CommonSubTitle"><CP:ResourceControl id="Resourcecontrol3" runat="server" ResourceName="CP_Reader_Settings_SubTitle_Roller"></CP:ResourceControl></H3>
			<table cellpadding="0" cellspacing="0" border="0">
			<tr>
			<td class="CommonFormFieldName">
			<cp:helpicon id="Helpicon2" runat="Server" resourcename="CP_Reader_Settings_TruncationLength_Description"></cp:helpicon>
			<cp:formlabel id="Formlabel1" runat="Server" resourcename="CP_Reader_Settings_TruncationLength"
				controltolabel="truncationLength"></cp:formlabel>
			</td>
			<td class="CommonFormField">
			<asp:textbox id="truncationLength" runat="Server" width="50" cssclass="ControlPanelTextInput"></asp:textbox>
			<asp:requiredfieldvalidator id="Requiredfieldvalidator2" runat="Server" name="Requiredfieldvalidator1" controltovalidate="truncationLength"
				display="Dynamic" text="*"></asp:requiredfieldvalidator>
			</td>
			</tr>
			
			<tr>
			<td class="CommonFormFieldName">
			<cp:helpicon id="Helpicon7" runat="Server" resourcename="CP_Reader_Settings_PageSize_Description"></cp:helpicon>
			<cp:formlabel id="Formlabel7" runat="Server" resourcename="CP_Reader_Settings_PageSize" controltolabel="truncationLength"></cp:formlabel>
			</td>
			<td class="CommonFormField">
			<asp:textbox id="pagerSize" runat="Server" width="50" cssclass="ControlPanelTextInput"></asp:textbox>
			<asp:requiredfieldvalidator id="Requiredfieldvalidator3" runat="Server" name="Requiredfieldvalidator2" controltovalidate="pagerSize"
				display="Dynamic" text="*"></asp:requiredfieldvalidator>
			</td>
			</tr>
			
			<tr>
			<td class="CommonFormFieldName">
			<cp:helpicon id="Helpicon8" runat="Server" resourcename="CP_Reader_Settings_RSSResultSize_Description"></cp:helpicon>
			<cp:formlabel id="Formlabel8" runat="Server" resourcename="CP_Reader_Settings_RSSResultSize" controltolabel="truncationLength"></cp:formlabel>
			</td>
			<td class="CommonFormField">
			<asp:textbox id="rssResultSize" runat="Server" width="50" cssclass="ControlPanelTextInput"></asp:textbox>
			<asp:requiredfieldvalidator id="Requiredfieldvalidator4" runat="Server" name="Requiredfieldvalidator2" controltovalidate="rssResultSize"
				display="Dynamic" text="*"></asp:requiredfieldvalidator>
			</td>
			</tr>
			</table>
		</div>
		<p class="PanelSaveButton DetailsFixedWidth">
			<cp:ResourceLinkButton id="SaveButton" runat="Server" CssClass="CommonTextButton" ResourceName="Save"></cp:ResourceLinkButton></p>
	</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="FeedEditor.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Reader.FeedEditor" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="ManageFeeds" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_Reader_FeedEditor_Header"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonFormArea">
			<TABLE cellSpacing="0" cellPadding="2" border="0">
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon2" runat="Server" resourcename="CP_Reader_FeedEditor_FeedId_Description"></cp:helpicon>
						<cp:resourcecontrol id="ResourceControl7" runat="server" resourcename="CP_Reader_FeedEditor_FeedId"
							name="ResourceControl7"></cp:resourcecontrol></TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<asp:Literal id="lFeedId" runat="server"></asp:Literal></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_Reader_FeedEditor_Url_Description"></cp:helpicon>
						<cp:resourcecontrol id="Resourcecontrol2" runat="server" resourcename="CP_Reader_FeedEditor_Url" name="ResourceControl7"></cp:resourcecontrol></TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<asp:TextBox id="txtUrl" MaxLength="255" size="40" runat="server"></asp:TextBox></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon3" runat="Server" resourcename="CP_Reader_FeedEditor_Title_Description"></cp:helpicon>
						<cp:resourcecontrol id="Resourcecontrol3" runat="server" resourcename="CP_Reader_FeedEditor_Title" name="ResourceControl7"></cp:resourcecontrol></TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<asp:Literal id="lTitle" runat="server"></asp:Literal></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon4" runat="Server" resourcename="CP_Reader_FeedEditor_Link_Description"></cp:helpicon>
						<cp:resourcecontrol id="Resourcecontrol4" runat="server" resourcename="CP_Reader_FeedEditor_Link" name="ResourceControl7"></cp:resourcecontrol></TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<asp:Literal id="lLink" runat="server"></asp:Literal></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon5" runat="Server" resourcename="CP_Reader_FeedEditor_Language_Description"></cp:helpicon>
						<cp:resourcecontrol id="Resourcecontrol5" runat="server" resourcename="CP_Reader_FeedEditor_Language"
							name="ResourceControl7"></cp:resourcecontrol></TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<asp:Literal id="lLanguage" runat="server"></asp:Literal></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon6" runat="Server" resourcename="CP_Reader_FeedEditor_Generator_Description"></cp:helpicon>
						<cp:resourcecontrol id="Resourcecontrol6" runat="server" resourcename="CP_Reader_FeedEditor_Generator"
							name="ResourceControl7"></cp:resourcecontrol></TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<asp:Literal id="lGenerator" runat="server"></asp:Literal></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon7" runat="Server" resourcename="CP_Reader_FeedEditor_SubscribeDate_Description"></cp:helpicon>
						<cp:resourcecontrol id="Resourcecontrol8" runat="server" resourcename="CP_Reader_FeedEditor_SubscribeDate"
							name="ResourceControl7"></cp:resourcecontrol></TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<asp:Literal id="lSubscribeDate" runat="server"></asp:Literal></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon8" runat="Server" resourcename="CP_Reader_FeedEditor_LastUpdateDate_Description"></cp:helpicon>
						<cp:resourcecontrol id="Resourcecontrol9" runat="server" resourcename="CP_Reader_FeedEditor_LastUpdateDate"
							name="ResourceControl7"></cp:resourcecontrol></TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<asp:Literal id="lLastUpdateDate" runat="server"></asp:Literal></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon9" runat="Server" resourcename="CP_Reader_FeedEditor_LastModified_Description"></cp:helpicon>
						<cp:resourcecontrol id="Resourcecontrol10" runat="server" resourcename="CP_Reader_FeedEditor_LastModified"
							name="ResourceControl7"></cp:resourcecontrol></TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<asp:Literal id="lLastModified" runat="server"></asp:Literal></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon10" runat="Server" resourcename="CP_Reader_FeedEditor_ETag_Description"></cp:helpicon>
						<cp:resourcecontrol id="Resourcecontrol11" runat="server" resourcename="CP_Reader_FeedEditor_ETag" name="ResourceControl7"></cp:resourcecontrol></TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<asp:Literal id="lETag" runat="server"></asp:Literal></TD>
				</TR>
				<TR>
					<TD class="CommonFormFieldName">
						<cp:helpicon id="Helpicon11" runat="Server" resourcename="CP_Reader_FeedEditor_FeedState_Description"></cp:helpicon>
						<cp:resourcecontrol id="Resourcecontrol12" runat="server" resourcename="CP_Reader_FeedEditor_FeedState"
							name="ResourceControl7"></cp:resourcecontrol></TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<asp:Literal id="lFeedState" runat="server"></asp:Literal>
						
					</TD>
				</TR>
			</TABLE>

		<div class="CommonFormField" align="right">
			<cp:resourcelinkbutton id="SaveButton" runat="server" cssclass="CommonTextButton" resourcename="Save"></cp:resourcelinkbutton>
		</div>

		</DIV>
	</CP:Content>
</CP:Container>

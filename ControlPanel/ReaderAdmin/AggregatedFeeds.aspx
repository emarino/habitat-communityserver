<%@ Page language="c#" Codebehind="AggregatedFeeds.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Reader.AggregatedFeeds" %>
<%@ Register TagPrefix="ReaderPanel" TagName="AggregatedFeedList" Src = "~/ControlPanel/ReaderAdmin/AggregatedFeedListControl.ascx" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server"><CP:ResourceControl runat="server" ResourceName="CP_Reader_AggFeeds_Header" ID="Resourcecontrol1"/></CP:Content>
	<CP:Content id="TaskRegion" runat="Server">

		<ReaderPanel:AggregatedFeedList id="aggregatedFeeds" runat="server" />
		
	</CP:Content>
</CP:Container>

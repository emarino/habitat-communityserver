<%@ Page language="c#" Codebehind="default.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Reader._default" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="Reader" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server"><CP:ResourceControl runat="server" ResourceName="CP_Reader_default_title" ID="Resourcecontrol1"/></CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<cp:ResourceControl runat="server" resourcename="CP_Reader_default_Description" ID="Resourcecontrol2"/>
		</DIV>
	</CP:Content>
</CP:Container>

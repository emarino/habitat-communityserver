<%@ Page language="c#" Codebehind="ManageFeeds.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Reader.ManageFeeds" %>
<%@ Register TagPrefix="ReaderPanel" TagName="FeedList" Src = "~/ControlPanel/ReaderAdmin/FeedListControl.ascx" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="SetupControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server"><CP:ResourceControl runat="server" ResourceName="CP_Reader_ManageFeeds_Title" ID="Resourcecontrol1"/></CP:Content>
	<CP:Content id="TaskRegion" runat="Server">

		<ReaderPanel:FeedList id="feedList" runat="server" />
		
	</CP:Content>
</CP:Container>

<%@ Control Language="c#" AutoEventWireup="false" Codebehind="FeedListControl.ascx.cs" Inherits="CommunityServer.ControlPanel.Reader.FeedListControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<div id="GrayGrid">
<CA:grid runat="Server" id="Grid1" CssClass="Grid" AutoCallBackOnDelete="true" >
    <Levels>
        <CA:gridlevel datakeyfield="PostID" >
            <Columns>
				<CA:GridColumn DataField="FeedId" HeadingText="ResourceManager.CP_Reader_ManageFeeds_Col_FeedId" visible="false" />
                <CA:GridColumn DataField="Title" HeadingText="ResourceManager.CP_Reader_ManageFeeds_Col_Title" />
                <CA:GridColumn DataField="Link" HeadingText="ResourceManager.CP_Reader_ManageFeeds_Col_Link" />
                <CA:GridColumn DataField="Url" HeadingText="ResourceManager.CP_Reader_ManageFeeds_Col_Url" visible="false" />
                <CA:GridColumn DataField="LastUpdateDate" HeadingText="ResourceManager.CP_Reader_ManageFeeds_Col_LastUpdateDate" />
                <CA:gridcolumn headingtext="ResourceManager.CP_Reader_ManageFeeds_Col_Actions" DataCellClientTemplateId="ActionTemplate" />
            </Columns>
        </CA:gridlevel>
    </Levels>
    <ClientTemplates>
        <CA:ClientTemplate Id="ActionTemplate">
            <a href="FeedEditor.aspx?FeedId=## DataItem.GetMember("FeedId").Text ##" class="CommonTextButton">Edit</a> 
        </CA:ClientTemplate>
    </ClientTemplates>
</CA:grid>
</div>


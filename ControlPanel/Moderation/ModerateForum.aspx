<%@ Page language="c#" Codebehind="ModerateForum.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Moderation.ModerateForum" %>
<%@ Import Namespace="CommunityServer.Discussions.Components" %>
<%@ Import Namespace="CommunityServer.Components" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="CommonTasks" runat="server" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ModerationControlPanelMaster.ascx">
	<CP:Content id="TopLeft" runat="server">
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr >
				<td width="100%"><CP:ResourceControl runat="server" ResourceName="CP_Moderation_CurrentForum" />&nbsp;<CP:CurrentForum runat = "Server" /></td>
				<td style="padding-right: 3px;" nowrap="nowrap"><CP:HyperLink ID="HyperLink1" Runat="server" CssClass="CommonTextButton" urlname="moderate_Home" ResourceName="CP_Moderation_Switch_ChangeForum" />
			</tr>
		</table>
	</CP:Content>
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl runat="server" resourcename="CP_Moderation_Posts" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonFormArea">
			<cp:StatusMessage runat="server" id="NoPostsToModerate" Visible="false" Success="false" ResourceFile="ControlPanelResources.xml" ResourceName="CP_Moderation_NoPostsToModerate" />

			<asp:Repeater id="PostRepeater" Runat="server">
				<HeaderTemplate>
					<ul class="ForumPostList">
				</HeaderTemplate>
				<ItemTemplate>
					<li id="Post_<%# ((Post)Container.DataItem).PostID %>">

						<asp:HyperLink id="Approve" runat="server" cssclass="CommonTextButton" />
						<asp:HyperLink id="ApproveView" runat="server" cssclass="CommonTextButton" />
						<asp:HyperLink id="Delete" runat="server" cssclass="CommonTextButton" />
						<asp:HyperLink id="JoinSplit" runat="server" cssclass="CommonTextButton" />
						<asp:HyperLink id="Move" runat="server" cssclass="CommonTextButton" />
						<asp:HyperLink id="EditUser" runat="server" cssclass="CommonTextButton" visible="false" />
						<asp:HyperLink id="ToggleUserModeration" runat="server" cssclass="CommonTextButton" />
						<asp:HyperLink id="ModerationHistory" runat="server" cssclass="CommonTextButton" visible="false" />

						<cp:PostFlatPreview id="PostPreview" LinkToParent="true" runat="server">
						    <SkinTemplate>
                                <asp:Repeater id="PostRepeater" Runat="server">
	                                <HeaderTemplate>
		                                <ul class="ForumPostList">
	                                </HeaderTemplate>
	                                <ItemTemplate>
		                                <li>
		                                <div class="ForumPostArea">
			                                <h4 class="ForumPostHeader">
				                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
					                                <tr valign="middle">
						                                <td align="left"><a runat="server" id="PostAnchor"><asp:Image runat="server" border="0" ImageUrl="~/ControlPanel/images/icon_post_show.gif" /></a>&nbsp;<%# Formatter.FormatAgoDate (((ForumPost) Container.DataItem).PostDate) %></td>
					                                </tr>
				                                </table>
			                                </h4>
			                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
				                                <tr valign=top>
					                                <td rowspan="2" class="ForumPostUserArea">
						                                <div class="ForumPostUserContent">
						                                <ul class="ForumPostUserPropertyList">
							                                <li class="ForumPostUserName">
								                                <cp:UserOnlineStatus runat="server" id="OnlineStatus" />
								                                <asp:Literal runat="server" id="Username" />
							                                </li>
							                                <li class="ForumPostUserAvatar">
								                                <cp:UserAvatar runat="server" Border="1" PadImage="False" id="Avatar" />
							                                </li>
							                                <li class="ForumPostUserIcons">
								                                <cp:PostIcons runat="server" id="PostIcon" />
							                                </li>
							                                <li class="ForumPostUserAttribute">
								                                <cp:UserAttribute FormatString="{0}" Attribute="Joined" runat="server" id="JoinedAttribute" />
							                                </li>
							                                <li class="ForumPostUserAttribute">
								                                <cp:UserAttribute FormatString="{0}" Attribute="Location" runat="server" id="LocationAttribute" />
							                                </li>
							                                <li class="ForumPostUserAttribute">
								                                <cp:UserAttribute FormatString="{0}" Attribute="Posts" runat="server" id="PostsAttribute" />
							                                </li>
							                                <li class="ForumPostUserAttribute">
								                                <cp:UserAttribute FormatString="{0}" Attribute="Points" runat="server" id="PointsAttribute" />
							                                </li>
							                                <li class="ForumPostRoleIcons">
								                                <cp:RoleIcons runat="server" id="RoleIcon" EnablePadding="false"/>
							                                </li>
						                                </ul>
						                                </div>
					                                </td>
					                                <td class="ForumPostContentArea">
						                                <div class="ForumPostTitleArea">
							                                <h4 class="ForumPostTitle">
								                                <asp:Literal runat="server" id="Emoticon" />
								                                <asp:HyperLink runat="server" id="Subject" Target="_blank" />
							                                </h4>
							                                
							                                <div>
							                                    <CP:ForumPostAttachment runat="server" id="PostAttachment" CssClass="ForumPostAttachment" />
                                                            </div>
                                							
							                                <div class="ForumPostAttachment">
							                                </div>

							                                <div class="ForumPostThreadStatus">
							                                </div>

							                                <div class="ForumPostButtons">
							                                </div>
						                                </div>
                                						
						                                <table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%" style="table-layout: fixed;">
							                                <tr><td>
							                                <div class="ForumPostBodyArea">
							                                <div class="ForumPostContentText" id = "PostViewWrapper" runat="server">
								                                <cp:TextPost runat="server" id="TextPost">
								                                    <SkinTemplate>
    								                                    <asp:Literal id="Body" Runat="server" />
                                                                        <asp:Literal id="EditNotes" runat="server" />
                                                                        <asp:Literal id="Signature" Runat="server" />
								                                    </SkinTemplate>
								                                </cp:TextPost>
							                                </div>
							                                </div>
							                                </td></tr>
						                                </table>
					                                </td>
				                                </tr>
				                                <tr valign="bottom">
					                                <td class="ForumPostFooterArea">								
						                                <ul class="ForumPostStatistics" style="clear: both;">
							                                &nbsp;
						                                </ul>
					                                </td>
				                                </tr>
			                                </table>
		                                </div>
		                                </li>
	                                </ItemTemplate>
	                                <FooterTemplate>
		                                </ul>
	                                </FooterTemplate>
                                </asp:Repeater>
						    </SkinTemplate>
						</cp:PostFlatPreview>
						<br/><br/>
					</li>
				</ItemTemplate>
				<FooterTemplate>
					</ul>
				</FooterTemplate>
			</asp:Repeater>

		</div>
	</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="DeletePost.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Moderation.DeletePost" %>
<%@ Register TagPrefix="CP" TagName="ModerationForumListControl" Src = "~/ControlPanel/Moderation/ModerationForumListControl.ascx" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Controls" %>
<%@ Import Namespace="CommunityServer.Discussions.Components" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="CommonTasks" runat="server" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ModerationControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl runat="server" resourcename="CP_Moderation_DeletePost" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonFormArea">
			<cp:DeletePost runat="server">
			    <SkinTemplate>
                    <div class="CommonContentArea">
	                    <div class="CommonContent">
		                    <span id="Span1" runat="server">
			                    <h3 class="CommonSubTitle">
				                    <cp:ResourceLabel runat="server" ResourceName="DeletePost_Title" />
			                    </h3>
			                    <cp:PostFlatPreview id="PostPreview" LinkToParent="true" runat="server">
			                        <SkinTemplate>
                                        <asp:Repeater id="PostRepeater" Runat="server">
	                                        <HeaderTemplate>
		                                        <ul class="ForumPostList">
	                                        </HeaderTemplate>
	                                        <ItemTemplate>
		                                        <li>
		                                        <div class="ForumPostArea">
			                                        <h4 class="ForumPostHeader">
				                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
					                                        <tr valign="middle">
						                                        <td align="left"><a runat="server" id="PostAnchor"><asp:Image runat="server" border="0" ImageUrl="~/controlpanel/images/icon_post_show.gif" id="Themedimage1" /></a>&nbsp;<%# Formatter.FormatAgoDate (((ForumPost) Container.DataItem).PostDate) %></td>
					                                        </tr>
				                                        </table>
			                                        </h4>
			                                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
				                                        <tr valign=top>
					                                        <td rowspan="2" class="ForumPostUserArea">
						                                        <div class="ForumPostUserContent">
						                                        <ul class="ForumPostUserPropertyList">
							                                        <li class="ForumPostUserName">
								                                        <cp:UserOnlineStatus runat="server" id="OnlineStatus" />
								                                        <asp:Literal runat="server" id="Username" />
							                                        </li>
							                                        <li class="ForumPostUserAvatar">
								                                        <cp:UserAvatar runat="server" Border="1" PadImage="False" id="Avatar" />
							                                        </li>
							                                        <li class="ForumPostUserIcons">
								                                        <cp:PostIcons runat="server" id="PostIcon" />
							                                        </li>
							                                        <li class="ForumPostUserAttribute">
								                                        <cp:UserAttribute FormatString="{0}" Attribute="Joined" runat="server" id="JoinedAttribute" />
							                                        </li>
							                                        <li class="ForumPostUserAttribute">
								                                        <cp:UserAttribute FormatString="{0}" Attribute="Location" runat="server" id="LocationAttribute" />
							                                        </li>
							                                        <li class="ForumPostUserAttribute">
								                                        <cp:UserAttribute FormatString="{0}" Attribute="Posts" runat="server" id="PostsAttribute" />
							                                        </li>
							                                        <li class="ForumPostUserAttribute">
								                                        <cp:UserAttribute FormatString="{0}" Attribute="Points" runat="server" id="PointsAttribute" />
							                                        </li>
							                                        <li class="ForumPostRoleIcons">
								                                        <cp:RoleIcons runat="server" id="RoleIcon" EnablePadding="false"/>
							                                        </li>
						                                        </ul>
						                                        </div>
					                                        </td>
					                                        <td class="ForumPostContentArea">
						                                        <div class="ForumPostTitleArea">
							                                        <h4 class="ForumPostTitle">
								                                        <asp:Literal runat="server" id="Emoticon" />
								                                        <asp:HyperLink runat="server" id="Subject" Target="_blank" />
							                                        </h4>
                                        							
							                                        <div class="ForumPostAttachment">
							                                        </div>

							                                        <div class="ForumPostThreadStatus">
							                                        </div>

							                                        <div class="ForumPostButtons">
							                                        </div>
						                                        </div>
                                        						
						                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%" style="table-layout: fixed;">
							                                        <tr><td>
							                                        <div class="ForumPostBodyArea">
							                                        <div class="ForumPostContentText" id = "PostViewWrapper" runat="server">
								                                        <cp:TextPost runat="server" id="TextPost">
								                                            <SkinTemplate>
    								                                            <asp:Literal id="Body" Runat="server" />
                                                                                <asp:Literal id="EditNotes" runat="server" />
                                                                                <asp:Literal id="Signature" Runat="server" />
								                                            </SkinTemplate>
								                                        </cp:TextPost>
							                                        </div>
							                                        </div>
							                                        </td></tr>
						                                        </table>
					                                        </td>
				                                        </tr>
				                                        <tr valign="bottom">
					                                        <td class="ForumPostFooterArea">								
						                                        <ul class="ForumPostStatistics" style="clear: both;">
							                                        &nbsp;
						                                        </ul>
					                                        </td>
				                                        </tr>
			                                        </table>
		                                        </div>
		                                        </li>
	                                        </ItemTemplate>
	                                        <FooterTemplate>
		                                        </ul>
	                                        </FooterTemplate>
                                        </asp:Repeater>
			                        </SkinTemplate>
			                    </cp:PostFlatPreview>
		                    </span>

		                    <div class="CommonFormArea">
			                    <asp:Label id="deleteChildrenExplanation" runat="server" />
			                    <div class="CommonFormFieldName">
				                    <cp:ResourceLabel runat="server" ResourceName="DeletePost_DeleteChildren" />
			                    </div>
			                    <div class="CommonFormField">
				                    <cp:YesNoRadioButtonList id="DeleteChildren" runat="server" RepeatDirection="Horizontal" />
				                    <asp:RequiredFieldValidator id="DeleteChildrenValidator" runat="server" Cssclass="CommonValidationWarning" ControlToValidate="DeleteChildren" />
			                    </div>
			                    <div class="CommonFormField">
                                    <CSControl:FormLabel LabelForId="SendAuthorDeleteNotification" runat="server" ResourceName="DeletePost_SendAuthorDeleteNotification" />
				                    <asp:CheckBox id="SendAuthorDeleteNotification" runat="server" />
				                </div>
		                    <div>
		                    <br />
		                    <div>
			                    <cp:ResourceLabel runat="server" ResourceName="DeletePost_ReasonText" />
			                    <div class="CommonFormFieldName">
				                    <cp:ResourceLabel runat="server" ResourceName="DeletePost_ReasonTemplate" />
			                    </div>
			                    <div class="CommonFormField">
				                    <cp:templatedropdownlist id="ReasonTemplate" runat="server" type="PostDeleteReason" autopostback="true" />
			                    </div>
			                    <div class="CommonFormFieldName">
				                    <cp:ResourceLabel runat="server" ResourceName="DeletePost_Reason" ID="Resourcelabel1" NAME="Resourcelabel1"/>
			                    </div>
			                    <div>
				                    <asp:textbox id="DeleteReason" runat="server" columns="100" TextMode="MultiLine" rows="12" />
			                    </div>
			                    <div>
				                    <asp:Button id="DeletePost" runat="server" />&nbsp;<asp:Button id="CancelDelete" runat="server" />
				                    <asp:requiredfieldvalidator id="ValidateReason" runat="server" Cssclass="CommonValidationWarning" ControlToValidate="DeleteReason" EnableClientScript="False" />
			                    </div>
		                    </div>
	                    </div>
                    </div>
			    </SkinTemplate>
			</cp:DeletePost>
		</div>
	</CP:Content>
</CP:Container>

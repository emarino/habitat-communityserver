<%@ Page language="c#" Codebehind="Default.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Moderation.ModerationHomePage" %>
<%@ Register TagPrefix="CP" TagName="ModerationForumListControl" Src = "~/ControlPanel/Moderation/ModerationForumListControl.ascx" %>
<%@ Import Namespace="CommunityServer.ControlPanel.Controls" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Discussions.Components" %>
<%@ Import Namespace="CommunityServer.Controls" %>

<CP:ControlPanelSelectedNavigation SelectedNavItem="CommonTasks" runat="server" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ModerationControlPanelMaster.ascx">
	<CP:Content id="TopLeft" runat="server">
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr >
				<td width="100%"><CP:ResourceControl runat="server" ResourceName="CP_Moderation_CurrentForum" ID="Resourcecontrol1"/></td>
				<td style="padding-right: 3px;" nowrap="nowrap"><CP:HyperLink ID="HyperLink1" Runat="server" CssClass="CommonTextButton" urlname="moderate_Home" ResourceName="CP_Moderation_Switch_ChangeForum" />
			</tr>
		</table>
	</CP:Content>
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl runat="server" resourcename="CP_Moderation_SelectForum" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonDescription">
			<cp:resourcecontrol runat="server" resourcename="CP_Moderation_SelectForum_Description" />
		</div>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Forums" />
		<cp:statusmessage id="status" runat="server" visible="false"></cp:statusmessage>
		<div class="CommonFormArea">
			<cp:ForumGroupView id="forums" runat="server" Mode="Moderator">
			    <SkinTemplate>
                    <asp:Repeater EnableViewState="false" runat="server" id="forumGroupRepeater">
	                    <ItemTemplate>
		                    <div class="CommonListArea">
		                    <h4 class="CommonListTitle"><%# DataBinder.Eval(Container.DataItem, "Name") %></h4>
		                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
			                    <thead>
			                    <tr> 
				                    <th class="CommonListHeaderLeftMost"><cp:ResourceLabel runat="server" ResourceName="ForumGroupView_Moderation_Column_Forum" /></td>
				                    <th class="CommonListHeader"><cp:ResourceLabel runat="server" ResourceName="ForumGroupView_Moderation_Column_PostsToModerate" /></td>
			                    </tr>
			                    </thead>
			                    <tbody>

			                    <cp:ForumRepeater GroupID='<%# DataBinder.Eval(Container.DataItem, "GroupID") %>' Mode="Moderator" HideForums='<%# DataBinder.Eval(Container.DataItem, "HideSections") %>' runat="server">
				                    <ItemTemplate>
					                    <tr>
						                    <td class="CommonListCellLeftMost" width="75%">
							                    <div style="font-weight: bold;"><%# DataBinder.Eval(Container.DataItem, "Name") %></div>
							                    <%# DataBinder.Eval(Container.DataItem, "Description") %><%# ForumFormatter.FormatSubForum( (Forum) Container.DataItem ) %>
						                    </td>
						                    <td class="CommonListCell" width="25%">
							                    <a href="<%# Globals.GetSiteUrls().ModerateForum( ((Forum) Container.DataItem).SectionID ) %>"><%# string.Format(ResourceManager.GetString("ForumGroupView_Moderation_Count"), DataBinder.Eval(Container.DataItem, "PostsToModerate" )) %></a>
						                    </td>
					                    </tr>
				                    </ItemTemplate>
			                    </cp:ForumRepeater>
                    		
			                    </tbody>
		                    </table>
		                    </div>
	                    </ItemTemplate>
                    </asp:Repeater>
			    </SkinTemplate>
			</cp:ForumGroupView>
		</div>
	</CP:Content>
</CP:Container>

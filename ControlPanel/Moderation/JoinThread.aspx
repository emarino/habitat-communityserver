<%@ Page language="c#" Codebehind="JoinThread.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Moderation.JoinThread" %>
<%@ Register TagPrefix="CP" TagName="ModerationForumListControl" Src = "~/ControlPanel/Moderation/ModerationForumListControl.ascx" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="CommonTasks" runat="server" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ModerationControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl runat="server" resourcename="CP_Moderation_JoinThread" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonFormArea">
			<cp:ThreadJoin runat="server">
			    <SkinTemplate>
			        <table cellSpacing="1" cellPadding="3">
                        <tr>
                          <td align="left" colspan="2">
                            <table>
                              <tr> 
                                <td align="left" colspan="2">
                                  <span class="txt3Bold"><cp:ResourceLabel runat="server" ResourceName="ThreadJoin_Select" /></span>
                                </td>
                              </tr>

                              <tr> 
                                <td align="right">
                                  <span class="txt3Bold"><cp:ResourceLabel runat="server" ResourceName="ThreadJoin_ParentID" /></span>
                                </td>
                                <td align="left">
                                  <asp:TextBox columns="20" CssClass="txt3" runat="server" id="ParentThreadID" />
                                  <asp:RequiredFieldValidator runat="server" ErrorMessage="*" Font-Bold="True" ControlToValidate="ParentThreadID" />
                                  <asp:Button id="ValidateParentThread" runat="server" />
                                </td>
                              </tr>

                              <tr> 
                                <td align="left" colspan="2" class="txt3">
                                  <asp:Checkbox runat="server" id="ParentThreadIsValid" />
                                </td>
                              </tr>

                              <tr> 
                                <td align="left" colspan="2">
                                  &nbsp;
                                </td>
                              </tr>

                              <tr>
                                <td align="left" colspan="2">
                                  <asp:HyperLink Target="_blank" CssClass="txt3Bold" runat="server" id="ChildThread" /> <span class="txt3"><cp:ResourceLabel runat="server" ResourceName="ThreadJoin_JoinRelationship" /></span> <asp:HyperLink Target="_blank" CssClass="txt3Bold" runat="server" id="ParentThread" />
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr> 
                          <td colspan="2" valign="top" align="left">
                            <span class="txt3"><asp:CheckBox Checked="true" id="SendUserEmail" runat="server" /></span>
                          </td>
                        </tr>
                        <tr>
                          <td vAlign="top" colspan="2" nowrap="nowrap" align="right"><asp:LinkButton CssClass="linkSmallBold" id="CancelMove" runat="server" CausesValidation="False" /> &nbsp; <asp:LinkButton CssClass="linkSmallBold" id="MovePost" runat="server" CausesValidation="false" /></td>
                        </tr>
                      </table>
			    </SkinTemplate>
			</cp:ThreadJoin>
		</div>
	</CP:Content>
</CP:Container>

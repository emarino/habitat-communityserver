<%@ Page language="c#" Codebehind="SplitThread.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Moderation.SplitThread" %>
<%@ Register TagPrefix="CP" TagName="ModerationForumListControl" Src = "~/ControlPanel/Moderation/ModerationForumListControl.ascx" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="CommonTasks" runat="server" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ModerationControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl runat="server" resourcename="CP_Moderation_SplitThread" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonFormArea">
			<cp:ThreadSplit runat="server">
			    <SkinTemplate>
			        <table cellspacing="0" cellpadding="0" border="0">
	                    <tr>
		                    <td class="CommonFormDescription" colspan="2">
			                    <cp:ResourceLabel runat="server" ResourceName="ThreadSplit_Description" />
		                    </td>
	                    </tr>
	                    <tr> 
		                    <td class="CommonFormFieldName">
			                    <cp:ResourceLabel runat="server" ResourceName="MovePost_Subject" />
		                    </td>
		                    <td class="CommonFormField">
			                    <asp:TextBox columns="60" runat="server" id="Subject" />
		                    </td>
	                    </tr>
	                    <tr> 
		                    <td class="CommonFormFieldName">
			                    <cp:ResourceLabel runat="server" ResourceName="MovePost_HasReplies" />
		                    </td>
		                    <td class="CommonFormField">
			                    <asp:Label runat="server" id="HasReplies" />
		                    </td>
	                    </tr>
	                    <tr> 
		                    <td class="CommonFormFieldName">
			                    <cp:ResourceLabel runat="server" ResourceName="MovePost_PostedBy" />
		                    </td>
		                    <td class="CommonFormField">
			                    <asp:Label runat="server" id="PostedBy" />
		                    </td>
	                    </tr>
	                    <tr> 
		                    <td class="CommonFormFieldName" valign="top">
			                    <cp:ResourceLabel runat="server" ResourceName="MovePost_Body"/>
		                    </td>
		                    <td class="CommonFormField">
			                    <asp:Label runat="server" id="Body" />
		                    </td>
	                    </tr>
	                    <tr> 
		                    <td class="CommonFormFieldName" valign="top">
			                    <cp:ResourceLabel runat="server" ResourceName="MovePost_MoveTo"/>
			                    <asp:RequiredFieldValidator runat="server" ErrorMessage="*" Font-Bold="True" ControlToValidate="MoveTo" id="MoveToValidator" />
		                    </td>
		                    <td class="CommonFormField">
			                    <cp:ForumListBox CssClass="txt4" id="MoveTo" Height="200" ShowButtons="true" runat="server">
			                        <SkinTemplate>
	                                        <CA:TreeView id="ForumTree" 
		                                        DragAndDropEnabled="false" 
		                                        NodeEditingEnabled="false"
		                                        KeyboardEnabled="true"
		                                        CssClass="CommonTreeFormArea" 
		                                        NodeCssClass="CommonTreeFormNode" 
		                                        SelectedNodeCssClass="CommonTreeFormNodeSelected" 
		                                        HoverNodeCssClass="CommonTreeFormNodeHover"
		                                        LineImageWidth="19" 
		                                        LineImageHeight="20"
		                                        DefaultImageWidth="16" 
		                                        DefaultImageHeight="16"
		                                        ItemSpacing="0" 
		                                        ImagesBaseUrl="~/themes/default/images/file/"
		                                        NodeLabelPadding="3"
		                                        ParentNodeImageUrl="folder.gif" 
		                                        ExpandedParentNodeImageUrl="folder_open.gif" 
		                                        LeafNodeImageUrl="folder_open.gif" 
		                                        ShowLines="true" 
		                                        LineImagesFolderUrl="~/themes/default/images/file/lines/"
		                                        EnableViewState="true"
		                                        runat="server"  />

                                        <asp:Panel id="ButtonPanel" runat="server" visible="False" HorizontalAlign="Right">
	                                        <input type="button" onclick="<%= CommunityServer.Controls.CSControlUtility.Instance().FindControl(this, "ForumTree").ClientID %>.ExpandAll()" value="Expand All" />
	                                        <input type="button" onclick="<%= CommunityServer.Controls.CSControlUtility.Instance().FindControl(this, "ForumTree").ClientID %>.CollapseAll()" value="Collapse All" />
                                        </asp:Panel>
			                        </SkinTemplate>
			                    </cp:ForumListBox>
		                    </td>
	                    </tr>
	                    <tr> 
		                    <td colspan="2" class="CommonFormField">
			                    <asp:CheckBox Checked="true" id="SendUserEmail" runat="server" />
		                    </td>
	                    </tr>
	                    <tr>
		                    <td colspan="2" class="CommonFormField">
			                    <asp:Button id="MovePost" runat="server" />
			                    <asp:Button id="CancelMove" runat="server" />
		                    </td>
	                    </tr>
                    </table>
			    </SkinTemplate>
			</cp:ThreadSplit>
		</div>
	</CP:Content>
</CP:Container>

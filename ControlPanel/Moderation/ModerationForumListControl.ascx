<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ModerationForumListControl.ascx.cs" Inherits="CommunityServer.ControlPanel.Moderation.ModerationForumListControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<script type="text/javascript">
	function onCallbackError(excString)
	{
		if (confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_CallBackWarning") %>')) alert(excString); 
		<%= Grid1.ClientID %>.Page(1); 
	}
</script>

<div id="GrayGrid" >
	<ca:grid id="Grid1" runat="server" 
		autocallbackoninsert="false" 
		autocallbackonupdate="false" 
		autocallbackondelete="false" 
		clientsideoncallbackerror="onCallbackError" 
		>
		<levels>
			<ca:gridlevel datakeyfield="SectionID">
				<columns>
					<ca:gridcolumn datafield="SectionID" visible="false" allowgrouping="false" />
					<ca:gridcolumn datafield="Name" headingtext="ResourceManager.CP_Moderation_GridCol_Name" />
					<ca:gridcolumn datafield="Description" headingtext="ResourceManager.CP_Moderation_GridCol_Description" />
					<ca:gridcolumn datafield="GroupName" headingtext="ResourceManager.CP_Moderation_GridCol_Group" />
					<ca:gridcolumn datafield="PostsToModerate" headingtext="ResourceManager.CP_Moderation_GridCol_PostsToModerate" align="center" />
					<ca:gridcolumn headingtext="ResourceManager.Actions" datacellclienttemplateid="EditTemplate" align="Center" datacellcssclass="LastDataCell" />
				</columns>
			</ca:gridlevel>
		</levels>
		<clienttemplates>
			<ca:clienttemplate id="EditTemplate">
				<a href="ModerateForum.aspx?ForumID=## DataItem.GetMember("SectionID").Text ##" class="CommonTextButton">Moderate</a>
			</ca:clienttemplate>
		</clienttemplates>
	</ca:grid>
</div>

<%@ Page language="c#" Codebehind="ForumsToModerate.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Moderation.ForumsToModerate" %>
<%@ Register TagPrefix="CP" TagName="ModerationForumListControl" Src = "~/ControlPanel/Moderation/ModerationForumListControl.ascx" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="CommonTasks" runat="server" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ModerationControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl runat="server" resourcename="CP_Moderation_Forums" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonFormArea">
			<CP:ModerationForumListControl runat="server" />
		</div>
	</CP:Content>
</CP:Container>

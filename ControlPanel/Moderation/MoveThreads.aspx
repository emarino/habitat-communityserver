<%@ Page language="c#" Codebehind="MoveThreads.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Moderation.MoveThreads" %>
<%@ Register TagPrefix="CP" TagName="ModerationForumListControl" Src = "~/ControlPanel/Moderation/ModerationForumListControl.ascx" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="CommonTasks" runat="server" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ModerationControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl runat="server" resourcename="CP_Moderation_MoveThreads" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonFormArea">
			<cp:ThreadsMove runat="server">
			    <SkinTemplate>
			        <table cellPadding="0" cellspacing="0" border="0">
	                    <tr>
		                    <td valign="top" class="CommonFormFieldName">
			                    <cp:ResourceLabel runat="server" ResourceName="MovePost_MoveTo" id="Resourcelabel4" NAME="Resourcelabel4"/>
			                    <asp:RequiredFieldValidator runat="server" ErrorMessage="*" Font-Bold="True" ControlToValidate="MoveTo" id="MoveToValidator" EnableClientScript="False" />
		                    </td>
		                    <td class="CommonFormField">
			                    <cp:ForumListBox CssClass="txt4" runat="server" id="MoveTo" ShowButtons="true" Rows="12">
			                        <SkinTemplate>
	                                        <CA:TreeView id="ForumTree" 
		                                        DragAndDropEnabled="false" 
		                                        NodeEditingEnabled="false"
		                                        KeyboardEnabled="true"
		                                        CssClass="CommonTreeFormArea" 
		                                        NodeCssClass="CommonTreeFormNode" 
		                                        SelectedNodeCssClass="CommonTreeFormNodeSelected" 
		                                        HoverNodeCssClass="CommonTreeFormNodeHover"
		                                        LineImageWidth="19" 
		                                        LineImageHeight="20"
		                                        DefaultImageWidth="16" 
		                                        DefaultImageHeight="16"
		                                        ItemSpacing="0" 
		                                        ImagesBaseUrl="~/themes/default/images/file/"
		                                        NodeLabelPadding="3"
		                                        ParentNodeImageUrl="folder.gif" 
		                                        ExpandedParentNodeImageUrl="folder_open.gif" 
		                                        LeafNodeImageUrl="folder_open.gif" 
		                                        ShowLines="true" 
		                                        LineImagesFolderUrl="~/themes/default/images/file/lines/"
		                                        EnableViewState="true"
		                                        runat="server"  />

                                        <asp:Panel id="ButtonPanel" runat="server" visible="False" HorizontalAlign="Right">
	                                        <input type="button" onclick="<%= CommunityServer.Controls.CSControlUtility.Instance().FindControl(this, "ForumTree").ClientID %>.ExpandAll()" value="Expand All" />
	                                        <input type="button" onclick="<%= CommunityServer.Controls.CSControlUtility.Instance().FindControl(this, "ForumTree").ClientID %>.CollapseAll()" value="Collapse All" />
                                        </asp:Panel>
			                        </SkinTemplate>
			                    </cp:ForumListBox>
		                    </td>
	                    </tr>
	                    <tr> 
		                    <td colspan="2" class="CommonFormField">
			                    <asp:CheckBox Checked="false" id="SendUserEmail" runat="server" />
		                    </td>
	                    </tr>
	                    <tr>
		                    <td colspan="2" class="CommonFormField">
			                    <asp:Button id="MovePost" runat="server" /> <asp:Button id="CancelMove" runat="server" />
		                    </td>
	                    </tr>
                    </table>
			    </SkinTemplate>
			</cp:ThreadsMove>
		</div>
	</CP:Content>
</CP:Container>

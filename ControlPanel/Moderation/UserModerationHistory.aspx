<%@ Page language="c#" Codebehind="UserModerationHistory.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Moderation.UserModerationHistory" %>
<%@ Register TagPrefix="CP" TagName="ModerationForumListControl" Src = "~/ControlPanel/Moderation/ModerationForumListControl.ascx" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="CommonTasks" runat="server" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ModerationControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl runat="server" resourcename="CP_Moderation_UserModerationHistory" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonFormArea">
			<cp:UserModerationHistory runat="server">
			    <SkinTemplate>
			        <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                              <!-- Display Moderation action counters -->
                              <cp:UserModerationCounters id="ModerationCounters" runat="server">
                                <SkinTemplate>
                                    <div align="left" class="countertext">
                                      <b><cp:ResourceLabel runat="server" ResourceName="UserModerationCounters_MessageAction" id="Resourcelabel4"/></b><br />
                                      <asp:HyperLink id="ApprovedPostsLink" runat="server" CssClass="counterlink" /> |
                                      <asp:HyperLink id="EditedPostsLink" runat="server" CssClass="counterlink" /> |
                                      <asp:HyperLink id="MovedPostsLink" runat="server" CssClass="counterlink" /> |
                                      <asp:HyperLink id="DeletedPostsLink" runat="server" CssClass="counterlink" /> | 
                                      <asp:HyperLink id="LockedPostLink" runat="server" CssClass="counterlink" /> |
                                      <asp:HyperLink id="UnlockedPostLink" runat="server" CssClass="counterlink" /> |
                                      <asp:HyperLink id="MergedPostLink" runat="server" CssClass="counterlink" /> |
                                      <asp:HyperLink id="SplitedPostLink" runat="server" CssClass="counterlink" /> |
                                      <asp:HyperLink id="UnApprovedPostLink" runat="server" CssClass="counterlink" /> |
                                      <asp:HyperLink id="PostIsAnnouncementLink" runat="server" CssClass="counterlink" /> |
                                      <asp:HyperLink id="PostIsNotAnnoucementLink" runat="server" CssClass="counterlink" />
                                      <br />
                                      <b><cp:ResourceLabel runat="server" ResourceName="UserModerationCounters_ForumAction" id="Resourcelabel1"/></b><br />
                                      <asp:HyperLink id="BannedLink" runat="server" CssClass="counterlink" /> |
                                      <asp:HyperLink id="UnBannedLink" runat="server" CssClass="counterlink" /> |
                                      <asp:HyperLink id="ModeratedLink" runat="server" CssClass="counterlink" /> |
                                      <asp:HyperLink id="UnmoderatedLink" runat="server" CssClass="counterlink" /> |
                                      <asp:HyperLink id="ResetPasswdLink" runat="server" CssClass="counterlink" /> |
                                      <asp:HyperLink id="ChangePasswdLink" runat="server" CssClass="counterlink" /> |
                                      <asp:HyperLink id="EditedLink" runat="server" CssClass="counterlink" />
                                    </div>
                                </SkinTemplate>
                              </cp:UserModerationCounters>
                              <br />
                            </td>       
                        </tr>
                        <tr>
                            <td>
                              <asp:Label id="CurrentCounterName" runat="server" CssClass="countertitle" />
                              <br />
                              <!-- Display moderation actions repeater -->
                                <cp:RepeaterPlusNone EnableViewState="false" runat="server" id="AuditHistoryRepeater">

	                                <HeaderTemplate>
                                      <table width="100%" class="tableBorder" cellpadding="4" cellspacing="1">
                                        <tr> 
                                            <td class="column" align="center" width="80"><cp:ResourceLabel runat="server" ResourceName="ModerationHistory_Date" id="Resourcelabel7" /></td>
                                            <td class="column" id="PostHeaderColumn" runat="server" align="center" width="150"><cp:ResourceLabel runat="server" ResourceName="ModerationHistory_Post" id="Resourcelabel1" /></td>
                                            <td class="column" align="center" width="100"><cp:ResourceLabel runat="server" ResourceName="ModerationHistory_Moderator" id="Resourcelabel3" /></td>
                                            <td class="column" align="center" width="*"><cp:ResourceLabel runat="server" ResourceName="ModerationHistory_Notes" id="Resourcelabel4" /></td>
                                        </tr>
	                                </HeaderTemplate>

	                                <ItemTemplate>
			                                  <tr valign="top">
				                                  <td class="f" align="center">
  											                    <asp:Label runat="server" id="ItemDate" />
				                                  </td>
				                                  <td class="f" id="PostColumn" runat="server" align="left">
				                                    <b><asp:HyperLink runat="server" Id="ItemPostLink" /></b>
				                                  </td>
				                                  <td class="f" align="center">
				                                    <b><asp:HyperLink runat="server" Id="ItemUsernameLink" /></b>
				                                  </td>
				                                  <td class="f" align="left">
  											                    <asp:Label runat="server" id="ItemNotes" />
				                                  </td>
			                                  </tr>            			            			
	                                </ItemTemplate>
                                	
  						                    <AlternatingItemTemplate>
			                                  <tr valign="top">
				                                  <td class="fh" align="center">
  											                    <asp:Label runat="server" id="ItemDate" />
				                                  </td>
				                                  <td class="fh" id="PostColumn" runat="server" align="left">
				                                    <b><asp:HyperLink runat="server" Id="ItemPostLink" /></b>
				                                  </td>
				                                  <td class="fh" align="center">
				                                    <b><asp:HyperLink runat="server" Id="ItemUsernameLink" /></b>
				                                  </td>
				                                  <td class="fh" align="left">
  											                    <asp:Label runat="server" id="ItemNotes" />
				                                  </td>
			                                  </tr>            			
  						                    </AlternatingItemTemplate>
                                	
							                    <NoneTemplate>
									                      <tr>
									                        <td align="left" class="fh" colspan="4">
									                          <br>
										                        <cp:ResourceLabel runat="server" ResourceName="ForumMembers_NoRecords" id="Resourcelabel2" NAME="Resourcelabel2"/>
									                        </td>
									                      </tr>
							                    </NoneTemplate>
                                	
	                                <FooterTemplate>
                                	
		                                  </table>
                                		
	                                </FooterTemplate>
                    	            				
                                </cp:RepeaterPlusNone>          
                            </td>       
                        </tr>
                        <tr>
                            <td>    
                                <!-- Display the pager -->
						                    <table cellpadding="0" cellspacing="0" width="100%">
							                    <tr>
								                    <td valign="top" align="right">
									                    <asp:Panel id="DisplayPager" Runat="server">					
										                    <div style="padding-top: 12px; padding-bottom: 12px;">
											                    <table class="tableBorder" cellpadding="0" cellspacing="0">
												                    <tr>
													                    <td>
														                    <table width="100%" cellpadding="2" cellspacing="0">
															                    <tr>
																                    <td valign="middle" class="column" nowrap="nowrap">
																	                    &nbsp;<cp:CurrentPage Cssclass="columnText" id="CurrentPage" runat="server" />
																                    </td>
																                    <td valign="middle" align="right" class="column" nowrap="nowrap">
																	                    <cp:ForumPager id="Pager" runat="server" />
																                    </td>
															                    </tr>
														                    </table>
													                    </td>
												                    </tr>
											                    </table>
										                    </div>
									                    </asp:Panel>
								                    </td> 
							                    </tr>
						                    </table>
                            </td>       
                        </tr>    
                    </table>   
                </SkinTemplate>
			</cp:UserModerationHistory>
		</div>
	</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="Tags.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Moderation.Tags" %>
<%@ Import Namespace = "CommunityServer.Components" %>
<%@ Import Namespace = "CommunityServer.ControlPanel" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="Tags" runat="server" id="ControlPanelSelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ModerationControlPanelMaster.ascx">

<CP:Content id="DescriptionRegion" runat="server"><CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_Moderation_Tags" /></CP:Content>
<CP:Content id="TaskRegion" runat="Server">

<script language = "javascript" type="text/javascript">
 
  function reloadCallback(res)
  {
    if(res)
    {
	    refresh();
	}
  }

</script>	
	
<DIV class="CommonDescription"><CP:ResourceControl id="Resourcecontrol2" runat="server" ResourceName="CP_Moderation_Tags_Description" />
</DIV>
<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Forums" />
		
<div class="CommonListArea">
	<asp:Repeater runat="Server" id="theTags">
		<HeaderTemplate>
			<table id="CommentListing" cellSpacing="0" cellPadding="0" border="0" width="100%">
				<thead>
					<tr>
						<th class="CommonListHeaderLeftMost"><CP:ResourceControl runat="server" ResourceName="CP_Moderation_Tags_List_Name" /></th>
                        <th class="CommonListHeader"><CP:ResourceControl runat="server" ResourceName="CP_Moderation_Tags_List_Posts" /></th>																																	
                        <th class="CommonListHeader"><CP:ResourceControl runat="server" ResourceName="CP_Moderation_Tags_List_Actions" /></th>
					</tr>
				</thead>
		</HeaderTemplate>
		<ItemTemplate>
			<tr>
			    <td class="CommonListCellLeftMost">
					<%# DataBinder.Eval(Container.DataItem,"Name")%>
			    </td>
			    <td class="CommonListCell">
					<cp:LiteralOrLink runat="Server" NavigateUrl='<%# "TagPosts.aspx?Tags=" + Globals.UrlEncodePathComponent(DataBinder.Eval(Container.DataItem, "Name").ToString()) %>' Text='<%# DataBinder.Eval(Container.DataItem,"TotalCount") %>' ID="PostsLink" />
			    </td>			    			    			    
			    <td  class="CommonListCell" nowrap="nowrap">
					<a href="javascript:Telligent_Modal.Open('RenameTag.aspx?Tags=<%# Globals.UrlEncodePathComponent(DataBinder.Eval(Container.DataItem, "Name").ToString()) %>', 450, 150, reloadCallback);" class="CommonTextButton"><CP:ResourceControl runat="server" resourcename="Rename" /></a>
                    <asp:LinkButton CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Name")%>' CommandName="Delete" Runat="server" ID="DeleteButton" CssClass="CommonTextButton"><CP:ResourceControl runat="server" resourcename="Delete" /></asp:LinkButton>
			    </td>
			</tr>
		</ItemTemplate>
        <AlternatingItemTemplate>
            <tr class="AltListRow">
			    <td class="CommonListCellLeftMost">
					<%# DataBinder.Eval(Container.DataItem,"Name")%>
			    </td>
			    <td class="CommonListCell">
					<cp:LiteralOrLink runat="Server" NavigateUrl='<%# "TagPosts.aspx?Tags=" + Globals.UrlEncodePathComponent(DataBinder.Eval(Container.DataItem, "Name").ToString()) %>' Text='<%# DataBinder.Eval(Container.DataItem,"TotalCount") %>' ID="Literalorlink1" />
			    </td>			    			    			    
			    <td  class="CommonListCell" nowrap="nowrap">
					<a href="javascript:Telligent_Modal.Open('RenameTag.aspx?Tags=<%# Globals.UrlEncodePathComponent(DataBinder.Eval(Container.DataItem, "Name").ToString()) %>', 450, 150, reloadCallback);" class="CommonTextButton" ><CP:ResourceControl runat="server" resourcename="Rename" /></a>
                    <asp:LinkButton CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Name")%>' CommandName="Delete" Runat="server" ID="DeleteButton" CssClass="CommonTextButton"><CP:ResourceControl runat="server" resourcename="Delete" /></asp:LinkButton>
			    </td>
			</tr>
		</AlternatingItemTemplate>			
		<FooterTemplate>
			</table>
		</FooterTemplate>
	</asp:Repeater>
</div>			

<cp:Pager id="pager" runat="server" PageSize="25" />

</CP:Content>
</CP:Container>

<%@ Page language="c#" Codebehind="TagPosts.aspx.cs" AutoEventWireup="false" Inherits="CommunityServerWeb.ControlPanel.Moderation.TagPosts" %>
<%@ Import Namespace = "CommunityServer.Components" %>
<%@ Import Namespace = "CommunityServer.ControlPanel" %>

<CP:ControlPanelSelectedNavigation SelectedNavItem="CommonTasks" runat="server" id="ControlPanelSelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ModerationControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
	<%= string.Format(CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Moderation_TagPosts_Title"), CSContext.Current.Tags[0]) %>
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">
	
<script type = "text/javascript">
// <![CDATA[
function checkAll(cb, parent)
{
	var p =$(parent);
	var pa = p.getElementsByTagName('INPUT');
	for(var i=0; i<pa.length; i++)
	{
		var e = pa[i];
		if(e.getAttribute('type') == 'checkbox' && e.name != cb.name)
		{
		    e.checked = cb.checked;
		}
	}
}

function bulkEdit(action)
{
    var result = 'action=' + action + '&postids=';
    var cb = $('master');
	var pa = $('PostListing').getElementsByTagName('INPUT');
	var b = true;
	for(var i=0; i<pa.length; i++)
	{
		var e = pa[i];
		if(e.getAttribute('type') == 'checkbox' && e.name != cb.name && e.checked)
		{
		    var delimit = ',';
		    if(b)
		    {
	        	delimit = '';
	        	b = false;    
		    }
            result += e.name.replace('cb-',delimit);
		}
	}
	
	window.location = '<%= this.GetFilteredUrl() %>&' + result;    
}
// ]]>
</script>	
	
<div class="CommonDescription">
	<CP:ResourceControl id="Resourcecontrol2" runat="server" ResourceName="CP_Moderation_TagPosts_Description" />
</div>
<div>			
<table border = "0" cellpadding="3" cellpadding="3">
    <tr>
        <td>
            <CP:ResourceControl id="FeedbackFilterLabelBulk" runat="Server" resourcename="CP_Moderation_TagPosts_BulkActions" />
        </td>
        <td align="left">
            <a href="javascript:if(confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Moderation_TagPosts_RemoveFromTag_Warning") %>')){bulkEdit('remove');}" class="CommonTextButton"><CP:ResourceControl runat="server" ResourceName="CP_Moderation_TagPosts_RemoveFromTag" /></a>
        </td>
    </tr>
</table>			
</div>

<div class="CommonListArea">
	<asp:Repeater runat="Server" id="thePosts">
		<HeaderTemplate>
			<table id="PostListing" cellSpacing="0" cellPadding="0" border="0" width="100%">
				<thead>
					<tr>
						<th class="CommonListHeaderLeftMost"><input type="checkbox" name="master" id="master" onclick="checkAll(this,'PostListing');" /></th>
						<th class="CommonListHeader"><CP:ResourceControl runat="server" ResourceName="CP_Moderation_TagPosts_List_Post" /></th>
						<th class="CommonListHeader"><CP:ResourceControl runat="server" ResourceName="CP_Moderation_TagPosts_List_Published" /></th>
                        <th class="CommonListHeader"><CP:ResourceControl runat="server" ResourceName="CP_Moderation_TagPosts_List_Actions" /></th>
					</tr>
				</thead>
		</HeaderTemplate>
		<ItemTemplate>
			<tr>
			    <td class="CommonListCellLeftMost"><input type="checkbox" name="cb-<%# DataBinder.Eval(Container.DataItem,"PostID") %>" /></td>
			    <td class="CommonListCell">
					<strong><%# DataBinder.Eval(Container.DataItem,"Title")%></strong><br /> 
					<%# Formatter.GetBodySummary(DataBinder.Eval(Container.DataItem, "FormattedBody").ToString(), 200, "", System.Drawing.Color.Black, System.Drawing.Color.Black) %>
				</td>
			    <td class="CommonListCell" nowrap="nowrap">
			        by: <strong><%# DataBinder.Eval(Container.DataItem,"UserName") %></strong><br/>
			        <%# DataBinder.Eval(Container.DataItem,"PostDate", "{0:MMM dd yyyy, hh:mm tt}") %>
			    </td>
			    <td  class="CommonListCell" nowrap="nowrap">
			        <a href="<%# DataBinder.Eval(Container.DataItem, "Url")%>" class="CommonTextButton">View</a>
                    <a onclick="return confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Moderation_TagPosts_RemoveFromTag_Warning") %>')" href='<%# GetActionUrl("remove",DataBinder.Eval(Container.DataItem, "PostID"))%>' class="CommonTextButton"><CP:ResourceControl runat="server" ResourceName="CP_Moderation_TagPosts_RemoveFromTag" /></a>			        
			   </td>
			</tr>
		</ItemTemplate>
        <AlternatingItemTemplate>
            <tr class="AltListRow">
				<td class="CommonListCellLeftMost"><input type="checkbox" name="cb-<%# DataBinder.Eval(Container.DataItem,"PostID") %>" /></td>
			    <td class="CommonListCell">
					<strong><%# DataBinder.Eval(Container.DataItem,"Title")%></strong><br /> 
					<%# Formatter.GetBodySummary(DataBinder.Eval(Container.DataItem, "FormattedBody").ToString(), 200, "", System.Drawing.Color.Black, System.Drawing.Color.Black) %>
				</td>
			    <td class="CommonListCell" nowrap="nowrap">
			        by: <strong><%# DataBinder.Eval(Container.DataItem,"UserName") %></strong><br/>
			        <%# DataBinder.Eval(Container.DataItem,"PostDate", "{0:MMM dd yyyy, hh:mm tt}") %>
			    </td>
			    <td  class="CommonListCell" nowrap="nowrap">
			        <a href="<%# DataBinder.Eval(Container.DataItem, "Url")%>" class="CommonTextButton">View</a>
                    <a onclick="return confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Moderation_TagPosts_RemoveFromTag_Warning") %>')" href='<%# GetActionUrl("remove",DataBinder.Eval(Container.DataItem, "PostID"))%>' class="CommonTextButton"><CP:ResourceControl runat="server" ResourceName="CP_Moderation_TagPosts_RemoveFromTag" /></a>			        
			   </td>
			</tr>
		</AlternatingItemTemplate>			
		<FooterTemplate>
			</table>
		</FooterTemplate>
	</asp:Repeater>
</div>
			
<cp:Pager runat="Server" id="pager" />

</CP:Content>
</CP:Container>
<%@ Page language="c#" Codebehind="SyndicationOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.SyndicationOptionsPage" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="AdvancedSyndicationSettings" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="BlogControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="Resourcecontrol1" runat="server" resourcename="CP_Blogs_SyndicationOptions_Title" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<cp:configokstatusmessage id="Status" runat="server" visible="false" />
		<div class="CommonDescription">
			<cp:resourcecontrol runat="server" resourcename="CP_Blogs_SyndicationOptions_SubTitle" />
		</div>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Blogs" />
		<div class="CommonFormArea">
		<div class="FixedWidthContainer">
		    <fieldset>
		        <legend><cp:resourcecontrol runat="server" resourcename="CP_Blogs_SyndicationOptions_Title" /></legend>
		    
			<table cellSpacing="0" cellPadding="0" border="0">
				<tr>
					<td class="CommonFormField"><asp:CheckBox id="EnableRss" Runat="Server" /></td>
					<td class="CommonFormFieldName" vAlign="top">
						<cp:helpicon runat="server" resourcename="CP_Blogs_SyndicationOptions_EnableRss_Help" />
						<CP:FormLabel runat="server" resourcename="CP_Blogs_SyndicationOptions_EnableRss" ControlToLabel="EnableRss" />
					</td>
				</tr>
				<tr>
					<td class="CommonFormField"><asp:CheckBox id="EnableAtom" Runat="Server" /></td>
					<td class="CommonFormFieldName">
						<cp:helpicon runat="server" resourcename="CP_Blogs_SyndicationOptions_EnableAtom_Help" />
						<CP:FormLabel runat="server" resourcename="CP_Blogs_SyndicationOptions_EnableAtom" ControlToLabel="EnableAtom" />
					</td>
				</tr>
				<tr>
					<td class="CommonFormField"><asp:CheckBox id="EnableTagsRss" runat="server" /></td>
					<td class="CommonFormFieldName">
						<cp:helpicon runat="server" resourcename="CP_Blogs_SyndicationOptions_EnableTagsRss_Help" />
						<CP:FormLabel runat="server" ControlToLabel="EnableRssComments" ResourceName="CP_Blogs_SyndicationOptions_EnableTagsRss" />
					</td>
				</tr>
				<tr>
					<td class="CommonFormField"><asp:CheckBox id="SyndicateExternalLinks" Runat="Server" /></td>
					<td class="CommonFormFieldName">
						<cp:helpicon runat="server" resourcename="CP_Blogs_SyndicationOptions_SyndicateExternalLinks_Help" />
						<CP:FormLabel runat="server" resourcename="CP_Blogs_SyndicationOptions_SyndicateExternalLinks" ControlToLabel="SyndicateExternalLinks" />
					</td>
				</tr>
				<tr>
					<td class="CommonFormFieldName" colspan="2">
						<asp:literal runat="server" id="CommentHeader" />
					</td>
				</tr>
				<tr>
					<td class="CommonFormField"><asp:CheckBox id="EnableRssCommentFeeds" runat="server" /></td>
					<td class="CommonFormField">
						<cp:helpicon runat="server" resourcename="CP_Blogs_SyndicationOptions_EnableRssCommentFeeds_Help" />
						<CP:FormLabel runat="server" ControlToLabel="EnableRssCommentFeeds" ResourceName="CP_Blogs_SyndicationOptions_EnableRssCommentFeeds" />
					</td>
				</tr>
				<tr>
					<td class="CommonFormField"><asp:CheckBox id="EnableRssCommentPosting" runat="server" /></td>
					<td class="CommonFormField">
						<cp:helpicon runat="server" resourcename="CP_Blogs_SyndicationOptions_EnableRssCommentPosting_Help" />
						<CP:FormLabel runat="server" ControlToLabel="EnableRssCommentPosting" ResourceName="CP_Blogs_SyndicationOptions_EnableRssCommentPosting" />
					</td>
				</tr>
			</table>
		    </fieldset>
			
			<br />
			<fieldset>
			<legend><cp:resourcecontrol runat="server" resourcename="CP_Blogs_SyndicationOptions_ExternalFeedUrl" />&nbsp;</legend>
			<p><a href="http://feedburner.com/" target="_blank">FeedBurner</a>&#153; provides both free and commercial services for hosting the RSS feed for your site. 
			The services include basic statistics as well as options for including ads in your RSS to help you generate revenue. 
			Community Server has built-in support for FeedBurner. <a href="http://www.feedburner.com/fb/a/about" target="_blank">Read more about FeedBurner�s services</a>.
            </p>
            <p  class="CommonFormField">1. When registering with FeedBurner you will be asked for the URL to your RSS feed.<br /> 
            <asp:TextBox id="PrivateRSS" runat="server" cssclass="ControlPanelTextInput" /></p>
				
				<p class="CommonFormField">
				2. Next, enter the URL that FeedBurner gave you for your RSS feed. When a URL is entered, FeedBurner RSS is enabled. When no URL is present, Community Server serves the RSS feed.
				<br />
					<asp:TextBox id="ExternalSyndicationFeed" runat="server" cssclass="ControlPanelTextInput" />
				</p>
				<p class="CommonFormField">
                    3. Finally, FeedBurner can provide provide you details about web page views, incoming and outgoing and some other statistics. To use this functionality, you will need to enable <b>StandardStats</b> on your FeedBurner account and check the box below.<br />
                    <asp:CheckBox id = "EnableTotalStats" runat="server" cssclass="ControlPanelTextInput" Text= "Do you want Community Server to enable FeedBurner StandardStats?"  /><br />
				</p>
			</fieldset>
			
		</div>
			<p class="PanelSaveButton DetailsFixedWidth">
				<cp:ResourceLinkButton id="SaveButton" runat="Server" ResourceName="Save" CssClass="CommonTextButtonBig" />
			</p>
		</div>
	</CP:Content>
</CP:Container>

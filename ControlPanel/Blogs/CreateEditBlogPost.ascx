<%@ Import Namespace = "CommunityServer.Components" %>
<%@ Control CodeBehind="CreateEditBlogPost.ascx.cs" Language="c#" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.CreateEditBlogPost" %>

<script language="C#" runat="server">

    protected override void OnLoad(EventArgs e)
    {
        // Add sub-forms here
        // this.AddSubForm(subFormControl);
        
        base.OnLoad(e);
    }

</script>

<script type="text/javascript">
// <![CDATA[
		function getAttachmentData(result)
		{
			// result = new Array(TemporaryPostAttachmentGuid, FileName, FriendlyFileName, IsRemote)
			if (result != null)
			{
				document.getElementById('<%= ServeruploadtempID.ClientID %>').value = result[0];
				document.getElementById('<%= serverfilename.ClientID %>').innerHTML = result[1];
				document.getElementById('<%= Serverfriendlyfilename.ClientID %>').value = result[1];
				document.getElementById('<%= RemoveAttachment.ClientID %>').style.visibility = 'visible';
			}
		}
	
       function removeAttachment()
       {
			document.getElementById('<%= ServeruploadtempID.ClientID %>').value = '';
			document.getElementById('<%= serverfilename.ClientID %>').innerHTML = '';
			document.getElementById('<%= Serverfriendlyfilename.ClientID %>').value = '';
			document.getElementById('<%= RemoveAttachment.ClientID %>').style.visibility = 'hidden';
       }
       
       function SelectPreview()
       {
            $('PreviewBody').innerHTML = '';
            Element.show('loading');
            CreateEditBlogPost.PreviewPost('<%= this.ClientID %>',<%= PostBody.GetContentScript() %>,previewCallBack);
            $('PreviewTitle').innerHTML = $('<%= PostSubject.ClientID %>').value;
            
            InkCheck(false);
       }
       
       function previewCallBack(res)
       {
                Element.hide('loading');
                $('PreviewBody').innerHTML = res.value;
       }
       
       function toggleInk(cb)
       {
			var inkEditor = $('<%= inkWeb.ClientID %>');
			if(cb.checked)
			{
				Element.show('<%= inkWeb.ClientID %>');
			}
			else
			{
				Element.hide('<%= inkWeb.ClientID %>');
			}
       }
       
       function showAdvancedTabs(show)
       {
			var tabbedPanes = <%=EditorTabs.ClientID%>;
			var tabCount = tabbedPanes.GetTabCount();
			for (var i = 1; i < tabCount - 1; i++)
				tabbedPanes.GetTabAtIndex(i).Disabled = !show;
				
			tabbedPanes.Refresh();
       }
       function InkHidden()
       {
			InkCheck(false);
       }
       function InkVisible()
       {
			InkCheck(true);
       }
       function InkCheck(showInk)
       {
			var InkCheckCB = $('<%= EnableInk.ClientID %>');
			var InkCheckInkEditor = $('inkWrapper');
			if(showInk && InkCheckCB.checked)
			{
				$('inkWrapper').style.position= 'static';
			}
			else
 			{
				$('inkWrapper').style.position= 'absolute';
 			}
      }
      
      function SetEditorBookmark()
      {
            <%= PostBody.GetBookmarkScript() %>;
      }
      function InsertEditorContent(content)
      {
            <%= PostBody.GetContentInsertScript("content") %>;
      }
      function UpdateEditorContent(content)
      {
            <%= PostBody.GetContentUpdateScript("content") %>;
      }

// ]]>
</script>

<div style="text-align: right">
    <asp:checkbox ID="toggleAdvanced" runat="server" onclick="showAdvancedTabs(this.checked);" /><label for="toggleAdvanced"><CP:ResourceControl ID="ResourceControl3" runat="server" ResourceName="CP_BlogContentEditor_Toggle_Advanced_Options" /></label>
</div>

<asp:ValidationSummary Runat = "server" ID = "valSummary"  HeaderText="Your post/article has the following errors" />

<TWC:TabbedPanes id="EditorTabs" runat="server"
	PanesCssClass="CommonPane"
	TabSetCssClass="CommonPaneTabSet"
	TabCssClasses="CommonPaneTab,CommonPaneTab1,CommonPaneTab2"
	TabSelectedCssClasses="CommonPaneTabSelected,CommonPaneTabSelected1,CommonPaneTabSelected2"
	TabHoverCssClasses="CommonPaneTabHover,CommonPaneTabHover1,CommonPaneTabHover2"
	>
	<TWC:TabbedPane runat="server">
		<Tab  OnClickClientFunction="InkHidden"><CP:ResourceControl resourcename="CP_BlogContentEditor_Write" runat = "Server" /></Tab>
		<Content>
			<div class="CommonFormFieldName">
				<cp:formlabel id="tt" runat="Server" controltolabel="PostSubject" resourcename="Weblog_CreateEditBlogPost_Title" />
				<asp:requiredfieldvalidator id="postSubjectValidator" runat="server" cssclass="validationWarning" controltovalidate="PostSubject" Display ="Dynamic" ErrorMessage ="A post title is required" >*</asp:requiredfieldvalidator>
			</div>
			<div class="CommonFormField">
				<asp:textbox id="PostSubject" cssclass="ControlPanelTextInputBig" maxlength="256" runat="server" />
			</div>
			<div class="CommonFormFieldName">
				<cp:formlabel id="Formlabel1" runat="Server" controltolabel="PostBody" resourcename="CP_Blogs_CreateEditBlogPost_Body" />
				<asp:requiredfieldvalidator id="postBodyValidator" display="Dynamic" runat="server" cssclass="validationWarning"
					controltovalidate="PostBody" ErrorMessage="Please add some content">*<br /></asp:requiredfieldvalidator>
			</div>
			<div class="CommonFormField">
				<CSControl:editor runat="Server" id="PostBody" width="100%" />
			</div>
			<p />
			<div class="CommonFormFieldName">
				<cp:helpicon id="Helpicon1" runat="Server" resourcename="Weblog_CreateEditBlogPost_Categories_Sub" />
				<cp:formlabel id="Formlabel3" runat="Server" resourcename="Weblog_CreateEditBlogPost_Categories" />
			</div>
			<div class="CommonFormField">
				<cp:TagEditor runat="server" id="Tags">
				    <SkinTemplate>
                        <table cellpadding="0" cellspacing="0" border="0">
                        <tr valign="top">
                        <td>
	                        <asp:TextBox Columns="70" ID="Tags" runat="server" />
                        </td>
                        <td>
	                        <CP:Modal modaltype="Button" width="400" height="300" runat="Server" id="SelectTags" ResourceName="TagEditor_SelectTags" />
                        </td>
                        </tr>
                        </table>
				    </SkinTemplate>
				</cp:TagEditor>
			</div>
			<p id="DefaultTagsContainer" runat="server">
				<cp:resourcecontrol runat="server" resourcename="CP_Blogs_CreateEditBlogPost_DefaultTagsNotice" /><br />
				<asp:literal id="DefaultTags" runat="server" />
			</p>
			<p />
		</Content>
	</TWC:TabbedPane>
	<TWC:TabbedPane runat="server">
		<Tab Enabled="false" OnClickClientFunction="InkHidden"><CP:ResourceControl resourcename="CP_BlogContentEditor_Post_Attachments" runat = "Server" /></Tab>
		<Content>
			<div class="CommonFormFieldName">
				<cp:formlabel id="FileOrLinkMessage" runat="Server" resourcename="CP_Blogs_CreateEditBlogPost_File" />
			</div>
			<div class="CommonFormField">
				<asp:label id="serverfilename" runat="server" />
				<input type="hidden" id="ServeruploadtempID" runat="server" name="ServeruploadtempID">
				<input type="hidden" id="Serverfriendlyfilename" runat="server" name="ServeruploadtempID">
				<CP:Modal modaltype="Link" CssClass="CommonTextButton" url="Attachment.aspx" width="640" height="180"
					runat="Server" callback="getAttachmentData" resourcename="CP_Blogs_CreateEditBlogPost_AddUpdate" ResourceFile="ControlPanelResources.xml" id="Modal2" />
				<a href="#" onclick="removeAttachment(); return false;" class="CommonTextButton" runat="server"
					id="RemoveAttachment"><CP:ResourceControl id = "ResourceControl1" runat="server" ResourceName="CP_Blogs_CreateEditBlogPost_RemoveAttachement"/></a>
			</div>		
		</Content>
	</TWC:TabbedPane>
	<TWC:TabbedPane runat="server">
		<Tab Enabled="false"  OnClickClientFunction="InkHidden"><CP:ResourceControl resourcename="CP_BlogContentEditor_Video" runat = "Server" /></Tab>
		<Content>
			<div class="CommonFormArea" style="width: 650px">
				<div class="CommonFormFieldName">
					<label for "<%= VideoUrl.ClientID %>"><cp:ResourceControl runat="server" ResourceName="CreateEditPost_VideoUrl" ID="Resourcecontrol7"/></label>
				</div>
				<div class="CommonFormField">
					<asp:textbox id="VideoUrl" runat="server" autocomplete="off" style="width:80%" />
				</div>
				
				<div class="CommonFormFieldName">
					<label for "<%= VideoImageUrl.ClientID %>"><cp:ResourceControl runat="server" ResourceName="CreateEditPost_PreviewImageUrl" ID="Resourcecontrol8"/></label>
				</div>
				<div class="CommonFormField">
					<asp:textbox id="VideoImageUrl" runat="server" autocomplete="off" style="width:80%" />
				</div>
				
				<div class="CommonFormFieldName">
					<label for "<%= VideoDuration.ClientID %>"><cp:ResourceControl runat="server" ResourceName="CreateEditPost_VideoDuration" ID="Resourcecontrol9"/></label>
				</div>
				<div class="CommonFormField">
					<asp:textbox id="VideoDuration" runat="server" autocomplete="off" style="width:80%" />
				</div>
				
				<div class="CommonFormFieldName">
					<label for "<%= VideoWidth.ClientID %>"><cp:ResourceControl runat="server" ResourceName="CreateEditPost_VideoWidth" ID="Resourcecontrol4"/></label>
				</div>
				<div class="CommonFormField">
					<asp:textbox id="VideoWidth" runat="server" autocomplete="off" style="width:80%" />
					<asp:RegularExpressionValidator id="VideoWidthValidator" ControlToValidate="VideoWidth" ValidationExpression="^[0-9]*$" runat="server">*</asp:RegularExpressionValidator>
				</div>
				
				<div class="CommonFormFieldName">
					<label for "<%= VideoDuration.ClientID %>"><cp:ResourceControl runat="server" ResourceName="CreateEditPost_VideoHeight" ID="Resourcecontrol5"/></label>
				</div>
				<div class="CommonFormField">
					<asp:textbox id="VideoHeight" runat="server" autocomplete="off" style="width:80%" />
					<asp:RegularExpressionValidator id="VideoHeightValidator" ControlToValidate="VideoHeight" ValidationExpression="^[0-9]*$" runat="server">*</asp:RegularExpressionValidator>
				</div>
			</div>		
		</Content>
	</TWC:TabbedPane>
	<TWC:TabbedPane runat="server">
		<Tab Enabled="false"  OnClickClientFunction="InkVisible"><CP:ResourceControl resourcename="CP_BlogContentEditor_WhiteBoard" runat = "Server" /></Tab>
		<Content>
			<p>
				<asp:checkbox id="EnableInk" resourcename="CP_Blogs_CreateEditBlogPost_EnableInk" runat="server" onclick="toggleInk(this)" checked="True" />
			</p>
		</Content>
	</TWC:TabbedPane>
	<TWC:TabbedPane runat="server">
		<Tab Enabled="false"  OnClickClientFunction="InkHidden"><CP:ResourceControl resourcename="CP_BlogContentEditor_Options" runat = "Server" /></Tab>
		<Content>
			<div class="CommonFormFieldName">
				<cp:helpicon id="Helpicon8" runat="Server" resourcename="Weblog_CreateEditBlogPost_Excerpt_Sub" />
				<cp:formlabel id="Formlabel2" runat="Server" controltolabel="postExcerpt" resourcename="Weblog_CreateEditBlogPost_Excerpt" />
			</div>
			<div class="CommonFormField">
				<asp:textbox id="postExcerpt" columns="75" cssclass="ControlPanelTextInput" maxlength="500" runat="server"
					textmode="MultiLine" rows="5" />
			</div>
			<p />
			<div class="CommonFormFieldName">
				<cp:helpicon id="Helpicon2" runat="Server" resourcename="Weblog_CreateEditBlogPost_Name_Sub" />
				<cp:formlabel id="Formlabel4" runat="Server" controltolabel="postName" resourcename="Weblog_CreateEditBlogPost_Name" />
				<asp:regularexpressionvalidator id="vRegexPostName" controltovalidate="postName" runat="server" display="Dynamic">*</asp:RegularExpressionValidator>
			</div>
			<div class="CommonFormField">
				<asp:textbox id="postName" cssclass="ControlPanelTextInput" maxlength="256" runat="server" />
			</div>
			<p />
			<div class="CommonFormFieldName">
				<cp:helpicon id="Helpicon3" runat="Server" resourcename="Weblog_CreateEditBlogPost_PostDate_Sub" />
				<cp:formlabel id="Formlabel5" runat="Server" controltolabel="postDate" resourcename="Weblog_CreateEditBlogPost_PostDate" />
			</div>
			<div class="CommonFormField">
				<TWC:DateTimeSelector runat="server" id="DatePicker" DateTimeFormat="MMMM d yyyy" ShowCalendarPopup="true" />
				<CP:ResourceControl id = "Resourcecontrol2" runat="server" ResourceName="CP_Blogs_CreateEditBlogPost_At"/>
				<cp:dropdownrange start="1" factor="1" max="12" runat="Server" id="HourPicker" />
				:
				<cp:dropdownrange start="0" factor="1" max="59" runat="Server" id="MinutePicker" format="00" />
				:
				<asp:dropdownlist id="AMPM" runat="server">
					<asp:listitem value="AM">AM</asp:listitem>
					<asp:listitem value="PM">PM</asp:listitem>
				</asp:dropdownlist>
			</div>
		</Content>
	</TWC:TabbedPane>
	<TWC:TabbedPane runat="server">
		<Tab Enabled="false"  OnClickClientFunction="InkHidden"><CP:ResourceControl resourcename="CP_BlogContentEditor_Advanced_Options" runat = "Server" /></Tab>
		<Content>
			<table cellspacing="0" cellpadding="0" width="100%" border="0" id="AdvancedProperties"
				runat="server">

				<tr>
					<td class="CommonFormFieldName">
						<cp:helpicon id="Helpicon5" runat="Server" resourcename="Weblog_CreateEditBlogPost_AllowReplies_Sub" />
						<cp:formlabel id="Formlabel7" runat="Server" controltolabel="ynEnableReplies" resourcename="Weblog_CreateEditBlogPost_AllowReplies" />
					</td>
					<td class="CommonFormField">
						<cp:yesnoradiobuttonlist id="ynEnableReplies" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput1" />
					</td>
				</tr>
				<tr>
					<td class="CommonFormFieldName">
						<cp:helpicon id="Helpicon7" runat="Server" resourcename="Weblog_CreateEditBlogPost_ModerateComments_Sub" />
						<cp:formlabel id="Formlabel9" runat="Server" controltolabel="ModerationDDL" resourcename="Weblog_CreateEditBlogPost_ModerateComments" />
					</td>
					<td class="CommonFormField">
						<cp:commentmoderationdropdownlist runat="Server" id="ModerationDDL" /></td>
				</tr>
				<tr>
					<td class="CommonFormFieldName">
						<cp:helpicon id="Helpicon6" runat="Server" resourcename="CP_Blogs_CreateEditBlogPost_FeedbackNotify_Help" />
						<cp:formlabel id="Formlabel8" runat="Server" controltolabel="NotificationType" resourcename="CP_Blogs_CreateEditBlogPost_FeedbackNotify"
							resourcefile="ControlPanelResources.xml" />
					</td>
					<td class="CommonFormField">
						<cp:feedbacknotificationdropdownlist id="NotificationType" runat="server" cssclass="ControlPanelTextInput1" />
					</td>
				</tr>
				<tr runat="server" id="ynNotifyAllOwners_Row">
					<td class="CommonFormFieldName">
						<cp:helpicon id="Helpicon15" runat="Server" resourcename="CP_Blogs_CreateEditBlogPost_NotifyAllOwners_Help" />
						<cp:formlabel id="Formlabel16" runat="Server" controltolabel="ynNotifyAllOwners" resourcename="CP_Blogs_CreateEditBlogPost_NotifyAllOwners"
							resourcefile="ControlPanelResources.xml" />
					</td>
					<td class="CommonFormField">
						<cp:yesnoradiobuttonlist id="ynNotifyAllOwners" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput1" />
					</td>
				</tr>
				<tr>
					<td class="CommonFormFieldName">
						<cp:helpicon id="Helpicon10" runat="Server" resourcename="Weblog_CreateEditBlogPost_EnableTrackBacks_Sub" />
						<cp:formlabel id="Formlabel11" runat="Server" controltolabel="ynEnableTrackbacks" resourcename="Weblog_CreateEditBlogPost_EnableTrackBacks" />
					</td>
					<td class="CommonFormField">
						<cp:yesnoradiobuttonlist id="ynEnableTrackbacks" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput1" />
					</td>
				</tr>
				<tr>
					<td class="CommonFormFieldName">
						<cp:helpicon id="Helpicon9" runat="Server" resourcename="Weblog_CreateEditBlogPost_EnableRatings_Sub" />
						<cp:formlabel id="Formlabel10" runat="Server" controltolabel="ynEnableRatings" resourcename="Weblog_CreateEditBlogPost_EnableRatings" />
					</td>
					<td class="CommonFormField">
						<cp:yesnoradiobuttonlist id="ynEnableRatings" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput1" />
					</td>
				</tr>
				<tr runat="server" id="ynAggregatePost_Row">
					<td class="CommonFormFieldName">
						<cp:helpicon id="Helpicon11" runat="Server" resourcename="Weblog_CreateEditBlogPost_AggregatePost_Sub" />
						<cp:formlabel id="Formlabel12" runat="Server" controltolabel="ynAggregatePost" resourcename="Weblog_CreateEditBlogPost_AggregatePost" />
					</td>
					<td class="CommonFormField">
						<cp:yesnoradiobuttonlist id="ynAggregatePost" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput1" />
					</td>
				</tr>
				<tr runat="server" id="ynCommunity_Row">
					<td class="CommonFormFieldName">
						<cp:helpicon id="Helpicon12" runat="Server" resourcename="Weblog_CreateEditBlogPost_CommunityParticipation_Sub" />
						<cp:formlabel id="Formlabel13" runat="Server" controltolabel="ynCommunity" resourcename="Weblog_CreateEditBlogPost_CommunityParticipation" />
					</td>
					<td class="CommonFormField">
						<cp:yesnoradiobuttonlist id="ynCommunity" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput1" />
					</td>
				</tr>
				<tr runat="server" id="ynSyndicateExcerpt_Row">
					<td class="CommonFormFieldName">
						<cp:helpicon id="Helpicon13" runat="Server" resourcename="Weblog_CreateEditBlogPost_SyndicateExcerpt_Sub" />
						<cp:formlabel id="Formlabel14" runat="Server" controltolabel="ynSyndicateExcerpt" resourcename="Weblog_CreateEditBlogPost_SyndicateExcerpt" />
					</td>
					<td class="CommonFormField">
						<cp:yesnoradiobuttonlist id="ynSyndicateExcerpt" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput1" />
					</td>
				</tr>
				<tr runat="server" id="ynPersonalHomePage_Row" visible="false">
					<td class="CommonFormFieldName">
						<cp:helpicon id="Helpicon14" runat="Server" resourcename="Weblog_CreateEditBlogPost_HomePage_Sub" />
						<cp:formlabel id="Formlabel15" runat="Server" controltolabel="ynPersonalHomePage" resourcename="Weblog_CreateEditBlogPost_HomePage" />
					</td>
					<td class="CommonFormField">
						<cp:yesnoradiobuttonlist id="ynPersonalHomePage" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput1" />
					</td>
				</tr>
				<tr id="CrossPostingRow" runat="server">
					<td class="CommonFormFieldName">
						<cp:helpicon id="Helpicon16" runat="Server" resourcename="CP_Blogs_CreateEditBlogPost_EnableCrossPosting_Help" />
						<cp:formlabel id="Formlabel17" runat="Server" controltolabel="ynEnableCrossPosting" resourcename="CP_Blogs_CreateEditBlogPost_EnableCrossPosting" />
					</td>
					<td class="CommonFormField">
						<cp:yesnoradiobuttonlist id="ynEnableCrossPosting" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput1" />
					</td>
				</tr>
			</table>
		</Content>
	</TWC:TabbedPane>
	<TWC:TabbedPane runat="server">
		<Tab OnClickClientFunction="SelectPreview" ><CP:ResourceControl resourcename="CP_BlogContentEditor_Preview" runat = "Server" /></Tab>
		<Content>
			<h3 class="CommonSubTitle" id="PreviewTitle"></h3>
			<span id="loading" style="display:none">Loading <img src = "<%=CommunityServer.Components.SiteUrls.Instance().LoadingImageUrl%>"/></span>
			<span id="PreviewBody"></span>
		</Content>
	</TWC:TabbedPane>
</TWC:TabbedPanes>

<div id="inkWrapper" class="CommonPane" style="position:absolute;left:-5000px;">
<cp:inkwebcontrol runat="Server" id="inkWeb">
    <SkinTemplate>
        <asp:Literal runat="server" id="objecttag" />
        <input type="hidden" runat="server" id="HiddenInk" />
    </SkinTemplate>
</cp:inkwebcontrol>
</div>
<div id="ErrorPanel" class="CommonMessageError" runat="server" visible="False">
	<cp:resourcecontrol runat="Server" id="Message" />
</div>
<div id="Post" runat="server" visible="true">
</div>
<p class="PanelSaveButton">
<CP:resourcelinkbutton id="SaveEditButton" runat="server" resourcename="CP_Blogs_CreateEditBlogPost_SaveContinue" CssClass="CommonTextButtonBig" />
<CP:resourcelinkbutton id="SaveButton" runat="server" resourcename="CP_Blogs_CreateEditBlogPost_Save" CssClass="CommonTextButtonBig" />
<strong><CP:resourcelinkbutton id="PostButton" runat="server" resourcename="CP_Blogs_CreateEditBlogPost_Post" CssClass="CommonTextButtonBig" /></strong>
</p>

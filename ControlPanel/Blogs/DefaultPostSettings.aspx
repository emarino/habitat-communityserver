<%@ Page language="c#" Codebehind="DefaultPostSettings.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.DefaultPostSettings" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="DefaultPostSettings" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="BlogControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol runat="server" resourcename="CP_Blogs_DefaultPostSettings_Title" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<cp:configokstatusmessage id="Status" runat="server" visible="false" />
		<DIV class="CommonDescription">
			<cp:resourcecontrol runat="server" resourcename="CP_Blogs_DefaultPostSettings_SubTitle" />
		</DIV>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Blogs" />

		<table cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_Blogs_DefaultPostSettings_AllowReplies_Help" />
				<CP:FormLabel ID="FormLabel1" runat="Server" ControlToLabel="ynEnableReplies" ResourceName="CP_Blogs_DefaultPostSettings_AllowReplies" />
			</td>
			<td class="CommonFormField">
				<cp:YesNoRadioButtonList id="ynEnableReplies" runat="server" RepeatColumns="2" CssClass="ControlPanelTextInput1" />
			</td>
		</tr>
		<TR>
			<TD class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_Blogs_DefaultPostSettings_ModerateComments_Help" />
				<CP:FormLabel ID="FormLabel2" runat="Server" ControlToLabel="ModerationDDL" ResourceName="CP_Blogs_DefaultPostSettings_ModerateComments" />
			</TD>
			<TD class="CommonFormField">
				<cp:CommentModerationDropDownList runat="Server" id="ModerationDDL" />
			</TD>
		</TR>
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_Blogs_DefaultPostSettings_FeedbackNotify_Help" />
				<cp:formlabel ID="Formlabel3" runat="Server" controltolabel="NotificationType" resourcename="CP_Blogs_DefaultPostSettings_FeedbackNotify" />
			</td>
			<td class="CommonFormField">
				<cp:feedbacknotificationdropdownlist id="NotificationType" runat="server" cssclass="ControlPanelTextInput1" />
			</td>
		</tr>
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon id="Helpicon15" runat="Server" resourcename="CP_Blogs_CreateEditBlogPost_NotifyAllOwners_Help" />
				<cp:formlabel id="Formlabel16" runat="Server" controltolabel="ynNotifyAllOwners" resourcename="CP_Blogs_CreateEditBlogPost_NotifyAllOwners"
					resourcefile="ControlPanelResources.xml" />
			</td>
			<td class="CommonFormField">
				<cp:yesnoradiobuttonlist id="ynNotifyAllOwners" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput1" />
			</td>
		</tr>
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_Blogs_DefaultPostSettings_EnableRatings_Help" />
				<CP:FormLabel id="Formlabel10" runat="Server" ControlToLabel="ynEnableRatings" ResourceName="CP_Blogs_DefaultPostSettings_EnableRatings" />
			</td>
			<td class="CommonFormField">
				<cp:YesNoRadioButtonList id="ynEnableRatings" runat="server" RepeatColumns="2" CssClass="ControlPanelTextInput1" />
			</td>
		</tr>
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_Blogs_DefaultPostSettings_EnableTrackBacks_Help" />
				<CP:FormLabel ID="FormLabel4" runat="Server" ControlToLabel="ynEnableTrackbacks" ResourceName="CP_Blogs_DefaultPostSettings_EnableTrackBacks" />
			</td>
			<td class="CommonFormField">
				<cp:YesNoRadioButtonList id="ynEnableTrackbacks" runat="server" RepeatColumns="2" CssClass="ControlPanelTextInput1" />
			</td>
		</tr>
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_Blogs_DefaultPostSettings_AggregatePost_Help" />
				<CP:FormLabel ID="FormLabel5" runat="Server" ControlToLabel="ynAggregatePost" ResourceName="CP_Blogs_DefaultPostSettings_AggregatePost" />
			</td>
			<td class="CommonFormField">
				<cp:YesNoRadioButtonList id="ynAggregatePost" runat="server" RepeatColumns="2" CssClass="ControlPanelTextInput1" />
			</td>
		</tr>
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_Blogs_DefaultPostSettings_CommunityParticipation_Help" />
				<CP:FormLabel ID="FormLabel6" runat="Server" ControlToLabel="ynCommunity" ResourceName="CP_Blogs_DefaultPostSettings_CommunityParticipation" />
			</td>
			<td class="CommonFormField">
				<cp:YesNoRadioButtonList id="ynCommunity" runat="server" RepeatColumns="2" CssClass="ControlPanelTextInput1" />
			</td>
		</tr>
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_Blogs_DefaultPostSettings_SyndicateExcerpt_Help" />
				<CP:FormLabel ID="FormLabel7" runat="Server" ControlToLabel="ynSyndicateExcerpt" ResourceName="CP_Blogs_DefaultPostSettings_SyndicateExcerpt" />
			</td>
			<td class="CommonFormField">
				<cp:YesNoRadioButtonList id="ynSyndicateExcerpt" runat="server" RepeatColumns="2" CssClass="ControlPanelTextInput1" />
			</td>
		</tr>
		<tr id="CrossPostingRow" runat="server">
			<td class="CommonFormFieldName">
				<cp:helpicon id="Helpicon16" runat="Server" resourcename="CP_Blogs_CreateEditBlogPost_EnableCrossPosting_Help" />
				<cp:formlabel id="Formlabel17" runat="Server" controltolabel="ynEnableCrossPostingDefault" resourcename="CP_Blogs_CreateEditBlogPost_EnableCrossPosting" />
			</td>
			<td class="CommonFormField">
				<cp:yesnoradiobuttonlist id="ynEnableCrossPostingDefault" runat="server" repeatcolumns="2" cssclass="ControlPanelTextInput1" />
			</td>
		</tr>
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_Blogs_DefaultPostSettings_AutoName_Help" />
				<CP:FormLabel runat="Server" ControlToLabel="ynAutoName" ResourceName="CP_Blogs_DefaultPostSettings_AutoName" />
			</td>
			<td class="CommonFormField">
				<cp:YesNoRadioButtonList id="ynAutoName" runat="server" RepeatColumns="2" CssClass="ControlPanelTextInput1" />
			</td>
		</tr>					
		</table>

		<p class="PanelSaveButton DetailsFixedWidth">
			<cp:ResourceLinkButton id="SaveButton" runat="Server" CssClass="CommonTextButtonBig" ResourceName="Save" />
		</p>
	</CP:Content>
</CP:Container>

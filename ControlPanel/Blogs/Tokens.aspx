<%@ Page language="c#" Codebehind="Tokens.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.TokensPage" %>
<%@ Import Namespace = "CommunityServer.Components" %>
<%@ Import Namespace = "CommunityServer.Blogs.Components" %>
<%@ Import Namespace = "CommunityServer.ControlPanel" %>
<cp:controlpanelselectednavigation selectednavitem="TextParts" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="BlogControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
	<cp:resourcelabel id="title" runat="server" resourcename="CP_Blog_TextParts_Title"></cp:resourcelabel>
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">

<script>
function refreshPage(res)
{
    refresh();
}
</script>

<DIV class="CommonDescription">
	<CP:ResourceControl runat="server" ResourceName="CP_Blog_TextParts_SubTitle" />
</DIV>
<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Blogs" />

<p class="PanelSaveButton">
	<cp:Modallink id="NewTextPart" runat="Server" resourcename="CP_Blog_TextParts_NewTextPart" height="300"
		width="400" url="TokenForm.aspx" cssclass="CommonTextButtonBig" CallBack="refreshCallback" />
</p>			
			
<div class="CommonListArea">
<asp:Repeater runat = "Server" id = "theTokens">
<HeaderTemplate>
	<table id="CommentListing" cellSpacing="0" cellPadding="0" border="0" width="100%">
    <thead>
        <tr>
        <th class="CommonListHeaderLeftMost" ><cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Name" ID="Resourcecontrol3" NAME="Resourcecontrol3"/></th>
        <th class="CommonListHeader"><cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Link" ID="Resourcecontrol4" NAME="Resourcecontrol4"/></th>
        <th class="CommonListHeader"><cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Text" ID="Resourcecontrol5" NAME="Resourcecontrol4"/></th>
        <th class="CommonListHeader"><cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Actions" ID="Resourcecontrol7" NAME="Resourcecontrol4"/></th>
        </tr>
    </thead>
</HeaderTemplate>
<ItemTemplate>
	<tr>
	<td class="CommonListCellLeftMost"><%# DataBinder.Eval(Container.DataItem,"Token")%></td>
	<td class="CommonListCell"><a href="<%# DataBinder.Eval(Container.DataItem,"Link")%>"><%# DataBinder.Eval(Container.DataItem,"Link")%></a></td>
	<td class="CommonListCell"><%# DataBinder.Eval(Container.DataItem,"Text") %></td>
	<td  class="CommonListCell" nowrap="nowrap">
	<a href="javascript:Telligent_Modal.Open('TokenForm.aspx?TokenID=<%# DataBinder.Eval(Container.DataItem,"TokenID") %>', 400, 300, refreshCallback);" class="CommonTextButton"><CP:ResourceControl runat="Server" resourcename="CP_Blog_GridCol_Edit" /></a>
	<CP:ResourceLinkButton CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TokenID")%>' CommandName="Delete" Runat="server" ID = "DeleteButton" ResourceName="CP_Blog_GridCol_Delete" CssClass="CommonTextButton" /></td>
	</tr>
</ItemTemplate>
<AlternatingItemTemplate>
	<tr class="AltListRow">
	<td class="CommonListCellLeftMost"><%# DataBinder.Eval(Container.DataItem,"Token")%></td>
	<td class="CommonListCell"><a href="<%# DataBinder.Eval(Container.DataItem,"Link")%>"><%# DataBinder.Eval(Container.DataItem,"Link")%></a></td>
	<td class="CommonListCell"><%# DataBinder.Eval(Container.DataItem,"Text") %></td>
	<td  class="CommonListCell" nowrap="nowrap">
	<a href="javascript:Telligent_Modal.Open('TokenForm.aspx?TokenID=<%# DataBinder.Eval(Container.DataItem,"TokenID") %>', 400, 300, refreshCallback);" class="CommonTextButton"><CP:ResourceControl runat="Server" resourcename="CP_Blog_GridCol_Edit" /></a>
	<CP:ResourceLinkButton CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TokenID")%>' CommandName="Delete" Runat="server" ID = "DeleteButton" ResourceName="CP_Blog_GridCol_Delete" CssClass="CommonTextButton" /></td>
	</tr>
</AlternatingItemTemplate>

<FooterTemplate>
	</table>
</FooterTemplate>
</asp:Repeater>

</div>
</CP:Content>
</CP:Container>

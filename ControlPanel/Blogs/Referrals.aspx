
<%@ Page CodeBehind="Referrals.aspx.cs" Language="c#" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.Referrals" %>
<%@ Register TagPrefix="CP" TagName = "ReferalsList" Src = "~/ControlPanel/Blogs/ReferralsListControl.ascx" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="Posts" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="BlogControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server"><cp:resourcelabel id="Resourcelabel2" runat="server" resourcename="CP_Blogs_Referrals_Title"></cp:resourcelabel></CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<div class="CommonDescription">
			<cp:resourcelabel id="Resourcelabel1" runat="server" resourcename="CP_Blogs_Referrals_SubTitle"></cp:resourcelabel>
		</div>
		<CP:ReferalsList id="ReferalsList1" runat="Server"></CP:ReferalsList>
	</CP:Content>
</CP:Container>

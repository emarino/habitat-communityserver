<%@ Page language="c#" EnableViewState="False" Codebehind="switch.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.BlogSwitchPage" %>
<%@ Import Namespace = "CommunityServer.Components" %>
<%@ Import Namespace = "CommunityServer.Blogs.Components" %>
<%@ Import Namespace = "CommunityServer.ControlPanel" %>
<CP:ControlPanelSelectedNavigation id="SelectedNavigation1" runat="server" SelectedTab="Blogs" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelMaster.ascx">
	<CP:Content id="PanelDescription" runat="server">
		<cp:resourcecontrol id="Resourcecontrol1" runat="server" resourcename="CP_Blog_TitleDefault"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="Resourcecontrol2" runat="server" resourcename="CP_Blog_Switch_Title"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="PanelNavigation" runat="server">
		<CP:ControlPanelNavigationSidebar id="Controlpanelnavigationsidebar1" runat="server"></CP:ControlPanelNavigationSidebar>
	</CP:Content>
	<CP:Content id="TopLeft" runat="server">
		<TABLE cellSpacing="0" cellPadding="0" border="0"  width="100%">
			<TR>
				<TD  width="100%">
					<cp:resourcecontrol id="Resourcecontrol3" runat="server" resourcename="CP_Blog_Switch_CurrentBlog"></cp:resourcecontrol>&nbsp;</TD>
				<TD style="padding-right: 3px;" nowrap="nowrap">
					<cp:hyperlink id="Hyperlink1" runat="server" resourcename="CP_Blog_Switch_ChangeBlog" cssclass="CommonTextButton"
						urlname="blog_ControlPanel_Switch"></cp:hyperlink></TD>
			</TR>
		</TABLE>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<cp:resourcecontrol id="Resourcecontrol4" runat="server" resourcename="CP_Blog_Switch_Description"></cp:resourcecontrol></DIV>
		<cp:statusmessage id="Status" runat="server" visible="false"></cp:statusmessage>
		
<div class="CommonListArea">
<asp:Repeater runat = "Server" id = "theBlogs">
<HeaderTemplate>
<table id="BlogLists" cellSpacing="0" cellPadding="0" border="0" width="100%">
<thead>
<tr>
    <th class="CommonListHeaderLeftMost"><cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Name" ID="Resourcecontrol6" NAME="Resourcecontrol4"/></th>
    <th class="CommonListHeader"><cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Owners" ID="Resourcecontrol7" NAME="Resourcecontrol4"/></th>
    <th class="CommonListHeader"><cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Group" ID="Resourcecontrol8" NAME="Resourcecontrol4"/></th>
    <th class="CommonListHeader" ><cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_AppKey" ID="Resourcecontrol5" NAME="Resourcecontrol3"/></th>
    <th class="CommonListHeader"><cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Enabled" ID="Resourcecontrol9" NAME="Resourcecontrol4"/></th>    
</tr>
</thead>
</HeaderTemplate>
<ItemTemplate>
<tr>
    <td class="CommonListCellLeftMost"><a href="<%= BlogUrls.Instance().ControlPanel_Blogs %>?sectionid=<%# DataBinder.Eval(Container.DataItem,"SectionID")%>"><%# Formatter.RemoveHtml(DataBinder.Eval(Container.DataItem,"Name").ToString(), 50)%></a></td>
    <td class="CommonListCell"><%# FormatOwners(DataBinder.Eval(Container.DataItem,"Owners")) %></td>
    <td class="CommonListCell" nowrap="nowrap"><%# DataBinder.Eval(Container.DataItem,"GroupName") %></td>
    <td class="CommonListCell"><%# Formatter.RemoveHtml(DataBinder.Eval(Container.DataItem,"ApplicationKey").ToString(), 50) %></td>
	<td class="CommonListCell" align="center"><img src="<%=SiteUrls.Instance().Locations["ControlPanel"]%>images/<%# DataBinder.Eval(Container.DataItem,"IsActive").ToString() %>.gif" width="16" height="15" /></td>    
</tr>
</ItemTemplate>
<AlternatingItemTemplate>
<tr class="AltListRow">
    <td class="CommonListCellLeftMost"><a href="<%= BlogUrls.Instance().ControlPanel_Blogs %>?sectionid=<%# DataBinder.Eval(Container.DataItem,"SectionID")%>"><%# Formatter.RemoveHtml(DataBinder.Eval(Container.DataItem,"Name").ToString(), 50)%></a></td>
    <td class="CommonListCell"><%# FormatOwners(DataBinder.Eval(Container.DataItem,"Owners")) %></td>
    <td class="CommonListCell" nowrap="nowrap"><%# DataBinder.Eval(Container.DataItem,"GroupName") %></td>
    <td class="CommonListCell"><%# Formatter.RemoveHtml(DataBinder.Eval(Container.DataItem,"ApplicationKey").ToString(), 50) %></td>
	<td class="CommonListCell" align="center"><img src="<%=SiteUrls.Instance().Locations["ControlPanel"]%>images/<%# DataBinder.Eval(Container.DataItem,"IsActive").ToString() %>.gif" width="16" height="15" /></td>    
</tr>
</AlternatingItemTemplate>
<FooterTemplate>
</table>
</FooterTemplate>
</asp:Repeater>
</div>		
	</CP:Content>
</CP:Container>

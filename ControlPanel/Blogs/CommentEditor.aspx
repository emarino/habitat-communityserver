<%@ Page language="c#" Codebehind="CommentEditor.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.CommentEditor" %>

<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelModalMaster.ascx" IsModal="true">
	<CP:Content id="bcr" runat="server">
		<div class="CommonContentArea">
			<div class="CommonContent">
			
						<div class="CommonFormFieldName">
							<CP:ResourceControl resourcename="FeedbackEditor_Title" runat="Server" id="FeedbackEditor_Title" />
						</div>
						<div class="CommonFormField">
							<asp:textbox runat="server" id="Subject" cssclass="ControlPanelTextInput" width="500" />
						</div>
						<div class="CommonFormFieldName">
							<CP:ResourceControl resourcename="FeedbackEditor_Url" runat="Server" id="FeedbackEditor_Url" />
						</div>
						<div class="CommonFormField">
							<asp:textbox runat="server" id="Url" cssclass="ControlPanelTextInput" width="500" />
						</div>
						<div class="CommonFormFieldName">
							<CP:ResourceControl resourcename="FeedbackEditor_UserName" runat="Server" id="FeedbackEditor_UserName" />
						</div>
						<div class="CommonFormField">
							<asp:textbox runat="server" id="UserName" cssclass="ControlPanelTextInput" width="500" />
						</div>
						<div class="CommonFormFieldName">
							<CP:ResourceControl resourcename="FeedbackEditor_Comment" runat="Server" id="FeedbackEditor_Comment" />
						</div>												
						<div class="CommonFormField">
							<CSControl:editor runat="Server" id="Comment" width="500" height="125" />
						</div>			
				        <div class="CommonFormField PanelSaveButton">
				            <asp:LinkButton id="UnPublishButton" runat="server" CssClass="CommonTextButton" CommandArgument = "unpublish"></asp:LinkButton>
					        <asp:LinkButton id="PublishButton" runat="server" CssClass="CommonTextButton" CommandArgument="publish"></asp:LinkButton>
				        </div>
			</div>
		</div>
	</CP:Content>
</CP:Container>

<%@ Import Namespace="CommunityServer.Components" %>
<%@ Page language="c#" Codebehind="TokenForm.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.TokenForm" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelModalMaster.ascx" IsModal="true">
<CP:Content id="bcr" runat="server">
<DIV class="CommonContentArea">
<DIV class="CommonContent">
	<DIV class="CommonFormFieldName">
		<cp:helpicon id="hi1" runat="Server" resourcename="CP_Blog_TextParts_TextPartName_Help"></cp:helpicon>
		<CP:FormLabel id="fl1" runat="Server" ControlToLabel="TextPartBox" resourcename="CP_Blog_TextParts_TextPartName"></CP:FormLabel>
		<asp:RequiredFieldValidator id="TextPartValidator" Runat="server" ControlToValidate="TextPartBox" Display="Dynamic" />
		<asp:RegularExpressionValidator id="TextPartRegexValidator" runat="server" ControlToValidate="TextPartBox" Display="Dynamic" ValidationExpression="^[0-9a-zA-Z]{2,50}$" />
	</DIV>
	<DIV class="CommonFormField">
		<asp:TextBox id="TextPartBox" Runat="server" CssClass="ControlPanelTextInput" MaxLength="50" />
	</DIV>
	<br />
	<DIV class="CommonFormFieldName">
		<cp:helpicon id="hi2" runat="Server" resourcename="CP_Blog_TextParts_Link_Help"></cp:helpicon>
		<CP:FormLabel id="fl2" runat="Server" ControlToLabel="LinkBox" resourcename="CP_Blog_TextParts_Link" />
	</DIV>
	<DIV class="CommonFormField">
		<asp:TextBox id="LinkBox" Runat="server" CssClass="ControlPanelTextInput" MaxLength="255" />
	</DIV>
	<br />
	<DIV class="CommonFormFieldName">
		<cp:helpicon id="hi3" runat="Server" resourcename="CP_Blog_TextParts_Text_Help"></cp:helpicon>
		<CP:FormLabel id="fl3" runat="Server" ControlToLabel="TextDescBox" resourcename="CP_Blog_TextParts_Text"></CP:FormLabel>
		<asp:RequiredFieldValidator id="TextValidator" Runat="server" ControlToValidate="TextDescBox" Display="Dynamic" />
	</DIV>
	<DIV class="CommonFormField">
		<asp:TextBox id="TextDescBox" Runat="server" CssClass="ControlPanelTextInput" MaxLength="500" Rows="3" TextMode="MultiLine" />
	</DIV>
	<br />
	<DIV class="CommonFormField PanelSaveButton">
		<cp:resourcelinkbutton runat="server" id="SaveButton" cssclass="CommonTextButton" resourcename="Save"></cp:resourcelinkbutton>
	</DIV>
</DIV>
</DIV>
</CP:Content>
</CP:Container>

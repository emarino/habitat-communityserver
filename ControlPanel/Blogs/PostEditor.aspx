<%@ Page language="c#" Codebehind="PostEditor.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.PostEditorPage" %>
<%@ Register TagPrefix="CP" TagName = "Editor" Src = "~/ControlPanel/Blogs/CreateEditBlogPost.ascx" %>
<CP:ControlPanelSelectedNavigation id="SelectedNavigation1" runat="server" SelectedNavItem="Posts" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="BlogControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="Description" runat="server" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Blogs" />
		<CP:Editor id="Editor1" runat="server" />
	</CP:Content>
</CP:Container>

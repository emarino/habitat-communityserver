<%@ Control Language="c#" AutoEventWireup="false" Codebehind="PostListControl.ascx.cs" Inherits="CommunityServer.ControlPanel.Blogs.PostListControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Import Namespace = "CommunityServer.Components" %>
<%@ Import Namespace = "CommunityServer.Blogs.Components" %>
<%@ Import Namespace = "CommunityServer.ControlPanel" %>
<script language = "javascript" type="text/javascript">

</script>

<p>
	<table cellpadding="0" cellspacing="0" border="0"  width="100%">
		<tr>
			<td nowrap="nowrap">
				<CP:ResourceLinkButton visible="false" Runat="server" ResourceName="CP_Blog_CreatePost" ID="NewPost" CssClass="CommonTextButtonBig" resourcefile="ControlPanelResources.xml"/>
			</td>		
			<td align="right">
				<CP:ResourceControl resourcename="Feedback_Filter" runat="Server" id="FeedbackFilterLabel" />
				<asp:DropDownList id="MonthList" Runat="Server" />
				<asp:DropDownList id="CategoryList" Runat="Server" />
				<asp:dropdownlist id="filterPublished" runat="server"></asp:dropdownlist>&nbsp;
				<CP:resourcelinkbutton runat="server" resourcename="CP_ApplyFilter" resourcefile="ControlPanelResources.xml" id="Button1" cssclass="CommonTextButton" />
			</td>

		</tr>
	</table>
 </p>

		<div class="CommonListArea">
		<asp:Repeater runat = "Server" id = "theposts">
		<HeaderTemplate>
		<table id="CommentListing" cellSpacing="0" cellPadding="0" border="0" width="100%">
		<thead>
			<tr>
				<th class="CommonListHeaderLeftMost" ><cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Title" ID="Resourcecontrol3" NAME="Resourcecontrol3"/></th>
				<th class="CommonListHeader"><cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Views" ID="Resourcecontrol1" NAME="Resourcecontrol4"/></th>
                <th class="CommonListHeader"><cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_AggViews" ID="Resourcecontrol2" NAME="Resourcecontrol4"/></th>
                <th class="CommonListHeader"><cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Comments" ID="Resourcecontrol7" NAME="Resourcecontrol4"/></th>																																	
				<th class="CommonListHeader"><cp:resourcecontrol runat="server" resourcename="CP_Photos_GridCol_PublishedDate" ID="Resourcecontrol4" NAME="Resourcecontrol4"/></th>
				<th class="CommonListHeader"><cp:resourcecontrol runat="server" resourcename="CP_Photos_GridCol_Approved" ID="Resourcecontrol5" NAME="Resourcecontrol5"/></th>
                <th class="CommonListHeader"></th>
			</tr>
		</thead>
		</HeaderTemplate>
			<ItemTemplate>
			<tr>
			    <td class="CommonListCellLeftMost">
			    <a href="<%#  BlogUrls.Instance().PostEditor((int)DataBinder.Eval(Container.DataItem,"SectionID"), (int)DataBinder.Eval(Container.DataItem,"PostID")) %>">
			    <%# ShorterTitle((string)DataBinder.Eval(Container.DataItem,"Subject"), 40)%></a>
			    </td>
			    <td class="CommonListCell">
			    <a href="<%#  BlogUrls.Instance().ReferralToPost((int)DataBinder.Eval(Container.DataItem,"SectionID"), (int)DataBinder.Eval(Container.DataItem,"PostID")) %>">
			    <%# DataBinder.Eval(Container.DataItem,"Views")%></a>
			    </td>
			    <td class="CommonListCell"><%# DataBinder.Eval(Container.DataItem,"AggViews") %></td>
			    <td class="CommonListCell">
			    <%# DataBinder.Eval(Container.DataItem,"Replies") %>
			    
			    </td>			    			    			    
			    <td class="CommonListCell" nowrap="nowrap">
			        <CP:ResourceControl runat="Server" resourcename="CP_By" />: <strong><%# DataBinder.Eval(Container.DataItem,"DisplayName") %></strong><br/>
			        <%# UserTime.ConvertToUserTime((DateTime)DataBinder.Eval(Container.DataItem, "PostDate")).ToString(CSContext.Current.User.Profile.DateFormat + " hh:mm tt")%>
			    </td>

			    <td class="CommonListCell" align="center">
			        <img src="<%=SiteUrls.Instance().Locations["ControlPanel"]%>images/<%# DataBinder.Eval(Container.DataItem,"IsApproved").ToString() %>.gif" width="16" height="15" />
			    </td>
			    <td  class="CommonListCell" nowrap="nowrap">
			        <a href="<%# DataBinder.Eval(Container.DataItem, "ViewPostURL")%>" class="CommonTextButton"><CP:ResourceControl runat="Server" resourcename="CP_Blog_GridCol_View" /></a>
			        <cp:ResourceLinkButton CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PostID")%>' CommandName="Delete" Runat="server" ID = "DeleteButton" ResourceName="CP_Blog_GridCol_Delete" CssClass="CommonTextButton" />
			    </td>
			  </tr>
			</ItemTemplate>
            <AlternatingItemTemplate>
            <tr class="AltListRow">
			    <td class="CommonListCellLeftMost">
			    <a href="<%#  BlogUrls.Instance().PostEditor((int)DataBinder.Eval(Container.DataItem,"SectionID"), (int)DataBinder.Eval(Container.DataItem,"PostID")) %>">
			    <%# ShorterTitle((string)DataBinder.Eval(Container.DataItem,"Subject"), 40)%></a>
			    </td>
			    <td class="CommonListCell">
			    <a href="<%#  BlogUrls.Instance().ReferralToPost((int)DataBinder.Eval(Container.DataItem,"SectionID"), (int)DataBinder.Eval(Container.DataItem,"PostID")) %>">
			    <%# DataBinder.Eval(Container.DataItem,"Views")%></a>
			    </td>
			    <td class="CommonListCell"><%# DataBinder.Eval(Container.DataItem,"AggViews") %></td>
			    <td class="CommonListCell">
			    <%# DataBinder.Eval(Container.DataItem,"Replies") %>
			    
			    </td>			    			    			    
			    <td class="CommonListCell" nowrap="nowrap">
			        <CP:ResourceControl runat="Server" resourcename="CP_By" />: <strong><%# DataBinder.Eval(Container.DataItem, "DisplayName")%></strong><br/>
			        <%# UserTime.ConvertToUserTime((DateTime)DataBinder.Eval(Container.DataItem, "PostDate")).ToString(CSContext.Current.User.Profile.DateFormat + " hh:mm tt")%>
			    </td>

			    <td class="CommonListCell" align="center">
			        <img src="<%=SiteUrls.Instance().Locations["ControlPanel"]%>images/<%# DataBinder.Eval(Container.DataItem,"IsApproved").ToString() %>.gif" width="16" height="15" />
			    </td>
			    <td  class="CommonListCell" nowrap="nowrap">
			        <a href="<%# DataBinder.Eval(Container.DataItem, "ViewPostURL")%>" class="CommonTextButton"><CP:ResourceControl runat="Server" resourcename="CP_Blog_GridCol_View" /></a>
			        <cp:ResourceLinkButton CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PostID")%>' CommandName="Delete" Runat="server" ID = "DeleteButton" ResourceName="CP_Blog_GridCol_Delete" CssClass="CommonTextButton" />
			    </td>
			  </tr>
			</AlternatingItemTemplate>			
			
			
			<FooterTemplate>
			    </table>
			</FooterTemplate>
			
			</asp:Repeater>
			
			<cp:Pager runat = "Server" id = "thePager" />
			
			</div>	




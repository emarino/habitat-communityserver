<%@ Page language="c#" Codebehind="LinkCategoryForm.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.LinkCategoryForm" %>
<%@ Import Namespace="CommunityServer.Components" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelModalMaster.ascx" IsModal="true">
	<CP:Content id="bcr" runat="server">
		<script type="text/javascript">
		// <![CDATA[
		function closeModal(resultArray)
		{
			window.parent.Telligent_Modal.Close(resultArray);
		}
		// ]]>
		</script>
		<DIV class="CommonContentArea">
			<DIV class="CommonContent">
				<DIV class="CommonFormFieldName">
					<CP:FormLabel id="tt" runat="Server" ResourceName="CP_Blog_CategoryForm_Name" ControlToLabel="CategoryName"></CP:FormLabel>
					<asp:RequiredFieldValidator id="RequiredFieldValidator1" Text=" Please include a name" Display="Dynamic" ControlToValidate="CategoryName"
						Runat="server"></asp:RequiredFieldValidator></DIV>
				<DIV class="CommonFormField">
					<asp:TextBox id="CategoryName" Runat="server" MaxLength="256" CssClass="ControlPanelTextInput"></asp:TextBox></DIV>
				<DIV class="CommonFormFieldName">
					<CP:FormLabel id="Formlabel1" runat="Server" ResourceName="CP_Blog_CategoryForm_Description" ControlToLabel="CategoryDesc"></CP:FormLabel></DIV>
				<DIV class="CommonFormField">
					<asp:TextBox id="CategoryDesc" Runat="server" MaxLength="256" CssClass="ControlPanelTextInput"
						TextMode="MultiLine"></asp:TextBox></DIV>
				<DIV class="CommonFormFieldName">
					<asp:checkbox id="CategoryEnabled" runat="server"></asp:checkbox>
					<CP:FormLabel id="Formlabel2" runat="Server" ResourceName="CP_Blog_CategoryForm_Enabled" ControlToLabel="CategoryEnabled"></CP:FormLabel></DIV>
				<p><div class="CommonFormField PanelSaveButton">
					<CP:ResourceLinkButton id="Button1" ResourceName="Save" Runat="server"  CssClass="CommonTextButton"></CP:ResourceLinkButton></div></p>
			</DIV>
		</DIV>
	</CP:Content>
</CP:Container>

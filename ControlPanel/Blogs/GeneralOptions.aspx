<%@ Page language="c#" Codebehind="GeneralOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.GeneralOptionsPage" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="ChangeOwnerEmailAndDefaultLanguage" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="BlogControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
	<cp:resourcecontrol id="Resourcecontrol1" runat="server" resourcename="CP_Blogs_GeneralOptions_Title"></cp:resourcecontrol>
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">
	<cp:configokstatusmessage id="Status" runat="server" visible="false" />
	<DIV class="CommonDescription">
		<cp:resourcecontrol runat="server" resourcename="CP_Blogs_GeneralOptions_SubTitle" />
	</DIV>
	<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Blogs" />
	<DIV class="CommonFormArea">

	<DIV class="FixedWidthContainer">
        <fieldset>
            <legend><CP:FormLabel runat="server" ResourceName="CP_Blogs_GeneralOptions_AdminEmail" ControlToLabel="Email" /></legend>
		        <cp:resourcecontrol runat="server" resourcename="CP_Blogs_GeneralOptions_AdminEmail_Help" />
		        <asp:TextBox id="Email" width="300" Runat="Server" CssClass="ControlPanelTextInput" />
                <asp:RequiredFieldValidator id="EmailValidator" Runat="server" Display="Dynamic" ControlToValidate="Email" />
        </fieldset>

        <p>
        <fieldset>
            <legend><CP:FormLabel runat="server" ResourceName="CP_Blogs_GeneralOptions_Contact" ControlToLabel="EnableContact" /></legend>
		        <cp:resourcecontrol runat="server" resourcename="CP_Blogs_GeneralOptions_EnableContact_Help" /><br />
    			<asp:checkbox id="EnableContact" runat="Server" /> &nbsp; 
    			<CP:FormLabel runat="server" ResourceName="CP_Blogs_GeneralOptions_EnableContact" ControlToLabel="EnableContact" />
        </fieldset>
        </p>
		
        <p>
        <fieldset>
            <legend><CP:resourcecontrol runat="server" ResourceName="CP_Blogs_GeneralOptions_EmailNotifications" /></legend>
			<asp:checkbox id="BlockSpamFeedbackNotifications" runat="Server" cssclass="ControlPanelTextInput" />
			<cp:formlabel runat="Server" resourcename="CP_Blogs_GeneralOptions_BlockSpamFeedbackNotifications" controltolabel="BlockSpamFeedbackNotifications" /><br />
			<cp:formlabel runat="Server" resourcename="CP_Blogs_GeneralOptions_ModerationNotificationThreshold" controltolabel="ModerationNotificationThreshold" /> 
			<asp:TextBox id="ModerationNotificationThreshold" width="50" Runat="Server" CssClass="ControlPanelTextInput" />
        </fieldset>
        </p>
		
		<p style="text-align:right;">
			<cp:ResourceLinkButton id="SaveButton" runat="Server" ResourceName="Save" CssClass="CommonTextButtonBig" />
		</p>
	</DIV>
</CP:Content>
</CP:Container>

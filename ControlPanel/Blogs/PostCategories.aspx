<%@ Page language="c#" Codebehind="PostCategories.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.PostCategoriesListPage" %>
<%@ Import Namespace = "CommunityServer.Components" %>
<%@ Import Namespace = "CommunityServer.Blogs.Components" %>
<%@ Import Namespace = "CommunityServer.ControlPanel" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="PostCategories" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="BlogControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server"><CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_Blog_PostCategories_Title" /></CP:Content>
<CP:Content id="TaskRegion" runat="Server">
<script language = "javascript" type="text/javascript">
  function reloadCategories(res)
  {
    if(res)
    {
	    refresh();
	}
  }
</script>	
	
<DIV class="CommonDescription">
	<CP:ResourceControl id="Resourcecontrol2" runat="server" ResourceName="CP_Blog_PostCategories_SubTitle" />
</DIV>
<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Blogs" />

<p class="PanelSaveButton">
	<cp:ModalLink CssClass="CommonTextButtonBig" ModalType="Link" Height="300" Width="400" runat="Server" ResourceName="CP_Blog_Categories_NewCategory" Callback="refreshCallback" id="NewCategory" />
</p>

<div class="CommonListArea">
	<asp:Repeater runat = "Server" id = "theTags">
	
	<HeaderTemplate>
		<table id="CommentListing" cellSpacing="0" cellPadding="0" border="0" width="100%">
		<thead>
		<tr>
			<th class="CommonListHeaderLeftMost" >
			<cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Name" ID="Resourcecontrol3" NAME="Resourcecontrol3"/></th>
			<th class="CommonListHeader">
			<cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Description" ID="Resourcecontrol4" NAME="Resourcecontrol4"/></th>
			<th class="CommonListHeader">
			<cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_LastPost" ID="Resourcecontrol5" NAME="Resourcecontrol4"/></th>
			<th class="CommonListHeader">
			<cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Posts" ID="Resourcecontrol7" NAME="Resourcecontrol4"/></th>																																	
			<th class="CommonListHeader">
			<cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Published" ID="Resourcecontrol8" NAME="Resourcecontrol5"/></th>
			<th class="CommonListHeader">
			<cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Actions" ID="Resourcecontrol9" NAME="Resourcecontrol6"/></th>											
		</tr>
		</thead>
	</HeaderTemplate>
	<ItemTemplate>
		<tr>
			<td class="CommonListCellLeftMost">
			<a href="javascript:Telligent_Modal.Open('CategoryForm.aspx?sectionid=<%# DataBinder.Eval(Container.DataItem,"SectionID") %>&categoryID=<%# DataBinder.Eval(Container.DataItem,"CategoryID") %>', 400, 300, refreshCallback);">
			<%# DataBinder.Eval(Container.DataItem,"Name")%></a>
			</td>
			<td class="CommonListCell"><%# DataBinder.Eval(Container.DataItem,"Description")%></td>
			<td class="CommonListCell"><%# LastPostDesc(DataBinder.Eval(Container.DataItem, "MostRecentPostDate", "{0:" + CSContext.Current.User.Profile.DateFormat + ", hh:mm tt}"))%></td>
			<td class="CommonListCell">
			<cp:LiteralOrLink runat = "Server" NavigateUrl='<%# "postlist.aspx?tab=postlist&cid=" + DataBinder.Eval(Container.DataItem,"CategoryID").ToString() %>' Text = '<%# DataBinder.Eval(Container.DataItem,"TotalThreads") %>' ID="PostsLink" />
		        
			</td>			    			    			    
			<td class="CommonListCell" align="center">
				<img src="<%=SiteUrls.Instance().Locations["ControlPanel"]%>images/<%# DataBinder.Eval(Container.DataItem,"IsEnabled").ToString() %>.gif" width="16" height="15" />
			</td>
			<td  class="CommonListCell" nowrap="nowrap">
				<a href="javascript:Telligent_Modal.Open('CategoryForm.aspx?sectionid=<%# DataBinder.Eval(Container.DataItem,"SectionID") %>&categoryID=<%# DataBinder.Eval(Container.DataItem,"CategoryID") %>', 400, 300, reloadCategories);" class="CommonTextButton"><CP:ResourceControl runat="Server" resourcename="CP_Blog_GridCol_Edit" /></a>
				<cp:ResourceLinkButton CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CategoryID")%>' CommandName="Delete" Runat="server" ID = "DeleteButton" ResourceName="CP_Blog_GridCol_Delete" CssClass="CommonTextButton" />
			</td>
		  </tr>
	</ItemTemplate>
    <AlternatingItemTemplate>
		<tr class="AltListRow">
			<td class="CommonListCellLeftMost">
			<a href="javascript:Telligent_Modal.Open('CategoryForm.aspx?sectionid=<%# DataBinder.Eval(Container.DataItem,"SectionID") %>&categoryID=<%# DataBinder.Eval(Container.DataItem,"CategoryID") %>', 400, 300, reloadCategories);">
			<%# DataBinder.Eval(Container.DataItem,"Name")%></a>
			</td>
			<td class="CommonListCell"><%# DataBinder.Eval(Container.DataItem,"Description")%></td>
			<td class="CommonListCell"><%# LastPostDesc(DataBinder.Eval(Container.DataItem, "MostRecentPostDate", "{0:" + CSContext.Current.User.Profile.DateFormat + ", hh:mm tt}"))%></td>
			<td class="CommonListCell">
			<cp:LiteralOrLink runat = "Server" NavigateUrl='<%# "postlist.aspx?tab=postlist&cid=" + DataBinder.Eval(Container.DataItem,"CategoryID").ToString() %>' Text = '<%# DataBinder.Eval(Container.DataItem,"TotalThreads") %>' ID="Literalorlink1" />
		        
			</td>			    			    			    
			<td class="CommonListCell" align="center">
				<img src="<%=SiteUrls.Instance().Locations["ControlPanel"]%>images/<%# DataBinder.Eval(Container.DataItem,"IsEnabled").ToString() %>.gif" width="16" height="15" />
			</td>
			<td  class="CommonListCell" nowrap="nowrap">
				<a href="javascript:Telligent_Modal.Open('CategoryForm.aspx?sectionid=<%# DataBinder.Eval(Container.DataItem,"SectionID") %>&categoryID=<%# DataBinder.Eval(Container.DataItem,"CategoryID") %>', 400, 300, refreshCallback);" class="CommonTextButton"><CP:ResourceControl runat="Server" resourcename="CP_Blog_GridCol_Edit" /></a>
				<cp:ResourceLinkButton CommandArgument='<%# DataBinder.Eval(Container.DataItem, "CategoryID")%>' CommandName="Delete" Runat="server" ID = "DeleteButton" ResourceName="CP_Blog_GridCol_Delete" CssClass="CommonTextButton" />
			</td>
		  </tr>
	</AlternatingItemTemplate>			
	<FooterTemplate>
	    </table>
	</FooterTemplate>
	</asp:Repeater>
	
</div>			
</CP:Content>
</CP:Container>

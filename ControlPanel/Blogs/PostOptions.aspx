<%@ Page language="c#" Codebehind="PostOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.PostOptionsPage" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="AdvancedPostSettings" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="BlogControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol runat="server" resourcename="CP_Blogs_PostOptions_Title" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<cp:configokstatusmessage id="Status" runat="server" visible="false" />
		<DIV class="CommonDescription">
			<cp:resourcecontrol runat="server" resourcename="CP_Blogs_PostOptions_SubTitle" />
		</DIV>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Blogs" />

		<table cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_Blogs_PostOptions_AllowAnonymousReplies_Help" />
				<CP:FormLabel runat="Server" ControlToLabel="ynEnableAnonymousReplies" ResourceName="CP_Blogs_PostOptions_AllowAnonymousReplies" />
			</td>
			<td class="CommonFormField">
				<cp:YesNoRadioButtonList id="ynEnableAnonymousReplies" runat="server" RepeatColumns="2" CssClass="ControlPanelTextInput1" />
				<asp:literal id="AnonymousRepliesMessage" runat="server" visible="false" />
				<CP:LiteralOrLink runat="server" ID="AnonymousRepliesEnableLink" Visible="false" />
			</td>
		</tr>
		<TR>
			<TD class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_Blogs_PostOptions_ModerateComments_Help" />
				<CP:FormLabel ID="FormLabel2" runat="Server" ControlToLabel="ModerationDDLOverride" ResourceName="CP_Blogs_PostOptions_ModerateComments" />
			</TD>
			<TD class="CommonFormField">
				<cp:CommentModerationDropDownList runat="Server" id="ModerationDDLOverride" AllowIgnore="true" />
			</TD>
		</TR>
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_Blogs_PostOptions_AllowReplies_Help" />
				<CP:FormLabel runat="Server" ControlToLabel="ynEnableRepliesOverride" ResourceName="CP_Blogs_PostOptions_AllowReplies" />
			</td>
			<td class="CommonFormField">
				<cp:YesNoRadioButtonList id="ynEnableRepliesOverride" runat="server" RepeatColumns="2" CssClass="ControlPanelTextInput1" />
			</td>
		</tr>
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_Blogs_PostOptions_EnableTrackBacks_Help" />
				<CP:FormLabel runat="Server" ControlToLabel="ynEnableTrackbacksOverride" ResourceName="CP_Blogs_PostOptions_EnableTrackBacks" />
			</td>
			<td class="CommonFormField">
				<cp:YesNoRadioButtonList id="ynEnableTrackbacksOverride" runat="server" RepeatColumns="2" CssClass="ControlPanelTextInput1" />
			</td>
		</tr>
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_Blogs_PostOptions_ExpiredDays_Help" />
				<CP:FormLabel runat="Server" ControlToLabel="ExpiredDays" ResourceName="CP_Blogs_PostOptions_ExpiredDays" />
			</td>
			<td class="CommonFormField">
				<asp:DropDownList id="ExpiredDays" Runat="server" />
			</td>
		</tr>
		<tr>
			<td class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_Blogs_PostOptions_EnableRatings_Help" />
				<CP:FormLabel ID="FormLabel1" runat="Server" ControlToLabel="ynEnableRatingsOverride" ResourceName="CP_Blogs_PostOptions_EnableRatings" />
			</td>
			<td class="CommonFormField">
				<cp:YesNoRadioButtonList id="ynEnableRatingsOverride" runat="server" RepeatColumns="2" CssClass="ControlPanelTextInput1" />
			</td>
		</tr>
		</table>

		<p class="PanelSaveButton DetailsFixedWidth">
			<cp:ResourceLinkButton id="SaveButton" runat="Server" CssClass="CommonTextButtonBig" ResourceName="Save" />
		</p>
	</CP:Content>
</CP:Container>

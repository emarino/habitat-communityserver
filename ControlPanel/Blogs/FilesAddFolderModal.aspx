<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="FilesAddFolderModal.aspx.cs" Inherits="CommunityServerWeb.ControlPanel.Blogs.FilesAddFolderModal" %>
<%@ Import Namespace="CommunityServer.Components" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelModalMaster.ascx" IsModal="true">
<CP:Content id="bcr" runat="server">
<script type="text/javascript">
// <![CDATA[
function closeModal()
{
	window.parent.Telligent_Modal.Close(true);
}
// ]]>
</script>
<div class="CommonContentArea">
	<div class="CommonContent">
	
	<div class="CommonFormFieldName">
		<CP:FormLabel id="tt" runat="Server" ResourceName="CP_Blog_Files_FolderName" ControlToLabel="FolderName" />
	</div>
	<div class="CommonFormField">
		<asp:TextBox runat="server" id="FolderName" maxlength="250" />
		<asp:RegularExpressionValidator ValidationExpression="^[^\.\$\~\|\\\/].*" 
                     ControlToValidate="FolderName" Display="Static"
                     ErrorMessage="<br/>Folder name starts with an invalid character." runat="server" />
	</div>
	<div class="CommonFormField PanelSaveButton">
		<CP:ResourceLinkButton id="btnSave" ResourceName="Save" Runat="server"  CssClass="CommonTextButton" />
	</div>

	</div>
</div>
</CP:Content>
</CP:Container>

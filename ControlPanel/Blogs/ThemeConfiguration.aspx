<%@ Page Language="C#" EnableViewState="false" AutoEventWireup="true" CodeBehind="ThemeConfiguration.aspx.cs" Inherits="CommunityServer.ControlPanel.Blogs.ThemeConfiguration" %>
<cp:controlpanelselectednavigation selectednavitem="ThemeConfiguration" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer"  ThemeMasterFile="BlogControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
    <CP:ResourceControl ResourceName="CP_Blog_ThemeConfiguration_Title" runat="server" />
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">

    <script type="text/javascript">
    // <![CDATA[
    function refresh(shouldRefresh)
    {
        if (shouldRefresh)
            window.location = window.location;
    }
    // ]]>
    </script>

	<CP:StatusMessage id="StatusMessage" runat="server" />
	
	<DIV class="CommonDescription">
	    <CP:ResourceControl runat="server" ResourceName="CP_Blog_ThemeConfiguration_Description" />
	</DIV>
	<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Blogs" />
	<DIV class="CommonFormArea">
	<div style="margin-bottom: 1em;">
    <table cellspacing="0" cellpadding="2" border="0">
    <tr>
        <td class="CommonFormFieldName">
            <CP:ResourceControl runat="server" ResourceName="CP_Blog_ThemeConfiguration_ThemeName" />
        </td>
        <td class="CommonFormField">
            <TWC:DropDownList runat="server" ID="Themes" ShowHtmlWhenSelected="false" SelectListWidth="440" SelectListHeight="375" />
        </td>
    </tr>
    </table>
    </div>
	<div class="FixedWidthContainer">
	    <div style="float: left;"><asp:Image runat="server" id="ThemePreviewImage" Width="100" Height="75" BorderWidth="1" style="margin-right: 1em;" /></div>
	    <h4 class="CommonFormTitle"><asp:Literal runat="server" id="ThemeName" /></h4>
	    <div class="CommonFormDescription">
	        <asp:Literal runat="server" id="ThemePreviewText" />
	    </div>
	    <div style="clear: both; padding-bottom: 1em;"></div>
	
	    <CSDynConfig:ConfigurationForm runat="server" id="ConfigurationForm"
	        RenderGroupsInTabs="true" 
	        PanesCssClass="CommonPane"
		    TabSetCssClass="CommonPaneTabSet"
		    TabCssClasses="CommonPaneTab,CommonPaneTab1,CommonPaneTab2"
		    TabSelectedCssClasses="CommonPaneTabSelected,CommonPaneTabSelected1,CommonPaneTabSelected2"
		    TabHoverCssClasses="CommonPaneTabHover,CommonPaneTabHover1,CommonPaneTabHover2"
		    >
		    <PropertyFormGroupHeaderTemplate>
		        <div class="CommonFormDescription">
		            <CSDynConfig:PropertyGroupData Property="Description" runat="server" />
		        </div>
		        <table cellspacing="0" cellpadding="2" border="0">
		    </PropertyFormGroupHeaderTemplate>
		    <PropertyFormSubGroupHeaderTemplate>
		        <tr><td colspan="2">
                    <div class="CommonSubTitle"><CSDynConfig:PropertySubGroupData Property="Name" runat="server" /></div>
                </td></tr>
		    </PropertyFormSubGroupHeaderTemplate>
		    <PropertyFormPropertyTemplate>
		        <tr valign="top">
		            <td class="CommonFormFieldName"><CP:DynamicThemePropertyHelpIcon runat="server" /><CSDynConfig:PropertyData Property="Name" runat="server" /></td>
		            <td class="CommonFormField"><CSDynConfig:PropertyControl runat="server" /></td>
		        </tr>
		    </PropertyFormPropertyTemplate>
		    <PropertyFormSubGroupFooterTemplate>
		        <tr><td colspan="2"><div style="height: .5em;"></div></td></tr>
		    </PropertyFormSubGroupFooterTemplate>
		    <PropertyFormGroupFooterTemplate>
		        </table>
		    </PropertyFormGroupFooterTemplate>
		    <AppendedTabbedPanes>
		        <TWC:TabbedPane runat="server">
		            <Tab><CP:ResourceControl ResourceName="CP_Blog_ThemeConfiguration_Preview_Title" runat="server" /></Tab>
		            <Content>
		                <asp:Literal runat="server" id="PreviewMessage" />
	                    <p />
	                    <asp:HyperLink runat="server" id="OpenPreview" Target="_blank" runat="server" CssClass="CommonTextButton"><cp:ResourceControl runat="server" ResourceName="CP_Blog_ThemeConfiguration_OpenNewPreviewWindow" /></asp:HyperLink>
	                    <cp:resourcelinkbutton id="UpdatePreviewButton" runat="server" ResourceName="CP_Blog_ThemeConfiguration_StartPreview" cssclass="CommonTextButton" />
                        <cp:resourcelinkbutton id="EndPreviewButton" runat="server" ResourceName="CP_Blog_ThemeConfiguration_EndPreview" Visible="false" cssclass="CommonTextButton" />
		            </Content>
		        </TWC:TabbedPane>
		    </AppendedTabbedPanes>
	    </CSDynConfig:ConfigurationForm>
	</div>
	<p class="PanelSaveButton DetailsFixedWidth">
        <cp:resourcelinkbutton id="RestoreDefaultsButton" runat="server" resourcename="CP_Blog_ThemeConfiguration_RestoreDefaults" cssclass="CommonTextButton" />
        &nbsp;&nbsp;
        <cp:ModalLink runat="server" id="ImportButton" Width="550" Height="200" Callback="refresh" Text="Import" CssClass="CommonTextButton" />
        <asp:HyperLink runat="server" id="ExportConfiguration" CssClass="CommonTextButton">Export</asp:HyperLink>
        &nbsp;&nbsp;	        
        <cp:resourcelinkbutton id="SaveButton" runat="server" resourcename="Save" cssclass="CommonTextButton" />
	</p>
</DIV>
</CP:Content>
</CP:Container>
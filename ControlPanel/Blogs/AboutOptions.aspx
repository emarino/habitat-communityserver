<%@ Page language="c#" Codebehind="AboutOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.AboutOptionsPage" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="AboutMyBlog" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="BlogControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
	<CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_Blogs_AboutOptions_Title" />
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">
	<cp:configokstatusmessage id="Status" runat="server" visible="false" />
	<DIV class="CommonDescription">
		<CP:ResourceControl id="Resourcecontrol2" runat="server" ResourceName="CP_Blogs_AboutOptions_SubTitle" />
	</DIV>
	<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Blogs" />
	<DIV class="CommonFormArea">
		<DIV class="FixedWidthContainer">
			<TABLE cellSpacing="0" cellPadding="0" border="0">
			<TR>
				<TD nowrap="noWrap" class="CommonFormFieldName">
					<cp:helpicon runat="Server" resourcename="CP_Blogs_AboutOptions_EnableAbout_Help" />
					<CP:FormLabel runat="Server" ResourceName="CP_Blogs_AboutOptions_EnableAbout" ControlToLabel="ynEnableAbout" />&nbsp;
				</TD>
				<TD nowrap="noWrap" class="CommonFormField">
					<cp:YesNoRadioButtonList id="ynEnableAbout" runat="server" RepeatColumns="2" CssClass="ControlPanelTextInput1" />
				</TD>
			</TR>
			</TABLE>
			<DIV class="CommonFormFieldName">
				<cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_Blogs_AboutOptions_AboutTitle_Help" />
				<CP:FormLabel id="FormLabel1" runat="server" ResourceName="CP_Blogs_AboutOptions_AboutTitle" ControlToLabel="AboutTitle" />
			</DIV>
			<DIV class="CommonFormField">
				<asp:TextBox id="AboutTitle" CssClass="ControlPanelTextInput" Runat="Server" />
			</DIV>
			<DIV class="CommonFormFieldName">
				<cp:helpicon id="Helpicon2" runat="Server" resourcename="CP_Blogs_AboutOptions_AboutDescription_Help" />
				<CP:FormLabel id="FormLabel2" runat="server" ResourceName="CP_Blogs_AboutOptions_AboutDescription" ControlToLabel="Description" />
			</DIV>
			<DIV class="CommonFormField">
				<CSControl:Editor id="AboutDescription" runat="Server" Height="225px" />
			</DIV>
		</DIV>
		<p class="PanelSaveButton DetailsFixedWidth">
			<cp:ResourceLinkButton id="SaveButton" runat="Server" CssClass="CommonTextButtonBig" ResourceName="Save" />
		</p>
	</DIV>
</CP:Content>
</CP:Container>

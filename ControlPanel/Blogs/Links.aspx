<%@ Page language="c#" Codebehind="Links.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.LinksPage" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="Links" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="BlogControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<cp:resourcecontrol id="Resourcecontrol1" runat="server" resourcename="CP_Blog_Links_Title"></cp:resourcecontrol>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<script type="text/javascript">
    // <![CDATA[

	function reloadCategories(res)
	{
		if (res)
		{
			var element = $('<%=  CategoryList.ClientID %>');
			res[1] = res[1].split('@@@').join('"');
			window.location = window.location.pathname + '?categoryid=' + res[2];
			return;
		}
	}

	function selectCategory()
	{
		var element = $('<%=  CategoryList.ClientID %>');
		var index = element.selectedIndex;
		window.location = window.location.pathname + '?categoryid=' + element.options[index].value;
 
	}

	function removeCategory(res)
	{
	    if(res)
	    {
            window.location = window.location.pathname;
        }
        return;
  	}
  	// ]]>
		</script>
		<DIV class="CommonDescription">
			<cp:resourcecontrol id="Resourcecontrol2" runat="server" resourcename="CP_Blog_Links_SubTitle" />
		</DIV>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Blogs" />
		<DIV class="CommonFormArea">
			<TABLE cellSpacing="0" cellPadding="0" border="0">
				<TR>
					<TD class="CommonFormName" nowrap="nowrap">
						<cp:formlabel id="Formlabel1" runat="server" resourcename="CP_Blog_Links_SelectCategory" controltolabel="CategoryList" />
					</TD>
					<TD class="CommonFormField" width="100%">
						<asp:DropDownList id="CategoryList" runat="Server" />
					</TD>
					<TD class="CommonFormField" nowrap="nowrap">
						<cp:ModalLink id="ModalLink1" runat="server" Url="~/ControlPanel/Blogs/LinkCategoryForm.aspx" resourcename="CP_Blog_Categories_NewSideBarList" callback="reloadCategories" Width="400" Height="300" CssClass="CommonTextButtonBig" />
					</TD>
				</TR>
			</TABLE>
		</DIV>

				<div>
					<cp:ModalLink runat="server" CssClass="CommonTextButton" Height="300" Width="400" Callback="refreshCallback" ResourceName="CP_Blog_Links_NewLink" id="NewLink" />
					<cp:ModalLink runat="server" CssClass="CommonTextButton" Height="300" Width="400" Callback="reloadCategories" ResourceName="CP_Blog_Links_EditCategory" id="EditCategory" />
					<cp:ModalLink runat="server" CssClass="CommonTextButton" Height="150" Width="300" Callback="removeCategory" ResourceName="CP_Blog_Links_DeleteCategory" id="DeleteCategory" />
				</div>
				<asp:Repeater Runat="server" id="LinkList">
					<HeaderTemplate>
						<div class="CommonListArea">
							<table id="Listing" cellSpacing="0" cellPadding="0" border="0" width="100%">
								<thead>
									<tr>
										<th class="CommonListHeaderLeftMost">
											<cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Link" /></th>
										<th class="CommonListHeader">
											<cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Relations" /></th>
										<th class="CommonListHeader">
											<cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Enabled" /></th>
										<th class="CommonListHeader">
											<cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Actions" /></th>
									</tr>
								</thead>
					</HeaderTemplate>
					<ItemTemplate>
						<tr>
							<td class="CommonListCellLeftMost">
								<a target="_blank" href='<%# DataBinder.Eval(Container.DataItem, "Url") %>'>
									<%# DataBinder.Eval(Container.DataItem, "Title") %>
								</a>
								<br />
								<%# DataBinder.Eval(Container.DataItem, "Description") %>
							</td>
							<td class="CommonListCell">
								<%# DataBinder.Eval(Container.DataItem,"Rel") %>
								&nbsp;
							</td>
							<td class="CommonListCell">
								<center>
									<%# String.Format(@"<img src=""{0}images/{1}.gif"" width=""16"" height=""15"" />", CommunityServer.Components.SiteUrls.Instance().Locations["ControlPanel"], DataBinder.Eval(Container.DataItem,"IsEnabled")) %>
								</center>
							</td>
							<td class="CommonListCell">
								<center>
								<cp:ModalLink CssClass="CommonTextButton" runat = "Server" Height="300" Width="400" callback="refreshCallback" resourcename="Edit" Url = '<%# "LinkForm.aspx?LinkID=" + DataBinder.Eval(Container.DataItem, "LinkID") + "&CategoryID=" +  DataBinder.Eval(Container.DataItem, "LinkCategoryID") %>' />
								<cp:ModalLink CssClass="CommonTextButton" runat = "Server" Height="150" Width="400" callback="refreshCallback" resourcename="Delete" Url = '<%# "BlogCommand.aspx?Command=CommunityServer.ControlPanel.Blogs.DeleteLinkCommand,CommunityServer.Web&LinkID=" + DataBinder.Eval(Container.DataItem, "LinkID") + "&CategoryID=" +  DataBinder.Eval(Container.DataItem, "LinkCategoryID") %>' />
								</center>
							</td>
						</tr>
					</ItemTemplate>
					<AlternatingItemTemplate>
						<tr>
							<td class="CommonListCellLeftMost">
								<a target="_blank" href='<%# DataBinder.Eval(Container.DataItem, "Url") %>'>
									<%# DataBinder.Eval(Container.DataItem, "Title") %>
								</a>
								<br />
								<%# DataBinder.Eval(Container.DataItem, "Description") %>
							</td>
							<td class="CommonListCell">
								<%# DataBinder.Eval(Container.DataItem,"Rel") %>
								&nbsp;
							</td>
							<td class="CommonListCell">
								<center>
									<%# String.Format(@"<img src=""{0}images/{1}.gif"" width=""16"" height=""15"" />", CommunityServer.Components.SiteUrls.Instance().Locations["ControlPanel"], DataBinder.Eval(Container.DataItem,"IsEnabled")) %>
								</center>
							</td>
							<td class="CommonListCell">
								<center>
								<cp:ModalLink CssClass="CommonTextButton" runat = "Server" Height="300" Width="400" callback="refreshCallback" resourcename="Edit" Url = '<%# "LinkForm.aspx?LinkID=" + DataBinder.Eval(Container.DataItem, "LinkID") + "&CategoryID=" +  DataBinder.Eval(Container.DataItem, "LinkCategoryID") %>' ID="Modallink2"/>
								<cp:ModalLink CssClass="CommonTextButton" runat = "Server" Height="150" Width="400" callback="refreshCallback" resourcename="Delete" Url = '<%# "BlogCommand.aspx?Command=CommunityServer.ControlPanel.Blogs.DeleteLinkCommand,CommunityServer.Web&LinkID=" + DataBinder.Eval(Container.DataItem, "LinkID") + "&CategoryID=" +  DataBinder.Eval(Container.DataItem, "LinkCategoryID") %>' ID="Modallink3"/>
								</center>
							</td>

						</tr>
					</AlternatingItemTemplate>
					<FooterTemplate>
						</table>
						</div>
					</FooterTemplate>
				</asp:Repeater>

	</CP:Content>
</CP:Container>

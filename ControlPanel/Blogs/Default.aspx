<%@ Page language="c#" Codebehind="Default.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.BlogEditorHomePage" EnableViewstate="false" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="CommonTasks" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="BlogControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
	<cp:resourcecontrol id="Resourcecontrol1" runat="server" resourcename="CP_Blog_Home_Title" />
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">

	<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Blogs" />

	<fieldset>
	<legend><cp:resourcecontrol runat="server" resourcename="CP_Blog_Home_Owner" /></legend>

	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td class="CommonFormFieldName">
		<cp:resourcecontrol runat="server" resourcename="CP_Blog_Home_Author" />
	</td>
	<td class="CommonFormField">
		<asp:repeater id="Owners" runat="Server">
			<itemtemplate>
				<asp:HyperLink id="owner" runat="server" />
			</itemtemplate>
			<separatortemplate>;</separatortemplate>
		</asp:repeater>
	</td>
	</tr>
		
	</table>					

	<asp:literal id="DateCreated" runat="Server" />
    </fieldset>

	<p />

	<fieldset>
	<legend><cp:resourcecontrol runat="server" resourcename="CP_Blog_Home_PostsArticles" /></legend>
	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td class="CommonFormFieldName">
		<cp:resourcecontrol runat="server" resourcename="CP_Blog_Home_TotalPosts" />
	</td>
	<td class="CommonFormField">
		<asp:literal id="PostCount" runat="Server" />
	</td>
	</tr>
	
	<tr>
	<td class="CommonFormFieldName">
		<cp:resourcecontrol runat="server" resourcename="CP_Blog_Home_TotalArticles" />
	</td>
	<td class="CommonFormField">
		<asp:literal id="ArticleCount" runat="Server" />
	</td>
	</tr>

	<tr id="MostRecentPostSpan" runat="server">
	<td class="CommonFormFieldName">
		<cp:resourcecontrol runat="server" resourcename="CP_Blog_Home_MostRecentPost" />
	</td>
	<td class="CommonFormField">
		<asp:hyperlink id="MostRecentPostLink" runat="Server"></asp:hyperlink>
		<cp:resourcecontrol runat="server" resourcename="CP_Blog_Home_on" />
		<asp:literal id="MostRecentPostDate" runat="Server" />
		<cp:resourcecontrol runat="server" resourcename="CP_Blog_Home_by" />
		<asp:hyperlink id="MostRecentPostUser" runat="Server" />
	</td>
	</tr>
	
	<tr id="MostRecentArticleSpan" runat="server">
	<td class="CommonFormFieldName">
		<cp:resourcecontrol runat="server" resourcename="CP_Blog_Home_MostRecentArticle" />
	</td>
	<td class="CommonFormField">
		<asp:hyperlink id="MostRecentArticleLink" runat="Server"></asp:hyperlink>
		<cp:resourcecontrol runat="server" resourcename="CP_Blog_Home_on" />
		<asp:literal id="MostRecentArticleDate" runat="Server" />
		<cp:resourcecontrol runat="server" resourcename="CP_Blog_Home_by" />
		<asp:hyperlink id="MostRecentArticleUser" runat="Server" />
	</td>
	</tr>
	</table>
    </fieldset>


	<p />
	<fieldset>
	<legend><cp:resourcecontrol runat="server" resourcename="CP_Blog_Home_Feedback" /></legend>
	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
	<td class="CommonFormFieldName">
		<cp:resourcecontrol ID="Resourcecontrol2" runat="server" resourcename="CP_Blog_Home_CommentsModerate" />
	</td>
	<td class="CommonFormField">
		<asp:hyperlink id="CommentModerateCount" runat="Server" />
	</td>
	</tr>

	<tr>
	<td class="CommonFormFieldName">
		<cp:resourcecontrol runat="server" resourcename="CP_Blog_Home_TotalComments" />
	</td>
	<td class="CommonFormField">
		<asp:hyperlink id="CommentCount" runat="Server" />
	</td>
	</tr>
	
	<tr>
	<td class="CommonFormFieldName">
		<cp:resourcecontrol id="CP_Blog_Home_TotalTrackbacks" runat="server" resourcename="CP_Blog_Home_TotalTrackbacks" />
	</td>
	<td class="CommonFormField">
		<asp:hyperlink id="TrackbackCount" runat="Server" />
	</td>
	</tr>
	</table>
	</fieldset>


</CP:Content>
</CP:Container>

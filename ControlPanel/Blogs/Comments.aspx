<%@ Page language="c#" EnableViewState = "false" Codebehind="Comments.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.CommentsPage" %>
<%@ Register TagPrefix="CPB" Namespace="CommunityServer.ControlPanel.Blogs" Assembly="CommunityServer.Web" %>
<%@ Import Namespace = "CommunityServer.Components" %>
<%@ Import Namespace = "CommunityServer.ControlPanel" %>
<script runat="Server">

string Clean(object obj)
{
    string name = obj as string;
    if(name == null)
        return string.Empty;
        
      name = name.Trim();
      if(name.Length <= 50)
        return name;
        
      return name.Substring(0,47) + "...";
}

</script>

<CP:ControlPanelSelectedNavigation SelectedNavItem="Comments" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="BlogControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
	<CP:ResourceControl runat="server" ResourceName="CP_Blog_Comments_Title" />
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">
<script type = "text/javascript">
function redirectUrl()
{
    var filterPost = $('<%=filterPost.ClientID%>');
    var filterPublished = $('<%=filterPublished.ClientID%>');
    var filterBlogPostType = $('<%=filterBlogPostType.ClientID%>');
    var spamFilter = $('<%=SpamFilter.ClientID%>');
    var url = window.location.pathname + '?pid=' + filterPost[filterPost.selectedIndex].value + '&ip=' + filterPublished[filterPublished.selectedIndex].value + '&bpt=' + filterBlogPostType[filterBlogPostType.selectedIndex].value + '&spam=' + spamFilter[spamFilter.selectedIndex].value;
    window.location = url; 
}

function checkAll(cb, parent)
{
	var p =$(parent);
	var pa = p.getElementsByTagName('INPUT');
	for(var i=0; i<pa.length; i++)
	{
		var e = pa[i];
		if(e.getAttribute('type') == 'checkbox' && e.name != cb.name)
		{
		    e.checked = cb.checked;
		}
	}
}

function bulkEdit(action)
{
    var result = 'action=' + action + '&postids=';
    var cb = $('master');
	var pa = $('CommentListing').getElementsByTagName('INPUT');
	var b = true;
	for(var i=0; i<pa.length; i++)
	{
		var e = pa[i];
		if(e.getAttribute('type') == 'checkbox' && e.name != cb.name && e.checked)
		{
		    var delimit = ',';
		    if(b)
		    {
	        	delimit = '';
	        	b = false;    
		    }
            result += e.name.replace('cb-',delimit);
		}
	}
	
	if(window.location.search)
      window.location =   window.location + '&' + result;    
    else
      window.location =   window.location + '?' + result;
}

function updatePublishStatus(postid, defaultStatus)
{
    var basePath = '<%=SiteUrls.Instance().Locations["ControlPanel"]%>images/';   
    var img = $('status-image-' + postid);
    var button = $('button-' + postid);
    
    if(button.isPublished == null)
    {
        button.isPublished = !defaultStatus;
             
    }
    else
    {
        button.isPublished = !button.isPublished;
    }

    Comments.Toggle(postid,null);

    if(button.isPublished)
        button.innerHTML = '<%= ResourceManager.GetString("FeedbackEditor_NotSaved") %>';
    else
        button.innerHTML = '<%= ResourceManager.GetString("FeedbackEditor_Save") %>';
    
    img.src = basePath + button.isPublished + '.gif';
    
}

function checkIsPublished(postid, defaultStatus)
{
    var button = $('button-' + postid);
    var published;
    
    if(button.isPublished == null)
    {
        published = defaultStatus;
    }
    else
    {
        published = button.isPublished;
    }

	if (!published)  
	{
		alert('<%= ResourceManager.GetString("FeedbackEditor_ViewUnpublished") %>');
		return false;
	}
	
	return true;
}
</script>	
	
<div class="CommonDescription">
	<CP:ResourceControl runat="server" ResourceName="CP_Blog_Comments_SubTitle" /><br />
</div>
<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Blogs" />
<div>			
<table border="0" cellpadding="3" cellpadding="3">
<tr>
    <td align="right">
		<CP:ResourceControl id="FeedbackFilterLabel" runat="Server" resourcename="Feedback_Filter" />
    </td>
    <td>
		<asp:dropdownlist id="filterPost" runat="server" width="200px"></asp:dropdownlist>
		<asp:dropdownlist id="filterBlogPostType" runat="server"></asp:dropdownlist>
		<asp:dropdownlist id="filterPublished" runat="server"></asp:dropdownlist>&nbsp;
		<asp:dropdownlist id="SpamFilter" runat="server"></asp:dropdownlist>&nbsp;
		<CP:ResourceButton id="FilterButton" Runat="server" ResourceName="CP_ApplyFilter" visible = "false" />
		<input type = "button" value = "<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_ApplyFilter")%>" onclick = "redirectUrl();" />
    </td>        
</tr>
<tr>
	<td>
		<CP:ResourceControl id="FeedbackFilterLabelBulk" runat="Server" resourcename="Feedback_BulkAction" />
	</td>
	<td align="left">
		<a href="javascript:bulkEdit('approve')" class="CommonTextButton"><cp:ResourceControl runat = "Server" resourceName = "FeedbackEditor_PublishSelected" ID="Resourcecontrol3"/></a> 
		<a href="javascript:bulkEdit('moderate')" class="CommonTextButton"><cp:ResourceControl runat = "Server" resourceName = "FeedbackEditor_UnPublishSelected" ID="Resourcecontrol4"/></a>
		<a href="javascript:if(confirm('<%= CommunityServer.Components.ResourceManager.GetString("FeedbackEditor_BulkDeleteSelected") %>')){bulkEdit('delete');}" class="CommonTextButton"><cp:ResourceControl runat = "Server" resourceName = "FeedbackEditor_DeleteSelected" ID="Resourcecontrol5"/></a>
	</td>
</tr>
</table>			
</div>

<div class="CommonListArea">
<asp:Repeater runat = "Server" id = "theComments">

<HeaderTemplate>
	<table id="CommentListing" cellSpacing="0" cellPadding="0" border="0" width="100%">
	<thead>
		<tr>
			<th class="CommonListHeaderLeftMost"><input type="checkbox" name="master" id="master" onclick="checkAll(this,'CommentListing');" /></th>
			<th class="CommonListHeader"><cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Title"/></th>
			<th class="CommonListHeader"><cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_PublishedDate" /></th>
			<th class="CommonListHeader">&nbsp;</th>
			<th class="CommonListHeader">&nbsp;</th>											
		</tr>
	</thead>
</HeaderTemplate>
<ItemTemplate>
	<tr>
	<td class="CommonListCellLeftMost"><input type ="checkbox" name = "cb-<%# DataBinder.Eval(Container.DataItem,"PostID") %>" /></td>
	<td class="CommonListCell" style="max-width:200px">
		<a href="javascript:Telligent_Modal.Open('commenteditor.aspx?cid=<%# DataBinder.Eval(Container.DataItem,"PostID") %>', 625, 475, refreshCallback);">
	    <strong><%# DataBinder.Eval(Container.DataItem,"Subject")%></strong></a><br /> <%# DataBinder.Eval(Container.DataItem,"ForceExcerpt") %>
	</td>
	<td class="CommonListCell" >
		<CP:ResourceControl runat="Server" resourcename="CP_By" />: <strong><%# Clean(DataBinder.Eval(Container.DataItem,"DisplayName")) %></strong><br/>
		<%# UserTime.ConvertToUserTime((DateTime)DataBinder.Eval(Container.DataItem, "PostDate")).ToString(CSContext.Current.User.Profile.DateFormat + " hh:mm tt")%>
	</td>
	<td class="CommonListCell" align="center">
		<img id = "status-image-<%# DataBinder.Eval(Container.DataItem,"PostID") %>"  src="<%=SiteUrls.Instance().Locations["ControlPanel"]%>images/<%# DataBinder.Eval(Container.DataItem,"IsApproved").ToString() %>.gif" width="16" height="15" />
	</td>
	<td  class="CommonListCell" width="200px">
		<CPB:CommentActionsPopup visible="false" runat = "Server" id = "actionMenu" ActiveButtonCssClass="CommonImageTextButtonHighlight" InactiveButtonCssClass="CommonImageTextButton" InnerButtonCssClass="CommonButtonInnerContentDown">
		    <SkinTemplate>
                <TWC:PopupMenu runat="server"
	                id="Menu"
	                MenuGroupCssClass="CommonContextMenuGroup"
	                MenuItemCssClass="CommonContextMenuItem"
	                MenuItemSelectedCssClass="CommonContextMenuItemHover"
	                MenuItemExpandedCssClass="CommonContextMenuItemExpanded"
	                MenuItemIconWidth="20"
	                MenuItemIconHeight="20"
	                MenuDirection="UpDown" />
		    </SkinTemplate>
		</CPB:CommentActionsPopup>
		<a id = "button-<%# DataBinder.Eval(Container.DataItem,"PostID") %>" href="javascript:updatePublishStatus(<%# DataBinder.Eval(Container.DataItem,"PostID") %>,<%# DataBinder.Eval(Container.DataItem,"IsApproved").ToString().ToLower() %>)" class="CommonTextButton"><%# !(bool)DataBinder.Eval(Container.DataItem,"IsApproved") ? ResourceManager.GetString("FeedbackEditor_Save") : ResourceManager.GetString("FeedbackEditor_NotSaved") %></a>
		<a onclick="return checkIsPublished(<%# DataBinder.Eval(Container.DataItem,"PostID") %>,<%# DataBinder.Eval(Container.DataItem,"IsApproved").ToString().ToLower() %>);" href="<%# DataBinder.Eval(Container.DataItem, "ViewPostURL")%>" class="CommonTextButton"><CP:ResourceControl runat="Server" resourcename="CP_Blog_GridCol_View" /></a>
		<a onclick="if(confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Blog_Comments_Delete_Warning") %>')){this.href='<%# GetActionUrl("delete",DataBinder.Eval(Container.DataItem, "PostID"))%>';}" href="#" class="CommonTextButton"><CP:ResourceControl runat="Server" resourcename="CP_Blog_GridCol_Delete" /></a>
	</td>
	</tr>
</ItemTemplate>
<AlternatingItemTemplate>
	<tr class="AltListRow">
	<td class="CommonListCellLeftMost"><input type ="checkbox" name = "cb-<%# DataBinder.Eval(Container.DataItem,"PostID") %>" /></td>
	<td class="CommonListCell" style="max-width:200px">
	<a href="javascript:Telligent_Modal.Open('commenteditor.aspx?cid=<%# DataBinder.Eval(Container.DataItem,"PostID") %>', 625, 475, refreshCallback);">
	<strong><%# DataBinder.Eval(Container.DataItem,"Subject")%></strong></a><br /> <%# DataBinder.Eval(Container.DataItem,"ForceExcerpt") %>
	</td>
	<td class="CommonListCell" >
		<CP:ResourceControl runat="Server" resourcename="CP_By" />: <strong><%# Clean(DataBinder.Eval(Container.DataItem,"DisplayName")) %></strong><br/>
		<%# DataBinder.Eval(Container.DataItem, "PostDate", "{0:" + CSContext.Current.User.Profile.DateFormat + ", hh:mm tt}")%>
	</td>
	<td class="CommonListCell" align="center">
		<img id = "status-image-<%# DataBinder.Eval(Container.DataItem,"PostID") %>" src="<%=SiteUrls.Instance().Locations["ControlPanel"]%>images/<%# DataBinder.Eval(Container.DataItem,"IsApproved").ToString() %>.gif" width="16" height="15" />
	</td>
	<td  class="CommonListCell" width="200px">
		<CPB:CommentActionsPopup visible="false" runat = "Server" id = "actionMenu" ActiveButtonCssClass="CommonImageTextButtonHighlight" InactiveButtonCssClass="CommonImageTextButton" InnerButtonCssClass="CommonButtonInnerContentDown">
		    <SkinTemplate>
                <TWC:PopupMenu runat="server"
	                id="Menu"
	                MenuGroupCssClass="CommonContextMenuGroup"
	                MenuItemCssClass="CommonContextMenuItem"
	                MenuItemSelectedCssClass="CommonContextMenuItemHover"
	                MenuItemExpandedCssClass="CommonContextMenuItemExpanded"
	                MenuItemIconWidth="20"
	                MenuItemIconHeight="20"
	                MenuDirection="UpDown" />   
		    </SkinTemplate>
		</CPB:CommentActionsPopup>
		<a id = "button-<%# DataBinder.Eval(Container.DataItem,"PostID") %>" href="javascript:updatePublishStatus(<%# DataBinder.Eval(Container.DataItem,"PostID") %>,<%# DataBinder.Eval(Container.DataItem,"IsApproved").ToString().ToLower() %>)" class="CommonTextButton"><%# !(bool)DataBinder.Eval(Container.DataItem,"IsApproved") ? ResourceManager.GetString("FeedbackEditor_Save") : ResourceManager.GetString("FeedbackEditor_NotSaved") %></a>   
		<a onclick="return checkIsPublished(<%# DataBinder.Eval(Container.DataItem,"PostID") %>,<%# DataBinder.Eval(Container.DataItem,"IsApproved").ToString().ToLower() %>);" href="<%# DataBinder.Eval(Container.DataItem, "ViewPostURL")%>" class="CommonTextButton"><CP:ResourceControl runat="Server" resourcename="CP_Blog_GridCol_View" /></a>
		<a onclick="if(confirm('<%= CommunityServer.ControlPanel.Components.ResourceManager.GetString("CP_Blog_Comments_Delete_Warning") %>')){this.href='<%# GetActionUrl("delete",DataBinder.Eval(Container.DataItem, "PostID"))%>';}" href="#" class="CommonTextButton"><CP:ResourceControl runat="Server" resourcename="CP_Blog_GridCol_Delete" /></a>
	</td>
	</tr>
</AlternatingItemTemplate>			
<FooterTemplate>
	</table>
</FooterTemplate>
</asp:Repeater>

</div>

<table border = "0" cellpadding="0" cellspacing="0" width="100%">
<tr>
	<td align="left">
		<cp:Pager runat = "Server" id = "csPager" MessageText = "More Comments" />        
	</td>
	<td align="right">
		<asp:literal id="CommentRSS_Message" runat="server" />
	</td>
</tr>
</table>
</CP:Content>
</CP:Container>

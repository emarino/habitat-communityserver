<%@ Page language="c#" Codebehind="DescOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.DescriptiveOptionsPage" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="TitleDescriptionAndNews" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="BlogControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
	<cp:resourcecontrol id="Resourcecontrol1" runat="server" resourcename="CP_Blogs_DescOptions_Title" />
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">
	<cp:configokstatusmessage id="Status" runat="server" visible="false" />
	<div class="CommonDescription">
		<cp:resourcecontrol runat="server" resourcename="CP_Blogs_DescOptions_SubTitle" />
	</div>
	<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Blogs" />
	<div class="CommonFormArea">
	<div class="FixedWidthContainer">
		<div class="CommonFormFieldName">
			<cp:helpicon runat="server" resourcename="CP_Blogs_DescOptions_BlogTitle_Help" />
			<CP:FormLabel runat="server" resourcename="CP_Blogs_DescOptions_BlogTitle" ControlToLabel="Name" />
			<asp:RequiredFieldValidator id="TitleValidator" ControlToValidate="Name" Display="Dynamic" Runat="server" />
		</div>
		<div class="CommonFormField">
			<asp:TextBox id="Name" Runat="Server" CssClass="ControlPanelTextInput" />
		</div>
		<div class="CommonFormFieldName">
			<cp:helpicon runat="server" resourcename="CP_Blogs_DescOptions_Description_Help" />
			<CP:FormLabel runat="server" resourcename="CP_Blogs_DescOptions_Description" ControlToLabel="Description" />
		</div>
		<div class="CommonFormField">
			<asp:TextBox id="Description" Runat="Server" CssClass="ControlPanelTextInput" TextMode="MultiLine" rows="3" />
		</div>
		<div class="CommonFormFieldName">
			<cp:helpicon runat="server" resourcename="CP_Blogs_DescOptions_News_Help" />
			<CP:FormLabel runat="server" resourcename="CP_Blogs_DescOptions_News" ControlToLabel="News" />
		</div>
		<div class="CommonFormField">
			<asp:TextBox id="News" Runat="Server" CssClass="ControlPanelTextInput" TextMode="MultiLine" rows="5" />
		</div>
		<div class="CommonFormFieldName">
			<cp:helpicon runat="server" resourcename="CP_Blogs_DescOptions_MetaTagDescription_Help" />
			<cp:formlabel runat="server" resourcename="CP_Blogs_DescOptions_MetaTagDescription" controltolabel="MetaTagDescription" />
		</div>
		<div class="CommonFormField">
			<asp:textbox id="MetaTagDescription" runat="Server" maxlength="512" cssclass="ControlPanelTextInput" />
		</div>
		<div class="CommonFormFieldName">
			<cp:helpicon runat="server" resourcename="CP_Blogs_DescOptions_MetaTagKeyword_Help" />
			<cp:formlabel runat="server" resourcename="CP_Blogs_DescOptions_MetaTagKeyword" controltolabel="MetaTagKeywords" />
		</div>
		<div class="CommonFormField">
			<asp:textbox id="MetaTagKeywords" runat="Server" maxlength="512" cssclass="ControlPanelTextInput" />
		</div>

		<asp:PlaceHolder id="RawHTMLPanel" runat="Server">
		<div class="CommonFormFieldName">
			<cp:helpicon runat="server" resourcename="CP_Blogs_DescOptions_RawHTMLHeader_Help" />
			<cp:formlabel runat="server" resourcename="CP_Blogs_DescOptions_RawHTMLHeader" controltolabel="RawHTMLHeader" />
		</div>
		<div class="CommonFormField">
			<asp:textbox id="RawHTMLHeader" runat="Server" cssclass="ControlPanelTextInput" textmode="MultiLine" rows="5" />
		</div>
		</asp:PlaceHolder>
		
	</div>
	<p class="PanelSaveButton DetailsFixedWidth">
		<cp:ResourceLinkButton id="SaveButton" runat="Server" CssClass="CommonTextButtonBig" ResourceName="Save" />
	</p>
	</div>
</CP:Content>
</CP:Container>

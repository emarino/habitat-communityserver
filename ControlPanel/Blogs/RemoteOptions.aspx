<%@ Page language="c#" Codebehind="RemoteOptions.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.RemoteOptionsPage" %>
<cp:controlpanelselectednavigation selectednavitem="RemoteSettings" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" thememasterfile="BlogControlPanelMaster.ascx">
<CP:Content id="DescriptionRegion" runat="server">
	<cp:resourcecontrol id="Resourcecontrol1" runat="server" resourcename="CP_Blogs_RemoteOptions_Title" />
</CP:Content>
<CP:Content id="TaskRegion" runat="Server">
	<cp:configokstatusmessage id="Status" runat="server" visible="false" />
	<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Blogs" />
	<div class="CommonFormArea">
	<div class="FixedWidthContainer">
		<fieldset>
		<legend><cp:resourcecontrol runat="server" resourcename="CP_Blogs_RemoteOptions_SpamSettings" />&nbsp;</legend>
			<div class="CommonFormFieldName">
				<asp:checkbox id="OverrideSpamSetting" runat="Server" cssclass="ControlPanelTextInput" autopostback="true" />
				<cp:formlabel runat="Server" resourcename="CP_Blogs_RemoteOptions_OverrideSpamSettings" controltolabel="OverrideSpamSetting" />
            </div>
		    <asp:placeholder id="SpamSettingsPanel"  runat="Server">
			<div class="CommonFormFieldName">
			    <asp:literal id="CurrentSpamSettings" runat="Server" />
			</div>
			<div class="CommonFormFieldName">
				<cp:formlabel runat="Server" resourcename="CP_Blogs_RemoteOptions_AutoModerate" controltolabel="SpamAutoModerate" />
				<asp:textbox id="SpamAutoModerate" runat="Server" width="50"  /><asp:customvalidator id="sam_valide" controltovalidate="SpamAutoModerate" display="dynamic" errormessage="*" font-bold="True" onservervalidate="ServerValidation" runat="server"/>
			</div>
			<div class="CommonFormFieldName">
				<cp:formlabel runat="Server" resourcename="CP_Blogs_RemoteOptions_AutoDelete" controltolabel="SpamAutoDelete" />
				<asp:textbox id="SpamAutoDelete" runat="Server" width="50"  /><asp:customvalidator id="sad_valide" controltovalidate="SpamAutoDelete" display="dynamic" errormessage="*" font-bold="True" onservervalidate="ServerValidation" runat="server"/>
			</div>
			</asp:placeholder>
		</fieldset>		
		<br />

		<fieldset>
		<legend><cp:resourcecontrol runat="server" resourcename="CP_Blogs_RemoteOptions_PingSettings" />&nbsp;</legend>
			<div class="CommonFormFieldName">
				<asp:checkbox id="OverridePingServices" runat="Server" cssclass="ControlPanelTextInput" autopostback="true" />
				<cp:formlabel runat="Server" resourcename="CP_Blogs_RemoteOptions_OverridePingServices" controltolabel="OverridePingServices" />
			</div>
			<asp:placeholder id="PingServicesPanel"  runat="Server">
			<div class="CommonFormFieldName">
				<cp:resourcecontrol runat="server" resourcename="CP_Blogs_RemoteOptions_CurrentSettings" /><br />
    			<asp:literal id="CurrentPingServices" runat="Server" />
			</div>
			<div class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_Blogs_RemoteOptions_PingServices_Help" />
				<cp:formlabel runat="Server" resourcename="CP_Blogs_RemoteOptions_PingServices" controltolabel="PingServices" />
			</div>
			<div class="CommonFormField">
				<asp:textbox id="PingServices" runat="Server" cssclass="ControlPanelTextInput" />
			</div>
			</asp:placeholder>
		</fieldset>		
		<br />

		<fieldset>
		<legend><cp:resourcecontrol runat="server" resourcename="CP_Blogs_RemoteOptions_CrossPosting" />&nbsp;</legend>
			<asp:repeater id="SettingsList" runat="server">
			<headertemplate>
				<table width="100%" cellspacing="0" cellpadding="3" border="0" style="border: 1px solid #999999;">
					<tr>
						<th><cp:resourcecontrol runat="Server" resourcename="CP_Blogs_RemoteOptions_Url" /></th>
						<th><cp:resourcecontrol runat="Server" resourcename="CP_Blogs_RemoteOptions_AppKey" /></th>
						<th><cp:resourcecontrol runat="Server" resourcename="CP_Blogs_RemoteOptions_Username" /></th>
					</tr>
				</headertemplate>
				<itemtemplate>
					<tr>
						<td><asp:literal id="ID" runat="Server" visible="false" /><asp:literal id="Url" runat="Server" /></td>
						<td><asp:literal id="AppKey" runat="Server" /></td>
						<td><asp:literal id="Username" runat="Server" /></td>
						<td><cp:hyperlink id="Edit" runat="Server" resourcename="Edit" /> | <cp:hyperlink id="Delete" runat="Server" resourcename="Delete" /></td>
					</tr>
				</itemtemplate>
				<footertemplate>
				</table>
			</footertemplate>
			</asp:repeater>
			<cp:resourcecontrol id="CPDisabledMessage" runat="Server" resourcename="CP_Blogs_RemoteOptions_CrossPostingDisabled" visible="false" />
			<p class="PanelSaveButton DetailsFixedWidth">
				<cp:hyperlink id="NewButton" runat="Server" resourcename="CP_Blogs_RemoteOptions_NewSetting" cssclass="CommonTextButton" />
			</p>
		</fieldset>		
		</div>

	</div>
	<p class="PanelSaveButton DetailsFixedWidth">
		<cp:resourcelinkbutton id="SaveButton" runat="Server" resourcename="Save" cssclass="CommonTextButtonBig" />
	</p>
	</div>
</CP:Content>
</CP:Container>

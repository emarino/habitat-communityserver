<%@ Page language="c#" Codebehind="CategoryForm.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.CategoryForm" %>
<%@ Import Namespace="CommunityServer.Components" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelModalMaster.ascx" IsModal="true">
	<CP:Content id="bcr" runat="server">
		<script type="text/javascript">
		function closeModal()
		{
			window.parent.Telligent_Modal.Close(true);
		}
		</script>
		<div class="CommonContentArea">
			<div class="CommonContent">
				<p>
				<div class="CommonFormFieldName">
					<CP:FormLabel id="tt" runat="Server" ResourceName="CP_Blog_CategoryForm_Name" ControlToLabel="CategoryName"></CP:FormLabel>
					<asp:RequiredFieldValidator id="RequiredFieldValidator1" Text=" Please include a name" Display="Dynamic" ControlToValidate="CategoryName"
						Runat="server"></asp:RequiredFieldValidator></div>
				<div class="CommonFormField">
					<asp:TextBox id="CategoryName" Runat="server" MaxLength="256" CssClass="ControlPanelTextInput"></asp:TextBox></div>
				</p>
				<p>
					<div class="CommonFormFieldName">
						<CP:FormLabel id="Formlabel1" runat="Server" ResourceName="CP_Blog_CategoryForm_Description" ControlToLabel="CategoryDesc"></CP:FormLabel></div>
					<div class="CommonFormField">
						<asp:TextBox id="CategoryDesc" Runat="server" MaxLength="256" CssClass="ControlPanelTextInput"
							TextMode="MultiLine"></asp:TextBox></div></p>
				<p>
					<table cellSpacing="0" cellPadding="0" border="0">
						<tr>
							<td class="CommonFormFieldName">
								<CP:FormLabel id="Formlabel2" runat="Server" ResourceName="CP_Blog_CategoryForm_Enabled" ControlToLabel="CategoryEnabled"></CP:FormLabel></td>
							<td class="CommonFormField">
								<asp:CheckBox id="CategoryEnabled" Runat="server"></asp:CheckBox></td>
						</tr>
					</table></p>
				<p>
					<div class="CommonFormField PanelSaveButton">
						<CP:ResourceLinkButton id="Button1" ResourceName="Save" Runat="server"  CssClass="CommonTextButton"></CP:ResourceLinkButton></div></p>
			</DIV>
		</DIV>
	</CP:Content>
</CP:Container>

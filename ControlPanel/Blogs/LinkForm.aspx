<%@ Page language="c#" Codebehind="LinkForm.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.LinkForm" %>
<%@ Import Namespace="CommunityServer.Components" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelModalMaster.ascx" IsModal="true">
	<CP:Content id="bcr" runat="server">
		<script type="text/javascript">
		// <![CDATA[
	function closeModal()
	{
		window.parent.Telligent_Modal.Close(true);
	}
	
	function insertRel()
	{
		var txt = document.getElementById('<%=LinkRel.ClientID%>');
		var sel = document.getElementById('<%=RelQuickList.ClientID%>');
		
		relText = txt.value;
		if (relText.length > 0)
		{
			relText += " ";
		}
		txt.value = relText + sel[sel.selectedIndex].value;
		sel.selectedIndex = 0;
	}
	    // ]]>
		</script>
		<DIV class="CommonContentArea">
			<DIV class="CommonContent">
				<DIV class="CommonFormFieldName">
					<CP:FormLabel id="tt" runat="Server" ControlToLabel="LinkTitle" resourcename="CP_Blog_LinkForm_LinkTitle"></CP:FormLabel>
					&nbsp;<asp:RequiredFieldValidator id="LinkTitleValidator" Runat="server" ControlToValidate="LinkTitle" Display="Dynamic"
						Text="*"></asp:RequiredFieldValidator></DIV>
				<DIV class="CommonFormField">
					<asp:TextBox id="LinkTitle" Runat="server" CssClass="ControlPanelTextInput" MaxLength="256" width="350px"></asp:TextBox></DIV>
				<DIV class="CommonFormFieldName">
					<CP:FormLabel id="FormLabel1" runat="server" ControlToLabel="LinkUrl" resourcename="CP_Blog_LinkForm_Url"></CP:FormLabel>
					&nbsp;<asp:RequiredFieldValidator id="LinkUrlValidator" Runat="server" ControlToValidate="LinkUrl" Display="Dynamic"
						Text="*"></asp:RequiredFieldValidator></DIV>
				<DIV class="CommonFormField">
					<asp:TextBox id="LinkUrl" Runat="server" CssClass="ControlPanelTextInput" MaxLength="256" width="350px"></asp:TextBox></DIV>
				<DIV class="CommonFormFieldName">
					<cp:formlabel id="Formlabel3" runat="server" resourcename="CP_Blog_LinkForm_Description" controltolabel="LinkDesc"></cp:formlabel></DIV>
				<DIV class="CommonFormField">
					<asp:textbox id="LinkDesc" runat="server" cssclass="ControlPanelTextInput" maxlength="2000" width="350px"></asp:textbox></DIV>
				<DIV class="CommonFormFieldName">
					<cp:helpicon id="Helpicon1" runat="Server" resourcename="CP_Blog_LinkForm_Relations_Desc"></cp:helpicon>
					<cp:formlabel id="Formlabel4" runat="server" resourcename="CP_Blog_LinkForm_Relations" controltolabel="LinkRel"></cp:formlabel></DIV>
				<DIV class="CommonFormField">
					<asp:textbox id="LinkRel" runat="server" cssclass="ControlPanelTextInput" maxlength="100" width="150"></asp:textbox>&nbsp;
					<asp:dropdownlist id="RelQuickList" runat="server" enableviewstate="false"></asp:dropdownlist></DIV>
				<DIV class="CommonFormFieldName">
					<asp:checkbox id="CategoryEnabled" runat="server"></asp:checkbox>
					<CP:FormLabel id="Formlabel2" runat="Server" ControlToLabel="CategoryEnabled" resourcename="CP_Blog_LinkForm_Enabled"></CP:FormLabel></DIV>
				<DIV class="CommonFormField PanelSaveButton">
					<cp:ResourceLinkButton id="SaveButton" resourcename="Save" Runat="server" CssClass="CommonTextButton" />
				</DIV>
			</DIV>
		</DIV>
	</CP:Content>
</CP:Container>

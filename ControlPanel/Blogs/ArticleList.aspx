<%@ Page language="c#" Codebehind="ArticleList.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.ArticleListPage" %>
<%@ Register TagPrefix="BlogPanel" TagName = "PostList" Src = "~/ControlPanel/Blogs/PostListControl.ascx" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="Articles" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="BlogControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl id="Resourcecontrol1" runat="server" ResourceName="CP_Blog_ArticleList"></CP:ResourceControl>
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<CP:ResourceControl id="Resourcecontrol2" runat="server" ResourceName="CP_Blog_ArticleList_SubTitle"></CP:ResourceControl></DIV>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Blogs" />
		<BlogPanel:PostList id="PostList1" runat="server" BlogPostType="Article"></BlogPanel:PostList>
	</CP:Content>
</CP:Container>

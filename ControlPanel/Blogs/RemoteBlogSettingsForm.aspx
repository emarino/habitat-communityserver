<%@ Page language="c#" Codebehind="RemoteMetaBlogSettingsForm.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.RemoteMetaBlogSettingsForm" %>
<CP:Container runat="server" id="MPContainer" thememasterfile="ControlPanelModalMaster.ascx">
	<CP:Content id="bcr" runat="Server">
		<div class="CommonFormArea">
			<h3 class="CommonSubTitle"><cp:resourcecontrol runat="Server" resourcename="CP_Blogs_RemoteOptions_RemoteMetaWeblogTitle" /></h3>
		
			<div class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_Blogs_RemoteOptions_Url_Help" />
				<cp:formlabel runat="Server" controltolabel="Url" resourcename="CP_Blogs_RemoteOptions_Url" />&nbsp;
				<asp:textbox id="Url" cssclass="ControlPanelTextInput" runat="server" />
			</div>

			<div class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_Blogs_RemoteOptions_AppKey_Help" />
				<cp:formlabel runat="Server" controltolabel="AppKey" resourcename="CP_Blogs_RemoteOptions_AppKey" />&nbsp;
				<asp:textbox id="AppKey" cssclass="ControlPanelTextInput" runat="server" />
			</div>

			<div class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_Blogs_RemoteOptions_Username_Help" />
				<cp:formlabel runat="Server" controltolabel="Username" resourcename="CP_Blogs_RemoteOptions_Username" />&nbsp;
				<asp:textbox id="Username" cssclass="ControlPanelTextInput" runat="server" />
			</div>

			<div class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_Blogs_RemoteOptions_Password_Help" />
				<cp:formlabel runat="Server" controltolabel="Password" resourcename="CP_Blogs_RemoteOptions_Password" />&nbsp;
				<asp:textbox id="Password" cssclass="ControlPanelTextInput" runat="server" />
			</div>

			<div class="CommonFormFieldName">
				<cp:helpicon runat="Server" resourcename="CP_Blogs_RemoteOptions_Message_Help" />
				<cp:formlabel runat="Server" controltolabel="Message" resourcename="CP_Blogs_RemoteOptions_Message" />&nbsp;
				<asp:textbox id="AppendMessage" cssclass="ControlPanelTextInput" runat="server" />
			</div>

			<p class="CommonFormField PanelSaveButton">
				<cp:resourcelinkbutton id="SaveButton" runat="server" cssclass="CommonTextButton" resourcename="Save" />
			</p>
		
		</div>
	</CP:Content>
</CP:Container>

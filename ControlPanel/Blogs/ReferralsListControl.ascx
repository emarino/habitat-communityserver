<%@ Control Language="c#" AutoEventWireup="false" Codebehind="ReferralsListControl.ascx.cs" Inherits="CommunityServer.ControlPanel.Blogs.ReferralsListControl" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>
<%@ Import Namespace="CommunityServer.Components" %>
<DIV id="filters" style="TEXT-ALIGN: right">
	<P>
		<cp:resourcelabel id="Resourcelabel1" runat="server" resourcename="CP_Blogs_Referrals_Post"></cp:resourcelabel>
		<asp:dropdownlist id="filterPost" runat="server" AutoPostBack="True"></asp:dropdownlist>
	</P>
</DIV>

<div class="CommonListArea">
	<asp:Repeater Runat="server" ID="referrals">
		<HeaderTemplate>
			<table id="ReferralListing" border="0" cellspacing="0" cellpadding="0" width="100%">
				<thead>
					<tr>
						<th class="CommonListHeaderLeftMost">
							<cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_URL" />
						</th>
						<th class="CommonListHeader">
							<cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Title" />
						</th>
						<th class="CommonListHeader">
							<cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Hits" />
						</th>
						<th class="CommonListHeader">
							<cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_LastDate" />
						</th>
						<th class="CommonListHeader">
							<cp:resourcecontrol runat="server" resourcename="CP_Blog_GridCol_Actions" />
						</th>
					</tr>
				</thead>
		</HeaderTemplate>

		<ItemTemplate>
			<tr>
				<td class="CommonListCellLeftMost">
					<a href='<%# DataBinder.Eval(Container.DataItem, "Url") %>'>
						<%# Formatter.Truncate(DataBinder.Eval(Container.DataItem, "Url").ToString(), 50, true) %>
					</a>
				</td>
				<td class="CommonListCell">
					<a href='<%# DataBinder.Eval(Container.DataItem, "ViewPostUrl") %>'>
						<%# Formatter.Truncate(DataBinder.Eval(Container.DataItem, "Title").ToString(), 50, true) %>
					</a>
				</td>
				<td class="CommonListCell">
					<%# DataBinder.Eval(Container.DataItem, "Hits") %>
				</td>
				<td class="CommonListCell">
					<%# ((DateTime)DataBinder.Eval(Container.DataItem, "LastDate")).ToString("MMM dd yyyy, hh:mm tt") %>
				</td>
				<td class="CommonListCell">
					<a href='javascript:Telligent_Modal.Open("BlogCommand.aspx?Command=CommunityServer.ControlPanel.Blogs.ConvertReferralToTrackbackCommand,CommunityServer.Web&PostID=<%# DataBinder.Eval(Container.DataItem, "PostID")%>&ReferralID=<%# DataBinder.Eval(Container.DataItem, "ReferralID")%>", 400, 200, null);' class="CommonTextButton">Create Trackback</a>
				</td>
			</tr>
		</ItemTemplate>
            <AlternatingItemTemplate>
            <tr class="AltListRow">
				<td class="CommonListCellLeftMost">
					<a href='<%# DataBinder.Eval(Container.DataItem, "Url") %>'>
						<%# Formatter.Truncate(DataBinder.Eval(Container.DataItem, "Url").ToString(), 50, true) %>
					</a>
				</td>
				<td class="CommonListCell">
					<a href='<%# DataBinder.Eval(Container.DataItem, "ViewPostUrl") %>'>
						<%# Formatter.Truncate(DataBinder.Eval(Container.DataItem, "Title").ToString(), 50, true) %>
					</a>
				</td>
				<td class="CommonListCell">
					<%# DataBinder.Eval(Container.DataItem, "Hits") %>
				</td>
				<td class="CommonListCell">
					<%# ((DateTime)DataBinder.Eval(Container.DataItem, "LastDate")).ToString("MMM dd yyyy, hh:mm tt") %>
				</td>
				<td class="CommonListCell">
					<a href='javascript:Telligent_Modal.Open("BlogCommand.aspx?Command=CommunityServer.ControlPanel.Blogs.ConvertReferralToTrackbackCommand,CommunityServer.Web&PostID=<%# DataBinder.Eval(Container.DataItem, "PostID")%>&ReferralID=<%# DataBinder.Eval(Container.DataItem, "ReferralID")%>", 400, 200, null);' class="CommonTextButton">Create Trackback</a>
				</td>
			</tr>
		</AlternatingItemTemplate>		

		<FooterTemplate>
			</table>
		</FooterTemplate>
	</asp:Repeater>

	<cp:Pager Runat="Server" ID="pager" />
</div>

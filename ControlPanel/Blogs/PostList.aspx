<%@ Page language="c#"  Codebehind="PostList.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.PostListPage" %>
<%@ Register TagPrefix="BlogPanel" TagName = "PostList" Src = "~/ControlPanel/Blogs/PostListControl.ascx" %>
<%@ Import Namespace = "CommunityServer.Components" %>
<%@ Import Namespace = "CommunityServer.ControlPanel" %>
<CP:ControlPanelSelectedNavigation SelectedNavItem="Posts" runat="server" id="SelectedNavigation1" />
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="BlogControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">
		<CP:ResourceControl runat="server" ResourceName="CP_Blog_PostList" />
	</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<DIV class="CommonDescription">
			<CP:ResourceControl runat="server" ResourceName="CP_Blog_PostList_SubTitle" />
		</DIV>
		<CP:LicenseCheck runat="server" CssClass="CommonLicenseMessageError" UsageType="Blogs" />
		<BlogPanel:PostList id="PostList1" runat="server" />
	</CP:Content>
</CP:Container>

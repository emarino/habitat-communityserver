<%@ Page language="c#" Codebehind="BlogCommand.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel.Blogs.BlogCommandPage" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelModalMaster.ascx" IsModal="true">
	<CP:Content id="bcr" runat="server">
		<DIV class="CommonContentArea">
			<DIV class="CommonContent">
				<asp:Literal id="CommandMessage" Text="Message" Runat="server"></asp:Literal>
				<P>
					<DIV align="center">
						<asp:Button id="OK_Button" Text="OK" Runat="server"></asp:Button><INPUT id="Cancel_Button" onclick="window.parent.Telligent_Modal.Close(false); return false;" type="button"
							value="Cancel" runat="server">
					</DIV>
			</DIV>
		</DIV>
	</CP:Content>
</CP:Container>

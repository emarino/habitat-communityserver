<%@ Page language="c#" Codebehind="license.aspx.cs" AutoEventWireup="false" Inherits="CommunityServer.ControlPanel._Default2" %>
<CP:Container runat="server" id="MPContainer" ThemeMasterFile="ControlPanelMaster.ascx">
	<CP:Content id="DescriptionRegion" runat="server">Add New License Page</CP:Content>
	<CP:Content id="TaskRegion" runat="Server">
		<asp:Textbox id="LicenseText" runat="server" rows="10" cols="80" TextMode="MultiLine"></asp:Textbox>
		<BR>
		<asp:Button id="UpdateButton" runat="server" text="Update"></asp:Button>
		<BR>
		<asp:Label id="Status" runat="server"></asp:Label>
	</CP:Content>
</CP:Container>

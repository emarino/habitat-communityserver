using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.ComponentModel;
using CommunityServer;
using CommunityServer.Components;
using PartnerNetEncryptDecrypt;
using PartnerNetErrorHandler;
using PartnernetGlobalFunctions;

public partial class Diagnostics : CommunityServer.Components.CSPage
{
	UserQuery objQuery;

	protected void SearchUser(object sender, EventArgs e)
	{
		listUsers.Visible = false;
		if (sender != null)
			litUserResultStatus.Text = "";

		objQuery = new UserQuery();
		objQuery.Status = UserAccountStatus.Approved;
		objQuery.Order = SortOrder.Ascending;
		objQuery.SortBy = SortUsersBy.Username;
		objQuery.IncludeHiddenUsers = false;
		objQuery.PageSize = 50;
		objQuery.SearchText = "%" + txtUsername.Text + "%";

		objQuery.PageIndex = 0;
		UserSet userset = Users.GetUsers(objQuery, false);

		if (userset.Users.Count < userset.TotalRecords)
			litUserResultStatus.Text += "<font color=brown>Showing " + userset.Users.Count.ToString() + " of " + userset.TotalRecords.ToString() + " (Tip: Try modifying the search text to narrow results)</font>";
		if (userset.TotalRecords == 0)
			litUserResultStatus.Text = "<font color=red>No Users found for the serach criteria</font>";
		else
			listUsers.Visible = true;

		listUsers.Items.Clear();
		foreach (User user in userset.Users)
			listUsers.Items.Add(new ListItem(user.Username + "(" + user.Email + ")", user.Username));


	}

	protected void GetSelectedUserDetails(object sender, EventArgs e)
	{
		try
		{
			User user = Users.GetUser(listUsers.SelectedItem.Value);
			txtNewEmail.Text = user.Email;
			//litUserResultStatus.Text = "<b>User Details:</b> <br/> <div style='padding-left:10px'>";
			//litUserResultStatus.Text += "User Name:" + user.Username + "<br/>";
			//litUserResultStatus.Text += "Email:" + user.Email + "<br/>";
			//litUserResultStatus.Text += "</div>";

			litRoles.Text = "";
			string[] UserRoles = CommunityServer.Components.Roles.GetUserRoleNames(listUsers.SelectedItem.Value, false);
			litRoles.Text += "<b>Roles for " + listUsers.SelectedItem.Value + ": </b><br/>  <div style='padding-left:10px'>";
			foreach (string strRole in UserRoles)
				litRoles.Text += strRole + "<br/>";
			litRoles.Text += "</div>";
		}
		catch (Exception ex)
		{
			litUserResultStatus.Text = ex.ToString();
		}
	}

	protected void UpdateEmailAddress(object sender, EventArgs e)
	{
		try
		{
			User user = Users.GetUser(listUsers.SelectedItem.Value);
			user.Email = txtNewEmail.Text;
			if (CommunityServer.Users.UpdateUser(user))
				litUserResultStatus.Text = "Email Updated";
			else
				litUserResultStatus.Text = "Not Updated";

			GetSelectedUserDetails(null, null);
			SearchUser(null, null);
		}
		catch (Exception ex)
		{
			litUserResultStatus.Text = ex.ToString();
		}
	}

	protected void AddToRole(object sender, EventArgs e)
	{
		CommunityServer.Components.Roles.AddUserToRole(listUsers.SelectedItem.Value, txtRoleName.Text, true);
		GetSelectedUserDetails(null, null);
	}

	protected void RemoveFromRole(object sender, EventArgs e)
	{
		try
		{
			CommunityServer.Components.Roles.RemoveUserFromRole(listUsers.SelectedItem.Value, txtRoleName.Text, true);
			GetSelectedUserDetails(null, null);
		}
		catch (Exception ex)
		{
			litUserResultStatus.Text = ex.ToString();
		}
	}

	protected void GetAllRoles()
	{
		try
		{
			string[] AllRoles = CommunityServer.Components.Roles.GetRoleNames();
			litAllRoles.Text = "<b>TIP:</b> These are the valid role names:<br/>";

			foreach (string strRole in AllRoles)
			{
				litAllRoles.Text += strRole + "<br/>";
			}
		}
		catch (Exception ex)
		{
			litUserResultStatus.Text = ex.ToString();
		}
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		try
		{
			if (IsPostBack)
				return;

			GetAllRoles();

			if (Request.QueryString["Enc"] != null)
			{
				string SymmetricKey = GlobalFunctions.GetPartnerNetConfig("SymmetricKey");
				string strDecrypted = EncryptDecrypt.DecryptString(Request.QueryString["Enc"].ToString(), SymmetricKey);

				litUserResultStatus.Text += "Decrypted value = <b>" + strDecrypted + "</b> <br/>";

			}

			if (Request.QueryString["Email"] != null && Request.QueryString["Username"] != null)
			{
				string SymmetricKey = GlobalFunctions.GetPartnerNetConfig("SymmetricKey");
				string strDecryptedEmail = EncryptDecrypt.DecryptString(Request.QueryString["Email"].ToString(), SymmetricKey);
				string strDecryptedUsername = EncryptDecrypt.DecryptString(Request.QueryString["Username"].ToString(), SymmetricKey);

				User existing = GetUserByEmail(strDecryptedEmail);

				if (existing.Username.ToUpper() != strDecryptedUsername.ToUpper())
				{
					litUserResultStatus.Text += "Decrypted Username = <b>" + strDecryptedUsername + "</b> <br/>";
					litUserResultStatus.Text += "Decrypted Email = <b>" + strDecryptedEmail + "</b> <br/>";
					litUserResultStatus.Text += "<b><font color = red >But this email already exists in the system for the user: " + existing.Username + "</font></b></br>";
					return;
				}
			}

			if (Request.QueryString["Username"] != null)
			{
				string SymmetricKey = GlobalFunctions.GetPartnerNetConfig("SymmetricKey");
				string strDecrypted = EncryptDecrypt.DecryptString(Request.QueryString["Username"].ToString(), SymmetricKey);

				litUserResultStatus.Text += "Decrypted Username = <b>" + strDecrypted + "</b> <br/>";
			}

			if (Request.QueryString["Email"] != null)
			{
				string SymmetricKey = GlobalFunctions.GetPartnerNetConfig("SymmetricKey");
				string strDecrypted = EncryptDecrypt.DecryptString(Request.QueryString["Email"].ToString(), SymmetricKey);

				litUserResultStatus.Text += "Decrypted Email = <b>" + strDecrypted + "</b> <br/>";
			}
		}
		catch (Exception ex)
		{
			litUserResultStatus.Text = ex.ToString();
		}
	}

	protected User  GetUserByEmail(string Email)
	{
		try
		{
			objQuery = new UserQuery();
			objQuery.Status = UserAccountStatus.Approved;
			objQuery.Order = SortOrder.Ascending;
			objQuery.SortBy = SortUsersBy.Username;
			objQuery.IncludeHiddenUsers = false;
			objQuery.PageSize = 50;
			objQuery.SearchText = Email;

			objQuery.PageIndex = 0;
			UserSet userset = Users.GetUsers(objQuery, false);

			return userset.Users[0];
		}
		catch (Exception ex)
		{
			return null;
		}
	}

}

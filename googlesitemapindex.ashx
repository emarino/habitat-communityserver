<%@ WebHandler Language="C#" class = "GoogleSiteMapIndexHandler"   %>

using System;
using System.Collections.Generic;
using CommunityServer.Blogs.Components;
using CommunityServer.Components;
using CommunityServer.Discussions.Components;
using CommunityServer.Files.Components;
using CommunityServer.Galleries.Components;
using CommunityServer.Urls;

    public class GoogleSiteMapIndexHandler : BaseSyndicationHandler
    {
		/// <summary>
		/// Appends http://Host:Port to all blog urls
		/// </summary>
		protected override string BaseUrl
		{
			get
			{
				return Globals.HostPath(Context.Request.Url);
			}
		}
		protected override Section GetCurrentSection
		{
			get{return null;}
		}

 
    	protected override int CacheTime
    	{
    		get { throw new NotImplementedException(); }
    	}

    	protected override string CacheKey
    	{
    		get { throw new NotImplementedException(); }
    	}

		protected override bool FeedIsCacheable
		{
			get { return false; }
		}


    	protected override CachedFeed BuildFeed()
        {
            BaseSyndicationWriter writer;
            List<GoogleSiteMapItem> items = new List<GoogleSiteMapItem>();
            UrlsData urldata = Globals.GetSiteUrls().UrlData;
    	    foreach (CSApplicationData data in ApplicationSet.Applications)
			{
				if(data.Enabled && data.IsSearchable)
				switch(data.ApplicationType)
				{
					case ApplicationType.Weblog:
						
						foreach(Weblog w in Weblogs.GetWeblogs(true,false,false))
						{
                            string loc = urldata.FormatUrl("googleSiteMap","blog","app",w.ApplicationKey);
							DateTime lastmod = w.MostRecentPostDate;
							if (w.IsActive && !w.IsPrivate && w.IsSearchable && w.PostCount > 0)
								items.Add(new GoogleSiteMapItem(loc,lastmod));
						}
						break;
					case ApplicationType.Forum:
                        foreach (Forum f in Forums.GetForums(false, true, false, false))
						{
                            string loc = urldata.FormatUrl("googleSiteMap","forum","sectionid",f.SectionID);
							DateTime lastmod = f.MostRecentPostDate;
							if(f.IsActive && !f.IsPrivate && f.IsSearchable && f.ForumType != ForumType.Deleted && f.ForumType != ForumType.Reporting && f.TotalPosts > 0)
								items.Add(new GoogleSiteMapItem(loc,lastmod));
						}
						break;
					case ApplicationType.Gallery:
                        foreach (Gallery g in CommunityServer.Galleries.Galleries.GetGalleries(CSContext.Current.UserID, false, true, false))
						{

                            string loc = urldata.FormatUrl("googleSiteMap","photo","app",g.ApplicationKey);
							DateTime lastmod = g.MostRecentPostDate;
							if(g.IsActive && !g.IsPrivate && g.IsSearchable && g.PostCount > 0)
								items.Add(new GoogleSiteMapItem(loc,lastmod));
						}
						break;
					case ApplicationType.FileGallery:
                        foreach (Folder f in Folders.GetFolders(CSContext.Current.UserID, false, false, true))
						{
                            String loc = urldata.FormatUrl("googleSiteMap","file","app",f.ApplicationKey);
							DateTime lastmod = f.MostRecentPostDate;
							if (f.IsActive && !f.IsPrivate && f.IsSearchable && f.TotalPosts > 0)
								items.Add(new GoogleSiteMapItem(loc,lastmod));
						}
						break;

				}
			}

			writer = new GoogeSiteMapIndexWriter(items, new List<Post>(), null, BaseUrl); 
            
            return new CachedFeed(DateTime.Now,null,writer.GetXml());

        }

 

    }

<%@ Page Language="C#" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Security.Cryptography" %>
<%@ Import Namespace="System.Text.RegularExpressions" %>

<script runat="server">

 /**
 * $Id: tiny_mce_gzip.aspx 123 2006-10-21 16:15:53Z spocke $
 *
 * @author Moxiecode
 * @copyright Copyright � 2006, Moxiecode Systems AB, All rights reserved.
 *
 * This file compresses the TinyMCE JavaScript using GZip and
 * enables the browser to do two requests instead of one for each .js file.
 *
 * It's a good idea to use the diskcache option since it reduces the servers workload.
 */
    
 /*
  * Modified: 8/7/2007 by Ben Tiedt, Telligent Systems, http://telligent.com/
  * 
  * Updated to use System.IO.Compression.GZipStream instead of 3rd party component
  * Updated to use StringBuilder instead of string cancatenation
  * Updated to remove support for file-based caching
  * Updated to use a compiled Regex instead of inline Regex to clean querystring parameters
  * 
  */
 
    private static Regex ParameterCleaner = new Regex(@"[^0-9a-zA-Z\\-_,]+", RegexOptions.Singleline | RegexOptions.Compiled);

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        string enc, suffix;
        StringBuilder content = new StringBuilder();
        string[] plugins, languages, themes;
        bool supportsGzip, isJS, compress;
        int i, x, bytes, expiresOffset;
        System.IO.Compression.GZipStream gzipStream;
        Encoding encoding = Encoding.GetEncoding("windows-1252");
        byte[] buff;

        // Get input
        plugins = GetParam("plugins", "").Split(',');
        languages = GetParam("languages", "en").Split(',');
        themes = GetParam("themes", "advanced").Split(',');
        isJS = GetParam("js", "") == "true";
        compress = GetParam("compress", "true") == "true";
        suffix = GetParam("suffix", "") == "_src" ? "_src" : "";
        expiresOffset = 10; // Cache for 10 days in browser cache

        // Custom extra javascripts to pack
        string[] custom = {/*
	    "some custom .js file",
	    "some custom .js file"
            */};

        // Set response headers
        Response.ContentType = "text/javascript";
        Response.Charset = "UTF-8";
        Response.Buffer = false;

        // Setup cache
        Response.Cache.SetExpires(DateTime.Now.AddDays(expiresOffset));
        Response.Cache.SetCacheability(HttpCacheability.Public);
        Response.Cache.SetValidUntilExpires(false);

        // Vary by all parameters and some headers
        Response.Cache.VaryByHeaders["Accept-Encoding"] = true;
        Response.Cache.VaryByParams["theme"] = true;
        Response.Cache.VaryByParams["language"] = true;
        Response.Cache.VaryByParams["plugins"] = true;
        Response.Cache.VaryByParams["lang"] = true;
        Response.Cache.VaryByParams["index"] = true;

        // Is called directly then auto init with default settings
        if (!isJS)
        {
            Response.WriteFile(Server.MapPath("tiny_mce_gzip.js"));
            Response.Write("tinyMCE_GZ.init({});");
            return;
        }

        // Check if it supports gzip
        enc = Regex.Replace("" + Request.Headers["Accept-Encoding"], @"\s+", "").ToLower();
        supportsGzip = enc.IndexOf("gzip") != -1 || Request.Headers["---------------"] != null;
        enc = enc.IndexOf("x-gzip") != -1 ? "x-gzip" : "gzip";

        // Add core
        content.Append(GetFileContents("tiny_mce" + suffix + ".js"));

        // Patch loading functions
        content.Append("tinyMCE_GZ.start();");

        // Add core languages
        for (x = 0; x < languages.Length; x++)
            content.Append(GetFileContents("langs/" + languages[x] + ".js"));

        // Add themes
        for (i = 0; i < themes.Length; i++)
        {
            content.Append(GetFileContents("themes/" + themes[i] + "/editor_template" + suffix + ".js"));

            for (x = 0; x < languages.Length; x++)
                content.Append(GetFileContents("themes/" + themes[i] + "/langs/" + languages[x] + ".js"));
        }

        // Add plugins
        for (i = 0; i < plugins.Length; i++)
        {
            content.Append(GetFileContents("plugins/" + plugins[i] + "/editor_plugin" + suffix + ".js"));

            for (x = 0; x < languages.Length; x++)
                content.Append(GetFileContents("plugins/" + plugins[i] + "/langs/" + languages[x] + ".js"));
        }

        // Add custom files
        for (i = 0; i < custom.Length; i++)
            content.Append(GetFileContents(custom[i]));

        // Restore loading functions
        content.Append("tinyMCE_GZ.end();");

        // Generate GZIP'd content
        if (supportsGzip)
        {
            if (compress)
                Response.AppendHeader("Content-Encoding", enc);

            gzipStream = new System.IO.Compression.GZipStream(Response.OutputStream, System.IO.Compression.CompressionMode.Compress);
            buff = encoding.GetBytes(content.ToString().ToCharArray());
            gzipStream.Write(buff, 0, buff.Length);
            gzipStream.Flush();
            gzipStream.Close();
        }
        else
            Response.Write(content.ToString());
    }

    public string GetParam(string name, string def)
    {
        return ParameterCleaner.Replace(Request.QueryString[name] ?? def, "");
    }

    public string GetFileContents(string path)
    {
        try
        {
            string content;

            path = Server.MapPath(path);

            if (!File.Exists(path))
                return "";

            StreamReader sr = new StreamReader(path);
            content = sr.ReadToEnd();
            sr.Close();

            return content;
        }
        catch (Exception ex)
        {
            // Ignore any errors
        }

        return "";
    }

</script>
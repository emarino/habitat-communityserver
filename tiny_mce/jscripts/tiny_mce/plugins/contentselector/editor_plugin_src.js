
tinyMCE.importPluginLanguagePack('contentselector', 'en,tr,sv,cs,zh_cn,fr_ca,da,he,nb,de,hu,ru,ru_KOI8-R,ru_UTF-8,nn,fi,es,cy,is,pl,nl,fr,pt_br');

var TinyMCE_ContentSelectorPlugin = {
	_currentEditor : null,
	_currentBookmark : null,
	
	getInfo : function() {
		return {
			longname : 'Community Server Content Selector Plugin',
			author : 'Telligent Systems',
			authorurl : 'http://telligentsystems.com/',
			infourl : 'http://telligentsystems.com/',
			version : tinyMCE.majorVersion + "." + tinyMCE.minorVersion
		};
	},

	initInstance : function(inst) {
		if (tinyMCE.isMSIE && tinyMCE.getParam("paste_auto_cleanup_on_paste", false))
			tinyMCE.addEvent(inst.getBody(), "paste", TinyMCE_PastePlugin._handlePasteEvent);
	},

	getControlHTML : function(cn) { 
		switch (cn) { 
			case "contentselector":
				return tinyMCE.getButtonHTML(cn, 'lang_contentselector_desc', '{$pluginurl}/images/browse.gif', 'csContentSelector', true); //Commented by Kishan on 25 Feb 08
		} 

		return ''; 
	},

	execCommand : function(editor_id, element, command, user_interface, value) { 
		switch (command) { 
			case "csContentSelector": 
				TinyMCE_ContentSelectorPlugin._openContentSelector(editor_id);

				return true;
		} 

		// Pass to next handler in chain 
		return false; 
	},
		
	_openContentSelector : function(editor_id) {
		TinyMCE_ContentSelectorPlugin._currentEditor = tinyMCE.getInstanceById(editor_id);
		TinyMCE_ContentSelectorPlugin._currentBookmark = this._currentEditor.selection.getBookmark();
		
		if (tinyMCE_CommunityServerOptions)
			tinyMCE_CommunityServerOptions.ContentSelectorOpenFunction();	
	},
		
	_addContent : function(content) {
		TinyMCE_ContentSelectorPlugin._currentEditor.selection.moveToBookmark(this._currentBookmark);	
		TinyMCE_ContentSelectorPlugin._currentEditor.execCommand("mceInsertRawHTML", false, content)
	}
	
};

tinyMCE.addPlugin("contentselector", TinyMCE_ContentSelectorPlugin);

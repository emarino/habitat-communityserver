/* Import plugin specific language pack */
tinyMCE.importPluginLanguagePack('smilies', 'en,tr,sv,zh_cn,cs,fa,fr_ca,fr,de,pl,pt_br,nl,da,he,nb,hu,ru,ru_KOI8-R,ru_UTF-8,nn,es,cy,is,zh_tw,zh_tw_utf8,sk');

// Plucin static class
var TinyMCE_SmiliesPlugin = {
	getInfo : function() {
		return {
			longname : 'Smilies',
			author : 'Telligent Systems',
			authorurl : 'http://telligentsystems.com/',
			infourl : 'http://telligentsystems.com/',
			version : tinyMCE.majorVersion + "." + tinyMCE.minorVersion
		};
	},

	/**
	 * Returns the HTML contents of the emoticons control.
	 */
	getControlHTML : function(cn) {
		switch (cn) {
			case "smiley":
				var smilies = (tinyMCE_CommunityServerOptions && tinyMCE_CommunityServerOptions.Smilies) ? tinyMCE_CommunityServerOptions.Smilies : new Array();
				if (smilies.length > 0)
					return tinyMCE.getButtonHTML(cn, 'lang_smilies_desc', '{$pluginurl}/images/smiley.gif', 'csSmiley');
		}

		return "";
	},

	/**
	 * Executes the mceEmoticon command.
	 */
	execCommand : function(editor_id, element, command, user_interface, value) {
		// Handle commands
		switch (command) {
			case "csSmiley":
				var template = new Array();

				template['file'] = '../../plugins/smilies/smilies.htm'; // Relative to theme
				template['width'] = 160;
				template['height'] = 160;

				// Language specific width and height addons
				template['width'] += tinyMCE.getLang('lang_smilies_delta_width', 0);
				template['height'] += tinyMCE.getLang('lang_smilies_delta_height', 0);

				var smilies = (tinyMCE_CommunityServerOptions && tinyMCE_CommunityServerOptions.Smilies) ? tinyMCE_CommunityServerOptions.Smilies : new Array();
				tinyMCE.openWindow(template, {editor_id : editor_id, inline : "yes", smilies : smilies});

				return true;
		}

		// Pass to next handler in chain
		return false;
	}
};

// Register plugin
tinyMCE.addPlugin('smilies', TinyMCE_SmiliesPlugin);

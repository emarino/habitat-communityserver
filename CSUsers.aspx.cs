using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using CommunityServer.Components;
using PartnerNetEncryptDecrypt;
using PartnerNetErrorHandler;
using PartnernetGlobalFunctions;

public partial class CSUsers : CommunityServer.Components.CSPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //try
        //{            
            if(Request.QueryString.Count > 0)
            {
                string SymmetricKey = GlobalFunctions.GetPartnerNetConfig("SymmetricKey");

                string strUserName = EncryptDecrypt.DecryptString(Request.QueryString["UserName"].ToString(),SymmetricKey);
                string strEmail = EncryptDecrypt.DecryptString(Request.QueryString["Email"].ToString(),SymmetricKey);
                string strReturnUrl = Request.QueryString["ReturnUrl"].ToString();
                string PrivateForums = EncryptDecrypt.DecryptString(Request.QueryString["PrivateForums"], SymmetricKey);
                Boolean blnExecutive = false;
                //Boolean blnExecutive = Convert.ToBoolean(EncryptDecrypt.DecryptString(Request.QueryString["blnExecutiveDirec"],SymmetricKey));

                User oCSUser = new User(strUserName);
                
                MembershipUser oMembershipUser = Membership.GetUser(strUserName);                

                if (oMembershipUser == null)
                {
                    //Create User
                    oCSUser.AccountStatus = UserAccountStatus.Approved;
                    oCSUser.Username = strUserName;
                    oCSUser.IsAnonymous = false;
                    oCSUser.Email = strEmail;
                    oCSUser.EnableAvatar = true;
                    oCSUser.EnableEmail = true;
                    oCSUser.ForceLogin = false;
                    oCSUser.Password = strUserName.ToUpper();

                    CommunityServer.Users.Create(oCSUser, false);
                }
                else
                {                    
                    //Update user details   
                    User oUser = CommunityServer.Users.GetUser(strUserName);
                    oUser.Email = strEmail;
                    CommunityServer.Users.UpdateUser(oUser);
                }

                //Check if the user's password in in uppercase. If not, change the password to uppercase
                if (!Membership.ValidateUser(strUserName, strUserName.ToUpper()))
                {
                    User oUser = CommunityServer.Users.GetUser(strUserName);
                    oUser.ChangePassword(strUserName, strUserName.ToUpper());
                    CommunityServer.Users.UpdateUser(oUser);
                }

                if (PrivateForums.Length > 0)
                {
                    //Get the Roles from PartnerNet_Config.xml to add/remove current user to a CS Role
                    string PrivateSharePointGroups = GlobalFunctions.GetPartnerNetConfig("PrivateSharePointGroups");
                    String[] arrPrivateSharePointGroups;
                    String[] arrPrivateForums;

                    if (PrivateSharePointGroups.IndexOf(";") > 0)
                    {
                        //Split the roles and mode based on ";" and store it in arrays
                        arrPrivateSharePointGroups = PrivateSharePointGroups.Split(';');
                        arrPrivateForums = PrivateForums.Split(';');

                        //Loop thru Roles array and call AddRemoveUserToRole() mehtod to add/remove a user from CS role
                        for (int Counter = 0; Counter < arrPrivateForums.Length; Counter++)
                        {
                            if (arrPrivateSharePointGroups[Counter].Length > 0)
                                    AddRemoveUserToRole(strUserName, arrPrivateSharePointGroups[Counter].ToString().Trim(), Convert.ToBoolean(arrPrivateForums[Counter].ToString().Trim()));
                        }
                    }
                    else
                        AddRemoveUserToRole(strUserName, PrivateSharePointGroups.Replace(";", "").Trim(), Convert.ToBoolean(PrivateForums.Replace(";","").Trim()));
                }

                /* Code to Add/Remove user from CS Executive Directors role.
                if (blnExecutive == true)
                {
                    string[] UserRoles = CommunityServer.Components.Roles.GetUserRoleNames(strUserName);
                    Boolean blnExecutiveDirec = false;

                    foreach (string strRole in UserRoles)
                    {
                        if (strRole == "Executive Directors")
                        {
                            blnExecutiveDirec = true;
                            break;
                        }
                    }

                    if (blnExecutiveDirec == false)
                        CommunityServer.Components.Roles.AddUserToRole(strUserName, "Executive Directors");
                }
                else
                {
                    string[] UserRoles = CommunityServer.Components.Roles.GetUserRoleNames(strUserName);

                    foreach (string strRole in UserRoles)
                    {
                        if (strRole == "Executive Directors")
                        {
                            CommunityServer.Components.Roles.RemoveUserFromRole(strUserName, "Executive Directors");
                            break;
                        }                        
                    }
                }*/

                //Code to set the time zone   
                User oUserProfile = CommunityServer.Users.GetUserWithWriteableProfile(0, strUserName, false);
                oUserProfile.Profile.Timezone = -5;
                //oUserProfile.EnableEmail = false;
                //oUserProfile.EnableHtmlEmail = false;
                //oUserProfile.EnableThreadTracking = false;
                CommunityServer.Users.UpdateUser(oUserProfile);
                oUserProfile = null;               
                
                //if (Membership.ValidateUser(strUserName, strUserName.ToUpper()))
                {
                    /*CommunityServer.Components.User userToLogin = new CommunityServer.Components.User();
                    userToLogin.Username = strUserName;
                    userToLogin.Password = strUserName.ToUpper();
                    CommunityServer.Components.LoginUserStatus loginStatus = CommunityServer.Users.ValidUser(userToLogin);
                    */
                    //if (loginStatus == CommunityServer.Components.LoginUserStatus.Success)
                    {
                        FormsAuthentication.RedirectFromLoginPage(strUserName, false);

                        if (strReturnUrl == "")
                            Response.Redirect(System.Configuration.ConfigurationSettings.AppSettings["CSWebURL"].ToString() + "Forums", false);
                        else
                            Response.Redirect(strReturnUrl, false);                        
                    }
                }
            }
        //}
        //catch (Exception Ex)
        //{
            //Response.Write(Ex.ToString());
            //Response.Write(GlobalFunctions.GetPartnerNetConfig("CommonPageError.English"));
            //PartnerNetErrorHandler.ErrorHandler.ReportError(System.Web.HttpContext.Current.User.Identity.Name.ToString(), Ex);
            //Response.Redirect(System.Configuration.ConfigurationSettings.AppSettings["CSWebURL"].ToString() + "Error.htm", false);
        //}
    }

    public void AddRemoveUserToRole(string strUserName, string strRoleName, Boolean blnMode)
    {
        if (blnMode == true)
        {
            string[] UserRoles = CommunityServer.Components.Roles.GetUserRoleNames(strUserName);
            Boolean blnUserRole = false;

            foreach (string strRole in UserRoles)
            {
                if (strRole == strRoleName)
                {
                    blnUserRole = true;
                    break;
                }
            }

            string[] oRoles = CommunityServer.Components.Roles.GetRoleNames();
            Boolean blnRoleExists = false;
            foreach (string oRole in oRoles)
            {
                if (oRole == strRoleName)
                {
                    blnRoleExists = true;
                    break;
                }
            }

            if (blnUserRole == false && blnRoleExists == true)
                CommunityServer.Components.Roles.AddUserToRole(strUserName, strRoleName);
        }
        else
        {
            string[] UserRoles = CommunityServer.Components.Roles.GetUserRoleNames(strUserName);

            foreach (string strRole in UserRoles)
            {
                if (strRole == strRoleName)
                {
try{
                    CommunityServer.Components.Roles.RemoveUserFromRole(strUserName, strRoleName);
}
catch (Exception Ex2){}
                    break;
                }
            }
        }
    }
}

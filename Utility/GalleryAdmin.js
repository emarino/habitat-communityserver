  function TogglePreview(x, y, z) {
    previewWindow = document.getElementById('PreviewWindow');
    previewImage = document.getElementById('PreviewImage');
    
    if (previewWindow.style.visibility == "hidden") {
      xbox = document.getElementById(x);
      ybox = document.getElementById(y);
      zbox = document.getElementById(z);      

      previewWindow.style.visibility = "visible";
      previewWindow.style.display = 'block';
      
      previewImage.width = xbox.value;
      previewImage.height = ybox.value;

      previewWindow.style.left = getposOffset(zbox, "left") + zbox.offsetWidth - previewWindow.offsetWidth;
      previewWindow.style.top = getposOffset(zbox, "top") + zbox.offsetHeight;
    } else {
      previewWindow.style.visibility = "hidden";
      previewWindow.style.display = 'none';
    }
  }

function getposOffset(what, offsettype){
  var totaloffset=(offsettype=="left")? what.offsetLeft : what.offsetTop;
  var parentEl=what.offsetParent;
  while (parentEl!=null){
    totaloffset=(offsettype=="left")? totaloffset+parentEl.offsetLeft : totaloffset+parentEl.offsetTop;
    parentEl=parentEl.offsetParent;
  }
  return totaloffset;
}

function OpenWindow (target) { 
  window.open(target, "_Child", "toolbar=no,scrollbars=yes,resizable=yes,width=500,height=400"); 
}

function HighlightThumbnailCell(cellID, color, times)
{
	thumbnailCell = document.getElementById(cellID);
	thumbnailCell.style.backgroundColor = color;

	if(times < 10)
	{
		if(color == "yellow")
			color = "red";
		else color = "yellow";
		setTimeout("HighlightThumbnailCell('" + cellID + "', '" + color + "', " + (times+1) + ")", 500);
	}
	else
	{
	thumbnailCell.style.backgroundColor = "";
	}
}
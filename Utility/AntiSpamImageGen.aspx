<%@ Page language="C#" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Imaging" %>
<%@ Import Namespace="CommunityServer" %>
<%@ Import Namespace="CommunityServer.Components" %>

<script runat="server" language="C#">
  void Page_Load(Object sender, EventArgs e)
  {
    HttpContext context = HttpContext.Current;
    int charsNo = 6;
    int fontSize = 30;
    int bgWidth = 250; 
    int bgHeight = 80;
    float x = (bgWidth - (charsNo * (fontSize + 0.5F))) / 2; // TODO: optimize
    float y = (bgHeight - (fontSize * 1.7F)) / 2; // TODO: optimize
    
    // Get params from QueryString
    //
    fontSize = CSContext.Current.GetIntFromQueryString("FontSize", fontSize);   
    bgWidth = CSContext.Current.GetIntFromQueryString("ImgWidth", bgWidth);  
    bgHeight = CSContext.Current.GetIntFromQueryString("ImgHeight", bgHeight);    
    
    // Load defaults if params are empty
    //
    if(fontSize == -1) fontSize = 30; 
    if(bgWidth == -1) bgWidth = 290;  
    if(bgHeight == -1) bgHeight = 80; 
    
    // Generate the text
    //
    string genText = CreateTemporaryPassword(charsNo).ToUpper();
    
    // Add the generate text to a session variable 
    //
    Session.Add("forumsAntiSpamTextGen", genText);           
    
    // Create the memory map 
    //
    Bitmap raster;
    System.Drawing.Imaging.PixelFormat pixFormat = System.Drawing.Imaging.PixelFormat.Format32bppArgb;
    
    // Select an memory image from file of 290x80 px
    // in the current dir, NoSpamBgImgs folder named bg_X.jpg
    //
    Graphics graphicObj;
    string bgFilePath = context.Server.MapPath(@".\AntiSpamBgImgs\bg_" + new Random().Next(5) + ".jpg");      
    System.Drawing.Image imgObj = System.Drawing.Image.FromFile(bgFilePath);
    
    // Creating the raster image & graphic objects
    //
    raster = new Bitmap(imgObj, bgWidth, bgHeight);
    graphicObj = Graphics.FromImage(raster);
    
    // Creating an array for most readable yet cryptic fonts for OCR's
    // This is entirely up to developer's discretion
    // CAPTCHA recomandation
    //
    String[] crypticFonts = new String[10];    
    crypticFonts [0] = "Arial";
    crypticFonts [1] = "Verdana";
    crypticFonts [2] = "Comic Sans MS";
    crypticFonts [3] = "Impact";
    crypticFonts [4] = "Haettenschweiler";
    crypticFonts [5] = "Lucida Sans Unicode";
    crypticFonts [6] = "Garamond";
    crypticFonts [7] = "Courier New";
    crypticFonts [8] = "Book Antiqua";
    crypticFonts [9] = "Arial Narrow";

    // Instantiate object of brush with black color
    //
    SolidBrush[] brushes = new SolidBrush[1];
    brushes [0] = new SolidBrush(Color.Black);

    // Loop to write the characters on image with different fonts
    // CAPTCHA method
    //
    for(int a = 0; a < genText.Length; a++) {    
      Font fontObj = new Font(crypticFonts[new Random().Next(a)], fontSize);
      
      graphicObj.DrawString(genText.Substring(a, 1), fontObj, brushes[0], x + (a * fontSize), y);
    }
    
    // Flush again
    //
    graphicObj.Flush();

    // Setting the response header content MIME type
    //
    context.Response.ContentType = "image/gif";
    
    // Saving the file to output stream to be displayed in browser
    //
    raster.Save(context.Response.OutputStream, System.Drawing.Imaging.ImageFormat.Gif);
    
    // Flushing to the Response 
    //
    context.Response.Flush();    
  }

        public static String CreateTemporaryPassword(int length) {

            string strTempPassword = Guid.NewGuid().ToString("N");
	    strTempPassword = strTempPassword.Replace("0", "");
	    strTempPassword = strTempPassword.Replace("O", "");

            for(int i = 0; i < (length / 32); i++) {
                strTempPassword += Guid.NewGuid().ToString("N");
            }

            return strTempPassword.Substring(0, length);
        }

</script>



<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Discussions.Controls.CSForumThemePage" MasterPageFile="forums.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Discussions.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
		if (!Telligent.Registration.Licensing.Licenses.ContainsKey("NewsGateway") || string.IsNullOrEmpty(CurrentCSContext.SiteSettings.NntpServerLocation))
            throw new CSException(CSExceptionType.AccessDenied, "No NNTP server license is installed or the NNTP server location is undefined.");
        
        SetTitle(ResourceManager.GetString("NntpInformation_Title"), true);
        Content.Text = string.Format(ResourceManager.GetString("NntpInformation_Content"), CurrentCSContext.SiteSettings.NntpServerLocation, Globals.ApplicationPath);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
<div class="CommonContentArea">
	<CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdTop runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
	
    <CSControl:ResourceControl runat="server" ResourceName="NntpInformation_Title" Tag="h2" CssClass="CommonTitle" />		
	<div class="CommonContent">
        <asp:Literal Runat="server" ID="Content" />
	</div>
	
	<CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdBottom runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
</div>
</asp:Content>
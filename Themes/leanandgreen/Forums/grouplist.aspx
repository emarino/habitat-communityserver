<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Discussions.Controls.CSForumThemePage" MasterPageFile="forums.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Discussions.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        if (CurrentGroup != null)
            SetTitle(CurrentGroup.Name, true);
        else
            SetTitle(ResourceManager.GetString("forums"), true);
    }

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="bcr" runat="server">
<div class="CommonContentArea">
    <CSForum:BreadCrumb runat="server" Tag="Div" CssClass="CommonBreadCrumbArea">
        <DisplayConditions><CSControl:QueryStringPropertyComparison QueryStringProperty1="GroupID" Operator="IsSetOrTrue" runat="server" /></DisplayConditions>
    </CSForum:BreadCrumb>

	<CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdTop runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>

    <%-- This GroupEditableData appears when viewing a single group, otherwise it is not rendered --%>
    <CSForum:GroupEditableData runat="server" Property="Name" Tag="h2" CssClass="CommonTitle" EditorLinkCssClass="CommonTextButton" EditorCssClass="CommonInlineTagEditor" ContentCssClass="CommonContentPartBorderOff" ContentEditingCssClass="CommonContentPartBorderOn" ContentHoverCssClass="CommonContentPartBorderOn">
        <DisplayConditions><CSControl:QueryStringPropertyValueComparison runat="server" QueryStringProperty="GroupID" Operator="IsSetOrTrue" /></DisplayConditions>
    </CSForum:GroupEditableData>
    
    <%-- This ResourceControl is rendered only when viewing all groups (when the GroupEdtableArea above is not rendered) --%>
    <CSControl:ResourceControl runat="server" ResourceName="discussions" Tag="h2" CssClass="CommonTitle">
        <DisplayConditions Operator="Not"><CSControl:QueryStringPropertyValueComparison runat="server" QueryStringProperty="GroupID" Operator="IsSetOrTrue" /></DisplayConditions>
    </CSControl:ResourceControl>    

	<div class="CommonContent">
	
    	<%-- This GroupList is displayed when personalizing displayed groups/forums --%>
        <CSForum:GroupList runat="server">
            <DisplayConditions>
                <CSControl:QueryStringPropertyValueComparison runat="server" QueryStringProperty="personalize" Operator="EqualTo" ComparisonValue="true" />
	            <CSControl:Conditions Operator="Not" runat="server">
	                <CSControl:QueryStringPropertyValueComparison runat="server" QueryStringProperty="GroupID" Operator="IsSetOrTrue" />
	            </CSControl:Conditions>
	        </DisplayConditions>
            <QueryOverrides ApplyUserGroupFilter="false" PageSize="999999" />
            <LeaderTemplate>
                <CSControl:ModifiedUrl runat="server" QueryStringModification="personalize=false" ResourceName="ForumGroupView_Personalize_View" />
            </LeaderTemplate>
            <ItemTemplate>
                <div class="CommonListArea">
			        <div class="CommonListTitle">
			            <CSForum:GroupData runat="server" LinkTo="GroupHome" Property="Name" />
			            <CSControl:GroupVisibilityToggleButton runat="server">
			                <LeaderTemplate>(<CSControl:ResourceControl runat="server" ResourceName="ForumGroupView_Personalize_GroupDisplay" />: </LeaderTemplate>
			                <OffTemplate><CSControl:ResourceControl ResourceName="Yes" runat="server" /></OffTemplate>
			                <OnTemplate><CSControl:ResourceControl ResourceName="No" runat="server" /></OnTemplate>
			                <ProcessingTemplate>...</ProcessingTemplate>
			                <TrailerTemplate>)</TrailerTemplate>
			            </CSControl:GroupVisibilityToggleButton>
			        </div>
		            <CSForum:ForumList runat="server">
		                <QueryOverrides ApplyUserSectionFilter="false" PageSize="999999" />
		    	        <HeaderTemplate>
					        <table width="100%" cellpadding="0" cellspacing="0" border="0">
					        <thead>
						        <tr> 
							        <th colspan="2" nowrap="nowrap" class="CommonListHeaderLeftMost ForumGroupImageAndNameHeader" align="center"><CSControl:ResourceControl runat="server" ResourceName="ForumGroupView_Inline1" /></th>
							        <th class="CommonListHeader ForumGroupLastPostHeader"><CSControl:ResourceControl runat="server" ResourceName="ForumGroupView_Inline4" /></th>                                    
							        <th class="CommonListHeader ForumGroupTotalThreadsHeader" align="center"><CSControl:ResourceControl runat="server" ResourceName="ForumGroupView_Inline2" /></th>                                    
							        <th class="CommonListHeader ForumGroupTotalPostsHeader" align="center"><CSControl:ResourceControl runat="server" ResourceName="ForumGroupView_Inline3" /></th>
							        <th class="CommonListHeader ForumGroupTotalPostsHeader" align="center"><CSControl:ResourceControl runat="server" ResourceName="ForumGroupView_Inline5" /></th> 
						        </tr>
					        </thead>
					        <tbody>
				        </HeaderTemplate>
				        <ItemTemplate>
					        <tr>
						        <td class="ForumListCellLeftMostImageOnly  ForumGroupImageColumn">
							        <%# ForumFormatter.StatusIcon( (Forum) Container.DataItem ) %>
						        </td>
						        <td class="CommonListCell ForumGroupNameColumn">
						            <CSForum:ForumData LinkCssClass="ForumGroupNameRead" Property="Name" LinkTo="HomePage" runat="server"><DisplayConditions><CSForum:ForumPropertyValueComparison ComparisonProperty="HasRead" Operator="IsSetOrTrue" runat="server" /></DisplayConditions></CSForum:ForumData>
						            <CSForum:ForumData LinkCssClass="ForumGroupNameUnRead" Property="Name" LinkTo="HomePage" runat="server"><DisplayConditions Operator="Not"><CSForum:ForumPropertyValueComparison ComparisonProperty="HasRead" Operator="IsSetOrTrue" runat="server" /></DisplayConditions></CSForum:ForumData>
							        <CSForum:ForumData Property="Description" runat="server" Tag="Div" />
							        <CSForum:ForumList runat="server" ShowHeaderFooterOnNone="false">
							            <QueryOverrides PageSize="999999" />
							            <HeaderTemplate><CSControl:ResourceControl ResourceName="Subforums" runat="server" Tag="B" /></HeaderTemplate>
							            <ItemTemplate><CSForum:ForumData runat="server" Property="Name" LinkTo="HomePage" /></ItemTemplate>
							            <SeparatorTemplate>, </SeparatorTemplate>
							            <NoneTemplate></NoneTemplate>
							        </CSForum:ForumList>
						        </td>
						        <td class="CommonListCell ForumGroupLastPostColumn">
						            <CSControl:PlaceHolder runat="server">
						                <DisplayConditions><CSForum:ForumPropertyValueComparison ComparisonProperty="MostRecentPostDate" ComparisonValue="6/8/1980" Operator="GreaterThan" runat="server" /></DisplayConditions>
						                <ContentTemplate>
    					                    <CSForum:ForumData LinkTo="MostRecentPost" Property="MostRecentPostSubject" runat="server" Tag="B" TruncateAt="15" />
                                            <CSForum:ForumData LinkTo="MostRecentPostAuthor" Property="MostRecentPostAuthor" runat="server" Tag="Div" TruncateAt="15"><LeaderTemplate>by </LeaderTemplate></CSForum:ForumData>
                                            <CSForum:ForumData Property="MostRecentPostDate" runat="server" IncludeTimeInDate="true" Tag="Div" />
                                        </ContentTemplate>
                                    </CSControl:PlaceHolder>
                                    &nbsp;
						        </td>
						        <td class="CommonListCell ForumGroupTotalThreadsColumn" align="center"><CSForum:ForumData runat="server" Property="TotalThreads" /></td>
						        <td class="CommonListCell ForumGroupTotalPostsColumn" align="center"><CSForum:ForumData runat="server" Property="TotalPosts" /></td>
						        <td class="CommonListCell ForumGroupTotalPostsColumn" align="center">
							        <CSControl:SectionVisibilityToggleButton runat="server">
			                            <OffTemplate><CSControl:ResourceControl ResourceName="Yes" runat="server" /></OffTemplate>
			                            <OnTemplate><CSControl:ResourceControl ResourceName="No" runat="server" /></OnTemplate>
			                            <ProcessingTemplate>...</ProcessingTemplate>
			                        </CSControl:SectionVisibilityToggleButton>
						        </td> 
					        </tr>
				        </ItemTemplate>
				        <FooterTemplate>
					        </tbody>
					        </table>				
				        </FooterTemplate>
			        </CSForum:ForumList>
			    </div>
            </ItemTemplate>
        </CSForum:GroupList>
        
        <%-- This GroupList is displayed when *not* personalizing displayed groups/forums (or when viewing a single Group) --%>
        <CSForum:GroupList runat="server">
            <DisplayConditions Operator="Not">
                <CSControl:QueryStringPropertyValueComparison QueryStringProperty="personalize" ComparisonValue="true" Operator="EqualTo" runat="server" />
            </DisplayConditions>
            <QueryOverrides ApplyUserGroupFilter="true" PageSize="999999" />
            <LeaderTemplate>
                <CSControl:ModifiedUrl runat="server" QueryStringModification="personalize=true" ResourceName="ForumGroupView_Personalize_Manage">
	                <DisplayConditions Operator="And">
	                    <CSControl:UserInRoleCondition ID="UserInRoleCondition1" Role="Registered Users" UseAccessingUser="true" runat="server" />
	                    <CSControl:Conditions Operator="Not" runat="server">
	                        <CSControl:QueryStringPropertyValueComparison runat="server" QueryStringProperty="personalize" Operator="EqualTo" ComparisonValue="true" />
	                        <CSControl:QueryStringPropertyValueComparison runat="server" QueryStringProperty="GroupID" Operator="IsSetOrTrue" />
	                    </CSControl:Conditions>
                    </DisplayConditions>
	            </CSControl:ModifiedUrl>
            </LeaderTemplate>
            <ItemTemplate>
                <div class="CommonListArea">
			        <CSControl:GroupCollapsableArea
				        runat="server" 
				        ToggleButtonId="ToggleLink"
				        ToggleCollapsedCssClass="ForumGroupToggleCollapsed"
				        ToggleExpandedCssClass="ForumGroupToggleExpanded"
				        HeaderCollapsedCssClass="CommonListTitle"
				        HeaderExpandedCssClass="CommonListTitle"
				        HeaderProcessingCssClass="CommonListTitle ForumGroupTitleProcessing"
				        EnableHeaderDblClickToggle="false"
				        >
				        <EnableCollapseExpandConditions Operator="Not">
				            <CSControl:QueryStringPropertyValueComparison runat="server" QueryStringProperty="GroupID" Operator="IsSetOrTrue" />
				        </EnableCollapseExpandConditions>
				        <HeaderTemplate>
					        <table cellspacing="0" cellpadding="0" width="100%" border="0"><tr><td align="left">
					            <CSForum:GroupData runat="server" LinkTo="GroupHome" Property="Name" />
					        </td><td align="right">
						        <asp:LinkButton ID="ToggleLink" Runat="server">&nbsp;</asp:LinkButton>
					        </td></tr></table>
				        </HeaderTemplate>
				        <ContentTemplate>
				            <CSForum:ForumList runat="server">
				                <QueryOverrides ApplyUserSectionFilter="true" PageSize="999999" />
				    	        <HeaderTemplate>
							        <table width="100%" cellpadding="0" cellspacing="0" border="0">
							        <thead>
								        <tr> 
									        <th colspan="2" nowrap="nowrap" class="CommonListHeaderLeftMost ForumGroupImageAndNameHeader" align="center"><CSControl:ResourceControl runat="server" ResourceName="ForumGroupView_Inline1" /></th>
									        <th class="CommonListHeader ForumGroupLastPostHeader"><CSControl:ResourceControl runat="server" ResourceName="ForumGroupView_Inline4" /></th>                                    
									        <th class="CommonListHeader ForumGroupTotalThreadsHeader" align="center"><CSControl:ResourceControl runat="server" ResourceName="ForumGroupView_Inline2" /></th>                                    
									        <th class="CommonListHeader ForumGroupTotalPostsHeader" align="center"><CSControl:ResourceControl runat="server" ResourceName="ForumGroupView_Inline3" /></th>
								        </tr>
							        </thead>
							        <tbody>
						        </HeaderTemplate>
						        <ItemTemplate>
							        <tr>
								        <td class="ForumListCellLeftMostImageOnly  ForumGroupImageColumn">
									        <%# ForumFormatter.StatusIcon( (Forum) Container.DataItem ) %>
								        </td>
								        <td class="CommonListCell ForumGroupNameColumn">
								            <CSForum:ForumData LinkCssClass="ForumGroupNameRead" Property="Name" LinkTo="HomePage" runat="server"><DisplayConditions><CSForum:ForumPropertyValueComparison ComparisonProperty="HasRead" Operator="IsSetOrTrue" runat="server" /></DisplayConditions></CSForum:ForumData>
								            <CSForum:ForumData LinkCssClass="ForumGroupNameUnRead" Property="Name" LinkTo="HomePage" runat="server"><DisplayConditions Operator="Not"><CSForum:ForumPropertyValueComparison ComparisonProperty="HasRead" Operator="IsSetOrTrue" runat="server" /></DisplayConditions></CSForum:ForumData>
									        <CSForum:ForumData Property="Description" runat="server" Tag="Div" />
									        <CSForum:ForumList runat="server" ShowHeaderFooterOnNone="false">
									            <QueryOverrides PageSize="999999" />
									            <HeaderTemplate><CSControl:ResourceControl ResourceName="Subforums" runat="server" Tag="B" /></HeaderTemplate>
									            <ItemTemplate><CSForum:ForumData runat="server" Property="Name" LinkTo="HomePage" /></ItemTemplate>
									            <SeparatorTemplate>, </SeparatorTemplate>
									            <NoneTemplate></NoneTemplate>
									        </CSForum:ForumList>
								        </td>
								        <td class="CommonListCell ForumGroupLastPostColumn">
								            <CSControl:PlaceHolder runat="server">
								                <DisplayConditions><CSForum:ForumPropertyValueComparison ComparisonProperty="MostRecentPostDate" ComparisonValue="6/8/1980" Operator="GreaterThan" runat="server" /></DisplayConditions>
								                <ContentTemplate>
            					                    <CSForum:ForumData LinkTo="MostRecentPost" Property="MostRecentPostSubject" runat="server" Tag="B" TruncateAt="15" />
                                                    <CSForum:ForumData LinkTo="MostRecentPostAuthor" Property="MostRecentPostAuthor" runat="server" Tag="Div" TruncateAt="15"><LeaderTemplate>by </LeaderTemplate></CSForum:ForumData>
                                                    <CSForum:ForumData Property="MostRecentPostDate" runat="server" IncludeTimeInDate="true" Tag="Div" />
                                                </ContentTemplate>
                                            </CSControl:PlaceHolder>
                                            &nbsp;
								        </td>
								        <td class="CommonListCell ForumGroupTotalThreadsColumn" align="center"><CSForum:ForumData runat="server" Property="TotalThreads" /></td>
								        <td class="CommonListCell ForumGroupTotalPostsColumn" align="center"><CSForum:ForumData runat="server" Property="TotalPosts" /></td>
							        </tr>
						        </ItemTemplate>
						        <FooterTemplate>
							        </tbody>
							        </table>				
						        </FooterTemplate>
					        </CSForum:ForumList>
				        </ContentTemplate>
			        </CSControl:GroupCollapsableArea>
			    </div>
            </ItemTemplate>
        </CSForum:GroupList>
    	
	    <CSControl:UserOnlineList runat="server" ShowHeaderFooterOnNone="false">
            <LeaderTemplate>
                <div class="CommonInlineMessageArea">
                    <CSControl:SiteUrl runat="server" UrlName="WhoIsOnline" ResourceName="WhoIsOnline_Current" Tag="H4" CssClass="CommonInlineMessageTitle" />
                    <div class="CommonInlineMessageContent">
                        <CSControl:UsersOnlineData runat="server" Property="GuestCount" ResourceName="WhoIsOnlineView_GuestUsers" />
                        <CSControl:UsersOnlineData runat="server" Property="MemberCount" ResourceName="WhoIsOnlineView_UsersOnlineCount" />
            </LeaderTemplate>
            <QueryOverrides QueryType="AuthenticatedUsers" PageSize="999" />
	        <HeaderTemplate>- </HeaderTemplate>
	        <ItemTemplate><CSControl:UserOnlineData runat="server" Property="DisplayName" LinkTo="Profile" /></ItemTemplate>
            <SeparatorTemplate>, </SeparatorTemplate>
            <NoneTemplate></NoneTemplate>
            <TrailerTemplate>
                    </div>
                </div>
            </TrailerTemplate>
        </CSControl:UserOnlineList>

        <CSControl:PlaceHolder runat="server">
            <DisplayConditions><CSControl:SiteSettingsPropertyComparison ComparisonProperty1="EnableSiteStatistics" Operator="IsSetOrTrue" runat="server" /></DisplayConditions>
            <ContentTemplate>
                <div class="CommonInlineMessageArea">
	                <CSControl:ResourceControl runat="server" ResourceName="SearchView_Title" Tag="H4" CssClass="CommonInlineMessageTitle" />
	                <div class="CommonInlineMessageContent">
	                    <CSControl:SiteStatisticsData runat="server" Property="TotalUsers" Tag="B" />
		                <CSControl:ResourceControl runat="server" ResourceName="SearchView_Inline1" />
		                <CSControl:SiteStatisticsData runat="server" Property="TotalThreads" Tag="B" />
		                <CSControl:ResourceControl runat="server" ResourceName="SearchView_Inline2" />
		                <CSControl:SiteStatisticsData runat="server" Property="TotalPosts" Tag="B" />
		                <CSControl:ResourceControl runat="server" ResourceName="SearchView_Inline3" />
		                <br />
		                <br />
		                <CSControl:ResourceControl runat="server" ResourceName="SearchView_Inline4" />
		                <CSControl:SiteStatisticsData runat="server" Property="NewThreadsInPast24Hours" Tag="B" />
		                <CSControl:ResourceControl runat="server" ResourceName="SearchView_Inline5" />
		                <CSControl:SiteStatisticsData runat="server" Property="NewPostsInPast24Hours" Tag="B" />
		                <CSControl:ResourceControl runat="server" ResourceName="SearchView_Inline6" />
		                <CSControl:SiteStatisticsData runat="server" Property="NewUsersInPast24Hours" Tag="B" />
		                <CSControl:ResourceControl runat="server" ResourceName="SearchView_Inline7" />.
		                <br />
		                <br />
		                <CSControl:ResourceControl runat="server" ResourceName="SearchView_Past3days" />
		                <CSControl:ResourceControl runat="server" ResourceName="SearchView_Inline8" />
		                &quot;<CSControl:SiteStatisticsData runat="server" LinkTo="MostViewsPost" Property="MostViewsSubject" />&quot;.
		                <CSControl:ResourceControl runat="server" ResourceName="SearchView_Inline10" />
		                &quot;<CSControl:SiteStatisticsData runat="server" LinkTo="MostReadPost" Property="MostReadPostSubject" />&quot;.
		                <CSControl:ResourceControl runat="server" ResourceName="SearchView_Inline9" />
		                &quot;<CSControl:SiteStatisticsData runat="server" LinkTo="MostActivePost" Property="MostActiveSubject" />&quot;.
		                <br />
		                <br />					
		                <CSControl:ResourceControl runat="server" ResourceName="SearchView_Inline12" />
		                <CSControl:SiteStatisticsData runat="server" LinkTo="NewestUserProfile" Property="NewestUser" Tag="B" />.
	                </div>
                </div>
            </ContentTemplate>
        </CSControl:PlaceHolder>
        
     	<div align="right" class="CommonFeedArea">
		    <CSForum:AggregateRss ID="AggregateRss1" runat="server">
		        <DisplayConditions Operator="Not">
		            <CSControl:CurrentSiteUrlCondition ID="CurrentSiteUrlCondition1" runat="server" SiteUrlName="forums.user_MyForums" />
		        </DisplayConditions>
		        <ContentTemplate><CSControl:Image ID="Image1" ImageUrl="~/utility/images/rss.gif" BorderWidth="0" runat="server" /></ContentTemplate>
		    </CSForum:AggregateRss>
	    </div>

	</div>
	
	<CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdBottom runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
</div>
</asp:Content>
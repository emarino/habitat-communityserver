<%@ Page Language="C#" AutoEventWireup="true" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="System.Drawing" %>

<script language="C#" runat="server">

    protected ThemeConfigurationData ThemeData = CSContext.Current.ThemePreviewCookie.IsPreviewing() ? ThemeConfigurationDatas.GetThemeConfigurationData("leanandgreen", CSContext.Current.ThemePreviewCookie.PreviewID, true) : ThemeConfigurationDatas.GetThemeConfigurationData("leanandgreen", true);

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Page.Response.ContentType = "text/css";
   
        Page.Response.Expires = 30;
    }

    protected Color AdjustIntensity(Color color, int amount)
    {
        int r = color.R;
        if (r + amount > 255)
            r = 255;
        else if (r + amount < 0)
            r = 0;
        else
            r += amount;

        int g = color.G;
        if (g + amount > 255)
            g = 255;
        else if (g + amount < 0)
            g = 0;
        else
            g += amount;

        int b = color.B;
        if (b + amount > 255)
            b = 255;
        else if (b + amount < 0)
            b = 0;
        else
            b += amount;
        
        return Color.FromArgb((byte) r, (byte) g, (byte) b); 
    }

    protected string UrlOrNone(Uri url)
    {
        if (url == null)
            return "none";
        else
            return "url(" + ResolveUrl(url.ToString()) + ")";
    }

</script>

body, html
{
    background-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("siteBackgroundColor", ColorTranslator.FromHtml("#606060"))) %>;
    background-image: <%= UrlOrNone(ThemeData.GetUrlValue("siteBackgroundImage", null))%>;
}

body, html, .CommonContent
{
    font-family: <%= ThemeData.GetStringValue("textFont", "Tahoma, Arial, Helvetica") %>;
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("textColor", Color.Black)) %>;
}

A:LINK, .CommonSidebarArea A:LINK
{
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("linkColor", ColorTranslator.FromHtml("#698d73")))%>;
}

A:ACTIVE, .CommonSidebarArea A:ACTIVE
{
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("activeLinkColor", ColorTranslator.FromHtml("#698d73")))%>;
}

A:VISITED, .CommonSidebarArea A:VISITED
{
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("visitedLinkColor", ColorTranslator.FromHtml("#88aa88")))%>;
}

SELECT, TEXTAREA, INPUT, BUTTON
{
    font-family: <%= ThemeData.GetStringValue("textFont", "Tahoma, Arial, Helvetica") %>;
}

#CommonLeftColumn, #CommonRightColumn
{
    background-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("sidebarBackgroundColor", ColorTranslator.FromHtml("#eff7e0")))%>;
    background-image: <%= UrlOrNone(ThemeData.GetUrlValue("sidebarBackgroundImage", null))%>;
}

#Common
{
    border-top-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("siteTitleTopBorderColor", ColorTranslator.FromHtml("#bbde79")))%>;
}

.CommonTitleBar
{
    background-image: <%= UrlOrNone(ThemeData.GetUrlValue("siteTitleBackgroundImage", null))%>;
    background-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("siteTitleBackgroundColor", ColorTranslator.FromHtml("#ffffff")))%>;
}

.CommonTitle, .CommonProfileTitle, .CommonTitle A:LINK, .CommonTitle A:ACTIVE, .CommonTitle A:VISITED, .CommonTitle A:HOVER, .ForumThreadPostTitle
{
    font-family: <%= ThemeData.GetStringValue("titleFont", "Tahoma, Arial, Helvetica") %>;
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("titleColor", ColorTranslator.FromHtml("#91723f"))) %>;
}

.CommonTitle, .CommonProfileTitle, .CommonModalTitle, .CommonModalTitle, .JoinTitle, .CommonMessageTitle
{
    font-size: <%= ThemeData.GetStringValue("titleFontSize", "180%")%>;
}

.CommonModalTitle, .CommonModalTitle, .JoinTitle, .CommonMessageTitle
{
    font-family: <%= ThemeData.GetStringValue("titleFont", "Tahoma, Arial, Helvetica") %>;
    background-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("boxedContentHeaderBackgroundColor", ColorTranslator.FromHtml("#d2e6bd")))%>;
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("boxedContentHeaderTextColor", ColorTranslator.FromHtml("#91723f")))%>;
}

.CommonTitleBarTitle
{
    font-size: <%= ThemeData.GetStringValue("siteTitleFontSize", "250%")%>;
}

.CommonTitleBarTitle, .CommonTitleBarTitle A:LINK, .CommonTitleBarTitle A:VISITED, .CommonTitleBarTitle A:ACTIVE, .CommonTitleBarDescription
{
    font-family: <%= ThemeData.GetStringValue("siteTitleFont", "Tahoma, Arial, Helvetica") %>;
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("siteTitleColor", ColorTranslator.FromHtml("#555555"))) %>;
}

.CommonSubTitle, .CommonHeader, .CommonFormTitle
{
    font-family: <%= ThemeData.GetStringValue("headerFont", "Tahoma, Arial, Helvetica") %>;
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("headerColor", ColorTranslator.FromHtml("#999999"))) %>;
}

.CommonSidebarHeader, .CommonSidebarTopRound div
{
    background-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("sidebarHeaderBackgroundColor", ColorTranslator.FromHtml("#d2e6bd")))%>;
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("sidebarHeaderTextColor", ColorTranslator.FromHtml("#698d73")))%>;
}

.CommonSidebarContent, .CommonSidebarFooter
{
    border-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("sidebarHeaderBackgroundColor", ColorTranslator.FromHtml("#d2e6bd")))%>;
}

.CommonSidebarHeader A:LINK, .CommonSidebarHeader A:VISITED, .CommonSidebarHeader A:ACTIVE, .CommonSidebarHeader A:HOVER
{
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("sidebarHeaderTextColor", ColorTranslator.FromHtml("#698d73")))%>;
}

.CommonListHeaderLeftMost, .CommonListHeader
{
}

.CommonListTitle, .CommonListTitle A:LINK, .CommonListTitle A:VISITED, .CommonListTitle A:ACTIVE, .CommonListTitle A:HOVER, .CommonInlineMessageTitle, 
.ForumPostHeader, .FileFileTitle, .FileCommentsTitle, .CommonInlineMessageTitle A:LINK, .CommonInlineMessageTitle A:VISITED, .CommonInlineMessageTitle A:ACTIVE, 
.CommonInlineMessageTitle A:HOVER
{
    font-family: <%= ThemeData.GetStringValue("headerFont", "Tahoma, Arial, Helvetica") %>;
    background-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("boxedContentHeaderBackgroundColor", ColorTranslator.FromHtml("#d2e6bd")))%>;
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("boxedContentHeaderTextColor", ColorTranslator.FromHtml("#698d73")))%>;
}

.CommonTag1 a:link, .CommonTag1 a:visited, .CommonTag1 a:active
{
    color: <%= ColorTranslator.ToHtml(AdjustIntensity(ThemeData.GetColorValue("linkColor", ColorTranslator.FromHtml("#698d73")), -85))%>;
}

.CommonTag2 a:link, .CommonTag2 a:visited, .CommonTag2 a:active
{
    color: <%= ColorTranslator.ToHtml(AdjustIntensity(ThemeData.GetColorValue("linkColor", ColorTranslator.FromHtml("#698d73")), -68))%>;
}

.CommonTag3 a:link, .CommonTag3 a:visited, .CommonTag3 a:active
{
    color: <%= ColorTranslator.ToHtml(AdjustIntensity(ThemeData.GetColorValue("linkColor", ColorTranslator.FromHtml("#698d73")), -51))%>;
}

.CommonTag4 a:link, .CommonTag4 a:visited, .CommonTag4 a:active
{
    color: <%= ColorTranslator.ToHtml(AdjustIntensity(ThemeData.GetColorValue("linkColor", ColorTranslator.FromHtml("#698d73")), -34))%>;
}

.CommonTag5 a:link, .CommonTag5 a:visited, .CommonTag5 a:active
{
    color: <%= ColorTranslator.ToHtml(AdjustIntensity(ThemeData.GetColorValue("linkColor", ColorTranslator.FromHtml("#698d73")), -17))%>;
}

.CommonTag6 a:link, .CommonTag6 a:visited, .CommonTag6 a:active
{
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("linkColor", ColorTranslator.FromHtml("#698d73")))%>;
}

A.ForumGroupNameRead, .ForumGroupNameRead:LINK, .ForumGroupNameRead:VISITED, A.ForumNameRead, .ForumNameRead:LINK, .ForumNameRead:VISITED
{
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("visitedLinkColor", ColorTranslator.FromHtml("#88aa88")))%>;
}

A.ForumGroupNameUnRead, .ForumGroupNameUnRead:LINK, .ForumGroupNameUnRead:VISITED, A.ForumNameUnRead, .ForumNameUnRead:LINK, .ForumNameUnRead:VISITED
{
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("linkColor", ColorTranslator.FromHtml("#698d73")))%>;
}

.CommonContextMenuItemHover, .FileFolderTreeNodeSelected, .FileFolderTreeNodeHover, .ForumPostTreeNodeSelected, .ForumPostTreeNodeHover
{
    background-color: <%= ColorTranslator.ToHtml(AdjustIntensity(ThemeData.GetColorValue("boxedContentHeaderBackgroundColor", ColorTranslator.FromHtml("#d2e6bd")), 9))%>;
}

.CommonTabBar
{
    background-image: <%= UrlOrNone(ThemeData.GetUrlValue("navigationTabBackgroundImage", new Uri("~/themes/leanandgreen/images/common/top_group_bg.gif", UriKind.RelativeOrAbsolute)))%>;
    background-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("navigationTabBackgroundColor", ColorTranslator.FromHtml("#cccccc")))%>;
}

.CommonSimpleTabStripSelectedTab, .CommonSimpleTabStripTabHover
{
    background-image: <%= UrlOrNone(ThemeData.GetUrlValue("navigationTabSelectedBackgroundImage", new Uri("~/themes/leanandgreen/images/common/top_item_selected_bg.gif", UriKind.RelativeOrAbsolute)))%>;
    background-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("navigationTabSelectedBackgroundColor", ColorTranslator.FromHtml("#cccccc")))%>;
}

.CommonSimpleTabStripTab, .CommonSimpleTabStripTab A:LINK, .CommonSimpleTabStripTab A:VISITED, .CommonSimpleTabStripTab A:ACTIVE, .CommonSimpleTabStripTab A:HOVER,
.CommonSimpleTabStripTabHover, .CommonSimpleTabStripTabHover A:LINK, .CommonSimpleTabStripTabHover A:VISITED, .CommonSimpleTabStripTabHover A:ACTIVE, .CommonSimpleTabStripTabHover A:HOVER,
.CommonSimpleTabStripSelectedTab, .CommonSimpleTabStripSelectedTab A:LINK, .CommonSimpleTabStripSelectedTab A:VISITED, .CommonSimpleTabStripSelectedTab A:ACTIVE, .CommonSimpleTabStripSelectedTab A:HOVER
{
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("navigationTabTextColor", ColorTranslator.FromHtml("#000000")))%>;
}

.CommonSimpleTabStripTab, .CommonSimpleTabStripTabHover, .CommonSimpleTabStripSelectedTab, .CommonTabBarInner
{
    border-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("navigationTabSeparatorColor", ColorTranslator.FromHtml("#999999"))) %>;
}

#CommonFooter
{
    background-image: <%= UrlOrNone(ThemeData.GetUrlValue("siteFooterBackgroundImage", null))%>;
    background-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("siteFooterBackgroundColor", ColorTranslator.FromHtml("#d3d3d3")))%>;
}

.CommonSidebarHeader, .CommonSidebarContent, .CommonSidebarFooter
{
    width: <%= ThemeData.GetUnitValue("sidebarWidth", Unit.Parse("174px")) %>;
}

#CommonOuter
{
    width: <%= ThemeData.GetUnitValue("width", Unit.Parse("956px")) %>;
}

.CommonTitleBar, .CommonTitleBarImage
{
    height: <%= ThemeData.GetUnitValue("siteTitleHeight", Unit.Parse("120px")) %>;
}

<%= ThemeData.GetStringValue("cssOverrides", string.Empty) %>
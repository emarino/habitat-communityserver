<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Galleries.Controls.CSGalleryThemePage" MasterPageFile="galleries.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Galleries.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="GallerySidebar" Src="gallerysidebar.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        SetTitle(this.CurrentGallery.AboutTitle, true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
    
    <div class="CommonContentArea">
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <CSControl:UserAvatar runat="server" Tag="Td" CssClass="OwnerAvatar" />
                <td>
                    <CSGallery:GalleryData Property="Name" LinkTo="ViewGallery" runat="server" Tag="h2" CssClass="CommonTitle" />
                    <div class="CommonContent">
                        <CSGallery:GalleryData Property="Description" runat="server" />
                        <div class="ClearLeft">&nbsp;</div>
                    </div>
                </td>
            </tr>
        </table>
    </div>

	<div class="CommonContentArea">
	    <div class="CommonContent">
		    <CSGallery:GalleryData Property="AboutTitle" runat="server" Tag="H3" CssClass="ContentHeader" />
    	    
    	    <CSGallery:GalleryData Property="AboutDescription" runat="server" />
	    </div>
	</div>
    
</asp:Content>

<asp:Content ContentPlaceHolderID="rcr" runat="server">
    <CSUserControl:GallerySidebar runat="server" />
</asp:Content>

<%@ Page EnableViewState="true" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Galleries.Controls.CSGalleryThemePage" MasterPageFile="galleries.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Galleries.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="GallerySidebar" Src="gallerysidebar.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        SetTitle(ResourceManager.GetString("Gallery_Email_Subscriptions"), true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">

    <div class="CommonContentArea">
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <CSControl:UserAvatar runat="server" Tag="Td" CssClass="OwnerAvatar" />
                <td>
                    <CSGallery:GalleryData Property="Name" LinkTo="ViewGallery" runat="server" Tag="h2" CssClass="CommonTitle" />
                    <div class="CommonContent">
                        <CSGallery:GalleryData Property="Description" runat="server" />
                        <div class="ClearLeft">&nbsp;</div>
                    </div>
                </td>
            </tr>
        </table>
    </div>

	<div class="CommonContentArea">
	    <div class="CommonContent">
	        <CSGallery:EmailSubscriptionsForm runat="server" 
	            GallerySubscriptionRadioButtonListId="GallerySubscriptionList" 
	            SaveGallerySubscriptionButtonId="SaveGallerySubscription" 
	            PostSubscriptionsCheckBoxListId="PostList" 
	            UnsubscribeAllPostsButtonId="UnSubAll" 
	            UnsubscribeSelectedPostsButtonId="UnSub" 
	            NoPostSubscriptionsControlIds="NoSubscriptionsMessage"
	            >
	            <FormTemplate>
	            
	                <h3 class="ContentHeader"><CSControl:ResourceControl ResourceName = "Gallery_Email_Subscriptions" runat="Server" /></h3>

	                <asp:RadioButtonList id="GallerySubscriptionList" runat="Server" />
	                <p>
	                    <CSControl:ResourceLinkButton CssClass="CommonTextButton" ResourceName="Gallery_Email_GlobalButton" id="SaveGallerySubscription" runat="Server" />
	                </p>

	                <h3 class="ContentHeader"><CSControl:ResourceControl ResourceName="Gallery_Email_Individaul" runat="Server" /></h3>
	                <p>
	                    <CSControl:ResourceLinkButton CssClass="CommonTextButton" ResourceName="Gallery_Email_UnSubButton" id="UnSub" runat="Server" />
	                    <CSControl:ResourceLinkButton CssClass="CommonTextButton" ResourceName="Gallery_Email_UnSubAllButton" id="UnSubAll" runat="Server" />
	                </p>
        	        
	                <asp:CheckBoxList id="PostList" runat="Server" RepeatColumns="1" RepeatDirection="Horizontal" RepeatLayout="Table" />
        	        
                    <CSControl:ResourceControl ResourceName="Gallery_Email_NoSubscriptions" runat="Server" id="NoSubscriptionsMessage" Visible="false" Tag="P" />
	            </FormTemplate>
	        </CSGallery:EmailSubscriptionsForm>
	    </div>
	</div>
    
</asp:Content>

<asp:Content ContentPlaceHolderID="rcr" runat="server">
    <CSUserControl:GallerySidebar runat="server" />
</asp:Content>
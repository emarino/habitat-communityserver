<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Controls.CSThemePage" MasterPageFile="../Common/master.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        SetTitle(ResourceManager.GetString("WhoIsOnlineView_Title"), Title, true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">

<div class="CommonContentArea">
    <CSControl:WrappedLiteral ID="Title" runat="server" Tag="H2" CssClass="CommonTitle" />
	<div class="CommonContent">
	    <CSControl:ResourceControl Tag="Div" CssClass="CommonDescription" ResourceName="WhoIsOnlineView_Description" runat="server" />

		<div class="CommonListArea">
		    <CSControl:UsersOnlineData runat="server" Property="MemberCount" ResourceName="WhoIsOnlineView_UsersOnlineCount" Tag="H4" CssClass="CommonListTitle" />
			<CSControl:UserOnlineList runat="server">
			    <QueryOverrides QueryType="AuthenticatedUsers" PageSize="9999" />
				<HeaderTemplate>
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<thead>
							<tr> 
								<th class="CommonListHeaderLeftMost" align="center" nowrap="nowrap"><CSControl:ResourceControl runat="server" ResourceName="WhoIsOnlineView_List_Username" /></th>
								<th class="CommonListHeader" align="center" nowrap="nowrap"><CSControl:ResourceControl runat="server" ResourceName="WhoIsOnlineView_List_LastUpdated" /></th>
								<th class="CommonListHeader" align="center"><CSControl:ResourceControl runat="server" ResourceName="WhoIsOnlineView_List_ForumLocation" /></th>
							</tr>
						</thead>
						<tbody>
				</HeaderTemplate>
				<ItemTemplate>
						<tr>
							<td class="CommonListCellLeftMost"><CSControl:UserOnlineData Property="DisplayName" LinkTo="Profile" runat="server" /></td>
							<td class="CommonListCell" align="center"><CSControl:UserOnlineData Property="LastActivity" runat="server" IncludeTimeInDate="true" /></td>
							<td class="CommonListCell" align="left">&nbsp;<CSControl:UserOnlineData Property="Location" LinkTo="CurrentLocation" runat="server" /></td>
						</tr>
				</ItemTemplate>	
				<FooterTemplate>
					</tbody>
					</table>
				</FooterTemplate>
			</CSControl:UserOnlineList>
		</div>
		
		<div class="CommonListArea">
		    <CSControl:UsersOnlineData runat="server" Property="GuestCount" ResourceName="WhoIsOnlineView_GuestUsers" Tag="H4" CssClass="CommonListTitle" />
			<CSControl:UserOnlineList runat="server">
			    <QueryOverrides QueryType="AnonymousUsers" PageSize="9999" />
				<HeaderTemplate>
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<thead>
							<tr> 
								<th class="CommonListHeader" align="center" nowrap="nowrap"><CSControl:ResourceControl runat="server" ResourceName="WhoIsOnlineView_List_LastUpdated" /></th>
								<th class="CommonListHeader" align="center"><CSControl:ResourceControl runat="server" ResourceName="WhoIsOnlineView_List_ForumLocation" /></th>
							</tr>
						</thead>
						<tbody>
				</HeaderTemplate>
				<ItemTemplate>
						<tr>
						    <td class="CommonListCellLeftMost"><CSControl:UserOnlineData Property="LastActivity" runat="server" IncludeTimeInDate="true" /></td>
							<td class="CommonListCell" align="left">&nbsp;<CSControl:UserOnlineData Property="Location" LinkTo="CurrentLocation" runat="server" /></td>
						</tr>
				</ItemTemplate>
				<FooterTemplate>
					</tbody>
					</table>
				</FooterTemplate>
			</CSControl:UserOnlineList>
		</div>
	</div>
</div>

</asp:Content>

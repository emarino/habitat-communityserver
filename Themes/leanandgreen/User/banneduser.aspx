<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Controls.CSThemePage" MasterPageFile="../Common/master.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        SetTitle(ResourceManager.GetString("UserBanned_Title"), Title, true);

        Body.Text = string.Format(ResourceManager.GetString("UserBanned_Message"),
            CurrentCSContext.User.BannedUntil.ToString(CurrentCSContext.User.Profile.DateFormat), 
            CurrentCSContext.User.BannedUntil.ToString(CurrentCSContext.SiteSettings.TimeFormat),
            ResourceManager.GetString("Utility_UserBanReason_" + CurrentCSContext.User.BanReason.ToString()));
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">

<div align="center">
    <div class="CommonMessageArea">
        <CSControl:WrappedLiteral runat="server" Tag="H4" CssClass="CommonMessageTitle" ID="Title" />
        <CSControl:WrappedLiteral runat="server" Tag="Div" CssClass="CommonMessageContent" ID="Body" />
    </div>
</div>

</asp:Content>

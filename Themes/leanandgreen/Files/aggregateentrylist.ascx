<%@ Control Language="C#" AutoEventWireup="true"  %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Files.Components" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        if (!string.IsNullOrEmpty(Page.Request.QueryString["Sort"]))
        {
            try
            {
                AggregateEntryList.QueryOverrides.SortBy = (EntriesSortBy)Enum.Parse(typeof(EntriesSortBy), Page.Request.QueryString["Sort"], true);
            }
            catch { }
        }
    }

</script>

<CSControl:ModifiedUrl ResourceName="Files_PortalEntryListing_TitleDownloads" ResourceFile="FileGallery.xml" QueryStringModification="Sort=Downloads" runat="server">
    <DisplayConditions Operator="And">
        <CSControl:QueryStringPropertyValueComparison QueryStringProperty="Sort" Operator="IsSetOrTrue" runat="server" />    	                
        <CSControl:Conditions runat="server" Operator="Not">
            <CSControl:QueryStringPropertyValueComparison QueryStringProperty="Sort" ComparisonValue="Downloads" Operator="EqualTo" runat="server" />
        </CSControl:Conditions>
    </DisplayConditions>
</CSControl:ModifiedUrl>
<CSControl:ModifiedUrl Tag="B" ResourceName="Files_PortalEntryListing_TitleDownloads" ResourceFile="FileGallery.xml" QueryStringModification="Sort=Downloads" runat="server">
    <DisplayConditions Operator="Or">
        <CSControl:QueryStringPropertyValueComparison QueryStringProperty="Sort" ComparisonValue="Downloads" Operator="EqualTo" runat="server" />
        <CSControl:Conditions runat="server" Operator="Not">
            <CSControl:QueryStringPropertyValueComparison QueryStringProperty="Sort" Operator="IsSetOrTrue" runat="server" />    
        </CSControl:Conditions>
    </DisplayConditions>
</CSControl:ModifiedUrl> |

<CSControl:ModifiedUrl ResourceName="Files_PortalEntryListing_TitleRating" ResourceFile="FileGallery.xml" QueryStringModification="Sort=Rating" runat="server">
    <DisplayConditions Operator="Not"><CSControl:QueryStringPropertyValueComparison QueryStringProperty="Sort" ComparisonValue="Rating" Operator="EqualTo" runat="server" /></DisplayConditions>
</CSControl:ModifiedUrl>
<CSControl:ModifiedUrl Tag="B" ResourceName="Files_PortalEntryListing_TitleRating" ResourceFile="FileGallery.xml" QueryStringModification="Sort=Rating" runat="server">
    <DisplayConditions><CSControl:QueryStringPropertyValueComparison QueryStringProperty="Sort" ComparisonValue="Rating" Operator="EqualTo" runat="server" /></DisplayConditions>
</CSControl:ModifiedUrl> |

<CSControl:ModifiedUrl ResourceName="Files_PortalEntryListing_TitlePostDate" ResourceFile="FileGallery.xml" QueryStringModification="Sort=PostDate" runat="server">
    <DisplayConditions Operator="Not"><CSControl:QueryStringPropertyValueComparison QueryStringProperty="Sort" ComparisonValue="PostDate" Operator="EqualTo" runat="server" /></DisplayConditions>
</CSControl:ModifiedUrl>
<CSControl:ModifiedUrl Tag="B" ResourceName="Files_PortalEntryListing_TitlePostDate" ResourceFile="FileGallery.xml" QueryStringModification="Sort=PostDate" runat="server">
    <DisplayConditions ><CSControl:QueryStringPropertyValueComparison QueryStringProperty="Sort" ComparisonValue="PostDate" Operator="EqualTo" runat="server" /></DisplayConditions>
</CSControl:ModifiedUrl> |

<CSControl:ModifiedUrl ResourceName="Files_PortalEntryListing_TitleReplies" ResourceFile="FileGallery.xml" QueryStringModification="Sort=Comments" runat="server">
    <DisplayConditions Operator="Not"><CSControl:QueryStringPropertyValueComparison QueryStringProperty="Sort" ComparisonValue="Comments" Operator="EqualTo" runat="server" /></DisplayConditions>
</CSControl:ModifiedUrl>
<CSControl:ModifiedUrl Tag="B" ResourceName="Files_PortalEntryListing_TitleReplies" ResourceFile="FileGallery.xml" QueryStringModification="Sort=Comments" runat="server">
    <DisplayConditions ><CSControl:QueryStringPropertyValueComparison QueryStringProperty="Sort" ComparisonValue="Comments" Operator="EqualTo" runat="server" /></DisplayConditions>
</CSControl:ModifiedUrl>

<CSFile:EntryList runat="server" ID="AggregateEntryList">
    <QueryOverrides SortBy="Downloads" PageSize="10" SortOrder="Descending" />
    <HeaderTemplate>
        <div class="CommonListArea">
        <h4 class="CommonListTitle">
            <CSControl:ResourceControl ResourceName="Files_PortalEntryListing_TitleDownloads" ResourceFile="FileGallery.xml" runat="server">
                <DisplayConditions Operator="Or">
                    <CSControl:QueryStringPropertyValueComparison QueryStringProperty="Sort" ComparisonValue="Downloads" Operator="EqualTo" runat="server" />
                    <CSControl:Conditions runat="server" Operator="Not">
                        <CSControl:QueryStringPropertyValueComparison QueryStringProperty="Sort" Operator="IsSetOrTrue" runat="server" />    
                    </CSControl:Conditions>
                </DisplayConditions>
            </CSControl:ResourceControl>
	       
            <CSControl:ResourceControl ResourceName="Files_PortalEntryListing_TitleRating" ResourceFile="FileGallery.xml" runat="server">
                <DisplayConditions><CSControl:QueryStringPropertyValueComparison QueryStringProperty="Sort" ComparisonValue="Rating" Operator="EqualTo" runat="server" /></DisplayConditions>
            </CSControl:ResourceControl>

            <CSControl:ResourceControl ResourceName="Files_PortalEntryListing_TitlePostDate" ResourceFile="FileGallery.xml" runat="server">
                <DisplayConditions ><CSControl:QueryStringPropertyValueComparison QueryStringProperty="Sort" ComparisonValue="PostDate" Operator="EqualTo" runat="server" /></DisplayConditions>
            </CSControl:ResourceControl>

            <CSControl:ResourceControl ResourceName="Files_PortalEntryListing_TitleReplies" ResourceFile="FileGallery.xml" runat="server">
                <DisplayConditions ><CSControl:QueryStringPropertyValueComparison QueryStringProperty="Sort" ComparisonValue="Comments" Operator="EqualTo" runat="server" /></DisplayConditions>
            </CSControl:ResourceControl>
        </h4>
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <thead>
            <tr>
	                <th class="CommonListHeaderLeftMost FileFileNameHeader"><CSControl:ResourceControl runat="server" ResourceName="Files_PortalEntryListing_FieldFileName" ResourceFile="FileGallery.xml" /></th>
	                <th class="CommonListHeader FileFileDateHeader"><CSControl:ResourceControl runat="server" ResourceName="Files_PortalEntryListing_FieldDateAdded" ResourceFile="FileGallery.xml" /></th>
	                <th class="CommonListHeader FileFileDownloadCountHeader"><CSControl:ResourceControl runat="server" ResourceName="Files_PortalEntryListing_FieldDownloads" ResourceFile="FileGallery.xml" /></th>
	                <th class="CommonListHeader FileFileActionHeader">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
    </HeaderTemplate>
    <ItemTemplate>
	    <tr>
		    <td class="CommonListCellLeftMost FileFileNameColumn">
			    <CSFile:EntryData Tag="Div" CssClass="FileFileName" Property="Subject" LinkTo="ViewEntry" runat="server">
                    <LeaderTemplate><CSFile:EntryThumbnail ImageAlign="absmiddle" runat="server" ThumbnailSize="Small" /> </LeaderTemplate>
                </CSFile:EntryData>
			    <CSFile:EntryData Tag="Div" CssClass="FileFileDescription" Property="FormattedBody" TruncateAt="200" runat="server" />
                <CSFile:EntryRating Tag="Div" CssClass="FileFileRating" runat="server" IsReadOnly="true" RatingCssClass="CommonRateControl" RatingReadOnlyCssClass="CommonRateControlReadOnly" RatingActiveCssClass="CommonRateControlActive" ImagesBaseUrl="~/Themes/default/images/common/" />
		    </td>
		    <td class="CommonListCell FileFileDateColumn">
		        <CSFile:EntryData Property="PostDate" runat="server" />
		    </td>
		    <td class="CommonListCell FileFileDownloadCountColumn">
		        <CSFile:EntryData Property="Downloads" runat="server" />
		    </td>
		    <td class="CommonListCell FileFileActionColumn">
		        <CSFile:EntryData LinkCssClass="CommonImageTextButton CommonDownloadButton" LinkTo="Download" ResourceName="Files_Button_Download" ResourceFile="FileGallery.xml" runat="server" />

                <CSFile:EntryData runat="server" Tag="Div" CssClass="FileFileDetail" Property="FileSize">
                    <LeaderTemplate><CSControl:ResourceControl runat="server" ResourceName="Files_PortalEntryListing_FieldFileSize" ResourceFile="FileGallery.xml" /> </LeaderTemplate>
                </CSFile:EntryData>
		    </td>
	    </tr>
    </ItemTemplate>
    <NoneTemplate>
	    <tr>
	    <td colspan="5" class="CommonListCellLeftMost"><CSControl:ResourceControl runat="server" ResourceName="NoRecords" ResourceFile="FileGallery.xml" /></td>
	    </tr>
    </NoneTemplate>
    <FooterTemplate>
        </table>
        </div>
    </FooterTemplate>
</CSFile:EntryList>
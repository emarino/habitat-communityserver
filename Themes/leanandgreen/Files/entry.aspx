<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Files.Controls.CSFileThemePage" MasterPageFile="files.Master" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        if (CurrentEntry != null)
            SetTitle(CurrentEntry.Subject, Title, true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
    <div class="CommonContentArea">
	    <CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	        <DefaultContentTemplate>
	            <CSUserControl:AdTop runat="server" />
	        </DefaultContentTemplate>
	    </CSControl:AdPart>

        <CSControl:WrappedLiteral runat="server" ID="Title" Tag="H2" CssClass="CommonTitle"><LeaderTemplate><CSFile:EntryThumbnail ImageAlign="absmiddle" runat="server" ThumbnailSize="Normal" /> </LeaderTemplate></CSControl:WrappedLiteral>
        <div class="CommonContent">
	        <div style="padding-bottom: 8px;">
	            <CSFile:EntryData LinkCssClass="CommonImageTextButton CommonDownloadButton" LinkTo="Download" ResourceName="Files_Button_Download" ResourceFile="FileGallery.xml" runat="server" />
	            <CSFile:EntrySubscriptionToggleButton runat="server" ButtonOnCssClass="CommonImageTextButton CommonEmailSubscriptionButtonEnabled" ButtonOffCssClass="CommonImageTextButton CommonEmailSubscriptionButtonDisabled" ButtonProcessingCssClass="CommonImageTextButton CommonEmailSubscriptionButtonProcessing">
                    <OnTemplate><CSControl:ResourceControl runat="server" ResourceName="PostFlatView_EnableThreadTrackingOn" /></OnTemplate>
				    <OffTemplate><CSControl:ResourceControl runat="server" ResourceName="PostFlatView_EnableThreadTrackingOff" /></OffTemplate>
				    <ProcessingTemplate>...</ProcessingTemplate>
                </CSFile:EntrySubscriptionToggleButton>
                <CSControl:FavoritePopupMenu ResourceName="FavoritePopupMenu_Text" runat="server" ButtonActiveCssClass="CommonImageTextButtonHighlight CommonFavoriteButton" ButtonInactiveCssClass="CommonImageTextButton CommonFavoriteButton" MenuGroupCssClass="CommonContextMenuGroup" MenuItemCssClass="CommonContextMenuItem" MenuItemSelectedCssClass="CommonContextMenuItemHover" MenuItemExpandedCssClass="CommonContextMenuItemExpanded" MenuItemIconWidth="20" MenuItemIconHeight="20" MenuDirection="UpDown" />
			    <CSControl:UserContactPopupMenu ResourceName="ContactPopupMenu_Text" runat="server" ButtonActiveCssClass="CommonImageTextButtonHighlight CommonContactButton" ButtonInactiveCssClass="CommonImageTextButton CommonContactButton" MenuGroupCssClass="CommonContextMenuGroup" MenuItemCssClass="CommonContextMenuItem" MenuItemSelectedCssClass="CommonContextMenuItemHover" MenuItemExpandedCssClass="CommonContextMenuItemExpanded" MenuItemIconWidth="20" MenuItemIconHeight="20" MenuDirection="UpDown" />
			    <CSFile:EntryData LinkCssClass="CommonImageTextButton CommonEditButton" LinkTo="Edit" ResourceName="Files_Button_Edit" ResourceFile="FileGallery.xml" runat="server" />
			    <CSFile:EntryData LinkCssClass="CommonImageTextButton CommonDeleteButton" LinkTo="Delete" ResourceName="Files_Button_Delete" ResourceFile="FileGallery.xml" runat="server" />
		    </div>
		    <div class="FileFileArea">
			    <CSControl:ResourceControl runat="server" ResourceName="Files_FileDetails" ResourceFile="FileGallery.xml" Tag="Div" CssClass="FileFileTitle" />
			    <div class="FileFilePropertiesArea">
				    <table cellpadding="0" cellspacing="0" border="0" width="100%">
					    <tr>
						    <td class="FilePropertyName"><CSControl:ResourceControl runat="server" ResourceName="Files_PortalEntryListing_FieldDownloads" ResourceFile="FileGallery.xml" />:</td>
						    <td class="FileProperty"><CSFile:EntryData runat="server" Property="Downloads" /></td>
						    <td class="FilePropertyName"><CSControl:ResourceControl runat="server" ResourceName="Files_PortalEntryListing_FieldFileSize" ResourceFile="FileGallery.xml" />:</td>
						    <td class="FileProperty"><CSFile:EntryData runat="server" Property="FileSize" /></td>
					    </tr>
					    <tr>
						    <td class="FilePropertyName"><CSControl:ResourceControl runat="server" ResourceName="Files_PortalEntryListing_FieldPostedBy" ResourceFile="FileGallery.xml" />:</td>
						    <td class="FileProperty"><CSControl:UserData runat="server" Property="DisplayName" LinkTo="Profile" /></td>
						    <td class="FilePropertyName"><CSControl:ResourceControl runat="server" ResourceName="Files_PortalEntryListing_FieldViews" ResourceFile="FileGallery.xml" />:</td>
						    <td class="FileProperty"><CSFile:EntryData runat="server" Property="Views" /></td>
					    </tr>
					    <tr>
						    <td class="FilePropertyName"><CSControl:ResourceControl runat="server" ResourceName="Files_PortalEntryListing_FieldDateAdded" ResourceFile="FileGallery.xml" />:</td>
						    <td class="FileProperty"><CSFile:EntryData runat="server" Property="PostDate" /></td>
						    <td colspan="2" class="FileProperty"><CSFile:EntryRating runat="server" RatingCssClass="CommonRateControl" RatingReadOnlyCssClass="CommonRateControlReadOnly" RatingActiveCssClass="CommonRateControlActive" ImagesBaseUrl="~/Themes/default/images/common/" /></td>
					    </tr>
				    </table>
			    </div>
			    <div class="FileFileDescriptionArea">
				    <CSFile:EntryData runat="server" Property="FormattedBody" IncrementViewCount="true" />
				    <p />
				    <CSFile:EntryTagEditableList runat="server" Tag="Div" EditorLinkCssClass="CommonTextButton" EditorCssClass="CommonInlineTagEditor" />
			    </div>
		    </div>
		    
		    <div class="FileCommentsArea">
	            <CSControl:ResourceControl runat="server" ResourceName="Files_UserComments" ResourceFile="FileGallery.xml" Tag="Div" CssClass="FileCommentsTitle" />
	            <div class="FileCommentsContent">

                <CSFile:EntryCommentList runat="server">
		            <HeaderTemplate>
			            <table cellpadding="0" cellspacing="0" border="0" width="100%">
		            </HeaderTemplate>
		            <ItemTemplate>
			            <tr valign="top">
			            <td class="FileCommentArea FileCommentAvatar">
				            <CSControl:UserAvatar runat="server" BorderWidth="1" />&nbsp;
			            </td>
			            <td class="FileCommentArea FileCommentContent" width="100%">
				            <h4 class="FileCommentHeader">
				                <CSFile:EntryCommentData runat="server" Property="DisplayName" LinkTo="User" />
				                <CSControl:ResourceControl runat="server" ResourceName="Weblog_CommentForm_Said" />
				            </h4>
				            <CSFile:EntryCommentData runat="server" Property="FormattedBody" Tag="Div" CssClass="FileCommentText" />
				            <div class="FileCommentFooter">
				                <CSFile:EntryCommentData runat="server" Property="PostDate" IncludeTimeInDate="true" />
				                <CSFile:DeleteEntryCommentForm DeleteButtonId="DeleteComment" ConfirmationResourceName="Weblog_Comment_DeleteVerify" runat="server">
				                    <SuccessActions>
				                        <CSControl:GoToModifiedUrlAction runat="server" />
				                    </SuccessActions>
				                    <FormTemplate>
				                        [<asp:LinkButton runat="server" Text="Delete" ID="DeleteComment" />]
				                    </FormTemplate>
				                </CSFile:DeleteEntryCommentForm>
				            </div>
			            </td>
			            </tr>
		            </ItemTemplate>
		            <NoneTemplate>
			            <tr><td colspan="2" class="FileCommentArea FileCommentContent">
				            <CSControl:ResourceControl runat="server" ResourceName="Files_NoComments" ResourceFile="FileGallery.xml" />
			            </td></tr>
		            </NoneTemplate>
		            <FooterTemplate>
			            </table>
		            </FooterTemplate>
	            </CSFile:EntryCommentList>

                <CSControl:ResourceControl runat="server" ResourceName="Files_Comment_Pending" ResourceFile="FileGallery.xml" Tag="Div" CssClass="CommonMessageSuccess">
                    <DisplayConditions>
                        <CSControl:QueryStringPropertyValueComparison QueryStringProperty="CommentPosted" Operator="IsSetOrTrue" runat="server" />
                    </DisplayConditions>
                    <LeaderTemplate><a name="commentmessage"></a></LeaderTemplate>
                </CSControl:ResourceControl>

                <CSFile:CreateEntryCommentForm runat="server" 
                        MessageTextBoxId="CommentBody" 
                        NameTextBoxId="CommentName" 
                        RememberCheckboxId="CommentRemember" 
                        SubjectTextBoxId="CommentTitle" 
                        SubmitButtonId="CommentSubmit" 
                        UrlTextBoxId="CommentWebsite" 
                        ControlIdsToHideFromRegisteredUsers="RememberWrapper,AnonymousUser"
                        ValidationGroup="CreateCommentForm"
                    >
                        <SuccessActions>
                            <CSControl:GoToModifiedUrlAction runat="server" QueryStringModification="CommentPosted=true" TargetLocationModification="commentmessage" />
                        </SuccessActions>
                        <FormTemplate>
                            <div class="CommonFormArea">
			                    <h4 class="CommonFormTitle"><CSControl:ResourceControl runat="server" ResourceName="CommentListing_AddComment2" ResourceFile="FileGallery.xml" /></h4>
			                    
			                    <div class="FommonFormFieldName"><CSControl:FormLabel runat="server" ResourceName="Title" LabelForId="CommentTitle" /> <em>(<CSControl:ResourceControl runat="server" ResourceName="Required"/>)</em><asp:RequiredFieldValidator runat="server" ErrorMessage="*" ControlToValidate="CommentTitle" ValidationGroup="CreateCommentForm" /></div>
	                            <div class="CommonFormField"><asp:TextBox id="CommentTitle" runat="server"  Columns="60" MaxLength="256" ValidationGroup="CreateCommentForm" /></div>
			                    
			                    <asp:PlaceHolder id="AnonymousUser" runat="server">
				                    <div class="CommonFormFieldName"><CSControl:FormLabel LabelForId="CommentName" runat="server" ResourceName="CommentListing_Name" ResourceFile="FileGallery.xml" /> <em>(<CSControl:ResourceControl runat="server" ResourceName="Required" />)</em></div>
				                    <div class="CommonFormField"><asp:TextBox id="CommentName" runat="server" Columns="40" MaxLength="256" ValidationGroup="CreateCommentForm" /></div>
			                    </asp:PlaceHolder>
                    			
			                    <div class="CommonFormFieldName"><CSControl:FormLabel LabelForId="CommentWebsite" runat="server" ResourceName="CommentListing_Website" ResourceFile="FileGallery.xml" /> <em>(<CSControl:ResourceControl runat="server" ResourceName="Optional" />)</em></div>
			                    <div class="CommonFormField"><asp:TextBox id="CommentWebsite" runat="server" Columns="60" MaxLength="256" ValidationGroup="CreateCommentForm" /></div>

			                    <div class="CommonFormFieldName"><CSControl:FormLabel LabelForId="CommentBody" runat="server" ResourceName="CommentListing_Body" ResourceFile="FileGallery.xml" /> <em>(<CSControl:ResourceControl runat="server" ResourceName="Required" />)</em><asp:RequiredFieldValidator runat="server" ErrorMessage="*" ControlToValidate="CommentBody" ValidationGroup="CreateCommentForm" /></div>
			                    <div class="CommonFormField"><asp:TextBox id="CommentBody" runat="server" Columns="60" Rows="8" MaxLength="3000" TextMode="MultiLine" ValidationGroup="CreateCommentForm" /></div>

                                <asp:PlaceHolder runat="server" id="RememberWrapper">
                                    <div class="CommonFormField"><asp:CheckBox id="CommentRemember" runat="server" Text="Remember Me?" ValidationGroup="CreateCommentForm"></asp:CheckBox></div>
                                </asp:PlaceHolder>

			                    <CSControl:ResourceLinkButton runat="server" id="CommentSubmit" CssClass="CommonTextButton" ResourceName="Add" ResourceFile="FileGallery.xml" ValidationGroup="CreateCommentForm" />
		                    </div>
                        </FormTemplate>
                </CSFile:CreateEntryCommentForm>
	        </div>
		</div>
    	
	    <CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	        <DefaultContentTemplate>
	            <CSUserControl:AdBottom runat="server" />
	        </DefaultContentTemplate>
	    </CSControl:AdPart>

    </div>
</asp:Content>

<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Blogs.Controls.CSBlogThemePage" MasterPageFile="../Common/master.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        if (CurrentGroup != null)
            SetTitle(CurrentGroup.Name, true);
        else
            SetTitle(ResourceManager.GetString("weblogs"), true);
    }

</script>

<asp:Content ContentPlaceHolderID="HeaderRegion" runat="server" >
    <head runat="server">
    <CSControl:Head runat="Server">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <CSControl:Script runat="server" />
        <CSControl:Style runat="server" visible = "true" />
        <CSControl:Style runat="server" Href="../style/Common.css" />
		<CSControl:Style runat="server" Href="../style/Blog.css" />
		<CSControl:Style runat="server" Href="../style/common_print.css" media="print" />
		<CSControl:Style runat="server" Href="../style/blog_print.css" media="print" />
		<CSControl:ThemeStyle runat="server" Href="~/style/DynamicStyle.aspx" EnsureNotCachedOnPreview="true" />
    </CSControl:Head>
    </head>
</asp:Content>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
<% Response.Redirect("/Forums", false); //Added by Kishan on 09 Oct 2007%>

<div class="CommonContentArea">
	<CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdTop runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>

	<div class="CommonContent">
		<CSBlog:GroupData runat="server" Property="Name" Tag="H4" CssClass="CommonSubTitle" />
		<CSControl:ResourceControl ResourceName="LatestPosts" Tag="H4" CssClass="CommonSubTitle" runat="server">
		    <DisplayConditions Operator="Not"><CSControl:QueryStringPropertyValueComparison QueryStringProperty="GroupID" Operator="IsSetOrTrue" runat="server" /></DisplayConditions>
		</CSControl:ResourceControl>
	
		<CSBlog:WeblogPostList runat="Server" >
		    <QueryOverrides PagerID="Pager" IsAggregate="true" BlogMirrorDisplayType="MirrorBlogsOnly" />
	        <HeaderTemplate>
		        <ul class="BlogPostList">
	        </HeaderTemplate>
	        <ItemTemplate>
		        <li class="BlogPostArea">
			        <table cellpadding="0" cellspacing="0" border="0">
				        <tr>
					        <td valign="top" id="CommentCountBox" runat="server">
						        <div class="BlogRating">
						            <CSBlog:WeblogPostData Property="Replies" LinkTo="PostComments" runat="server" CssClass="BlogCommentCount" Tag="Div" />
							        <CSBlog:WeblogPostData ResourceName="Weblog_EntryList_Comment" LinkTo="PostComments" runat="server" Tag="Div" />
							        <CSBlog:WeblogPostData ResourceName="Weblog_EntryList_Views" Property="Views" LinkTo="Post" runat="server" Tag="Div" />
							        <CSBlog:WeblogPostRating id="Rating" runat="server" RatingCssClass="CommonRateControl" RatingReadOnlyCssClass="CommonRateControlReadOnly" RatingActiveCssClass="CommonRateControlActive" ImagesBaseUrl="~/Themes/default/images/common/" />
							        <br />
						        </div>
					        </td>
					        <td width="100%">
					            <CSBlog:WeblogPostData Property="Subject" LinkTo="Post" Tag="H4" CssClass="BlogPostHeader" runat="server" />
					            <CSBlog:WeblogPostData Property="Excerpt" Tag="Div" CssClass="BlogPostContent" runat="server" />
						        <div class="BlogPostFooter">
							        <div>
							            <CSBlog:WeblogPostData Property="UserTime" LinkTo="Post" FormatString="MMM dd yyyy, hh:mm tt" runat="server" />
							            <CSControl:ResourceControl runat="server" ResourceName="Weblog_Aggregate_By" /> 
							            <CSBlog:WeblogPostData Property="DisplayName" LinkTo="AuthorUrl" runat="server" /> 
							            <CSControl:ResourceControl runat="server" ResourceName="To" /> 
							            <CSBlog:WeblogData Property="Name" LinkTo="HomePage" runat="server" />
							        </div>
						            <CSBlog:WeblogPostTagEditableList runat="server" EditorLinkCssClass="CommonTextButton" EditorCssClass="CommonInlineTagEditor" Tag="Div" />
						        </div>
					        </td>
				        </tr>
			        </table>
		        </li>
	        </ItemTemplate>
	        <FooterTemplate>
		        </ul>
	        </FooterTemplate>
        </CSBlog:WeblogPostList>

        <CSControl:SinglePager runat="Server" id="Pager"  />
	</div>
	
	<CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdBottom runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="rcr" runat="server" >
	<div class="CommonSidebar">
	
	    <div class="CommonSidebarArea">
	        <div class="CommonSidebarTopRound"><div class="t1"></div><div class="t2"></div><div class="t3"></div><div class="t4"></div></div>
            <CSControl:ResourceControl ResourceName="GroupOptions" Tag="H4" CssClass="CommonSidebarHeader" runat="server" />
            <div class="CommonSidebarContent"><ul class="CommonSidebarList">
                <%-- The GroupData control will only render if a Group is present --%>
                <CSBlog:GroupData ResourceName="Syndication" LinkTo="GroupRssMirrorBlogsOnly" Tag="Li" CssClass="CommonSidebarRssListItem" runat="server" />
	            
	            <%-- This PlaceHolder control will render only if GroupID is not defined on the querystring --%>
	            <CSControl:PlaceHolder runat="server">
	                <DisplayConditions Operator="Not"><CSControl:QueryStringPropertyValueComparison Operator="IsSetOrTrue" QueryStringProperty="GroupID" runat="server" /></DisplayConditions>
	                <ContentTemplate>
	                    <CSControl:SiteUrl ResourceName="Syndication" UrlName="blog_aggregate_Syndication" Parameter1="-1" Parameter2="MirrorBlogsOnly" Tag="Li" CssClass="CommonSidebarRssListItem" runat="server" />
                    </ContentTemplate>
                </CSControl:PlaceHolder>
            </ul></div>
        </div>
        
        <CSBlog:GroupList runat="server" Tag="Div" CssClass="CommonSidebarArea">
            <QueryOverrides GroupID="-1" />
            <LeaderTemplate>
                <div class="CommonSidebarTopRound"><div class="t1"></div><div class="t2"></div><div class="t3"></div><div class="t4"></div></div>
                <CSControl:ResourceControl ResourceName="Groups" Tag="H4" CssClass="CommonSidebarHeader" runat="server" />
                <div class="CommonSidebarContent">
            </LeaderTemplate>
            <HeaderTemplate><ul class="CommonSidebarList"></HeaderTemplate>
            <ItemTemplate><CSBlog:GroupData Property="Name" runat="server" LinkTo="RollerGroupHome" Tag="Li" /></ItemTemplate>
            <TrailerTemplate></ul></TrailerTemplate>
            <TrailerTemplate>
                </div>
                <CSControl:SiteUrl UrlName="rollerhome" ResourceName="ViewAllGroups" Tag="Div" CssClass="CommonSidebarFooter" runat="server" />
            </TrailerTemplate>
        </CSBlog:GroupList>
        
    </div>
</asp:Content>

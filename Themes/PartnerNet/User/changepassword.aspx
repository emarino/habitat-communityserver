<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Controls.CSThemePage" MasterPageFile="../Common/master.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        SetTitle(ResourceManager.GetString("ChangePassword_Title"), true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">

<div align=center>
<div class="CommonMessageArea">
    <h4 class="CommonMessageTitle"><CSControl:ResourceControl runat="server" ResourceName="ChangePassword_Title" /> <CSControl:UserData UseAccessingUser="true" Property="DisplayName" runat="server" /></h4>
	<div class="CommonMessageContent">
	    <CSControl:ChangePasswordForm runat="server" CurrentPasswordTextBoxId="Password" NewPasswordTextBoxId="NewPassword1" ChangePasswordButtonId="ChangePasswordButton" ChangePasswordCustomValidatorId="ChangePasswordCustomValidator">
	        <SuccessActions>
	            <CSControl:GoToReferralUrlAction runat="server" />
	            <CSControl:GoToSiteUrlAction runat="server" UrlName="user_EditProfile_Clean" />
	        </SuccessActions>
	        <FormTemplate>
		        <div class="CommonFormArea">
		        <div class="CommonFormDescription">
			        <CSControl:ResourceControl runat="server" ResourceName="ChangePassword_Instructions" />
		        </div>
		        <table cellpadding="3" cellspacing="0" border="0" width="100%">            
		            <tr>
		                <td align="right" class="CommonFormField" colspan="2">
					        <div>
						        <asp:CustomValidator id="ChangePasswordCustomValidator" runat="server" Width="100%" CssClass="validationWarning" />
					        </div>
				        </td>
			        </tr>
			        <CSControl:PlaceHolder runat="server">
			            <DisplayConditions>
			                <CSControl:ControlVisibilityCondition runat="server" ControlId="Password" ControlVisiblilityEquals="true" />
			            </DisplayConditions>
			            <ContentTemplate>
			                <tr>
				                <td align="left" class="CommonFormFieldName" nowrap="nowrap">
					                <CSControl:ResourceControl runat="server" ResourceName="ChangePassword_CurrentPassword" />
				                </td>
				                <td align="left" class="CommonFormField" nowrap="nowrap">
					                <asp:TextBox TextMode="Password" runat="server" id="Password" Columns="30" MaxLength="64"/>
					                <asp:RequiredFieldValidator id="ValidatePassword" runat="server" ControlToValidate="Password" Display=Dynamic Cssclass="validationWarning">*</asp:RequiredFieldValidator>
				                </td>
			                </tr>
			            </ContentTemplate>
			        </CSControl:PlaceHolder>
			        <tr>
				        <td align="left" nowrap="nowrap" class="CommonFormFieldName">
					        <CSControl:ResourceControl runat="server" ResourceName="ChangePassword_NewPassword" />
				        </td>
				        <td align="left" class="CommonFormField" nowrap="nowrap">
					        <asp:TextBox TextMode="Password" runat="server" id="NewPassword1" Columns="30" MaxLength="64"/>
					        <asp:RequiredFieldValidator id="ValidatePassword1" runat="server" ControlToValidate="NewPassword1" Display=Dynamic Cssclass="validationWarning">*</asp:RequiredFieldValidator>
				        </td>
			        </tr>        
			        <tr>
				        <td align="left" nowrap="nowrap" class="CommonFormFieldName">
					        <CSControl:ResourceControl runat="server" ResourceName="ChangePassword_ReEnterNewPassword" />
				        </td>
				        <td align="left" class="CommonFormField" nowrap="nowrap">
					        <asp:TextBox TextMode="Password" runat="server" id="NewPassword2" Columns="30" MaxLength="64"/>
					        <asp:RequiredFieldValidator id="ValidatePassword2" runat="server" ControlToValidate="NewPassword2" Display=Dynamic Cssclass="validationWarning">*</asp:RequiredFieldValidator>                 
					        <asp:CompareValidator id="ComparePassword" runat="server" ControlToValidate="NewPassword2" ControlToCompare="NewPassword1" Display=Dynamic Cssclass="validationWarning">*</asp:CompareValidator>
				        </td>        
			        </tr>
			        <tr>
				        <td align="right" class="CommonFormField" colspan="2">
					        <CSControl:ResourceButton id="ChangePasswordButton" runat="server" ResourceName="ChangePassword_ChangePassword" />
				        </td>
			        </tr>            
		        </table>
		        </div>
            </FormTemplate>
        </CSControl:ChangePasswordForm>
	</div>
</div>	
</div>

</asp:Content>
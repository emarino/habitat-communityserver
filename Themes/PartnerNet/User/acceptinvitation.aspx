<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Controls.CSThemePage" MasterPageFile="../Common/master.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        SetTitle(ResourceManager.GetString("AcceptInvitation_Title"), true);
        Message.Text = string.Format(ResourceManager.GetString("AcceptInvitation_AnonymousUser"), Server.HtmlEncode(CurrentUser.Username), Server.HtmlEncode(CurrentUserInvitation.Message), SiteUrls.Instance().UserRegisterWithInvitation(CurrentUserInvitation.InvitationKey), SiteUrls.Instance().LoginWithInvitation(CurrentUserInvitation.InvitationKey));
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">

<div align="center">
	<div class="CommonContentArea" style="width:650px;">
		<h2 class="CommonTitle"><CSControl:ResourceControl runat="server" ResourceName="AcceptInvitation_Title" /></h2>
		<div class="CommonContent">
			<div class="CommonFormArea" >
				<div class="JoinArea">
					<CSControl:WrappedLiteral runat="server" ID="Message" />
				</div>
			</div>
		</div>
	</div>
</div>
<CSControl:AcceptInvitationForm runat="server" />

</asp:Content>


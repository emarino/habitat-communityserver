<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Controls.CSThemePage" MasterPageFile="../Common/master.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        SetTitle(ResourceManager.GetString("CreateNewAccount_Title"), true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">

<div align="center">
<div class="CommonContentArea" style="width:650px;">
<h2 class="CommonTitle">
	<CSControl:ResourceControl runat="server" ResourceName="CreateNewAccount_Title" />
</h2>

<div class="CommonContent">
<div class="CommonFormArea" >
<div class="CommonFormFieldDescription"><CSControl:ResourceControl runat="server" ResourceName="CreateNewAccount_Instructions" ID="Resourcecontrol8"/></div>

<CSControl:CreateUserForm runat="server" 
    AcceptAgreementCheckBoxId="AcceptAgreement" 
    AcceptAgreementHyperLinkId="AcceptAgreementLink" 
    AllowSitePartnersToContactCheckBoxId="AllowSitePartnersToContact" 
    AllowSiteToContactCheckBoxId="AllowSiteToContact" 
    CreateButtonId="CreateAccount" 
    CreateUserCustomValidatorId="CreateUserCustomValidator" 
    EmailAddressTextBoxId="Email" 
    PasswordTextBoxId="Password" 
    SubFormIds="" 
    TimeZoneDropDownListId="Timezone" 
    UserNameTextBoxId="Username"
    >
    <FormTemplate>
        <h4 class="JoinTitle">
            <CSControl:ResourceControl runat="server" ResourceName="CreateNewAccount_Required">
                <DisplayConditions>
                    <CSControl:Conditions Operator="not" runat="server">
                        <CSControl:SiteSettingsPropertyValueComparison ComparisonProperty="AccountActivation" Operator="EqualTo" ComparisonValue="Email" runat="server" />
                    </CSControl:Conditions>
                </DisplayConditions>
            </CSControl:ResourceControl>
            <CSControl:ResourceControl runat="server" ResourceName="CreateNewAccount_Required_ForEmailActivation">
                <DisplayConditions>
                    <CSControl:SiteSettingsPropertyValueComparison ComparisonProperty="AccountActivation" Operator="EqualTo" ComparisonValue="Email" runat="server" />
                </DisplayConditions>
            </CSControl:ResourceControl>
        </h4>
        <div class="JoinArea">
	        <table cellpadding="0" cellspacing="0" border="0">
	        <tr>
	            <td class="CommonFormFieldDescription" align="left" colspan="2">
	                <asp:CustomValidator runat="server" ID="CreateUserCustomValidator" />
                </td>
	        </tr>
	        <tr>
		        <td align="right">
			        <CSControl:ResourceControl runat="server" ResourceName="CreateNewAccount_UserName" />
		        </td>
		        <td align="left">
			        <div class="CommonFormField">
				        <asp:TextBox id="Username" MaxLength="64" runat="server" columns="40" onkeyup="validateForm(this);" />
				        <asp:RequiredFieldValidator EnableClientScript="false" id="usernameValidator" runat="server" ControlToValidate="Username" Cssclass="validationWarning">*</asp:RequiredFieldValidator>
			        </div>
		        </td>
	        </tr>
	        <CSControl:PlaceHolder runat="server">
	            <DisplayConditions Operator="Or">
	                <CSControl:Conditions runat="server" Operator="Not">
	                    <CSControl:SiteSettingsPropertyValueComparison ComparisonProperty="AccountActivation" ComparisonValue="Email" runat="server" Operator="EqualTo" />
                    </CSControl:Conditions>	                    
	                <CSControl:UserInRoleCondition runat="server" UseAccessingUser="true" Role="SystemAdministrator" />
	                <CSControl:UserInRoleCondition runat="server" UseAccessingUser="true" Role="MembershipAdministrator" />
	            </DisplayConditions>
	            <ContentTemplate>
	                <tr>
		                <td align="right">
				                <CSControl:ResourceControl runat="server" ResourceName="CreateNewAccount_PasswordDescription" />
		                </td>

		                <td align="left">
			                <div class="CommonFormField">
				                <asp:TextBox id="Password" MaxLength="64" TextMode="Password" runat="server" columns="40" onkeyup ="validateForm(this);" />
				                <asp:RequiredFieldValidator id="passwordValidator" runat="server" ControlToValidate="Password" Cssclass="validationWarning">*</asp:RequiredFieldValidator>
			                </div>
		                </td>
	                </tr>
	                <tr>
		                <td align="right">
				                <CSControl:ResourceControl runat="server" ResourceName="CreateNewAccount_ReEnterPassword" />
		                </td>
		                <td align="left">
			                <div class="CommonFormField">
				                <asp:TextBox id="Password2" MaxLength="64" TextMode="Password" runat="server" columns="40" onkeyup ="validateForm(this);" />
				                <asp:RequiredFieldValidator id="password2Validator" runat="server" ControlToValidate="Password2" Cssclass="validationWarning">*</asp:RequiredFieldValidator>
				                <asp:comparevalidator id="ComparePassword" runat="server" ControlToValidate="Password2" ControlToCompare="Password" Cssclass="validationWarning">*</asp:comparevalidator>
			                </div>
		                </td>
	                </tr>
                </ContentTemplate>
            </CSControl:PlaceHolder>
	        <tr>
		        <td align="right">
                    <CSControl:ResourceControl runat="server" ResourceName="CreateNewAccount_Email" />
		        </td>
		        <td align="left" colspan="2">
			        <div class="CommonFormField">
				        <asp:TextBox id="Email" runat="server" MaxLength="128" columns="40" onkeyup="validateForm(this);" />
				        <CSControl:ResourceControl runat="server" ResourceName="CreateNewAccount_EmailDescription" ID="Resourcecontrol6"/>
				        <asp:RequiredFieldValidator id="emailValidator" runat="server" ControlToValidate="Email" Cssclass="validationWarning">*</asp:RequiredFieldValidator>
			        </div>
		        </td>
	        </tr>
	        <tr>
		        <td align="right">
                    <CSControl:ResourceControl runat="server" ResourceName="CreateNewAccount_ReEnterEmail" />
		        </td>
		        <td align="left" colspan="2">
			        <div class="CommonFormField">
				        <asp:TextBox id="Email2" runat="server" MaxLength="128" columns="40" onkeyup ="validateForm(this);" />
				        <asp:RequiredFieldValidator id="email2Validator" runat="server" ControlToValidate="Email2" Cssclass="validationWarning">*</asp:RequiredFieldValidator>
				        <asp:comparevalidator id="CompareEmail" runat="server" ControlToValidate="Email2" ControlToCompare="Email" Cssclass="validationWarning">*</asp:comparevalidator>
			        </div>
		        </td>
	        </tr>
	        <tr>
		        <td align="right">
			        <CSControl:ResourceControl runat="server" resourcename="CreateNewAccount_Timezone" />
		        </td>
		        <td>
			        <div class="CommonFormField">
			            <asp:DropDownList id="Timezone" runat="server" />
			        </div>
		        </td>
	        </tr>
	        
	        <CSControl:PlaceHolder runat="server">
	            <DisplayConditions><CSControl:SiteSettingsPropertyValueComparison runat="server" ComparisonProperty="ShowContactCheckboxes" Operator="IsSetOrTrue" /></DisplayConditions>
	            <ContentTemplate>
		            <tr>
			            <td align="right">
				            <CSControl:ResourceControl runat="server" resourcename="CreateNewAccount_AllowSiteToContact" />
			            </td>
			            <td	align="left" colspan="2">
				            <div class="CommonFormField">
				                <asp:CheckBox id="AllowSiteToContact" runat="server" />
				            </div>
			            </td>
		            </tr>
		            <tr>
			            <td align="right">
				            <CSControl:ResourceControl runat="server" resourcename="CreateNewAccount_AllowSitePartnersToContact" />
			            </td>
			            <td align="left" colspan="2">
				            <div class="CommonFormField">
				                <asp:CheckBox id="AllowSitePartnersToContact" runat="server" />
				            </div>
			            </td>
		            </tr>
	            </ContentTemplate>
	        </CSControl:PlaceHolder>
	        
	        <CSControl:PlaceHolder runat="server">
	            <DisplayConditions><CSControl:SiteSettingsPropertyValueComparison runat="server" ComparisonProperty="TermsOfServiceUrl" Operator="IsSetOrTrue" /></DisplayConditions>
	            <ContentTemplate>
	                <tr>
		                <td align="right">
			                <CSControl:ResourceControl runat="server" resourcename="CreateNewAccount_ForumRules" />
		                </td>
		                <td>
			                <asp:CheckBox id="AcceptAgreement" Runat="server" onclick="validateForm(this);" /><asp:HyperLink runat="server" id="AcceptAgreementLink"  />
							<CSControl:SiteSettingsData LinkTo="TermsOfService" LinkTarget="_blank" ResourceName="CreateNewAccount_ForumRulesDesc" runat="server" />
			                <CSControl:RequiredCheckBoxValidator EnableClientScript="false" id="RequiredAcceptAgreement" runat="server" ControlToValidate="AcceptAgreement" Cssclass="validationWarning">*</CSControl:RequiredCheckBoxValidator>
		                </td>
	                </tr>
                </ContentTemplate>
            </CSControl:PlaceHolder>
	        </table>
        </div>

        </div>
        </div>
        </div>
        
        <CSControl:ResourceButton ResourceName="CreateNewAccount_CreateAccount" CssClass="CommonBigButton" id="CreateAccount" Runat="server" disabled="disabled" />
		        
    </FormTemplate>
</CSControl:CreateUserForm>

</div>
</div>

<script type = "text/javascript">
// <![CDATA[
function validateForm(item)
{
	var state = true;
	if(item.value == '')
		state = false;

	if(state){ state = checkInput('<%= CSControlUtility.Instance().FindControl(this, "Username").ClientID %>');}
	if(state){ state = checkInput('<%= CSControlUtility.Instance().FindControl(this, "Password").ClientID %>');}
	if(state){ state = checkInput('<%= CSControlUtility.Instance().FindControl(this, "Password2").ClientID %>');}
	if(state){ state = checkInput('<%= CSControlUtility.Instance().FindControl(this, "Email").ClientID %>');}
	if(state){ state = checkInput('<%= CSControlUtility.Instance().FindControl(this, "Email2").ClientID %>');}

	if(state)
	{
		var cb = document.getElementById('<%= CSControlUtility.Instance().FindControl(this, "AcceptAgreement").ClientID %>');
		if(cb != null)
		    state = cb.checked;

	}


	document.getElementById('<%= CSControlUtility.Instance().FindControl(this, "CreateAccount").ClientID %>').disabled = !state;
}

function checkInput(id)
{
	var item = document.getElementById(id);
	return item == null || item.value.replace(/^\s+|\s+$/g, '') != '';
}
// ]]>
</script>

</asp:Content>

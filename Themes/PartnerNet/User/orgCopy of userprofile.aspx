<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Controls.CSThemePage" MasterPageFile="../Common/master.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<%@ Import Namespace="PartnerNetEncryptDecrypt" %>
<%@ Import Namespace="PartnernetGlobalFunctions" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        if (CurrentCSContext.SiteSettings.RequireAuthenticationForProfiles && CurrentCSContext.User.IsAnonymous)
            Response.Redirect(SiteUrls.Instance().Login, true);
    
        if (CurrentUser != null)
            SetTitle(CurrentUser.DisplayName, true);

        //Added by Kishan to redirect the user from CS user profile page to SharePoint user profile page
        string SymmetricKey = GlobalFunctions.GetPartnerNetConfig("SymmetricKey");

        if (CurrentUser.DisplayName.IndexOf("\\") > 0)
            Response.Redirect(System.Configuration.ConfigurationSettings.AppSettings["PartnerNetWebURL"].ToString() + "WPP/UserDisp.aspx?UserName=" + HttpUtility.UrlEncode(EncryptDecrypt.Encrypt(CurrentUser.Username, SymmetricKey)), false);
        else        
            Response.Redirect(System.Configuration.ConfigurationSettings.AppSettings["PartnerNetWebURL"].ToString() + "WPP/UserDisp.aspx?UserName=" + HttpUtility.UrlEncode(EncryptDecrypt.Encrypt(System.Configuration.ConfigurationSettings.AppSettings["PartnerNet.ProviderName"].ToString() + ":" + CurrentUser.Username, SymmetricKey)), false);	
    }

</script>

<asp:Content ContentPlaceHolderID="lcr" runat="server">
<div class="CommonSidebar">

    <div class="CommonSidebarArea">
        <div class="CommonSidebarTopRound"><div class="t1"></div><div class="t2"></div><div class="t3"></div><div class="t4"></div></div>
        <h4 class="CommonSidebarHeader"><CSControl:UserData Property="DisplayName" runat="server" /></h4>
        <div class="CommonSidebarContent">
        
            <div style="text-align: center; margin-bottom: 8px;">
		        <CSControl:UserAvatar runat="server" />
		    </div>
	        
            <table cellpadding="0" cellspacing="0" border="0">
                <CSControl:UserData runat="server" Property="DateCreated">
                    <LeaderTemplate>
                        <tr>
                            <td class="CommonFormField">
                                <CSControl:ResourceControl runat="server" ResourceName="ViewUserProfile_Joined" />
                            </td>
                            <td class="CommonFormField">
                    </LeaderTemplate>
                    <TrailerTemplate>
                            </td>
                        </tr>
                    </TrailerTemplate>
                </CSControl:UserData>
                <CSControl:UserData runat="server" Property="LastActivity">
                    <LeaderTemplate>
                        <tr>
                            <td class="CommonFormField">
                                <CSControl:ResourceControl runat="server" ResourceName="ViewUserProfile_LastVisit" />
                            </td>
                            <td class="CommonFormField">
                    </LeaderTemplate>
                    <TrailerTemplate>
                            </td>
                        </tr>
                    </TrailerTemplate>
                </CSControl:UserData>
                <CSControl:UserProfileData runat="server" Property="Timezone">
                    <LeaderTemplate>
                        <tr>
                            <td class="CommonFormField">
                                <CSControl:ResourceControl runat="server" ResourceName="ViewUserProfile_Timezone" />
                            </td>
                            <td class="CommonFormField">
                    </LeaderTemplate>
                    <TrailerTemplate>
                                 GMT
                            </td>
                        </tr>
                    </TrailerTemplate>
                </CSControl:UserProfileData>
                <CSControl:UserProfileData runat="server" Property="Location">
                    <LeaderTemplate>
                        <tr>
                            <td class="CommonFormField">
                                <CSControl:ResourceControl runat="server" ResourceName="ViewUserProfile_Location" />
                            </td>
                            <td class="CommonFormField">
                     </LeaderTemplate>
                     <TrailerTemplate>
                            </td>
                        </tr>
                     </TrailerTemplate>
                </CSControl:UserProfileData>
                <CSControl:UserProfileData runat="server" Property="Occupation">
                    <LeaderTemplate>
                        <tr>
                            <td class="CommonFormField">
                                <CSControl:ResourceControl runat="server" ResourceName="ViewUserProfile_Occupation" />
                            </td>
                            <td class="CommonFormField">
                    </LeaderTemplate>
                    <TrailerTemplate>
                            </td>
                        </tr>
                    </TrailerTemplate>
                </CSControl:UserProfileData>
                <CSControl:UserProfileData runat="server" Property="Interests">
                    <LeaderTemplate>
                        <tr>
                            <td class="CommonFormField">
                                <CSControl:ResourceControl runat="server" ResourceName="ViewUserProfile_Interests" />
                            </td>
                            <td class="CommonFormField">
                    </LeaderTemplate>
                    <TrailerTemplate>
                            </td>
                        </tr>
                    </TrailerTemplate>
                </CSControl:UserProfileData>
                <CSControl:UserProfileData runat="server" Property="BirthDate">
                    <LeaderTemplate>
                        <tr>
                            <td class="CommonFormField">
                                <CSControl:ResourceControl runat="server" ResourceName="ViewUserProfile_Birthday" />
                            </td>
                            <td class="CommonFormField">
                                <div>
                    </LeaderTemplate>
                    <TrailerTemplate>
                                </div>
                                <CSControl:UserProfileData runat="server" Property="Age" ResourceName="ViewUserProfile_Age" Tag="Div" />
                            </td>
                        </tr>
                    </TrailerTemplate>
                </CSControl:UserProfileData>
                <CSControl:UserData runat="server" Property="TotalPosts" LinkTo="PostsSearch">
                    <LeaderTemplate>
                        <tr>
                            <td class="CommonFormField">
                                <CSControl:ResourceControl runat="server" ResourceName="ViewUserProfile_TotalPosts" />
                            </td>
                            <td class="CommonFormField">
                     </LeaderTemplate>
                     <TrailerTemplate>
                            </td>
                        </tr>
                     </TrailerTemplate>
                </CSControl:UserData>
                <CSControl:UserData runat="server" Property="PostRank">
                    <LeaderTemplate>
                        <tr>
                            <td class="CommonFormField">
                                <CSControl:ResourceControl runat="server" ResourceName="ViewUserProfile_PostRank" />
                            </td>
                            <td class="CommonFormField">
                     </LeaderTemplate>
                     <TrailerTemplate>
                            </td>
                        </tr>
                     </TrailerTemplate>
                </CSControl:UserData>
            
                <CSControl:UserData runat="server" Property="Points">
                    <LeaderTemplate>
                        <tr>
                            <td class="CommonFormField">
                                <CSControl:ResourceControl runat="server" ResourceName="ViewUserProfile_Points" />
                            </td>
                            <td class="CommonFormField">
                    </LeaderTemplate>
                    <TrailerTemplate>
                            </td>
                        </tr>
                    </TrailerTemplate>
                </CSControl:UserData>
            </table>
        </div>
    </div>
        
    <div class="CommonSidebarArea">
        <div class="CommonSidebarTopRound"><div class="t1"></div><div class="t2"></div><div class="t3"></div><div class="t4"></div></div>
        <h4 class="CommonSidebarHeader"><CSControl:ResourceControl runat="server" ResourceName="Profile_Options_Title" /></h4>
        <div class="CommonSidebarContent">
            <table cellpadding="0" cellspacing="0" border="0">
            <CSControl:UserData LinkTo="EditProfile" Property="DisplayName" ResourceName="Profile_EditProfile" runat="server">
                <LeaderTemplate><tr><td class="CommonFormField"><CSControl:ThemeImage ImageUrl="~/images/forum/edit.gif" runat="server" /></td><td class="CommonFormField"></LeaderTemplate>
                <TrailerTemplate></td></tr></TrailerTemplate>
            </CSControl:UserData>
            <CSControl:FavoriteToggleButton runat="server" ToggleFavoriteType="User">
                <OnTemplate><CSControl:UserData Property="DisplayName" ResourceName="FavoritePopupMenu_User_Remove" runat="server" /></OnTemplate>
                <OffTemplate><CSControl:UserData Property="DisplayName" ResourceName="FavoritePopupMenu_User_Add" runat="server" /></OffTemplate>
                <ProcessingTemplate>...</ProcessingTemplate>
                <LeaderTemplate><tr><td class="CommonFormField"><CSControl:ThemeImage ImageUrl="~/images/common/favorite.gif" runat="server" /></td><td class="CommonFormField"></LeaderTemplate>
                <TrailerTemplate></td></tr></TrailerTemplate>
            </CSControl:FavoriteToggleButton>
            <CSControl:UserData runat="server" Property="DisplayName" ResourceName="Profile_SendPrivateMessage" LinkTo="PrivateMessage" LinkTarget="_blank">
                <LeaderTemplate><tr><td class="CommonFormField"><CSControl:ThemeImage ImageUrl="~/images/common/privatemessage.gif" runat="server" /></td><td class="CommonFormField"></LeaderTemplate>
                <TrailerTemplate></td></tr></TrailerTemplate>
            </CSControl:UserData>
            <CSControl:UserData runat="server" Property="DisplayName" ResourceName="Profile_SendEmail" LinkTo="SendEmail" >
                <LeaderTemplate><tr><td class="CommonFormField"><CSControl:ThemeImage ImageUrl="~/images/common/email.gif" runat="server" /></td><td class="CommonFormField"></LeaderTemplate>
                <TrailerTemplate></td></tr></TrailerTemplate>
            </CSControl:UserData>
            <CSControl:UserData LinkTo="WebAddress" Property="DisplayName" ResourceName="Profile_ViewUserWebSite" runat="server" LinkTarget="_blank" >
                <LeaderTemplate><tr><td class="CommonFormField"><CSControl:ThemeImage ImageUrl="~/images/common/homepage.gif" runat="server" /></td><td class="CommonFormField"></LeaderTemplate>
                <TrailerTemplate></td></tr></TrailerTemplate>
            </CSControl:UserData>
            <CSControl:UserData LinkTo="Weblog" Property="DisplayName" ResourceName="Profile_ViewUserBlog" runat="server" LinkTarget="_blank" >
                <LeaderTemplate><tr><td class="CommonFormField"><CSControl:ThemeImage ImageUrl="~/images/common/weblog.gif" runat="server" /></td><td class="CommonFormField"></LeaderTemplate>
                <TrailerTemplate></td></tr></TrailerTemplate>
            </CSControl:UserData>
            <CSControl:UserData LinkTo="WebGallery" Property="DisplayName" ResourceName="Profile_ViewUserGallery" runat="server" LinkTarget="_blank" >
                <LeaderTemplate><tr><td class="CommonFormField"><CSControl:ThemeImage ImageUrl="~/images/common/webgallery.gif" runat="server" /></td><td class="CommonFormField"></LeaderTemplate>
                <TrailerTemplate></td></tr></TrailerTemplate>
            </CSControl:UserData>
            <CSControl:UserData LinkTo="PostsSearch" Property="DisplayName" ResourceName="Profile_SearchUserPosts" runat="server" LinkTarget="_blank" >
                <LeaderTemplate><tr><td class="CommonFormField"><CSControl:ThemeImage ImageUrl="~/images/common/search.gif" runat="server" /></td><td class="CommonFormField"></LeaderTemplate>
                <TrailerTemplate></td></tr></TrailerTemplate>
            </CSControl:UserData>
            <CSControl:PlaceHolder runat="server">
                <DisplayConditions Operator="Not"><CSControl:UserPropertyValueComparison runat="server" UseAccessingUser="true" ComparisonProperty="IsAnonymous" Operator="IsSetOrTrue" /></DisplayConditions>
                <ContentTemplate>
                    <CSControl:UserProfileData Property="MsnIM" runat="server">
                        <LeaderTemplate><tr><td class="CommonFormField"><CSControl:ThemeImage ImageUrl="~/images/post_button_msnm.gif" runat="server" /></td><td class="CommonFormField"></LeaderTemplate>
                        <TrailerTemplate></td></tr></TrailerTemplate>
                    </CSControl:UserProfileData>
                    <CSControl:UserProfileData Property="AolIM" runat="server">
                        <LeaderTemplate><tr><td class="CommonFormField"><CSControl:ThemeImage ImageUrl="~/images/post_button_aim.gif" runat="server" /></td><td class="CommonFormField"></LeaderTemplate>
                        <TrailerTemplate></td></tr></TrailerTemplate>
                    </CSControl:UserProfileData>
                    <CSControl:UserProfileData Property="YahooIM" runat="server">
                        <LeaderTemplate><tr><td class="CommonFormField"><CSControl:ThemeImage ImageUrl="~/images/post_button_yahoo.gif" runat="server" /></td><td class="CommonFormField"></LeaderTemplate>
                        <TrailerTemplate></td></tr></TrailerTemplate>
                    </CSControl:UserProfileData>
                    <CSControl:UserProfileData Property="IcqIM" runat="server">
                        <LeaderTemplate><tr><td class="CommonFormField"><CSControl:ThemeImage ImageUrl="~/images/post_button_icq.gif" runat="server" /></td><td class="CommonFormField"></LeaderTemplate>
                        <TrailerTemplate></td></tr></TrailerTemplate>
                    </CSControl:UserProfileData>
                </ContentTemplate>
            </CSControl:PlaceHolder>
            </table>
        </div>
    </div>
</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="bcr" runat="server">

<div class="CommonContentArea">
	<CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdTop runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
	<h2 class="CommonTitle"><CSControl:UserData Property="CommonNameOrUserName" ResourceName="ViewUserProfile_AboutBioFormat" runat="server" /></h2>
	<div class="CommonContent">
	
        <CSControl:UserProfileData Property="Bio" runat="server" />
        <CSControl:ResourceControl runat="server" ResourceName="ViewUserProfile_BioMissing"><DisplayConditions Operator="Not"><CSControl:UserProfilePropertyValueComparison ComparisonProperty="Bio" Operator="IsSetOrTrue" runat="server" /></DisplayConditions></CSControl:ResourceControl>
		
		<p />
        <div class="CommonInlineMessageArea">
        <h3 class="CommonInlineMessageTitle"><CSControl:ResourceControl runat="server" ResourceName="ViewUserProfile_Tab_RssFeeds" /></h3>
        <div class="CommonInlineMessageContent">
            <div class="CommonHalfWidthArea">
	            <div class="CommonHalfWidthContent">
                    <CSControl:DelayedContent runat="server" LoadImmediately="true">
                        <ContentTemplate>
                            <div class="CommonInlineListArea">
                                <h4 class="CommonInlineListHeader"><CSControl:UserData LinkTo="PostsSearch" ResourceName="ViewUserProfile_Tab_RecentPosts" runat="server" /></h4>
                                <CSControl:IndexPostList runat="server">
                                    <QueryOverrides PageSize="5" QueryType="ByCurrentUser" SortBy="DateDescending" />
                                    <HeaderTemplate><ul class="CommonInlineList"></HeaderTemplate>
                                    <ItemTemplate>
                                        <li>
                                            <CSControl:IndexPostData runat="server" Property="Title" LinkTo="Post" Tag="Div" />
                                            <CSControl:IndexPostData runat="server" Property="FormattedBody" Tag="Div" TruncateAt="100" />
                                        </li>
                                    </ItemTemplate>
                                    <FooterTemplate></ul></FooterTemplate>
                                </CSControl:IndexPostList>
                            </div>
                        </ContentTemplate>		        
                        <NotLoadedTemplate>
                            <CSControl:UserData LinkTo="PostsSearch" ResourceName="ViewUserProfile_Tab_RecentPosts" runat="server" />
                        </NotLoadedTemplate>
	                </CSControl:DelayedContent>
	            </div>
	        </div>
        
		<CSControl:TemporaryRssFeedList runat="server">
		    <QueryOverrides QueryType="ForCurrentUser" />
		    <ItemTemplate>
		        <div class="CommonHalfWidthArea">
		            <div class="CommonHalfWidthContent">
                        <CSControl:DelayedContent runat="server" LoadImmediately="true">
                            <ContentTemplate>
                                <CSControl:PlaceHolder runat="server">
                                    <DisplayConditions><CSControl:TemporaryRssFeedPropertyValueComparison runat="server" ComparisonProperty="TotalItems" Operator="GreaterThan" ComparisonValue="0" /></DisplayConditions>
                                    <ContentTemplate>
                                        <div class="CommonInlineListArea">
                                            <h4 class="CommonInlineListHeader"><CSControl:TemporaryRssFeedData runat="server" Property="Title" LinkTo="Feed" /></h4>
                                            <CSControl:TemporaryRssFeedItemList runat="server">
                                                <QueryOverrides PageSize="5" />
                                                <HeaderTemplate><ul class="CommonInlineList"></HeaderTemplate>
                                                <ItemTemplate>
                                                    <li>
                                                        <CSControl:TemporaryRssFeedItemData runat="server" Property="Title" LinkTo="FeedItem" Tag="Div" />
                                                        <CSControl:TemporaryRssFeedItemData runat="server" Property="Description" Tag="Div" TruncateAt="100" />
                                                    </li>
                                                </ItemTemplate>
                                                <FooterTemplate></ul></FooterTemplate>
                                            </CSControl:TemporaryRssFeedItemList>
                                        </div>
                                    </ContentTemplate>
                                </CSControl:PlaceHolder>
                                <CSControl:PlaceHolder runat="server">
                                    <DisplayConditions Operator="Not"><CSControl:TemporaryRssFeedPropertyValueComparison runat="server" ComparisonProperty="TotalItems" Operator="GreaterThan" ComparisonValue="0" /></DisplayConditions>
                                    <ContentTemplate>
                                        <CSControl:TemporaryRssFeedData runat="server" LinkTo="Feed" Property="Url" />
                                    </ContentTemplate>
                                </CSControl:PlaceHolder>
                            </ContentTemplate>		        
                            <NotLoadedTemplate>
                                <CSControl:TemporaryRssFeedData runat="server" LinkTo="Feed" Property="Url" />
                            </NotLoadedTemplate>
		                </CSControl:DelayedContent>
		            </div>
		        </div>
		        <div class="CommonHalfWidthBreak"></div>
		    </ItemTemplate>
		    <AlternatingItemTemplate>
		        <div class="CommonHalfWidthArea">
		            <div class="CommonHalfWidthContent">
                        <CSControl:DelayedContent runat="server" LoadImmediately="true">
                            <ContentTemplate>
                                <CSControl:PlaceHolder runat="server">
                                    <DisplayConditions><CSControl:TemporaryRssFeedPropertyValueComparison runat="server" ComparisonProperty="TotalItems" Operator="GreaterThan" ComparisonValue="0" /></DisplayConditions>
                                    <ContentTemplate>
                                        <div class="CommonInlineListArea">
                                            <h4 class="CommonInlineListHeader"><CSControl:TemporaryRssFeedData runat="server" Property="Title" LinkTo="Feed" /></h4>
                                            <CSControl:TemporaryRssFeedItemList runat="server">
                                                <QueryOverrides PageSize="5" />
                                                <HeaderTemplate><ul class="CommonInlineList"></HeaderTemplate>
                                                <ItemTemplate>
                                                    <li>
                                                        <CSControl:TemporaryRssFeedItemData runat="server" Property="Title" LinkTo="FeedItem" Tag="Div" />
                                                        <CSControl:TemporaryRssFeedItemData runat="server" Property="Description" Tag="Div" TruncateAt="100" />
                                                    </li>
                                                </ItemTemplate>
                                                <FooterTemplate></ul></FooterTemplate>
                                            </CSControl:TemporaryRssFeedItemList>
                                        </div>
                                    </ContentTemplate>
                                </CSControl:PlaceHolder>
                                <CSControl:PlaceHolder runat="server">
                                    <DisplayConditions Operator="Not"><CSControl:TemporaryRssFeedPropertyValueComparison runat="server" ComparisonProperty="TotalItems" Operator="GreaterThan" ComparisonValue="0" /></DisplayConditions>
                                    <ContentTemplate>
                                        <CSControl:TemporaryRssFeedData runat="server" LinkTo="Feed" Property="Url" />
                                    </ContentTemplate>
                                </CSControl:PlaceHolder>
                            </ContentTemplate>		        
                            <NotLoadedTemplate>
                                <CSControl:TemporaryRssFeedData runat="server" LinkTo="Feed" Property="Url" />
                            </NotLoadedTemplate>
		                </CSControl:DelayedContent>
		            </div>
		        </div>
		    </AlternatingItemTemplate>
		    <NoneTemplate></NoneTemplate>
		</CSControl:TemporaryRssFeedList>
		<div class="CommonHalfWidthBreak"></div>
		</div>
		</div>
		
        
	    <CSControl:UserList runat="server" ShowHeaderFooterOnNone="false">
	        <QueryOverrides QueryType="CurrentUserFavorites" PageSize="999999" />
		    <HeaderTemplate>
		        <p />
		        <div class="CommonInlineMessageArea">
	            <h4 class="CommonInlineMessageTitle"><CSControl:ResourceControl runat="server" resourcename="ViewUserProfile_Tab_FavoriteUsers" /></h4>
	            <div class="CommonInlineMessageContent">
		    </HeaderTemplate>
		    <ItemTemplate>
		        <div class="CommonAvatarListArea">
		            <CSControl:UserData LinkTo="Profile" runat="server"><ContentTemplate>
                        <CSControl:UserAvatar runat="server" Width="60" Height="60"><TrailerTemplate><br /></TrailerTemplate></CSControl:UserAvatar>
                        <CSControl:UserData runat="server" Property="DisplayName" />
                    </ContentTemplate></CSControl:UserData>
                </div>
		    </ItemTemplate>
		    <FooterTemplate>
			    <div style="clear: both;"></div>
                </div>
                </div>
		    </FooterTemplate>
		    <NoneTemplate></NoneTemplate>
	    </CSControl:UserList>
	    
	    <CSControl:UserList runat="server" ShowHeaderFooterOnNone="false">
	        <DisplayConditions>
                <CSControl:CurrentUserIsAccessingUserCondition runat="server" />
            </DisplayConditions>
	        <QueryOverrides QueryType="UsersWatchingAccessingUser" PageSize="999999" />
		    <HeaderTemplate>
		        <p />
		        <div class="CommonInlineMessageArea">
	            <h4 class="CommonInlineMessageTitle"><CSControl:ResourceControl runat="server" ResourceName="ViewUserProfile_Tab_UserWatchingYou" /></h4>
	            <div class="CommonInlineMessageContent">
		    </HeaderTemplate>
		    <ItemTemplate>
		        <div class="CommonAvatarListArea">
		            <CSControl:UserData LinkTo="Profile" runat="server"><ContentTemplate>
                        <CSControl:UserAvatar runat="server" Width="60" Height="60"><TrailerTemplate><br /></TrailerTemplate></CSControl:UserAvatar>
                        <CSControl:UserData runat="server" Property="DisplayName" />
                    </ContentTemplate></CSControl:UserData>
                </div>
		    </ItemTemplate>
		    <FooterTemplate>
			    <div style="clear: both;"></div>
                </div>
                </div>
		    </FooterTemplate>
		    <NoneTemplate></NoneTemplate>
	    </CSControl:UserList>
			
        <CSControl:PlaceHolder runat="server">
            <DisplayConditions Operator="And">
                <CSControl:CurrentUserIsAccessingUserCondition runat="server" />
                <CSControl:ApplicationPropertyValueComparison runat="server" ApplicationType="Forum" ComparisonProperty="Enabled" Operator="IsSetOrTrue" />
            </DisplayConditions>
            <ContentTemplate>
                <p />
                <div class="CommonListArea">
                <h4 class="CommonListTitle"><CSControl:ResourceControl ResourceName="ViewUserProfile_Tab_UnreadPrivateMessages" runat="server" /></h4>
	            <CSForum:ThreadList runat="server" ID="FilteredThreadList" ShowHeaderFooterOnNone="true">
	                <QueryOverrides PageSize="20" ForumID="0" UserFilter="All" UnReadOnly="true" />
	                <HeaderTemplate>
		                <table width="100%" cellpadding="0" cellspacing="0" border="0">
		                <thead>
		                <tr>
                        <th class="CommonListHeaderLeftMost" align="center">
					        <CSControl:ResourceControl runat="server" ResourceName="PrivateMessages_Messages" />
				        </th>
				        <th class="CommonListHeader" align="center" nowrap="nowrap">
					        <CSControl:ResourceControl runat="server" ResourceName="PrivateMessages_Recipients" />
				        </th>
				        <th class="CommonListHeader" align="center" nowrap="nowrap">
					        <CSControl:ResourceControl runat="server" ResourceName="ViewThreads_TitleReplies" />
				        </th>
				        <th class="CommonListHeader" align="center" nowrap="nowrap">
					        <CSControl:ResourceControl runat="server" ResourceName="ViewThreads_TitleLastPost" />
				        </th>
				        </tr>
				        </thead>
				        <tbody>
	                </HeaderTemplate>
		            <ItemTemplate>
				            <tr>
					            <td class="CommonListCellLeftMost">
					                <table cellpadding="0" cellspacing="0" border="0">
								        <tr>
								            <td rowspan="2" style="padding: 2px;"><%# CommunityServer.Discussions.Components.ForumFormatter.StatusIcon((CommunityServer.Discussions.Components.Thread)Container.DataItem)%></td>
									        <td  style="padding: 2px;"><%# Formatter.GetEmotionMarkup(((CommunityServer.Discussions.Components.Thread)Container.DataItem).EmoticonID)%>
										        <CSForum:ThreadData Property="Subject" TruncateAt="65" LinkTo="Thread" LinkCssClass="ForumNameRead" runat="server"><DisplayConditions><CSForum:ThreadPropertyComparison ComparisonProperty1="HasRead" Operator="IsSetOrTrue" runat="server" /></DisplayConditions></CSForum:ThreadData>
								                <CSForum:ThreadData Property="Subject" TruncateAt="65" LinkTo="Thread" LinkCssClass="ForumNameUnRead" runat="server"><DisplayConditions Operator="Not"><CSForum:ThreadPropertyComparison ComparisonProperty1="HasRead" Operator="IsSetOrTrue" runat="server" /></DisplayConditions></CSForum:ThreadData>
										        <CSForum:ThreadPostPageLinks runat="server" />
									        </td>
									        <td style="padding: 2px;">
										        <CSForum:ThreadRating runat="server" IsReadOnly="true" RatingCssClass="CommonRateControl" RatingReadOnlyCssClass="CommonRateControlReadOnly" RatingActiveCssClass="CommonRateControlActive" ImagesBaseUrl="~/Themes/default/images/common/" />
                                                <CSControl:PlaceHolder runat="server">
									                <DisplayConditions Operator="And">
									                    <CSForum:ForumConfigurationPropertyComparison ComparisonProperty1="EnableThreadStatus" Operator="IsSetOrTrue" runat="server" />
									                    <CSForum:ThreadPropertyValueComparison ComparisonProperty="Status" Operator="EqualTo" ComparisonValue="NotAnswered" runat="server" />
                                                    </DisplayConditions>
									                <ContentTemplate><CSControl:ThemeImage ImageUrl="~/images/status_NotAnswered.gif" runat="server" AlternateTextResourceName="Status_NotAnswered" /></ContentTemplate>
									            </CSControl:PlaceHolder>
        									    
									            <CSControl:PlaceHolder runat="server">
									                <DisplayConditions Operator="And">
									                    <CSForum:ForumConfigurationPropertyComparison ComparisonProperty1="EnableThreadStatus" Operator="IsSetOrTrue" runat="server" />
									                    <CSForum:ThreadPropertyValueComparison ComparisonProperty="Status" Operator="EqualTo" ComparisonValue="Answered" runat="server" />
                                                    </DisplayConditions>
									                <ContentTemplate><CSControl:ThemeImage ImageUrl="~/images/status_Answered.gif" runat="server" AlternateTextResourceName="Status_Answered" /></ContentTemplate>
									            </CSControl:PlaceHolder>
									        </td>
								        </tr>
								        <tr> 
									        <td colspan="2" style="padding: 2px;">
										        <CSControl:ResourceControl runat="server" ResourceName="ForumGroupView_Legend_LastPostBy" /><CSControl:UserData Property="DisplayName" LinkTo="Profile" runat="server" />
									        </td>
								        </tr>
							        </table>
					            </td>
					            <td class="CommonListCell" align="center">
							        <%# CommunityServer.Discussions.Components.ForumFormatter.FormatPrivateMessageRecipients((CommunityServer.Discussions.Components.PrivateMessage)Container.DataItem)%>
						        </td>
						        <td class="CommonListCell" align="center">
						            <CSForum:ThreadData Property="Replies" runat="server" />
						        </td>
						        <td class="CommonListCell" align="center">
					                <CSControl:PlaceHolder runat="server">
		                                <DisplayConditions><CSForum:ThreadPropertyValueComparison ComparisonProperty="ThreadDate" ComparisonValue="6/8/1980" Operator="GreaterThan" runat="server" /></DisplayConditions>
		                                <ContentTemplate>
                                            <CSForum:ThreadData LinkTo="MostRecentPostAuthor" Property="MostRecentPostAuthor" runat="server" TruncateAt="15"><LeaderTemplate>by </LeaderTemplate><TrailerTemplate><br /></TrailerTemplate></CSForum:ThreadData>
                                            <CSForum:ThreadData Property="ThreadDate" runat="server" IncludeTimeInDate="true" />
                                        </ContentTemplate>
                                    </CSControl:PlaceHolder>
					            </td>
				            </tr>				
		            </ItemTemplate>
		            <FooterTemplate>
		                </tbody>
	                    </table>
		            </FooterTemplate>
		            <NoneTemplate>
		                <tr><td colspan="4" class="CommonListCellLeftMost" align="center">
		                    <CSControl:ResourceControl runat="server" ResourceName="ViewUserProfile_NoUnreadPrivateMessages" />
		                </td></tr>
		            </NoneTemplate>
	            </CSForum:ThreadList>
	            <CSControl:UserData runat="server" LinkTo="UserPrivateMessages" ResourceName="ViewUserProfile_ViewAllPrivateMessages" Tag="Div" style="padding-top: .5em;" />
	            <p />
	            </div>
	            </div>
            </ContentTemplate>
        </CSControl:PlaceHolder>
	</div>
	<CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdBottom runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
</div>

</asp:Content>

<asp:Content ContentPlaceHolderID="rcr" runat="server">
</asp:Content>

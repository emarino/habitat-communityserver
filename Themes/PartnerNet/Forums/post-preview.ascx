<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Register TagPrefix="CustomAvatar" TagName="CSAvatar" Src="CustomAvatar.ascx" %> <!--Added by Kishan-->

<div class="ForumPostArea">
    <h4 class="ForumPostHeader">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr valign="middle">
                <td align="left">
                    <CSControl:ThemeImage runat="server" BorderWidth="0" ImageUrl="~/images/icon_post_show.gif" />
                    <CSForum:ForumPostData runat="server" Property="PostDate" IncludeTimeInDate="true" />    
                </td>
            </tr>
        </table>
    </h4>
    <table cellspacing="0" cellpadding="0" border="0" width="100%">
        <tr valign=top>
            <td rowspan="2" class="ForumPostUserArea">
                <CustomAvatar:CSAvatar ID="Avatar1"  UserName="GETFROMPARENTREPEATERITEM" /> <!--Added by Kishan-->
                <div class="ForumPostUserContent" style="visibility:hidden"> <!--Visibility set to hidden by Kishan-->
                <ul class="ForumPostUserPropertyList">
                    <li class="ForumPostUserName">
                        <CSControl:PlaceHolder runat="server">
				            <DisplayConditions Operator="Not"><CSControl:UserPropertyValueComparison runat="server" ComparisonProperty="IsAnonymous" Operator="IsSetOrTrue" /></DisplayConditions>
				            <ContentTemplate>
                                <CSControl:PlaceHolder runat="server">
                                    <DisplayConditions><CSControl:UserPropertyValueComparison ComparisonProperty="IsOnline" Operator="IsSetOrTrue" runat="server" /></DisplayConditions>
                                    <ContentTemplate><CSControl:ThemeImage ImageUrl="~/images/user_IsOnline.gif" runat="server" /></ContentTemplate>
                                </CSControl:PlaceHolder>
                                <CSControl:PlaceHolder runat="server">
                                    <DisplayConditions Operator="Not"><CSControl:UserPropertyValueComparison ComparisonProperty="IsOnline" Operator="IsSetOrTrue" runat="server" /></DisplayConditions>
                                    <ContentTemplate><CSControl:ThemeImage ImageUrl="~/images/user_IsOffline.gif" runat="server" /></ContentTemplate>
                                </CSControl:PlaceHolder>
                            </ContentTemplate>
                        </CSControl:PlaceHolder>
                        <CSControl:UserData runat="server" LinkTo="Profile" Property="DisplayName" />
                    </li>
                    <CSControl:UserAvatar runat="server" BorderWidth="1" Tag="Li" CssClass="ForumPostUserAvatar" />
                    <CSControl:PlaceHolder runat="server">
			            <DisplayConditions Operator="Not"><CSControl:UserPropertyValueComparison runat="server" ComparisonProperty="IsAnonymous" Operator="IsSetOrTrue" /></DisplayConditions>
			            <ContentTemplate>
                            <CSControl:UserPostIcons runat="server" Tag="Li" CssClass="ForumPostUserIcons" />
                            <CSControl:UserData runat="server" Property="DateCreated" Tag="Li" CssClass="ForumPostUserAttribute"><LeaderTemplate><CSControl:ResourceControl runat="server" ResourceName="PostFlatView_Joined" /></LeaderTemplate></CSControl:UserData>
                            <CSControl:UserProfileData runat="server" Property="Location" Tag="Li" CssClass="ForumPostUserAttribute" />
                            <CSControl:UserData runat="server" LinkTo="PostsSearch" Property="TotalPosts" Tag="Li" CssClass="ForumPostUserAttribute">
			                    <DisplayConditions><CSControl:SiteSettingsPropertyValueComparison runat="server" ComparisonProperty="PostingActivityDisplay" ComparisonValue="PostCount" Operator="EqualTo" /></DisplayConditions>
			                    <LeaderTemplate><CSControl:ResourceControl ResourceName="PostFlatView_Posts" runat="server" /></LeaderTemplate>
			                </CSControl:UserData>
			                <CSControl:UserData runat="server" LinkTo="PostsSearch" Property="UserRank" Tag="Li" CssClass="ForumPostUserAttribute">
			                    <DisplayConditions><CSControl:SiteSettingsPropertyValueComparison runat="server" ComparisonProperty="PostingActivityDisplay" ComparisonValue="UserRank" Operator="EqualTo" /></DisplayConditions>
			                </CSControl:UserData>
                            <CSControl:UserData runat="server" Property="Points" Tag="Li" CssClass="ForumPostUserAttribute"><LeaderTemplate><CSControl:ResourceControl runat="server" ResourceName="PostFlatView_UserPoints" /></LeaderTemplate></CSControl:UserData>
                            <CSControl:UserRoleIcons runat="server" LinkTo="Role" Tag="Li" CssClass="ForumPostRoleIcons">
                                <SeparatorTemplate><br /></SeparatorTemplate>
                            </CSControl:UserRoleIcons>
                        </ContentTemplate>
                    </CSControl:PlaceHolder>
                </ul>
                </div>
            </td>
            <td class="ForumPostContentArea">
                <div class="ForumPostTitleArea">
                    <h4 class="ForumPostTitle">
                        <CSControl:PostEmoticon runat="server" />
                        <CSForum:ForumPostData runat="server" Property="Subject" />
                    </h4>
					
                    <div class="ForumPostThreadStatus">
                    </div>

                    <div class="ForumPostButtons">
                    </div>
                </div>
				
                <table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%" style="table-layout: fixed;">
                    <tr><td>
                    <div class="ForumPostBodyArea">
                    <div class="ForumPostContentText">
                        <CSForum:ForumPostData Property="FormattedBody" runat="server" />
                        <CSForum:ForumPostData Property="EditNotes" runat="server">
                            <DisplayConditions><CSForum:ForumConfigurationPropertyValueComparison runat="server" ComparisonProperty="DisplayEditNotesInPost" Operator="IsSetOrTrue" /></DisplayConditions>
                            <LeaderTemplate><br /><br /></LeaderTemplate>
                        </CSForum:ForumPostData>
                    </div>
                    <CSControl:UserProfileData Property="SignatureFormatted" runat="server" Tag="Div" CssClass="ForumPostSignature" />
                    </div>
                    </td></tr>
                </table>
            </td>
        </tr>
        <tr valign="bottom">
            <td class="ForumPostFooterArea">	
                <ul class="ForumPostStatistics CommonPrintHidden" style="clear: both;">
                    <li></li>
                    <CSForum:ForumPostData runat="server" Property="UserHostAddress" ResourceName="PostFlatView_IPAddress" Tag="Li" />
                </ul>
            </td>
        </tr>
    </table>
</div>

<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Discussions.Controls.CSForumThemePage" MasterPageFile="forums.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Discussions.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        if (CurrentForum != null)
        {
            SetTitle(ResourceManager.GetString("PostPendingModeration_Title"), Title, true);
            Description.Text = String.Format(ResourceManager.GetString("PostPendingModeration_Body"), "<a href=\"" + ForumUrls.Instance().Forum(CurrentForum.SectionID) + "\"> " + CurrentForum.Name + "</a>");
        }
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
<div class="CommonContentArea">
    <CSForum:BreadCrumb runat="server" Tag="Div" CssClass="CommonBreadCrumbArea" />

	<CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdTop runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
	
	<div class="CommonContent">
	    <div align="center">
            <div class="CommonMessageArea">
	            <CSControl:WrappedLiteral runat="server" ID="Title" Tag="H4" CssClass="CommonMessageTitle" />
	            <div class="CommonMessageContent">
		            <div class="CommonFormArea">
		                <CSControl:WrappedLiteral runat="server" ID="Description" />
		            </div>
	            </div>
            </div>
       </div>
	</div>
	
	<CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdBottom runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="rcr" runat="server" />
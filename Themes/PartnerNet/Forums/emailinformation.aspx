<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Discussions.Controls.CSForumThemePage" MasterPageFile="forums.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Discussions.Components" %>
<%@ Import Namespace="CommunityServer.MailGateway.MailRoom" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        if (!CurrentCSContext.SiteSettings.EnableMailGateway || CurrentCSContext.User.IsAnonymous)
            throw new CSException(CSExceptionType.AccessDenied, "Mail Gateway is not enabled for this site");

        MailingList mailingList = MailingLists.GetMailingList(CurrentForum.SectionID);
        if (mailingList == null || !mailingList.IsActive || !mailingList.IsMailingList)
            throw new CSException(CSExceptionType.AccessDenied, "The specified forum is not a mailing list");

        ForumEmailAddress.Text = string.Format(ResourceManager.GetString("EmailInformation_ForumEmailAddress"), mailingList.DetermineEmailAddress());
        CurrentEmailAddress.Text = string.Format(ResourceManager.GetString("EmailInformation_CurrentEmailAddress"), CurrentCSContext.User.Email, SiteUrls.Instance().UserEditProfile, ForumUrls.Instance().ForumSubscriptions);
        
        SetTitle(ResourceManager.GetString("EmailInformation_Title"), true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
<div class="CommonContentArea">
	<CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdTop runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
	
    <CSControl:ResourceControl runat="server" ResourceName="EmailInformation_Title" Tag="h2" CssClass="CommonTitle" />		
	<div class="CommonContent">
        <CSControl:ResourceControl runat="server" ResourceName="EmailInformation_Description" />
		<p />
		<table cellpadding="0" cellspacing="0" border="0">
		<tr>
		<td class="CommonFormFieldName"><CSControl:ResourceControl runat="server" ResourceName="EmailInformation_Subscribed" /></td>
		<td class="CommonFormField"><CSControl:SectionSubscriptionToggleButton runat="server" ToggleSubscriptionType="Thread" ButtonCssClass="CommonTextButton" /></td>
		</tr>
		</table>
		<p />
		<asp:Literal Runat="server" ID="ForumEmailAddress" />
		<p />
		<asp:Literal Runat="server" ID="CurrentEmailAddress" />
	</div>
	
	<CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdBottom runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
</div>
</asp:Content>
<%@ control language="C#" autoeventwireup="true" inherits="CustomAvatar, App_Web_customavatar.ascx.cdcab7d2" %>

<asp:Label ID="lblErrorMessage" runat="server" Text="" Visible="false"/>

<div style="text-align:left" class="Avatar">
    <table cellspacing="0" border="0">
        <tr><td><asp:Label ID="lblLoginName" runat="server" Text="" Visible="false"/></td></tr>
        <tr><td><asp:HyperLink runat="server" ID="lnkUserName" NavigateUrl="" Text="" Visible="false"></asp:HyperLink></td></tr>
        <tr><td><asp:Label ID="lblAffiliateName" runat="server" Text="" Visible="false"/></td></tr>
        <tr><td><asp:Label ID="lblAffiliateCity" runat="server" Text="" Visible="false"/></td></tr>
        <tr><td><asp:Label ID="lblAffiliateState" runat="server" Text="" Visible="false"/></td></tr>
        <tr><td><asp:Label ID="lblAffiliateCountry" runat="server" Text="" Visible="false"/></td></tr>
        <tr><td><asp:Label ID="lblEmailID" runat="server" Text="" Visible="false"/></td></tr>
        <tr><td><asp:Label ID="lblPartnerID" runat="server" Text="" Visible="false"/></td></tr>
        <tr><td><asp:Label ID="lblTitle" runat="server" Text="" Visible="false"/></td></tr>
        <tr><td><asp:Label ID="lblDepartment" runat="server" Text="" Visible="false"/></td></tr>
    </table>
</div>
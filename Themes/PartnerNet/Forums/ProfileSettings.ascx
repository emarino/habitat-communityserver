<%@ control language="C#" autoeventwireup="true" inherits="ProfileSettings, App_Web_profilesettings.ascx.cdcab7d2" %>


<div class="CommonTitle2">Email Settings</div>  
<div style="border-style: solid; border-width: 1px; border-color: #cccccc;">  
    <table style="margin-left:10px" border="0" width="100%">
        <tr>
            <td class="CommonFormFieldName">Receive Emails</td>
            <td class="CommonFormField" nowrap="nowrap">
                <asp:RadioButton ID="chkEmailsYes" Checked="true" GroupName="grpEmails" Text="Yes" runat="server" />
                <asp:RadioButton ID="chkEmailsNo" GroupName="grpEmails" Text="No" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="CommonFormFieldName">Receive rich (HTML) emails</td>
            <td class="CommonFormField" nowrap="nowrap">
                <asp:RadioButton ID="chkHTMLEmailsYes" Checked="true" GroupName="grpHTMLEmails" Text="Yes" runat="server" />
                <asp:RadioButton ID="chkHTMLEmailsNo" GroupName="grpHTMLEmails" Text="No" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="CommonFormFieldName">Enable Email Notifications of forum/thread subscriptions and replies to my posts.</td>
            <td class="CommonFormField" nowrap="nowrap">
                <asp:RadioButton ID="chkEmailMyPostsYes" Checked="true" GroupName="grpEmailMyPosts" Text="Yes" runat="server" />
                <asp:RadioButton ID="chkEmailMyPostsNo" GroupName="grpEmailMyPosts" Text="No" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right" style="padding:10px; padding-right:20px"><asp:Button ID="btnSave" Text="Save" OnClick="btnSave_Click" runat="server" /></td>
        </tr>
    </table>    
</div>
<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Discussions.Controls.CSForumThemePage" MasterPageFile="forums.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Discussions.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        switch (CurrentCSContext.RewrittenUrlName)
        {
            case "forums.post_Unanswered":
                FilteredThreadList.QueryOverrides.UnAnsweredOnly = true;
                SetTitle(ResourceManager.GetString("ViewUnansweredThreads_Title"), Title, true);
                Description.Text = ResourceManager.GetString("ViewUnansweredThreads_Description");
                break;
            case "forums.post_Active":
                FilteredThreadList.QueryOverrides.ActiveTopics = true;
                SetTitle(ResourceManager.GetString("ViewActiveThreads_Title"), Title, true);
                Description.Text = ResourceManager.GetString("ViewActiveThreads_Description");
                break;
            case "forums.post_NotRead":
                if (CurrentCSContext.User.IsAnonymous)
                    Response.Redirect(SiteUrls.Instance().Login);
                
                FilteredThreadList.QueryOverrides.UnReadOnly = true;
                SetTitle(ResourceManager.GetString("ViewNotReadThreads_Title"), Title, true);
                Description.Text = ResourceManager.GetString("ViewNotReadThreads_Description");
                break;
            case "forums.post_Videos":
                FilteredThreadList.QueryOverrides.PostMedia = PostMediaType.Video;
                SetTitle(ResourceManager.GetString("ViewVideos_Title"), Title, true);
                Description.Text = ResourceManager.GetString("ViewVideos_Description");
                break;
            case "forums.user_MyForums":
                if (CurrentCSContext.User.IsAnonymous)
                    Response.Redirect(SiteUrls.Instance().Login);
                
                FilteredThreadList.QueryOverrides.UserFilter = ThreadUsersFilter.ShowTopicsParticipatedIn;
                SetTitle(ResourceManager.GetString("ViewMyForumsThreads_Title"), Title, true);
                Description.Text = ResourceManager.GetString("ViewMyForumsThreads_Description");
                break;
        }        
    }

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="bcr" runat="server">
<div class="CommonContentArea">
    <CSForum:BreadCrumb runat="server" Tag="Div" CssClass="CommonBreadCrumbArea" />

	<CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdTop runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
	
	<CSControl:WrappedLiteral runat="server" Tag="H2" CssClass="CommonTitle" ID="Title" />
	
	<div class="CommonContent">
        <CSControl:WrappedLiteral runat="server" Tag="Div" CssClass="CommonDescription" ID="Description" />

        <CSForum:ThreadListFilterForm runat="server" 
            Tag="Div" 
            CssClass="CommonFilterArea" 
            ApplyChangesImmediately="true" 
            DateFilterDropDownListId="DateFilter" 
            ForumFilterDropDownListId="ForumFilter" 
            ThreadListId="FilteredThreadList">
            <FormTemplate>
                Forum: 
                <asp:DropDownList runat="server" ID="ForumFilter" />
                <asp:DropDownList runat="server" ID="DateFilter" />
            </FormTemplate>
        </CSForum:ThreadListFilterForm>

	    <CSControl:Pager runat="server" ID="PagerTop" ShowTotalSummary="true">
	        <LeaderTemplate><div align="right" class="CommonPagingArea"></LeaderTemplate>
	        <TrailerTemplate></div></TrailerTemplate>
	    </CSControl:Pager>

	    <CSForum:ThreadList runat="server" ID="FilteredThreadList" ShowHeaderFooterOnNone="false">
	        <QueryOverrides PagerID="Pager" />
		    <HeaderTemplate>
			    <div class="CommonListArea">
			    <CSControl:ResourceControl runat="server" ResourceName="ViewThreads_Posts" Tag="H4" CssClass="CommonListTitle" />
			    <table width="100%" cellpadding="0" cellspacing="0" border="0">
				    <thead>
					    <tr>
						    <th class="CommonListHeaderLeftMost ForumMyImageAndNameHeader" colspan="2"><CSControl:ResourceControl runat="server" ResourceName="ViewThreads_TitleThread" /></th>
						    <th class="CommonListHeader ForumMyRepliesHeader"><CSControl:ResourceControl runat="server" ResourceName="ViewThreads_TitleReplies" /></th>
					    </tr>
				    </thead>
				    <tbody>
		    </HeaderTemplate>
		    <ItemTemplate>
				    <tr>
					    <td class="ForumListCellLeftMostImageOnly ForumMyImageColumn">
						    <%# ForumFormatter.StatusIcon( (Thread) Container.DataItem ) %>
					    </td>
					    <td class="CommonListCell ForumMyNameColumn">
						    <table width="100%" cellpadding="0" cellspacing="0" border="0">
							    <tr>
								    <td class="ForumSubListCellLeftMost">
								        <%# Formatter.GetEmotionMarkup( ((Thread) Container.DataItem).EmoticonID ) %>
								        <CSForum:ThreadData Property="Subject" TruncateAt="65" LinkTo="Thread" LinkCssClass="ForumNameRead" runat="server"><DisplayConditions><CSForum:ThreadPropertyComparison ComparisonProperty1="HasRead" Operator="IsSetOrTrue" runat="server" /></DisplayConditions></CSForum:ThreadData>
								        <CSForum:ThreadData Property="Subject" TruncateAt="65" LinkTo="Thread" LinkCssClass="ForumNameUnRead" runat="server"><DisplayConditions Operator="Not"><CSForum:ThreadPropertyComparison ComparisonProperty1="HasRead" Operator="IsSetOrTrue" runat="server" /></DisplayConditions></CSForum:ThreadData>
								    </td>
								    <td class="ForumSubListCell" align="right" nowrap="nowrap">
								        <CSForum:ThreadRating runat="server" IsReadOnly="true" RatingCssClass="CommonRateControl" RatingReadOnlyCssClass="CommonRateControlReadOnly" RatingActiveCssClass="CommonRateControlActive" ImagesBaseUrl="~/Themes/default/images/common/" />

									    <CSControl:PlaceHolder runat="server">
									        <DisplayConditions Operator="And">
									            <CSForum:ForumConfigurationPropertyComparison ComparisonProperty1="EnableThreadStatus" Operator="IsSetOrTrue" runat="server" />
									            <CSForum:ThreadPropertyValueComparison ComparisonProperty="Status" Operator="EqualTo" ComparisonValue="NotAnswered" runat="server" />
									            <CSForum:ForumPropertyValueComparison runat="server" ComparisonProperty="EnableThreadStatus" Operator="issetortrue" />
                                            </DisplayConditions>
									        <ContentTemplate><CSControl:ThemeImage ImageUrl="~/images/status_NotAnswered.gif" runat="server" AlternateTextResourceName="Status_NotAnswered" /></ContentTemplate>
									    </CSControl:PlaceHolder>
									    
									    <CSControl:PlaceHolder runat="server">
									        <DisplayConditions Operator="And">
									            <CSForum:ForumConfigurationPropertyComparison ComparisonProperty1="EnableThreadStatus" Operator="IsSetOrTrue" runat="server" />
									            <CSForum:ThreadPropertyValueComparison ComparisonProperty="Status" Operator="EqualTo" ComparisonValue="Answered" runat="server" />
									            <CSForum:ForumPropertyValueComparison runat="server" ComparisonProperty="EnableThreadStatus" Operator="issetortrue" />
                                            </DisplayConditions>
									        <ContentTemplate><CSControl:ThemeImage ImageUrl="~/images/status_Answered.gif" runat="server" AlternateTextResourceName="Status_Answered" /></ContentTemplate>
									    </CSControl:PlaceHolder>
								    </td>
							    </tr>
						    </table>
						    <table width="100%" cellpadding="0" cellspacing="0" border="0">
							    <tr>
								    <td class="ForumSubListCellLeftMost ForumLastPost">
									    <CSControl:PlaceHolder runat="server">
						                    <DisplayConditions><CSForum:ThreadPropertyValueComparison ComparisonProperty="ThreadDate" ComparisonValue="6/8/1980" Operator="GreaterThan" runat="server" /></DisplayConditions>
						                    <ContentTemplate>
    					                        <CSForum:ThreadData LinkTo="MostRecentPost" ResourceName="ForumGroupView_Inline4" runat="server" />
                                                <CSForum:ThreadData LinkTo="MostRecentPostAuthor" Property="MostRecentPostAuthor" runat="server" TruncateAt="15"><LeaderTemplate>by </LeaderTemplate></CSForum:ThreadData>
                                                <CSForum:ThreadData Property="ThreadDate" runat="server" IncludeTimeInDate="true"><LeaderTemplate>, </LeaderTemplate></CSForum:ThreadData>
                                            </ContentTemplate>
                                        </CSControl:PlaceHolder>
								    </td>
								    <td class="ForumSubListCellPager">
									    <CSForum:ThreadPostPageLinks runat="server" />
								    </td>
							    </tr>
						    </table>
					    </td>
					    <td class="CommonListCell ForumMyRepliesColumn">
					        <CSForum:ThreadData Property="Replies" runat="server" />
					    </td>
				    </tr>				
		    </ItemTemplate>
		    <FooterTemplate>
			    </tbody>
			    </table>
			    </div>
		    </FooterTemplate>
		    <NoneTemplate>
		        <CSControl:ResourceControl runat="server" ResourceName="ViewThreads_NoTopics" Tag="Div" CssClass="CommonMessageError" />
		    </NoneTemplate>
	    </CSForum:ThreadList>
    	
	    <CSControl:PagerGroup runat="server" id="Pager" PagerIds="PagerTop,PagerBottom" />
	    <CSControl:Pager runat="server" ID="PagerBottom" ShowTotalSummary="true">
	        <LeaderTemplate><div align="right" class="CommonPagingArea"></LeaderTemplate>
	        <TrailerTemplate></div></TrailerTemplate>
        </CSControl:Pager>
    	
	    <div align="right" class="CommonFeedArea">
		    <CSForum:AggregateRss runat="server">
		        <DisplayConditions Operator="Not">
		            <CSControl:CurrentSiteUrlCondition runat="server" SiteUrlName="forums.user_MyForums" />
		        </DisplayConditions>
		        <ContentTemplate><CSControl:Image ImageUrl="~/utility/images/rss.gif" BorderWidth="0" runat="server" /></ContentTemplate>
		    </CSForum:AggregateRss>
	    </div>
	
	</div>
	
	<CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdBottom runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
</div>
</asp:Content>

<asp:Content ID="RightColumnRegion" ContentPlaceHolderID="rcr" runat="server">
</asp:Content>
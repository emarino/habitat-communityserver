<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Discussions.Controls.CSForumThemePage" MasterPageFile="forums.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Discussions.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        if (CurrentForum != null)
            SetTitle(CurrentForum.Name, true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
    <div class="CommonFormArea">
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
	        <tr>
		        <!--<td align="left" class="CommonFormField">
		            <table width="28%" height="100%" cellpadding="0" cellspacing="0">
		                <tr>
		                    <td nowrap></td>
		                    <td class="CommonBarDivider"></td>
		                </tr>
		            </table>			        
		        </td>	-->	        
		        <td valign="top">
                    <CSControl:Pager runat="server" ID="PagerTop" ShowTotalSummary="true">
                        <LeaderTemplate><div align="right" class="CommonPagingArea"></LeaderTemplate>
                        <TrailerTemplate></div></TrailerTemplate>
                    </CSControl:Pager>
                </td> 
	        </tr>
        </table>
    </div>
<div class="CommonContentArea">
    <CSForum:BreadCrumb runat="server" Tag="Div" CssClass="CommonBreadCrumbArea" />

	<CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdTop runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>

	<br/>	
	<table align="right"><tr><td><img src="/Themes/PartnerNet/Images/PartnerNet/NewPost.jpg" /></td><td><CSForum:ForumData LinkTo="PostCreate" LinkCssClass="NewPost" ResourceName="Button_NewPost" runat="server" /></td></tr></table>
	<br/><br/>

	<CSForum:ForumEditableData runat="server" Property="Name" CssClass="CommonTitle2" EditorLinkCssClass="CommonTextButton" EditorCssClass="CommonInlineTagEditor" ContentCssClass="CommonContentPartBorderOff" ContentEditingCssClass="CommonContentPartBorderOn" ContentHoverCssClass="CommonContentPartBorderOn" />
		
	<div class="CommonContent" style="padding:0px">
	    <!--Commented by Kishan & removed runatt serverr-->
        <!--<CSForum:ForumEditableData  Property="Description" Tag="Div" CssClass="CommonDescription" EditorLinkCssClass="CommonTextButton" EditorCssClass="CommonInlineTagEditor" ContentCssClass="CommonContentPartBorderOff" ContentEditingCssClass="CommonContentPartBorderOn" ContentHoverCssClass="CommonContentPartBorderOn" />

		<CSForum:ForumList  Tag="Div" CssClass="ForumSubArea" ShowHeaderFooterOnNone="false">
		    <QueryOverrides PageSize="999999" />
	        <HeaderTemplate>
		        <table width="100%" cellpadding="0" cellspacing="0" border="0">
		        <thead>
			        <tr>
				        <th colspan="2" nowrap="nowrap" class="CommonListHeaderLeftMost ForumGroupImageAndNameHeader" align="center"><CSControl:ResourceControl  ResourceName="ForumGroupView_Inline1" /></th>
				        <th class="CommonListHeader ForumGroupLastPostHeader"><CSControl:ResourceControl  ResourceName="ForumGroupView_Inline4" /></th>                                    
				        <th class="CommonListHeader ForumGroupTotalThreadsHeader" align="center"><CSControl:ResourceControl  ResourceName="ForumGroupView_Inline2" /></th>                                    
				        <th class="CommonListHeader ForumGroupTotalPostsHeader" align="center"><CSControl:ResourceControl  ResourceName="ForumGroupView_Inline3" /></th>
			        </tr>
		        </thead>
		        <tbody>
	        </HeaderTemplate>
	        <ItemTemplate>
		        <tr>
			        <td class="ForumListCellLeftMostImageOnly  ForumGroupImageColumn">
				        <%//# ForumFormatter.StatusIcon( (Forum) Container.DataItem ) %> //Commented by Kishan
			        </td>
			        <td class="CommonListCell ForumGroupNameColumn">
			            <CSForum:ForumData LinkCssClass="ForumGroupNameRead" Property="Name" LinkTo="HomePage" ><DisplayConditions><CSForum:ForumPropertyValueComparison ComparisonProperty="HasRead" Operator="IsSetOrTrue"  /></DisplayConditions></CSForum:ForumData>
			            <CSForum:ForumData LinkCssClass="ForumGroupNameUnRead" Property="Name" LinkTo="HomePage" ><DisplayConditions Operator="Not"><CSForum:ForumPropertyValueComparison ComparisonProperty="HasRead" Operator="IsSetOrTrue"  /></DisplayConditions></CSForum:ForumData>
				        <CSForum:ForumData Property="Description"  Tag="Div" />
				        <CSForum:ForumList  ShowHeaderFooterOnNone="false">
				            <HeaderTemplate><CSControl:ResourceControl ResourceName="Subforums"  Tag="B" /></HeaderTemplate>
				            <ItemTemplate><CSForum:ForumData  Property="Name" LinkTo="HomePage" /></ItemTemplate>
				            <SeparatorTemplate>, </SeparatorTemplate>
				            <NoneTemplate></NoneTemplate>
				        </CSForum:ForumList>
			        </td>
			        <td class="CommonListCell ForumGroupLastPostColumn">
			            <CSControl:PlaceHolder >
			                <DisplayConditions><CSForum:ForumPropertyValueComparison ComparisonProperty="MostRecentPostDate" ComparisonValue="6/8/1980" Operator="GreaterThan"  /></DisplayConditions>
			                <ContentTemplate>
			                    <CSForum:ForumData LinkTo="MostRecentPost" Property="MostRecentPostSubject"  Tag="B" TruncateAt="15" />
                                <CSForum:ForumData LinkTo="MostRecentPostAuthor" Property="MostRecentPostAuthor"  Tag="Div" TruncateAt="15"><LeaderTemplate>by </LeaderTemplate></CSForum:ForumData>
                                <CSForum:ForumData Property="MostRecentPostDate"  IncludeTimeInDate="true" Tag="Div" />
                            </ContentTemplate>
                        </CSControl:PlaceHolder>
                        &nbsp;
			        </td>
			        <td class="CommonListCell ForumGroupTotalThreadsColumn" align="center"><CSForum:ForumData  Property="TotalThreads" /></td>
			        <td class="CommonListCell ForumGroupTotalPostsColumn" align="center"><CSForum:ForumData  Property="TotalPosts" /></td>
		        </tr>
	        </ItemTemplate>
	        <FooterTemplate>
		        </tbody>
		        </table>				
	        </FooterTemplate>
	        <NoneTemplate></NoneTemplate>
		</CSForum:ForumList>-->

        
	    
	    <CSForum:ThreadList runat="server" ID="AnnouncementThreadList" ShowHeaderFooterOnNone="false">
	        <QueryOverrides PageSize="0" PageIndex="0" />
		    <HeaderTemplate>
			    <div class="CommonListArea">
			    <!--<CSControl:ResourceControl ResourceName="ViewThreads_FaqsAnnouncements" Tag="H4" CssClass="CommonListTitle" />--> <!--Commented by Kishan & removed runatt serverr-->
			    <table width="100%" cellpadding="0" cellspacing="0" border="0">
				    <thead>
					    <tr>
						    <th class="CommonListHeader" colspan="2"><CSControl:ResourceControl runat="server" ResourceName="ViewThreads_TitleThread" /></th>
						    <th class="CommonListHeader ForumMyRepliesHeader"><CSControl:ResourceControl runat="server" ResourceName="ViewThreads_TitleReplies" /></th>
					    </tr>
				    </thead>
				    <tbody>
		    </HeaderTemplate>
		    <ItemTemplate>
				    <tr>
					    <td class="ForumListCellLeftMostImageOnly ForumMyImageColumn">
						    <%# ForumFormatter.StatusIcon( (Thread) Container.DataItem ) %>
					    </td>
					    <td class="CommonListCell ForumMyNameColumn">
						    <table width="100%" cellpadding="0" cellspacing="0" border="0">
							    <tr>
								    <td class="ForumSubListCellLeftMost">
								        <%# Formatter.GetEmotionMarkup( ((Thread) Container.DataItem).EmoticonID ) %>
								        <CSForum:ThreadData Property="Subject" TruncateAt="65" LinkTo="Thread" LinkCssClass="ForumNameRead" runat="server"><DisplayConditions><CSForum:ThreadPropertyComparison ComparisonProperty1="HasRead" Operator="IsSetOrTrue" runat="server" /></DisplayConditions></CSForum:ThreadData>
								        <CSForum:ThreadData Property="Subject" TruncateAt="65" LinkTo="Thread" LinkCssClass="ForumNameUnRead" runat="server"><DisplayConditions Operator="Not"><CSForum:ThreadPropertyComparison ComparisonProperty1="HasRead" Operator="IsSetOrTrue" runat="server" /></DisplayConditions></CSForum:ThreadData>
								    </td>
								    <td class="ForumSubListCell" align="right" nowrap="nowrap">
								        <CSForum:ThreadRating runat="server" IsReadOnly="true" RatingCssClass="CommonRateControl" RatingReadOnlyCssClass="CommonRateControlReadOnly" RatingActiveCssClass="CommonRateControlActive" ImagesBaseUrl="~/Themes/default/images/common/" />

									    <CSControl:PlaceHolder runat="server">
									        <DisplayConditions Operator="And">
									            <CSForum:ForumConfigurationPropertyComparison ComparisonProperty1="EnableThreadStatus" Operator="IsSetOrTrue" runat="server" />
									            <CSForum:ThreadPropertyValueComparison ComparisonProperty="Status" Operator="EqualTo" ComparisonValue="NotAnswered" runat="server" />
                                                <CSForum:ForumPropertyValueComparison runat="server" ComparisonProperty="EnableThreadStatus" Operator="issetortrue" />
                                            </DisplayConditions>
									        <ContentTemplate><CSControl:ThemeImage ImageUrl="~/images/status_NotAnswered.gif" runat="server" AlternateTextResourceName="Status_NotAnswered" /></ContentTemplate>
									    </CSControl:PlaceHolder>
									    
									    <CSControl:PlaceHolder runat="server">
									        <DisplayConditions Operator="And">
									            <CSForum:ForumConfigurationPropertyComparison ComparisonProperty1="EnableThreadStatus" Operator="IsSetOrTrue" runat="server" />
									            <CSForum:ThreadPropertyValueComparison ComparisonProperty="Status" Operator="EqualTo" ComparisonValue="Answered" runat="server" />
									            <CSForum:ForumPropertyValueComparison runat="server" ComparisonProperty="EnableThreadStatus" Operator="issetortrue" />
                                            </DisplayConditions>
									        <ContentTemplate><CSControl:ThemeImage ImageUrl="~/images/status_Answered.gif" runat="server" AlternateTextResourceName="Status_Answered" /></ContentTemplate>
									    </CSControl:PlaceHolder>
								    </td>
							    </tr>
						    </table>
						    <table width="100%" cellpadding="0" cellspacing="0" border="0">
							    <tr>
								    <td class="ForumSubListCellLeftMost ForumLastPost">
									    <CSControl:PlaceHolder runat="server">
						                    <DisplayConditions><CSForum:ThreadPropertyValueComparison ComparisonProperty="ThreadDate" ComparisonValue="6/8/1980" Operator="GreaterThan" runat="server" /></DisplayConditions>
						                    <ContentTemplate>
    					                        <CSForum:ThreadData LinkTo="MostRecentPost" ResourceName="ForumGroupView_Inline4" runat="server" />
                                                <CSForum:ThreadData LinkTo="MostRecentPostAuthor" Property="MostRecentPostAuthor" runat="server" TruncateAt="15"><LeaderTemplate>by </LeaderTemplate></CSForum:ThreadData>
                                                <CSForum:ThreadData Property="ThreadDate" runat="server" IncludeTimeInDate="true"><LeaderTemplate>, </LeaderTemplate></CSForum:ThreadData>
                                            </ContentTemplate>
                                        </CSControl:PlaceHolder>
								    </td>
								    <td class="ForumSubListCellPager">
									    <CSForum:ThreadPostPageLinks runat="server" />
								    </td>
							    </tr>
						    </table>
					    </td>
					    <td class="CommonListCell ForumMyRepliesColumn">
					        <CSForum:ThreadData Property="Replies" runat="server" />
					    </td>
				    </tr>				
		    </ItemTemplate>
		    <FooterTemplate>
			    </tbody>
			    </table>
			    </div>
		    </FooterTemplate>
		    <NoneTemplate>
		    </NoneTemplate>
	    </CSForum:ThreadList>

	    <CSForum:ThreadList runat="server" ID="FilteredThreadList" ShowHeaderFooterOnNone="false">
	        <QueryOverrides PagerID="Pager" AnnouncementsThreadListId="AnnouncementThreadList" />
		    <HeaderTemplate>
			    <div class="CommonListArea">
			    <!--<CSControl:ResourceControl ResourceName="ViewThreads_Posts" Tag="H4" CssClass="CommonListTitle" />--> <!--Commented by Kishan & removed runatt serverr-->
			    <table width="100%" cellpadding="0" cellspacing="0" border="0">
				    <thead>
					    <tr>
						    <th class="CommonListHeader" colspan="2"><CSControl:ResourceControl runat="server" ResourceName="ViewThreads_TitleThread" visible="false" />Topics</th>
						    <th class="CommonListHeader ForumMyRepliesHeader"><CSControl:ResourceControl runat="server" ResourceName="ViewThreads_TitleReplies" /></th>
						    <th id="MoveThreadsColumnHeader" class="CommonListHeader ForumMyMoveHeader" runat="server" Visible='<%# MoveThreadsForm.Visible %>'>&nbsp;</th> 
					    </tr>
				    </thead>
				    <tbody>
		    </HeaderTemplate>
		    <ItemTemplate>
				    <tr>
					    <td class="ForumListCellLeftMostImageOnly ForumMyImageColumn">
						    <%# ForumFormatter.StatusIcon( (Thread) Container.DataItem ) %>
					    </td>
					    <td class="CommonListCell ForumMyNameColumn">
						    <table width="100%" cellpadding="0" cellspacing="0" border="0">
							    <tr>
								    <td class="ForumSubListCellLeftMost">
								        <%# Formatter.GetEmotionMarkup( ((Thread) Container.DataItem).EmoticonID ) %>
								        <CSForum:ThreadData Property="Subject" TruncateAt="65" LinkTo="Thread" LinkCssClass="ForumNameRead" runat="server"><DisplayConditions><CSForum:ThreadPropertyComparison ComparisonProperty1="HasRead" Operator="IsSetOrTrue" runat="server" /></DisplayConditions></CSForum:ThreadData>
								        <CSForum:ThreadData Property="Subject" TruncateAt="65" LinkTo="Thread" LinkCssClass="ForumNameUnRead" runat="server"><DisplayConditions Operator="Not"><CSForum:ThreadPropertyComparison ComparisonProperty1="HasRead" Operator="IsSetOrTrue" runat="server" /></DisplayConditions></CSForum:ThreadData>
								    </td>
								    <td class="ForumSubListCell" align="right" nowrap="nowrap">
								        <CSForum:ThreadRating runat="server" IsReadOnly="true" RatingCssClass="CommonRateControl" RatingReadOnlyCssClass="CommonRateControlReadOnly" RatingActiveCssClass="CommonRateControlActive" ImagesBaseUrl="~/Themes/default/images/common/" />

									    <CSControl:PlaceHolder runat="server">
									        <DisplayConditions Operator="And">
									            <CSForum:ForumConfigurationPropertyComparison ComparisonProperty1="EnableThreadStatus" Operator="IsSetOrTrue" runat="server" />
									            <CSForum:ThreadPropertyValueComparison ComparisonProperty="Status" Operator="EqualTo" ComparisonValue="NotAnswered" runat="server" />
                                                <CSForum:ForumPropertyValueComparison runat="server" ComparisonProperty="EnableThreadStatus" Operator="issetortrue" />
                                            </DisplayConditions>
									        <ContentTemplate><CSControl:ThemeImage ImageUrl="~/images/status_NotAnswered.gif" runat="server" AlternateTextResourceName="Status_NotAnswered" /></ContentTemplate>
									    </CSControl:PlaceHolder>
									    
									    <CSControl:PlaceHolder runat="server">
									        <DisplayConditions Operator="And">
									            <CSForum:ForumConfigurationPropertyComparison ComparisonProperty1="EnableThreadStatus" Operator="IsSetOrTrue" runat="server" />
									            <CSForum:ThreadPropertyValueComparison ComparisonProperty="Status" Operator="EqualTo" ComparisonValue="Answered" runat="server" />
									            <CSForum:ForumPropertyValueComparison runat="server" ComparisonProperty="EnableThreadStatus" Operator="issetortrue" />
                                            </DisplayConditions>
									        <ContentTemplate><CSControl:ThemeImage ImageUrl="~/images/status_Answered.gif" runat="server" AlternateTextResourceName="Status_Answered" /></ContentTemplate>
									    </CSControl:PlaceHolder>
								    </td>
							    </tr>
						    </table>
						    <table width="100%" cellpadding="0" cellspacing="0" border="0">
							    <tr>
								    <td class="ForumSubListCellLeftMost ForumLastPost">
									    <CSControl:PlaceHolder runat="server">
						                    <DisplayConditions><CSForum:ThreadPropertyValueComparison ComparisonProperty="ThreadDate" ComparisonValue="6/8/1980" Operator="GreaterThan" runat="server" /></DisplayConditions>
						                    <ContentTemplate>
    					                        <CSForum:ThreadData LinkTo="MostRecentPost" ResourceName="ForumGroupView_Inline4" runat="server" />
                                                <CSForum:ThreadData LinkTo="MostRecentPostAuthor" Property="MostRecentPostAuthor" runat="server" TruncateAt="15"><LeaderTemplate>by </LeaderTemplate></CSForum:ThreadData>
                                                <CSForum:ThreadData Property="ThreadDate" runat="server" IncludeTimeInDate="true"><LeaderTemplate>, </LeaderTemplate></CSForum:ThreadData>
                                            </ContentTemplate>
                                        </CSControl:PlaceHolder>
								    </td>
								    <td class="ForumSubListCellPager">
									    <CSForum:ThreadPostPageLinks runat="server" />
								    </td>
							    </tr>
						    </table>
					    </td>
					    <td class="CommonListCell ForumMyRepliesColumn">
					        <CSForum:ThreadData Property="Replies" runat="server" />
					    </td>
					    <CSForum:ThreadCheckbox runat="server" Visible='<%# MoveThreadsForm.Visible %>'><LeaderTemplate><td class="CommonListCell ForumMyMoveColumn"></LeaderTemplate><TrailerTemplate></td></TrailerTemplate></CSForum:ThreadCheckbox>
				    </tr>				
		    </ItemTemplate>
		    <FooterTemplate>
			    </tbody>
			    </table>
			    </div>
		    </FooterTemplate>
		    <NoneTemplate>
		        <CSControl:ResourceControl runat="server" ResourceName="ViewThreads_NoTopics" Tag="Div" CssClass="CommonMessageError" />
		    </NoneTemplate>
	    </CSForum:ThreadList>
    	
    	<table cellpadding="0" cellspacing="0" width="100%" border="0">
		    <tr>
			    <td valign="top" align="left">
				    <a href="#options" class="CommonImageTextButton" style="display:none;background-image: url(<%=Globals.GetSkinPath() %>/images/forum/forumsettings.gif);" onclick="var fo = document.getElementById('ForumOptions'); if (fo.style.display=='none') { fo.style.display='block'; return true; } else { fo.style.display='none'; return false; }; ">
                        <CSControl:ResourceControl runat="server" ResourceName="Button_ForumSettings" />
                    </a>
			    </td>	
			    <td valign="top">
				   <CSControl:PagerGroup runat="server" id="Pager" PagerIds="PagerTop,PagerBottom" />
	                <CSControl:Pager runat="server" ID="PagerBottom" ShowTotalSummary="true">
	                    <LeaderTemplate><div align="right" class="CommonPagingArea"></LeaderTemplate>
	                    <TrailerTemplate></div></TrailerTemplate>
                    </CSControl:Pager>
			    </td> 
		    </tr>
	    </table>
	    
	    <CSForum:MoveThreadsForm id="MoveThreadsForm" runat="server" SubmitButtonId="MoveThreads">
	        <FormTemplate>
	            <div class="CommonFormArea" style="padding:0px;margin:0px">
		            <table cellpadding="0" cellspacing="0" border="0" width="100%">
			            <tr>
				            <td align="right" class="CommonFormField">
					            <CSControl:ResourceLinkButton id="MoveThreads" runat="server" ResourceName="Move" CssClass="CommonTextButton" CausesValidation="false" />
				            </td>
			            </tr>
		            </table>
	            </div>
            </FormTemplate>
        </CSForum:MoveThreadsForm>
    	
        <CSForum:ThreadListFilterForm runat="server" Tag="Div" ContainerId="ForumOptions" style="display: none;"
            DateFilterDropDownListId="DateFilter" 
             SortThreadByDropDownListId="SortBy"
             SortOrderDropDownListId="SortOrder"
             HideReadPostsDropDownListId="HideRead"
             UserFilterDropDownListId="UserFilter"
             EmailNotificationDropDownListId="Email"
             ApplyButtonId="Apply"
             ApplyTemporarilyButtonId="ApplyTemp"
             ControlIdsToHideFromAnonymousUsers="UserFilterArea,EmailArea"
            ThreadListId="FilteredThreadList">
            <FormTemplate>
                <a name="options"></a>
	            <div class="CommonFormArea">
	                <fieldset>
	                    <legend><CSControl:ResourceControl runat="server" ResourceName="ViewThreads_ForumOptions" /></legend>
	                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
		                    <tr>
			                    <td align="left" class="CommonFormFieldName" nowrap="nowrap">
				                    <CSControl:ResourceControl runat="server" ResourceName="ViewThreads_SortedBy" />
			                    </td>
			                    <td align="left" class="CommonFormField" nowrap="nowrap">
				                    <asp:DropDownList runat="server" ID="SortBy" />
				                    <CSControl:ResourceControl runat="server" ResourceName="ViewThreads_In" />
				                    <asp:DropDownList runat="server" ID="SortOrder" />
				                    <CSControl:ResourceControl runat="server" ResourceName="ViewThreads_OrderFrom" />
			                    </td>
			                    <td align="right" valign="top" class="CommonFormField" rowspan="5">
				                    <CSControl:ResourceControl ResourceName="User_UserPermissions_attachment_enabled" runat="server" Tag="Div"><DisplayConditions><CSForum:ForumPermissionCondition Permission="LocalAttachment" runat="server" /></DisplayConditions></CSControl:ResourceControl>
				                    <CSControl:ResourceControl ResourceName="User_UserPermissions_attachment_disabled" runat="server" Tag="Div"><DisplayConditions Operator="Not"><CSForum:ForumPermissionCondition Permission="LocalAttachment" runat="server" /></DisplayConditions></CSControl:ResourceControl>
				                    
				                    <CSControl:ResourceControl ResourceName="User_UserPermissions_postNewTopics_enabled" runat="server" Tag="Div"><DisplayConditions><CSForum:ForumPermissionCondition Permission="Post" runat="server" /></DisplayConditions></CSControl:ResourceControl>
				                    <CSControl:ResourceControl ResourceName="User_UserPermissions_postNewTopics_disabled" runat="server" Tag="Div"><DisplayConditions Operator="Not"><CSForum:ForumPermissionCondition Permission="Post" runat="server" /></DisplayConditions></CSControl:ResourceControl>
				                    
				                    <CSControl:ResourceControl ResourceName="User_UserPermissions_replyToTopics_enabled" runat="server" Tag="Div"><DisplayConditions><CSForum:ForumPermissionCondition Permission="Reply" runat="server" /></DisplayConditions></CSControl:ResourceControl>
				                    <CSControl:ResourceControl ResourceName="User_UserPermissions_replyToTopics_disabled" runat="server" Tag="Div"><DisplayConditions Operator="Not"><CSForum:ForumPermissionCondition Permission="Reply" runat="server" /></DisplayConditions></CSControl:ResourceControl>
				                    
				                    <CSControl:ResourceControl ResourceName="User_UserPermissions_deletePosts_enabled" runat="server" Tag="Div"><DisplayConditions><CSForum:ForumPermissionCondition Permission="Delete" runat="server" /></DisplayConditions></CSControl:ResourceControl>
				                    <CSControl:ResourceControl ResourceName="User_UserPermissions_deletePosts_disabled" runat="server" Tag="Div"><DisplayConditions Operator="Not"><CSForum:ForumPermissionCondition Permission="Delete" runat="server" /></DisplayConditions></CSControl:ResourceControl>
				                    
				                    <CSControl:ResourceControl ResourceName="User_UserPermissions_editPosts_enabled" runat="server" Tag="Div"><DisplayConditions><CSForum:ForumPermissionCondition Permission="Edit" runat="server" /></DisplayConditions></CSControl:ResourceControl>
				                    <CSControl:ResourceControl ResourceName="User_UserPermissions_editPosts_disabled" runat="server" Tag="Div"><DisplayConditions Operator="Not"><CSForum:ForumPermissionCondition Permission="Edit" runat="server" /></DisplayConditions></CSControl:ResourceControl>
				                    
				                    <CSControl:ResourceControl ResourceName="User_UserPermissions_createPolls_enabled" runat="server" Tag="Div"><DisplayConditions><CSForum:ForumPermissionCondition Permission="CreatePoll" runat="server" /></DisplayConditions></CSControl:ResourceControl>
				                    <CSControl:ResourceControl ResourceName="User_UserPermissions_createPolls_disabled" runat="server" Tag="Div"><DisplayConditions Operator="Not"><CSForum:ForumPermissionCondition Permission="CreatePoll" runat="server" /></DisplayConditions></CSControl:ResourceControl>
				                    
				                    <CSControl:ResourceControl ResourceName="User_UserPermissions_vote_enabled" runat="server" Tag="Div"><DisplayConditions><CSForum:ForumPermissionCondition Permission="Vote" runat="server" /></DisplayConditions></CSControl:ResourceControl>
				                    <CSControl:ResourceControl ResourceName="User_UserPermissions_vote_disabled" runat="server" Tag="Div"><DisplayConditions Operator="Not"><CSForum:ForumPermissionCondition Permission="Vote" runat="server" /></DisplayConditions></CSControl:ResourceControl>
				                    
				                    <CSControl:ResourceControl ResourceName="User_UserPermissions_postStats_enabled" runat="server" Tag="Div"><DisplayConditions><CSForum:ForumPropertyValueComparison ComparisonProperty="EnablePostStatistics" Operator="IsSetOrTrue" runat="server" /></DisplayConditions></CSControl:ResourceControl>
				                    <CSControl:ResourceControl ResourceName="User_UserPermissions_postStats_disabled" runat="server" Tag="Div"><DisplayConditions Operator="Not"><CSForum:ForumPropertyValueComparison ComparisonProperty="EnablePostStatistics" Operator="IsSetOrTrue" runat="server" /></DisplayConditions></CSControl:ResourceControl>
				                    
				                    <CSControl:ResourceControl ResourceName="User_UserPermissions_forumModeration_enabled" runat="server" Tag="Div"><DisplayConditions><CSForum:ForumPropertyValueComparison ComparisonProperty="IsModerated" Operator="IsSetOrTrue" runat="server" /></DisplayConditions></CSControl:ResourceControl>
				                    <CSControl:ResourceControl ResourceName="User_UserPermissions_forumModeration_disabled" runat="server" Tag="Div"><DisplayConditions Operator="Not"><CSForum:ForumPropertyValueComparison ComparisonProperty="IsModerated" Operator="IsSetOrTrue" runat="server" /></DisplayConditions></CSControl:ResourceControl>
			                    </td>
		                    </tr>
		                    <tr>
			                    <td align="left" class="CommonFormFieldName">
				                    <CSControl:ResourceControl runat="server" ResourceName="ViewThreads_FilterByDate" />
			                    </td>
			                    <td align="left" class="CommonFormField">
				                    <asp:DropDownList runat="server" ID="DateFilter" />
			                    </td>
		                    </tr>
		                    <tr runat="server" id="UserFilterArea">
			                    <td align="left" class="CommonFormFieldName" nowrap="nowrap">
				                    <CSControl:ResourceControl runat="server" ResourceName="ViewThreads_FilterByTopic" />
			                    </td>
			                    <td align="left" class="CommonFormField">	
				                    <asp:DropDownList runat="server" ID="HideRead" />
				                    <asp:DropDownList runat="server" ID="UserFilter" />
			                    </td>
		                    </tr>
		                    <tr runat="server" id="EmailArea">
			                    <td align="left" class="CommonFormFieldName" nowrap="nowrap">
				                    <CSControl:ResourceControl runat="server" ResourceName="EmailNotificationDropDownList_When" />
			                    </td>
			                    <td align="left" class="CommonFormField">
				                    <asp:DropDownList runat="server" ID="Email" />
			                    </td>
		                    </tr>
		                    <tr>
			                    <td align="right" class="CommonFormField" colspan="2" nowrap="nowrap">
			                        <CSControl:ResourceButton ID="ApplyTemp" ResourceName="ViewThreads_SortThreads" runat="server" />
			                        <CSControl:ResourceButton ID="Apply" ResourceName="ViewThreads_RememberSettings" runat="server" />
				                    
				                    <CSForum:MarkAllReadForm runat="server" SubmitButtonId="MarkAllRead">
				                        <SuccessActions><CSControl:GoToModifiedUrlAction runat="server" /></SuccessActions>
				                        <FormTemplate><CSControl:ResourceButton ID="MarkAllRead" ResourceName="MarkAllRead_Threads" runat="server" /></FormTemplate>
				                    </CSForum:MarkAllReadForm>
			                    </td>
		                    </tr>
	                    </table>
	                </fieldset>
	            </div>
	        </FormTemplate>
        </CSForum:ThreadListFilterForm>
    	
	    <div align="right" class="CommonFeedArea">
	        <CSForum:ForumData LinkTo="EmailInformation" runat="server">
	            <DisplayConditions Operator="And">
	                <CSMail:MailingListPropertyValueComparison runat="server" ComparisonProperty="IsMailingList" Operator="IsSetOrTrue" />
	                <CSMail:MailingListPropertyValueComparison runat="server" ComparisonProperty="IsActive" Operator="IsSetOrTrue" />
	            </DisplayConditions>
	            <ContentTemplate><CSControl:Image ImageUrl="~/utility/images/email.gif" BorderWidth="0" runat="server" /></ContentTemplate>
	        </CSForum:ForumData>
	        
	        <CSForum:ForumData LinkTo="NntpInformation" runat="server">
	            <ContentTemplate><CSControl:Image ImageUrl="~/utility/images/nntp.gif" BorderWidth="0" runat="server" /></ContentTemplate>
	        </CSForum:ForumData>
	        
	        <CSForum:ForumData LinkTo="Rss" runat="server">
	            <ContentTemplate><CSControl:Image ImageUrl="~/utility/images/rss.gif" BorderWidth="0" runat="server" /></ContentTemplate>
	        </CSForum:ForumData>
	    </div>
	
	</div>
	
	<CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdBottom runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
</div>
</asp:Content>

<asp:Content ID="RightColumnRegion" ContentPlaceHolderID="rcr" runat="server">
<!-- Commented by Kishan and removed runatt serverr-->
<div style="visibility:hidden" id="RightPanel">
    <!--<div class="CommonSidebar">
	    <div class="CommonSidebarArea">
	        <div class="CommonSidebarTopRound"><div class="t1"></div><div class="t2"></div><div class="t3"></div><div class="t4"></div></div>
			<CSControl:ResourceControl  ResourceName="Shortcuts" Tag="H4" CssClass="CommonSidebarHeader" />
			<div class="CommonSidebarContent">
			    <ul class="CommonSidebarList">
				    <CSControl:SiteUrl UrlName="user_List" ResourceName="Utility_ForumAnchorType_MenuMemberList" Tag="Li"  />
                    <CSControl:PlaceHolder >
                        <DisplayConditions><CSControl:UserInRoleCondition Role="Registered Users" UseAccessingUser="true"  /></DisplayConditions>        
                        <ContentTemplate>
                            <CSControl:SiteUrl UrlName="post_NotRead" Parameter1="-1" ResourceName="Utility_ForumAnchorType_PostsNotRead" Tag="Li"  />
				            <CSControl:SiteUrl UrlName="forumSubscriptions" ResourceName="Utility_ForumAnchorType_ForumSubscriptions" Tag="Li"  />
                        </ContentTemplate>
                    </CSControl:PlaceHolder>
                </ul>
			</div>
		</div>
	
        <CSControl:TagCloud  Tag="Div" CssClass="CommonSidebarArea" ApplicationType="Forum" IgnoreFilterTags="true" TagCssClasses="CommonTag6,CommonTag5,CommonTag4,CommonTag3,CommonTag2,CommonTag1" TagCloudCssClass="CommonSidebarTagCloud" MaximumNumberOfTags="25">
            <LeaderTemplate>
                <div class="CommonSidebarTopRound"><div class="t1"></div><div class="t2"></div><div class="t3"></div><div class="t4"></div></div>
                <CSControl:ResourceControl ResourceName="PopularTags" Tag="H4" CssClass="CommonSidebarHeader"  />
                <div class="CommonSidebarContent">
            </LeaderTemplate>
            <TrailerTemplate>
                </div>
                <CSControl:SiteUrl ResourceName="ViewAllTags" UrlName="tags_home" Tag="Div" CssClass="CommonSidebarFooter"  />
            </TrailerTemplate>
        </CSControl:TagCloud>
    </div>-->
    </div>
</asp:Content>

<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Discussions.Controls.CSForumThemePage" MasterPageFile="forums.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Discussions.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>
<%@ Register TagPrefix="CustomAvatar" TagName="CSAvatar" Src="CustomAvatar.ascx" %> <!--Added by Kishan-->

<script language="C#" runat="server">

    void Page_Load()
    {
        if (CurrentThread != null)
            SetTitle(CurrentThread.Subject, Title, true);

        if (CurrentForum != null && !Permissions.ValidatePermissions(CurrentForum, Permission.Read, CurrentCSContext.User))
            PermissionBase.RedirectOrException(CSExceptionType.AccessDenied);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
<div class="CommonContentArea">
    <CSForum:BreadCrumb runat="server" Tag="Div" CssClass="CommonBreadCrumbArea" />

	<CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdTop runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
	
    <CSControl:WrappedLiteral runat="server" id="Title" Tag="h2" CssClass="CommonTitle" />		
	<div class="CommonContent">
        
        <ul class="ForumPostList">
			<li>
				<div class="ForumPostArea">
					<h4 class="ForumPostHeader">
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr valign="middle">
								<td align="left">
								    <CSForum:ForumPostData runat="server" LinkTo="PermaLink"><ContentTemplate><CSControl:ThemeImage runat="server" BorderWidth="0" ImageUrl="~/images/icon_post_show.gif" /></ContentTemplate></CSForum:ForumPostData>
								    <CSForum:ForumPostData runat="server" Property="PostDate" IncludeTimeInDate="true" />    
								</td>
								<td align="right">
								    <CSForum:ForumPostData runat="server" ResourceName="PostView_ReplyToLabel" LinkTo="ParentPost" />
								    <CSForum:ForumPostRating runat="server" RatingCssClass="CommonRateControl" RatingReadOnlyCssClass="CommonRateControlReadOnly" RatingActiveCssClass="CommonRateControlActive" ImagesBaseUrl="~/Themes/default/images/common/" />
								</td>
							</tr>
						</table>
					</h4>
					<table cellspacing="0" cellpadding="0" border="0" width="100%">
						<tr valign=top>
							<td rowspan="2" class="ForumPostUserArea">
							    <CustomAvatar:CSAvatar ID="Avatar1" runat="server" UserName="GETFROMPARENTREPEATERITEM" /> <!--Added by Kishan-->
								<div class="ForumPostUserContent" style="visibility:hidden"> <!--Visibility set to hidden by Kishan-->
								<ul class="ForumPostUserPropertyList">
									<li class="ForumPostUserName">
									    <CSControl:PlaceHolder runat="server">
								            <DisplayConditions Operator="Not"><CSControl:UserPropertyValueComparison runat="server" ComparisonProperty="IsAnonymous" Operator="IsSetOrTrue" /></DisplayConditions>
								            <ContentTemplate>
									            <CSControl:PlaceHolder runat="server">
									                <DisplayConditions><CSControl:UserPropertyValueComparison ComparisonProperty="IsOnline" Operator="IsSetOrTrue" runat="server" /></DisplayConditions>
									                <ContentTemplate><CSControl:ThemeImage ImageUrl="~/images/user_IsOnline.gif" runat="server" /></ContentTemplate>
									            </CSControl:PlaceHolder>
									            <CSControl:PlaceHolder runat="server">
									                <DisplayConditions Operator="Not"><CSControl:UserPropertyValueComparison ComparisonProperty="IsOnline" Operator="IsSetOrTrue" runat="server" /></DisplayConditions>
									                <ContentTemplate><CSControl:ThemeImage ImageUrl="~/images/user_IsOffline.gif" runat="server" /></ContentTemplate>
									            </CSControl:PlaceHolder>
									        </ContentTemplate>
									    </CSControl:PlaceHolder>
									    <CSControl:UserData runat="server" LinkTo="Profile" Property="DisplayName" />
									</li>
									<CSControl:UserAvatar runat="server" BorderWidth="1" Tag="Li" CssClass="ForumPostUserAvatar" />
									<CSControl:PlaceHolder runat="server">
							            <DisplayConditions Operator="Not"><CSControl:UserPropertyValueComparison runat="server" ComparisonProperty="IsAnonymous" Operator="IsSetOrTrue" /></DisplayConditions>
							            <ContentTemplate>
									        <CSControl:UserPostIcons runat="server" Tag="Li" CssClass="ForumPostUserIcons" />
									        <CSControl:UserData runat="server" Property="DateCreated" Tag="Li" CssClass="ForumPostUserAttribute"><LeaderTemplate><CSControl:ResourceControl runat="server" ResourceName="PostFlatView_Joined" /></LeaderTemplate></CSControl:UserData>
									        <CSControl:UserProfileData runat="server" Property="Location" Tag="Li" CssClass="ForumPostUserAttribute" />
									        <CSControl:UserData runat="server" LinkTo="PostsSearch" Property="TotalPosts" Tag="Li" CssClass="ForumPostUserAttribute">
								                    <DisplayConditions><CSControl:SiteSettingsPropertyValueComparison runat="server" ComparisonProperty="PostingActivityDisplay" ComparisonValue="PostCount" Operator="EqualTo" /></DisplayConditions>
								                    <LeaderTemplate><CSControl:ResourceControl ResourceName="PostFlatView_Posts" runat="server" /></LeaderTemplate>
								                </CSControl:UserData>
								                <CSControl:UserData runat="server" LinkTo="PostsSearch" Property="UserRank" Tag="Li" CssClass="ForumPostUserAttribute">
								                    <DisplayConditions><CSControl:SiteSettingsPropertyValueComparison runat="server" ComparisonProperty="PostingActivityDisplay" ComparisonValue="UserRank" Operator="EqualTo" /></DisplayConditions>
								                </CSControl:UserData>
                                            <CSControl:UserData runat="server" Property="Points" Tag="Li" CssClass="ForumPostUserAttribute"><LeaderTemplate><CSControl:ResourceControl runat="server" ResourceName="PostFlatView_UserPoints" /></LeaderTemplate></CSControl:UserData>
                                            <CSControl:UserRoleIcons runat="server" LinkTo="Role" Tag="Li" CssClass="ForumPostRoleIcons">
                                                <SeparatorTemplate><br /></SeparatorTemplate>
                                            </CSControl:UserRoleIcons>
                                        </ContentTemplate>
                                    </CSControl:PlaceHolder>
								</ul>
								</div>
							</td>
							<td class="ForumPostContentArea">
								<div class="ForumPostTitleArea">
									<h4 class="ForumPostTitle">
									    <CSControl:PostEmoticon runat="server" />
										<CSForum:ForumPostData runat="server" Property="Subject" />
									</h4>
									
								    <CSForum:PostAttachmentData runat="server" Property="FileName" LinkTo="Attachment" Tag="Div" CssClass="ForumPostAttachment" />

									<div class="ForumPostButtons">
									    <CSControl:ResourceControl runat="server" ResourceName="Button_Locked" CssClass="CommonImageTextButton CommonLockedButton" Tag="Span">
									        <DisplayConditions><CSForum:ForumPostPropertyValueComparison ComparisonProperty="IsLocked" Operator="IsSetOrTrue" runat="server" /></DisplayConditions>
									    </CSControl:ResourceControl>
									    <CSForum:ForumPostData runat="server" LinkTo="Reply" ResourceName="Button_Reply" LinkCssClass="CommonImageTextButton CommonReplyButton" />
									    <CSForum:ForumPostData runat="server" LinkTo="Delete" ResourceName="Button_Delete" LinkCssClass="CommonImageTextButton CommonDeleteButton" />
									    <CSForum:ForumPostData runat="server" LinkTo="Edit" ResourceName="Button_Edit" LinkCssClass="CommonImageTextButton CommonEditButton" />
									    <CSControl:FavoritePopupMenu ResourceName="FavoritePopupMenu_Text" runat="server" ButtonActiveCssClass="CommonImageTextButtonHighlight CommonFavoriteButton" ButtonInactiveCssClass="CommonImageTextButton CommonFavoriteButton" MenuGroupCssClass="CommonContextMenuGroup" MenuItemCssClass="CommonContextMenuItem" MenuItemSelectedCssClass="CommonContextMenuItemHover" MenuItemExpandedCssClass="CommonContextMenuItemExpanded" MenuItemIconWidth="20" MenuItemIconHeight="20" MenuDirection="UpDown" />
									    <CSControl:UserContactPopupMenu ResourceName="ContactPopupMenu_Text" runat="server" ButtonActiveCssClass="CommonImageTextButtonHighlight CommonContactButton" ButtonInactiveCssClass="CommonImageTextButton CommonContactButton" MenuGroupCssClass="CommonContextMenuGroup" MenuItemCssClass="CommonContextMenuItem" MenuItemSelectedCssClass="CommonContextMenuItemHover" MenuItemExpandedCssClass="CommonContextMenuItemExpanded" MenuItemIconWidth="20" MenuItemIconHeight="20" MenuDirection="UpDown" />
									    <CSForum:ForumPostModerationPopupMenu ResourceName="ModerationPopupMenu_Text" runat="server" ButtonActiveCssClass="CommonTextButtonHighlight" ButtonInactiveCssClass="CommonTextButton" MenuGroupCssClass="CommonContextMenuGroup" MenuItemCssClass="CommonContextMenuItem" MenuItemSelectedCssClass="CommonContextMenuItemHover" MenuItemExpandedCssClass="CommonContextMenuItemExpanded" MenuItemIconWidth="20" MenuItemIconHeight="20" MenuDirection="UpDown">
									        <SuccessActions><CSControl:GoToModifiedUrlAction runat="server" /></SuccessActions>
									    </CSForum:ForumPostModerationPopupMenu>
									</div>
								</div>
								
								<table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%" style="table-layout: fixed;">
									<tr><td>
									<div class="ForumPostBodyArea">
									<div class="ForumPostContentText">
										<CSForum:ForumPostData Property="FormattedBody" runat="server" />
										<CSForum:ForumPostData Property="EditNotes" runat="server">
		                                    <DisplayConditions><CSForum:ForumConfigurationPropertyValueComparison runat="server" ComparisonProperty="DisplayEditNotesInPost" Operator="IsSetOrTrue" /></DisplayConditions>
		                                    <LeaderTemplate><br /><br /></LeaderTemplate>
		                                </CSForum:ForumPostData>
									</div>
									<CSControl:UserProfileData Property="SignatureFormatted" runat="server" Tag="Div" CssClass="ForumPostSignature" />
									</div>
									</td></tr>
								</table>
							</td>
						</tr>
						<tr valign="bottom">
							<td class="ForumPostFooterArea">	
								<CSForum:ForumPostTagEditableList runat="server" EditorLinkCssClass="CommonTextButton" EditorCssClass="CommonInlineTagEditor" Tag="Div" />
								<ul class="ForumPostStatistics CommonPrintHidden" style="clear: both;">
								    <li></li>
								    <CSForum:ForumPostData runat="server" Property="Points" ResourceName="PostFlatView_PostPoints" Tag="Li" />
									<CSForum:ForumPostData runat="server" Property="UserHostAddress" ResourceName="PostFlatView_IPAddress" Tag="Li" />
									<CSForum:ForumPostData runat="server" LinkTo="Report" ResourceName="Report" Tag="Li" />
									<CSForum:ForumPostData runat="server" LinkTo="QuickReply" ResourceName="QuickReply" Tag="Li" />
								</ul>
							</td>
						</tr>
					</table>
				</div>
			</li>
		</ul>
		
		<CSForum:ForumPostData runat="server" LinkTo="Thread" ResourceName="SinglePostView_ViewThread" LinkCssClass="CommonTextButton" />
       
	</div>
	
	<CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdBottom runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="rcr" runat="server" />
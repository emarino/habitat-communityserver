<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Controls.CSThemePage" MasterPageFile="home.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Discussions.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        Response.Redirect("/Forums");// Added by Deepak to never let the user see the CS Home Page.
        SetTitle(CurrentCSContext.SiteSettings.SiteName, false);
    }

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="bcr" runat="server">
<div class="CommonContentArea">
	<CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdTop runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>    
	<div class="CommonContent">
        <CSControl:ContentPart ContentName="welcome" runat="server" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
            <DefaultContentTemplate>
                <h2 class="CommonTitle">Welcome!</h2>
                <div class="CommonContent">
                    Thanks for trying Community Server 2007!
                    <p>Community Server is the platform that powers the web's most popular communities and we're thrilled that you've taken the time to check it out (P.S., we're confident that you'll love it!).</p>
                    <p>We've designed Community Server 2007 to be amazingly simple to use.  It now includes browser-based tools to make changing page layout, selecting fonts and font sizes, site colors, and images unbelievably simple.</p>
                    <p>For designers and non-technical people these tools will enable Community Server to look however you want without requiring any knowledge of HTML, CSS, or .NET.  If you're a .NET developer we've done a lot for you too.  You'll find simpler and cleaner organization of code and files.  We're almost ashamed to admit it, but in Community Server 2.1 there were nearly 55 files for a blog theme.  But we're proud that in Community Server 2007 a blog theme has only 8 files!</p>
                    <p>You'll also find tools for exporting and importing themes as single files (images and all) -- making sharing your cool custom designs as easy as sending an email.</p>
                    <p><a href="http://communityserver.org/r.ashx?2">Telligent</a>, the company that builds Community Server, addtionally offers full creative, .NET consulting, hosting, and customization for your Community Server implementation.  If we can't help we've got a number of partners that can.</p>
                    <p>Below you'll find some helpful information for evaluating Community Server 2007 and don't forget to visit <a href="http://communityserver.org/r.ashx?3">www.communityserver.org</a> -- the "community" for Community Server.</p>
                    <p>Again, we sincerely appreciate you taking the time to evaluate or buy our software.  If you have any <a href="http://communityserver.org/i/contact.aspx">questions or problems please contact us</a> or drop me a note!</p>
                    <p>Sincerely,</p>
                    <img src="utility/images/cs-home-robsignature.gif" width="261" height="64" alt="Rob M. Howard" />
                    <p>Robert M. Howard (<a href="mailto:rhoward@telligent.com">rhoward@telligent.com</a>)<br />CEO, Telligent Systems, Inc.</p>

                    <div class="CommonListArea">
                        <div class="CommonListTitle">Documentation &amp; Support</div>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <thead>
                                <tr>
                                    <th class="CommonListHeaderLeftMost">&nbsp;</th>
                                    <th class="CommonListHeader">Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr valign="top">
                                    <td class="CommonListCellLeftMost"><a href="http://communityserver.org/r.ashx?3"><img src="utility/images/cs-home-weblink.gif" alt="" border="0" /></a></td>
                                    <td class="CommonListCell">
                                        <div style="font-size:130%; font-weight: bold;"><a href="http://communityserver.org/r.ashx?3">www.communityserver.org</a></div>
                                        <div>CommunityServer.org is the community for Community Server.  Here you can meet other people using the sofware, download resources, or just browse around to judge what an active customer base Community Server has.</div>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td class="CommonListCellLeftMost"><a href="http://communityserver.org/files/folders/565077/download.aspx"><img src="utility/images/cs-home-pdflink.gif" alt="" border="0" /></a></td>
                                    <td class="CommonListCell">
                                        <div style="font-size:130%; font-weight: bold;"><a href="http://communityserver.org/files/folders/565077/download.aspx">Community Server Evaluation Guide</a></div>
                                        <div>This 45+ page document covers many of the frequently asked questions about Community Server as well as a break-down of many of the features found in the various Community Server applications.</div>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td class="CommonListCellLeftMost"><a href="http://communityserver.org/files/folders/567243/download.aspx"><img src="utility/images/cs-home-pdflink.gif" alt="" border="0" /></a></td>
                                    <td class="CommonListCell">
                                        <div style="font-size:130%; font-weight: bold;"><a href="http://communityserver.org/files/folders/567243/download.aspx">Community Server 2007 Licensing Guide</a></div>
                                        <div>This document details how Community Server 2007 is licensed for both commercial and non-commercial customers.  These changes go into effect April 15th 2007.  <a href="http://communityserver.org/i/contact.aspx">If you have any questions, please contact us</a>.</div>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td class="CommonListCellLeftMost"><a href="http://communityserver.org/files/folders/558125/download.aspx"><img src="utility/images/cs-home-pdflink.gif" alt="" border="0" /></a></td>
                                    <td class="CommonListCell">
                                        <div style="font-size:130%; font-weight: bold;"><a href="http://communityserver.org/files/folders/558125/download.aspx">Community Server Case Studies</a></div>
                                        <div>Want to read about how other customers have used Community Server?  This document details 15 different customers in a variety of industries and details how they are using Community Server.</div>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td class="CommonListCellLeftMost"><a href="http://videos.communityserver.org/Cs_Overview/Cs_Overview.html"><img src="utility/images/cs-home-medialink.gif" alt="" border="0" /></a></td>
                                    <td class="CommonListCell">
                                        <div style="font-size:130%; font-weight: bold;"><a href="http://videos.communityserver.org/Cs_Overview/Cs_Overview.html">Community Server Case Studies Video</a></div>
                                        <div>Watch this video for a more narrative overview of how customers are using Community Server.  Although briefer, the video provides more insight into each case study.  <a href="http://videos.communityserver.org/Cs_Overview/Cs_Overview.zip">Download the video</a>.</div>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td class="CommonListCellLeftMost"><a href="http://videos.communityserver.org/Walkthrough/Walkthrough.html"><img src="utility/images/cs-home-medialink.gif" alt="" border="0" /></a></td>
                                    <td class="CommonListCell">
                                        <div style="font-size:130%; font-weight: bold;"><a href="http://videos.communityserver.org/Walkthrough/Walkthrough.html">Community Server Walk Through Video</a></div>
                                        <div>This 45+ minute video is the same demo we've given to hundreds of customers in interactive demo sessions.  It provides a high-level tour of all the major features of Community Server.  <a href="http://videos.communityserver.org/Walkthrough/Walkthrough.zip">Download the video</a>.</div>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td class="CommonListCellLeftMost"><a href="http://docs.communityserver.org/"><img src="utility/images/cs-home-questionlink.gif" alt="" border="0" /></a></td>
                                    <td class="CommonListCell">
                                        <div style="font-size:130%; font-weight: bold;"><a href="http://docs.communityserver.org/">Community Server Documentation</a></div>
                                        <div>Our documentation includes overviews of the various features, details on how to setup Community Server and more.</div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                
                    <div class="CommonListArea">
                        <div class="CommonListTitle">Vendors</div>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <thead>
                                <tr>
                                    <th class="CommonListHeaderLeftMost">&nbsp;</th>
                                    <th class="CommonListHeader">Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr valign="top">
                                    <td class="CommonListCellLeftMost"><a href="http://cutesoft.net/Products/Community-Server/default.aspx"><img src="utility/images/cs-home-weblink.gif" alt="" border="0" /></a></td>
                                    <td class="CommonListCell">
                                        <div style="font-size:130%; font-weight: bold;"><a href="http://cutesoft.net/Products/Community-Server/default.aspx">CuteSoft Components, Inc.</a></div>
                                        <div>CuteSoft builds a number of chat, messenger and other tools for adding additional capabilities to your Community Server system.</div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                    <div class="CommonListArea">
                        <div class="CommonListTitle">Books</div>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <thead>
                                <tr>
                                    <th class="CommonListHeaderLeftMost">&nbsp;</th>
                                    <th class="CommonListHeader">Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr valign="top">
                                    <td class="CommonListCellLeftMost"><img src="utility/images/cs-home-procs.gif" alt="" border="0" /></td>
                                    <td class="CommonListCell">
                                        <div style="font-size:130%; font-weight: bold;">Professional Community Server</div>
                                        <div>This book provides a developer's guide to using Community Server and is written for someone who needs to dig into the application and understand how to take Community Server to the next level.<br />Buy now on <a href="http://www.amazon.com/Professional-Community-Server-Wyatt-Preul/dp/0470108282/ref=pd_bxgy_b_img_b/102-6484994-8888161">Amazon.com</a> or <a href="http://search.barnesandnoble.com/booksearch/isbnInquiry.asp?z=y&amp;EAN=9780470108284&amp;itm=2">Barnes and Noble</a>.</div>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td class="CommonListCellLeftMost"><img src="utility/images/cs-home-csquickly.gif" alt="" border="0" /></td>
                                    <td class="CommonListCell">
                                        <div style="font-size:130%; font-weight: bold;">Community Server Quickly</div>
                                        <div>This book provides a comprehensive tour of Community Server and is written for someone who needs to manage a Community Server system.  It does include some detail on customization and extending the platform.<br />Buy now from <a href="http://www.amazon.com/Community-Server-Quickly-Administration-Customization/dp/1847190871/sr=8-1/qid=1167746266/ref=pd_bbs_sr_1/002-4941344-8885657">Amazon</a> or <a href="http://search.barnesandnoble.com/booksearch/isbnInquiry.asp?z=y&amp;EAN=9781847190871&amp;itm=1">Barnes and Noble</a>.</div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                    <div class="CommonListArea">
                        <div class="CommonListTitle">Recommended Hosts</div>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <thead>
                                <tr>
                                    <th class="CommonListHeaderLeftMost">&nbsp;</th>
                                    <th class="CommonListHeader">Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr valign="top">
                                    <td class="CommonListCellLeftMost"><a href="http://communityserver.org/r.ashx?F"><img src="utility/images/cs-home-weblink.gif" alt="" border="0" /></a></td>
                                    <td class="CommonListCell">
                                        <div style="font-size:130%; font-weight: bold;"><a href="http://communityserver.org/r.ashx?F">CommunityServer.com</a></div>
                                        <div>CommunityServer.com provides dedicated Community Server hosting and allows you to get your web community up-and-running quickly.</div>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td class="CommonListCellLeftMost"><a href="http://orcsweb.com/"><img src="utility/images/cs-home-weblink.gif" alt="" border="0" /></a></td>
                                    <td class="CommonListCell">
                                        <div style="font-size:130%; font-weight: bold;"><a href="http://orcsweb.com/">ORCS Web</a></div>
                                        <div>ORCS Web is a premier hosting facilitator and is the host of choice for Telligent's high-end Community Server customers.</div>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td class="CommonListCellLeftMost"><a href="http://aquesthosting.com/"><img src="utility/images/cs-home-weblink.gif" alt="" border="0" /></a></td>
                                    <td class="CommonListCell">
                                        <div style="font-size:130%; font-weight: bold;"><a href="http://aquesthosting.com/">Aquest Hosting</a></div>
                                        <div>Aquest is a recommended host and has experience with a number of customers running Community Server.</div>
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td class="CommonListCellLeftMost"><a href="http://serverintellect.com/"><img src="utility/images/cs-home-weblink.gif" alt="" border="0" /></a></td>
                                    <td class="CommonListCell">
                                        <div style="font-size:130%; font-weight: bold;"><a href="http://serverintellect.com/">Server Intellect</a></div>
                                        <div>Server Intellect is a recommended host and has experience with a number of customers running Community Server.</div>
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                
                </div>
            </DefaultContentTemplate>
        </CSControl:ContentPart>
	</div>
	
	<CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdBottom runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="rcr" runat="server">
	<div class="CommonSidebar">
	    <CSControl:ContentPart ContentName="featured" runat="server" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
            <DefaultContentTemplate>
                <div class="CommonSidebarArea">
                    <div class="CommonSidebarTopRound"><div class="t1"></div><div class="t2"></div><div class="t3"></div><div class="t4"></div></div>
                    <h4 class="CommonSidebarHeader">Featured Item</h4>
                    <div class="CommonSidebarContent">
                        Content Management is easy in Community Server! 
                        <p>
                            Sign-in with your Admin account and double-click to edit me!
                        </p>
                    </div>
                </div>
            </DefaultContentTemplate>
        </CSControl:ContentPart>
	
        <CSForum:ThreadList runat="server">
	        <DisplayConditions>
	            <CSControl:UserInRoleCondition Role="Registered Users" UseAccessingUser="true" runat="server" />
	            <CSControl:ApplicationPropertyValueComparison runat="server" ApplicationType="forum" ComparisonProperty="Enabled" Operator="isSetOrTrue" />
	        </DisplayConditions>
	        <QueryOverrides UserFilter="ShowTopicsParticipatedIn" GroupID="-1" ForumID="-1" PageSize="10" />
	        <HeaderTemplate>
                <div class="CommonSidebarArea">
                <div class="CommonSidebarTopRound"><div class="t1"></div><div class="t2"></div><div class="t3"></div><div class="t4"></div></div>
                <CSControl:SiteUrl UrlName="user_MyForums" ResourceName="Utility_ForumAnchorType_MenuMyForums" Tag="H4" CssClass="CommonSidebarHeader" runat="server" />
				<div class="CommonSidebarContent">
				<ul class="CommonSidebarList">
			</HeaderTemplate>
			<ItemTemplate>
					<CSForum:ThreadData runat="server" LinkTo="Thread" Property="Subject" TruncateAt="22" Tag="Li" />
			</ItemTemplate>
			<FooterTemplate>
			    </ul>
				</div>
			    </div>
			</FooterTemplate>
	    </CSForum:ThreadList>
	    
	    <CSForum:ThreadList runat="server">
	        <DisplayConditions><CSControl:ApplicationPropertyValueComparison runat="server" ApplicationType="forum" ComparisonProperty="Enabled" Operator="isSetOrTrue" /></DisplayConditions>
	        <QueryOverrides ActiveTopics="true" GroupID="-1" ForumID="-1" PageSize="10" />
	        <HeaderTemplate>
                <div class="CommonSidebarArea">
                <div class="CommonSidebarTopRound"><div class="t1"></div><div class="t2"></div><div class="t3"></div><div class="t4"></div></div>
                <CSControl:SiteUrl UrlName="post_Active" Parameter1="-1" ResourceName="Utility_ForumAnchorType_PostsActive" Tag="H4" CssClass="CommonSidebarHeader" runat="server" />
				<div class="CommonSidebarContent">
				<ul class="CommonSidebarList">
			</HeaderTemplate>
			<ItemTemplate>
					<CSForum:ThreadData runat="server" LinkTo="Thread" Property="Subject" TruncateAt="22" Tag="Li" />
			</ItemTemplate>
			<FooterTemplate>
			    </ul>
				</div>
			    </div>
			</FooterTemplate>
	    </CSForum:ThreadList>
	    
	    <CSForum:ThreadList runat="server">
	        <DisplayConditions><CSControl:ApplicationPropertyValueComparison runat="server" ApplicationType="forum" ComparisonProperty="Enabled" Operator="isSetOrTrue" /></DisplayConditions>
	        <QueryOverrides UnAnsweredOnly="true" GroupID="-1" ForumID="-1" PageSize="10" />
	        <HeaderTemplate>
                <div class="CommonSidebarArea">
                <div class="CommonSidebarTopRound"><div class="t1"></div><div class="t2"></div><div class="t3"></div><div class="t4"></div></div>
				<CSControl:SiteUrl UrlName="post_Unanswered" Parameter1="-1" ResourceName="Utility_ForumAnchorType_PostsUnanswered" Tag="H4" CssClass="CommonSidebarHeader" runat="server" />
				<div class="CommonSidebarContent">
				<ul class="CommonSidebarList">
			</HeaderTemplate>
			<ItemTemplate>
					<CSForum:ThreadData runat="server" LinkTo="Thread" Property="Subject" TruncateAt="22" Tag="Li" />
			</ItemTemplate>
			<FooterTemplate>
			    </ul>
				</div>
			    </div>
			</FooterTemplate>
	    </CSForum:ThreadList>

        <CSGallery:GalleryPostList runat="server" Tag="Div" CssClass="CommonSidebarArea">
            <DisplayConditions><CSControl:ApplicationPropertyValueComparison runat="server" ApplicationType="gallery" ComparisonProperty="Enabled" Operator="isSetOrTrue" /></DisplayConditions>
	        <QueryOverrides SortBy="PictureDate" SortOrder="Descending" PageSize="5" />
	        <LeaderTemplate>
	            <div class="CommonSidebarTopRound"><div class="t1"></div><div class="t2"></div><div class="t3"></div><div class="t4"></div></div>
	            <CSControl:ResourceControl runat="server" ResourceName="Gallery_PortalPictureListing_TitlePostDate" Tag="H4" CssClass="CommonSidebarHeader" />
	            <div class="CommonSidebarContent">
	        </LeaderTemplate>
	        <HeaderTemplate>
	            <ul class="CommonSidebarImageList">
            </HeaderTemplate>
            <ItemTemplate>
                <li><CSGallery:GalleryPostData LinkTo="ViewPicture" runat="server" Tag="Div" CssClass="CommonSidebarImage"><ContentTemplate><CSGallery:GalleryPostImage ImageType="Other" Width="120" Height="90" runat="server" /></ContentTemplate></CSGallery:GalleryPostData></li>
            </ItemTemplate>
            <NoneTemplate>
	            <li><CSControl:ResourceControl ResourceName="None" runat="server" Tag="Div" CssClass="CommonSidebarImage" /></li>
            </NoneTemplate>
            <FooterTemplate>
	            </ul>
            </FooterTemplate>
            <TrailerTemplate>
                </div>
            </TrailerTemplate>
	    </CSGallery:GalleryPostList>
    </div>
</asp:Content>
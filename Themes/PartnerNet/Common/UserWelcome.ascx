<%@ Control Language="C#" %>

<CSControl:UserData runat="server" UseAccessingUser="true" Visible="false"> <%-- Visibility set to false by Kishan --%>
    <DisplayConditions><CSControl:UserPropertyValueComparison runat="server" UseAccessingUser="true" ComparisonProperty="IsAnonymous" Operator="IsSetOrTrue" /></DisplayConditions>
    <ContentTemplate>
        <CSControl:SiteSettingsData runat="server" Property="SiteName" ResourceName="DisplayUserWelcome_AlternateUserWelcome" />
        <CSControl:UserData runat="server" LinkTo="Login" ResourceName="A_Login" />
         <CSControl:SiteUrl runat="server" UrlName="user_Register" Parameter1="" ResourceName="register">
            <DisplayConditions Operator="Not"><CSControl:SiteSettingsPropertyValueComparison ComparisonProperty="AccountActivation" ComparisonValue="InvitationOnly" Operator="EqualTo" runat="server" /></DisplayConditions>
            <LeaderTemplate>| </LeaderTemplate>
        </CSControl:SiteUrl>
        <CSControl:UserData runat="server" ResourceName="help" LinkTo="FAQ"><LeaderTemplate>| </LeaderTemplate></CSControl:UserData>
     </ContentTemplate>
</CSControl:UserData>

<CSControl:UserData runat="server" UseAccessingUser="true">
    <DisplayConditions Operator="Not"><CSControl:UserPropertyValueComparison runat="server" UseAccessingUser="true" ComparisonProperty="IsAnonymous" Operator="IsSetOrTrue" /></DisplayConditions>
    <ContentTemplate>
        <!-- Modified the commented line to <CSControl:UserData LinkTo="Profile" Property="DisplayName"></CSControl:UserData>
        <CSControl:UserData LinkTo="Profile" Property="DisplayName"><LeaderTemplate><CSControl:ResourceControl runat="server" ResourceName="Main_DisplayUserWelcome_UserWelcome" /></LeaderTemplate></CSControl:UserData>-->
        <CSControl:UserData runat="server" LinkTo="Profile" Property="DisplayName"></CSControl:UserData>
         <%-- Visibility set to false by Kishan --%>
        <CSControl:UserData runat="server" Visible="false" LinkTo="EditProfile" ResourceName="BreadCrumb_EditUserProfile"><LeaderTemplate>| </LeaderTemplate></CSControl:UserData>
        <CSControl:SiteSettingsData runat="server">
            <DisplayConditions Operator="Not">
                <CSControl:SiteSettingsPropertyValueComparison runat="server" ComparisonProperty="AuthenticationType" Operator="EqualTo" ComparisonValue="windows" />
                <CSControl:SiteSettingsPropertyValueComparison runat="server" ComparisonProperty="AuthenticationType" Operator="EqualTo" ComparisonValue="passport" />
            </DisplayConditions>
            <ContentTemplate>
                 <%-- Visibility set to false by Kishan --%>
                <CSControl:UserData Visible="false" runat="server" LinkTo="Logout" ResourceName="logout"><LeaderTemplate>| </LeaderTemplate></CSControl:UserData>
            </ContentTemplate>
        </CSControl:SiteSettingsData>         
        <CSControl:SiteUrl runat="server" UrlName="user_Invite" ResourceName="invite" Visible="false"> <%-- Visibility set to false by Kishan --%>
            <DisplayConditions><CSControl:SiteSettingsPropertyValueComparison ComparisonProperty="AccountActivation" ComparisonValue="InvitationOnly" Operator="EqualTo" runat="server" /></DisplayConditions>
            <LeaderTemplate>| </LeaderTemplate>
        </CSControl:SiteUrl>
         <%-- Visibility set to false by Kishan --%>
        <CSControl:UserData runat="server" Visible="false" ResourceName="help" LinkTo="FAQ"><LeaderTemplate>| </LeaderTemplate></CSControl:UserData>
        <CSControl:UserData runat="server" Visible="false" LinkTo="UserPrivateMessages" ResourceName="PrivateMessage_Unread"><LeaderTemplate>| </LeaderTemplate></CSControl:UserData>
    </ContentTemplate>
</CSControl:UserData>
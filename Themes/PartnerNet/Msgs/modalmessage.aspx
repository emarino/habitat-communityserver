<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Controls.CSThemePage" MasterPageFile="../Common/modal.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        if (CurrentMessage != null)   
            SetTitle(CurrentMessage.Title, Title, true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">

<div align=center>
    <div class="CommonMessageArea">
        <CSControl:WrappedLiteral runat="server" Tag="H4" CssClass="CommonMessageTitle" ID="Title" />
        <CSControl:MessageData Property="Body" runat="server" Tag="Div" CssClass="CommonMessageContent" />
    </div>
</div>

</asp:Content>
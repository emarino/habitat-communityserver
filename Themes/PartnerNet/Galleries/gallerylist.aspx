<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Galleries.Controls.CSGalleryThemePage" MasterPageFile="galleries.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Galleries.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        SetTitle(ResourceManager.GetString("Gallery_AggregateGalleryListing_Title"), Title, true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
    <div class="CommonContentArea">
	    <CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	        <DefaultContentTemplate>
	            <CSUserControl:AdTop runat="server" />
	        </DefaultContentTemplate>
	    </CSControl:AdPart>
	    
	    <CSControl:WrappedLiteral runat="server" ID="Title" Tag="H2" CssClass="CommonTitle" />
	    <div class="CommonContent">
	        <CSGallery:GroupList runat="server">
	            <QueryOverrides />
	            <ItemTemplate>
		            <div class="CommonListArea">
		                <CSGallery:GroupData runat="server" Property="Name" Tag="H4" CssClass="CommonListTitle" />

                        <CSGallery:GalleryList runat="server">
                            <QueryOverrides PagerID="Pager" />
				            <HeaderTemplate>
					            <table width="100%" cellpadding="0" cellspacing="0">
						            <thead>
						                <tr>
							            <th class="CommonListHeaderLeftMost" width="*"><CSControl:ResourceControl ResourceName="Gallery_AggregateGalleryListing_Gallery" runat="server" /></th>
							            <th class="CommonListHeader" align="center" width="150"><CSControl:ResourceControl ResourceName="Gallery_AggregateGalleryListing_LastChanged" runat="server" /></th>
							            <th class="CommonListHeader" align="center" width="150"><CSControl:ResourceControl ResourceName="Gallery_AggregateGalleryListing_PictureCount" runat="server" /></th>
							            </tr>
						            </thead>
						            <tbody>
				            </HeaderTemplate>
				            <ItemTemplate>
					            <tr>
						            <td class="CommonListCellLeftMost" valign="top">
						                <CSGallery:GalleryData runat="server" LinkTo="ViewGallery" Property="Name" /><br />
                                        <CSGallery:GalleryData runat="server" Property="Description" Tag="Em" />
						            </td>
						            <td class="CommonListCell" align="center"><CSGallery:GalleryData runat="server" Property="MostRecentPostDate" IncludeTimeInDate="true" />&nbsp;</td>
						            <td class="CommonListCell" align="center"><CSGallery:GalleryData runat="server" Property="TotalThreads" /></td>
					            </tr>
				            </ItemTemplate>
				            <FooterTemplate>
				                </tbody>
					            </table>
				            </FooterTemplate>
			            </CSGallery:GalleryList>
            								
			            <CSControl:PostbackPager id="Pager" runat="server" Tag="Div" CssClass="CommonPagingArea" ShowTotalSummary="true" />
		            </div>
	            </ItemTemplate>
	            <SeparatorTemplate>
		            <div class="spacer">&nbsp;</div>
	            </SeparatorTemplate>
            </CSGallery:GroupList>
	    </div>
	    
	    <CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	        <DefaultContentTemplate>
	            <CSUserControl:AdBottom runat="server" />
	        </DefaultContentTemplate>
	    </CSControl:AdPart>
    </div>
</asp:Content>

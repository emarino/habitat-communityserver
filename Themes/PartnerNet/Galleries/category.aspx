<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Galleries.Controls.CSGalleryThemePage" MasterPageFile="galleries.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Galleries.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="GallerySidebar" Src="gallerysidebar.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        if (CurrentPostCategory != null)
            SetTitle(CurrentPostCategory.Name, true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">

    <div class="CommonContentArea">
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <CSControl:UserAvatar runat="server" Tag="Td" CssClass="OwnerAvatar" />
                <td>
                    <CSGallery:GalleryData Property="Name" LinkTo="ViewGallery" runat="server" Tag="h2" CssClass="CommonTitle" />
                    <div class="CommonContent">
                        <CSGallery:GalleryData Property="Description" runat="server" />
                        <div class="ClearLeft">&nbsp;</div>
                    </div>
                </td>
            </tr>
        </table>
    </div>

	<div class="CommonContentArea">
	    <div class="CommonContent">
		     <h3 class="ContentHeader"><CSGallery:PostCategoryData Property="Name" runat="server" /></h3>
            <CSGallery:BreadCrumb runat="server" Tag="Div" CssClass="BreadcrumbList" />
    	    
    	    <CSGallery:GalleryPostList runat="server">
    	        <QueryOverrides PagerID="Pager" />
			    <HeaderTemplate>
                    <div class="PictureList"><div class="ClearLeft">&nbsp;</div>
			    </HeaderTemplate>
			    <ItemTemplate>
				    <div class="pic">
					    <span><CSGallery:GalleryPostData LinkTo="ViewPicture" runat="server"><ContentTemplate><CSGallery:GalleryPostImage ImageType="Thumbnail" runat="server"/></ContentTemplate></CSGallery:GalleryPostData></span>
				    </div>
			    </ItemTemplate>
			    <NoneTemplate>
    			    <div class="CommonBodyContent"><CSControl:ResourceControl runat="server" ResourceName="ForumMembers_NoRecords" Tag="Em" /></div>
			    </NoneTemplate>
			    <FooterTemplate>
                    <div class="ClearLeft">&nbsp;</div>
                    </div>
			    </FooterTemplate>
		    </CSGallery:GalleryPostList>
        
            <div class="PictureListControls">
                <div class="PictureListPager">
                    <CSControl:Pager id="Pager" runat="server" ShowTotalSummary="true" />
                </div>
                <CSGallery:PostCategoryData ResourceName="Gallery_SlideShow" LinkTo="ViewSlideshowPro" runat="server" />
            </div>
	    </div>
	</div>
    
</asp:Content>

<asp:Content ContentPlaceHolderID="rcr" runat="server">
    <CSUserControl:GallerySidebar runat="server" />
</asp:Content>
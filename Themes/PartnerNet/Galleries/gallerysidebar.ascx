<%@ Control Language="C#" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Galleries.Components" %>

<script language="C#" runat="server">

    protected bool IsSelected(int categoryID)
    {
        CSGalleryThemePage page = this.Page as CSGalleryThemePage;
        if (page == null || page.CurrentPostCategory == null)
            return false;
        else
            return page.CurrentPostCategory.CategoryID == categoryID;
    }

</script>

<div class="CommonSidebar">
    
    <div class="CommonSidebarArea">
        <div class="CommonSidebarTopRound"><div class="t1"></div><div class="t2"></div><div class="t3"></div><div class="t4"></div></div>
        <CSControl:ResourceControl runat="server" ResourceName="Gallery_ThisGallery" Tag="H4" CssClass="CommonSidebarHeader" />
        <div class="CommonSidebarContent">
            <CSControl:PlaceHolder runat="server">
                <DisplayConditions><CSGallery:GalleryPropertyValueComparison ComparisonProperty="CategorizationType" ComparisonValue="Album" Operator="EqualTo" runat="server" /></DisplayConditions>
                <ContentTemplate>
	                <CSGallery:PostCategoryList runat="server">
	                    <QueryOverrides SortBy="Name" SortOrder="Descending" PageSize="1" PageIndex="0" />
	                    <ItemTemplate>
	                        <div class="SidebarPrevNext">
	                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
			                    <tr>
				                    <td nowrap="nowrap" align="left">
	                                    � <CSGallery:PostCategoryData runat="server" LinkTo="ViewCategory" ResourceName="Gallery_PictureDetails_PrevAlbum" />
                                    </td>       
	                    </ItemTemplate>
	                </CSGallery:PostCategoryList>
	                <CSGallery:PostCategoryList runat="server">
	                    <QueryOverrides SortBy="Name" SortOrder="Ascending" PageSize="1" PageIndex="0" />
	                    <ItemTemplate>
				                <td width="100%" align="center" class="AlbumIcon">&nbsp;</td>
				                <td nowrap="nowrap" align="right">
	                                    <CSGallery:PostCategoryData runat="server" LinkTo="ViewCategory" ResourceName="Gallery_PictureDetails_NextAlbum" /> �
	                                </td>
			                    </tr>
                            </table>
                            </div>
	                    </ItemTemplate>
	                </CSGallery:PostCategoryList> 
                </ContentTemplate>
            </CSControl:PlaceHolder>
            <CSGallery:GalleryPostData LinkTo="ViewPicture" UseRandomPost="True" runat="server" Tag="div" CssClass="SidebarPhotoBox">
                <ContentTemplate>
                    <CSGallery:GalleryPostImage ImageType="Other" Width="166" Height="166" Quality="70" runat="server" />
                </ContentTemplate>
            </CSGallery:GalleryPostData>
	        <CSGallery:GalleryData runat="server" Property="Description" Tag="p" CssClass="SidebarItemText SidebarItemDescription" />
	        <p class="SidebarItemText">
		        <CSGallery:GalleryData runat="server" Property="TotalThreads" Tag="Span" CssClass="boldtext">
		            <TrailerTemplate> <CSControl:ResourceControl runat="server" ResourceName="Gallery_PictureDetails_TotalPhotos" /><br /></TrailerTemplate>
		        </CSGallery:GalleryData>
		        <CSGallery:GalleryData runat="server" Property="MostRecentPostDate">
		            <LeaderTemplate><CSControl:ResourceControl runat="server" ResourceName="Gallery_PictureDetails_LastUpdated" /> </LeaderTemplate>
		        </CSGallery:GalleryData>
	        </p>
        </div>
    </div>
    
    <CSGallery:GalleryPostData runat="server" Tag="Div" CssClass="CommonSidebarArea">
        <ContentTemplate>
            <div class="CommonSidebarTopRound"><div class="t1"></div><div class="t2"></div><div class="t3"></div><div class="t4"></div></div>
            <h4 class="CommonSidebarHeader"><CSControl:ResourceControl runat="server" ResourceName="Gallery_PictureDetails_Statistics" id="Resourcecontrol1"/></h4>
            <div class="CommonSidebarContent">
                <ul class="CommonSidebarList">
                    <CSControl:UserData runat="server" Property="DisplayName" LinkTo="Profile">
                        <LeaderTemplate>
                            <li><CSControl:ResourceControl ResourceName="Gallery_CommentListing_PostedBy" runat="server" /> 
                        </LeaderTemplate>
                        <TrailerTemplate>
                            </li>
                        </TrailerTemplate>
                    </CSControl:UserData>
                    <CSGallery:GalleryPostData Property="Views" runat="server">
                        <LeaderTemplate>
                             <li><CSControl:ResourceControl ResourceName="Gallery_PictureDetails_ViewCount" runat="server" />: <span class="boldtext">
                        </LeaderTemplate>
                        <TrailerTemplate>
                            </span></li>
                        </TrailerTemplate>
                    </CSGallery:GalleryPostData>          
                    <CSGallery:GalleryPostData Property="Replies" runat="server">
                        <LeaderTemplate>
                             <li><CSControl:ResourceControl ResourceName="Gallery_PictureDetails_TotalComments" runat="server" />: <span class="boldtext">
                        </LeaderTemplate>
                        <TrailerTemplate>
                            </span></li>
                        </TrailerTemplate>
                    </CSGallery:GalleryPostData>          
                    <CSGallery:GalleryPostRating runat="server" RatingCssClass="CommonRateControl" RatingReadOnlyCssClass="CommonRateControlReadOnly" RatingActiveCssClass="CommonRateControlActive" ImagesBaseUrl="~/Themes/default/images/common/">
                        <LeaderTemplate>
                            <CSGallery:GalleryPostData Property="TotalRatings" runat="server">
                                <LeaderTemplate>
                                     <li><CSControl:ResourceControl ResourceName="Gallery_RatingListing_Ratings" runat="server" />: <span class="boldtext">
                                </LeaderTemplate>
                                <TrailerTemplate>
                                    </span></li>
                                </TrailerTemplate>
                            </CSGallery:GalleryPostData> 
			                <li>
			                    <CSControl:ResourceControl ResourceName="Gallery_RatingListing_Rating" runat="server" />
                        </LeaderTemplate>
                        <TrailerTemplate>
                            </li>
                        </TrailerTemplate>
                    </CSGallery:GalleryPostRating>
	            </ul>
            </div>
        </ContentTemplate>
    </CSGallery:GalleryPostData>

    <div class="CommonSidebarArea">
        <div class="CommonSidebarTopRound"><div class="t1"></div><div class="t2"></div><div class="t3"></div><div class="t4"></div></div>
        <h4 class="CommonSidebarHeader"><CSControl:ResourceControl runat="server" ResourceName="Gallery_ThisGallery" /></h4>
        <div class="CommonSidebarContent">
        <ul class="CommonSidebarList">
            <CSGallery:GalleryData LinkTo="About" ResourceName="Gallery_Links_About" Tag="Li" runat="server" />
            <CSGallery:GalleryData LinkTo="EmailSubscriptions" ResourceName="Gallery_Links_Email" Tag="Li" runat="server" />
            <CSGallery:GalleryData LinkTo="RssPictures" ResourceName="Gallery_Links_Rss" Tag="Li" runat="server" /> 
            <CSGallery:GalleryPostSubscriptionToggleButton Tag="Li" runat="server">
                <OffTemplate><CSControl:ResourceControl runat="server" ResourceName="Gallery_Track" /></OffTemplate>
                <OnTemplate><CSControl:ResourceControl runat="server" ResourceName="Gallery_UnTrack" /></OnTemplate>
                <ProcessingTemplate><CSControl:ResourceControl runat="server" ResourceName="Gallery_TrackWorking" /></ProcessingTemplate>
            </CSGallery:GalleryPostSubscriptionToggleButton>
        </ul>
        </div>
    </div>

    <CSControl:PlaceHolder runat="server" Tag="Div" CssClass="CommonSidebarArea">
        <DisplayConditions><CSGallery:GalleryPropertyValueComparison ComparisonProperty="CategorizationType" ComparisonValue="Album" Operator="EqualTo" runat="server" /></DisplayConditions>
        <ContentTemplate>
            <div class="CommonSidebarTopRound"><div class="t1"></div><div class="t2"></div><div class="t3"></div><div class="t4"></div></div>
            <CSControl:ResourceControl runat="server" ResourceName="Gallery_CategoriesList" Tag="H4" CssClass="CommonSidebarHeader" />
            <div class="CommonSidebarContent">
                <CSGallery:PostCategoryList runat="server" ShowHeaderFooterOnNone="false">
                    <QueryOverrides SortBy="Name" SortOrder="Ascending" PageSize="999" QueryType="ForSection" ParentCategoryID="-1" />
                    <HeaderTemplate><ul class="CommonTreeList"></HeaderTemplate>
                    <ItemTemplate>
                        <li class="<%# IsSelected((int) Eval("CategoryID")) ? "CommonTreeListItemSelected" : "CommonTreeListItem" %>">
                            <CSGallery:PostCategoryData runat="server" LinkTo="ViewCategory"><ContentTemplate><%# Eval("Name") %> <em><%# Eval("TotalThreads") %></em></ContentTemplate></CSGallery:PostCategoryData>
                            <CSGallery:PostCategoryList runat="server" ShowHeaderFooterOnNone="false">
                                <QueryOverrides SortBy="Name" SortOrder="Ascending" PageSize="999" QueryType="ForSection" />
                                <HeaderTemplate><ul class="CommonTreeList"></HeaderTemplate>
                                <ItemTemplate>
                                    <li class="<%# IsSelected((int) Eval("CategoryID")) ? "CommonTreeListItemSelected" : "CommonTreeListItem" %>">
                                        <CSGallery:PostCategoryData runat="server" LinkTo="ViewCategory"><ContentTemplate><%# Eval("Name") %> <em><%# Eval("TotalThreads") %></em></ContentTemplate></CSGallery:PostCategoryData>
                                        <CSGallery:PostCategoryList runat="server" ShowHeaderFooterOnNone="false">
                                            <QueryOverrides SortBy="Name" SortOrder="Ascending" PageSize="999" QueryType="ForSection" />
                                            <HeaderTemplate><ul class="CommonTreeList"></HeaderTemplate>
                                            <ItemTemplate>
                                                <li class="<%# IsSelected((int) Eval("CategoryID")) ? "CommonTreeListItemSelected" : "CommonTreeListItem" %>">
                                                    <CSGallery:PostCategoryData runat="server" LinkTo="ViewCategory"><ContentTemplate><%# Eval("Name") %> <em><%# Eval("TotalThreads") %></em></ContentTemplate></CSGallery:PostCategoryData>
                                                </li>
                                            </ItemTemplate>
                                            <NoneTemplate></NoneTemplate>
                                            <FooterTemplate></ul></FooterTemplate>
                                        </CSGallery:PostCategoryList>
                                    </li>
                                </ItemTemplate>
                                <NoneTemplate></NoneTemplate>
                                <FooterTemplate></ul></FooterTemplate>
                            </CSGallery:PostCategoryList>          
                        </li>
                    </ItemTemplate>
                    <NoneTemplate></NoneTemplate>
                    <FooterTemplate></ul></FooterTemplate>
                </CSGallery:PostCategoryList>
            </div>
        </ContentTemplate>
    </CSControl:PlaceHolder>
	
	<CSGallery:TagCloud runat="server" Tag="Div" CssClass="CommonSidebarArea" TagCssClasses="CommonTag6,CommonTag5,CommonTag4,CommonTag3,CommonTag2,CommonTag1" MaximumNumberOfTags="25" ShowTagCounts="false" IgnoreFilterTags="true" TagCloudCssClass="CommonSidebarTagCloud">
	    <DisplayConditions><CSGallery:GalleryPropertyValueComparison ComparisonProperty="CategorizationType" ComparisonValue="Tag" Operator="EqualTo" runat="server" /></DisplayConditions>
        <LeaderTemplate>
            <div class="CommonSidebarTopRound"><div class="t1"></div><div class="t2"></div><div class="t3"></div><div class="t4"></div></div>
            <CSControl:ResourceControl runat="server" ResourceName="PopularTags" Tag="H4" CssClass="CommonSidebarHeader" />
            <div class="CommonSidebarContent">
        </LeaderTemplate>
        <TrailerTemplate>
            </div>
            <CSControl:SiteUrl UrlName="tags_home" ResourceName="ViewAllTags" runat="server" Tag="Div" CssClass="CommonSidebarFooter" />
        </TrailerTemplate>
    </CSGallery:TagCloud>
    
    <CSControl:PlaceHolder runat="server" Tag="Div" CssClass="CommonSidebarArea">
        <DisplayConditions><CSGallery:GalleryPermissionCondition Permission="Post" runat="server" /></DisplayConditions>
        <ContentTemplate>
            <div class="CommonSidebarTopRound"><div class="t1"></div><div class="t2"></div><div class="t3"></div><div class="t4"></div></div>
		    <h4 class="CommonSidebarHeader"><CSControl:ResourceControl runat="server" ResourceName="Weblog_Quick_Content" /></h4>
		    <div class="CommonSidebarContent">
		        <ul class="CommonSidebarList">
                    <CSGallery:GalleryData runat="server" LinkTo="AddGalleryPost" ResourceName="GalleryNav_AddPicture" Tag="Li" />
                    <CSGallery:GalleryPostData runat="server" LinkTo="Edit" ResourceName="GalleryNav_EditPicture" Tag="Li" />
                    <CSGallery:GalleryData runat="server" LinkTo="ManagePostCategories" ResourceName="GalleryNav_ManageAlbums" Tag="Li" />
                    <CSGallery:GalleryData runat="server" LinkTo="ControlPanel" ResourceName="GalleryNav_ControlPanel" Tag="Li" />
                </ul>
		    </div>
		</ContentTemplate>
	</CSControl:PlaceHolder>
	
</div>

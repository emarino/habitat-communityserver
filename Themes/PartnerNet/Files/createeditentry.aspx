<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Files.Controls.CSFileThemePage" MasterPageFile="files.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        if (CurrentEntry != null)
            SetTitle(CurrentEntry.Subject, Title, true);
        else
            SetTitle(ResourceManager.GetString("Files_UploadFile", "FileGallery.xml"), Title, true);
    }

</script>

<asp:Content ContentPlaceHolderID="lcr" runat="server">
</asp:Content>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
    <div class="CommonContentArea">
        <CSControl:WrappedLiteral runat="server" id="Title" Tag="H2" CssClass="CommonTitle" />
	    <div class="CommonContent">
	        <CSFile:CreateEditEntryForm runat="server" 
	            NoFileErrorMessageControlId="NoFileError"
	            AddUpdateAttachmentModalId="UploadFile" 
	            EntryDescriptionTextBoxId="EntryDescription" 
	            EntrySubjectTextBoxId="EntryName" 
	            FilenameLabelId="FileName" 
	            PreviewEntryListId="" 
	            SubFormIds="TagsSubForm" 
	            SubmitButtonId="EntrySave">
	            <ModeratedSuccessActions>
	                <CSControl:GoToSiteUrlAction UrlName="files_ViewFolder" Parameter1='<%# CurrentSection.ApplicationKey %>' runat="server" />
                </ModeratedSuccessActions>
                <UnmoderatedSuccessActions>
                    <CSControl:GoToCurrentPostAction runat="server" />
                </UnmoderatedSuccessActions>
	            <FormTemplate>
	            
	                <div class="CommonFormArea">
			            <CSControl:ResourceControl runat="server" ResourceName="EntryAdmin_EntrySettings" ResourceFile="FileGallery.xml" Tag="H4" CssClass="CommonFormTitle" />

			            <div class="CommonFormFieldName">
				            <CSControl:ResourceControl runat="server" ResourceName="EntryAdmin_FileOrUrl" ResourceFile="FileGallery.xml" />
			            </div>
			            <div class="CommonFormField">
				            <asp:Label id="FileName" runat="server" />
				            <CSControl:Modal ModalType="Link" ID="UploadFile" Runat="server" CssClass="CommonTextButton" Width="500" Height="300" ResourceName="EntryAdmin_FileOrUrl_Upload" ResourceFile="FileGallery.xml" />
				            <asp:Label runat="server" ID="NoFileError" ForeColor="Red">*</asp:Label>
			            </div>

			            <div class="CommonFormFieldName">
				            <CSControl:FormLabel LabelForId="EntryName" runat="server" ResourceName="EntryAdmin_Name" ResourceFile="FileGallery.xml" />
				            <CSControl:ResourceControl runat="server" ResourceName="EntryAdmin_Name_Detail" ResourceFile="FileGallery.xml" Tag="Div" CssClass="CommonFormFieldDescription" />
			            </div>
			            <div class="CommonFormField">
				            <asp:TextBox id="EntryName" Columns="70" MaxLength="256" runat="server" />
				            <asp:requiredfieldvalidator runat="server" Cssclass="CommonValidationWarning" ControlToValidate="EntryName">*</asp:requiredfieldvalidator>
			            </div>

			            <div class="CommonFormFieldName">
				            <CSControl:FormLabel LabelForId="EntryDescription" runat="server" ResourceName="EntryAdmin_Desc" ResourceFile="FileGallery.xml" />
				            <CSControl:ResourceControl runat="server" ResourceName="EntryAdmin_Desc_Detail" ResourceFile="FileGallery.xml" Tag="Div" CssClass="CommonFormFieldDescription" />
			            </div>
			            <div class="CommonFormField">
				            <CSControl:Editor runat="Server" id="EntryDescription" width="100%" columns="110" />
			            </div>

                        <CSFile:EntryTagsSubForm runat="server" ID="TagsSubForm" SelectTagsModalButtonId="SelectTags" TagsTextBoxId="Tags">
    					    <FormTemplate>
    					        <div class="CommonFormFieldName">
				                    <CSControl:FormLabel LabelForId="Tags" runat="server" ResourceName="EntryAdmin_Categories" ResourceFile="FileGallery.xml" />
				                    <CSControl:ResourceControl runat="server" ResourceName="EntryAdmin_Categories_Detail" ResourceFile="FileGallery.xml" Tag="Div" CssClass="CommonFormFieldDescription" />
			                    </div>
    					        <div class="CommonFormField">
    					            <asp:TextBox runat="server" ID="Tags" Columns="70" /> <CSControl:Modal ModalType="Button" Width="400" Height="350" ID="SelectTags" ResourceName="TagEditor_SelectTags" runat="server" />
    					        </div>
    					    </FormTemplate>
    					</CSFile:EntryTagsSubForm>

			            <p />
			            <div class="CommonFormField">
				            <CSControl:ResourceLinkButton id="EntrySave" runat="server" ResourceName="Save" CssClass="CommonTextButton" />
			            </div>
		            </div>	            
	            </FormTemplate>
	        </CSFile:CreateEditEntryForm>
	    </div>
    </div>
</asp:Content>
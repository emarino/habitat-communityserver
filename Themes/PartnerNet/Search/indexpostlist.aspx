<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Controls.CSThemePage" MasterPageFile="../Common/master.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        SetTitle(ResourceManager.GetString("Search"), Title, true);

        if (!string.IsNullOrEmpty(CurrentCSContext.QueryString["q"]))
            Description.Text = string.Format(ResourceManager.GetString("SearchViewSimple_SearchFor"), Server.HtmlEncode(CurrentCSContext.QueryString["q"]));
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">

<div class="CommonContentArea">
	<CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdTop runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
	
    <CSControl:WrappedLiteral runat="server" Tag="H2" CssClass="CommonTitle" ID="Title" />
	<div class="CommonContent">
	    <CSControl:WrappedLiteral runat="server" Tag="Div" CssClass="CommonDescription" ID="Description" />
	
        <CSControl:SearchForm runat="server" QueryTextBoxId="SearchTextTop" SubmitButtonId="SearchButtonTop" Tag="Div" CssClass="CommonFormArea">
            <FormTemplate>
		        <div id="SearchTexbox" class="CommonFormField">
			        <CSControl:DefaultButtonTextBox ButtonId="SearchButtonTop" id="SearchTextTop" runat="server"  columns="55" />
			        <CSControl:ResourceButton id="SearchButtonTop" ResourceName="Search" runat="server" /> 
			        <CSControl:SiteUrl UrlName="search_Advanced" ResourceName="Search_MoreSearchOptions" runat="server" />
			        
			        <CSControl:IndexPostRssLink runat="server" Tag="Div">
			            <ContentTemplate><CSControl:Image ImageUrl="~/utility/images/rss.gif" BorderWidth="0" runat="server" /></ContentTemplate>
			        </CSControl:IndexPostRssLink>
		        </div>
		    </FormTemplate>
		</CSControl:SearchForm>
		
        <CSControl:Pager id="PagerTop" runat="server" Tag="Div" CssClass="CommonPagingArea" ShowTotalSummary="true" />
	
        <CSControl:IndexPostList runat="server" ShowHeaderFooterOnNone="false">
            <QueryOverrides PagerID="PagerGroup" />
	        <HeaderTemplate>
		        <ul class="CommonSearchResultList">
	        </HeaderTemplate>
	        <ItemTemplate>
		        <li>
		        <div class="CommonSearchResultArea">
		            <CSControl:IndexPostData runat="server" Property="Title" LinkTo="Post" Tag="h4" CssClass="CommonSearchResultName" />
			        <div class="CommonSearchResult">
				        <%# Eval("BestMatch").ToString().Length > 0 ? Eval("BestMatch") : Formatter.GetBodySummary(Eval("Body").ToString(),350, ((TextBox) CSControlUtility.Instance().FindControl(this,"SearchTextTop")).Text, System.Drawing.Color.Black, System.Drawing.Color.Yellow) %>
			        </div>
			        <div class="CommonSearchResultDetails">
				        <CSControl:ResourceControl runat="server" ResourceName="SearchResults_PostTo" />
				        <CSControl:IndexPostData Property="SectionName" LinkTo="Section" runat="server" />
				        <CSControl:IndexPostData Property="ApplicationType" LinkTo="Application" Text="({0})" runat="server" />
				        <CSControl:ResourceControl runat="server" ResourceName="SearchResults_By" />
				        <CSControl:IndexPostData Property="UserName" LinkTo="Author" runat="server" />
				        <CSControl:ResourceControl runat="server" ResourceName="SearchResults_On" />
				        <CSControl:IndexPostData Property="PostDate" runat="server" />
			            <%# (bool)Eval("HasPostCategories") ? String.Concat("<br>", ResourceManager.GetString("TagListTitle"), String.Join(", ", (string[])Eval("PostCategories"))) : ""%>
			        </div>
		        </div>
		        </li>
	        </ItemTemplate>
	        <FooterTemplate>
		        </ul>
	        </FooterTemplate>
	        <NoneTemplate>
	            <CSControl:ResourceControl runat="server" ResourceName="SearchView_NoResults" Tag="Div" CssClass="CommonMessageError" />
	        </NoneTemplate>
        </CSControl:IndexPostList>

        <CSControl:SearchForm runat="server" QueryTextBoxId="SearchTextBottom" SubmitButtonId="SearchButtonBottom" Tag="Div" CssClass="CommonFormArea">
            <DisplayConditions><CSControl:QueryStringPropertyValueComparison QueryStringProperty="q" Operator="IsSetOrTrue" runat="server" /></DisplayConditions>
            <FormTemplate>
                <div id="SearchTexbox" class="CommonFormField">
	                <CSControl:DefaultButtonTextBox ButtonId="SearchButtonBottom" id="SearchTextBottom" runat="server" Columns="55" />
	                <CSControl:ResourceButton id="SearchButtonBottom" ResourceName="Search" runat="server" /> 
	                <CSControl:SiteUrl UrlName="search_Advanced" ResourceName="Search_MoreSearchOptions" runat="server" />
                </div>
            </FormTemplate>
        </CSControl:SearchForm>
			   
        <CSControl:PagerGroup ID="PagerGroup" runat="server" PagerIds="PagerTop,PagerBottom" /> 
        <CSControl:Pager id="PagerBottom" runat="server" Tag="Div" CssClass="CommonPagingArea" ShowTotalSummary="true" />
	</div>
	
	<CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdBottom runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
</div>

</asp:Content>


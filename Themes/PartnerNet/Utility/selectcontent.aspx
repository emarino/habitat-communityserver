<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Controls.CSThemePage" MasterPageFile="../Common/modal.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        SetTitle(ResourceManager.GetString("ContentSelector_Title"), false);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">

<script type="text/javascript">
// <![CDATA[

function ProcessContent(content)
{
	if (window.parent.ContentSelectorCallbackFunction)
		window.parent.ContentSelectorCallbackFunction(content);

	window.parent.Telligent_Modal.Close();		
}

// ]]>
</script>

<div class="CommonContentArea">
<div class="CommonContent">
<CSControl:SelectContentForm runat="server" 
    TabSetId="TabSet"
    ItemsAreaHtmlGenericControlId="ItemsArea" 
    NavigationTreeId="Tree" 
    OptionsAreaHtmlGenericControlId="OptionsArea" 
    SelectButtonId="SelectButton"
    ItemCssClass="CommonContentSelectorItem" 
	SelectedItemCssClass="CommonContentSelectorItemSelected" 
	ContentSelectedClientFunction="ProcessContent"
	ItemNameCssClass="CommonContentSelectorItemName"
	SelectedItemNameCssClass="CommonContentSelectorItemNameSelected"
	ItemAreaCssClass="CommonContentSelectorItemArea"
	ErrorMessageTextId="ErrorMessage"
	SuccessMessageTextId="SuccessMessage"
	>
    <FormTemplate>
        <CSControl:WrappedLiteral Tag="Div" CssClass="CommonMessageSuccess" ID="SuccessMessage" runat="server" />
        <CSControl:WrappedLiteral Tag="Div" CssClass="CommonMessageError" ID="ErrorMessage" runat="server" />
    
        <TWC:TabSet runat="server" id="TabSet"
			CssClass="CommonPaneTabSet"
			TabCssClasses="CommonPaneTab,CommonPaneTab1,CommonPaneTab2"
			TabSelectedCssClasses="CommonPaneTabSelected,CommonPaneTabSelected1,CommonPaneTabSelected2"
			TabHoverCssClasses="CommonPaneTabHover,CommonPaneTabHover1,CommonPaneTabHover2">
		</TWC:TabSet>
		<div class="CommonPane">
		    <table cellpadding="0" cellspacing="0" border="0" width="100%">
		        <tr valign="top"><td width="33%"><div class="CommonContentSelectorTreeArea">
			        <CA:TreeView id="Tree" ExpandNodeOnSelect="false"
			        
				        DragAndDropEnabled="false" 
				        NodeEditingEnabled="false"
				        KeyboardEnabled="true"
				        CssClass="CommonContentSelectorTree" 
				        NodeCssClass="CommonContentSelectorTreeNode" 
				        SelectedNodeCssClass="CommonContentSelectorTreeNodeSelected" 
				        HoverNodeCssClass="CommonContentSelectorTreeNodeHover"
				        LineImageWidth="19" 
				        LineImageHeight="20"
				        DefaultImageWidth="16" 
				        DefaultImageHeight="16"
				        ItemSpacing="0" 
				        ImagesBaseUrl="~/themes/default/images/file/"
				        NodeLabelPadding="3"
				        ParentNodeImageUrl="folder.gif" 
				        ExpandedParentNodeImageUrl="folder_open.gif" 
				        LeafNodeImageUrl="folder_open.gif" 
				        ShowLines="true" 
				        LineImagesFolderUrl="~/themes/default/images/file/lines/"
				        EnableViewState="true"
				        Width="100%"
				        Height="100%"
				        runat="server"  />
		        </div></td>
		        <td width="60%">
		            <div class="CommonContentSelectorItemsArea" id="itemsArea"><div runat="server" ID="ItemsArea"></div></div>
		            <div class="CommonContentSelectorOptionsArea" id="optionsArea"><div runat="server" id="OptionsArea"></div></div>
		        </td></tr></table>        
        </div>
        <div class="CommonContentSelectorButtonArea">
	        <CSControl:ResourceLinkButton ID="SelectButton" Runat="server" ResourceName="OK" CssClass="CommonTextButton" />
	        <CSControl:ResourceLinkButton Runat="server" ResourceName="Cancel" OnClientClick="window.parent.Telligent_Modal.Close(); return false;" CssClass="CommonTextButton" />
        </div>
    </FormTemplate>
</CSControl:SelectContentForm>
</div>
</div>

<script type="text/javascript">
// <![CDATA[

function resizeContent()
{
    try
    {
        var optionsArea = document.getElementById('optionsArea');
        var itemsArea = document.getElementById('itemsArea');

        if (optionsArea.childNodes[0].innerHTML == '')
        {
            optionsArea.style.display = 'none';
            itemsArea.style.height = '322px';
        }
        else
        {
            optionsArea.style.display = 'block';
            itemsArea.style.height = (316 - optionsArea.offsetHeight) + 'px';
        }
    }
    catch (e) {}
}

setInterval(resizeContent, 249);

// ]]>
</script>
        
</asp:Content>
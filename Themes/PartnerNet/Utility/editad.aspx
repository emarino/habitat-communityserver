<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Controls.CSThemePage" MasterPageFile="../Common/modal.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Discussions.Components" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        if (CurrentContent != null)
            SetTitle(string.Concat(ResourceManager.GetString("Editing"), " ", CurrentContent.Name), false);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
<div class="CommonContentArea">
	<div class="CommonContent">
	    <CSControl:CreateEditContentForm runat="server" 
	        BodyEditorId="content" 
	        TitleTextBoxId="" 
	        NameTextBoxId="" 
	        SaveButtonId="SaveButton" 
	        DeleteButtonId="DeleteButton"
	        >
	        <SaveSuccessActions>
	            <CSControl:ExecuteScriptAction runat="server" Script="window.parent.Telligent_Modal.Close(true);" />
	        </SaveSuccessActions>
	        <DeleteSuccessActions>
                <CSControl:ExecuteScriptAction runat="server" Script="window.parent.Telligent_Modal.Close(true);" />	        
	        </DeleteSuccessActions>
	        <FormTemplate>
		        <div class="CommonFormArea">
		        <table width="100%" cellpadding="0" cellspacing="0" border="0">
			        <tr>
				        <td colspan="2" class="CommonFormField">
					        <CSControl:DefaultTextEditor runat="server" id="content" Width="610" Height="400" />
				        </td>
			        </tr>
			        <tr>
				        <td colspan="2" class="CommonFormField">
					        <CSControl:ResourceButton id="SaveButton" Runat="server" ResourceName="Content_Save" />
					        <CSControl:ResourceButton id="DeleteButton" Runat="server" ResourceName="Content_Delete" />
				        </td>
			        </tr>
		        </table>
		        </div>
            </FormTemplate>
        </CSControl:CreateEditContentForm>
    </div>
</div>
</asp:Content>

<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Blogs.Controls.CSBlogThemePage" MasterPageFile="blogs.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        if (CurrentGroup != null)
            SetTitle(CurrentGroup.Name, true);
        else
            SetTitle(ResourceManager.GetString("weblogs"), true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
<div class="CommonContentArea">
	<CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdTop runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
	<div class="CommonContent">
		<CSControl:SearchForm runat="server" QueryTextBoxId="SearchBox" SubmitButtonId="SearchButton" Tag="Div" CssClass="CommonSearchArea">
		    <QueryOverrides ApplicationTypes="weblog" />
		    <FormTemplate>
                <table cellpadding="0" cellspacing="0" border="0">
                    <tr valign="middle">
                        <td><CSControl:DefaultButtonTextBox id="SearchBox" runat="server" CssClass="CommonInputSearch" Width="400px" ButtonId="SearchButton" /></td>
                        <td><span class="CommonSearchButtonOuter"><CSControl:ResourceLinkButton id="SearchButton" runat="server" CssClass="CommonSearchButton" ResourceName="Search" /></span></td>
                    </tr>
                </table>
		    </FormTemplate>
		</CSControl:SearchForm>
		
		<p />
		
		<CSBlog:GroupData runat="server" Property="Name" Tag="H4" CssClass="CommonSubTitle" />
		<CSControl:ResourceControl ResourceName="LatestPosts" Tag="H4" CssClass="CommonSubTitle" runat="server">
		    <DisplayConditions Operator="Not"><CSControl:QueryStringPropertyValueComparison QueryStringProperty="GroupID" Operator="IsSetOrTrue" runat="server" /></DisplayConditions>
		</CSControl:ResourceControl>
	
		<CSBlog:WeblogPostList runat="Server" >
		    <QueryOverrides PagerID="Pager" IsAggregate="true" />
	        <HeaderTemplate>
		        <ul class="BlogPostList">
	        </HeaderTemplate>
	        <ItemTemplate>
		        <li class="BlogPostArea">
			        <table cellpadding="0" cellspacing="0" border="0">
				        <tr>
					        <td valign="top" id="CommentCountBox" runat="server">
					            <CSControl:PlaceHolder runat="server">
					                <DisplayConditions Operator="Not"><CSBlog:WeblogPostPropertyValueComparison runat="server" ComparisonProperty="IsExternal" Operator="IsSetOrTrue" /></DisplayConditions>
					                <ContentTemplate>
                                        <div class="BlogRating">
						                    <CSBlog:WeblogPostData Property="Replies" LinkTo="PostComments" runat="server" CssClass="BlogCommentCount" Tag="Div" />
							                <CSBlog:WeblogPostData ResourceName="Weblog_ContentList_Comments" LinkTo="PostComments" runat="server" Tag="Div" />
							                <CSBlog:WeblogPostData ResourceName="Weblog_EntryList_Views" Property="Views" LinkTo="Post" runat="server" Tag="Div" />
							                <CSBlog:WeblogPostRating id="Rating" runat="server" RatingCssClass="CommonRateControl" RatingReadOnlyCssClass="CommonRateControlReadOnly" RatingActiveCssClass="CommonRateControlActive" ImagesBaseUrl="~/Themes/default/images/common/" />
							                <br />
						                </div>					                
					                </ContentTemplate>
					            </CSControl:PlaceHolder>
					            <CSControl:PlaceHolder runat="server">
					                <DisplayConditions><CSBlog:WeblogPostPropertyValueComparison runat="server" ComparisonProperty="IsExternal" Operator="IsSetOrTrue" /></DisplayConditions>
					                <ContentTemplate>
                                        <div class="BlogExternal">
						                    <CSBlog:WeblogPostData ResourceName="Weblog_ExternalBlogPost" LinkTo="Post" runat="server" />
						                </div>					                
					                </ContentTemplate>
					            </CSControl:PlaceHolder>
					        </td>
					        <td width="100%">
					            <CSBlog:WeblogPostData Property="Subject" LinkTo="Post" Tag="H4" CssClass="BlogPostHeader" runat="server" />
					            <CSBlog:WeblogPostData Property="Excerpt" Tag="Div" CssClass="BlogPostContent" runat="server" />
						        <div class="BlogPostFooter">
							        <div>
							            <CSBlog:WeblogPostData Property="UserTime" LinkTo="Post" IncludeTimeInDate="true" runat="server" />
							            <CSControl:ResourceControl runat="server" ResourceName="Weblog_Aggregate_By" /> 
							            <CSBlog:WeblogPostData Property="DisplayName" LinkTo="AuthorUrl" runat="server" /> 
							            <CSControl:ResourceControl runat="server" ResourceName="To" /> 
							            <CSBlog:WeblogData Property="Name" LinkTo="HomePage" runat="server" />
							        </div>
						            <CSBlog:WeblogPostTagEditableList runat="server" EditorLinkCssClass="CommonTextButton" EditorCssClass="CommonInlineTagEditor" Tag="Div" />
						        </div>
					        </td>
				        </tr>
			        </table>
		        </li>
	        </ItemTemplate>
	        <FooterTemplate>
		        </ul>
	        </FooterTemplate>
        </CSBlog:WeblogPostList>

        <CSControl:SinglePager runat="Server" id="Pager"  />
	</div>
	<CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdBottom runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
</div>
</asp:Content>
<%@ Page Language="C#" AutoEventWireup="true" EnableViewState="False" MasterPageFile="theme.Master" Inherits="CommunityServer.Blogs.Controls.CSBlogThemePage" %>
<%@ Import namespace="CommunityServer.Components"%>
<%@ Import namespace="CommunityServer.Blogs.Components"%>
<script runat="Server">

    void Page_Load(object sender, EventArgs e)
    {
        SetTitle("", false);   
    }

</script>
<asp:Content ID="Content1" ContentPlaceHolderID="Main" runat="Server">
    <CSControl:LinkCategoryList ID="LinkCategoryList1" runat="Server">
        <HeaderTemplate></HeaderTemplate>
        <ItemTemplate>
            <CSControl:LinkCategoryData runat="server" Property="Name" Tag="H5" CssClass="posthead" />
            <CSControl:LinkList runat="server">
                <HeaderTemplate><ul></HeaderTemplate>
                <ItemTemplate>
                    <li>
                        <div><CSControl:LinkData runat="server" Property="Title" LinkTo="Link" /></div>
                        <div><CSControl:LinkData runat="server" Property="Description" /></div>
                    </li>
                </ItemTemplate>
                <FooterTemplate></ul></FooterTemplate>
            </CSControl:LinkList>
        </ItemTemplate>
        <FooterTemplate></FooterTemplate>
    </CSControl:LinkCategoryList>
</asp:Content>
<%@ Page Language="C#" AutoEventWireup="true" EnableViewState="False" MasterPageFile="theme.Master" Inherits="CommunityServer.Blogs.Controls.CSBlogThemePage" %>
<%@ Import namespace="CommunityServer.Components"%>
<%@ Import namespace="CommunityServer.Blogs.Components"%>
<script runat="Server">
    void Page_Load(object sender, EventArgs e)
    {
        SetTitle("", false);
    }
</script>
<asp:Content ContentPlaceHolderID="bcr" runat="Server">
    <CSControl:WrappedLiteral Tag="h2" ContainerId="titleWrapper" CssClass="pageTitle" runat="server" Text="Search Results" />
    <CSControl:IndexPostList id="EntryItems" Runat="server">
        <QueryOverrides PagerID="pager" />
        <HeaderTemplate>
		    <ul class="BlogPostList">
		</HeaderTemplate>
        <ItemTemplate>
            <li class="BlogPostArea CommonListRow">
				<h4 class="BlogPostHeader"><CSControl:IndexPostData runat="server" Property="Title" LinkTo="Post" /></h4>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td width="100%">
							<div class="BlogPostContent">
                                <CSControl:IndexPostData Property="FormattedBody" TruncateAt="300" runat="server" />
                            </div>
							<div class="BlogPostFooter">	
                                <span class="em">Posted:</span>
                                <CSControl:IndexPostData Property="PostDate" LinkTo="Post" FormatString="MMM dd yyyy, hh:mm tt" runat="server" />
					            <CSControl:ResourceControl runat="server" ResourceName="Weblog_EntryList_By" id="PostByResource" />
					            <CSControl:IndexPostData Property="UserName" runat="server" /> 
                            </div>   
                        </td>
                    </tr>
                 </table>                         
            </li>
        </ItemTemplate>
        <FooterTemplate>
		    </ul>
		</FooterTemplate>
    </CSControl:IndexPostList>
    <CSControl:SinglePager id = "pager" runat = "Server"  />
</asp:Content>

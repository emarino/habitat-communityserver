<%@ Page Language="C#" AutoEventWireup="true" EnableViewState="False" MasterPageFile="theme.Master" Inherits="CommunityServer.Blogs.Controls.CSBlogThemePage" %>
<%@ Import namespace="CommunityServer.Components"%>
<%@ Import namespace="CommunityServer.Blogs.Components"%>
<script runat="Server">

    void Page_Load(object sender, EventArgs e)
    {
        this.SetTitle(ResourceManager.GetString("MyWeblogs_ContactMe"), false);
    }

</script>
<asp:Content ContentPlaceHolderID="bcr" runat="Server">

    <CSBlog:ContactForm runat="server"
        SubjectTextBoxId="tbSubject" 
        NameTextBoxId="tbName" 
        EmailTextBoxId="tbEmail" 
        MessageTextBoxId="tbBody" 
        SubmitButtonId="btnSubmit" 
        ControlIdsToHideFromRegisteredUsers="NameTitle,NameDesc"
    >
        <SuccessActions>
            <CSControl:SetVisibilityAction runat="server" ControlIdsToShow="Msg" />
        </SuccessActions>
        <FormTemplate>
            <h2 class="CommonSubTitle"><CSControl:ResourceControl runat="server" ResourceName="Weblog_ContactForm" /></h2>
	
            <div id="CommonContactForm" >
                <dl>
                    <dt id="NameTitle" runat="server"><CSControl:FormLabel LabelForId="tbName" runat="server" ResourceName="Weblog_ContactForm_Name" /><asp:RequiredFieldValidator runat="server" ErrorMessage="*" ControlToValidate="tbName" /></dt>
                    <dd id="NameDesc" runat="server"><asp:TextBox id="tbName" runat="server" CssClass="smallbox" /></dd>
                    
                    <dt><CSControl:FormLabel LabelForId="tbEmail" runat="server" ResourceName="Weblog_ContactForm_Email" /><asp:RequiredFieldValidator runat="server" ErrorMessage="*" ControlToValidate="tbEmail" ID="Requiredfieldvalidator2"/><asp:RegularExpressionValidator runat="server" ErrorMessage="Invalid email address format" ControlToValidate="tbEmail" Display="Dynamic" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$">*</asp:RegularExpressionValidator></dt>
                    <dd><asp:TextBox id="tbEmail" runat="server" CssClass="smallbox" /></dd>
                    
                    <dt><CSControl:FormLabel LabelForId="tbSubject" runat="server" ResourceName="Weblog_ContactForm_Subject" /><asp:RequiredFieldValidator runat="server" ErrorMessage="*" ControlToValidate="tbSubject" /></dt>
                    <dd><asp:TextBox id="tbSubject" runat="server" CssClass="smallbox" /></dd>        
                    
                    <dt><CSControl:FormLabel LabelForId="tbBody" runat="server" ResourceName="Weblog_ContactForm_Body" /><asp:RequiredFieldValidator runat="server" ErrorMessage="*" ControlToValidate="tbBody" /></dt>
                    <dd><asp:TextBox id="tbBody" runat="server" TextMode="MultiLine" /></dd>

                    <dt><asp:LinkButton CssClass="CommonTextButton" id="btnSubmit" runat="server" Text="Send"></asp:LinkButton>  <CSControl:ResourceControl runat="Server" id="Msg" ResourceName="Weblog_ContactForm_Sent" Visible="false" /></dt>
                </dl>
            </div>
        </FormTemplate>
    </CSBlog:ContactForm>
    
</asp:Content>

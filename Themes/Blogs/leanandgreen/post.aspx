<%@ Page Language="C#" AutoEventWireup="true" EnableViewState="False" MasterPageFile="theme.Master" Inherits="CommunityServer.Blogs.Controls.CSBlogThemePage" %>
<%@ Import namespace="CommunityServer.Components"%>
<%@ Import namespace="CommunityServer.Blogs.Components"%>
<script runat="Server">

    void Page_Load(object sender, EventArgs e)
    {
        if (CurrentWeblogPost != null)
            SetTitle(CurrentWeblogPost.Subject, false);
    }

</script>
<asp:Content ContentPlaceHolderID="bcr" runat="Server">
    <div class="BlogPostArea">
	    <h4 class="BlogPostHeader"><CSBlog:WeblogPostData runat="server" Property="Subject" /> <CSBlog:WeblogPostRating runat="server" RatingCssClass="CommonRateControl" RatingReadOnlyCssClass="CommonRateControlReadOnly" RatingActiveCssClass="CommonRateControlActive" ImagesBaseUrl="~/Themes/default/images/common/" /></h4>
	    <div class="BlogPostContent">
		    <CSBlog:WeblogPostData Property="FormattedBody" IncrementViewCount="true" runat="server" />
		    <CSBlog:WeblogPostData ResourceName="Weblog_ReadMirroredPost" Property="TitleUrl" runat="server">
		        <DisplayConditions><CSBlog:WeblogPostPropertyValueComparison runat="server" ComparisonProperty="IsExternal" Operator="IsSetOrTrue" /></DisplayConditions>
		        <LeaderTemplate><p><i></LeaderTemplate>
		        <TrailerTemplate></i></p></TrailerTemplate>
		    </CSBlog:WeblogPostData>
	    </div>
	    <div class="BlogPostFooter">
		    <CSControl:ResourceControl ResourceName = "Feedback_FilterPublished" runat="Server" /> 
		    <CSBlog:WeblogPostData Property="UserTime" LinkTo="Post" FormatString="MMM dd yyyy, hh:mm tt" runat="server" />
			<CSControl:ResourceControl runat="server" ResourceName="Weblog_EntryList_By" />
		    <CSBlog:WeblogPostData Property="DisplayName" LinkTo="AuthorUrl" runat="server" /> 
            <CSBlog:WeblogPostData LinkTo="PostEditor" ResourceName="Weblog_Link_EditPost" runat="server"><LeaderTemplate>| </LeaderTemplate></CSBlog:WeblogPostData>
            <CSBlog:WeblogPostTagEditableList runat="server" EditorLinkCssClass="CommonTextButton" EditorCssClass="CommonInlineTagEditor" Tag="Div" CssClass="em" />
		    <CSBlog:PostAttachmentData runat="server" LinkTo="Attachment" Tag="Div" Property="FileName"><LeaderTemplate><span class="em">Attachment:</span> </LeaderTemplate></CSBlog:PostAttachmentData>	
        </div>                            
    </div>

    <CSControl:PlaceHolder runat="server">
		<DisplayConditions Operator="Not"><CSBlog:WeblogPostPropertyValueComparison runat="server" ComparisonProperty="IsExternal" Operator="IsSetOrTrue" /></DisplayConditions>
        <ContentTemplate>
            <CSBlog:WeblogFeedbackList runat="server">
                <QueryOverrides PageSize="999999" />
                <HeaderTemplate>
	                <div id="comments">
		                <CSControl:ResourceControl runat="server" ResourceName="Weblog_CommentForm_Comments" Tag="H3"/>
		                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                </HeaderTemplate>
                <ItemTemplate>
	                <tr valign="top">
	                <td class="<%# ((bool) DataBinder.Eval(Container.DataItem, "IsOwner")) ? "CommentOwner" : "Comment" %> CommentAvatar">
		                <CSControl:UserAvatar runat="server" BorderWidth="1" />&nbsp;
	                </td>
	                <td class="<%# ((bool) DataBinder.Eval(Container.DataItem, "IsOwner")) ? "CommentOwner" : "Comment" %>  CommentContent" width="100%">
		                <h4 class="CommentTitle"><CSBlog:WeblogPostFeedbackData runat="server" LinkTo="User" Property="DisplayName" />
		                <CSControl:ResourceControl runat="server" ResourceName="Weblog_CommentForm_Said" /></h4>
		                <div class="CommentText"><CSBlog:WeblogPostFeedbackData runat="server" Property="FormattedBody" /></div>
		                <div class="CommentFooter">
		                    <CSBlog:WeblogPostFeedbackData runat="server" Property="PostDate" FormatString="MMMM d, yyyy h:mm tt" />
                            <CSBlog:DeleteWeblogPostFeedbackForm DeleteButtonId="DeleteComment" ConfirmationResourceName="Weblog_Comment_DeleteVerify" runat="server">
                                <SuccessActions>
	                                <CSControl:GoToModifiedUrlAction runat="server" />
	                            </SuccessActions>
                                <FormTemplate>
                                    [<asp:LinkButton runat="server" Text="Delete" ID="DeleteComment" />]
                                </FormTemplate>
                            </CSBlog:DeleteWeblogPostFeedbackForm>
		                </div>
	                </td>
	                </tr>
                </ItemTemplate>
                <FooterTemplate>
	                </table>
	                </div>
                </FooterTemplate>
                <NoneTemplate>
	                <tr><td colspan="2" class="Comment CommentContent">
		                <CSControl:ResourceControl runat="server" resourcename="Weblog_CommentForm_NoComments" />
	                </td></tr>
                </NoneTemplate>
            </CSBlog:WeblogFeedbackList>
            
            <CSControl:ResourceControl runat="server" ResourceName="Blog_Pending_Comment" Tag="Strong">
                <DisplayConditions>
                    <CSControl:QueryStringPropertyValueComparison QueryStringProperty="CommentPosted" Operator="IsSetOrTrue" runat="server" />
                </DisplayConditions>
                <LeaderTemplate><a name="commentmessage"></a></LeaderTemplate>
            </CSControl:ResourceControl>
            
            <CSBlog:WeblogPostCommentForm runat="server"
                MessageTextBoxId="tbComment" 
                NameTextBoxId="tbName" 
                RememberCheckboxId="chkRemember" 
                SubjectTextBoxId="tbTitle" 
                SubmitButtonId="btnSubmit" 
                UrlTextBoxId="tbUrl" 
                ControlIdsToHideFromRegisteredUsers="RememberWrapper"
                
            >
                <SuccessActions>
                    <CSControl:GoToModifiedUrlAction runat="server" QueryStringModification="CommentPosted=true" TargetLocationModification="commentmessage" />
                </SuccessActions>
                <FormTemplate>
                    <div id="CommonCommentForm">
                    <h3><CSControl:ResourceControl runat="server" ResourceName="Weblog_CommentForm_WhatDoYouThink" id="rc_think" /></h3>
                    <dl>
	                    <dt id ="NameTitle" runat="server"><CSControl:FormLabel LabelForId="tbName" runat="server" ResourceName="Weblog_CommentForm_Name" /> <em>(<CSControl:ResourceControl runat="server" ResourceName="Required" />)</em><asp:RequiredFieldValidator runat="server" ErrorMessage="*" ControlToValidate="tbName" ValidationGroup="CreateCommentForm" /></dt>
	                    <dd id ="NameDesc" runat="server"><asp:TextBox id="tbName" runat="server" CssClass="smallbox" ValidationGroup="CreateCommentForm" /></dd>
	                    <dt><CSControl:FormLabel LabelForId="tbUrl" runat="server" ResourceName="Weblog_CommentForm_YourUrl" /></label> <em>(<CSControl:ResourceControl runat="server" ResourceName="Optional" />)</em></dt>
	                    <dd><asp:TextBox id="tbUrl" runat="server" CssClass="smallbox" ValidationGroup="CreateCommentForm" /></dd>
	                    <dt><CSControl:FormLabel LabelForId="tbComment" runat="server" ResourceName="Weblog_CommentForm_Comments" /></label> <em>(<CSControl:ResourceControl runat="server" ResourceName="Required" />)</em><asp:RequiredFieldValidator runat="server" ErrorMessage="*" ControlToValidate="tbComment" ValidationGroup="CreateCommentForm" /></dt>
	                    <dd><asp:TextBox id="tbComment" runat="server" Rows="5" Columns="25" TextMode="MultiLine" ValidationGroup="CreateCommentForm" /></dd>
	                    <dt><asp:CheckBox id="chkRemember" runat="server" Text="Remember Me?"></asp:CheckBox></dt>
	                    <dt><asp:LinkButton CssClass="CommonTextButton" id="btnSubmit" runat="server" Text="Submit" ValidationGroup="CreateCommentForm" /></dt>
                    </dl>
                    </div>
                </FormTemplate>
            </CSBlog:WeblogPostCommentForm>
    
            <CSControl:UserProfileData Property="Bio" runat="server">
                <LeaderTemplate>
                    <CSControl:UserData runat="server" Property="DisplayName" Text="About {0}" Tag="H2" CssClass="CommonSubTitle" />
                </LeaderTemplate>
            </CSControl:UserProfileData>
        </ContentTemplate>
    </CSControl:PlaceHolder>
</asp:Content>

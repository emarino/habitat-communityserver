<%@ Page Language="C#" AutoEventWireup="true" EnableViewState="False" MasterPageFile="theme.Master" Inherits="CommunityServer.Blogs.Controls.CSBlogThemePage" %>
<%@ Import namespace="CommunityServer.Components"%>
<%@ Import namespace="CommunityServer.Blogs.Components"%>
<script runat="Server">

    void Page_Load(object sender, EventArgs e)
    {
        if (CurrentCSContext.Tags == null || CurrentCSContext.Tags.Length == 0)
            SetTitle(ResourceManager.GetString("TagBrowser_Title"), false);
        else
            SetTitle(string.Join(",", CurrentCSContext.Tags), false);
    }

</script>
<asp:Content ContentPlaceHolderID="bcr" runat="Server">
    
    <CSControl:ResourceControl runat="server" ResourceName="TagBrowser_Title" CssClass="CommonSubTitle" Tag="h2" />
    
    <CSBlog:TagBreadCrumb runat="server" ShowHome="false" />  <CSBlog:TagRssLink runat="server"><LeaderTemplate>(</LeaderTemplate><TrailerTemplate>)</TrailerTemplate></CSBlog:TagRssLink>
    
    <CSBlog:TagCloud runat="server" TagCssClasses="CommonTag6,CommonTag5,CommonTag4,CommonTag3,CommonTag2,CommonTag1" TagCloudCssClass="CommonTagCloud" NoRelatedTagsResourceName="TagCloud_NoSubTagsDefined" NoTagsResourceName="TagCloud_NoTagsDefined" />

    <CSBlog:WeblogPostList runat="server">
        <QueryOverrides PagerID="PostPager" />
        <DisplayConditions>
            <CSControl:QueryStringPropertyValueComparison QueryStringProperty="Tags" Operator="IsSetOrTrue" runat="server" />
        </DisplayConditions>
        <HeaderTemplate>
		    <ul class="BlogPostList">
		</HeaderTemplate>
		<ItemTemplate>
			<li class="BlogPostArea CommonListRow">
				<h4 class="BlogPostHeader"><CSBlog:WeblogPostData runat="server" Property="Subject" LinkTo="Post" />  <CSBlog:WeblogPostRating ID="WeblogPostRating1" runat="server" RatingCssClass="CommonRateControl" RatingReadOnlyCssClass="CommonRateControlReadOnly" RatingActiveCssClass="CommonRateControlActive" ImagesBaseUrl="~/Themes/default/images/common/" /></h4>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td width="100%">
							<div class="BlogPostContent">
								<CSBlog:WeblogPostData Property="Excerpt" runat="server" />
							</div>
							<div class="BlogPostFooter">
								<CSControl:ResourceControl runat="server" ResourceName="Weblog_EntryList_Posted" />
								<CSBlog:WeblogPostData Property="UserTime" LinkTo="Post" FormatString="MMM dd yyyy, hh:mm tt" runat="server" />
					            <CSControl:ResourceControl runat="server" ResourceName="Weblog_EntryList_By" id="PostByResource" />
								<CSBlog:WeblogPostData Property="DisplayName" LinkTo="AuthorUrl" runat="server" /> 
                                <CSControl:PlaceHolder runat="server">
    								<DisplayConditions Operator="Not"><CSBlog:WeblogPostPropertyValueComparison runat="server" ComparisonProperty="IsExternal" Operator="IsSetOrTrue" /></DisplayConditions>
	                                <ContentTemplate>
	                                    <CSControl:ResourceControl runat="server" ResourceName="Weblog_EntryList_With" id="CommentDesc"/> 
								        <CSBlog:WeblogPostData Text="{0} comment(s)" Property="Replies" LinkTo="PostComments" runat="server">
                                            <LeaderTemplate>| <CSControl:ResourceControl runat="server" ResourceName="Weblog_EntryList_With" /> </LeaderTemplate>
                                            <DisplayConditions><CSBlog:WeblogPostPropertyValueComparison runat="server" ComparisonProperty="Replies" ComparisonValue="0" Operator="GreaterThan" /></DisplayConditions>
                                        </CSBlog:WeblogPostData>
                                        <CSBlog:WeblogPostData Text="no comments" Property="Replies" LinkTo="PostComments" runat="server">
                                            <LeaderTemplate>| <CSControl:ResourceControl runat="server" ResourceName="Weblog_EntryList_With" /> </LeaderTemplate>
                                            <DisplayConditions><CSBlog:WeblogPostPropertyValueComparison runat="server" ComparisonProperty="Replies" ComparisonValue="0" Operator="LessThanOrEqualTo" /></DisplayConditions>
                                        </CSBlog:WeblogPostData>
                                     </ContentTemplate>
                                </CSControl:PlaceHolder>
								<CSBlog:WeblogPostTagEditableList runat="server" EditorLinkCssClass="CommonTextButton" EditorCssClass="CommonInlineTagEditor" Tag="Div" CssClass="em" />
							</div>
						</td>
					</tr>
				</table>
			</li>
		</ItemTemplate>
		<FooterTemplate>
		    </ul>
		</FooterTemplate>
    </CSBlog:WeblogPostList>

    <CSControl:SinglePager runat="server" ID="PostPager">
        <DisplayConditions>
            <CSControl:QueryStringPropertyValueComparison ID="QueryStringPropertyValueComparison1" QueryStringProperty="Tags" Operator="IsSetOrTrue" runat="server" />
        </DisplayConditions>
    </CSControl:SinglePager>
</asp:Content>

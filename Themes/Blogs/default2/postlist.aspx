<%@ Page Language="C#" AutoEventWireup="true" EnableViewState="false" MasterPageFile="theme.Master" Inherits="CommunityServer.Blogs.Controls.CSBlogThemePage" %>
<%@ Import namespace="CommunityServer.Components"%>
<%@ Import namespace="CommunityServer.Blogs.Components"%>
<script runat="Server">

    void Page_Load(object sender, EventArgs e)
    {
       BindData(EntryItems, a, pager);
    }

</script>
<asp:Content ContentPlaceHolderID="Main" runat="Server">
    
    <CSControl:WrappedLiteral  ID="a" Tag="h2" ContainerId="titleWrapper" CssClass="pageTitle" runat="server" />
    <CSBlog:WeblogPostList id="EntryItems" Runat="server">
        <ItemTemplate>
            <div class="post">
               <h5>
                   <CSBlog:WeblogPostData runat="server" Property="Subject" />
                   <CSBlog:WeblogPostRating runat="server" RatingCssClass="RateControl" RatingReadOnlyCssClass="RateControlReadOnly" RatingActiveCssClass="RateControlActive" ImagesBaseUrl="~/Themes/default/images/common/" />
               </h5>
               <CSBlog:WeblogPostData Property="FormattedBody" runat="server" />

                <p class="postfoot">	
                    <span class="em">Posted:</span>
                    <CSBlog:WeblogPostData Property="UserTime" LinkTo="Post" FormatString="f" runat="server" />
					<CSControl:ResourceControl runat="server" ResourceName="Weblog_EntryList_By" id="PostByResource" />
					<CSBlog:WeblogPostData Property="DisplayName" LinkTo="AuthorUrl" runat="server" /> 
					<CSControl:PlaceHolder runat="server">
						<DisplayConditions Operator="Not"><CSBlog:WeblogPostPropertyValueComparison runat="server" ComparisonProperty="IsExternal" Operator="IsSetOrTrue" /></DisplayConditions>
                        <ContentTemplate>
                            <CSBlog:WeblogPostData Text="{0} comment(s)" Property="Replies" LinkTo="PostComments" runat="server">
                                <LeaderTemplate><CSControl:ResourceControl runat="server" ResourceName="Weblog_EntryList_With" id="CommentDesc"/> </LeaderTemplate>
                                <DisplayConditions><CSBlog:WeblogPostPropertyValueComparison runat="server" ComparisonProperty="Replies" ComparisonValue="0" Operator="GreaterThan" /></DisplayConditions>
                            </CSBlog:WeblogPostData>
                            <CSBlog:WeblogPostData Text="no comments" Property="Replies" LinkTo="PostComments" runat="server">
                                <LeaderTemplate><CSControl:ResourceControl runat="server" ResourceName="Weblog_EntryList_With" id="CommentDesc"/> </LeaderTemplate>
                                <DisplayConditions><CSBlog:WeblogPostPropertyValueComparison ID="PropertyValueComparison1" runat="server" ComparisonProperty="Replies" ComparisonValue="0" Operator="LessThanOrEqualTo" /></DisplayConditions>
                            </CSBlog:WeblogPostData>
                        </ContentTemplate>
                    </CSControl:PlaceHolder>
                    <CSBlog:WeblogPostData LinkTo="PostEditor" ResourceName="Weblog_Link_Edit" runat="server" />
                    <CSBlog:WeblogPostTagEditableList runat="server" id="InlineTagEditorPanel" EditorLinkCssClass="TextButton" EditorCssClass="InlineTagEditor" Tag="Div" CssClass="em" />
                </p>                            
            </div>
        </ItemTemplate>
        <FooterTemplate></FooterTemplate>
    </CSBlog:WeblogPostList>
    <CSControl:SinglePager id = "pager" runat = "Server"  />
       
</asp:Content>

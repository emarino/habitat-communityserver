<%@ Page Language="C#" AutoEventWireup="true" EnableViewState="false" MasterPageFile="theme.Master" Inherits="CommunityServer.Blogs.Controls.CSBlogThemePage" %>
<%@ Import namespace="CommunityServer.Components"%>
<%@ Import namespace="CommunityServer.Blogs.Components"%>
<script runat="Server">

    void Page_Load(object sender, EventArgs e)
    {
       BindData(EntryItems, a, pager);
    }

</script>
<asp:Content ContentPlaceHolderID="bcr" runat="Server">
    
    <CSControl:WrappedLiteral  ID="a" Tag="h2" ContainerId="titleWrapper" CssClass="CommonSubTitle" runat="server" />
    <CSBlog:WeblogPostList id="EntryItems" Runat="server">
        <HeaderTemplate>
		    <ul class="BlogPostList">
		</HeaderTemplate>
		<ItemTemplate>
		    <li class="BlogPostArea None">
				<h4 class="BlogPostHeader"><CSBlog:WeblogPostData runat="server" Property="Subject" LinkTo="Post" />  <CSBlog:WeblogPostRating runat="server" RatingCssClass="CommonRateControl" RatingReadOnlyCssClass="CommonRateControlReadOnly" RatingActiveCssClass="CommonRateControlActive" ImagesBaseUrl="~/Themes/default/images/common/" /></h4>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td width="100%">
							<div class="BlogPostContent">
								<CSBlog:WeblogPostData ID="WeblogPostData2" Property="FormattedBody" runat="server" />
							</div>
							<div class="BlogPostFooter">
								<CSControl:ResourceControl runat="server" ResourceName="Weblog_EntryList_Posted" />
								<CSBlog:WeblogPostData Property="UserTime" LinkTo="Post" FormatString="MMM dd yyyy, hh:mm tt" runat="server" />
					            <CSControl:ResourceControl runat="server" ResourceName="Weblog_EntryList_By" id="PostByResource" />
								<CSBlog:WeblogPostData Property="DisplayName" LinkTo="AuthorUrl" runat="server" />  
								
								<CSControl:PlaceHolder runat="server">
    								<DisplayConditions Operator="Not"><CSBlog:WeblogPostPropertyValueComparison runat="server" ComparisonProperty="IsExternal" Operator="IsSetOrTrue" /></DisplayConditions>
	                                <ContentTemplate>
								        <CSBlog:WeblogPostData Text="{0} comment(s)" Property="Replies" LinkTo="PostComments" runat="server">
                                            <LeaderTemplate> <CSControl:ResourceControl runat="server" ResourceName="Weblog_EntryList_With" /> </LeaderTemplate>
                                            <DisplayConditions><CSBlog:WeblogPostPropertyValueComparison runat="server" ComparisonProperty="Replies" ComparisonValue="0" Operator="GreaterThan" /></DisplayConditions>
                                        </CSBlog:WeblogPostData>
                                        <CSBlog:WeblogPostData Text="no comments" Property="Replies" LinkTo="PostComments" runat="server">
                                            <LeaderTemplate> <CSControl:ResourceControl runat="server" ResourceName="Weblog_EntryList_With" /> </LeaderTemplate>
                                            <DisplayConditions><CSBlog:WeblogPostPropertyValueComparison runat="server" ComparisonProperty="Replies" ComparisonValue="0" Operator="LessThanOrEqualTo" /></DisplayConditions>
                                        </CSBlog:WeblogPostData>
                                    </ContentTemplate>
                                </CSControl:PlaceHolder>
                                    
								<CSBlog:WeblogPostTagEditableList runat="server" EditorLinkCssClass="CommonTextButton" EditorCssClass="CommonInlineTagEditor" Tag="Div" CssClass="em" />
							</div>
						</td>
					</tr>
				</table>
			</li>
		</ItemTemplate>
		<FooterTemplate>
		    </ul>
		</FooterTemplate>
    </CSBlog:WeblogPostList>
    <CSControl:SinglePager id = "pager" runat = "Server"  />
       
</asp:Content>

<%@ Page Language="C#" AutoEventWireup="true" EnableViewState="False" MasterPageFile="theme.Master" Inherits="CommunityServer.Blogs.Controls.CSBlogThemePage" %>
<%@ Import namespace="CommunityServer.Components"%>
<%@ Import namespace="CommunityServer.Blogs.Components"%>
<script runat="Server">

    void Page_Load(object sender, EventArgs e)
    {
        SetTitle(this.CurrentWeblog.AboutTitle, false);
    }

</script>
<asp:Content ContentPlaceHolderID="Main" runat="Server">
    <div class="entryview">
        <CSBlog:WeblogData Property="AboutTitle" runat="server" Tag="h3" CssClass="contentheading" />
        <CSBlog:WeblogData Property="AboutDescription" runat="server" />
    </div>
</asp:Content>
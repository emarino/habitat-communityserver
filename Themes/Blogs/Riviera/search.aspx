<%@ Page Language="C#" AutoEventWireup="true" EnableViewState="False" MasterPageFile="theme.Master" Inherits="CommunityServer.Blogs.Controls.CSBlogThemePage" %>
<%@ Import namespace="CommunityServer.Components"%>
<%@ Import namespace="CommunityServer.Blogs.Components"%>
<script runat="Server">
    void Page_Load(object sender, EventArgs e)
    {
        SetTitle("", false);
    }
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="Main" runat="Server">
    <CSControl:WrappedLiteral Tag="h3" ContainerId="titleWrapper" CssClass="contentheading" runat="server" Text="Search Results" />
    <CSControl:IndexPostList id="EntryItems" Runat="server">
        <QueryOverrides PagerID="pager" />
        <HeaderTemplate></HeaderTemplate>
        <ItemTemplate>
            <dl class="entrylist">
                <dt>
                    <CSControl:IndexPostData runat="server" Property="Title" LinkTo="Post" Tag="span" CssClass="entrylistheader" />
                    <br />
                    <span class="entrylistheadersub">
                        <CSControl:IndexPostData Property="PostDate" LinkTo="Post" FormatString="dd MMMM yy hh:mm tt" runat="server" />
                        <CSControl:IndexPostData Property="UserName" runat="server" LinkTo="Author"><LeaderTemplate>| </LeaderTemplate></CSControl:IndexPostData> 
                        <CSControl:PlaceHolder runat="server">
						    <DisplayConditions Operator="Not"><CSBlog:WeblogPostPropertyValueComparison runat="server" ComparisonProperty="IsExternal" Operator="IsSetOrTrue" /></DisplayConditions>
                            <ContentTemplate>
                                <CSBlog:WeblogPostData Text="{0} comment(s)" Property="Replies" LinkTo="PostComments" runat="server">
                                    <LeaderTemplate>| </LeaderTemplate>
                                    <DisplayConditions><CSBlog:WeblogPostPropertyValueComparison runat="server" ComparisonProperty="Replies" ComparisonValue="0" Operator="GreaterThan" /></DisplayConditions>
                                </CSBlog:WeblogPostData>
                                <CSBlog:WeblogPostData Text="no comments" Property="Replies" LinkTo="PostComments" runat="server">
                                    <LeaderTemplate>| <CSControl:ResourceControl runat="server" ResourceName="Weblog_EntryList_With" id="CommentDesc"/> </LeaderTemplate>
                                    <DisplayConditions><CSBlog:WeblogPostPropertyValueComparison ID="PropertyValueComparison1" runat="server" ComparisonProperty="Replies" ComparisonValue="0" Operator="LessThanOrEqualTo" /></DisplayConditions>
                                </CSBlog:WeblogPostData>
                             </ContentTemplate>
                        </CSControl:PlaceHolder>
                        <CSBlog:WeblogPostRating runat="server" RatingCssClass="CommonRateControl" RatingReadOnlyCssClass="CommonRateControlReadOnly" RatingActiveCssClass="CommonRateControlActive" ImagesBaseUrl="~/Themes/default/images/common/" />
                        <CSBlog:WeblogPostData LinkTo="PostEditor" ResourceName="Weblog_Link_Edit" runat="server" />                        
                    </span>
                </dt>
                <dd>
                    <CSControl:IndexPostData Property="FormattedBody" TruncateAt="300" runat="server">
                        <TrailerTemplate> <CSControl:IndexPostData runat="server" LinkTo="post" Text="Read More..." /></TrailerTemplate>
                    </CSControl:IndexPostData>
                </dd>                          
            </dl>
        </ItemTemplate>
        <FooterTemplate></FooterTemplate>
    </CSControl:IndexPostList>
    <CSControl:SinglePager id = "pager" runat = "Server"  />
</asp:Content>

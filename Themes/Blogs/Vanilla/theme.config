<?xml version="1.0" encoding="utf-8" ?>
<Theme previewImageUrl="~/themes/blogs/vanilla/preview.png" previewText="Includes options for customizing layout, fonts, colors, and images. Also allows CSS to be overridden.">
  <DynamicConfiguration>
    <propertyGroup id="themeVariation" text="Layout / General">
      <propertySubGroup text="Theme Variations">
        <property id="secondaryCssUrl" text="Layout" dataType="url" defaultValue="" descriptionText="Select a location for the blog sidebar.">
          <propertyValue value="" text="Sidebar on Left" />
          <propertyValue value="~/themes/blogs/vanilla/style/right.css" text="Sidebar on Right" />
        </property>
      </propertySubGroup>
      <propertySubGroup text="General Content">
        <property id="textColor" text="Text Color" dataType="color" defaultValue="#000000" descriptionText="Select the color for general text."  descriptionImageUrl="~/themes/default/images/location-all.png" />
        <property id="textFont" text="Font" dataType="string" defaultValue="Arial, Helvetica" descriptionText="Select the font for general content of this blog."  descriptionImageUrl="~/themes/default/images/location-all.png" controlType="Telligent.DynamicConfiguration.Controls.CssValueSelectionControl, Telligent.DynamicConfiguration" cssPropertyName="font-family">
          <propertyValue value="Andale Mono" text="Andale Mono" />
          <propertyValue value="'Apple Garamond', 'Times New Roman', 'Garamond', serif" text="Apple Garamond" />
          <propertyValue value="Arial, Helvetica" text="Arial" />
          <propertyValue value="Arial Black, Arial, Helvetica" text="Arial Black" />
          <propertyValue value="Book Antiqua" text="Book Antiqua" />
          <propertyValue value="Comic Sans MS" text="Comic Sans MS" />
          <propertyValue value="Courier New" text="Courier New" />
          <propertyValue value="Georgia" text="Georgia" />
          <propertyValue value="Helvetica, Arial" text="Helvetica" />
          <propertyValue value="Impact" text="Impact" />
          <propertyValue value="Tahoma, Arial, Helvetica" text="Tahoma" />
          <propertyValue value="Terminal" text="Terminal" />
          <propertyValue value="Times New Roman" text="Times New Roman" />
          <propertyValue value="Trebuchet MS" text="Trebuchet MS" />
          <propertyValue value="Verdana" text="Verdana" />
        </property>
      </propertySubGroup>
      <propertySubGroup text="Links">
        <property id="linkColor" text="Link Color" dataType="color" defaultValue="#006bad" descriptionText="Select the color for links in the blog."  descriptionImageUrl="~/themes/default/images/location-all.png" />
        <property id="visitedLinkColor" text="Link Color (Visited)" dataType="color" defaultValue="#006bad" descriptionText="Select the color for links which have previously been visited."  descriptionImageUrl="~/themes/default/images/location-all.png" />
        <property id="activeLinkColor" text="Link Color (Active)" dataType="color" defaultValue="#006bad" descriptionText="Select the color for links which are currently being selected."  descriptionImageUrl="~/themes/default/images/location-all.png" />
      </propertySubGroup>
      <propertySubGroup text="Background">
        <property id="siteBackgroundColor" text="Background Color" dataType="color" defaultValue="#ffffff" descriptionText="Select the background color for this blog."  descriptionImageUrl="~/themes/default/images/location-background.png" />
        <property id="siteBackgroundImage" text="Background Image URL" dataType="url" defaultValue="~/themes/blogs/vanilla/images/background.gif" descriptionText="Enter the URL of the image to tile horizontally accross the background of this blog."  descriptionImageUrl="~/themes/default/images/location-background.png" controlType="CommunityServer.Blogs.Controls.BlogFileUrlControl, CommunityServer.Blogs" />
      </propertySubGroup>
    </propertyGroup>

    <propertyGroup id="header" text="Header">
      <propertySubGroup text="Site Title">
        <property id="siteTitleHeight" text="Height" dataType="unit" descriptionText="Select the height of the blog title." defaultValue="" descriptionImageUrl="~/themes/default/images/location-titlebar.png" />
        <property id="siteTitleBackgroundImage" text="Background Image URL" dataType="url" defaultValue="" descriptionText="Enter the URL of the image to tile as the background of the blog title bar."  descriptionImageUrl="~/themes/default/images/location-titlebar.png" controlType="CommunityServer.Blogs.Controls.BlogFileUrlControl, CommunityServer.Blogs" />
        <property id="siteTitleColor" text="Text Color" dataType="color" defaultValue="#006bad" descriptionText="Select the color for the blog title text."  descriptionImageUrl="~/themes/default/images/location-titlebar.png" />
        <property id="siteTitleFont" text="Font" dataType="string" defaultValue="Arial, Helvetica" descriptionText="Select the font for the title of this blog." descriptionImageUrl="~/themes/default/images/location-titlebar.png" controlType="Telligent.DynamicConfiguration.Controls.CssValueSelectionControl, Telligent.DynamicConfiguration" cssPropertyName="font-family">
          <propertyValue value="Andale Mono" text="Andale Mono" />
          <propertyValue value="'Apple Garamond', 'Times New Roman', 'Garamond', serif" text="Apple Garamond" />
          <propertyValue value="Arial, Helvetica" text="Arial" />
          <propertyValue value="Arial Black, Arial, Helvetica" text="Arial Black" />
          <propertyValue value="Book Antiqua" text="Book Antiqua" />
          <propertyValue value="Comic Sans MS" text="Comic Sans MS" />
          <propertyValue value="Courier New" text="Courier New" />
          <propertyValue value="Georgia" text="Georgia" />
          <propertyValue value="Helvetica, Arial" text="Helvetica" />
          <propertyValue value="Impact" text="Impact" />
          <propertyValue value="Tahoma, Arial, Helvetica" text="Tahoma" />
          <propertyValue value="Terminal" text="Terminal" />
          <propertyValue value="Times New Roman" text="Times New Roman" />
          <propertyValue value="Trebuchet MS" text="Trebuchet MS" />
          <propertyValue value="Verdana" text="Verdana" />
        </property>
        <property id="siteTitleFontSize" text="Font Size" dataType="string" defaultValue="3em" descriptionText="Select the font size for the title of this blog." descriptionImageUrl="~/themes/default/images/location-titlebar.png" controlType="Telligent.DynamicConfiguration.Controls.CssValueSelectionControl, Telligent.DynamicConfiguration" cssPropertyName="font-size" width="200px" showStyleWhenSelected="false">
          <propertyValue value="2em" text="Smallest" />
          <propertyValue value="2.5em" text="Smaller" />
          <propertyValue value="3em" text="Medium" />
          <propertyValue value="3.5em" text="Larger" />
          <propertyValue value="4em" text="Largest" />
        </property>
        <property id="siteTitleBackgroundBorderColor" text="Border Color" dataType="color" defaultValue="#999999" descriptionText="Select the color for the border below the title of this blog."  descriptionImageUrl="~/themes/default/images/location-titlebar.png" />
      </propertySubGroup>
    </propertyGroup>

    <propertyGroup id="body" text="Body">
      <propertySubGroup text="Page Title">
        <property id="titleColor" text="Text Color" dataType="color" defaultValue="#006bad" descriptionText="Select the color for the text of page titles."  descriptionImageUrl="~/themes/default/images/location-content.png" />
        <property id="titleFont" text="Font" dataType="string" defaultValue="'Apple Garamond', 'Times New Roman', 'Garamond', serif" descriptionText="Select the font for titles of pages on this blog." descriptionImageUrl="~/themes/default/images/location-content.png" controlType="Telligent.DynamicConfiguration.Controls.CssValueSelectionControl, Telligent.DynamicConfiguration" cssPropertyName="font-family">
          <propertyValue value="Andale Mono" text="Andale Mono" />
          <propertyValue value="'Apple Garamond', 'Times New Roman', 'Garamond', serif" text="Apple Garamond" />
          <propertyValue value="Arial, Helvetica" text="Arial" />
          <propertyValue value="Arial Black, Arial, Helvetica" text="Arial Black" />
          <propertyValue value="Book Antiqua" text="Book Antiqua" />
          <propertyValue value="Comic Sans MS" text="Comic Sans MS" />
          <propertyValue value="Courier New" text="Courier New" />
          <propertyValue value="Georgia" text="Georgia" />
          <propertyValue value="Helvetica, Arial" text="Helvetica" />
          <propertyValue value="Impact" text="Impact" />
          <propertyValue value="Tahoma, Arial, Helvetica" text="Tahoma" />
          <propertyValue value="Terminal" text="Terminal" />
          <propertyValue value="Times New Roman" text="Times New Roman" />
          <propertyValue value="Trebuchet MS" text="Trebuchet MS" />
          <propertyValue value="Verdana" text="Verdana" />
        </property>
        <property id="titleFontSize" text="Font Size" dataType="string" defaultValue="1.7em" descriptionText="Select the font size for titles of pages on this blog." descriptionImageUrl="~/themes/default/images/location-content.png" controlType="Telligent.DynamicConfiguration.Controls.CssValueSelectionControl, Telligent.DynamicConfiguration" cssPropertyName="font-size" width="200px" showStyleWhenSelected="false">
          <propertyValue value="1.3em" text="Smallest" />
          <propertyValue value="1.5em" text="Smaller" />
          <propertyValue value="1.7em" text="Medium" />
          <propertyValue value="1.9em" text="Larger" />
          <propertyValue value="2.1em" text="Largest" />
        </property>
      </propertySubGroup>
      <propertySubGroup text="Sub Title">
        <property id="subTitleColor" text="Text Color" dataType="color" defaultValue="#006bad" descriptionText="Select the color for the text of page sub-titles."  descriptionImageUrl="~/themes/default/images/location-content.png" />
        <property id="subTitleFont" text="Font" dataType="string" defaultValue="'Apple Garamond', 'Times New Roman', 'Garamond', serif" descriptionText="Select the font for sub-titles of pages on this blog." descriptionImageUrl="~/themes/default/images/location-content.png" controlType="Telligent.DynamicConfiguration.Controls.CssValueSelectionControl, Telligent.DynamicConfiguration" cssPropertyName="font-family">
          <propertyValue value="Andale Mono" text="Andale Mono" />
          <propertyValue value="'Apple Garamond', 'Times New Roman', 'Garamond', serif" text="Apple Garamond" />
          <propertyValue value="Arial, Helvetica" text="Arial" />
          <propertyValue value="Arial Black, Arial, Helvetica" text="Arial Black" />
          <propertyValue value="Book Antiqua" text="Book Antiqua" />
          <propertyValue value="Comic Sans MS" text="Comic Sans MS" />
          <propertyValue value="Courier New" text="Courier New" />
          <propertyValue value="Georgia" text="Georgia" />
          <propertyValue value="Helvetica, Arial" text="Helvetica" />
          <propertyValue value="Impact" text="Impact" />
          <propertyValue value="Tahoma, Arial, Helvetica" text="Tahoma" />
          <propertyValue value="Terminal" text="Terminal" />
          <propertyValue value="Times New Roman" text="Times New Roman" />
          <propertyValue value="Trebuchet MS" text="Trebuchet MS" />
          <propertyValue value="Verdana" text="Verdana" />
        </property>
        <property id="subTitleFontSize" text="Font Size" dataType="string" defaultValue="1.7em" descriptionText="Select the font size for sub-titles of pages on this blog." descriptionImageUrl="~/themes/default/images/location-content.png" controlType="Telligent.DynamicConfiguration.Controls.CssValueSelectionControl, Telligent.DynamicConfiguration" cssPropertyName="font-size" width="200px" showStyleWhenSelected="false">
          <propertyValue value="1.3em" text="Smallest" />
          <propertyValue value="1.5em" text="Smaller" />
          <propertyValue value="1.7em" text="Medium" />
          <propertyValue value="1.9em" text="Larger" />
          <propertyValue value="2.1em" text="Largest" />
        </property>
      </propertySubGroup>
    </propertyGroup>

    <propertyGroup id="sidebar" text="Sidebar">
      <propertySubGroup text="Sidebar Header">
        <property id="sidebarHeaderTextColor" text="Text Color" dataType="color" defaultValue="#777777" descriptionText="Select the text color for headers in the sidebars."  descriptionImageUrl="~/themes/default/images/location-sidebars.png" />
        <property id="sidebarHeaderFont" text="Font" dataType="string" defaultValue="'Apple Garamond', 'Times New Roman', 'Garamond', serif" descriptionText="Select the font for headers in the sidebars."  descriptionImageUrl="~/themes/default/images/location-sidebars.png" controlType="Telligent.DynamicConfiguration.Controls.CssValueSelectionControl, Telligent.DynamicConfiguration" cssPropertyName="font-family">
          <propertyValue value="Andale Mono" text="Andale Mono" />
          <propertyValue value="'Apple Garamond', 'Times New Roman', 'Garamond', serif" text="Apple Garamond" />
          <propertyValue value="Arial, Helvetica" text="Arial" />
          <propertyValue value="Arial Black, Arial, Helvetica" text="Arial Black" />
          <propertyValue value="Book Antiqua" text="Book Antiqua" />
          <propertyValue value="Comic Sans MS" text="Comic Sans MS" />
          <propertyValue value="Courier New" text="Courier New" />
          <propertyValue value="Georgia" text="Georgia" />
          <propertyValue value="Helvetica, Arial" text="Helvetica" />
          <propertyValue value="Impact" text="Impact" />
          <propertyValue value="Tahoma, Arial, Helvetica" text="Tahoma" />
          <propertyValue value="Terminal" text="Terminal" />
          <propertyValue value="Times New Roman" text="Times New Roman" />
          <propertyValue value="Trebuchet MS" text="Trebuchet MS" />
          <propertyValue value="Verdana" text="Verdana" />
        </property>
        <property id="sidebarHeaderFontSize" text="Font Size" dataType="string" defaultValue="1.3em" descriptionText="Select the font size for headers in the sidebars." descriptionImageUrl="~/themes/default/images/location-sidebars.png" controlType="Telligent.DynamicConfiguration.Controls.CssValueSelectionControl, Telligent.DynamicConfiguration" cssPropertyName="font-size" width="200px" showStyleWhenSelected="false">
          <propertyValue value=".9em" text="Smallest" />
          <propertyValue value="1.1em" text="Smaller" />
          <propertyValue value="1.3em" text="Medium" />
          <propertyValue value="1.5em" text="Larger" />
          <propertyValue value="1.7em" text="Largest" />
        </property>
      </propertySubGroup>
      <propertySubGroup text="Sidebar Content">
        <property id="sidebarTextColor" text="Text Color" dataType="color" defaultValue="#000000" descriptionText="Select the text color for content in the sidebars."  descriptionImageUrl="~/themes/default/images/location-sidebars.png" />
        <property id="sidebarFont" text="Font" dataType="string" defaultValue="'Apple Garamond', 'Times New Roman', 'Garamond', serif" descriptionText="Select the font for content in the sidebars."  descriptionImageUrl="~/themes/default/images/location-sidebars.png" controlType="Telligent.DynamicConfiguration.Controls.CssValueSelectionControl, Telligent.DynamicConfiguration" cssPropertyName="font-family">
          <propertyValue value="Andale Mono" text="Andale Mono" />
          <propertyValue value="'Apple Garamond', 'Times New Roman', 'Garamond', serif" text="Apple Garamond" />
          <propertyValue value="Arial, Helvetica" text="Arial" />
          <propertyValue value="Arial Black, Arial, Helvetica" text="Arial Black" />
          <propertyValue value="Book Antiqua" text="Book Antiqua" />
          <propertyValue value="Comic Sans MS" text="Comic Sans MS" />
          <propertyValue value="Courier New" text="Courier New" />
          <propertyValue value="Georgia" text="Georgia" />
          <propertyValue value="Helvetica, Arial" text="Helvetica" />
          <propertyValue value="Impact" text="Impact" />
          <propertyValue value="Tahoma, Arial, Helvetica" text="Tahoma" />
          <propertyValue value="Terminal" text="Terminal" />
          <propertyValue value="Times New Roman" text="Times New Roman" />
          <propertyValue value="Trebuchet MS" text="Trebuchet MS" />
          <propertyValue value="Verdana" text="Verdana" />
        </property>
        <property id="sidebarFontSize" text="Font Size" dataType="string" defaultValue="1em" descriptionText="Select the font size for content in the sidebars." descriptionImageUrl="~/themes/default/images/location-sidebars.png" controlType="Telligent.DynamicConfiguration.Controls.CssValueSelectionControl, Telligent.DynamicConfiguration" cssPropertyName="font-size" width="200px" showStyleWhenSelected="false">
          <propertyValue value=".6em" text="Smallest" />
          <propertyValue value=".8em" text="Smaller" />
          <propertyValue value="1em" text="Medium" />
          <propertyValue value="1.2em" text="Larger" />
          <propertyValue value="1.4em" text="Largest" />
        </property>
      </propertySubGroup>
      <propertySubGroup text="Sidebar Links">
        <property id="sidebarLinkColor" text="Link Color" dataType="color" defaultValue="#006bad" descriptionText="Select the color for links in the sidebars."  descriptionImageUrl="~/themes/default/images/location-sidebars.png" />
        <property id="sidebarVisitedLinkColor" text="Link Color (Visited)" dataType="color" defaultValue="#006bad" descriptionText="Select the color for links which have previously been visited."  descriptionImageUrl="~/themes/default/images/location-sidebars.png" />
        <property id="sidebarActiveLinkColor" text="Link Color (Active)" dataType="color" defaultValue="#006bad" descriptionText="Select the color for links which are currently being selected."  descriptionImageUrl="~/themes/default/images/location-sidebars.png" />
      </propertySubGroup>
    </propertyGroup>

    <propertyGroup id="cssOverrides" text="Custom Styles (Advanced)">
      <property id="cssOverrides" text="CSS Overrides" dataType="string" defaultValue="" controlType="Telligent.DynamicConfiguration.Controls.MultilineStringControl, Telligent.DynamicConfiguration" descriptionText="Enter any CSS overrides to the default stylesheets." />
    </propertyGroup>
  </DynamicConfiguration>
</Theme>

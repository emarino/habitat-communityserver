<?xml version="1.0" encoding="utf-8" ?>
<Theme previewImageUrl="~/themes/blogs/hover/preview.png" previewText="Includes a variation featuring the Microsoft Style and options for customizing fonts, colors, and images.  Also allows CSS to be overridden.">
  <DynamicConfiguration>

    <propertyGroup id="themeVariation" text="Variations / General">
      <propertySubGroup text="Theme Variations">
        <property id="secondaryCssUrl" text="Variation" dataType="url" defaultValue="" descriptionText="Select a predefined theme variation.  When the variation is changed, any options that have not been customized will be updated to reflect the variation's predefined values.">
          <propertyValue value="" text="Normal" />
          <propertyValue value="~/themes/blogs/hover/style/ms.css" text="Microsoft Style" />
          <propertyRule type="Telligent.DynamicConfiguration.Rules.ValueAutomationRule, Telligent.DynamicConfiguration" processImmediately="true">
            <checkValue value="">
              <setValue id="linkColor" value="#066" />
              <setValue id="visitedLinkColor" value="#066" />
              <setValue id="activeLinkColor" value="#933" />
              <setValue id="siteTitleBackgroundColor" value="#088" />
              <setValue id="siteTitleColor" value="#fff" />
              <setValue id="siteTitleBorderColor" value="#933" />
              <setValue id="sidebarBackgroundColor" value="#DEE" />
              <setValue id="sidebarActiveLinkBackgroundColor" value="#CEE" />
              <setValue id="sidebarActiveLinkBorderColor" value="#999" />
              <setValue id="sidebarLinkColor" value="#333333" />
              <setValue id="sidebarActiveLinkColor" value="#333333" />
              <setValue id="sidebarVisitedLinkColor" value="#333333" />
              <setValue id="subTitleColor" value="#066" />
              <setValue id="titleColor" value="#933" />
            </checkValue>
            <checkValue value="~/themes/blogs/hover/style/ms.css">
              <setValue id="linkColor" value="#039" />
              <setValue id="visitedLinkColor" value="#039" />
              <setValue id="activeLinkColor" value="#f30" />
              <setValue id="siteTitleBackgroundColor" value="#039" />
              <setValue id="siteTitleColor" value="#fff" />
              <setValue id="siteTitleBorderColor" value="#69c" />
              <setValue id="sidebarBackgroundColor" value="#f1f1f1" />
              <setValue id="sidebarActiveLinkBackgroundColor" value="#ccc" />
              <setValue id="sidebarActiveLinkBorderColor" value="#999" />
              <setValue id="sidebarLinkColor" value="#000" />
              <setValue id="sidebarActiveLinkColor" value="#000" />
              <setValue id="sidebarVisitedLinkColor" value="#000" />
              <setValue id="subTitleColor" value="#393" />
              <setValue id="titleColor" value="#039" />
            </checkValue>
          </propertyRule>
        </property>
      </propertySubGroup>
      <propertySubGroup text="General Content">
        <property id="width" text="Width" dataType="unit" descriptionText="Select the width of the content." defaultValue="100%" />
        <property id="textColor" text="Text Color" dataType="color" defaultValue="#333333" descriptionText="Select the color for general text."  descriptionImageUrl="~/themes/default/images/location-all.png" />
        <property id="textFont" text="Font" dataType="string" defaultValue="Verdana, Arial, Helvetica, sans-serif" descriptionText="Select the font for general content of this blog."  descriptionImageUrl="~/themes/default/images/location-all.png" controlType="Telligent.DynamicConfiguration.Controls.CssValueSelectionControl, Telligent.DynamicConfiguration" cssPropertyName="font-family">
          <propertyValue value="Andale Mono" text="Andale Mono" />
          <propertyValue value="Arial, Helvetica" text="Arial" />
          <propertyValue value="Arial Black, Arial, Helvetica" text="Arial Black" />
          <propertyValue value="Book Antiqua" text="Book Antiqua" />
          <propertyValue value="Comic Sans MS" text="Comic Sans MS" />
          <propertyValue value="Courier New" text="Courier New" />
          <propertyValue value="Georgia" text="Georgia" />
          <propertyValue value="Helvetica, Arial" text="Helvetica" />
          <propertyValue value="Impact" text="Impact" />
          <propertyValue value="Tahoma, Arial, Helvetica" text="Tahoma" />
          <propertyValue value="Terminal" text="Terminal" />
          <propertyValue value="Times New Roman" text="Times New Roman" />
          <propertyValue value="Trebuchet MS" text="Trebuchet MS" />
          <propertyValue value="Verdana, Arial, Helvetica, sans-serif" text="Verdana" />
        </property>
      </propertySubGroup>
      <propertySubGroup text="Links">
        <property id="linkColor" text="Link Color" dataType="color" defaultValue="#066" descriptionText="Select the color for links in the blog."  descriptionImageUrl="~/themes/default/images/location-all.png" />
        <property id="visitedLinkColor" text="Link Color (Visited)" dataType="color" defaultValue="#066" descriptionText="Select the color for links which have previously been visited."  descriptionImageUrl="~/themes/default/images/location-all.png" />
        <property id="activeLinkColor" text="Link Color (Active)" dataType="color" defaultValue="#933" descriptionText="Select the color for links which are currently being selected."  descriptionImageUrl="~/themes/default/images/location-all.png" />
      </propertySubGroup>
      <propertySubGroup text="Background">
        <property id="siteBackgroundColor" text="Background Color" dataType="color" defaultValue="#ffffff" descriptionText="Select the background color for this blog."  descriptionImageUrl="~/themes/default/images/location-background.png" />
        <property id="siteBackgroundImage" text="Background Image URL" dataType="url" defaultValue="" descriptionText="Enter the URL of the image to tile as the background of this blog."  descriptionImageUrl="~/themes/default/images/location-background.png" controlType="CommunityServer.Blogs.Controls.BlogFileUrlControl, CommunityServer.Blogs" />
      </propertySubGroup>
    </propertyGroup>

    <propertyGroup id="header" text="Header">
      <propertySubGroup text="Site Title">
        <property id="siteTitleBackgroundColor" text="Background Color" dataType="color" defaultValue="#088" descriptionText="Select the background color for the blog title."  descriptionImageUrl="~/themes/default/images/location-titlebar.png" />
        <property id="siteTitleBorderColor" text="Border Color" dataType="color" defaultValue="#933" descriptionText="Select the color for the border below the title of this blog." descriptionImageUrl="~/themes/default/images/location-titlebar.png" />
        <property id="siteTitleColor" text="Text Color" dataType="color" defaultValue="#fff" descriptionText="Select the color for the blog title text."  descriptionImageUrl="~/themes/default/images/location-titlebar.png" />
        <property id="siteTitleFont" text="Font" dataType="string" defaultValue="Verdana, Arial, Helvetica, sans-serif" descriptionText="Select the font for the title of this blog." descriptionImageUrl="~/themes/default/images/location-titlebar.png" controlType="Telligent.DynamicConfiguration.Controls.CssValueSelectionControl, Telligent.DynamicConfiguration" cssPropertyName="font-family">
          <propertyValue value="Andale Mono" text="Andale Mono" />
          <propertyValue value="Arial, Helvetica" text="Arial" />
          <propertyValue value="Arial Black, Arial, Helvetica" text="Arial Black" />
          <propertyValue value="Book Antiqua" text="Book Antiqua" />
          <propertyValue value="Comic Sans MS" text="Comic Sans MS" />
          <propertyValue value="Courier New" text="Courier New" />
          <propertyValue value="Georgia" text="Georgia" />
          <propertyValue value="Helvetica, Arial" text="Helvetica" />
          <propertyValue value="Impact" text="Impact" />
          <propertyValue value="Tahoma, Arial, Helvetica" text="Tahoma" />
          <propertyValue value="Terminal" text="Terminal" />
          <propertyValue value="Times New Roman" text="Times New Roman" />
          <propertyValue value="Trebuchet MS" text="Trebuchet MS" />
          <propertyValue value="Verdana, Arial, Helvetica, sans-serif" text="Verdana" />
        </property>
        <property id="siteTitleFontSize" text="Font Size" dataType="string" defaultValue="1.6em" descriptionText="Select the font size for the title of this blog." descriptionImageUrl="~/themes/default/images/location-titlebar.png" controlType="Telligent.DynamicConfiguration.Controls.CssValueSelectionControl, Telligent.DynamicConfiguration" cssPropertyName="font-size" width="200px" showStyleWhenSelected="false">
          <propertyValue value="1.2em" text="Smallest" />
          <propertyValue value="1.4em" text="Smaller" />
          <propertyValue value="1.6em" text="Medium" />
          <propertyValue value="1.8em" text="Larger" />
          <propertyValue value="2em" text="Largest" />
        </property>
      </propertySubGroup>
    </propertyGroup>

    <propertyGroup id="body" text="Body">
      <propertySubGroup text="Page Title">
        <property id="titleColor" text="Text Color" dataType="color" defaultValue="#933" descriptionText="Select the color for the text of page titles."  descriptionImageUrl="~/themes/default/images/location-content.png" />
        <property id="titleFont" text="Font" dataType="string" defaultValue="Verdana, Arial, Helvetica, sans-serif" descriptionText="Select the font for titles of pages on this blog." descriptionImageUrl="~/themes/default/images/location-content.png" controlType="Telligent.DynamicConfiguration.Controls.CssValueSelectionControl, Telligent.DynamicConfiguration" cssPropertyName="font-family">
          <propertyValue value="Andale Mono" text="Andale Mono" />
          <propertyValue value="Arial, Helvetica" text="Arial" />
          <propertyValue value="Arial Black, Arial, Helvetica" text="Arial Black" />
          <propertyValue value="Book Antiqua" text="Book Antiqua" />
          <propertyValue value="Comic Sans MS" text="Comic Sans MS" />
          <propertyValue value="Courier New" text="Courier New" />
          <propertyValue value="Georgia" text="Georgia" />
          <propertyValue value="Helvetica, Arial" text="Helvetica" />
          <propertyValue value="Impact" text="Impact" />
          <propertyValue value="Tahoma, Arial, Helvetica" text="Tahoma" />
          <propertyValue value="Terminal" text="Terminal" />
          <propertyValue value="Times New Roman" text="Times New Roman" />
          <propertyValue value="Trebuchet MS" text="Trebuchet MS" />
          <propertyValue value="Verdana, Arial, Helvetica, sans-serif" text="Verdana" />
        </property>
        <property id="titleFontSize" text="Font Size" dataType="string" defaultValue="1.2em" descriptionText="Select the font size for titles of pages on this blog." descriptionImageUrl="~/themes/default/images/location-content.png" controlType="Telligent.DynamicConfiguration.Controls.CssValueSelectionControl, Telligent.DynamicConfiguration" cssPropertyName="font-size" width="200px" showStyleWhenSelected="false">
          <propertyValue value=".8em" text="Smallest" />
          <propertyValue value="1em" text="Smaller" />
          <propertyValue value="1.2em" text="Medium" />
          <propertyValue value="1.4em" text="Larger" />
          <propertyValue value="1.6em" text="Largest" />
        </property>
      </propertySubGroup>
      <propertySubGroup text="Sub Title">
        <property id="subTitleColor" text="Text Color" dataType="color" defaultValue="#066" descriptionText="Select the color for the text of page sub-titles."  descriptionImageUrl="~/themes/default/images/location-content.png" />
        <property id="subTitleFont" text="Font" dataType="string" defaultValue="Verdana, Arial, Helvetica, sans-serif" descriptionText="Select the font for sub-titles of pages on this blog." descriptionImageUrl="~/themes/default/images/location-content.png" controlType="Telligent.DynamicConfiguration.Controls.CssValueSelectionControl, Telligent.DynamicConfiguration" cssPropertyName="font-family">
          <propertyValue value="Andale Mono" text="Andale Mono" />
          <propertyValue value="Arial, Helvetica" text="Arial" />
          <propertyValue value="Arial Black, Arial, Helvetica" text="Arial Black" />
          <propertyValue value="Book Antiqua" text="Book Antiqua" />
          <propertyValue value="Comic Sans MS" text="Comic Sans MS" />
          <propertyValue value="Courier New" text="Courier New" />
          <propertyValue value="Georgia" text="Georgia" />
          <propertyValue value="Helvetica, Arial" text="Helvetica" />
          <propertyValue value="Impact" text="Impact" />
          <propertyValue value="Tahoma, Arial, Helvetica" text="Tahoma" />
          <propertyValue value="Terminal" text="Terminal" />
          <propertyValue value="Times New Roman" text="Times New Roman" />
          <propertyValue value="Trebuchet MS" text="Trebuchet MS" />
          <propertyValue value="Verdana, Arial, Helvetica, sans-serif" text="Verdana" />
        </property>
        <property id="subTitleFontSize" text="Font Size" dataType="string" defaultValue="1.5em" descriptionText="Select the font size for sub-titles of pages on this blog." descriptionImageUrl="~/themes/default/images/location-content.png" controlType="Telligent.DynamicConfiguration.Controls.CssValueSelectionControl, Telligent.DynamicConfiguration" cssPropertyName="font-size" width="200px" showStyleWhenSelected="false">
          <propertyValue value="1.1em" text="Smallest" />
          <propertyValue value="1.3em" text="Smaller" />
          <propertyValue value="1.5em" text="Medium" />
          <propertyValue value="1.7em" text="Larger" />
          <propertyValue value="1.9em" text="Largest" />
        </property>
      </propertySubGroup>
    </propertyGroup>

    <propertyGroup id="sidebar" text="Sidebar">
      <propertySubGroup text="Sidebar">
        <property id="sidebarWidth" text="Width" dataType="unit" descriptionText="Select the width of the sidebars." defaultValue="180px" descriptionImageUrl="~/themes/default/images/location-sidebars.png" />
        <property id="sidebarBackgroundColor" text="Background Color" dataType="color" defaultValue="#DEE" descriptionText="Select the background color for the sidebars."  descriptionImageUrl="~/themes/default/images/location-sidebars.png" />
      </propertySubGroup>
      <propertySubGroup text="Sidebar Header">
        <property id="sidebarHeaderTextColor" text="Text Color" dataType="color" defaultValue="#333333" descriptionText="Select the text color for headers in the sidebars."  descriptionImageUrl="~/themes/default/images/location-sidebars.png" />
        <property id="sidebarHeaderFont" text="Font" dataType="string" defaultValue="Verdana, Arial, Helvetica, sans-serif" descriptionText="Select the font for headers in the sidebars."  descriptionImageUrl="~/themes/default/images/location-sidebars.png" controlType="Telligent.DynamicConfiguration.Controls.CssValueSelectionControl, Telligent.DynamicConfiguration" cssPropertyName="font-family">
          <propertyValue value="Andale Mono" text="Andale Mono" />
          <propertyValue value="Arial, Helvetica" text="Arial" />
          <propertyValue value="Arial Black, Arial, Helvetica" text="Arial Black" />
          <propertyValue value="Book Antiqua" text="Book Antiqua" />
          <propertyValue value="Comic Sans MS" text="Comic Sans MS" />
          <propertyValue value="Courier New" text="Courier New" />
          <propertyValue value="Georgia" text="Georgia" />
          <propertyValue value="Helvetica, Arial" text="Helvetica" />
          <propertyValue value="Impact" text="Impact" />
          <propertyValue value="Tahoma, Arial, Helvetica" text="Tahoma" />
          <propertyValue value="Terminal" text="Terminal" />
          <propertyValue value="Times New Roman" text="Times New Roman" />
          <propertyValue value="Trebuchet MS" text="Trebuchet MS" />
          <propertyValue value="Verdana, Arial, Helvetica, sans-serif" text="Verdana" />
        </property>
        <property id="sidebarHeaderFontSize" text="Font Size" dataType="string" defaultValue="1em" descriptionText="Select the font size for headers in the sidebars." descriptionImageUrl="~/themes/default/images/location-sidebars.png" controlType="Telligent.DynamicConfiguration.Controls.CssValueSelectionControl, Telligent.DynamicConfiguration" cssPropertyName="font-size" width="200px" showStyleWhenSelected="false">
          <propertyValue value=".6em" text="Smallest" />
          <propertyValue value=".8em" text="Smaller" />
          <propertyValue value="1em" text="Medium" />
          <propertyValue value="1.2em" text="Larger" />
          <propertyValue value="1.4em" text="Largest" />
        </property>
      </propertySubGroup>
      <propertySubGroup text="Sidebar Content">
        <property id="sidebarTextColor" text="Text Color" dataType="color" defaultValue="#333333" descriptionText="Select the text color for content in the sidebars."  descriptionImageUrl="~/themes/default/images/location-sidebars.png" />
        <property id="sidebarFont" text="Font" dataType="string" defaultValue="Verdana, Arial, Helvetica, sans-serif" descriptionText="Select the font for content in the sidebars."  descriptionImageUrl="~/themes/default/images/location-sidebars.png" controlType="Telligent.DynamicConfiguration.Controls.CssValueSelectionControl, Telligent.DynamicConfiguration" cssPropertyName="font-family">
          <propertyValue value="Andale Mono" text="Andale Mono" />
          <propertyValue value="Arial, Helvetica" text="Arial" />
          <propertyValue value="Arial Black, Arial, Helvetica" text="Arial Black" />
          <propertyValue value="Book Antiqua" text="Book Antiqua" />
          <propertyValue value="Comic Sans MS" text="Comic Sans MS" />
          <propertyValue value="Courier New" text="Courier New" />
          <propertyValue value="Georgia" text="Georgia" />
          <propertyValue value="Helvetica, Arial" text="Helvetica" />
          <propertyValue value="Impact" text="Impact" />
          <propertyValue value="Tahoma, Arial, Helvetica" text="Tahoma" />
          <propertyValue value="Terminal" text="Terminal" />
          <propertyValue value="Times New Roman" text="Times New Roman" />
          <propertyValue value="Trebuchet MS" text="Trebuchet MS" />
          <propertyValue value="Verdana, Arial, Helvetica, sans-serif" text="Verdana" />
        </property>
        <property id="sidebarFontSize" text="Font Size" dataType="string" defaultValue=".9em" descriptionText="Select the font size for content in the sidebars." descriptionImageUrl="~/themes/default/images/location-sidebars.png" controlType="Telligent.DynamicConfiguration.Controls.CssValueSelectionControl, Telligent.DynamicConfiguration" cssPropertyName="font-size" width="200px" showStyleWhenSelected="false">
          <propertyValue value=".5em" text="Smallest" />
          <propertyValue value=".7em" text="Smaller" />
          <propertyValue value=".9em" text="Medium" />
          <propertyValue value="1.1em" text="Larger" />
          <propertyValue value="1.3em" text="Largest" />
        </property>
      </propertySubGroup>
      <propertySubGroup text="Sidebar Links">
        <property id="sidebarLinkColor" text="Link Color" dataType="color" defaultValue="#333333" descriptionText="Select the color for links in the sidebars."  descriptionImageUrl="~/themes/default/images/location-sidebars.png" />
        <property id="sidebarVisitedLinkColor" text="Link Color (Visited)" dataType="color" defaultValue="#333333" descriptionText="Select the color for links which have previously been visited."  descriptionImageUrl="~/themes/default/images/location-sidebars.png" />
        <property id="sidebarActiveLinkColor" text="Link Color (Active)" dataType="color" defaultValue="#333333" descriptionText="Select the color for links which are currently being selected."  descriptionImageUrl="~/themes/default/images/location-sidebars.png" />
        <property id="sidebarActiveLinkBackgroundColor" text="Link Background Color (Active)" dataType="color" defaultValue="#CEE" descriptionText="Select the background color for linkes which are being selected."  descriptionImageUrl="~/themes/default/images/location-sidebars.png" />
        <property id="sidebarActiveLinkBorderColor" text="Link Border Color (Active)" dataType="color" defaultValue="#999" descriptionText="Select the border color for links which are being selected."  descriptionImageUrl="~/themes/default/images/location-sidebars.png" />
      </propertySubGroup>
    </propertyGroup>

    <propertyGroup id="cssOverrides" text="Custom Styles (Advanced)">
      <property id="cssOverrides" text="CSS Overrides" dataType="string" defaultValue="" controlType="Telligent.DynamicConfiguration.Controls.MultilineStringControl, Telligent.DynamicConfiguration" descriptionText="Enter any CSS overrides to the default stylesheets." />
    </propertyGroup>
  </DynamicConfiguration>
</Theme>
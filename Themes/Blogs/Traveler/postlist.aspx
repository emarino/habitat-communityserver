<%@ Page Language="C#" AutoEventWireup="true" EnableViewState="false" MasterPageFile="theme.Master" Inherits="CommunityServer.Blogs.Controls.CSBlogThemePage" %>
<%@ Import namespace="CommunityServer.Components"%>
<%@ Import namespace="CommunityServer.Blogs.Components"%>
<script runat="Server">

    void Page_Load(object sender, EventArgs e)
    {
       BindData(EntryItems, a, pager);
       if (string.IsNullOrEmpty(a.Text))
           a.Text = "Recent Posts";
    }

</script>
<asp:Content ContentPlaceHolderID="Main" runat="Server">
    
    <CSControl:WrappedLiteral  ID="a" Tag="h2" CssClass="pageTitle" runat="server" />
    <CSBlog:WeblogPostList id="EntryItems" Runat="server">
        <ItemTemplate>
            <div class="post">
                <div class="postcommentarea">
                    <CSControl:PlaceHolder runat="server">
					    <DisplayConditions Operator="Not"><CSBlog:WeblogPostPropertyValueComparison runat="server" ComparisonProperty="IsExternal" Operator="IsSetOrTrue" /></DisplayConditions>
                        <ContentTemplate>
                            <CSBlog:WeblogPostData Text="{0}" Property="Replies" LinkTo="PostComments" runat="server" /><br />Comments
                        </ContentTemplate>
                    </CSControl:PlaceHolder>
                </div>
                <div class="postasidecomment">
                   <div class="postsub">
                       <h2>
                         <CSBlog:WeblogPostData runat="server" Property="Subject" LinkTo="post" />
                         <span>
                            <CSControl:ResourceControl runat="server" ResourceName="Weblog_EntryList_By" id="PostByResource" /> <CSBlog:WeblogPostData Property="DisplayName" LinkTo="AuthorUrl" runat="server" />
                         </span>
                         <CSBlog:WeblogPostRating runat="server" RatingCssClass="CommonRateControl" RatingReadOnlyCssClass="CommonRateControlReadOnly" RatingActiveCssClass="CommonRateControlActive" ImagesBaseUrl="~/Themes/default/images/common/" />
                       </h2>
                       <CSBlog:WeblogPostData Property="Excerpt" runat="server" />

                        <div class="postfoot">	
                            <CSBlog:WeblogPostTagEditableList runat="server" id="InlineTagEditorPanel" EditorLinkCssClass="TextButton" EditorCssClass="InlineTagEditor" Tag="Div" />
                            <CSBlog:WeblogPostData LinkTo="PostEditor" ResourceName="Weblog_Link_Edit" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </ItemTemplate>
        <FooterTemplate></FooterTemplate>
    </CSBlog:WeblogPostList>
    
    <CSControl:SinglePager id="pager" runat="Server"  />
</asp:Content>

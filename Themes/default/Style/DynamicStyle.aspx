<%@ Page Language="C#" AutoEventWireup="true" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="System.Drawing" %>

<script language="C#" runat="server">

    protected ThemeConfigurationData ThemeData = CSContext.Current.ThemePreviewCookie.IsPreviewing() ? ThemeConfigurationDatas.GetThemeConfigurationData("default", CSContext.Current.ThemePreviewCookie.PreviewID, true) : ThemeConfigurationDatas.GetThemeConfigurationData("default", true);

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Page.Response.ContentType = "text/css";
   
        Page.Response.Expires = 30;
    }

    protected Color AdjustIntensity(Color color, int amount)
    {
        int r = color.R;
        if (r + amount > 255)
            r = 255;
        else if (r + amount < 0)
            r = 0;
        else
            r += amount;

        int g = color.G;
        if (g + amount > 255)
            g = 255;
        else if (g + amount < 0)
            g = 0;
        else
            g += amount;

        int b = color.B;
        if (b + amount > 255)
            b = 255;
        else if (b + amount < 0)
            b = 0;
        else
            b += amount;
        
        return Color.FromArgb((byte) r, (byte) g, (byte) b); 
    }

    protected string UrlOrNone(Uri url)
    {
        if (url == null)
            return "none";
        else
            return "url(" + ResolveUrl(url.ToString()) + ")";
    }

</script>

/* General Styles */

body, html, .CommonContent
{
    font-family: <%= ThemeData.GetStringValue("textFont", "Arial, Helvetica") %>;
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("textColor", ColorTranslator.FromHtml("#333"))) %>;
}

form
{
    background-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("siteBackgroundColor", ColorTranslator.FromHtml("#fff"))) %>;
    background-image: <%= UrlOrNone(ThemeData.GetUrlValue("siteBackgroundImage", null))%>;
    min-width: <%= ThemeData.GetUnitValue("width", Unit.Parse("960px")) %>;
}


A:LINK
{
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("linkColor", ColorTranslator.FromHtml("#000")))%>;
}

A:ACTIVE
{
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("activeLinkColor", ColorTranslator.FromHtml("#000")))%>;
}

A:VISITED
{
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("visitedLinkColor", ColorTranslator.FromHtml("#666")))%>;
}

SELECT, TEXTAREA, INPUT, BUTTON
{
    font-family: <%= ThemeData.GetStringValue("textFont", "Arial, Helvetica") %>;
}

.Common
{
    width: <%= ThemeData.GetUnitValue("width", Unit.Parse("960px")) %>;
}

/* Header Styles */

#CommonHeader
{
    border-top-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("siteTitleTopBorderColor", ColorTranslator.FromHtml("#3A477A")))%>;
    background-image: <%= UrlOrNone(ThemeData.GetUrlValue("siteTitleBackgroundImage", null))%>;
    background-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("siteTitleBackgroundColor", ColorTranslator.FromHtml("#ffffff")))%>;
}

#CommonHeaderUserRoundBottom .r1, #CommonHeaderUserRoundBottom .r2, #CommonHeaderUserRoundBottom .r3, #CommonHeaderUserRoundBottom .r4,
#CommonHeaderUserRoundLeft .r1, #CommonHeaderUserRoundLeft .r2, #CommonHeaderUserRoundLeft .r3, #CommonHeaderUserRoundLeft .r4,
#CommonHeaderUserRoundRight .r1, #CommonHeaderUserRoundRight .r2, #CommonHeaderUserRoundRight .r3, #CommonHeaderUserRoundRight .r4,
#CommonHeaderUserContent
{
    background-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("siteTitleTopBorderColor", ColorTranslator.FromHtml("#3A477A")))%>;
}

#CommonHeader table td
{
    height: <%= ThemeData.GetUnitValue("siteTitleHeight", Unit.Parse("113px"))%>;
}

/* Navigation Styles */

#CommonNavigation
{
    background-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("navigationTabBackgroundColor", ColorTranslator.FromHtml("#9AB5EC")))%>;
}

#CommonNavigation a:link, #CommonNavigation a:active, #CommonNavigation a:visited
{
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("navigationTabTextColor", ColorTranslator.FromHtml("#000")))%>;
}

#CommonNavigation a:hover
{
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("navigationTabHoverTextColor", ColorTranslator.FromHtml("#fff")))%>;
}

#CommonNavigation a.Selected:link, #CommonNavigation a.Selected:active, #CommonNavigation a.Selected:visited
{
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("navigationTabSelectedTextColor", ColorTranslator.FromHtml("#fff")))%>;
}

.CommonBreadCrumbArea
{
    background-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("breadcrumbBackgroundColor", ColorTranslator.FromHtml("#E9EEF9")))%>;
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("breadcrumbTextColor", ColorTranslator.FromHtml("#000")))%>;
}

.CommonBreadCrumbArea a:link, .CommonBreadCrumbArea a:visited, .CommonBreadCrumbArea a:active
{
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("breadcrumbTextColor", ColorTranslator.FromHtml("#000")))%>;
}

/* Body Styles */

.CommonTitle, .CommonProfileTitle, .CommonTitle A:LINK, .CommonTitle A:ACTIVE, .CommonTitle A:VISITED, .CommonTitle A:HOVER, .ForumThreadPostTitle
{
    font-family: <%= ThemeData.GetStringValue("titleFont", "Arial, Helvetica") %>;
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("titleColor", ColorTranslator.FromHtml("#aaa"))) %>;
}

.CommonTitle, .CommonProfileTitle, 
{
    font-size: <%= ThemeData.GetStringValue("titleFontSize", "280%")%>;
}

#CommonHeaderTitle
{
    font-size: <%= ThemeData.GetStringValue("siteTitleFontSize", "140%")%>;
}

#CommonHeaderTitle h1, #CommonHeaderTitle h1 A:LINK, #CommonHeaderTitle h1 A:VISITED, #CommonHeaderTitle h1 A:ACTIVE
{
    font-family: <%= ThemeData.GetStringValue("siteTitleFont", "Arial, Helvetica") %>;
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("siteTitleColor", ColorTranslator.FromHtml("#A0AE5A"))) %>;
}

.CommonSubTitle, .CommonHeader, .CommonFormTitle
{
    font-family: <%= ThemeData.GetStringValue("headerFont", "Arial, Helvetica") %>;
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("headerColor", ColorTranslator.FromHtml("#aaa"))) %>;
}

.CommonInlineMessageTitle
{
    font-size: <%= ThemeData.GetStringValue("boxedContentHeaderFontSize", "100%") %>;
}

.CommonInlineMessageTitle, .CommonMessageTitle
{
    font-family: <%= ThemeData.GetStringValue("boxedContentHeaderFont", "Arial, Helvetica") %>;
    background-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("boxedContentHeaderBackgroundColor", ColorTranslator.FromHtml("#E1E1E1")))%>;
}

.CommonInlineMessageTitle, .CommonInlineMessageTitle A:LINK, .CommonInlineMessageTitle A:VISITED, .CommonInlineMessageTitle A:ACTIVE, 
.CommonInlineMessageTitle A:HOVER, .CommonMessageTitle
{
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("boxedContentHeaderTextColor", ColorTranslator.FromHtml("#999")))%>;
}

.CommonInlineMessageArea, .CommonInlineMessageTitle, .CommonMessageContent, .CommonMessageTitle
{
    border-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("boxedContentBorderColor", ColorTranslator.FromHtml("#E1E1E1")))%>;
}

.ForumPostHeader
{
    font-size: <%= ThemeData.GetStringValue("forumPostHeaderFontSize", "90%")%>;
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("forumPostHeaderTextColor", ColorTranslator.FromHtml("#000")))%>;
    font-family: <%= ThemeData.GetStringValue("forumPostHeaderFont", "Arial, Helvetica")%>;
    background-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("forumPostHeaderBackgroundColor", ColorTranslator.FromHtml("#9AB5EC")))%>;
}

.ForumPostHeader, .ForumPostManagementArea, .ForumPostTitleArea, .ForumPostContentArea, .ForumPostUserArea, .ForumPostFooterArea
{
    border-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("forumPostBorderColor", ColorTranslator.FromHtml("#E1E1E1")))%>;
}

.ForumPostTitleArea, .ForumPostManagementArea
{
    background-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("forumPostManagementAreaBackgroundColor", ColorTranslator.FromHtml("#f4f4f4")))%>;
}

.ForumPostUserArea
{
    background-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("forumPostUserAreaBackgroundColor", ColorTranslator.FromHtml("#eee")))%>;
}

.ForumPostContentArea
{
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("forumPostContentColor", ColorTranslator.FromHtml("#000")))%>;
}


/* Tag Styles */

.CommonTag1 a:link, .CommonTag1 a:visited, .CommonTag1 a:active
{
    color: <%= ColorTranslator.ToHtml(AdjustIntensity(ThemeData.GetColorValue("linkColor", ColorTranslator.FromHtml("#000")), -85))%>;
}

.CommonTag2 a:link, .CommonTag2 a:visited, .CommonTag2 a:active
{
    color: <%= ColorTranslator.ToHtml(AdjustIntensity(ThemeData.GetColorValue("linkColor", ColorTranslator.FromHtml("#000")), -68))%>;
}

.CommonTag3 a:link, .CommonTag3 a:visited, .CommonTag3 a:active
{
    color: <%= ColorTranslator.ToHtml(AdjustIntensity(ThemeData.GetColorValue("linkColor", ColorTranslator.FromHtml("#000")), -51))%>;
}

.CommonTag4 a:link, .CommonTag4 a:visited, .CommonTag4 a:active
{
    color: <%= ColorTranslator.ToHtml(AdjustIntensity(ThemeData.GetColorValue("linkColor", ColorTranslator.FromHtml("#000")), -34))%>;
}

.CommonTag5 a:link, .CommonTag5 a:visited, .CommonTag5 a:active
{
    color: <%= ColorTranslator.ToHtml(AdjustIntensity(ThemeData.GetColorValue("linkColor", ColorTranslator.FromHtml("#000")), -17))%>;
}

.CommonTag6 a:link, .CommonTag6 a:visited, .CommonTag6 a:active
{
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("linkColor", ColorTranslator.FromHtml("#000")))%>;
}

A.ForumGroupNameRead, .ForumGroupNameRead:LINK, .ForumGroupNameRead:VISITED, A.ForumNameRead, .ForumNameRead:LINK, .ForumNameRead:VISITED
{
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("visitedLinkColor", ColorTranslator.FromHtml("#666")))%>;
}

A.ForumGroupNameUnRead, .ForumGroupNameUnRead:LINK, .ForumGroupNameUnRead:VISITED, A.ForumNameUnRead, .ForumNameUnRead:LINK, .ForumNameUnRead:VISITED
{
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("linkColor", ColorTranslator.FromHtml("#000")))%>;
}

/* List Styles */

.CommonListTitle
{
    font-family: <%= ThemeData.GetStringValue("listTitleFont", "Arial, Helvetica") %>;
    font-size: <%= ThemeData.GetStringValue("listTitleFontSize", "180%")%>;
}

.CommonListTitle, .CommonListTitle a:link, .CommonListTitle a:visited, .CommonListTitle a:active
{
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("listTitleTextColor", ColorTranslator.FromHtml("#000")))%>;
}

.CommonListHeader, .CommonListHeaderLeftMost
{
    background-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("listHeaderBackgroundColor", ColorTranslator.FromHtml("#E1E1E1")))%>;
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("listHeaderTextColor", ColorTranslator.FromHtml("#999")))%>;
}

.CommonListRowAlt td
{
    background-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("listAlternateRowColor", ColorTranslator.FromHtml("#ffc")))%>;
}

.CommonListHeader, .CommonListHeaderLeftMost, .CommonListCell, .CommonListCellLeftMost, .ForumListCellLeftMostImageOnly, .ForumListCellImageOnly, .FileListCellLeftMostImageOnly
{
    border-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("listBorderColor", ColorTranslator.FromHtml("#E1E1E1")))%>;
}

/* Footer Styles */

#CommonFooter, body, html
{
    background-image: <%= UrlOrNone(ThemeData.GetUrlValue("siteFooterBackgroundImage", null))%>;
    background-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("siteFooterBackgroundColor", ColorTranslator.FromHtml("#EAECE0")))%>;
}

/* Sidebar Styles */

#CommonSidebarLeft .CommonSidebar, #CommonSidebarRight .CommonSidebar
{
    width: <%= ThemeData.GetUnitValue("sidebarWidth", Unit.Parse("312px")) %>;
}

.CommonSidebarInnerArea, .CommonSidebarRoundTop .r1, .CommonSidebarRoundTop .r2, .CommonSidebarRoundTop .r3, .CommonSidebarRoundTop .r4, .CommonSidebarRoundBottom .r1, .CommonSidebarRoundBottom .r2, .CommonSidebarRoundBottom .r3, .CommonSidebarRoundBottom .r4
{
    background-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("sidebarBackgroundColor", ColorTranslator.FromHtml("#D7D7CE")))%>;
    border-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("sidebarBorderColor", ColorTranslator.FromHtml("#999")))%>;
}

.CommonSidebarHeader, .CommonSidebarFooter
{
    border-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("sidebarHeaderBorderColor", ColorTranslator.FromHtml("#fff")))%>;
}

.CommonSidebarHeader, .CommonSidebarHeader A:LINK, .CommonSidebarHeader A:VISITED, .CommonSidebarHeader A:ACTIVE, .CommonSidebarHeader A:HOVER
{
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("sidebarHeaderTextColor", ColorTranslator.FromHtml("#333")))%>;
}

/* Modal Styles */

.CommonModal1, .CommonModal2, .CommonModal3, .CommonModal4, .CommonModal5 
{
    background-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("modalBackgroundColor", ColorTranslator.FromHtml("#ccc")))%>;
}

.CommonModal1, .CommonModal2, .CommonModal3, .CommonModal4, .CommonModal5, .CommonModalTitle, .CommonModalFooter
{
    border-color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("modalBorderColor", ColorTranslator.FromHtml("#666")))%>;
}

.CommonModalTitle
{
    color: <%= ColorTranslator.ToHtml(ThemeData.GetColorValue("modalTitleTextColor", ColorTranslator.FromHtml("#000")))%>;
    font-family: <%= ThemeData.GetStringValue("modalTitleFont", "Arial, Helvetica") %>;
}

<%= ThemeData.GetStringValue("cssOverrides", string.Empty) %>

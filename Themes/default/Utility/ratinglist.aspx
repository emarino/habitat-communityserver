<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Controls.CSThemePage" MasterPageFile="../Common/modal.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Discussions.Components" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        SetTitle("Ratings", false);
        
        if (CurrentPost != null && CurrentPost is ForumPost && CurrentCSContext.SiteSettings.SectionRatingType == SectionRatingType.PostRating)
            RatingsListRatings.QueryOverrides.RatingType = RatingType.Post;
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
<div class="CommonContentArea">
	<div class="CommonContent">
        <CSControl:RatingList runat="server" ID="RatingsListRatings">
            <QueryOverrides RatingType="Thread" />
            <HeaderTemplate>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td class="CommonListHeaderLeftMost">
                            <CSControl:ResourceControl runat="server" ResourceName="ThreadRatingSummary_RatedBy" />
                        </td>
                        <td class="CommonListHeader">
                            <CSControl:ResourceControl runat="server" ResourceName="ThreadRatingSummary_Rating" />
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                    <tr>
                        <td class="CommonListCellLeftMost">
                            <CSControl:UserData runat="server" Property="DisplayName" LinkTo="Profile" LinkTarget="_blank" />
                        </td>
                        <td class="CommonListCell">
                            <CSControl:RatingControl runat="server" RatingCssClass="ForumThreadRateControl" RatingReadOnlyCssClass="ForumThreadRateControlReadOnly" RatingActiveCssClass="ForumThreadRateControlActive" ImagesBaseUrl="~/Themes/default/images/common/" />
                        </td>
                    </tr>
            </ItemTemplate>
            <FooterTemplate>
            	</table>
            	<div class="CommonFormField">
                    <a href="javascript:self.close();" class="CommonTextButton"><CSControl:ResourceControl runat="server" ResourceName="Close_Window" /></a>
                </div>		
            </FooterTemplate>
        </CSControl:RatingList>
    </div>
</div>
</asp:Content>
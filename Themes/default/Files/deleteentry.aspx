<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Files.Controls.CSFileThemePage" MasterPageFile="files.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        if (CurrentEntry != null)
            SetTitle(ResourceManager.GetString("Delete", "FileGallery.xml") + " " + CurrentEntry.Subject, Title, true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
    <div class="CommonContentArea">
        <CSControl:WrappedLiteral runat="server" id="Title" Tag="H2" CssClass="CommonTitle" />
	    <div class="CommonContent">
	        <CSFile:DeleteEntryForm runat="server" DeleteButtonId="EntryDelete">
	            <SuccessActions>
	                <CSControl:GoToSiteUrlAction UrlName="files_ViewFolder" Parameter1='<%# CurrentSection.ApplicationKey %>' runat="server" />
	            </SuccessActions>
	            <FormTemplate>
	                <div class="CommonFormArea">
			            <CSControl:ResourceControl runat="server" ResourceName="EntryAdmin_EntrySettings" ResourceFile="FileGallery.xml" Tag="H4" CssClass="CommonFormTitle" />
			            <table cellspacing="0" cellpadding="0" border="0">
				            <tr>
					            <td class="CommonFormFieldName">
						            <CSControl:ResourceControl runat="server" ResourceName="EntryAdmin_Name" ResourceFile="FileGallery.xml" />
					            </td>
					            <td class="CommonFormField">
						            <CSFile:EntryData runat="server" Property="Subject" />
					            </td>
				            </tr>
				            <tr>
					            <td class="CommonFormFieldName">
						            <CSControl:ResourceControl runat="server" ResourceName="EntryAdmin_Desc" ResourceFile="FileGallery.xml" />
					            </td>
					            <td class="CommonFormField">
						            <CSFile:EntryData runat="server" Property="FormattedBody" />
					            </td>
				            </tr>
				            <tr>
					            <td class="CommonFormField" colspan="2">
						            <CSControl:ResourceButton id="EntryDelete" runat="server" ResourceName="Delete" ResourceFile="FileGallery.xml" />
						            <CSControl:ButtonActionForm ButtonId="EntryCancel" runat="server">
						                <Actions>
						                    <CSControl:GoToCurrentPostAction runat="server" />
						                </Actions>
                                        <FormTemplate>
    	                                    <CSControl:ResourceButton id="EntryCancel" runat="server" ResourceName="Cancel" ResourceFile="FileGallery.xml" />    					                
						                </FormTemplate>
						            </CSControl:ButtonActionForm>
					            </td>
				            </tr>
			            </table>
		            </div>
                </FormTemplate>
            </CSFile:DeleteEntryForm>
	    </div>
    </div>
</asp:Content>
<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Files.Controls.CSFileThemePage" MasterPageFile="files.Master" %>
<%@ Register TagPrefix="CSUserControl" TagName="AggregateEntryList" Src="aggregateentrylist.ascx" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Files.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        SetTitle(ResourceManager.GetString("files", "FileGallery.xml"), true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
    <div class="CommonContentArea">
	    <CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	        <DefaultContentTemplate>
	            <CSUserControl:AdTop runat="server" />
	        </DefaultContentTemplate>
	    </CSControl:AdPart>

        <CSControl:ContentPart runat="server" ContentName="FilesDefault" Tag="H2" CssClass="CommonTitle" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
            <DefaultContentTemplate>
                <CSControl:ResourceControl runat="server" ResourceName="files" ResourceFile="FileGallery.xml" />
            </DefaultContentTemplate>
        </CSControl:ContentPart>

	    <div class="CommonContent">
	        <CSUserControl:AggregateEntryList runat="server" />
	    </div>
    	
	    <CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	        <DefaultContentTemplate>
	            <CSUserControl:AdBottom runat="server" />
	        </DefaultContentTemplate>
	    </CSControl:AdPart>

    </div>		
</asp:Content>

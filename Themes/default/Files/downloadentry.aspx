<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Files.Controls.CSFileThemePage" MasterPageFile="files.Master" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        if (CurrentEntry != null)
            SetTitle(CurrentEntry.Subject, Title, true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
     <div class="CommonContentArea">
	    <CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	        <DefaultContentTemplate>
	            <CSUserControl:AdTop runat="server" />
	        </DefaultContentTemplate>
	    </CSControl:AdPart>
    
        <h2 class="CommonTitle"><CSControl:WrappedLiteral ID="Title" Runat="server" /> <CSControl:ResourceControl runat="server" ResourceName="Files_Disclaimer_Title" ResourceFile="FileGallery.xml" /></h2>
	    <div class="CommonContent">
		    <CSControl:ResourceControl runat="server" ResourceName="Files_Disclaimer_Detail" ResourceFile="FileGallery.xml" />
		    <CSFile:DownloadEntryForm runat="server" AcceptButtonId="EntryAccept" DeclineButtonId="EntryDecline">
		        <DeclinedActions>
		            <CSControl:GoToCurrentPostAction runat="server" />
		        </DeclinedActions>
		        <FormTemplate>
		            <div class="CommonFormArea">
			            <table cellspacing="0" cellpadding="0" border="0">
				            <tr>
					            <td class="CommonFormField">
						            <CSControl:ResourceButton id="EntryAccept" ResourceName="Accept" ResourceFile="FileGallery.xml" runat="server" />
						            <CSControl:ResourceButton id="EntryDecline" ResourceName="Decline" ResourceFile="FileGallery.xml" runat="server" />
					            </td>
				            </tr>
			            </table>
			        </div>
			    </FormTemplate>
			</CSFile:DownloadEntryForm>
	    </div>
    	
	    <CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	        <DefaultContentTemplate>
	            <CSUserControl:AdBottom runat="server" />
	        </DefaultContentTemplate>
	    </CSControl:AdPart>
    </div>
</asp:Content>

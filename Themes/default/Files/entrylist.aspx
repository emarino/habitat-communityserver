<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Files.Controls.CSFileThemePage" MasterPageFile="files.Master" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        if (CurrentFolder != null)
            SetTitle(CurrentFolder.Name, Title, true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
    <div class="CommonContentArea">
	    <CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	        <DefaultContentTemplate>
	            <CSUserControl:AdTop runat="server" />
	        </DefaultContentTemplate>
	    </CSControl:AdPart>

        <CSControl:WrappedLiteral runat="server" ID="Title" Tag="H2" CssClass="CommonTitle" />

	    <div class="CommonContent">
            
            <CSFile:FolderData runat="server" Property="Description" Tag="Div" CssClass="CommonDescription" />
		
		    <CSFile:TagCloud EnableNoTagsMessage="false" TagCssClasses="CommonTag6,CommonTag5,CommonTag4,CommonTag3,CommonTag2,CommonTag1" TagCloudCssClass="CommonTagCloud" runat="server" NoTagsResourceName="" />
	    
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
	            <tr>
		            <td>
		                <CSFile:FolderData runat="server" LinkCssClass="CommonImageTextButton CommonUploadButton" LinkTo="AddEntry" ResourceName="Files_Button_Upload" ResourceFile="FileGallery.xml" />
		            </td>
		            <td align="right">
		                <CSControl:Pager runat="server" ID="TopPager" Tag="Div" CssClass="CommonPagingArea" ShowTotalSummary="true" />
		            </td>
	            </tr>
            </table>

            <CSFile:EntryList runat="server" ID="AggregateEntryList">
                <QueryOverrides SortBy="Subject" PageSize="10" PagerID="PagerGroup" />
                <HeaderTemplate>
                    <div class="CommonListArea">
                    <CSControl:ResourceControl Tag="h4" CssClass="CommonListTitle" ResourceName="FileListing_Title" ResourceFile="FileGallery.xml" runat="server" />
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <thead>
                        <tr>
	                            <th class="CommonListHeaderLeftMost FileFileNameHeader"><CSControl:ResourceControl runat="server" ResourceName="Files_PortalEntryListing_FieldFileName" ResourceFile="FileGallery.xml" /></th>
	                            <th class="CommonListHeader FileFileDateHeader"><CSControl:ResourceControl runat="server" ResourceName="Files_PortalEntryListing_FieldDateAdded" ResourceFile="FileGallery.xml" /></th>
	                            <th class="CommonListHeader FileFileDownloadCountHeader"><CSControl:ResourceControl runat="server" ResourceName="Files_PortalEntryListing_FieldDownloads" ResourceFile="FileGallery.xml" /></th>
	                            <th class="CommonListHeader FileFileActionHeader">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
	                <tr>
		                <td class="CommonListCellLeftMost FileFileNameColumn">
			                <CSFile:EntryData Tag="Div" CssClass="FileFileName" Property="Subject" LinkTo="ViewEntry" runat="server">
                                <LeaderTemplate><CSFile:EntryThumbnail ImageAlign="absmiddle" runat="server" ThumbnailSize="Small" /> </LeaderTemplate>
                            </CSFile:EntryData>
			                <CSFile:EntryData Tag="Div" CssClass="FileFileDescription" Property="FormattedBody" TruncateAt="200" runat="server" />
                            <CSFile:EntryRating Tag="Div" CssClass="FileFileRating" runat="server" IsReadOnly="true" RatingCssClass="CommonRateControl" RatingReadOnlyCssClass="CommonRateControlReadOnly" RatingActiveCssClass="CommonRateControlActive" ImagesBaseUrl="~/Themes/default/images/common/" />
		                </td>
		                <td class="CommonListCell FileFileDateColumn">
		                    <CSFile:EntryData Property="PostDate" runat="server" />
		                </td>
		                <td class="CommonListCell FileFileDownloadCountColumn">
		                    <CSFile:EntryData Property="Downloads" runat="server" />
		                </td>
		                <td class="CommonListCell FileFileActionColumn">
		                    <CSFile:EntryData LinkCssClass="CommonImageTextButton CommonDownloadButton" LinkTo="Download" ResourceName="Files_Button_Download" ResourceFile="FileGallery.xml" runat="server" />

                            <CSFile:EntryData runat="server" Tag="Div" CssClass="FileFileDetail" Property="FileSize">
                                <LeaderTemplate><CSControl:ResourceControl runat="server" ResourceName="Files_PortalEntryListing_FieldFileSize" ResourceFile="FileGallery.xml" /> </LeaderTemplate>
                            </CSFile:EntryData>
		                </td>
	                </tr>
                </ItemTemplate>
                <NoneTemplate>
	                <tr>
	                <td colspan="5" class="CommonListCellLeftMost"><CSControl:ResourceControl runat="server" ResourceName="NoRecords" ResourceFile="FileGallery.xml" /></td>
	                </tr>
                </NoneTemplate>
                <FooterTemplate>
                    </table>
                    </div>
                </FooterTemplate>
            </CSFile:EntryList>

            <CSControl:PagerGroup ID="PagerGroup" PagerIds="TopPager,BottomPager" runat="server" />

            <table width="100%" border="0" cellpadding="0" cellspacing="0">
	            <tr>
		            <td>
		                <CSFile:FolderData runat="server" LinkCssClass="CommonImageTextButton CommonUploadButton" LinkTo="AddEntry" ResourceName="Files_Button_Upload" ResourceFile="FileGallery.xml" />
		            </td>
		            <td align="right">
		                <CSControl:Pager runat="server" ID="BottomPager" Tag="Div" CssClass="CommonPagingArea" ShowTotalSummary="true" />
		                <CSFile:FolderData runat="server" LinkTo="Rss" Tag="Div">
		                    <ContentTemplate><CSControl:Image ImageUrl="~/utility/images/rss.gif" BorderWidth="0" runat="server" /></ContentTemplate>
		                </CSFile:FolderData>
		            </td>
	            </tr>
            </table>
    
        </div>
    	
	    <CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	        <DefaultContentTemplate>
	            <CSUserControl:AdBottom runat="server" />
	        </DefaultContentTemplate>
	    </CSControl:AdPart>

    </div>		
</asp:Content>

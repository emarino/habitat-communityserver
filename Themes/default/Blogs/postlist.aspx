<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Blogs.Controls.CSBlogThemePage" MasterPageFile="blogs.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        if (CurrentGroup != null)
            SetTitle(CurrentGroup.Name, true);
        else
            SetTitle(ResourceManager.GetString("weblogs"), true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
<div class="CommonContentArea">
	<CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdTop runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
	<CSBlog:GroupData runat="server" Property="Name" Tag="H2" CssClass="CommonTitle" />
	<div class="CommonContent">
		<CSBlog:WeblogPostList runat="Server" >
		    <QueryOverrides PagerID="Pager" IsAggregate="true" />
	        <HeaderTemplate>
		        <ul class="BlogPostList">
	        </HeaderTemplate>
	        <ItemTemplate>
	            <CSControl:ConditionalContent runat="server">
	                <ContentConditions Operator="Not"><CSBlog:WeblogPostPropertyValueComparison runat="server" ComparisonProperty="IsExternal" Operator="IsSetOrTrue" /></ContentConditions>
                    <TrueContentTemplate><li class="BlogPostArea"></TrueContentTemplate>
	                <FalseContentTemplate><li class="BlogPostArea External"></FalseContentTemplate>
	            </CSControl:ConditionalContent>
		            <CSBlog:WeblogPostData Property="Subject" LinkTo="Post" Tag="H4" CssClass="BlogPostHeader" runat="server" />
		            <CSBlog:WeblogPostData Property="Excerpt" Tag="Div" CssClass="BlogPostContent" runat="server" />
			        <div class="BlogPostFooter">
				        <div>
				            <CSBlog:WeblogPostData Property="UserTime" LinkTo="Post" IncludeTimeInDate="true" runat="server" />
				            <CSControl:ResourceControl runat="server" ResourceName="Weblog_Aggregate_By" /> 
				            <CSBlog:WeblogPostData Property="DisplayName" LinkTo="AuthorUrl" runat="server" /> 
				            <CSControl:ResourceControl runat="server" ResourceName="To" /> 
				            <CSBlog:WeblogData Property="Name" LinkTo="HomePage" runat="server" />
				        </div>
			            <CSBlog:WeblogPostTagEditableList runat="server" EditorLinkCssClass="CommonTextButton" EditorCssClass="CommonInlineTagEditor" Tag="Div" />
			        </div>
		        </li>
	        </ItemTemplate>
	        <FooterTemplate>
		        </ul>
	        </FooterTemplate>
        </CSBlog:WeblogPostList>

        <CSControl:SinglePager runat="Server" id="Pager"  />
	</div>
	<CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdBottom runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
</div>
</asp:Content>
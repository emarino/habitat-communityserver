<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Blogs.Controls.CSBlogThemePage" MasterPageFile="blogs.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        if (CurrentGroup != null)
            SetTitle(CurrentGroup.Name, true);
        else
            SetTitle(ResourceManager.GetString("weblogs"), true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">

<div class="CommonContentArea">
	<CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdTop runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
	<CSControl:ResourceControl ResourceName="Weblog_Aggregate_Bloggers_Title" runat="server" Tag="H2" CssClass="CommonTitle" />
	<div class="CommonContent">
		<CSBlog:GroupList runat="server">
			<ItemTemplate>
				<div class="CommonListArea">
				    <CSBlog:GroupData Property="Name" Tag="H4" CssClass="CommonListTitle" runat="server" />
				
					<CSBlog:WeblogList runat="server">
					    <QueryOverrides PagerID="Pager" PageSize="10" />
						<HeaderTemplate>
							<table width="100%" cellpadding="0" cellspacing="0">
								<thead>
									<tr>
										<th class="CommonListHeaderLeftMost"><CSControl:ResourceControl runat="server" ResourceName="Weblog_Aggregate_Blog" /></th>
										<th class="CommonListHeader"><CSControl:ResourceControl runat="server" ResourceName="Weblog_Aggregate_Author" /></th>
										<th class="CommonListHeader" nowrap="nowrap"><CSControl:ResourceControl runat="server" ResourceName="Weblog_Aggregate_LastPost" /></th>    
										<th class="CommonListHeader" align="center" nowrap="nowrap"><CSControl:ResourceControl runat="server" ResourceName="Weblog_Aggregate_Posts" /></th>                                    
										<th class="CommonListHeader" align="center" nowrap="nowrap"><CSControl:ResourceControl runat="server" ResourceName="Weblog_Aggregate_Articles" /></th>
										<th class="CommonListHeader" align="center" nowrap="nowrap"><CSControl:ResourceControl runat="server" ResourceName="Weblog_Aggregate_Comments" /></th>				
										<th class="CommonListHeader" align="center" nowrap="nowrap"><CSControl:ResourceControl runat="server" ResourceName="Weblog_Aggregate_Trackbacks" /></th>
									</tr>
								</thead>
								<tbody>
						</HeaderTemplate>
						<ItemTemplate>
    							<tr class="CommonListRow">
									<td class="CommonListCellLeftMost BlogBlogNameColumn">
									    <CSBlog:WeblogData Property="Name" Tag="B" LinkTo="HomePage" runat="server" /><br />
									    <CSBlog:WeblogData Property="Description" runat="server" />
									</td>
									<td class="CommonListCell BlogAuthorColumn">
									    <CSControl:UserList runat="server">
                                            <ItemTemplate><CSControl:UserData Property="DisplayName" LinkTo="Profile" runat="server" /></ItemTemplate>
                                            <SeparatorTemplate>, </SeparatorTemplate>
									    </CSControl:UserList>
									    &nbsp;
									</td>
									<td class="CommonListCell BlogRecentPostColumn">
									    <CSBlog:WeblogData Property="MostRecentPostSubject" LinkTo="MostRecentPost" runat="server" />&nbsp;
									</td>
									<td class="CommonListCell BlogPostsColumn">
									    <CSBlog:WeblogData Property="PostCount" runat="server" />&nbsp;
									</td>                   						
									<td class="CommonListCell BlogArticlesColumn">
									    <CSBlog:WeblogData Property="ArticleCount" runat="server" />&nbsp;
									</td>
									<td class="CommonListCell BlogCommentsColumn">
									    <CSBlog:WeblogData Property="CommentCount" runat="server" />&nbsp;
									</td>
									<td class="CommonListCell BlogTrackbacksColumn">
									    <CSBlog:WeblogData Property="TrackbackCount" runat="server" />&nbsp;
									</td>														
								</tr>
						</ItemTemplate>
						<AlternatingItemTemplate>
    							<tr class="CommonListRowAlt">
									<td class="CommonListCellLeftMost BlogBlogNameColumn">
									    <CSBlog:WeblogData Property="Name" Tag="B" LinkTo="HomePage" runat="server" /><br />
									    <CSBlog:WeblogData Property="Description" runat="server" />
									</td>
									<td class="CommonListCell BlogAuthorColumn">
									    <CSControl:UserList runat="server">
                                            <ItemTemplate><CSControl:UserData Property="DisplayName" LinkTo="Profile" runat="server" /></ItemTemplate>
                                            <SeparatorTemplate>, </SeparatorTemplate>
									    </CSControl:UserList>
									    &nbsp;
									</td>
									<td class="CommonListCell BlogRecentPostColumn">
									    <CSBlog:WeblogData Property="MostRecentPostSubject" LinkTo="MostRecentPost" runat="server" />&nbsp;
									</td>
									<td class="CommonListCell BlogPostsColumn">
									    <CSBlog:WeblogData Property="PostCount" runat="server" />&nbsp;
									</td>                   						
									<td class="CommonListCell BlogArticlesColumn">
									    <CSBlog:WeblogData Property="ArticleCount" runat="server" />&nbsp;
									</td>
									<td class="CommonListCell BlogCommentsColumn">
									    <CSBlog:WeblogData Property="CommentCount" runat="server" />&nbsp;
									</td>
									<td class="CommonListCell BlogTrackbacksColumn">
									    <CSBlog:WeblogData Property="TrackbackCount" runat="server" />&nbsp;
									</td>														
								</tr>
						</AlternatingItemTemplate>
						<FooterTemplate>
								</tbody>
							</table>        
						</FooterTemplate>
					</CSBlog:WeblogList>

					<CSControl:PostbackPager runat="server" id="Pager" Tag="Div" CssClass="CommonPagingArea" />
				</div>
			</ItemTemplate>
		</CSBlog:GroupList>
	</div>
	<CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdBottom runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>	
</div>

</asp:Content>
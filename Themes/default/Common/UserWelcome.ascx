<%@ Control Language="C#" %>

<CSControl:UserData runat="server" UseAccessingUser="true">
    <DisplayConditions><CSControl:UserPropertyValueComparison runat="server" UseAccessingUser="true" ComparisonProperty="IsAnonymous" Operator="IsSetOrTrue" /></DisplayConditions>
    <ContentTemplate>
        <CSControl:UserData runat="server" LinkTo="Login" ResourceName="A_Login" />
         <CSControl:SiteUrl runat="server" UrlName="user_Register" Parameter1="" ResourceName="register">
            <DisplayConditions Operator="Not"><CSControl:SiteSettingsPropertyValueComparison ComparisonProperty="AccountActivation" ComparisonValue="InvitationOnly" Operator="EqualTo" runat="server" /></DisplayConditions>
            <LeaderTemplate>| </LeaderTemplate>
        </CSControl:SiteUrl>
        <CSControl:UserData runat="server" ResourceName="help" LinkTo="FAQ"><LeaderTemplate>| </LeaderTemplate></CSControl:UserData>
     </ContentTemplate>
</CSControl:UserData>

<CSControl:UserData runat="server" UseAccessingUser="true">
    <DisplayConditions Operator="Not"><CSControl:UserPropertyValueComparison runat="server" UseAccessingUser="true" ComparisonProperty="IsAnonymous" Operator="IsSetOrTrue" /></DisplayConditions>
    <ContentTemplate>
        <CSControl:UserData runat="server" LinkTo="Profile" Property="DisplayName" />
        <CSControl:UserData runat="server" LinkTo="EditProfile" ResourceName="BreadCrumb_EditUserProfile"><LeaderTemplate>| </LeaderTemplate></CSControl:UserData>
        <CSControl:SiteSettingsData runat="server">
            <DisplayConditions Operator="Not">
                <CSControl:SiteSettingsPropertyValueComparison runat="server" ComparisonProperty="AuthenticationType" Operator="EqualTo" ComparisonValue="windows" />
                <CSControl:SiteSettingsPropertyValueComparison runat="server" ComparisonProperty="AuthenticationType" Operator="EqualTo" ComparisonValue="passport" />
            </DisplayConditions>
            <ContentTemplate>
                <CSControl:UserData runat="server" LinkTo="Logout" ResourceName="logout"><LeaderTemplate>| </LeaderTemplate></CSControl:UserData>
            </ContentTemplate>
        </CSControl:SiteSettingsData>
        <CSControl:SiteUrl runat="server" UrlName="user_Invite" ResourceName="invite">
            <DisplayConditions><CSControl:SiteSettingsPropertyValueComparison ComparisonProperty="AccountActivation" ComparisonValue="InvitationOnly" Operator="EqualTo" runat="server" /></DisplayConditions>
            <LeaderTemplate>| </LeaderTemplate>
        </CSControl:SiteUrl>
        <CSControl:UserData runat="server" ResourceName="help" LinkTo="FAQ"><LeaderTemplate>| </LeaderTemplate></CSControl:UserData>
        <CSControl:UserData runat="server" LinkTo="UserPrivateMessages" ResourceName="PrivateMessage_Unread"><LeaderTemplate>| </LeaderTemplate></CSControl:UserData>
    </ContentTemplate>
</CSControl:UserData>
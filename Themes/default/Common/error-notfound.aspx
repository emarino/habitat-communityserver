<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Controls.CSThemePage" MasterPageFile="master.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        Page.Response.StatusCode = 404;
        SetTitle(ResourceManager.GetString("Error_NotFound_Title"), true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">

	<div align=center>
		<div class="CommonMessageArea">
			<h4 class="CommonMessageTitle"><CSControl:ResourceControl runat="server" ResourceName="Error_NotFound_Title" /></h4>
			<div class="CommonMessageContent">
				<div class="CommonFormArea">
					<CSControl:ResourceControl ResourceName="Error_NotFound_Description" runat="server" />
					<p />
					<CSControl:SearchForm runat="server" 
                        QueryTextBoxId="TitleBarSearchText" 
                        SubmitButtonId="TitleBarSearchButton"
                        >
                        <FormTemplate>
                            <table cellpadding="0" cellspacing="0" border="0" align="center"><tr valign="middle"><td nowrap="nowrap">
                                <CSControl:DefaultButtonTextBox id="TitleBarSearchText" runat="server" columns="45" maxlength="128" ButtonId="TitleBarSearchButton" />
                            </td><td>
                                <span class="CommonSearchButtonOuter"><CSControl:ResourceLinkButton id="TitleBarSearchButton" runat="server" CssClass="CommonSearchButton" ResourceName="Search" /></span>
                            </td></tr></table>
                        </FormTemplate>
                    </CSControl:SearchForm>
				</div>
			</div>
		</div>
	</div>

</asp:Content>
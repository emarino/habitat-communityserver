<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Controls.CSThemePage" MasterPageFile="master.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        SetTitle(CurrentCSContext.SiteSettings.SiteName, false);
        CSControlUtility.Instance().FindTextControl(this, "LogOutMessage").Text = string.Format(ResourceManager.GetString("Logout_Status"), CurrentCSContext.SiteSettings.SiteName);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
<CSControl:LogOutForm runat="server">
    <SuccessActions>
        <CSControl:GoToModifiedUrlAction runat="server" />
    </SuccessActions>
    <FormTemplate>
        <div align=center>
            <div class="CommonMessageArea">
                <CSControl:ResourceControl runat="server" ResourceName="Logout_Title" Tag="H4" CssClass="CommonMessageTitle" />
                <div class="CommonMessageContent">
                    <CSControl:WrappedLiteral id="LogOutMessage" runat="server" />
                    <p />
                    <div>
                        <CSControl:SiteUrl runat="server" UrlName="login_clean" LinkCssClass="CommonTextButton Big" ResourceName="login" />
                        <CSControl:SiteUrl runat="server" UrlName="home" LinkCssClass="CommonTextButton Big" ResourceName="Logout_ReturnHome" />
                    </div>
                </div>
            </div>
        </div>
    </FormTemplate>
</CSControl:LogOutForm>

</asp:Content>


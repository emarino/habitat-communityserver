<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Controls.CSThemePage" MasterPageFile="master.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        SetTitle(CurrentCSContext.SiteSettings.SiteName, false);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
<CSControl:LoginForm runat="server" AutoLoginCheckBoxId="autoLogin" UserNameTextBoxId="username" PasswordTextBoxId="password" LoginButtonId="loginButton">
    <SuccessActions>
        <CSControl:GoToReferralUrlAction runat="server" />
        <CSControl:GoToSiteUrlAction UrlName="home" runat="server" />
    </SuccessActions>
    <FormTemplate>
        <div align="center">
        <div class="CommonMessageArea">
	        <h4 class="CommonMessageTitle"><CSControl:ResourceControl runat="server" ResourceName="Login_Title" /></h4>
	        <div class="CommonMessageContent">
		        <div class="CommonFormArea">
		        <table cellspacing="1" border="0" cellpadding="5">
			        <tr>
				        <td align="right" class="CommonFormFieldName">
					        <CSControl:FormLabel LabelForId="username" runat="server" ResourceName="Login_UserName" />
				        </td>
				        <td class="CommonFormField">
					        <CSControl:DefaultButtonTextBox CssClass="CommonTextBig" ButtonId="loginButton" id="username" runat="server" Columns="30" maxlength="64" />
				        </td>
			        </tr>
			        <tr>
				        <td align="right" class="CommonFormFieldName">
					        <CSControl:FormLabel LabelForId="password" runat="server" ResourceName="Login_Password" />
				        </td>
				        <td class="CommonFormField">
					        <CSControl:DefaultButtonTextBox CssClass="CommonTextBig" ButtonId="loginButton" TextMode="Password" id="password" runat="server" Columns="11" maxlength="64" />
					        <span class="txt4">(<CSControl:SiteUrl UrlName="user_ForgotPassword" ResourceName="Utility_ForumAnchorType_UserForgotPassword" runat="server" />)</span>
				        </td>
			        </tr>
			        <tr>
				        <td></td>
				        <td align="left" class="CommonFormField" nowrap="nowrap">
					        <asp:CheckBox type="checkbox" Checked="true" runat="server" id="autoLogin" /> <CSControl:FormLabel LabelForId="autologin" runat="server" ResourceName="LoginSmall_AutoLogin" />
				        </td>
			        </tr>
			        <tr>
				        <td></td>
				        <td align="left" class="CommonFormField" nowrap="nowrap">
					        <CSControl:ResourceLinkButton id="loginButton" runat="server" CssClass="CommonTextButton Big" ResourceName="LoginSmall_Button" />
				        </td>
			        </tr>
		        </table>
		        </div>
	        </div>
	        <CSControl:ResourceControl runat="server" ResourceName="Login_NotMemberYet" />
		    <CSControl:SiteUrl runat="server" UrlName="user_Register" ResourceName="Login_JoinLink" />
        </div>
        </div>
    </FormTemplate>
</CSControl:LoginForm>

</asp:Content>

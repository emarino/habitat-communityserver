<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Controls.CSThemePage" MasterPageFile="../Common/master.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        SetTitle(ResourceManager.GetString("SearchAdvanced_Title"), true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">

<script type="text/javascript">
// <![CDATA[
var prevDateComparerIndex = 0;

function DateComparerChange() 
{
	var dateComparerIndex = document.getElementById('<%=CSControlUtility.Instance().FindControl(this, "DateComparer").ClientID%>').selectedIndex;

	if (dateComparerIndex == 0 || prevDateComparerIndex == -1)
		document.getElementById('StartDateContainer').style.display = 'none';
	else if (prevDateComparerIndex == 0)
		document.getElementById('StartDateContainer').style.display = 'inline';
	
	if (prevDateComparerIndex == 2 || prevDateComparerIndex == -1)
		document.getElementById('EndDateContainer').style.display = 'none';
	else if (dateComparerIndex == 2)
		document.getElementById('EndDateContainer').style.display = 'inline';

	prevDateComparerIndex = dateComparerIndex;
}

// ]]>
</script>

<div class="CommonContentArea">
	<CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdTop runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
	
	<h2 class="CommonTitle"><CSControl:ResourceControl ResourceName="SearchAdvanced_Title" runat="server" /></h2>
	<div class="CommonContent">
		<div class="CommonDescription"><CSControl:ResourceControl ResourceName="SearchAdvanced_Description" runat="server" /></div>
		
		<CSControl:SearchForm runat="server" 
		    CustomValidatorId="Validator" 
		    DateComparerDropDownListId="DateComparer" 
		    EndDateTimeSelectorId="EndDate" 
		    QueryTextBoxId="Keywords" 
		    SectionTreeViewId="" 
		    SortByDropDownListId="SortBy" 
		    StartDateTimeSelectorId="StartDate" 
		    SubmitButtonId="SearchButton" 
		    TagsTextBoxId="Tags" 
		    UsersTextBoxId="Users">
            <FormTemplate>
		
		        <asp:CustomValidator Runat="server" id="Validator" Display="Dynamic" />

		        <div class="CommonFormArea">
		        <table cellpadding="0" cellspacing="0" border="0">

		        <tr>
			        <td class="CommonFormFieldName">
				        <CSControl:HelpToolTip runat="server" PopupActivatorCssClass="CommonHelpIcon" PopupActivatorHoverCssClass="CommonHelpIconHover" ToolTipCssClass="CommonHelpToolTip" ToolTipResourceName="SearchAdvanced_Keywords_Info" />
				        <CSControl:ResourceControl runat="server" ResourceName="SearchAdvanced_Keywords" />
			        </td>
			        <td class="CommonFormField">
				        <asp:textbox id="Keywords" runat="server" columns="60" />
			        </td>
		        </tr>
        		
		        <tr>
			        <td class="CommonFormFieldName">
			            <CSControl:HelpToolTip runat="server" PopupActivatorCssClass="CommonHelpIcon" PopupActivatorHoverCssClass="CommonHelpIconHover" ToolTipCssClass="CommonHelpToolTip" ToolTipResourceName="SearchAdvanced_Tags_Info" />
				        <CSControl:ResourceControl runat="server" ResourceName="SearchAdvanced_Tags" />
			        </td>
			        <td class="CommonFormField">
				        <asp:TextBox Runat="server" ID="Tags" Columns="60" />
			        </td>
		        </tr>

		        <tr>
			        <td class="CommonFormFieldName">
				        <CSControl:HelpToolTip runat="server" PopupActivatorCssClass="CommonHelpIcon" PopupActivatorHoverCssClass="CommonHelpIconHover" ToolTipCssClass="CommonHelpToolTip" ToolTipResourceName="SearchAdvanced_Users_Info" />
				        <CSControl:ResourceControl runat="server" ResourceName="SearchAdvanced_Users" />
			        </td>
			        <td class="CommonFormField">
				        <asp:textbox id="Users" runat="server" columns="60" />
			        </td>
		        </tr>
        		
		        <tr>
			        <td class="CommonFormFieldName">
				        <CSControl:HelpToolTip runat="server" PopupActivatorCssClass="CommonHelpIcon" PopupActivatorHoverCssClass="CommonHelpIconHover" ToolTipCssClass="CommonHelpToolTip" ToolTipResourceName="SearchAdvanced_Date_Info" />
				        <CSControl:ResourceControl runat="server" ResourceName="SearchAdvanced_Date" />
			        </td>
			        <td class="CommonFormField">
				        <asp:dropdownlist id="DateComparer" runat="server" />
				        <span id="StartDateContainer" style="display: none;">
    				        <TWC:DateTimeSelector runat="server" ID="StartDate" DateTimeFormat="MMMM d yyyy" ShowCalendarPopup="true" />
				        </span>

				        <span id="EndDateContainer" style="display: none;">
				            <CSControl:ResourceControl ResourceName="SearchAdvanced_Date_Range_And" runat="server" />
				            <TWC:DateTimeSelector runat="server" ID="EndDate" DateTimeFormat="MMMM d yyyy" ShowCalendarPopup="true" />
				        </span>
			        </td>
		        </tr>
        		
		        <tr>
			        <td class="CommonFormFieldName">
			            <CSControl:HelpToolTip runat="server" PopupActivatorCssClass="CommonHelpIcon" PopupActivatorHoverCssClass="CommonHelpIconHover" ToolTipCssClass="CommonHelpToolTip" ToolTipResourceName="SearchAdvanced_Sort_Info" />
				        <CSControl:ResourceControl runat="server" ResourceName="SearchAdvanced_Sort" ID="Resourcelabel3"/>
			        </td>
			        <td class="CommonFormField">
				        <asp:DropDownList ID="SortBy" Runat="server" />
			        </td>
		        </tr>			
        		
		        <tr>
			        <td class="CommonFormField" colspan="2">
				        <CSControl:ResourceLinkButton ResourceName="Search" id="SearchButton" runat="server" CssClass="CommonTextButton" />
			        </td>
		        </tr>
	        </table>
	        </div>
            </FormTemplate>
        </CSControl:SearchForm>
    </div>

	<CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdBottom runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
</div>

<script type="text/javascript">
// <![CDATA[
document.getElementById('<%=CSControlUtility.Instance().FindControl(this, "DateComparer").ClientID%>').onchange = DateComparerChange;
DateComparerChange();
// ]]>
</script>

</asp:Content>

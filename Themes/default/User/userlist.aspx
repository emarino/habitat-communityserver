<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Controls.CSThemePage" MasterPageFile="../Common/master.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        SetTitle(ResourceManager.GetString("CommunityServerMembers_Inline1"), Title, true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">

<script type="text/javascript">
// <![CDATA[
prevJoinedDateIndex = 0;
prevPostDateIndex = 0;

function LastPostDateComparerChange()
{
    var dateComparerIndex = document.getElementById('<%=CSControlUtility.Instance().FindControl(this, "lastPostDateComparer").ClientID%>').selectedIndex;
    if (dateComparerIndex == 0)
        document.getElementById('lastPostDateContainer').style.display = 'none';
    else
        document.getElementById('lastPostDateContainer').style.display = 'inline';
}

function JoinedDateComparerChange()
{
    var dateComparerIndex = document.getElementById('<%=CSControlUtility.Instance().FindControl(this, "joinedDateComparer").ClientID%>').selectedIndex;
    if (dateComparerIndex == 0)
        document.getElementById('joinedDateContainer').style.display = 'none';
    else
        document.getElementById('joinedDateContainer').style.display = 'inline';
}
// ]]>
</script>

<div class="CommonContentArea">
<h2 class="CommonTitle"><CSControl:WrappedLiteral id="Title" runat="server" /></h2>
<div class="CommonContent">
	<div class="CommonDescription"><CSControl:ResourceControl runat="server" ResourceName="CommunityServerMembers_Inline2" /></div>
	
	<CSControl:UserSearchForm runat="server" 
	    CurrentAccountStatusDropDownListId="currentAccountStatus" 
	    JoinedDateTimeSelectorId="joinedDate" 
	    JoinedDateComparerDropDownListId="joinedDateComparer" 
	    LastPostDateTimeSelectorId="lastPostDate" 
	    LastPostDateComparerDropDownListId="lastPostDateComparer" 
	    MemberSortDropDownListId="memberSortDropDown" 
	    QueryTextBoxId="searchText" 
	    SearchRoleDropDownListId="searchRole" 
	    SearchTypeDropDownListId="searchType" 
	    SortOrderDropDownListId="sortOrderDropDown" 
	    SubmitButtonId="searchButton"
	    >
	    <FormTemplate>
	        <div class="CommonFormArea">
                <table border="0" cellpadding="2" cellspacing="0">
                    <tr>
	                    <td align="right" class="CommonFormFieldName">
		                    <CSControl:HelpToolTip runat="server" PopupActivatorCssClass="CommonHelpIcon" PopupActivatorHoverCssClass="CommonHelpIconHover" ToolTipCssClass="CommonHelpToolTip" ToolTipResourceName="UserSearchControl_Search_Desc" />
	                    </td>
	                    <td class="CommonFormField">
		                    <asp:textbox id="searchText" runat="server" Columns="75" maxlength="255" />
	                    </td>
	                    <td align="right" class="CommonFormFieldName">
		                    <CSControl:ResourceLinkButton id="searchButton" runat="server" Cssclass="CommonTextButton" ResourceName="Search" />
                        </td>
                    </tr>
                </table>

                <p />

                <fieldset>
                    <legend><CSControl:ResourceControl runat="server" resourcename="UserSearchControl_SortBy" /></legend>
                    <table border="0" cellpadding="2" cellspacing="0">
                        <tr>
	                        <td colspan="3" class="CommonFormField">
		                        <asp:DropDownList id="memberSortDropDown" runat="server" />
		                        <asp:DropDownList id="sortOrderDropDown" runat="server" />
	                        </td>
                        </tr>
                    </table>
                </fieldset>

                <p />
                <fieldset>
                    <legend><CSControl:ResourceControl runat="server" resourcename="UserSearchControl_FilterByDate" /></legend>
                    <table border="0" cellpadding="2" cellspacing="0">
                        <tr>
	                        <td align="right" class="CommonFormFieldName"><CSControl:ResourceControl  runat="server" resourcename="UserSearchControl_JoinedDate" /></td>
	                        <td class="CommonFormField" nowrap="nowrap">
		                        <asp:dropdownlist id="joinedDateComparer" runat="server" />
		                        <span id="joinedDateContainer">
		                            <TWC:DateTimeSelector runat="server" ID="joinedDate" DateTimeFormat="MMMM d yyyy" ShowCalendarPopup="true" />
                                </span>
	                        </td>
	                      </tr>
	                      <tr>
	                        <td align="right" class="CommonFormFieldName"><CSControl:ResourceControl runat="server" resourcename="UserSearchControl_LastPostDate" /></td>
	                        <td class="CommonFormField" nowrap="nowrap">
		                        <asp:dropdownlist id="lastPostDateComparer" runat="server" />
		                        <span id="lastPostDateContainer">
		                            <TWC:DateTimeSelector runat="server" ID="lastPostDate" DateTimeFormat="MMMM d yyyy" ShowCalendarPopup="true" />
                                </span>
	                        </td>
                        </tr>
                    </table>
                </fieldset>

                <p />
                <fieldset>
                    <legend><CSControl:ResourceControl runat="server" resourcename="UserSearchControl_FilterBy" /></legend>
                    <table border="0" cellpadding="2" cellspacing="0">
	                    <tr>
	                        <td align="right" class="CommonFormFieldName">
                                <CSControl:ResourceControl runat="server" resourcename="UserSearchControl_Role" />
                            </td>
	                        <td class="CommonFormField">
	                            <asp:DropDownList id="searchRole" runat="server" />
	                        </td>
	                    </tr>
	                    <CSControl:PlaceHolder runat="server">
	                        <DisplayConditions>
	                            <CSControl:UserPropertyValueComparison runat="server" UseAccessingUser="true" ComparisonProperty="IsAdministrator" Operator="IsSetOrTrue" />
	                            <CSControl:UserPropertyValueComparison runat="server" UseAccessingUser="true" ComparisonProperty="IsMembershipAdministrator" Operator="IsSetOrTrue" />
	                        </DisplayConditions>
	                        <ContentTemplate>
                                <tr>
	                                <td align="right" class="CommonFormFieldName">
		                                <CSControl:HelpToolTip runat="server" PopupActivatorCssClass="CommonHelpIcon" PopupActivatorHoverCssClass="CommonHelpIconHover" ToolTipCssClass="CommonHelpToolTip" ToolTipResourceName="UserSearchControl_SearchFor_Desc" />
		                                <CSControl:ResourceControl runat="server" ResourceName="UserSearchControl_SearchFor" />
	                                </td>
	                                <td class="CommonFormField">
        		                        <asp:DropDownList id="searchType" runat="server" align="absmiddle" />
	                                </td>
	                            </tr>
	                        </ContentTemplate>
	                    </CSControl:PlaceHolder>
	                    <tr>
	                        <td align="right" class="CommonFormFieldName">
		                        <CSControl:HelpToolTip runat="server" PopupActivatorCssClass="CommonHelpIcon" PopupActivatorHoverCssClass="CommonHelpIconHover" ToolTipCssClass="CommonHelpToolTip" ToolTipResourceName="UserSearchControl_Status_Desc" />
		                        <CSControl:ResourceControl runat="server" resourcename="UserSearchControl_Status" />
	                        </td>
        	                <td class="CommonFormField"><asp:DropDownList id="currentAccountStatus" runat="server" /></td>
                        </tr>
                    </table>
                </fieldset>
	        </div>
        </FormTemplate>
	</CSControl:UserSearchForm>
    
	<CSControl:UserList runat="server">
	    <QueryOverrides QueryType="Search" PagerID="Pager" />
	    <DisplayConditions Operator="Or">
	        <CSControl:QueryStringPropertyValueComparison runat="server" QueryStringProperty="search" Operator="EqualTo" ComparisonValue="1" runat="server" />
	    </DisplayConditions>
	    <HeaderTemplate>
		    <div class="CommonListArea">
		    <table cellspacing="0" cellpadding="0" width="100%">
		    <thead>
		    <tr>
			    <th class="CommonListHeaderLeftMost" nowrap="nowrap">
				    <CSControl:ResourceControl runat="server" ResourceName="ForumMembers_Username" />
			    </th>
			    <th class="CommonListHeader" align="center" nowrap="nowrap">
				    <CSControl:ResourceControl runat="server" ResourceName="ForumMembers_PrivateMessages" />
			    </th>
			    <th class="CommonListHeader" align="center" nowrap="nowrap">
				    <CSControl:ResourceControl runat="server" ResourceName="ForumMembers_Home" />
			    </th>
			    <th class="CommonListHeader" align="center" nowrap="nowrap">
				    <CSControl:ResourceControl runat="server" ResourceName="ForumMembers_WebLog" />
			    </th>
			    <th class="CommonListHeader" align="center" nowrap="nowrap">
				    <CSControl:ResourceControl runat="server" ResourceName="ForumMembers_Joined" />
			    </th>
			    <th class="CommonListHeader" align="center" nowrap="nowrap">
				    <CSControl:ResourceControl runat="server" ResourceName="ForumMembers_LastActive" />
			    </th>
			    <th class="CommonListHeader" align="center" nowrap="nowrap">
				    <CSControl:ResourceControl runat="server" ResourceName="ForumMembers_TotalPosts" />
			    </th>
		    </tr>
		    </thead>
		    <tbody>
	    </HeaderTemplate>    
	    <ItemTemplate>
	        <CSControl:ConditionalContent runat="server">
	            <ContentConditions Operator="not">
	                <CSControl:UserPropertyValueComparison runat="server" ComparisonProperty="IsAnonymous" Operator="IsSetOrTrue" />
	            </ContentConditions>
	            <TrueContentTemplate>
	                <tr>
			            <td class="CommonListCellLeftMost">
				            <div><CSControl:UserData Property="DisplayName" LinkTo="Profile" runat="server" /></div>
				            <div><CSControl:UserProfileData Property="Location" runat="server" /></div>
			            </td>
			            <td align="center" class="CommonListCell">
			                <CSControl:UserData ResourceName="Button_PrivateMessage" LinkTo="PrivateMessage" LinkCssClass="CommonImageTextButton CommonPrivateMessageButton" runat="server" />&nbsp;
			            </td>
			            <td align="center" class="CommonListCell">
			                <CSControl:UserData ResourceName="Button_Homepage" LinkTo="WebAddress" LinkCssClass="CommonImageTextButton CommonHomepageButton" runat="server" />&nbsp;
			            </td>
			            <td align="center" class="CommonListCell">
			                <CSControl:UserData ResourceName="Button_Weblog" LinkTo="WebLog" LinkCssClass="CommonImageTextButton CommonWeblogButton" runat="server" />&nbsp;
			            </td>
			            <td align="center" class="CommonListCell">
			                <CSControl:UserData Property="DateCreated" runat="server" />&nbsp;
			            </td>
			            <td align="center" class="CommonListCell">
			                <CSControl:UserData Property="LastActivity" runat="server" />&nbsp;
			            </td>
			            <td align="center" class="CommonListCell">
			                <CSControl:UserData LinkTo="PostsSearch" Property="TotalPosts" runat="server" />&nbsp;
			            </td>
		            </tr>
	            </TrueContentTemplate>
	        </CSControl:ConditionalContent>
	    </ItemTemplate>
	    <NoneTemplate>
		    <tr>
			    <td align="left" class="CommonListCellLeftMost" colspan="8">
				    <CSControl:ResourceControl runat="server" ResourceName="ForumMembers_NoRecords" />
			    </td>
		    </tr>
	    </NoneTemplate>
	    <FooterTemplate>
		    </tbody>
  		    </table>
  		    </div>
	    </FooterTemplate>
	</CSControl:UserList>

    <CSControl:PostbackPager runat="server" ID="Pager" CssClass="CommonPagingArea" Tag="Div" ShowTotalSummary="true">
        <DisplayConditions>
            <CSControl:QueryStringPropertyValueComparison runat="server" QueryStringProperty="search" Operator="EqualTo" ComparisonValue="1" runat="server" />
        </DisplayConditions>
    </CSControl:PostbackPager>
</div>
</div>

<script type="text/javascript">
// <![CDATA[
document.getElementById('<%=CSControlUtility.Instance().FindControl(this, "lastPostDateComparer").ClientID%>').onchange = LastPostDateComparerChange;
document.getElementById('<%=CSControlUtility.Instance().FindControl(this, "joinedDateComparer").ClientID%>').onchange = JoinedDateComparerChange;
LastPostDateComparerChange();
JoinedDateComparerChange();
// ]]>
</script>

</asp:Content>


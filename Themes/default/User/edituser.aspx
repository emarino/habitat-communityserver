<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Controls.CSThemePage" MasterPageFile="../Common/master.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="System.Web.UI.WebControls" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        SetTitle(CurrentCSContext.User.DisplayName, true);


        ((BaseValidator) CSControlUtility.Instance().FindControl(this, "editNameRegExVldt")).ErrorMessage = ResourceManager.GetString("EditProfile_WebAddress_ErrorMessage");
        ((BaseValidator) CSControlUtility.Instance().FindControl(this, "editNameRegExVldt2")).ErrorMessage = ResourceManager.GetString("EditProfile_Blog_ErrorMessage");
        ((BaseValidator) CSControlUtility.Instance().FindControl(this, "editNameRegExVldt3")).ErrorMessage = ResourceManager.GetString("EditProfile_Gallery_ErrorMessage");
        ((BaseValidator) CSControlUtility.Instance().FindControl(this, "emailValidator")).ErrorMessage = ResourceManager.GetString("EditProfile_EmailFormat_ErrorMessage");
        ((BaseValidator) CSControlUtility.Instance().FindControl(this, "emailRegExValidator")).ErrorMessage = ResourceManager.GetString("EditProfile_EmailRequired_ErrorMessage");
        ValidationSummary.HeaderText = ResourceManager.GetString("EditProfile_ErrorMessages_Header");
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">

<div class="CommonContentArea">
	<CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdTop runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
	<div class="CommonContent">
	
	    <CSControl:ResourceControl runat="server" Tag="Div" CssClass="CommonMessageSuccess" ResourceName="EditProfile_UpdateSuccess">
	        <DisplayConditions><CSControl:QueryStringPropertyValueComparison runat="server" QueryStringProperty="ProfileSaved" Operator="EqualTo" ComparisonValue="true" /></DisplayConditions>
	    </CSControl:ResourceControl>
		
		<asp:ValidationSummary Runat="server" ID="ValidationSummary" DisplayMode="BulletList" Enabled="True" EnableClientScript="True" />
		
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
			<tr valign="middle">
				<td style="padding-top: 8px; padding-bottom: 8px;">
					<CSControl:UserAvatar runat="server" UseAccessingUser="true" style="padding-right: 8px;" />
				</td>
				<td width="100%">
				    <CSControl:UserData Property="DisplayName" Tag="H2" CssClass="CommonProfileTitle" runat="server" UseAccessingUser="true" />
				</td>
			</tr>
		</table>
		
		<CSControl:EditUserForm runat="server" 
		    RssFeedUrlsTextBoxId="rssFeeds"
		    EnableUserAvatarsCheckBoxId="enableUserAvtars" 
		    TimeZoneDropDownListId="Timezone"
            NameTextBoxId="CommonName"
            LocationTextBoxId="Location"
            OccupationTextBoxId="Occupation"
            InterestsTextBoxId="Interests"
            WebAddressTextBoxId="WebAddress"
            WeblogTextBoxId="WebLog"
		    WebGalleryTextBoxId="WebGallery"
            SignatureTextBoxId="Signature"
            BioTextBoxId="bio"
            PrivateEmailTextBoxId="PrivateEmail"
            PublicEmailTextBoxId="PublicEmail"
            MsnIMTextBoxId="MsnIM"
            AolIMTextBoxId="AolIM"
            YahooIMTextBoxId="YahooIM"
            IcqIMTextBoxId="ICQ"
            UsernameTextBoxId=""
            HideReadPostsCheckBoxId=""
            EnableEmailTrackingCheckBoxId="EnableEmailTracking"
            DisplayInMemberListCheckBoxId="DisplayInMemberList"
            EnableHtmlEmailCheckBoxId="EnableHtmlEmail"
            EnableEmailCheckBoxId="ReceiveEmails"
		    EnableInkCheckBoxId="enableInk"
            EnableDisplayNameCheckBoxId="enableDisplayNames"
            EnableUserSignatureCheckBoxId="enableUserSignatures"
            GenderRadioButtonListId="Gender"
            LanguageDropDownListId="Language"
            DateFormatDropDownListId="DateFormat"
            DisableBirthdayRadioButtonId="DisableBirthday"
		    EnableBirthdayRadioButtonId="EnableBirthday"
		    BirthdayDateTimeSelectorId="Birthday"
            DefaultThreadDateFilterDropDownListId=""
            SignatureMaxLengthCustomValidatorId="SignatureMaxLengthValidator"
            EditUserCustomValidatorId="FormValidator"
            ThemeDropDownListId="Theme"
            EnablePostPreviewPopupCheckBoxId="EnablePostPreviewPopup"
            EnableEmoticonsCheckBoxId="EnableEmoticons"
            EnableCollapsingPanelsCheckBoxId="EnableCollapsingPanels"
            FontSizeDropDownListId="FontSize"
            DefaultPostSortOrderDropDownListId="SortOrder"
            EditorDropDownListId="EditorList"
		    DefaultThreadViewDropDownListId="PostViewType"
		    EnableHelpCheckBoxId="ynEnableHelp"
		    ControlPanelPageSizeDropDownListId="PageSize"
		    ControlPanelManageViewDropDownListId="ControlPanelManageView"
		    SectionFilterLanguageCheckBoxListId="FilterLanguageList"
		    AllowSiteToContactCheckBoxId="AllowSiteToContact"
		    AllowSitePartnersToContactCheckBoxId="AllowSitePartnersToContact"
            SubmitButtonId="UpdateButtonBottom"
            ShareFavoriteUsersCheckBoxId="ShareFavoriteUsers"
            ShareFavoritePostsCheckBoxId="ShareFavoritePosts"
            ShareFavoriteSectionsCheckBoxId="ShareFavoriteSections"
            SubFormIds="AvatarSubForm"
    		>
    		<SuccessActions>
    		    <CSControl:GoToModifiedUrlAction runat="server" QueryStringModification="ProfileSaved=true" />
    		</SuccessActions>
		    <FormTemplate>
		        <asp:CustomValidator runat="server" ID="FormValidator" />
		    
		        <TWC:TabbedPanes id="ProfileTabs" runat="server"
			        PanesCssClass="CommonPane"
			        TabSetCssClass="CommonPaneTabSet"
			        TabCssClasses="CommonPaneTab,CommonPaneTab1,CommonPaneTab2"
			        TabSelectedCssClasses="CommonPaneTabSelected,CommonPaneTabSelected1,CommonPaneTabSelected2"
			        TabHoverCssClasses="CommonPaneTabHover,CommonPaneTabHover1,CommonPaneTabHover2"
			        >
		        <TWC:TabbedPane runat="server">
			        <Tab><CSControl:ResourceControl runat="server" ResourceName="EditProfile_Tab_SignatureBio" /></Tab>
			        <Content>
				        <asp:Panel id="EnableSignature" Runat="server" cssclass="CommonGroupedContentArea">
				            <h3 class="CommonSubTitle"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_AboutSignature" /></h3>
				            <div class="CommonFormField">
					            <asp:TextBox id="Signature" TextMode="MultiLine" Columns="50" Rows="5" runat="server" />
					            <br /><asp:CustomValidator ControlToValidate="Signature" id="SignatureMaxLengthValidator" EnableClientScript="false" runat="server"/>
				            </div>
				        </asp:Panel>
        				
        				<div class="CommonGroupedContentArea">
				            <h3 class="CommonSubTitle"><CSControl:ResourceControl ID="ResourceControl1" runat="server" ResourceName="ViewUserProfile_Tab_Bio" /></h3>
				            <div class="CommonFormField">
					            <asp:TextBox id="bio" TextMode="MultiLine" Columns="50" Rows="5" runat="server" />
				            </div>
				        </div>
			        </Content>
		        </TWC:TabbedPane>
		        <TWC:TabbedPane runat="server">
			        <Tab><CSControl:ResourceControl runat="server" ResourceName="EditProfile_Tab_About" /></Tab>
			        <Content>
			            <div class="CommonGroupedContentArea">
				            <h3 class="CommonSubTitle"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_AboutTitle" /></h3>
				            <table cellspacing="0" cellpadding="0" border="0">
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_CommonName" />
						            </td>
						            <td class="CommonFormField">
							            <asp:Textbox id="CommonName" Columns="30" MaxLength="50" runat="server" /> 
						            </td>
					            </tr>
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_AboutLocation" />
						            </td>
						            <td class="CommonFormField">
							            <asp:TextBox id="Location" Columns="30" runat="server" MaxLength="50"/>
						            </td>
					            </tr>
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_AboutOccupation" />
						            </td>
						            <td class="CommonFormField">
							            <asp:TextBox id="Occupation" Columns="30" runat="server" MaxLength="50" />
						            </td>
					            </tr>
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_AboutInterests" />
						            </td>
						            <td class="CommonFormField">
							            <asp:TextBox id="Interests" Columns="30" runat="server" MaxLength="50" />
						            </td>
					            </tr>
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_Birthday" />
						            </td>
						            <td class="CommonFormField">
						                <asp:RadioButton GroupName="Birthday" ID="EnableBirthday" runat="server" /><TWC:DateTimeSelector runat="server" id="Birthday" DateTimeFormat="MMMM d, yyyy" ShowCalendarPopup="true" /><br />
						                <asp:RadioButton GroupName="Birthday" ID="DisableBirthday" runat="server" /><CSControl:ResourceControl runat="server" ResourceName="NotSet" />
						            </td>
					            </tr>
					            <CSControl:PlaceHolder runat="server">
					                <DisplayConditions><CSControl:ControlVisibilityCondition runat="server" ControlId="Gender" ControlVisiblilityEquals="true" /></DisplayConditions>
					                <ContentTemplate>
					                    <tr>
						                    <td class="CommonFormFieldName">
							                    <CSControl:ResourceControl runat="server" ResourceName="EditProfile_AboutGender" />
						                    </td>
						                    <td class="CommonFormField">
							                    <asp:RadioButtonList id="Gender" RepeatColumns="3" runat="server" />
						                    </td>
					                    </tr>
                                    </ContentTemplate>
                                </CSControl:PlaceHolder>
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_AboutWebAddress" />
						            </td>
						            <td class="CommonFormField">
							            <asp:TextBox id="WebAddress" Columns="30" runat="server" MaxLength="255" />
							            <asp:RegularExpressionValidator id="editNameRegExVldt" 
								            ControlToValidate="WebAddress" 
								            ValidationExpression="^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&%\$\-]+)*@)?((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+)(\:[0-9]+)?((/|/[^/][a-zA-Z0-9\.\,\?\'\\/\+&%\$#\=~_\-@]*))*$"
								            runat="server"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_InvalidUrl" /></asp:RegularExpressionValidator>
						            </td>
					            </tr>
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_AboutWebLog" />
						            </td>
						            <td class="CommonFormField">
							            <asp:TextBox id="WebLog" Columns="30" runat="server" MaxLength="255" />
							            <asp:RegularExpressionValidator id="editNameRegExVldt2" 
								            ControlToValidate="WebLog" 
								            ValidationExpression="^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&%\$\-]+)*@)?((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+)(\:[0-9]+)?((/|/[^/][a-zA-Z0-9\.\,\?\'\\/\+&%\$#\=~_\-@]*))*$"
								            runat="server"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_InvalidUrl" /></asp:RegularExpressionValidator>
						            </td>
					            </tr>
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_AboutWebGallery" />
						            </td>
						            <td class="CommonFormField">
							            <asp:TextBox id="WebGallery" Columns="30" runat="server" MaxLength="255" />
							            <asp:RegularExpressionValidator id="editNameRegExVldt3" 
								            ControlToValidate="WebGallery" 
								            ValidationExpression="^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&%\$\-]+)*@)?((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+)(\:[0-9]+)?((/|/[^/][a-zA-Z0-9\.\,\?\'\\/\+&%\$#\=~_\-@]*))*$"
								            runat="server"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_InvalidUrl" /></asp:RegularExpressionValidator>
						            </td>
					            </tr>
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_OptionsLanguage" />
						            </td>
						            <td class="CommonFormField">
							            <asp:DropDownList id="Language" runat="server" />
						            </td>
					            </tr>
				            </table>
				        </div>
				        <div class="CommonGroupedContentArea">
				            <h3 class="CommonSubTitle"><CSControl:ResourceControl runat="server" ResourceName="ViewUserProfile_Tab_RssFeeds" /></h3>
				            <div class="CommonFormFieldName"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_RssFeeds" /></div>
				            <div class="CommonFormField">
					            <asp:TextBox id="rssFeeds" TextMode="MultiLine" Columns="100" Rows="5" runat="server" Wrap="false" />
				            </div>
				        </div>
			        </Content>
			        </TWC:TabbedPane>
			        <TWC:TabbedPane runat="server" ID="AvatarTab">
			        <Tab><CSControl:ResourceControl runat="server" ResourceName="EditProfile_Tab_Avatar" /></Tab>
			        <Content>
			            <CSControl:UserAvatarSubForm ID="AvatarSubForm" runat="server" ControlIdsToHideWhenNotVisible="AvatarTab"
			                AvatarUrlTextBoxId="AvatarUrl" 
			                UploadAvatarHtmlInputFileId="AvatarUpload" 
			                EnableAvatarCheckBoxId="EnableAvatar" 
			                UploadAvatarButtonId="SubmitAvatar"
			                >
			                <FormTemplate>
			                    <div class="CommonGroupedContentArea">
				                    <h3 class="CommonSubTitle"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_AvatarTitle" /></h3>
				                    <table cellpadding="0" cellspacing="0" border="0">
					                    <tr>
						                    <td class="CommonFormFieldName">
							                    <CSControl:ResourceControl runat="server" ResourceName="EditProfile_AvatarEnable" />
						                    </td>
						                    <td class="CommonFormField">
							                    <CSControl:YesNoCheckBox id="EnableAvatar" runat="server"  /> 
							                    <CSControl:ResourceControl runat="server" ResourceName="EditProfile_AvatarsMustBeApprovedByModerator">
							                        <DisplayConditions Operator="Not"><CSControl:UserPropertyValueComparison runat="server" ComparisonProperty="IsAvatarApproved" Operator="IsSetOrTrue" /></DisplayConditions>
							                    </CSControl:ResourceControl>
						                    </td>
					                    </tr>
					                    <tr>
						                    <td class="CommonFormFieldName">
							                    <CSControl:ResourceControl runat="server" ResourceName="EditProfile_UploadAvatar" />
						                    </td>
						                    <td class="CommonFormField">
							                    <input type="file" runat="server" id="AvatarUpload" /> <CSControl:ResourceButton id="SubmitAvatar" runat="server" ResourceName="Update" CausesValidation="false" />
						                    </td>
					                    </tr>
					                    <tr>
						                    <td class="CommonFormFieldName">
							                    <CSControl:ResourceControl runat="server" ResourceName="EditProfile_AvatarUrl" />
						                    </td>
						                    <td class="CommonFormField">
							                    <asp:TextBox id="AvatarUrl" runat="server" MaxLength="256"/>
							                    <CSControl:ResourceControl runat="server" ResourceName="EditProfile_RemoteAvatarsDisabled">
							                        <DisplayConditions Operator="Not"><CSControl:SiteSettingsPropertyValueComparison runat="server" ComparisonProperty="EnableRemoteAvatars" Operator="IsSetOrTrue" /></DisplayConditions>
							                    </CSControl:ResourceControl>
						                    </td>
					                    </tr>
				                    </table>
				                 </div>
                            </FormTemplate>
                        </CSControl:UserAvatarSubForm>				                
			        </Content>
			        </TWC:TabbedPane>
			        <TWC:TabbedPane runat="server">
			        <Tab><CSControl:ResourceControl runat="server" ResourceName="EditProfile_Tab_Email" /></Tab>
			        <Content>
			            <div class="CommonGroupedContentArea">
				            <h3 class="CommonSubTitle"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_EmailTitle" /></h3>
				            <table cellspacing="0" cellpadding="0" border="0">
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_OptionsReceiveEmails" />
						            </td>
						            <td class="CommonFormField" nowrap="nowrap">
							            <CSControl:YesNoCheckBox id="ReceiveEmails" runat="server" />
						            </td>
					            </tr>
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_OptionsHtmlEmail" />
						            </td>
						            <td class="CommonFormField" nowrap="nowrap">
							            <CSControl:YesNoCheckBox id="EnableHtmlEmail" runat="server" />
						            </td>
					            </tr>
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_OptionsEmailTracking" />
						            </td>
						            <td class="CommonFormField" nowrap="nowrap">
							            <CSControl:YesNoCheckBox id="EnableEmailTracking" runat="server" />
						            </td>
					            </tr>
				            </table>
				        </div>

				        <div class="CommonGroupedContentArea">
				            <h3 class="CommonSubTitle"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_ContactTitle" /></h3>
				            <table cellspacing="0" cellpadding="0" border="0">
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_ContactPublicEmail" />
						            </td>
						            <td class="CommonFormField">
							            <asp:TextBox id="PublicEmail" Columns="30" runat="server" MaxLength="50"/>
						            </td>
					            </tr>
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_ContactMsn" />
						            </td>
						            <td class="CommonFormField">
							            <asp:TextBox id="MsnIM" Columns="30" runat="server" MaxLength="50"/>
						            </td>
					            </tr>
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_ContactAol" />
						            </td>
						            <td class="CommonFormField">
							            <asp:TextBox id="AolIM" Columns="30" runat="server" MaxLength="50"/>
						            </td>
					            </tr>
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_ContactYahoo" />
						            </td>
						            <td class="CommonFormField">
							            <asp:TextBox id="YahooIM" Columns="30" runat="server" MaxLength="50"/>
						            </td>
					            </tr>
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_ContactIcq" />
						            </td>
						            <td class="CommonFormField">
							            <asp:TextBox id="ICQ" Columns="30" runat="server" MaxLength="50"/>
						            </td>
					            </tr>
				            </table>
				        </div>

				        <div class="CommonGroupedContentArea">
				            <h3 class="CommonSubTitle"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_PrivateTitle" /></h3>
				            <table cellspacing="0" cellpadding="0" border="0">
					            <tr>
						            <td class="CommonFormFieldName" nowrap="nowrap">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_ContactPrivateEmail" />
						            </td>
						            <td class="CommonFormField">
							            <asp:TextBox id="PrivateEmail" Columns="30" runat="server" MaxLength="50"/>
							            <asp:RequiredFieldValidator id="emailValidator" runat="server" ControlToValidate="PrivateEmail" Cssclass="validationWarning">*</asp:RequiredFieldValidator>
							            <asp:RegularExpressionValidator id="emailRegExValidator" runat="server" ControlToValidate="PrivateEmail" Cssclass="validationWarning" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
						            </td>
					            </tr>
					             <CSControl:PlaceHolder runat="server">
					                <DisplayConditions><CSControl:SiteSettingsPropertyValueComparison runat="server" ComparisonProperty="PasswordRecovery" ComparisonValue="QuestionAndAnswer" Operator="EqualTo" /></DisplayConditions>
					                <ContentTemplate>
					                    <tr id="EnablePasswordQuestionAnswer" runat="server" visible="false">
						                    <td class="CommonFormFieldName" >
							                    <CSControl:ResourceControl runat="server" ResourceName="EditProfile_PasswordAnswer" />
						                    </td>
						                    <td class="CommonFormField">
						                        <CSControl:SiteUrl runat="server" UrlName="user_ChangePasswordAnswer" ResourceName="EditProfile_PasswordAnswerChange" />
						                    </td>
					                    </tr>
                                    </ContentTemplate>
                                </CSControl:PlaceHolder>
				            </table>
				        </div>
			        </Content>
			        </TWC:TabbedPane>
			        <TWC:TabbedPane runat="server">
			        <Tab><CSControl:ResourceControl runat="server" ResourceName="EditProfile_Tab_Options" /></Tab>
			        <Content>
				        <div class="CommonGroupedContentArea">
				            <h3 class="CommonSubTitle"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_OptionsTitle" /></h3>
				            <table cellspacing="0" border="0" cellpadding="0">
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_OptionsSortOrder" />
						            </td>
						            <td class="CommonFormField">
							            <asp:DropDownList id="SortOrder" runat="server" />
						            </td>
					            </tr>  
					            <CSControl:PlaceHolder runat="server">
					                <DisplayConditions><CSControl:ControlVisibilityCondition runat="server" ControlId="Theme" ControlVisiblilityEquals="true" /></DisplayConditions>
					                <ContentTemplate>
                                        <tr>
						                    <td class="CommonFormFieldName">
							                    <CSControl:ResourceControl runat="server" ResourceName="EditProfile_Theme" />
						                    </td>
						                    <td class="CommonFormField">
							                    <asp:DropDownList id="Theme" runat="server" />
						                    </td>
					                    </tr>					            
					                </ContentTemplate>
					            </CSControl:PlaceHolder>        
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_FontSize" />
						            </td>
						            <td class="CommonFormField">
							            <asp:DropDownList id="FontSize" runat="server" />
						            </td>
					            </tr>
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_Editor" />
						            </td>
						            <td class="CommonFormField">
							            <asp:DropDownList id="EditorList" runat="server" />
						            </td>
					            </tr>
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_PostViewType" />
						            </td>
						            <td class="CommonFormField">
							            <asp:DropDownList id="PostViewType" runat="server" />
						            </td>
					            </tr>		
					            <CSControl:PlaceHolder runat="server">
					                <DisplayConditions><CSControl:SiteSettingsPropertyValueComparison runat="server" ComparisonProperty="EnableInk" Operator="IsSetOrTrue" /></DisplayConditions>
					                <ContentTemplate>			
					                    <tr>
						                    <td class="CommonFormFieldName">
							                    <CSControl:ResourceControl runat="server" ResourceName="EditProfile_Ink" />
						                    </td>
						                    <td class="CommonFormField">
							                    <CSControl:YesNoCheckBox id="enableInk" runat="server" />
						                    </td>
					                    </tr>								
                                    </ContentTemplate>
                                </CSControl:PlaceHolder>
        					    <CSControl:PlaceHolder runat="server">
					                <DisplayConditions><CSControl:SiteSettingsPropertyValueComparison runat="server" ComparisonProperty="EnableDisplayNames" Operator="IsSetOrTrue" /></DisplayConditions>
					                <ContentTemplate>
					                    <tr>
						                    <td class="CommonFormFieldName">
							                    <CSControl:ResourceControl runat="server" ResourceName="EditProfile_DisplayName" />
						                    </td>
						                    <td class="CommonFormField">
							                    <CSControl:YesNoCheckBox id="enableDisplayNames" runat="server" />
						                    </td>
					                    </tr>									
					                </ContentTemplate>
					            </CSControl:PlaceHolder>
					            <tr>        
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_DisplayInMemberList" />
						            </td>
						            <td class="CommonFormField" nowrap="nowrap">
							            <CSControl:YesNoCheckBox id="DisplayInMemberList" runat="server" />
						            </td>
					            </tr>
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="Admin_SiteSettings_EnablePostPreviewPopup" />
						            </td>
						            <td class="CommonFormField" nowrap="nowrap">
							            <CSControl:YesNoCheckBox id="EnablePostPreviewPopup" runat="server" />
						            </td>
					            </tr>
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="Admin_SiteSettings_EnableCollapsingPanels" />
						            </td>
						            <td class="CommonFormField" nowrap="nowrap">
							            <CSControl:YesNoCheckBox id="EnableCollapsingPanels" runat="server" />
						            </td>
					            </tr>

					            <tr id="EmoticonRow" runat="Server">
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="Admin_SiteSettings_EnableEmoticons" />
						            </td>
						            <td class="CommonFormField" nowrap="nowrap">
							            <CSControl:YesNoCheckBox id="EnableEmoticons" runat="server" />
						            </td>
					            </tr>
            					
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_EnableUserAvatars" />
						            </td>
						            <td class="CommonFormField" nowrap="nowrap">
							            <CSControl:YesNoCheckBox id="enableUserAvtars" runat="server" />
						            </td>
					            </tr>
            					
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_EnableUserSignatures" />
						            </td>
						            <td class="CommonFormField" nowrap="nowrap">
							            <CSControl:YesNoCheckBox id="enableUserSignatures" runat="server" />
						            </td>
					            </tr>																

					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:FormLabel LabelForId="ynEnableHelp" ResourceName="EditProfile_ShowHelpTips" runat="server" />
						            </td>
						            <td class="CommonFormField">
							            <CSControl:YesNoCheckBox id="ynEnableHelp" runat="server" />
						            </td>
					            </tr>
					            <tr>
						            <td class="CommonFormFieldName">
							            <CSControl:FormLabel runat="server" ResourceName="EditProfile_CPGridItemsPerPage" LabelForId="PageSize" />
						            </td>
						            <td class="CommonFormField">
							            <asp:dropdownlist id="PageSize" runat="Server">
								            <asp:listitem value="5">5</asp:listitem>
								            <asp:listitem value="10">10</asp:listitem>
								            <asp:listitem value="20">20</asp:listitem>
								            <asp:listitem value="30">30</asp:listitem>
								            <asp:listitem value="40">40</asp:listitem>
								            <asp:listitem value="50">50</asp:listitem>
							            </asp:dropdownlist>
						            </td>
					            </tr>
					            <CSControl:PlaceHolder runat="server">
					                <DisplayConditions><CSControl:ControlVisibilityCondition runat="server" ControlId="ControlPanelManageView" ControlVisiblilityEquals="true" /></DisplayConditions>
					                <ContentTemplate>
					                    <tr>
						                    <td class="CommonFormFieldName">
							                    <CSControl:ResourceControl runat="server" ResourceName="EditProfile_ControlPanelManageView" />
						                    </td>
						                    <td class="CommonFormField">
							                    <asp:DropDownList id="ControlPanelManageView" runat="server" />
						                    </td>
					                    </tr>		
					                </ContentTemplate>
					            </CSControl:PlaceHolder>

				                <CSControl:PlaceHolder runat="server">
					                <DisplayConditions><CSControl:ControlVisibilityCondition runat="server" ControlId="AllowSiteToContact" ControlVisiblilityEquals="true" /></DisplayConditions>
					                <ContentTemplate>
					                    <tr>
						                    <td class="CommonFormFieldName">
							                    <CSControl:FormLabel runat="server" ResourceName="EditProfile_AllowSiteToContact" LabelForId="ynEnableHelp" />
						                    </td>
						                    <td class="CommonFormField">
							                    <CSControl:YesNoCheckBox id="AllowSiteToContact" runat="server" />
						                    </td>
					                    </tr>
					                </ContentTemplate>
					            </CSControl:PlaceHolder>
    					        
					             <CSControl:PlaceHolder runat="server">
					                <DisplayConditions><CSControl:ControlVisibilityCondition runat="server" ControlId="AllowSitePartnersToContact" ControlVisiblilityEquals="true" /></DisplayConditions>
					                <ContentTemplate>
					                    <tr>
						                    <td class="CommonFormFieldName">
							                    <CSControl:FormLabel runat="server" ResourceName="EditProfile_AllowSitePartnersToContact" LabelForId="ynEnableHelp" />
						                    </td>
						                    <td class="CommonFormField">
							                    <CSControl:YesNoCheckBox id="AllowSitePartnersToContact" runat="server" />
						                    </td>
					                    </tr>
					                </ContentTemplate>
					            </CSControl:PlaceHolder>
				            </table>
				        </div>
        				
				        <CSControl:PlaceHolder runat="server">
					        <DisplayConditions><CSControl:ControlVisibilityCondition runat="server" ControlId="FilterLanguageList" ControlVisiblilityEquals="true" /></DisplayConditions>
					        <ContentTemplate>
					            <div class="CommonGroupedContentArea">
						            <h3 class="CommonSubTitle">
							            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_ContentLanguagesToExclude" />
						            </h3>
						            <div class="CommonFormField">
							            <asp:CheckBoxList RepeatColumns="3" RepeatDirection="Horizontal" id="FilterLanguageList" runat="server" />
						            </div>
					            </div>
				            </ContentTemplate>
				        </CSControl:PlaceHolder>
        				
				        <div class="CommonGroupedContentArea">
				            <h3 class="CommonSubTitle"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_AboutTimeZone" /></h3>
				            <asp:DropDownList id="Timezone" runat="server" />
				        </div>

				        <div class="CommonGroupedContentArea">
				            <h3 class="CommonSubTitle"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_OptionsDateFormat" /></h3>
				            <asp:DropDownList id="DateFormat" runat="server" />
				        </div>

			        </Content>
			        </TWC:TabbedPane>
			        <TWC:TabbedPane runat="server">
			        <Tab><CSControl:ResourceControl runat="server" ResourceName="EditProfile_Tab_PasswordStatistics" /></Tab>
			        <Content>
			            <div class="CommonGroupedContentArea">
				            <h3 class="CommonSubTitle"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_Password" /></h3>
				            <div class="CommonFormField">
				                <CSControl:SiteUrl runat="server" UrlName="user_ChangePassword" ResourceName="EditProfile_PasswordChange" LinkCssClass="CommonTextButton" />
				            </div>
						    <div class="CommonFormField">	
					            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_LastLogin" />
					            <CSControl:UserData runat="server" Property="LastLogin" />
				            </div>
				            <div class="CommonFormField">
					            <CSControl:ResourceControl runat="server" ResourceName="EditProfile_DateCreated" />
					            <CSControl:UserData runat="server" Property="DateCreated" />
				            </div>
				            <div class="CommonFormField">
				    		    <CSControl:ResourceControl runat="server" ResourceName="EditProfile_LastActivity" />
					            <CSControl:UserData runat="server" Property="LastActivity" />
				            </div>
				         </div>
			        </Content>
			        </TWC:TabbedPane>
			        <TWC:TabbedPane runat="server">
			        <Tab><CSControl:ResourceControl runat="server" ResourceName="EditProfile_Tab_Favorites" /></Tab>
			        <Content>
			            <div class="CommonGroupedContentArea">
					        <h3 class="CommonSubTitle"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_Favorites_Title" /></h3>
					        <div class="CommonListArea">
						        <asp:checkbox id="ShareFavoriteUsers" runat="server" /><CSControl:FormLabel runat="server" LabelForId="ShareFavoriteUsers" ResourceName="EditProfile_FavoriteUsers_Share" />
					            <h4 class="CommonListTitle"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_FavoriteUsers_Title" /></h4>
					            <CSControl:UserList runat="server" ID="FavoriteUsers">
				                    <QueryOverrides QueryType="AccessingUserFavorites" PageSize="999999" />
					                <HeaderTemplate>
						                <table cellpadding="0" cellspacing="0" border="0" width="100%">
							                <thead>
							                <tr>
							                    <th class="CommonListHeaderLeftMost">&nbsp;</th>
								                <th class="CommonListHeader"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_FavoriteUsers_Username" /></th>
								                <th class="CommonListHeader"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_FavoriteUsers_TotalPosts" /></th>
								                <th class="CommonListHeader"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_FavoriteUsers_PostRank" /></th>
								                <th class="CommonListHeader" runat="server" visible="<%# CurrentCSContext.User.CanSeeUserPoints %>"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_FavoriteUsers_Points" /></th>
								                <th class="CommonListHeader"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_FavoriteUsers_Actions" /></th>
							                </tr>
							                </thead>
							                <tbody>
					                </HeaderTemplate>
					                <ItemTemplate>
							                <tr>
							                    <td class="CommonListCellLeftMost"><CSControl:UserCheckBox runat="server" /></td>
								                <td class="CommonListCell"><CSControl:UserData LinkTo="Profile" Property="DisplayName" runat="server" /></td>
								                <td class="CommonListCell"><CSControl:UserData Property="TotalPosts" runat="server" /></td>
								                <td class="CommonListCell"><CSControl:UserData Property="PostRank" runat="server" /></td>
								                <td class="CommonListCell" runat="server" visible="<%# CurrentCSContext.User.CanSeeUserPoints %>"><CSControl:UserData Property="Points" runat="server" /></td>
								                <td class="CommonListCell"><CSControl:UserData LinkTo="PrivateMessage" runat="server"><ContentTemplate><CSControl:ThemeImage ImageUrl="~/images/common/privatemessage.gif" BorderWidth="0" runat="server" /></ContentTemplate></CSControl:UserData></td>
							                </tr>
					                </ItemTemplate>
					                <FooterTemplate>
							                </tbody>
						                </table>
					                </FooterTemplate>
					                <NoneTemplate>
							                <tr>
								                <td align="center" colspan="6" class="CommonListCellLeftMost"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_FavoriteUsers_None" /></td>
							                </tr>
					                </NoneTemplate>
				                </CSControl:UserList>
					        </div>
					        <CSControl:DeleteFavoriteUsersForm runat="server" SubmitButtonId="DeleteFavoriteUsers">
					            <SuccessActions><CSControl:ClearAndDataBindRepeaterAction RepeaterId="FavoriteUsers" runat="server" /></SuccessActions>
					            <FormTemplate>
					                <div class="CommonFormField">
						                <CSControl:ResourceButton id="DeleteFavoriteUsers" runat="server" ResourceName="Delete" />
					                </div>
					            </FormTemplate>
					        </CSControl:DeleteFavoriteUsersForm>
				        <p />
					        <div class="CommonListArea">
					        <h4 class="CommonListTitle"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_Watchers_Title" /></h4>
					        <CSControl:UserList runat="server">
				                <QueryOverrides QueryType="UsersWatchingAccessingUser" PageSize="999999" />
					            <HeaderTemplate>
						            <table cellpadding="0" cellspacing="0" border="0" width="100%">
							            <thead>
							            <tr>
								            <th class="CommonListHeaderLeftMost"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_FavoriteUsers_Username" /></th>
								            <th class="CommonListHeader"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_FavoriteUsers_TotalPosts" /></th>
								            <th class="CommonListHeader"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_FavoriteUsers_PostRank" /></th>
								            <th class="CommonListHeader" runat="server" visible="<%# CurrentCSContext.User.CanSeeUserPoints %>"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_FavoriteUsers_Points" /></th>
								            <th class="CommonListHeader"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_FavoriteUsers_Actions" /></th>
							            </tr>
							            </thead>
							            <tbody>
					            </HeaderTemplate>
					            <ItemTemplate>
							            <tr>
								            <td class="CommonListCellLeftMost"><CSControl:UserData LinkTo="Profile" Property="DisplayName" runat="server" /></td>
								            <td class="CommonListCell"><CSControl:UserData Property="TotalPosts" runat="server" /></td>
								            <td class="CommonListCell"><CSControl:UserData Property="PostRank" runat="server" /></td>
								            <td class="CommonListCell" runat="server" visible="<%# CurrentCSContext.User.CanSeeUserPoints %>"><CSControl:UserData Property="Points" runat="server" /></td>
								            <td class="CommonListCell"><CSControl:UserData LinkTo="PrivateMessage" runat="server"><ContentTemplate><CSControl:ThemeImage ImageUrl="~/images/common/privatemessage.gif" BorderWidth="0" runat="server" /></ContentTemplate></CSControl:UserData></td>
							            </tr>
					            </ItemTemplate>
					            <FooterTemplate>
							            </tbody>
						            </table>
					            </FooterTemplate>
					            <NoneTemplate>
							            <tr>
								            <td align="center" colspan="5" class="CommonListCellLeftMost"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_Watchers_None" /></td>
							            </tr>
					            </NoneTemplate>
				            </CSControl:UserList>
					        </div>
				        <p />
					        <div class="CommonListArea">
						        <asp:checkbox id="ShareFavoritePosts" runat="server" /><CSControl:FormLabel runat="server" LabelForId="ShareFavoritePosts" ResourceName="EditProfile_FavoritePosts_Share" />
					            <h4 class="CommonListTitle"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_FavoritePosts_Title" /></h4>
					            <CSControl:PostList runat="server" ID="FavoritePosts">
				                    <QueryOverrides QueryType="AccessingUserFavorites" PageSize="999999" />
					                <HeaderTemplate>
						                <table cellpadding="0" cellspacing="0" border="0" width="100%">
							                <thead>
							                <tr>
							                    <th class="CommonListHeaderLeftMost">&nbsp;</th>
								                <th class="CommonListHeader"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_FavoritePosts_Subject" /></th>
								                <th class="CommonListHeader"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_FavoritePosts_Excerpt" /></th>
							                </tr>
							                </thead>
							                <tbody>
					                </HeaderTemplate>
					                <ItemTemplate>
							                <tr>
							                    <td class="CommonListCellLeftMost"><CSControl:PostCheckBox runat="server" /></td>
								                <td class="CommonListCell"><CSControl:PostData LinkTo="ViewPost" Property="Subject" runat="server" /></td>
								                <td class="CommonListCell"><CSControl:PostData Property="FormattedBody" runat="server" TruncateAt="300" /></td>
							                </tr>
					                </ItemTemplate>
					                <FooterTemplate>
							                </tbody>
						                </table>
					                </FooterTemplate>
					                <NoneTemplate>
							                <tr>
								                <td align="center" colspan="3" class="CommonListCellLeftMost"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_FavoritePosts_None" /></td>
							                </tr>
					                </NoneTemplate>
			                    </CSControl:PostList>
					        </div>
					        <CSControl:DeleteFavoritePostsForm runat="server" SubmitButtonId="DeleteFavoritePosts">
					            <SuccessActions><CSControl:ClearAndDataBindRepeaterAction RepeaterId="FavoritePosts" runat="server" /></SuccessActions>
					            <FormTemplate>
					                <div class="CommonFormField">
						                <CSControl:ResourceButton id="DeleteFavoritePosts" runat="server" ResourceName="Delete" />
					                </div>
					            </FormTemplate>
					        </CSControl:DeleteFavoritePostsForm>
				        <p />
					        <div class="CommonListArea">
						        <asp:checkbox id="ShareFavoriteSections" runat="server" /><CSControl:FormLabel runat="server" LabelForId="ShareFavoriteSections" ResourceName="EditProfile_FavoriteSections_Share" />
					        <h4 class="CommonListTitle"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_FavoriteSections_Title" /></h4>
					        <CSControl:SectionList runat="server" ID="FavoriteSections">
				                <QueryOverrides QueryType="AccessingUserFavorites" PageSize="999999" />
					            <HeaderTemplate>
						            <table cellpadding="0" cellspacing="0" border="0" width="100%">
							            <thead>
							            <tr>
							                <th class="CommonListHeaderLeftMost">&nbsp;</th>
								            <th class="CommonListHeader"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_FavoriteSections_Name" /></th>
								            <th class="CommonListHeader"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_FavoriteSections_Description" /></th>
							            </tr>
							            </thead>
							            <tbody>
					            </HeaderTemplate>
					            <ItemTemplate>
							            <tr>
							                <td class="CommonListCellLeftMost"><CSControl:SectionCheckBox runat="server" /></td>
								            <td class="CommonListCell">
								                <CSControl:SectionData LinkTo="SectionHome" Property="Name" runat="server" />
								                <CSControl:SectionData Property="ApplicationType" Text="({0})" runat="server" />
								            </td>
								            <td class="CommonListCell"><CSControl:SectionData Property="Description" runat="server" /></td>
							            </tr>
					            </ItemTemplate>
					            <FooterTemplate>
							            </tbody>
						            </table>
					            </FooterTemplate>
					            <NoneTemplate>
							            <tr>
								            <td align="center" colspan="3" class="CommonListCellLeftMost"><CSControl:ResourceControl runat="server" ResourceName="EditProfile_FavoriteSections_None" /></td>
							            </tr>
					            </NoneTemplate>
				            </CSControl:SectionList>
					        </div>
					         <CSControl:DeleteFavoriteSectionsForm runat="server" SubmitButtonId="DeleteFavoriteSections">
					            <SuccessActions><CSControl:ClearAndDataBindRepeaterAction RepeaterId="FavoriteSections" runat="server" /></SuccessActions>
					            <FormTemplate>
					                <div class="CommonFormField">
						                <CSControl:ResourceButton id="DeleteFavoriteSections" runat="server" ResourceName="Delete" />
					                </div>
					            </FormTemplate>
					         </CSControl:DeleteFavoriteSectionsForm>
					     </div>
			        </Content>
			        </TWC:TabbedPane>
		        </TWC:TabbedPanes>
		        
		        <div class="CommonFormField">		
                    <CSControl:ResourceLinkButton runat="server" CssClass="CommonTextButton" id="UpdateButtonBottom" ResourceName="Save" />
		        </div>
            </FormTemplate>
		</CSControl:EditUserForm>
	</div>
	<CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdBottom runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>

</div>

</asp:Content>

<asp:Content ContentPlaceHolderID="rcr" runat="server">
<div class="CommonSidebar">
    <div class="CommonSidebarArea">
	    <div class="CommonSidebarRoundTop"><div class="r1"></div><div class="r2"></div><div class="r3"></div><div class="r4"></div></div>
	    <div class="CommonSidebarInnerArea">
	        <h4 class="CommonSidebarHeader"><CSControl:ResourceControl runat="server" ResourceName="Profile_Options_Title" /></h4>
	        <div class="CommonSidebarContent">
		        <ul class="CommonSidebarList">
		            <CSControl:UserData runat="server"  UseAccessingUser="true">
		                <ContentTemplate>
			                <CSControl:UserData LinkTo="Profile" Property="DisplayName" ResourceName="Profile_ViewPublicProfile" runat="server" Tag="Li" />
			            </ContentTemplate>
			        </CSControl:UserData>
		        </ul>
	        </div>
        </div>
        <div class="CommonSidebarRoundBottom"><div class="r1"></div><div class="r2"></div><div class="r3"></div><div class="r4"></div></div>
    </div>
</div>
</asp:Content>
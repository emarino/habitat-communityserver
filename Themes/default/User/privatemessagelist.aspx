<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Discussions.Controls.CSForumThemePage" MasterPageFile="../forums/forums.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Discussions.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        if (CurrentForum != null)
            SetTitle(CurrentForum.Name, true);
    }

</script>

<asp:Content ContentPlaceHolderID="bbcr" runat="server">
    <CSForum:BreadCrumb runat="server" Tag="Div" CssClass="CommonBreadCrumbArea">
        <LeaderTemplate><div class="Common"></LeaderTemplate>
        <TrailerTemplate></div></TrailerTemplate>
    </CSForum:BreadCrumb>
</asp:Content>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
<div class="CommonContentArea">
	<CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdTop runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
	
	<CSForum:ForumEditableData runat="server" Property="Name" Tag="H2" CssClass="CommonTitle" EditorLinkCssClass="CommonTextButton" EditorCssClass="CommonInlineTagEditor" ContentCssClass="CommonContentPartBorderOff" ContentEditingCssClass="CommonContentPartBorderOn" ContentHoverCssClass="CommonContentPartBorderOn" />
	
	<div class="CommonContent">
        <CSForum:ForumEditableData runat="server" Property="Description" Tag="Div" CssClass="CommonDescription" EditorLinkCssClass="CommonTextButton" EditorCssClass="CommonInlineTagEditor" ContentCssClass="CommonContentPartBorderOff" ContentEditingCssClass="CommonContentPartBorderOn" ContentHoverCssClass="CommonContentPartBorderOn" />


        <CSControl:PostbackPager runat="server" ID="PagerTop" ShowTotalSummary="true">
            <LeaderTemplate><div align="right" class="CommonPagingArea"></LeaderTemplate>
            <TrailerTemplate></div></TrailerTemplate>
        </CSControl:PostbackPager>
        
        <div class="CommonListArea">
		    <table width="100%" cellpadding="0" cellspacing="0" border="0">
		        <thead>
		        <tr>
                <th class="CommonListHeaderLeftMost" colspan="3" align="center">
					<CSControl:ResourceControl runat="server" ResourceName="PrivateMessages_Messages" />
				</th>
				<th class="CommonListHeader" align="center" nowrap="nowrap">
					<CSControl:ResourceControl runat="server" ResourceName="PrivateMessages_Recipients" />
				</th>
				<th class="CommonListHeader" align="center" nowrap="nowrap">
					<CSControl:ResourceControl runat="server" ResourceName="ViewThreads_TitleReplies" />
				</th>
				<th class="CommonListHeader" align="center" nowrap="nowrap">
					<CSControl:ResourceControl runat="server" ResourceName="ViewThreads_TitleViews" />
				</th>          
				<th class="CommonListHeader" align="center" nowrap="nowrap">
					<CSControl:ResourceControl runat="server" ResourceName="ViewThreads_TitleLastPost" />
				</th>
				</tr>
				</thead>
				<tbody>
	    
	    <CSForum:ThreadList runat="server" ID="FilteredThreadList" ShowHeaderFooterOnNone="false">
	        <QueryOverrides PagerID="Pager" ForumID="0" UserFilter="All" />
		    <ItemTemplate>
				    <tr class="CommonListRow">
				        <td class="CommonListCellLeftMost">
				            <CSForum:ThreadCheckbox runat="server" />
				        </td>
					    <td class="ForumListCellImageOnly">
						    <%# ForumFormatter.StatusIcon( (Thread) Container.DataItem ) %>
					    </td>
					    <td class="CommonListCell">
					        <table cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="ForumSubListCellLeftMost"><%# Formatter.GetEmotionMarkup( ((Thread) Container.DataItem).EmoticonID ) %>
										<CSForum:ThreadData Property="Subject" TruncateAt="65" LinkTo="Thread" LinkCssClass="ForumNameRead" runat="server"><DisplayConditions><CSForum:ThreadPropertyComparison ComparisonProperty1="HasRead" Operator="IsSetOrTrue" runat="server" /></DisplayConditions></CSForum:ThreadData>
								        <CSForum:ThreadData Property="Subject" TruncateAt="65" LinkTo="Thread" LinkCssClass="ForumNameUnRead" runat="server"><DisplayConditions Operator="Not"><CSForum:ThreadPropertyComparison ComparisonProperty1="HasRead" Operator="IsSetOrTrue" runat="server" /></DisplayConditions></CSForum:ThreadData>
										<CSForum:ThreadPostPageLinks runat="server" />
									</td>
									<td class="ForumSubListCell">
										<CSForum:ThreadRating runat="server" IsReadOnly="true" RatingCssClass="CommonRateControl" RatingReadOnlyCssClass="CommonRateControlReadOnly" RatingActiveCssClass="CommonRateControlActive" ImagesBaseUrl="~/Themes/default/images/common/" />
                                        <CSControl:PlaceHolder runat="server">
									        <DisplayConditions Operator="And">
									            <CSForum:ForumConfigurationPropertyComparison ComparisonProperty1="EnableThreadStatus" Operator="IsSetOrTrue" runat="server" />
									            <CSForum:ThreadPropertyValueComparison ComparisonProperty="Status" Operator="EqualTo" ComparisonValue="NotAnswered" runat="server" />
                                            </DisplayConditions>
									        <ContentTemplate><CSControl:ThemeImage ImageUrl="~/images/status_NotAnswered.gif" runat="server" AlternateTextResourceName="Status_NotAnswered" /></ContentTemplate>
									    </CSControl:PlaceHolder>
									    
									    <CSControl:PlaceHolder runat="server">
									        <DisplayConditions Operator="And">
									            <CSForum:ForumConfigurationPropertyComparison ComparisonProperty1="EnableThreadStatus" Operator="IsSetOrTrue" runat="server" />
									            <CSForum:ThreadPropertyValueComparison ComparisonProperty="Status" Operator="EqualTo" ComparisonValue="Answered" runat="server" />
                                            </DisplayConditions>
									        <ContentTemplate><CSControl:ThemeImage ImageUrl="~/images/status_Answered.gif" runat="server" AlternateTextResourceName="Status_Answered" /></ContentTemplate>
									    </CSControl:PlaceHolder>
									</td>
								</tr>
								<tr> 
									<td colspan="2" class="ForumSubListCellLeftMost">
										<CSControl:ResourceControl runat="server" ResourceName="ForumGroupView_Legend_LastPostBy" /><CSControl:UserData Property="DisplayName" LinkTo="Profile" runat="server" />
									</td>
								</tr>
							</table>
					    </td>
					    <td class="CommonListCell" align="center">
							<%# ForumFormatter.FormatPrivateMessageRecipients ( (PrivateMessage) Container.DataItem ) %>
						</td>
						<td class="CommonListCell" align="center">
						    <CSForum:ThreadData Property="Replies" runat="server" />
						</td>
						<td class="CommonListCell" align="center">
						    <CSForum:ThreadData Property="Views" runat="server" />
						</td>
						<td class="CommonListCell" align="center">
					        <CSControl:PlaceHolder runat="server">
		                        <DisplayConditions><CSForum:ThreadPropertyValueComparison ComparisonProperty="ThreadDate" ComparisonValue="6/8/1980" Operator="GreaterThan" runat="server" /></DisplayConditions>
		                        <ContentTemplate>
                                    <CSForum:ThreadData LinkTo="MostRecentPostAuthor" Property="MostRecentPostAuthor" runat="server" TruncateAt="15"><LeaderTemplate>by </LeaderTemplate><TrailerTemplate><br /></TrailerTemplate></CSForum:ThreadData>
                                    <CSForum:ThreadData Property="ThreadDate" runat="server" IncludeTimeInDate="true" />
                                </ContentTemplate>
                            </CSControl:PlaceHolder>
					    </td>
				    </tr>				
		    </ItemTemplate>
		    <AlternatingItemTemplate>
				    <tr class="CommonListRowAlt">
				        <td class="CommonListCellLeftMost">
				            <CSForum:ThreadCheckbox runat="server" />
				        </td>
					    <td class="ForumListCellImageOnly">
						    <%# ForumFormatter.StatusIcon( (Thread) Container.DataItem ) %>
					    </td>
					    <td class="CommonListCell">
					        <table cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="ForumSubListCellLeftMost"><%# Formatter.GetEmotionMarkup( ((Thread) Container.DataItem).EmoticonID ) %>
										<CSForum:ThreadData Property="Subject" TruncateAt="65" LinkTo="Thread" LinkCssClass="ForumNameRead" runat="server"><DisplayConditions><CSForum:ThreadPropertyComparison ComparisonProperty1="HasRead" Operator="IsSetOrTrue" runat="server" /></DisplayConditions></CSForum:ThreadData>
								        <CSForum:ThreadData Property="Subject" TruncateAt="65" LinkTo="Thread" LinkCssClass="ForumNameUnRead" runat="server"><DisplayConditions Operator="Not"><CSForum:ThreadPropertyComparison ComparisonProperty1="HasRead" Operator="IsSetOrTrue" runat="server" /></DisplayConditions></CSForum:ThreadData>
										<CSForum:ThreadPostPageLinks runat="server" />
									</td>
									<td class="ForumSubListCell">
										<CSForum:ThreadRating runat="server" IsReadOnly="true" RatingCssClass="CommonRateControl" RatingReadOnlyCssClass="CommonRateControlReadOnly" RatingActiveCssClass="CommonRateControlActive" ImagesBaseUrl="~/Themes/default/images/common/" />
                                        <CSControl:PlaceHolder runat="server">
									        <DisplayConditions Operator="And">
									            <CSForum:ForumConfigurationPropertyComparison ComparisonProperty1="EnableThreadStatus" Operator="IsSetOrTrue" runat="server" />
									            <CSForum:ThreadPropertyValueComparison ComparisonProperty="Status" Operator="EqualTo" ComparisonValue="NotAnswered" runat="server" />
                                            </DisplayConditions>
									        <ContentTemplate><CSControl:ThemeImage ImageUrl="~/images/status_NotAnswered.gif" runat="server" AlternateTextResourceName="Status_NotAnswered" /></ContentTemplate>
									    </CSControl:PlaceHolder>
									    
									    <CSControl:PlaceHolder runat="server">
									        <DisplayConditions Operator="And">
									            <CSForum:ForumConfigurationPropertyComparison ComparisonProperty1="EnableThreadStatus" Operator="IsSetOrTrue" runat="server" />
									            <CSForum:ThreadPropertyValueComparison ComparisonProperty="Status" Operator="EqualTo" ComparisonValue="Answered" runat="server" />
                                            </DisplayConditions>
									        <ContentTemplate><CSControl:ThemeImage ImageUrl="~/images/status_Answered.gif" runat="server" AlternateTextResourceName="Status_Answered" /></ContentTemplate>
									    </CSControl:PlaceHolder>
									</td>
								</tr>
								<tr> 
									<td colspan="2" class="ForumSubListCellLeftMost">
										<CSControl:ResourceControl runat="server" ResourceName="ForumGroupView_Legend_LastPostBy" /><CSControl:UserData Property="DisplayName" LinkTo="Profile" runat="server" />
									</td>
								</tr>
							</table>
					    </td>
					    <td class="CommonListCell" align="center">
							<%# ForumFormatter.FormatPrivateMessageRecipients ( (PrivateMessage) Container.DataItem ) %>
						</td>
						<td class="CommonListCell" align="center">
						    <CSForum:ThreadData Property="Replies" runat="server" />
						</td>
						<td class="CommonListCell" align="center">
						    <CSForum:ThreadData Property="Views" runat="server" />
						</td>
						<td class="CommonListCell" align="center">
					        <CSControl:PlaceHolder runat="server">
		                        <DisplayConditions><CSForum:ThreadPropertyValueComparison ComparisonProperty="ThreadDate" ComparisonValue="6/8/1980" Operator="GreaterThan" runat="server" /></DisplayConditions>
		                        <ContentTemplate>
                                    <CSForum:ThreadData LinkTo="MostRecentPostAuthor" Property="MostRecentPostAuthor" runat="server" TruncateAt="15"><LeaderTemplate>by </LeaderTemplate><TrailerTemplate><br /></TrailerTemplate></CSForum:ThreadData>
                                    <CSForum:ThreadData Property="ThreadDate" runat="server" IncludeTimeInDate="true" />
                                </ContentTemplate>
                            </CSControl:PlaceHolder>
					    </td>
				    </tr>				
		    </AlternatingItemTemplate>
		    <NoneTemplate>
		        <tr><td colspan="7">
		            <CSControl:ResourceControl runat="server" ResourceName="ViewThreads_NoTopics" Tag="Div" CssClass="CommonMessageError" />
		        </td></tr>
		    </NoneTemplate>
	    </CSForum:ThreadList>
	    
	    </tbody>
	    </table>
	    </div>
    	
    	<table cellpadding="0" cellspacing="0" width="100%" border="0">
		    <tr>
			    <td valign="top" align="left">
                    <CSForum:DeletePrivateMessagesForm id="DeleteThreadsForm" runat="server" SubmitButtonId="DeleteThreads">
                        <SuccessActions>
                            <CSControl:GoToModifiedUrlAction runat="server" />
                        </SuccessActions>
	                    <FormTemplate>
	                        <div class="CommonFormArea">
		                        <div class="CommonFormField">
					                <CSControl:ResourceLinkButton id="DeleteThreads" runat="server" ResourceName="Delete" CssClass="CommonTextButton" CausesValidation="false" />
				                </div>
	                        </div>
                        </FormTemplate>
                    </CSForum:DeletePrivateMessagesForm>
			    </td>	
			    <td valign="top">
				   <CSControl:PagerGroup runat="server" id="Pager" PagerIds="PagerTop,PagerBottom" />
	                <CSControl:PostbackPager runat="server" ID="PagerBottom" ShowTotalSummary="true">
	                    <LeaderTemplate><div align="right" class="CommonPagingArea"></LeaderTemplate>
	                    <TrailerTemplate></div></TrailerTemplate>
                    </CSControl:PostbackPager>
			    </td> 
		    </tr>
	    </table>
	    
	    <p />
	    
        <CSForum:ThreadListFilterForm runat="server" Tag="Div" ContainerId="ForumOptions"
            DateFilterDropDownListId="DateFilter" 
             SortThreadByDropDownListId="SortBy"
             SortOrderDropDownListId="SortOrder"
             EmailNotificationDropDownListId="Email"
             ApplyButtonId="Apply"
             ApplyTemporarilyButtonId="ApplyTemp"
             ControlIdsToHideFromAnonymousUsers="EmailArea"
            ThreadListId="FilteredThreadList">
            <FormTemplate>
	            <div class="CommonFormArea">
	                <fieldset>
	                    <legend><CSControl:ResourceControl runat="server" ResourceName="ViewThreads_ForumOptions" /></legend>
	                    <table cellpadding="0" cellspacing="0" width="100%" border="0">
		                    <tr>
			                    <td align="left" class="CommonFormFieldName" nowrap="nowrap">
				                    <CSControl:ResourceControl runat="server" ResourceName="ViewThreads_SortedBy" />
			                    </td>
			                    <td align="left" class="CommonFormField" nowrap="nowrap">
				                    <asp:DropDownList runat="server" ID="SortBy" />
				                    <CSControl:ResourceControl runat="server" ResourceName="ViewThreads_In" />
				                    <asp:DropDownList runat="server" ID="SortOrder" />
				                    <CSControl:ResourceControl runat="server" ResourceName="ViewThreads_OrderFrom" />
			                    </td>
		                    </tr>
		                    <tr>
			                    <td align="left" class="CommonFormFieldName">
				                    <CSControl:ResourceControl runat="server" ResourceName="ViewThreads_FilterByDate" />
			                    </td>
			                    <td align="left" class="CommonFormField">
				                    <asp:DropDownList runat="server" ID="DateFilter" />
			                    </td>
		                    </tr>
		                    <tr runat="server" id="EmailArea">
			                    <td align="left" class="CommonFormFieldName" nowrap="nowrap">
				                    <CSControl:ResourceControl runat="server" ResourceName="EmailNotificationDropDownList_When" />
			                    </td>
			                    <td align="left" class="CommonFormField">
				                    <asp:DropDownList runat="server" ID="Email" />
			                    </td>
		                    </tr>
		                    <tr>
			                    <td align="left" class="CommonFormField" colspan="2" nowrap="nowrap">
			                        <CSControl:ResourceButton ID="ApplyTemp" ResourceName="ViewThreads_SortThreads" runat="server" />
			                        <CSControl:ResourceButton ID="Apply" ResourceName="ViewThreads_RememberSettings" runat="server" />
				                    
				                    <CSForum:MarkAllReadForm runat="server" SubmitButtonId="MarkAllRead">
				                        <SuccessActions><CSControl:GoToModifiedUrlAction runat="server" /></SuccessActions>
				                        <FormTemplate><CSControl:ResourceButton ID="MarkAllRead" ResourceName="MarkAllRead_Threads" runat="server" /></FormTemplate>
				                    </CSForum:MarkAllReadForm>
			                    </td>
		                    </tr>
	                    </table>
	                </fieldset>
	            </div>
	        </FormTemplate>
        </CSForum:ThreadListFilterForm>
	</div>
	
	<CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdBottom runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="rcr" runat="server"></asp:Content>
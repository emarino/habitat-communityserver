<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Controls.CSThemePage" MasterPageFile="../Common/master.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        SetTitle(ResourceManager.GetString("ForgottenPassword_Title"), true);

        TextBox email = CSControlUtility.Instance().FindControl(this, "EmailAddress") as TextBox;
        if (!string.IsNullOrEmpty(email.Text))
        {
            Message message;
            if (CurrentCSContext.SiteSettings.PasswordRecovery == CommunityServer.Components.PasswordRecovery.Reset)
                message = ResourceManager.GetMessage(CSExceptionType.UserPasswordResetSuccess);
            else
                message = ResourceManager.GetMessage(CSExceptionType.UserPasswordLinkSentSuccess);

            CSControlUtility.Instance().FindTextControl(this, "MessageTitle").Text = message.Title;
            CSControlUtility.Instance().FindTextControl(this, "MessageText").Text = string.Format(message.Body, email.Text.Trim());
        }
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">

<div align="center">
<div class="CommonMessageArea">
	<h4 class="CommonMessageTitle"><CSControl:ResourceControl runat="server" ResourceName="ForgottenPassword_Title" /></h4>
	<div class="CommonMessageContent">
	    <CSControl:ForgottenPasswordForm runat="server" AnswerValidationGroup="AnswerGroup" EmailValidationGroup="EmailGroup"
	        AnswerTextBoxId="SAnswer" 
	        EmailAddressTextBoxId="EmailAddress" 
	        NextButtonId="Next" 
	        QuestionTextId="sQuestion" 
	        SendPasswordButtonId="SendPassword">
	        <SuccessActions>
	            <CSControl:SetVisibilityAction runat="server" ControlIdsToShow="MessageArea" ControlIdsToHide="ButtonsArea,EmailArea,QuestionArea,DescriptionArea" />
	        </SuccessActions>
	        <FormTemplate>
		        <div class="CommonFormArea">
		        <div class="CommonFormDescription" runat="server" id="DescriptionArea">
			        <CSControl:ResourceControl runat="server" ResourceName="ForgottenPassword_Instructions" />
		        </div>
		        <table  cellspacing="0" cellpadding="0" border="0">
		            <CSControl:PlaceHolder runat="server" ID="EmailArea">
		                <DisplayConditions><CSControl:ControlVisibilityCondition ControlId="EmailAddress" ControlVisiblilityEquals="true" runat="server" /></DisplayConditions>
		                <ContentTemplate>
                            <tr id="Email_Row" runat="Server">
                                <td align="right" class="CommonFormFieldName" >
					                <CSControl:ResourceControl runat="server" ResourceName="ForgottenPassword_EmailAddress" />
                                </td>
                                <td align="left" class="CommonFormField" nowrap="nowrap">                  
					                <asp:TextBox id="EmailAddress" runat="server" Columns="30" MaxLength="128" ValidationGroup="EmailGroup" /> 
					                <asp:RequiredFieldValidator ValidationGroup="EmailGroup" ControlToValidate="EmailAddress" id="EmailValidator" runat="server">*</asp:RequiredFieldValidator>
					                <asp:RegularExpressionValidator ValidationGroup="EmailGroup" id="EmailRegexValidator" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="EmailAddress">*</asp:RegularExpressionValidator>
                                </td>                 
                            </tr>                
                        </ContentTemplate>
                    </CSControl:PlaceHolder>
                    <CSControl:PlaceHolder runat="server" ID="QuestionArea">
                        <DisplayConditions><CSControl:ControlVisibilityCondition ControlId="SAnswer" ControlVisiblilityEquals="true" runat="server" /></DisplayConditions>
                        <ContentTemplate>
                            <tr id="Question_Row" runat="server">
                                <td align="right" class="CommonFormFieldName" >
				                    <CSControl:ResourceControl runat="server" ResourceName="ForgottenPassword_SecretQuestion" />
                                </td>
                                <td align="left" class="CommonFormField">                  
			                        <asp:Literal runat="Server" id="sQuestion" />
                                </td>                 
                            </tr>                
                            <tr id="Answer_Row" runat="server">
                                <td align="right" class="CommonFormFieldName" >
					                <CSControl:ResourceControl runat="server" ResourceName="ForgottenPassword_SecretAnswer" />
                                </td>
                                <td align="left" class="CommonFormField" nowrap="nowrap">                  
					                <asp:TextBox id="SAnswer" runat="server" Columns="30" MaxLength="256" ValidationGroup="AnswerGroup" />             
					                <asp:RequiredFieldValidator ValidationGroup="AnswerGroup" ControlToValidate="SAnswer" id="SAnswerValidator" runat="server">*</asp:RequiredFieldValidator>
					                <asp:RegularExpressionValidator ValidationGroup="AnswerGroup" id="SAnswerRegexValidator" ControlToValidate="SAnswer" runat="server" ValidationExpression="[1-zA-Z0-1@.\s]{1,256}">*</asp:RegularExpressionValidator>
                                </td>                 
                            </tr>     
                        </ContentTemplate>
                    </CSControl:PlaceHolder>   
			        <tr runat="server" id="ButtonsArea">
			            <td>&nbsp;</td>
                        <td align="left" class="CommonFormField">    
					        <CSControl:ResourceLinkButton id="SendPassword" runat="server" CssClass="CommonTextButton Big" ResourceName="ForgottenPassword_SendButton" />
					        <CSControl:ResourceLinkButton id="Next" runat="server" CssClass="CommonTextButton Big" ResourceName="ForgottenPassword_NextButton" />
                        </td>
                    </tr>  
                    <tr runat="server" id="MessageArea" visible="false">
                        <td colspan="2" align="left" class="CommonFormField">
                            <b><asp:Literal runat="Server" id="MessageTitle" /></b><br>
                            <asp:Literal runat="Server" id="MessageText" />
                        </td>
                    </tr>              
		        </table>
		        </div>
            </FormTemplate>
        </CSControl:ForgottenPasswordForm>
	</div>
</div>	
</div>

</asp:Content>
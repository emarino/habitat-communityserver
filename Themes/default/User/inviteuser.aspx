<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Controls.CSThemePage" MasterPageFile="../Common/master.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        SetTitle(ResourceManager.GetString("InviteUser_Title"), true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">

<div align="center">
    <div class="CommonContentArea" style="width:650px;">
        <h2 class="CommonTitle"><CSControl:ResourceControl runat="server" ResourceName="InviteUser_Title" /></h2>
        <div class="CommonContent">
            <CSControl:ResourceControl runat="server" CssClass="CommonMessageSuccess" Tag="div" ResourceName="InviteUser_SendSuccess" Visible="false" ID="StatusMessage" />

            <CSControl:InviteUserForm runat="server" EmailTextBoxId="Email" MessageTextBoxId="Message" SendButtonId="Send">
                <SuccessActions>
                    <CSControl:SetVisibilityAction runat="server" ControlIdsToShow="StatusMessage" />
                </SuccessActions>
                <FormTemplate>
                    <div class="CommonFormArea" >
                        <div class="CommonFormFieldDescription"><CSControl:ResourceControl runat="server" ResourceName="InviteUser_Instructions" /></div>

                        <div align="center">
                            <div class="CommonMessageArea" style="width: 100%;">
                                <h4 class="CommonMessageTitle">
                                    <CSControl:ResourceControl runat="server" ResourceName="InviteUser_Required" />
                                </h4>
                                <div class="CommonMessageContent">
	                                <table cellpadding="0" cellspacing="0" border="0">
	                                <tr>
		                                <td class="CommonFormFieldName"><CSControl:ResourceControl runat="server" ResourceName="InviteUser_Email" /></td>
		                                <td class="CommonFormField">
			                                <asp:TextBox id="Email" runat="server" MaxLength="5000" columns="60" />
			                                <asp:RequiredFieldValidator id="emailValidator" runat="server" ControlToValidate="Email" Cssclass="validationWarning">*</asp:RequiredFieldValidator>
			                                <asp:RegularExpressionValidator id="emailRegExValidator" runat="server" ControlToValidate="Email" Cssclass="validationWarning" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*(\W*?\;\W*?\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*">*</asp:RegularExpressionValidator>
		                                </td>
	                                </tr>
	                                <tr valign="top">
		                                <td class="CommonFormFieldName"><CSControl:ResourceControl runat="server" ResourceName="InviteUser_Message" /></td>
		                                <td class="CommonFormField">
				                                <asp:TextBox id="Message" runat="server" textmode="MultiLine" rows="5" columns="60" />
			                            </td>
	                                </tr>	
	                                </table>
                                
                                    <div style="text-align: center; margin-top: 16px;">
                                        <CSControl:ResourceLinkButton runat="server" id="Send" CssClass="CommonTextButton Big" ResourceName="Send" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </FormTemplate>
            </CSControl:InviteUserForm>
        </div>
    </div>
</div>

</asp:Content>


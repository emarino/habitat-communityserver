<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Galleries.Controls.CSGalleryThemePage" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Galleries.Components" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" > 

<html>
	<head>
		<title>Processing request...</title>
	</head>
	<body id="Body" runat="server">
		<div align="center">
			<b>Submitting your photo to Shutterfly. Please wait...</b>
		</div>
 		
 		<img src="http://www.telligentsystems.com/Solutions/Gallery/orderPrints.aspx" alt="" width="1" height="1" />
 		
		<CSGallery:GalleryPostOrderPrintsRedirect runat="server" />
	</body>
</html>
  
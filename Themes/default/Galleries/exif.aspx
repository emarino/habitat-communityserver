<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Galleries.Controls.CSGalleryThemePage" MasterPageFile="galleries.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Galleries.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="GallerySidebar" Src="gallerysidebar.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        if (CurrentGallery != null)
            SetTitle(CurrentGallery.Name, true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">

    <script type="text/javascript">
    // <![CDATA[
    function toggleLayers(whichLayer)
    {
	    if(whichLayer != 'ViewSizes')
		    toggleLayer('ViewSizes', true);
	    if(whichLayer != 'ViewExif')
		    toggleLayer('ViewExif', true);
	    toggleLayer(whichLayer);
    }
    function toggleLayer(whichLayer, forceHide)
    {
	    var element = document.getElementById(whichLayer)
	    if(element != null)
	    {
		    var style2 = element.style;
		    if(forceHide)
			    style2.display =  ""
		    else
			    style2.display = style2.display? "":"block";
	    }
    }
    // ]]>
    </script>

    <div class="CommonContentArea">
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <CSControl:UserAvatar runat="server" Tag="Td" CssClass="OwnerAvatar" />
                <td>
                    <CSGallery:GalleryData Property="Name" LinkTo="ViewGallery" runat="server" Tag="h2" CssClass="CommonTitle" />
                    <div class="CommonContent">
                        <CSGallery:GalleryData Property="Description" runat="server" />
                        <div class="ClearLeft">&nbsp;</div>
                    </div>
                </td>
            </tr>
        </table>
    </div>

	<div class="CommonContentArea">
	    <div class="CommonContent">
            <h3 class="ContentHeader"><CSGallery:GalleryPostData Property="Name" runat="server" /></h3>
            <CSGallery:BreadCrumb runat="server" Tag="Div" CssClass="BreadcrumbList" />
            <CSGallery:GalleryPostPager runat="server" ShowTotalSummary="true">
                <LeaderTemplate>
                    <div style="float:right;position:relative;top:-15px;text-align:right;"><div class="PictureListPager"  style="position:relative; top: 8px;">
                </LeaderTemplate>
                <TrailerTemplate>
                    </div></div>
                </TrailerTemplate>
            </CSGallery:GalleryPostPager>
            <div class="clearBoth">&nbsp;</div>

            <div class="photocontentdetails">
                <div class="PictureDetail">
	                <span>
		                <CSGallery:GalleryPostImage ImageType="Other" Width="425" height="425" quality="70" BorderWidth="1" runat="server"/>
	                </span>
	                <div class="photodetailoptions">
		                <a href="javascript:toggleLayers('ViewSizes');" title="View Image Sizes" class="viewsizes">View Sizes</a>
		                <CSGallery:GalleryPostData runat="server" LinkTo="ViewPicture" LinkCssClass="viewcomments" ResourceName="Gallery_Exif_ViewComments" />
		                <div>
		                    <div id="ViewSizes">
	                            <ul>
	                                <CSGallery:GalleryPostData LinkTo="ImageThumbnail" runat="server" Tag="Li" ResourceName="Gallery_PictureSizes_Thumbnail">
	                                    <TrailerTemplate> <span class="emtext">(<CSGallery:GalleryPostData runat="server" Property="ThumbnailWidth" />x<CSGallery:GalleryPostData runat="server" Property="ThumbnailHeight" />)</span></TrailerTemplate>
	                                </CSGallery:GalleryPostData>
	                                <CSGallery:GalleryPostData LinkTo="ImageSecondaryThumbnail" runat="server" Tag="Li" ResourceName="Gallery_PictureSizes_Small">
	                                    <TrailerTemplate> <span class="emtext">(<CSGallery:GalleryPostData runat="server" Property="SecondaryThumbnailWidth" />x<CSGallery:GalleryPostData runat="server" Property="SecondaryThumbnailHeight" />)</span></TrailerTemplate>
	                                </CSGallery:GalleryPostData>
	                                <CSGallery:GalleryPostData LinkTo="ImageDetails" runat="server" Tag="Li" ResourceName="Gallery_PictureSizes_Medium">
	                                    <TrailerTemplate> <span class="emtext">(<CSGallery:GalleryPostData runat="server" Property="DetailsWidth" />x<CSGallery:GalleryPostData runat="server" Property="DetailsHeight" />)</span></TrailerTemplate>
	                                </CSGallery:GalleryPostData>
	                                <CSGallery:GalleryPostData LinkTo="ImageSlideshow" runat="server" Tag="Li" ResourceName="Gallery_PictureSizes_Large">
	                                    <TrailerTemplate> <span class="emtext">(<CSGallery:GalleryPostData runat="server" Property="SlideshowWidth" />x<CSGallery:GalleryPostData runat="server" Property="SlideshowHeight" />)</span></TrailerTemplate>
	                                </CSGallery:GalleryPostData>
	                                <CSGallery:GalleryPostData LinkTo="ImageOriginal" runat="server" Tag="Li" ResourceName="Gallery_PictureSizes_Original">
	                                    <TrailerTemplate> <span class="emtext">(<CSGallery:GalleryPostData runat="server" Property="OriginalWidth" />x<CSGallery:GalleryPostData runat="server" Property="OriginalHeight" />)</span></TrailerTemplate>
	                                </CSGallery:GalleryPostData>
	                            </ul>
                            </div>
		                </div>
	                </div>
                </div>
            </div>

            <div id="comments">
                <CSControl:ResourceControl runat="server" ResourceName="Gallery_ExifListing_ImageProperties" Tag="H3" />
	            <div class="CommonInlineMessageContent">
		            <CSGallery:GalleryPostExifList runat="server" ShowHeaderFooterOnNone="false">
						<QueryOverrides PropertyNames="" PageSize="999" />
			            <HeaderTemplate>
				            <ul class="CommonSidebarList">
			            </HeaderTemplate>
			            <ItemTemplate>
			                <li><strong><CSGallery:GalleryPostExifData Property="Name" runat="server" />:</strong> <CSGallery:GalleryPostExifData Property="Value" runat="server" /></li>
			            </ItemTemplate>
			            <NoneTemplate>
				            <div style="text-align: center;"><em><CSControl:ResourceControl ResourceName="Gallery_NoExif" runat="server" /></em></div>
			            </NoneTemplate>
			            <FooterTemplate>
				            </ul>
			            </FooterTemplate>
			        </CSGallery:GalleryPostExifList>
	            </div>
            </div>
            
	    </div>
	</div>
    
</asp:Content>

<asp:Content ContentPlaceHolderID="rcr" runat="server">
    <CSUserControl:GallerySidebar runat="server" />
</asp:Content>
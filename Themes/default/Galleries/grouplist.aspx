<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Galleries.Controls.CSGalleryThemePage" MasterPageFile="galleries.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Galleries.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        SetTitle(ResourceManager.GetString("Gallery_AggregateUpdatedGalleriesListing_Title"), Title, true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
    <div class="CommonContentArea">
	    <CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	        <DefaultContentTemplate>
	            <CSUserControl:AdTop runat="server" />
	        </DefaultContentTemplate>
	    </CSControl:AdPart>
	    
	    <CSControl:WrappedLiteral runat="server" ID="Title" Tag="H2" CssClass="CommonTitle" />
	    <div class="CommonContent">
	        <CSGallery:GalleryList runat="server">
	            <QueryOverrides SortBy="LastPost" SortOrder="Descending" PageSize="6" />
	            <HeaderTemplate>
	                <ul class="GalleryList">
	            </HeaderTemplate>
	            <ItemTemplate>
	                <li>
				        <div class="GalleryGalleryArea">
				            <CSGallery:GalleryData runat="server" Property="Name" LinkTo="ViewGallery" Tag="H4" CssClass="GalleryGalleryHeader" />
					        <div class="GalleryGalleryContent">
					            <CSGallery:GalleryPostList runat="server">
					                <QueryOverrides SortBy="PictureDate" SortOrder="Descending" PageSize="5" />
					                <HeaderTemplate>
                                        <ul class="GalleryImageList">
					                </HeaderTemplate>
					                <ItemTemplate>
					                    <li><CSGallery:GalleryPostData LinkTo="ViewPicture" runat="server" Tag="Div" CssClass="GalleryGalleryImage"><ContentTemplate><CSGallery:GalleryPostImage runat="Server" ImageType="Thumbnail" /></ContentTemplate></CSGallery:GalleryPostData></li>
					                </ItemTemplate> 
					                <FooterTemplate>
					                    </ul>
					                </FooterTemplate>
					            </CSGallery:GalleryPostList>

                                <div class="GalleryGalleryFooter">
                                <CSControl:PlaceHolder runat="server">
                                    <DisplayConditions><CSGallery:GalleryPropertyValueComparison ComparisonProperty="MostRecentPostID" Operator="GreaterThan" ComparisonValue="0" runat="server" /></DisplayConditions>
                                    <ContentTemplate>
                                        <CSControl:ResourceControl ResourceName="Gallery_AggregateUpdatedGalleriesListing_UploadedOn" runat="Server" /> 
                                        <CSGallery:GalleryData runat="server" Property="MostRecentPostDate" />
                                        <CSControl:ResourceControl ResourceName="Gallery_AggregateUpdatedGalleriesListing_By" runat="Server" /> 
                                        
                                        <CSGallery:GalleryData runat="server" LinkTo="MostRecentPostAuthor" Property="MostRecentPostAuthor" />
                                    </ContentTemplate>
                                </CSControl:PlaceHolder>
                                </div>
					        </div>
				        </div>
				    </li>
	            </ItemTemplate>
	            <FooterTemplate>
	                </ul>
	            </FooterTemplate>
	        </CSGallery:GalleryList>
	        
	        <CSControl:SiteUrl runat="server" UrlName="gallerylist" ResourceName="Gallery_AggregateUpdatedGalleriesListing_ViewAll" />
	    </div>
	    
	    <CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	        <DefaultContentTemplate>
	            <CSUserControl:AdBottom runat="server" />
	        </DefaultContentTemplate>
	    </CSControl:AdPart>
    </div>
</asp:Content>


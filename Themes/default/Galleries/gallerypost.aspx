<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Galleries.Controls.CSGalleryThemePage" MasterPageFile="galleries.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Galleries.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="GallerySidebar" Src="gallerysidebar.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        if (CurrentGallery != null)
            SetTitle(CurrentGallery.Name, true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">

    <script type="text/javascript">
    // <![CDATA[
    function toggleLayers(whichLayer)
    {
	    if(whichLayer != 'ViewSizes')
		    toggleLayer('ViewSizes', true);
	    if(whichLayer != 'ViewExif')
		    toggleLayer('ViewExif', true);
	    toggleLayer(whichLayer);
    }
    function toggleLayer(whichLayer, forceHide)
    {
	    var element = document.getElementById(whichLayer)
	    if(element != null)
	    {
		    var style2 = element.style;
		    if(forceHide)
			    style2.display =  ""
		    else
			    style2.display = style2.display? "":"block";
	    }
    }
    // ]]>
    </script>

    <div class="CommonContentArea">
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
                <CSControl:UserAvatar runat="server" Tag="Td" CssClass="OwnerAvatar" />
                <td>
                    <CSGallery:GalleryData Property="Name" LinkTo="ViewGallery" runat="server" Tag="h2" CssClass="CommonTitle" />
                    <div class="CommonContent">
                        <CSGallery:GalleryData Property="Description" runat="server" />
                        <div class="ClearLeft">&nbsp;</div>
                    </div>
                </td>
            </tr>
        </table>
    </div>

	<div class="CommonContentArea">
	    <div class="CommonContent">
            <h3 class="ContentHeader"><CSGallery:GalleryPostData Property="Name" runat="server" /></h3>
            <CSGallery:BreadCrumb runat="server" Tag="Div" CssClass="BreadcrumbList" />
            <CSGallery:GalleryPostPager runat="server" ShowTotalSummary="true">
                <LeaderTemplate>
                    <div style="float:right;position:relative;top:-15px;text-align:right;"><div class="PictureListPager"  style="position:relative; top: 8px;">
                </LeaderTemplate>
                <TrailerTemplate>
                    </div></div>
                </TrailerTemplate>
            </CSGallery:GalleryPostPager>
            <div class="clearBoth">&nbsp;</div>

            <div class="PictureDetail">
	            <span>
	                <CSGallery:GalleryPostData runat="server" LinkTo="ImageOriginal"><ContentTemplate>
		                <CSGallery:GalleryPostImage ImageType="Other" Width="425" height="425" quality="70" BorderWidth="1" runat="server"/>
		            </ContentTemplate></CSGallery:GalleryPostData>
	            </span>
	            <div class="photodetailoptions">
		            <a href="javascript:toggleLayers('ViewSizes');" title="View Image Sizes" class="viewsizes">View Sizes</a>
		            <a href="javascript:toggleLayers('ViewExif');" title="View Exif Info" class="viewexif">View Details</a>
		            <div>
		                <div id="ViewExif">
		                    <CSGallery:GalleryPostExifList runat="server" ShowHeaderFooterOnNone="false">
					            <HeaderTemplate>
						            <ul>
					            </HeaderTemplate>
					            <ItemTemplate>
					                <li><CSGallery:GalleryPostExifData Property="Name" runat="server" />: <CSGallery:GalleryPostExifData Property="Value" runat="server" Tag="Span" CssClass="boldtext" /></li>
					            </ItemTemplate>
					            <NoneTemplate>
						            <div style="text-align: center;"><em><CSControl:ResourceControl ResourceName="Gallery_NoExif" runat="server" /></em></div>
					            </NoneTemplate>
					            <FooterTemplate>
						            </ul >
						            <CSGallery:GalleryPostData LinkTo="ViewExif" ResourceName="Gallery_ExifListing_More" runat="server" />
					            </FooterTemplate>
	    			        </CSGallery:GalleryPostExifList>
                        </div>
		                <div id="ViewSizes">
	                        <ul>
	                            <CSGallery:GalleryPostData LinkTo="ImageThumbnail" runat="server" Tag="Li" ResourceName="Gallery_PictureSizes_Thumbnail">
	                                <TrailerTemplate> <span class="emtext">(<CSGallery:GalleryPostData runat="server" Property="ThumbnailWidth" />x<CSGallery:GalleryPostData runat="server" Property="ThumbnailHeight" />)</span></TrailerTemplate>
	                            </CSGallery:GalleryPostData>
	                            <CSGallery:GalleryPostData LinkTo="ImageSecondaryThumbnail" runat="server" Tag="Li" ResourceName="Gallery_PictureSizes_Small">
	                                <TrailerTemplate> <span class="emtext">(<CSGallery:GalleryPostData runat="server" Property="SecondaryThumbnailWidth" />x<CSGallery:GalleryPostData runat="server" Property="SecondaryThumbnailHeight" />)</span></TrailerTemplate>
	                            </CSGallery:GalleryPostData>
	                            <CSGallery:GalleryPostData LinkTo="ImageDetails" runat="server" Tag="Li" ResourceName="Gallery_PictureSizes_Medium">
	                                <TrailerTemplate> <span class="emtext">(<CSGallery:GalleryPostData runat="server" Property="DetailsWidth" />x<CSGallery:GalleryPostData runat="server" Property="DetailsHeight" />)</span></TrailerTemplate>
	                            </CSGallery:GalleryPostData>
	                            <CSGallery:GalleryPostData LinkTo="ImageSlideshow" runat="server" Tag="Li" ResourceName="Gallery_PictureSizes_Large">
	                                <TrailerTemplate> <span class="emtext">(<CSGallery:GalleryPostData runat="server" Property="SlideshowWidth" />x<CSGallery:GalleryPostData runat="server" Property="SlideshowHeight" />)</span></TrailerTemplate>
	                            </CSGallery:GalleryPostData>
	                            <CSGallery:GalleryPostData LinkTo="ImageOriginal" runat="server" Tag="Li" ResourceName="Gallery_PictureSizes_Original">
	                                <TrailerTemplate> <span class="emtext">(<CSGallery:GalleryPostData runat="server" Property="OriginalWidth" />x<CSGallery:GalleryPostData runat="server" Property="OriginalHeight" />)</span></TrailerTemplate>
	                            </CSGallery:GalleryPostData>
	                        </ul>
                        </div>
		            </div>
	            </div>
            </div>

            <div class="PictureDescription">
                <CSGallery:GalleryPostData Property="FormattedBody" IncrementViewCount="true" runat="server" />
                <div><CSGallery:GalleryPostTagEditableList runat="server" EditorLinkCssClass="CommonTextButton" EditorCssClass="CommonInlineTagEditor" /></div>
            </div>

            <div id="comments">
                <CSControl:ResourceControl runat="server" ResourceName="Gallery_CommentListing_Comments" Tag="H4" Class="CommonSubTitle" />
                <CSGallery:GalleryPostFeedbackList runat="server">
                    <QueryOverrides PageSize="999999" />
                    <HeaderTemplate>
				        <table cellpadding="0" cellspacing="0" border="0" width="100%">
			        </HeaderTemplate>
			        <ItemTemplate>
				        <tr valign="top">
					        <td class="Comment CommentAvatar">
						        <CSControl:UserAvatar runat="server" BorderWidth="1" />
					        </td>
					        <td class="Comment CommentContent" width="100%">
						        <h4 class="CommentTitle">
						            <CSGallery:GalleryPostFeedbackData runat="server" Property="DisplayName" LinkTo="User" />
						            <CSControl:ResourceControl runat="server" resourcename="Weblog_CommentForm_Said" />
						        </h4>
						        <div class="CommentText"><CSGallery:GalleryPostFeedbackData Property="FormattedBody" runat="server" /></div>
						        <div class="CommentFooter">
						            <CSGallery:GalleryPostFeedbackData Property="PostDate" runat="server" FormatString="t" /> on <CSGallery:GalleryPostFeedbackData Property="PostDate" runat="server" FormatString="MMMM d, yyyy" />
						            <CSGallery:DeleteGalleryPostFeedbackForm DeleteButtonId="DeleteComment" ConfirmationResourceName="Weblog_Comment_DeleteVerify" runat="server">
                                        <SuccessActions>
				                            <CSControl:GoToModifiedUrlAction runat="server" />
				                        </SuccessActions>
				                        <FormTemplate>
				                            [<asp:LinkButton runat="server" Text="Delete" ID="DeleteComment" />]
				                        </FormTemplate>
				                    </CSGallery:DeleteGalleryPostFeedbackForm>
						        </div>
					        </td>
				        </tr>
			        </ItemTemplate>
			        <FooterTemplate>
				        </table>
			        </FooterTemplate>
			        <NoneTemplate>
				        <tr>
					        <td colspan="2" class="Comment CommentContent"><CSControl:ResourceControl runat="server" resourcename="Weblog_CommentForm_NoComments" /></td>
				        </tr>
			        </NoneTemplate>
		        </CSGallery:GalleryPostFeedbackList>
		    </div>

            <CSControl:ResourceControl runat="server" ResourceName="Blog_Pending_Comment" Tag="Div" CssClass="CommonMessageSuccess">
                <DisplayConditions><CSControl:QueryStringPropertyValueComparison QueryStringProperty="CommentPosted" Operator="IsSetOrTrue" runat="server" /></DisplayConditions>    
                <LeaderTemplate><a name="commentmessage"></a></LeaderTemplate>
            </CSControl:ResourceControl>

            <CSGallery:GalleryPostCommentForm runat="server" 
                ControlIdsToHideFromRegisteredUsers="NameTitle,NameDesc,Remember" 
                MessageTextBoxId="tbComment" 
                NameTextBoxId="tbName" 
                RememberCheckboxId="chkRemember" 
                SubmitButtonId="btnSubmit" 
                UrlTextBoxId="tbUrl"
                ValidationGroup="CreateCommentForm"
                >
                <SuccessActions>
                    <CSControl:GoToModifiedUrlAction runat="server" QueryStringModification="CommentPosted=true" TargetLocationModification="commentmessage" />
                </SuccessActions>
                <FormTemplate>
                     <div class="CommonFormArea">
			            <h4 class="CommonFormTitle"><CSControl:ResourceControl runat="server" ResourceName="CommentListing_AddComment2" ResourceFile="FileGallery.xml" /></h4>
			                    
                        <div class="CommonFormFieldName" id="NameTitle" runat="server">
	                            <CSControl:FormLabel runat="server" ResourceName="Weblog_CommentForm_Name" LabelForId="tbName" />
		                        <em>(<CSControl:ResourceControl runat="server" ResourceName="Required" />)</em>
		                        <asp:RequiredFieldValidator runat="server" ErrorMessage="*" ControlToValidate="tbName" ValidationGroup="CreateCommentForm" />
		                </div>
	                    <div class="CommonFormField" id="NameDesc" runat="server">
	                        <asp:TextBox id="tbName" runat="server" Columns="60" ValidationGroup="CreateCommentForm" />
	                    </div>
	                    
	                    <div class="CommonFormFieldName">
	                         <CSControl:FormLabel runat="server" ResourceName="Weblog_CommentForm_YourUrl" LabelForId="tbUrl" />
		                     <em>(<CSControl:ResourceControl runat="server" ResourceName="Optional" />)</em>
		                </div>
	                    <div class="CommonFormField">
	                        <asp:TextBox id="tbUrl" runat="server" Columns="60" ValidationGroup="CreateCommentForm" />
	                     </div>
	                        
	                     <div class="CommonFormFieldName">
	                        <CSControl:FormLabel runat="server" ResourceName="Weblog_CommentForm_Comments" LabelForId="tbComment" />
		                    <em>(<CSControl:ResourceControl runat="server" ResourceName="Required" />)</em>
		                    <asp:RequiredFieldValidator runat="server" ErrorMessage="*" ControlToValidate="tbComment" ValidationGroup="CreateCommentForm" />
		                 </div>
		                 <div class="CommonFormField">
		                    <asp:TextBox id="tbComment" runat="server" Columns="60" Rows="8" TextMode="MultiLine" ValidationGroup="CreateCommentForm" />
		                 </div>

                         <div class="CommonFormField" id="Remember" runat="server">
                            <asp:CheckBox id="chkRemember" runat="server" Text="Remember Me?" ValidationGroup="CreateCommentForm" />
                         </div>
                         
                         <div class="CommonFormField">
	                        <CSControl:ResourceLinkButton runat="server" id="btnSubmit" CssClass="CommonTextButton" ResourceName="Add" ValidationGroup="CreateCommentForm" />
                         </div>
                    </div>
                </FormTemplate>
            </CSGallery:GalleryPostCommentForm>
		    
	    </div>
	</div>
    
</asp:Content>

<asp:Content ContentPlaceHolderID="rcr" runat="server">
    <CSUserControl:GallerySidebar runat="server" />
</asp:Content>
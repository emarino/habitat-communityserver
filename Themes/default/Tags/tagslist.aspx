<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Controls.CSThemePage" MasterPageFile="../Common/master.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        SetTitle(ResourceManager.GetString("TagBrowser_Title"), Title, true);
    }

</script>

<asp:Content ContentPlaceHolderID="bbcr" runat="server">
    <CSControl:TagBreadCrumb runat="server" ShowHome="false" Tag="Div" CssClass="CommonBreadCrumbArea">
        <DisplayConditions><CSControl:QueryStringPropertyValueComparison QueryStringProperty="Tags" Operator="IsSetOrTrue" runat="server" /></DisplayConditions>
        <LeaderTemplate><div class="Common"></LeaderTemplate>
        <TrailerTemplate>
            <CSControl:TagRssLink runat="server"><LeaderTemplate>(</LeaderTemplate><TrailerTemplate>)</TrailerTemplate></CSControl:TagRssLink>
            </div>
        </TrailerTemplate>
    </CSControl:TagBreadCrumb>
</asp:Content>

<asp:Content ContentPlaceHolderID="bcr" runat="server">

<div class="CommonContentArea">
<CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
    <DefaultContentTemplate>
        <CSUserControl:AdTop runat="server" />
    </DefaultContentTemplate>
</CSControl:AdPart>

<CSControl:WrappedLiteral runat="server" ID="Title" Tag="H2" CssClass="CommonTitle" />
<div class="CommonContent">
	<CSControl:TagCloud TagCssClasses="CommonTag6,CommonTag5,CommonTag4,CommonTag3,CommonTag2,CommonTag1" TagCloudCssClass="CommonTagCloud" runat="server" />

	<CSControl:PlaceHolder runat="server" Tag="Div" CssClass="CommonSearchResultsArea">
	    <DisplayConditions><CSControl:QueryStringPropertyValueComparison QueryStringProperty="Tags" Operator="IsSetOrTrue" runat="server" /></DisplayConditions>
	    <ContentTemplate>

            <CSControl:IndexPostList runat="server">
                <QueryOverrides PagerID="Pager" />
		        <HeaderTemplate>
			        <ul class="CommonSearchResultList">
		        </HeaderTemplate>
		        <ItemTemplate>
			        <li>
			        <CSControl:IndexPostData runat="server" Property="ApplicationType" Text="&lt;div class=&quot;CommonSearchResultArea {0}&quot;&gt;" />
			            <CSControl:IndexPostData runat="server" LinkTo="Post" Property="Title" Tag="H4" CssClass="CommonSearchResultName" />
			            <CSControl:IndexPostData runat="server" Property="FormattedBody" TruncateAt="300" Tag="Div" CssClass="CommonSearchResult" />
				        <div class="CommonSearchResultDetails">
					        <CSControl:ResourceControl runat="server" ResourceName="SearchResults_PostTo" />
					        <CSControl:IndexPostData Property="SectionName" LinkTo="Section" runat="server" />
					        <CSControl:IndexPostData runat="server" LinkTo="Application" Property="ApplicationType" Text="({0})" />
					        <CSControl:ResourceControl runat="server" ResourceName="SearchResults_By" />
					        <CSControl:IndexPostData runat="Server" LinkTo="Author" Property="Username" />
					        <CSControl:ResourceControl runat="server" ResourceName="SearchResults_On" />
					        <CSControl:IndexPostData runat="server" Property="PostDate" />
					        <%# (bool) Eval("HasPostCategories") ? String.Concat("<br>", ResourceManager.GetString("TagListTitle"), String.Join(", ", (string[])Eval("PostCategories"))) : ""%>
				        </div>
			        </div>
			        </li>
		        </ItemTemplate>
		        <FooterTemplate>
			        </ul>
		        </FooterTemplate>
            </CSControl:IndexPostList>

	        <CSControl:Pager id="Pager" runat="server" Tag="Div" ShowTotalSummary="true" CssClass="CommonPagingArea" />

	    </ContentTemplate>
	</CSControl:PlaceHolder>
</div>
<CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
    <DefaultContentTemplate>
        <CSUserControl:AdBottom runat="server" />
    </DefaultContentTemplate>
</CSControl:AdPart>
</div>

</asp:Content>
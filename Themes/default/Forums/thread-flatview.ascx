<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Discussions.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        CSForumThemePage page = Page as CSForumThemePage;
        if (page != null && page.CurrentThread != null)
        {
            page.SetTitle(page.CurrentThread.Subject, Title, true);

            Description.Text = string.Format(ResourceManager.GetString("PostFlatView_BriefInfo"),
                Formatter.FormatUsername(page.CurrentThread.MostRecentPostAuthorID, page.CurrentThread.MostRecentPostAuthor, false),
                Formatter.FormatDate(page.CurrentThread.ThreadDate, true),
                page.CurrentThread.Replies);

            ((CommunityServer.Controls.Pager)ForumControlUtility.Instance().FindControl(this, "TopPager")).PagerUrlFormat = Globals.GetSiteUrls().UrlData.FormatUrl("thread_Paged", page.CurrentThread.ThreadID, "{0}");
            ((CommunityServer.Controls.Pager)ForumControlUtility.Instance().FindControl(this, "BottomPager")).PagerUrlFormat = Globals.GetSiteUrls().UrlData.FormatUrl("thread_Paged", page.CurrentThread.ThreadID, "{0}");

            if (!string.IsNullOrEmpty(Request.QueryString["PermaPostID"]) && !page.IsPostBack)
            {
                int permaPostId = int.Parse(Request.QueryString["PermaPostID"]);
                ((CommunityServer.Controls.IPager)ForumControlUtility.Instance().FindControl(this, "Pager")).PageIndex = page.GetPageIndexContainingPost(page.CurrentThread.ThreadID, permaPostId, page.ForumConfiguration.PostsPerPage);
            }
        }
    }

</script>

<div class="CommonContentArea">
	<CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdTop runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
	
    <CSControl:WrappedLiteral runat="server" id="Title" Tag="h2" CssClass="CommonTitle" />		
	<div class="CommonContent">
	
	    <CSControl:WrappedLiteral runat="server" ID="Description" Tag="Div" CssClass="CommonDescription" />
        
        <div class="CommonFormArea">
		    <table cellpadding="0" cellspacing="0" border="0" width="100%">
			    <tr>
				    <td colspan="2" align="right" class="CommonFormField CommonPrintHidden">
				        <CSControl:Pager runat="server" id="TopPager" ShowTotalSummary="true" Tag="Div" CssClass="CommonPagingArea" align="right" />
				    </td>
			    </tr>
			    <tr>
				    <td align="left" class="CommonFormField CommonPrintHidden">
				        <CSForum:ThreadRating runat="server" RatingCssClass="ForumThreadRateControl" RatingReadOnlyCssClass="ForumThreadRateControlReadOnly" RatingActiveCssClass="ForumThreadRateControlActive" ImagesBaseUrl="~/Themes/default/images/common/" EnableDetails="true" />
				        <CSForum:ThreadSubscriptionToggleButton runat="server" ButtonOnCssClass="CommonImageTextButton CommonEmailSubscriptionButtonEnabled" ButtonOffCssClass="CommonImageTextButton CommonEmailSubscriptionButtonDisabled" ButtonProcessingCssClass="CommonImageTextButton CommonEmailSubscriptionButtonProcessing">
				           <OnTemplate><CSControl:ResourceControl runat="server" ResourceName="PostFlatView_EnableThreadTrackingOn" /></OnTemplate>
				           <OffTemplate><CSControl:ResourceControl runat="server" ResourceName="PostFlatView_EnableThreadTrackingOff" /></OffTemplate>
				           <ProcessingTemplate>...</ProcessingTemplate>
				        </CSForum:ThreadSubscriptionToggleButton>
				        <CSForum:ChangeViewPopupMenu runat="server" ResourceName="ChangeViewPopupMenu_Text" ButtonActiveCssClass="CommonImageTextButtonHighlight CommonViewButton" ButtonInactiveCssClass="CommonImageTextButton CommonViewButton" MenuGroupCssClass="CommonContextMenuGroup" MenuItemCssClass="CommonContextMenuItem" MenuItemSelectedCssClass="CommonContextMenuItemHover" MenuItemExpandedCssClass="CommonContextMenuItemExpanded" MenuItemIconWidth="20" MenuItemIconHeight="20" MenuDirection="UpDown" />
				    </td>
				    <td align="right" class="CommonFormField CommonPrintHidden">
				        <CSForum:ForumPostListFilterForm runat="server" ForumPostListId="PostList" PostAgeSortOrderDropDownListId="SortOrder">
				            <FormTemplate>
				                <CSControl:ResourceControl runat="server" ResourceName="PostView_SortPostsLabel" />
				                <asp:DropDownList runat="server" ID="SortOrder" />
				            </FormTemplate>
				        </CSForum:ForumPostListFilterForm>
					    <CSForum:ThreadData runat="server" LinkTo="PreviousThread" ResourceName="Button_PreviousTopic" LinkCssClass="CommonImageTextButton CommonPreviousButton" />
					    <CSForum:ThreadData runat="server" LinkTo="NextThread" ResourceName="Button_NextTopic" LinkCssClass="CommonImageTextButton CommonNextButton" />
				    </td>
			    </tr>
		    </table>
		</div>
        
        <CSForum:ForumPostList runat="server" ID="PostList">
            <QueryOverrides SortBy="PostDate" PagerID="Pager" />
            <HeaderTemplate><ul class="ForumPostList"></HeaderTemplate>
            <ItemTemplate>
			    <li>
			        <a name="<%# Eval("PostID") %>"></a>
				    <div class="ForumPostArea">
					    <h4 class="ForumPostHeader">
						    <table cellpadding="0" cellspacing="0" border="0" width="100%">
							    <tr valign="middle">
								    <td align="left">
								        <CSForum:ForumPostData runat="server" LinkTo="PermaLink"><ContentTemplate><CSControl:ThemeImage runat="server" BorderWidth="0" ImageUrl="~/images/icon_post_show.gif" /></ContentTemplate></CSForum:ForumPostData>
								        <CSForum:ForumPostData runat="server" Property="PostDate" IncludeTimeInDate="true" />    
								    </td>
								    <td align="right">
								    
								    <CSForum:ForumPostData runat="server" ResourceName="PostView_ReplyToLabel" LinkTo="ParentPost" />
								    <CSForum:ForumPostRating runat="server" RatingCssClass="CommonRateControl" RatingReadOnlyCssClass="CommonRateControlReadOnly" RatingActiveCssClass="CommonRateControlActive" ImagesBaseUrl="~/Themes/default/images/common/" />
								    </td>
							    </tr>
						    </table>
					    </h4>
					    <table cellspacing="0" cellpadding="0" border="0" width="100%">
						    <tr valign=top>
							    <td rowspan="2" class="ForumPostUserArea">
								    <div class="ForumPostUserContent">
								    <ul class="ForumPostUserPropertyList">
									    <li class="ForumPostUserName">
									        <CSControl:PlaceHolder runat="server">
									            <DisplayConditions Operator="Not"><CSControl:UserPropertyValueComparison runat="server" ComparisonProperty="IsAnonymous" Operator="IsSetOrTrue" /></DisplayConditions>
									            <ContentTemplate>
									                <CSControl:PlaceHolder runat="server">
									                    <DisplayConditions><CSControl:UserPropertyValueComparison ComparisonProperty="IsOnline" Operator="IsSetOrTrue" runat="server" /></DisplayConditions>
									                    <ContentTemplate><CSControl:ThemeImage ImageUrl="~/images/user_IsOnline.gif" runat="server" /></ContentTemplate>
									                </CSControl:PlaceHolder>
									                <CSControl:PlaceHolder runat="server">
									                    <DisplayConditions Operator="Not"><CSControl:UserPropertyValueComparison ComparisonProperty="IsOnline" Operator="IsSetOrTrue" runat="server" /></DisplayConditions>
									                    <ContentTemplate><CSControl:ThemeImage ImageUrl="~/images/user_IsOffline.gif" runat="server" /></ContentTemplate>
									                </CSControl:PlaceHolder>
									            </ContentTemplate>
									        </CSControl:PlaceHolder>
									        <CSControl:UserData runat="server" LinkTo="Profile" Property="DisplayName" />
									    </li>
									    <CSControl:UserAvatar runat="server" BorderWidth="1" Tag="Li" CssClass="ForumPostUserAvatar" />
									    <CSControl:PlaceHolder runat="server">
								            <DisplayConditions Operator="Not"><CSControl:UserPropertyValueComparison runat="server" ComparisonProperty="IsAnonymous" Operator="IsSetOrTrue" /></DisplayConditions>
								            <ContentTemplate>
								                <CSControl:UserPostIcons runat="server" Tag="Li" CssClass="ForumPostUserIcons" />
								                <CSControl:UserData runat="server" Property="DateCreated" Tag="Li" CssClass="ForumPostUserAttribute"><LeaderTemplate><CSControl:ResourceControl runat="server" ResourceName="PostFlatView_Joined" /></LeaderTemplate></CSControl:UserData>
								                <CSControl:UserProfileData runat="server" Property="Location" Tag="Li" CssClass="ForumPostUserAttribute" />
								                <CSControl:UserData runat="server" LinkTo="PostsSearch" Property="TotalPosts" Tag="Li" CssClass="ForumPostUserAttribute">
								                    <DisplayConditions><CSControl:SiteSettingsPropertyValueComparison runat="server" ComparisonProperty="PostingActivityDisplay" ComparisonValue="PostCount" Operator="EqualTo" /></DisplayConditions>
								                    <LeaderTemplate><CSControl:ResourceControl ResourceName="PostFlatView_Posts" runat="server" /></LeaderTemplate>
								                </CSControl:UserData>
								                <CSControl:UserData runat="server" LinkTo="PostsSearch" Property="UserRank" Tag="Li" CssClass="ForumPostUserAttribute">
								                    <DisplayConditions><CSControl:SiteSettingsPropertyValueComparison runat="server" ComparisonProperty="PostingActivityDisplay" ComparisonValue="UserRank" Operator="EqualTo" /></DisplayConditions>
								                </CSControl:UserData>
                                                <CSControl:UserData runat="server" Property="Points" Tag="Li" CssClass="ForumPostUserAttribute"><LeaderTemplate><CSControl:ResourceControl runat="server" ResourceName="PostFlatView_UserPoints" /></LeaderTemplate></CSControl:UserData>
                                                <CSControl:UserRoleIcons runat="server" LinkTo="Role" Tag="Li" CssClass="ForumPostRoleIcons">
                                                    <SeparatorTemplate><br /></SeparatorTemplate>
                                                </CSControl:UserRoleIcons>
                                            </ContentTemplate>
                                        </CSControl:PlaceHolder>
								    </ul>
								    </div>
							    </td>
							    <td class="ForumPostContentArea">
								    <div class="ForumPostTitleArea">
									    <h4 class="ForumPostTitle">
									        <CSControl:PostEmoticon runat="server" />
										    <CSForum:ForumPostData runat="server" Property="Subject" />
									    </h4>
    									
								        <CSForum:PostAttachmentData runat="server" Property="FileName" LinkTo="Attachment" Tag="Div" CssClass="ForumPostAttachment" />

								        <div class="ForumPostThreadStatus">
								            <CSForum:ThreadStatusForm runat="server" StatusDropDownListId="ThreadStatus">
								                <FormTemplate>
                                                    <asp:DropDownList runat="server" ID="ThreadStatus" />								                
								                </FormTemplate>
								            </CSForum:ThreadStatusForm>
								            <CSForum:ForumPostAnswerToggleButton ID="AnswerToggleButton" runat="server" ButtonOnCssClass="CommonTextButton" ButtonOffCssClass="CommonTextButton" ButtonProcessingCssClass="CommonTextButton">
                                                <OffTemplate><CSControl:ResourceControl runat="server" ResourceName="MarkAsAnswer" /></OffTemplate>
                                                <OnTemplate><CSControl:ResourceControl runat="server" ResourceName="MarkAsNotAnswer" /></OnTemplate>
                                                <ProcessingTemplate>...</ProcessingTemplate>
						                    </CSForum:ForumPostAnswerToggleButton>
						                    <CSControl:ResourceControl runat="server" ResourceName="Button_Answer" Tag="Span" CssClass="CommonTextButton">
		                                        <DisplayConditions Operator="And">
		                                            <CSControl:ControlVisibilityCondition runat="server" ControlId="AnswerToggleButton" ControlVisiblilityEquals="false" />
		                                            <CSForum:ForumPostPropertyValueComparison ComparisonProperty="PostStatus" Operator="Contains" ComparisonValue="IsAnswer" runat="server" />
		                                        </DisplayConditions>
		                                    </CSControl:ResourceControl>
								        </div>

									    <div class="ForumPostButtons">
									        <CSControl:ResourceControl runat="server" ResourceName="Button_Locked" CssClass="CommonImageTextButton CommonLockedButton" Tag="Span">
									            <DisplayConditions><CSForum:ForumPostPropertyValueComparison ComparisonProperty="IsLocked" Operator="IsSetOrTrue" runat="server" /></DisplayConditions>
									        </CSControl:ResourceControl>
									        <CSForum:ForumPostData runat="server" LinkTo="Reply" ResourceName="Button_Reply" LinkCssClass="CommonImageTextButton CommonReplyButton" />
									        <CSForum:ForumPostData runat="server" LinkTo="Delete" ResourceName="Button_Delete" LinkCssClass="CommonImageTextButton CommonDeleteButton" />
									        <CSForum:ForumPostData runat="server" LinkTo="Edit" ResourceName="Button_Edit" LinkCssClass="CommonImageTextButton CommonEditButton" />
									        <CSControl:FavoritePopupMenu ResourceName="FavoritePopupMenu_Text" runat="server" ButtonActiveCssClass="CommonImageTextButtonHighlight CommonFavoriteButton" ButtonInactiveCssClass="CommonImageTextButton CommonFavoriteButton" MenuGroupCssClass="CommonContextMenuGroup" MenuItemCssClass="CommonContextMenuItem" MenuItemSelectedCssClass="CommonContextMenuItemHover" MenuItemExpandedCssClass="CommonContextMenuItemExpanded" MenuItemIconWidth="20" MenuItemIconHeight="20" MenuDirection="UpDown" />
									        <CSControl:UserContactPopupMenu ResourceName="ContactPopupMenu_Text" runat="server" ButtonActiveCssClass="CommonImageTextButtonHighlight CommonContactButton" ButtonInactiveCssClass="CommonImageTextButton CommonContactButton" MenuGroupCssClass="CommonContextMenuGroup" MenuItemCssClass="CommonContextMenuItem" MenuItemSelectedCssClass="CommonContextMenuItemHover" MenuItemExpandedCssClass="CommonContextMenuItemExpanded" MenuItemIconWidth="20" MenuItemIconHeight="20" MenuDirection="UpDown" />
									        <CSForum:ForumPostModerationPopupMenu ResourceName="ModerationPopupMenu_Text" runat="server" ButtonActiveCssClass="CommonTextButtonHighlight" ButtonInactiveCssClass="CommonTextButton" MenuGroupCssClass="CommonContextMenuGroup" MenuItemCssClass="CommonContextMenuItem" MenuItemSelectedCssClass="CommonContextMenuItemHover" MenuItemExpandedCssClass="CommonContextMenuItemExpanded" MenuItemIconWidth="20" MenuItemIconHeight="20" MenuDirection="UpDown">
									            <SuccessActions><CSControl:GoToModifiedUrlAction runat="server" /></SuccessActions>
									        </CSForum:ForumPostModerationPopupMenu>
									    </div>
								    </div>
    								
								    <table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%" style="table-layout: fixed;">
									    <tr><td>
									    <div class="ForumPostBodyArea">
									    <div class="ForumPostContentText">
										    <CSForum:ForumPostData Property="FormattedBody" runat="server" IncrementViewCount="true" />
										    <CSForum:ForumPostData Property="EditNotes" runat="server">
			                                    <DisplayConditions><CSForum:ForumConfigurationPropertyValueComparison runat="server" ComparisonProperty="DisplayEditNotesInPost" Operator="IsSetOrTrue" /></DisplayConditions>
			                                    <LeaderTemplate><br /><br /></LeaderTemplate>
			                                </CSForum:ForumPostData>
									    </div>
									    <CSControl:UserProfileData Property="SignatureFormatted" runat="server" Tag="Div" CssClass="ForumPostSignature" />
									    </div>
									    </td></tr>
								    </table>
							    </td>
						    </tr>
						    <tr valign="bottom">
							    <td class="ForumPostFooterArea">	
								    <CSForum:ForumPostTagEditableList runat="server" EditorLinkCssClass="CommonTextButton" EditorCssClass="CommonInlineTagEditor" Tag="Div" />
								    <ul class="ForumPostStatistics CommonPrintHidden" style="clear: both;">
								        <li></li>
								        <CSForum:ForumPostData runat="server" Property="Points" ResourceName="PostFlatView_PostPoints" Tag="Li" />
									    <CSForum:ForumPostData runat="server" Property="UserHostAddress" ResourceName="PostFlatView_IPAddress" Tag="Li" />
									    <CSForum:ForumPostData runat="server" LinkTo="Report" ResourceName="Report" Tag="Li" />
									    <CSForum:ForumPostData runat="server" LinkTo="QuickReply" ResourceName="QuickReply" Tag="Li" />
								    </ul>
							    </td>
						    </tr>
					    </table>
				    </div>
			    </li>
            </ItemTemplate>
            <FooterTemplate>
		        </ul>
		    </FooterTemplate>
        </CSForum:ForumPostList>
        
        <CSControl:PagerGroup runat="server" Id="Pager" PagerIds="TopPager,BottomPager" />
        <CSControl:Pager runat="server" id="BottomPager" ShowTotalSummary="true" Tag="Div" CssClass="CommonPagingArea" align="right" />
        
        <CSForum:ThreadData LinkTo="Rss" runat="server">
            <LeaderTemplate>
                <table cellpadding="0" cellspacing="0" width="100%" border="0">
                    <tr>
                        <td colspan="2" align="right">
	                        <div class="CommonWebFeedArea CommonPrintHidden">
            </LeaderTemplate>
            <ContentTemplate><CSControl:Image ImageUrl="~/utility/images/rss.gif" BorderWidth="0" runat="server" /></ContentTemplate>
            <TrailerTemplate>
                            </div>
                        </td>
                    </tr>
                </table>
            </TrailerTemplate>
        </CSForum:ThreadData>
					
	</div>
	
	<CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdBottom runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
</div>
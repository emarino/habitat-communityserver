<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Discussions.Controls.CSForumThemePage" MasterPageFile="forums.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Discussions.Components" %>
<%@ Import Namespace="CommunityServer.Discussions.Controls" %>

<script language="C#" runat="server">

    void Page_Init()
    {
        if (CurrentThread != null)
        {
            if (ChangeViewPopupMenu.GetPostViewType(CurrentThread) == PostViewType.Threaded)
                ViewContainer.Controls.Add(Page.LoadControl("thread-threadedview.ascx"));
            else
                ViewContainer.Controls.Add(Page.LoadControl("thread-flatview.ascx"));
        }
        
        if (CurrentForum != null && !Permissions.ValidatePermissions(CurrentForum, Permission.Read, CurrentCSContext.User))
            PermissionBase.RedirectOrException(CSExceptionType.AccessDenied);
    }

</script>

<asp:Content ContentPlaceHolderID="bbcr" runat="server">
    <CSForum:BreadCrumb runat="server" Tag="Div" CssClass="CommonBreadCrumbArea">
        <LeaderTemplate><div class="Common"></LeaderTemplate>
        <TrailerTemplate></div></TrailerTemplate>
    </CSForum:BreadCrumb>
</asp:Content>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
    <asp:PlaceHolder runat="server" ID="ViewContainer" />
</asp:Content>

<asp:Content ContentPlaceHolderID="rcr" runat="server" />
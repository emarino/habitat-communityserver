<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Discussions.Controls.CSForumThemePage" MasterPageFile="forums.Master" %>
<%@ Register TagPrefix="CSUserControl" TagName="PostPreview" Src="post-preview.ascx" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Discussions.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        if (CurrentForumPost != null)
        {
            SetTitle(ResourceManager.GetString("CreateEditPost_Title_EditMessage"), Title, true);
            Description.Text = string.Format(ResourceManager.GetString("CreateEditPost_Title_EditMessageDesc"), CurrentPost.Subject);
        }
        else if (PostForm.ReplyToPost != null)
        {
            SetTitle(ResourceManager.GetString("CreateEditPost_Title_ReplyMessage"), Title, true);
            Description.Text = string.Format(ResourceManager.GetString("CreateEditPost_Inline1"), PostForm.ReplyToPost.Subject);

            TextBox subject = ForumControlUtility.Instance().FindControl(this, "PostSubject") as TextBox;
            if (subject != null && !IsPostBack)
            {
                if (!PostForm.ReplyToPost.Subject.StartsWith(ResourceManager.GetString("CreateEditPost_ReplyPrePend")))
                    subject.Text = ResourceManager.GetString("CreateEditPost_ReplyPrePend") + Server.HtmlDecode(PostForm.ReplyToPost.Subject);
                else
                    subject.Text = Server.HtmlDecode(PostForm.ReplyToPost.Subject);
            }
        }
        else
        {
            SetTitle(ResourceManager.GetString("CreateEditPost_Title_PostNewMessage"), Title, true);
            Description.Text = ResourceManager.GetString("CreateEditPost_Title_PostNewMessage");
        }

        if (PostForm.ReplyToPost != null)
        {
            CommunityServer.Controls.IText replyDetails = ForumControlUtility.Instance().FindTextControl(this, "ReplyDetails");
            if (replyDetails != null)
                replyDetails.Text = string.Format(ResourceManager.GetString("CreateEditPost_ReplyFormat"), PostForm.ReplyToPostAuthorName, Formatter.FormatDate(PostForm.ReplyToPost.PostDate, true));
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        CheckBox includeVideo = ForumControlUtility.Instance().FindControl(this, "IncludeVideo") as CheckBox;
        CommunityServer.Controls.PlaceHolder videoContainer = ForumControlUtility.Instance().FindControl(this, "VideoContainer") as CommunityServer.Controls.PlaceHolder;

        if (includeVideo != null && videoContainer != null)
            videoContainer.Attributes["style"] = includeVideo.Checked ? "display: block;" : "display: none";

        CheckBox includePoll = ForumControlUtility.Instance().FindControl(this, "IncludePoll") as CheckBox;
        CommunityServer.Controls.PlaceHolder pollContainer = ForumControlUtility.Instance().FindControl(this, "PollContainer") as CommunityServer.Controls.PlaceHolder;

        if (includePoll != null && pollContainer != null)
            pollContainer.Attributes["style"] = includePoll.Checked ? "display: block;" : "display: none";
    }

</script>

<asp:Content ContentPlaceHolderID="bbcr" runat="server">
    <CSForum:BreadCrumb runat="server" Tag="Div" CssClass="CommonBreadCrumbArea">
        <LeaderTemplate><div class="Common"></LeaderTemplate>
        <TrailerTemplate></div></TrailerTemplate>
    </CSForum:BreadCrumb>
</asp:Content>

<asp:Content ContentPlaceHolderID="bcr" runat="server">

<script type="text/javascript">
// <![CDATA[
       function ToggleInk(cb)
       {
            if(cb.checked)
                document.getElementById('inkWrapper').style.position= 'static';
            else
                document.getElementById('inkWrapper').style.position= 'absolute';
       }
       function InkHidden()
       {
            InkCheck(false);
       }
       function InkVisible()
       {
            InkCheck(true);
       }
       function InkCheck(showInk)
       {
            var inkCheckCB = document.getElementById('<%= ForumControlUtility.Instance().FindControl(this, "EnableInk").ClientID %>');
            var inkCheckInkEditor = document.getElementById('inkWrapper');
            if(showInk && inkCheckCB && inkCheckCB.checked)
                inkCheckInkEditor.style.position= 'static';
            else
                inkCheckInkEditor.style.position= 'absolute';
 			
            return true;
      }
// ]]>
</script>

<div class="CommonContentArea">
	<CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdTop runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
	
    <CSControl:WrappedLiteral runat="server" id="Title" Tag="h2" CssClass="CommonTitle" />		
	<div class="CommonContent">
	
	    <CSControl:WrappedLiteral runat="server" ID="Description" Tag="Div" CssClass="CommonDescription" />
        
        <CSForum:CreateEditForumPostForm runat="server"
            ID="PostForm"
            CurrentEditNotesTextBoxId="CurrentEditNotesBody" 
            EditNotesBodyTextBoxId="EditNotesBody" 
            IsAnonymousPostCheckBoxId="IsAnonymousPost" 
            IsLockedCheckBoxId="IsLocked" 
            PinnedPostDropDownListId="PinnedPost" 
            PostBodyEditorId="PostBody" 
            PostIconRadioButtonListId="PostIcon" 
            PostSubjectTextBoxId="PostSubject" 
            PreviewForumPostListId="PreviewPostList"
            ReplyToAuthorTextId="" 
            ReplyToBodyTextId="ReplyBody" 
            SubFormIds="TagsSubForm,AttachmentSubForm,VideoSubForm,PollSubForm,InkSubForm" 
            SubmitButtonId="PostButton" 
            SubscribeToThreadCheckBoxId="SubscribeToThread"
            ValidationGroup="PostFormValidation"
            EditNotesValidationGroup="EditNotesValidation"
            QuoteButtonId="QuoteButton"
            RecipientsTextId="Recipients"
            >
            <ModeratedSuccessActions>
                <CSControl:GoToSiteUrlAction UrlName="post_PendingModeration" Parameter1='<%# CurrentSection.SectionID %>' runat="server" />
            </ModeratedSuccessActions>
            <UnmoderatedSuccessActions>
                <CSForum:GoToCurrentPostAction runat="server" />
            </UnmoderatedSuccessActions>
            <FormTemplate>
                <TWC:TabbedPanes id="EditorTabs" runat="server"
		            PanesCssClass="CommonPane"
		            TabSetCssClass="CommonPaneTabSet"
		            TabCssClasses="CommonPaneTab,CommonPaneTab1,CommonPaneTab2"
		            TabSelectedCssClasses="CommonPaneTabSelected,CommonPaneTabSelected1,CommonPaneTabSelected2"
		            TabHoverCssClasses="CommonPaneTabHover,CommonPaneTabHover1,CommonPaneTabHover2"
		            >
		            <TWC:TabbedPane runat="server" ID="ComposePane">
			            <Tab OnClickClientFunction="InkHidden"><CSControl:ResourceControl runat="server" ResourceName="CreateEditPost_Tab_Compose" /></Tab>
			            <Content>
				            <CSControl:PlaceHolder runat="server">
				                <DisplayConditions><CSControl:ControlVisibilityCondition ControlId="ReplyBody" ControlVisiblilityEquals="true" runat="server" /></DisplayConditions>
				                <ContentTemplate>
					                <strong><asp:Literal id="ReplyDetails" Runat="server" /></strong>
					                <div class="ForumReplyToPostArea">
					                    <div>
					                        <asp:Literal id="ReplyBody" runat="server" />
					                    </div>
					                    
					                    <CSControl:ResourceLinkButton ID="QuoteButton" runat="server" CssClass="CommonTextButton" ResourceName="Button_Quote" />
					                </div>
					             </ContentTemplate>
				            </CSControl:PlaceHolder>

				            <div class="CommonFormArea">
					            
					            <CSControl:WrappedLiteral runat="server" ID="Recipients">
					                <DisplayConditions>
					                    <CSForum:ForumPropertyValueComparison runat="server" ComparisonProperty="SectionID" Operator="equalTo" ComparisonValue="0" />
					                </DisplayConditions>
					                <LeaderTemplate>
					                    <div class="CommonFormFieldName">
						                    <CSControl:ResourceControl runat="server" ResourceName="PrivateMessages_Recipients" /> 
					                    </div>
					                    <div class="CommonFormField">
					                </LeaderTemplate>
					                <TrailerTemplate>
					                    </div>
					                </TrailerTemplate>
					            </CSControl:WrappedLiteral>
          					           					
					            <div class="CommonFormFieldName">
						            <CSControl:FormLabel LabelForId="PostSubject" runat="server" ResourceName="CreateEditPost_Inline7" /> <asp:requiredfieldvalidator id="postSubjectValidator" runat="server" CssClass="validationWarning" ControlToValidate="PostSubject" ErrorMessage="*" ValidationGroup="PostFormValidation" />
					            </div>
					            <div class="CommonFormField">
						            <asp:textbox autocomplete="off" id="PostSubject" CssClass="CommonInputBig" runat="server" style="width:80%;" />	
					            </div>
            					
					            <div class="CommonFormFieldName">
						            <CSControl:FormLabel LabelForId="PostBody" runat="server" ResourceName="CreateEditPost_Body" />
						            <asp:requiredfieldvalidator id="postBodyValidator" runat="server" CssClass="validationWarning" ControlToValidate="PostBody" EnableClientScript="False" ValidationGroup="PostFormValidation">*</asp:requiredfieldvalidator>
						            <CSControl:QuoteValidator id="postBodyQuoteValidator" runat="server" CssClass="validationWarning" ControlToValidate="PostBody" EnableClientScript="false" ValidationGroup="PostFormValidation">Non matching quote blocks in post</CSControl:QuoteValidator>
					            </div>
					            <div class="CommonFormField">
						            <CSControl:Editor Width="100%" runat="Server" id="PostBody" />
					            </div>
            					
            					<CSForum:PostTagsSubForm runat="server" ID="TagsSubForm" SelectTagsModalButtonId="SelectTags" TagsTextBoxId="Tags">
            					    <FormTemplate>
            					        <div class="CommonFormFieldName">
            					            <CSControl:FormLabel LabelForId="Tags" runat="server" ResourceName="CreateEditPost_Tags" />
            					        </div>
            					        <div class="CommonFormField">
            					            <asp:TextBox runat="server" ID="Tags" Columns="70" /> <CSControl:Modal ModalType="Button" Width="400" Height="350" ID="SelectTags" ResourceName="TagEditor_SelectTags" runat="server" />
            					        </div>
            					    </FormTemplate>
            					</CSForum:PostTagsSubForm>
            					
					            <CSControl:PlaceHolder Runat="server" ID="EditNotesArea">
					                <DisplayConditions><CSControl:ControlVisibilityCondition ControlId="EditNotesBody" ControlVisiblilityEquals="true" runat="server" /></DisplayConditions>
					                <ContentTemplate>
						                <div class="CommonFormFieldName">
							                <CSControl:FormLabel runat="server" ResourceName="CreateEditPost_EditNotes" LabelForId="EditNotesBody" />
						                </div>
						                <div class="CommonFormField">
							                <asp:textbox id="EditNotesBody" runat="server" columns="90" TextMode="MultiLine" rows="5"></asp:textbox>
							                <asp:requiredfieldvalidator id="editNotesValidator" runat="server" CssClass="validationWarning" ControlToValidate="EditNotesBody" ValidationGroup="EditNotesValidation" />
						                </div>
						                <div class="CommonFormFieldName">
							                <CSControl:FormLabel LabelForId="CurrentEditNotesBody" runat="server" ResourceName="CreateEditPost_CurrentEditNotes" />
						                </div>
						                <div class="CommonFormField">
							                <asp:textbox ReadOnly="true" id="CurrentEditNotesBody" runat="server" columns="90" TextMode="MultiLine" rows="5"></asp:textbox>
						                </div>
						            </ContentTemplate>
					            </CSControl:PlaceHolder>
				            </div>
			            </Content>
		            </TWC:TabbedPane>
		            <TWC:TabbedPane runat="server" id="OptionsPane">
			            <Tab OnClickClientFunction="InkHidden"><CSControl:ResourceControl runat="server" ResourceName="CreateEditPost_Tab_Options" /></Tab>
			            <Content>
			                <CSForum:PostAttachmentSubForm ID="AttachmentSubForm" runat="server" AddUpdateAttachmentModalId="AddUpdateAttachment" FilenameLabelId="Filename" RemoveAttachmentButtonId="RemoveAttachment">
		                        <FormTemplate>
		                            <div class="CommonFormFieldName">
						                <CSControl:FormLabel LabelForId="Filename" runat="server" ResourceName="CreateEditPost_Attachment" />
					                </div>
					                <div class="CommonFormField">
						                <asp:Label id="Filename" runat="server" />
						                <CSControl:modal modaltype="Link" CssClass="CommonTextButton" width="640" height="180" runat="Server" id="AddUpdateAttachment" ResourceName="CreateEditPost_AddUpdateAttachment" />
						                <CSControl:ResourceLinkButton runat="server" ID="RemoveAttachment" CssClass="CommonTextButton" ResourceName="CreateEditPost_RemoveAttachment" />
					                </div>
					                <p />
		                        </FormTemplate>
		                    </CSForum:PostAttachmentSubForm>

				            <CSControl:PlaceHolder Tag="div" CssClass="CommonFormField" runat="server">
				                <DisplayConditions><CSControl:ControlVisibilityCondition ControlId="IsLocked" ControlVisiblilityEquals="true" runat="server" /></DisplayConditions>
				                <ContentTemplate>
					                <asp:checkbox id="IsLocked" runat="server" /><CSControl:FormLabel runat="server" LabelForId="IsLocked" ResourceName="CreateEditPost_IsLocked" />
					            </ContentTemplate>
				            </CSControl:PlaceHolder>
        					
				            <CSControl:PlaceHolder Tag="div" CssClass="CommonFormField" runat="server">
				                <DisplayConditions><CSControl:ControlVisibilityCondition ControlId="SubscribeToThread" ControlVisiblilityEquals="true" runat="server" /></DisplayConditions>
				                <ContentTemplate>
					                <asp:checkbox id="SubscribeToThread" Checked="true" runat="server"/><CSControl:FormLabel runat="server" LabelForId="SubscribeToThread" ResourceName="CreateEditPost_SubscribeToThread" />
				                </ContentTemplate>
				            </CSControl:PlaceHolder>
        					
				            <CSControl:PlaceHolder Tag="div" CssClass="CommonFormField" runat="server">
				                <DisplayConditions>
				                    <CSControl:Conditions Operator="not" runat="server">
				                        <CSControl:UserPropertyValueComparison runat="server" ComparisonProperty="IsAnonymous" Operator="IsSetOrTrue" />
				                    </CSControl:Conditions>
				                    <CSForum:ForumConfigurationPropertyValueComparison runat="server" ComparisonProperty="EnableUserPostingAsAnonymous" Operator="issetortrue" />
				                    <CSForum:ForumPropertyValueComparison runat="server" ComparisonProperty="EnableAnonymousPosting" Operator="issetortrue" />
				                    <CSControl:SiteSettingsPropertyValueComparison runat="server" ComparisonProperty="EnableAnonymousUserPosting" Operator="issetortrue" />
				                </DisplayConditions>
				                <ContentTemplate>
					                <asp:checkbox id="IsAnonymousPost" runat="server" /><CSControl:FormLabel runat="server" LabelForId="IsAnonymousPost" ResourceName="CreateEditPost_IsAnonymousPost" />
				                </ContentTemplate>
				            </CSControl:PlaceHolder>
				            
                            <CSControl:PlaceHolder runat="server">
				                <DisplayConditions><CSControl:ControlVisibilityCondition ControlId="PinnedPost" ControlVisiblilityEquals="true" runat="server" /></DisplayConditions>
				                <ContentTemplate>
			                        <p />
				                    <div class="CommonFormFieldName">
					                    <CSControl:FormLabel runat="server" LabelForId="PinnedPost" ResourceName="CreateEditPost_Stickiness" />
				                    </div>
				                    <div class="CommonFormField">
					                    <asp:dropdownlist id="PinnedPost" runat="server" />
				                    </div>
				                </ContentTemplate>
				            </CSControl:PlaceHolder>
        					
        					<CSControl:PlaceHolder runat="server">
				                <DisplayConditions><CSControl:ControlVisibilityCondition ControlId="PostIcon" ControlVisiblilityEquals="true" runat="server" /></DisplayConditions>
				                <ContentTemplate>
				                    <p />
					                <div class="CommonFormFieldName">
					                    <CSControl:FormLabel runat="server" ResourceName="CreateEditPost_PostIcon" LabelForId="PostIcon"><TrailerTemplate>:</TrailerTemplate></CSControl:FormLabel>
					                </div>
					                <div class="CommonFormField">
						                <asp:RadioButtonList id="PostIcon" cellpadding="0" cellspacing="0" width="400" RepeatColumns="18" runat="server" />
					                </div>
				                </ContentTemplate>
				             </CSControl:PlaceHolder>
			            </Content>
		            </TWC:TabbedPane>
		            <TWC:TabbedPane runat="server" id="InkPane">
			            <Tab OnClickClientFunction="InkVisible"><CSControl:ResourceControl runat="server" ResourceName="CreateEditPost_Tab_Ink" /></Tab>
			            <Content>
			                <CSForum:PostInkSubForm ID="InkSubForm" runat="server" ControlIdsToHideWhenNotVisible="InkEditor,InkPane" EnableInkCheckBoxId="EnableInk" InkEditorPlaceHolderId="InkEditor">
			                    <FormTemplate>
			                        <asp:CheckBox ID="EnableInk" Runat="server" Checked="True" Text="Enable Ink?" onclick="ToggleInk(this);" />
			                    </FormTemplate>
			                </CSForum:PostInkSubForm>
			            </Content>
		            </TWC:TabbedPane>
		            <TWC:TabbedPane runat="server" id="VideoPane">
			            <Tab OnClickClientFunction="InkHidden"><CSControl:ResourceControl runat="server" ResourceName="CreateEditPost_Tab_Video" /></Tab>
			            <Content>
			                <CSForum:PostVideoSubForm runat="server" id="VideoSubForm" ControlIdsToHideWhenNotVisible="VideoPane"
			                    IncludeVideoCheckBoxId="IncludeVideo" 
			                    VideoDurationTextBoxId="VideoDuration" 
			                    VideoHeightTextBoxId="VideoHeight" 
			                    VideoImageUrlTextBoxId="VideoImageUrl" 
			                    VideoUrlTextBoxId="VideoUrl" 
			                    VideoWidthTextBoxId="VideoWidth"
			                    Tag="Div" CssClass="CommonFormArea" ValidationGroup="VideoValidationGroup">
			                    <FormTemplate>
		                            <div class="CommonFormFieldName">
				                        <asp:CheckBox Runat="server" ID="IncludeVideo" onclick="if (this.checked) { document.getElementById('VideoContainer').style.display = 'block'; } else { document.getElementById('VideoContainer').style.display = 'none'; }" /> <CSControl:FormLabel runat="server" ResourceName="CreateEditPost_IncludeVideo" LabelForId="IncludeVideo" />
			                        </div>
			                        <div class="CommonFormField">
					                    <CSControl:ResourceControl runat="server" ResourceName="CreateEditPost_IncludeVideo_Details" />
				                    </div>
			                        <CSControl:PlaceHolder ContainerId="VideoContainer" Tag="Div" ID="VideoContainer" runat="server"><ContentTemplate>
			                            <div class="CommonFormFieldName">
						                    <CSControl:FormLabel runat="server" ResourceName="CreateEditPost_VideoUrl" LabelForId="VideoUrl" />
					                    </div>
					                    <div class="CommonFormField">
						                    <asp:textbox id="VideoUrl" runat="server" autocomplete="off" style="width:80%" />
					                    </div>
                    					
					                    <div class="CommonFormFieldName">
						                    <CSControl:FormLabel runat="server" ResourceName="CreateEditPost_PreviewImageUrl" LabelForId="VideoImageUrl" />
					                    </div>
					                    <div class="CommonFormField">
						                    <asp:textbox id="VideoImageUrl" runat="server" autocomplete="off" style="width:80%" />
					                    </div>
                    					
					                    <div class="CommonFormFieldName">
						                    <CSControl:FormLabel runat="server" ResourceName="CreateEditPost_VideoDuration" LabelForId="VideoDuration" />
					                    </div>
					                    <div class="CommonFormField">
						                    <asp:textbox id="VideoDuration" runat="server" autocomplete="off" style="width:80%" />
					                    </div>
                    					
					                    <div class="CommonFormFieldName">
						                    <CSControl:FormLabel runat="server" ResourceName="CreateEditPost_VideoWidth" LabelForId="VideoWidth" />
					                    </div>
					                    <div class="CommonFormField">
						                    <asp:textbox id="VideoWidth" runat="server" autocomplete="off" style="width:80%" />
						                    <asp:RegularExpressionValidator id="VideoWidthValidator" ControlToValidate="VideoWidth" ValidationExpression="^[0-9]*$" runat="server" ValidationGroup="VideoValidationGroup">*</asp:RegularExpressionValidator>
					                    </div>
                    					
					                    <div class="CommonFormFieldName">
						                    <CSControl:FormLabel runat="server" ResourceName="CreateEditPost_VideoHeight" LabelForId="VideoHeight" />
					                    </div>
					                    <div class="CommonFormField">
						                    <asp:textbox id="VideoHeight" runat="server" autocomplete="off" style="width:80%" />
						                    <asp:RegularExpressionValidator id="VideoHeightValidator" ControlToValidate="VideoHeight" ValidationExpression="^[0-9]*$" runat="server" ValidationGroup="VideoValidationGroup">*</asp:RegularExpressionValidator>
					                    </div>
                                    </ContentTemplate></CSControl:PlaceHolder>
			                    </FormTemplate>
			                </CSForum:PostVideoSubForm>
			            </Content>
		            </TWC:TabbedPane>
		            <TWC:TabbedPane runat="server" id="PollPane">
			            <Tab OnClickClientFunction="InkHidden"><CSControl:ResourceControl runat="server" ResourceName="CreateEditPost_Tab_Poll" /></Tab>
			            <Content>
			                <CSForum:PostPollSubForm id="PollSubForm" runat="server" 
			                    AddButtonId="AddButton" 
			                    ControlIdsToHideWhenNotVisible="PollPane" 
			                    ExpireAfterDaysTextBoxId="ExpireAfterDays" 
			                    IncludePollCheckBoxId="IncludePoll" 
			                    NewAnswerTextBoxId="NewAnswer"
			                    PollAnswerRepeaterAnswerTextBoxId="Answer" 
			                    PollAnswerRepeaterDeleteButtonId="DeleteButton" 
			                    PollAnswerWrappedRepeaterId="PollAnswerRepeater" 
			                    PollAnswerRepeaterMoveDownButtonId="MoveDownButton" 
			                    PollAnswerRepeaterMoveUpButtonId="MoveUpButton" 
			                    PollDescriptionEditorId="PollDescription" 
			                    PollTitleTextBoxId="PollTitle"
			                    ValidationGroup="PollValidationGroup">
			                    <FormTemplate>
			                        <div class="CommonFormFieldName">
				                        <asp:CheckBox Runat="server" ID="IncludePoll" onclick="if (this.checked) { document.getElementById('PollContainer').style.display = 'block'; } else { document.getElementById('PollContainer').style.display = 'none'; }" /> <CSControl:FormLabel runat="server" ResourceName="CreateEditPost_IncludePoll" LabelForId="IncludeVideo" />
			                        </div>
			                         <div class="CommonFormField">
					                    <CSControl:ResourceControl runat="server" ResourceName="CreateEditPost_IncludePoll_Details" />
				                    </div>
			                        <CSControl:PlaceHolder ContainerId="PollContainer" Tag="Div" ID="PollContainer" runat="server"><ContentTemplate>
                                        <div class="CommonFormFieldName"><CSControl:FormLabel runat="server" ResourceName="PollEditor_Title" LabelForId="PollTitle" /></div>
                                        <div class="CommonFormField">
		                                        <asp:TextBox runat="server" ID="PollTitle" Style="width: 520px;" />
		                                        <asp:RequiredFieldValidator runat="server" CssClass="CommonValidationWarning"  ValidationGroup="PollValidationGroup" ControlToValidate="PollTitle">*</asp:RequiredFieldValidator>
                                        </div>

                                        <div class="CommonFormFieldName"><CSControl:FormLabel runat="server" ResourceName="PollEditor_Description" LabelForId="PollDescription" /></div>
                                        <div class="CommonFormField"><CSControl:Editor runat="server" ID="PollDescription" Width="650" Height="175" /></div>

                                        <div class="CommonFormFieldName">
                                            <CSControl:FormLabel runat="server" ResourceName="PollEditor_Answers" />
                                        </div>
                                        <div class="CommonFormField">
	                                        <CSControl:WrappedRepeater id="PollAnswerRepeater" runat="Server" EnableViewState="false" ShowHeaderFooterOnNone="false">
		                                        <HeaderTemplate>
			                                        <ol class="CommonPollAnswerList">
		                                        </HeaderTemplate>
		                                        <ItemTemplate>
			                                        <li>
				                                        <asp:TextBox runat="server" Columns="70" ID="Answer" />
				                                        <asp:LinkButton CssClass="CommonPollMoveUpButton" ID="MoveUpButton" runat="server">&nbsp;</asp:LinkButton>
				                                        <asp:LinkButton CssClass="CommonPollMoveDownButton" ID="MoveDownButton" runat="server">&nbsp;</asp:LinkButton>
				                                        <asp:LinkButton CssClass="CommonPollDeleteButton" ID="DeleteButton" runat="server">&nbsp;</asp:LinkButton>
			                                        </li>
		                                        </ItemTemplate>
		                                        <FooterTemplate>
			                                        </ol>
		                                        </FooterTemplate>
		                                        <NoneTemplate>
		                                            <CSControl:ResourceControl ResourceName="PollEditor_NoAnswers" runat="server" />
		                                        </NoneTemplate>
	                                        </CSControl:WrappedRepeater>
                                        </div>

                                        <div class="CommonFormFieldName"><CSControl:ResourceControl runat="server" ResourceName="PollEditor_AddAnswer" /></div>
                                        <div class="CommonFormField">
                                            <asp:TextBox Runat="server" ID="NewAnswer" Columns="70" /> 
                                            <asp:LinkButton Runat="server" ID="AddButton" CssClass="CommonTextButton" ValidationGroup="PollValidationGroup" />
                                        </div>

                                        <div class="CommonFormFieldName"><CSControl:ResourceControl runat="server" ResourceName="PollEditor_ExpiresAfter" /></div>
                                        <div class="CommonFormField">
	                                        <asp:TextBox Runat="server" ID="ExpireAfterDays" Columns="3" /> <CSControl:FormLabel runat="server" ResourceName="PollEditor_ExpiresAfterDays" LabelForId="ExpireAfterDays" />
	                                        <asp:CustomValidator id="ExpireAfterDaysValidator" runat="server" CssClass="CommonValidationWarning" ValidationGroup="PollValidationGroup">*</asp:CustomValidator>
                                        </div>
			                        </ContentTemplate></CSControl:PlaceHolder>
			                    </FormTemplate>
			                </CSForum:PostPollSubForm>
			            </Content>
		            </TWC:TabbedPane>
		            <TWC:TabbedPane runat="server" id="RelatedPane">
			            <Tab OnClickClientFunction="LoadRelatedPosts"><CSControl:ResourceControl runat="server" ResourceName="CreateEditPost_Tab_Related" /></Tab>
			            <Content>
			                <script type="text/javascript">
			                // <![CDATA[
			                    function LoadRelatedPosts()
			                    {
			                        InkHidden();
			                        document.getElementById('relatedpostsloading').style.display = 'block';
			                        var subject = document.getElementById('<%# ((TextBox) ForumControlUtility.Instance().FindControl(this, "PostSubject")).ClientID %>').value;
			                        <%# ((RelatedIndexPostList) ForumControlUtility.Instance().FindControl(this, "RelatedPosts")).GetRelatedIndexPostsScript("subject", "LoadRelatedPostsCallback();") %>
			                    }
			                    
			                    function LoadRelatedPostsCallback()
			                    {
			                        document.getElementById('relatedpostsloading').style.display = 'none';
			                    }
			                // ]]>
			                </script>
			            
			                <div id="relatedpostsloading" style="display:none">Loading <CSControl:Image runat="server" ImageUrl="~/utility/spinner.gif" /></div>
			                <CSControl:RelatedIndexPostList id="RelatedPosts" runat="server" ShowHeaderFooterOnNone="false">
			                    <QueryOverrides PageSize="15" />
			                    <HeaderTemplate>
			                        <ul class="CommonSearchResultList">
			                    </HeaderTemplate>
			                    <ItemTemplate>
			                        <li>
						                <div class="CommonSearchResultArea">
							                <CSControl:IndexPostData runat="server" LinkTo="Post" Property="Title" Tag="H4" CssClass="CommonSearchResultName" LinkTarget="_blank" />
    					                    <CSControl:IndexPostData runat="server" Property="BestMatch" TruncateAt="350" Tag="Div" CssClass="CommonSearchResult" />
							                <div class="CommonSearchResultDetails">
							                    Posted to <CSControl:IndexPostData runat="server" LinkTo="Application" LinkTarget="_blank">
							                        <ContentTemplate>
							                            <CSControl:IndexPostData runat="server" Property="SectionName" /> 
							                            (<CSControl:IndexPostData runat="server" Property="ApplicationType" />)
							                        </ContentTemplate>
							                    </CSControl:IndexPostData> by <CSControl:IndexPostData runat="server" Property="UserName" /> on <CSControl:IndexPostData runat="server" Property="PostDate" />
							                </div>
						                </div>
						            </li>
			                    </ItemTemplate>
			                    <FooterTemplate>
			                        </ul>
			                    </FooterTemplate>
			                    <NoneTemplate>
			                        Sorry, no related posts
			                    </NoneTemplate>
			                </CSControl:RelatedIndexPostList>
			            </Content>
		            </TWC:TabbedPane>
		            <TWC:TabbedPane runat="server" id="PreviewPane">
			            <Tab OnClickClientFunction="Preview"><CSControl:ResourceControl runat="server" ResourceName="CreateEditPost_Tab_Preview" /></Tab>
			            <Content>
			                <script type="text/javascript">
			                // <![CDATA[
			                    function Preview()
			                    {
			                        InkHidden();
			                        document.getElementById('previewloading').style.display = 'block';
			                        <%# ((CreateEditForumPostForm) ForumControlUtility.Instance().FindControl(this, "PostForm")).GetPreviewScript("PreviewCallback();") %>
			                    }
			                    
			                    function PreviewCallback()
			                    {
			                        document.getElementById('previewloading').style.display = 'none';
			                    }
			                // ]]>
			                </script>
			            
				            <div id="previewloading" style="display:none">Loading <CSControl:Image runat="server" ImageUrl="~/utility/spinner.gif" /></div>
				            <CSForum:ForumPostList runat="server" ID="PreviewPostList">
                                <HeaderTemplate><ul class="ForumPostList"></HeaderTemplate>
                                            <ItemTemplate>
			                                    <li>
				                                    <CSUserControl:PostPreview runat="server" />
			                                    </li>
                                            </ItemTemplate>
                                            <FooterTemplate>
		                                        </ul>
		                                    </FooterTemplate>
				            </CSForum:ForumPostList>
			            </Content>
		            </TWC:TabbedPane>
	            </TWC:TabbedPanes>

                <div id="inkWrapper" class="CommonPane" style="position:absolute;left:-5000px;">
                    <asp:PlaceHolder runat="Server" id="InkEditor" />
                </div>
		
	            <div class="CommonFormArea">
		            <div id="Buttons" class="CommonFormField">
			            <CSControl:ResourceButton ResourceName="CreateEditPost_PostButton" id="PostButton" runat="server" ValidationGroup="PostFormValidation" />
		            </div>
	            </div>
            
            </FormTemplate>
        </CSForum:CreateEditForumPostForm>


	</div>
	
	<CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdBottom runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="rcr" runat="server" />

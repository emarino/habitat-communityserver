<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Discussions.Controls.CSForumThemePage" MasterPageFile="forums.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Discussions.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        if (CurrentCSContext.User.IsAnonymous)
            throw new CSException(CSExceptionType.AccessDenied, "Forum subscriptions are only available to registered users");
        
        SetTitle(ResourceManager.GetString("ForumSubscriptionView_Title"), true);
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
<div class="CommonContentArea">
	<CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdTop runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
	
    <CSControl:ResourceControl runat="server" ResourceName="ForumSubscriptionView_Title" Tag="h2" CssClass="CommonTitle" />		
	<div class="CommonContent">
	    <CSControl:ResourceControl runat="server" ResourceName="ForumSubscriptionView_Description" Tag="Div" CssClass="CommonDescription" />
        
        <CSForum:GroupList runat="server">
            <QueryOverrides PageSize="999999" />
	        <ItemTemplate>
		        <div class="CommonListArea">
		        <CSForum:GroupData runat="server" Property="Name" LinkTo="GroupHome" Tag="Div" CssClass="CommonListTitle" />
		        <CSForum:ForumList runat="server">
		            <QueryOverrides IncludeLinkForums="false" PageSize="999999" />
			        <HeaderTemplate>
				        <table cellpadding="0" cellspacing="0" border="0" width="100%">
				        <thead>
					        <tr>
						        <CSControl:ResourceControl runat="server" ResourceName="ForumSubscriptionView_List_Forum" Tag="Th" CssClass="CommonListHeaderLeftMost" />
						        <CSControl:ResourceControl runat="server" ResourceName="ForumSubscriptionView_List_BidirectionalAddress" Tag="Th" CssClass="CommonListHeader"><DisplayConditions><CSControl:SiteSettingsPropertyValueComparison ComparisonProperty="EnableMailGateway" Operator="IsSetOrTrue" runat="server" /></DisplayConditions></CSControl:ResourceControl>
						        <CSControl:ResourceControl runat="server" ResourceName="ForumSubscriptionView_List_Subscribe" Tag="Th" CssClass="CommonListHeader" />
					        </tr>
				        </thead>
			        </HeaderTemplate>
			        <ItemTemplate>
				        <tr>
					        <td width="60%" class="CommonListCellLeftMost">
                                <CSForum:ForumData Property="Name" LinkTo="HomePage" runat="server" /><br />
                                <CSForum:ForumData Property="Description" runat="server" />
					        </td>
                            <CSControl:PlaceHolder runat="server" Tag="Td" width="30%" CssClass="CommonListCell" style="text-align:center;">
                                <DisplayConditions>
                                    <CSControl:SiteSettingsPropertyValueComparison ComparisonProperty="EnableMailGateway" Operator="IsSetOrTrue" runat="server" />
                                </DisplayConditions>    
                                <ContentTemplate>
                                    <CSMail:MailingListData runat="server" LinkTo="MailingList" Property="EmailAddress" />
                                    <CSControl:ResourceControl runat="server" ResourceName="ForumSubscriptionView_NotificationsOnly">
                                        <DisplayConditions Operator="Not">
                                            <CSControl:Conditions runat="server" Operator="And">
                                                <CSMail:MailingListPropertyValueComparison runat="server" ComparisonProperty="IsMailingList" Operator="IsSetOrTrue" />
                                                <CSMail:MailingListPropertyValueComparison runat="server" ComparisonProperty="IsActive" Operator="IsSetOrTrue" />
                                            </CSControl:Conditions>
                                        </DisplayConditions>
                                    </CSControl:ResourceControl>
                                </ContentTemplate>
                            </CSControl:PlaceHolder>
					        <td width="10%" class="CommonListCell" style="text-align: center;">
						        <CSControl:SectionSubscriptionToggleButton ToggleSubscriptionType="Post" runat="server" />
					        </td>
				        </tr>
				        <CSForum:ForumList runat="server">
				            <QueryOverrides IncludeLinkForums="false" PageSize="999999" />
				            <ItemTemplate>
				                <tr>
					                <td width="60%" class="CommonListCellLeftMost"><div style="padding-left: 20px;">
                                        <CSForum:ForumData Property="Name" LinkTo="HomePage" runat="server" /><br />
                                        <CSForum:ForumData Property="Description" runat="server" />
					                </div></td>
                                    <CSControl:PlaceHolder runat="server" Tag="Td" width="30%" CssClass="CommonListCell" style="text-align:center;">
                                        <DisplayConditions>
                                            <CSControl:SiteSettingsPropertyValueComparison ComparisonProperty="EnableMailGateway" Operator="IsSetOrTrue" runat="server" />
                                        </DisplayConditions>    
                                        <ContentTemplate>
                                            <CSMail:MailingListData runat="server" LinkTo="MailingList" Property="EmailAddress" />
                                            <CSControl:ResourceControl runat="server" ResourceName="ForumSubscriptionView_NotificationsOnly">
                                                <DisplayConditions Operator="Not">
                                                    <CSControl:Conditions runat="server" Operator="And">
                                                        <CSMail:MailingListPropertyValueComparison runat="server" ComparisonProperty="IsMailingList" Operator="IsSetOrTrue" />
                                                        <CSMail:MailingListPropertyValueComparison runat="server" ComparisonProperty="IsActive" Operator="IsSetOrTrue" />
                                                    </CSControl:Conditions>
                                                </DisplayConditions>
                                            </CSControl:ResourceControl>                               
                                        </ContentTemplate>
                                    </CSControl:PlaceHolder>
					                <td width="10%" class="CommonListCell" style="text-align: center;">
						                <CSControl:SectionSubscriptionToggleButton ToggleSubscriptionType="Post" runat="server" />
					                </td>
				                </tr>
                                <CSForum:ForumList runat="server">
                                    <QueryOverrides IncludeLinkForums="false" PageSize="999999" />
				                    <ItemTemplate>
				                        <tr>
					                        <td width="60%" class="CommonListCellLeftMost"><div style="padding-left: 40px;">
                                                <CSForum:ForumData Property="Name" LinkTo="HomePage" runat="server" /><br />
                                                <CSForum:ForumData Property="Description" runat="server" />
					                        </div></td>
                                            <CSControl:PlaceHolder runat="server" Tag="Td" width="30%" CssClass="CommonListCell" style="text-align:center;">
                                                <DisplayConditions>
                                                    <CSControl:SiteSettingsPropertyValueComparison ComparisonProperty="EnableMailGateway" Operator="IsSetOrTrue" runat="server" />
                                                </DisplayConditions>    
                                                <ContentTemplate>
                                                    <CSMail:MailingListData runat="server" LinkTo="MailingList" Property="EmailAddress" />
                                                    <CSControl:ResourceControl runat="server" ResourceName="ForumSubscriptionView_NotificationsOnly">
                                                        <DisplayConditions Operator="Not">
                                                            <CSControl:Conditions runat="server" Operator="And">
                                                                <CSMail:MailingListPropertyValueComparison runat="server" ComparisonProperty="IsMailingList" Operator="IsSetOrTrue" />
                                                                <CSMail:MailingListPropertyValueComparison runat="server" ComparisonProperty="IsActive" Operator="IsSetOrTrue" />
                                                            </CSControl:Conditions>
                                                        </DisplayConditions>
                                                    </CSControl:ResourceControl>                               
                                                </ContentTemplate>
                                            </CSControl:PlaceHolder>
					                        <td width="10%" class="CommonListCell" style="text-align: center;">
						                        <CSControl:SectionSubscriptionToggleButton ToggleSubscriptionType="Post" runat="server" />
					                        </td>
				                        </tr>
                                    </ItemTemplate>
				                </CSForum:ForumList>
                            </ItemTemplate>
				        </CSForum:ForumList>
			        </ItemTemplate>
			        <FooterTemplate>
				        </table>
			        </FooterTemplate>
			        <NoneTemplate>
                        <CSControl:ResourceControl runat="server" ResourceName="ForumSubscriptionView_NoEmailListsAvailable">
                            <DisplayConditions><CSControl:SiteSettingsPropertyValueComparison ComparisonProperty="EnableMailGateway" Operator="IsSetOrTrue" runat="server" /></DisplayConditions>
                            <LeaderTemplate><tr><td colspan="3" class="CommonListCellLeftMost" style="text-align: center;"></LeaderTemplate>
                            <TrailerTemplate></td></tr></TrailerTemplate>
                        </CSControl:ResourceControl>
                        <CSControl:ResourceControl runat="server" ResourceName="ForumSubscriptionView_NoEmailListsAvailable">
                            <DisplayConditions Operator="Not"><CSControl:SiteSettingsPropertyValueComparison ComparisonProperty="EnableMailGateway" Operator="IsSetOrTrue" runat="server" /></DisplayConditions>
                            <LeaderTemplate><tr><td colspan="2" class="CommonListCellLeftMost" style="text-align: center;"></LeaderTemplate>
                            <TrailerTemplate></td></tr></TrailerTemplate>
                        </CSControl:ResourceControl>
			        </NoneTemplate>
		        </CSForum:ForumList>
	            </div>
	        </ItemTemplate>
        </CSForum:GroupList>
	</div>
	
	<CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdBottom runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
</div>
</asp:Content>
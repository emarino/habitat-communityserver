<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Discussions.Controls.CSForumThemePage" MasterPageFile="forums.Master" %>
<%@ Register TagPrefix="CSUserControl" TagName="PostPreview" Src="post-preview.ascx" %>
<%@ Import Namespace="CommunityServer.Components" %>
<%@ Import Namespace="CommunityServer.Discussions.Components" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdTop" Src="../Common/Ad-Top.ascx" %>
<%@ Register TagPrefix="CSUserControl" TagName="AdBottom" Src="../Common/Ad-Bottom.ascx" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        SetTitle(ResourceManager.GetString("DeletePost_Title"), Title, true);

        ForumControlUtility.Instance().FindButtonControl(this, "DeletePost").Attributes["onclick"] = "return confirm('" + JavaScript.Encode(ResourceManager.GetString("DeletePost_PopupConfirmation")) + "');";
    }

</script>

<asp:Content ContentPlaceHolderID="bbcr" runat="server">
    <CSForum:BreadCrumb runat="server" Tag="Div" CssClass="CommonBreadCrumbArea">
        <LeaderTemplate><div class="Common"></LeaderTemplate>
        <TrailerTemplate></div></TrailerTemplate>
    </CSForum:BreadCrumb>
</asp:Content>

<asp:Content ContentPlaceHolderID="bcr" runat="server">
<div class="CommonContentArea">
	<CSControl:AdPart runat = "Server" contentname="StandardTop" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdTop runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
	
    <CSControl:WrappedLiteral runat="server" id="Title" Tag="h2" CssClass="CommonTitle" />		
	<div class="CommonContent">
        <CSForum:DeleteForumPostForm runat="server" 
            DeleteButtonId="DeletePost" 
            DeleteChildrenCheckBoxId="DeleteChildren" 
            DeleteReasonTextBoxId="DeleteReason"
            SendAuthorDeleteNotificationCheckBoxId="SendAuthorDeleteNotification"
            >
            <SuccessActions>
                <CSControl:GoToReferralUrlAction runat="server" />
                <CSControl:GoToSiteUrlAction UrlName="forum" Parameter1='<%# CurrentSection.SectionID %>' runat="server" />            
            </SuccessActions>
            <FormTemplate>
                <CSUserControl:PostPreview runat="server" />
            
		        <div class="CommonFormArea">
		            <p />
		            
			        <CSControl:ResourceControl runat="server" ResourceName="DeletePost_OptionDeleteChildren">
			            <DisplayConditions Operator="And">
			                <CSForum:ForumPostPropertyValueComparison runat="server" ComparisonProperty="PostLevel" Operator="GreaterThan" ComparisonValue="1" />
			                <CSForum:ForumPostPropertyValueComparison runat="server" ComparisonProperty="Replies" Operator="GreaterThan" ComparisonValue="0" />
			            </DisplayConditions>
			        </CSControl:ResourceControl>
			        <CSControl:ResourceControl runat="server" ResourceName="DeletePost_ForceDeleteChildren">
			            <DisplayConditions>
			                <CSForum:ForumPostPropertyValueComparison runat="server" ComparisonProperty="PostLevel" Operator="EqualTo" ComparisonValue="1" />
			            </DisplayConditions>
			        </CSControl:ResourceControl>
			        <CSControl:ResourceControl runat="server" ResourceName="DeletePost_ForcePreserveChildren">
			            <DisplayConditions Operator="And">
			                <CSForum:ForumPostPropertyValueComparison runat="server" ComparisonProperty="PostLevel" Operator="GreaterThan" ComparisonValue="1" />
			                <CSForum:ForumPostPropertyValueComparison runat="server" ComparisonProperty="Replies" Operator="EqualTo" ComparisonValue="0" />
			            </DisplayConditions>
			        </CSControl:ResourceControl>
			        
			        <div class="CommonFormFieldName">
				        <CSControl:FormLabel LabelForId="DeleteChildren" runat="server" ResourceName="DeletePost_DeleteChildren" />
				        <asp:CheckBox id="DeleteChildren" runat="server" />
			        </div>
			        
			        <div class="CommonFormFieldName">
                        <CSControl:FormLabel LabelForId="SendAuthorDeleteNotification" runat="server" ResourceName="DeletePost_SendAuthorDeleteNotification" />
				        <asp:CheckBox id="SendAuthorDeleteNotification" runat="server" />
				    </div>

                    <p />

			        <CSControl:ResourceControl runat="server" ResourceName="DeletePost_ReasonText" />
			        
			        <div class="CommonFormFieldName">
				        <CSControl:ResourceControl runat="server" ResourceName="DeletePost_Reason" />
				        <asp:requiredfieldvalidator id="ValidateReason" runat="server" Cssclass="CommonValidationWarning" ControlToValidate="DeleteReason" EnableClientScript="False">You must supply a reason why you are deleting this post.</asp:requiredfieldvalidator>
			        </div>
			        <div>
				        <asp:textbox id="DeleteReason" runat="server" columns="100" TextMode="MultiLine" rows="12" />
			        </div>
			        
			        <div>
				        <CSControl:ResourceButton id="DeletePost" runat="server" ResourceName="DeletePost_DeletePost" />
			        </div>
		        </div>
            </FormTemplate>
        </CSForum:DeleteForumPostForm>
	</div>
	
	<CSControl:AdPart runat="Server" ContentName="StandardBottom" ContentCssClass="CommonContentPartBorderOff" ContentHoverCssClass="CommonContentPartBorderOn">
	    <DefaultContentTemplate>
	        <CSUserControl:AdBottom runat="server" />
	    </DefaultContentTemplate>
	</CSControl:AdPart>
</div>
</asp:Content>

<asp:Content ContentPlaceHolderID="rcr" runat="server" />
<%@ Page EnableViewState="false" Language="C#" AutoEventWireup="true" Inherits="CommunityServer.Discussions.Controls.CSForumThemePage" MasterPageFile="../Common/modal.Master" %>
<%@ Import Namespace="CommunityServer.Components" %>

<script language="C#" runat="server">

    void Page_Load()
    {
        SetTitle(ResourceManager.GetString("Files_Title_UploadFile", "FileGallery.xml"), false);
        
        IText instructions = CSControlUtility.Instance().FindTextControl(this, "Instructions");
        if (instructions != null)
        {
            if (AttachmentForm.AllowLocalAttachment && AttachmentForm.AllowRemoteAttachment)
                instructions.Text = ResourceManager.GetString("UploadAttachment_Description");
            else if (AttachmentForm.AllowLocalAttachment)
                instructions.Text = ResourceManager.GetString("UploadAttachment_Description_FileOnly");
            else if (AttachmentForm.AllowRemoteAttachment)
                instructions.Text = ResourceManager.GetString("UploadAttachment_Description_UrlOnly");
            else
                instructions.Text = ResourceManager.GetString("UploadAttachment_Description_NoAccess");
        }
    }

</script>

<asp:Content ContentPlaceHolderID="bcr" runat="server">

<div class="CommonContentArea">
	<div class="CommonContent">
	
	    <CSForum:UploadPostAttachmentForm id="AttachmentForm" runat="server" ControlIdsToHideWhenRemoteAttachmentsNotAllowed="RemoteLinkArea" ControlIDsToHideWhenLocalAttachmentsNotAllowed="LocalLinkArea" 
	        ErrorTextId="Error" 
	        FileHtmlInputFileId="File" 
	        LinkToUrlRadioButtonId="LinkToUrl" 
	        UploadFileRadioButtonId="UploadFile" 
	        SaveButtonId="Save" 
	        UrlTextBoxId="Url"
	        >
	        <FormTemplate>
    	        <div class="CommonFormArea">
    	        
    	        <CSControl:WrappedLiteral runat="server" ID="Error" Tag="Div" CssClass="CommonValidationWarning" />
        				
		        <table width="100%" cellpadding="0" cellspacing="0" border="0">
			        <tr>
				        <td class="CommonFormDescription" colspan="3">
					        <asp:Literal Runat="server" ID="Instructions" />
				        </td>
			        </tr>
	                <tr id="LocalLinkArea" runat="server">
		                <td class="CommonFormField">
			                <asp:RadioButton ID="UploadFile" runat="server" GroupName="UploadOption" Checked="True" />
		                </td>
		                <td class="CommonFormFieldName">
			                <CSControl:ResourceControl ResourceName="UploadAttachment_File" runat="server" />
			                <div class="CommonFormFieldDescription">
				                <CSControl:ResourceControl ResourceName="UploadAttachment_File_Description" runat="server" />				
			                </div>
		                </td>
		                <td class="CommonFormField"><input id="File" type="file" runat="server" /></td>
	                </tr>
	                <tr id="RemoteLinkArea" runat="server">
		                <td class="CommonFormField">
			                <asp:RadioButton ID="LinkToUrl" Runat="server" GroupName="UploadOption" />
		                </td>
		                <td class="CommonFormFieldName">
			                <CSControl:ResourceControl ResourceName="UploadAttachment_Url" runat="server" />
			                <div class="CommonFormFieldDescription">
				                <CSControl:ResourceControl ResourceName="UploadAttachment_Url_Description" runat="server" />
			                </div>
		                </td>
		                <td class="CommonFormField"><asp:TextBox ID="Url" Runat="server" Columns="40" /></td>
	                </tr>
			        <tr>
				        <td colspan="3" align="right">
					        <CSControl:ResourceLinkButton id="Save" Runat="server" CssClass="CommonTextButton" ResourceName="Save" />
				        </td>
			        </tr>
		        </table>
		        </div>
            </FormTemplate>
	    </CSForum:UploadPostAttachmentForm>
	</div>
</div>

<script type="text/javascript">
// <![CDATA[
function enableFileUpload()
{
	var e = document.getElementById('<%= CSControlUtility.Instance().FindControl(this, "File").ClientID %>');
	if (e) { e.disabled = false; }
	e = document.getElementById('<%= CSControlUtility.Instance().FindControl(this, "Url").ClientID %>');
	if (e) { e.disabled = true; }
}

function enableLinkToUrl()
{
	var e = document.getElementById('<%= CSControlUtility.Instance().FindControl(this, "File").ClientID %>');
	if (e) { e.disabled = true; }
	e = document.getElementById('<%= CSControlUtility.Instance().FindControl(this, "Url").ClientID %>');
	if (e) { e.disabled = false; }
}

var e = document.getElementById('<%= CSControlUtility.Instance().FindControl(this, "UploadFile").ClientID %>');
if (e) { e.onclick = enableFileUpload; }
if (e && e.checked)
	enableFileUpload();

e = document.getElementById('<%= CSControlUtility.Instance().FindControl(this, "LinkToUrl").ClientID %>')
if (e) { e.onclick = enableLinkToUrl; }
if (e && e.checked)
	enableLinkToUrl();
// ]]>
</script>

</asp:Content>
